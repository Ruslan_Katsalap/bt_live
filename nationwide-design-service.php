<?php require_once('./inc/header.inc.php'); ?>
<style>
@font-face {
    font-family: 'libre_baskervillebold';
    src: url('/fonts/baskerville/librebaskerville-bold-webfont.woff2') format('woff2'),
         url('/fonts/baskerville/librebaskerville-bold-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}
.upgradetitlewrap p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color:#114577;
    line-height: 24px;
    font-weight: normal;
}
.upgradetitlewrap h1 {
   font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 10px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	padding-top: 0px;
	border-bottom: 1px solid #0e4776;
}
.upgradeleft {
	float:left;
	width:60%
	}
.upgraderight {
	float:right;
	width:38%;
	}
.upgradeimage img {
    width: 100%;
}	
.upgradepoints ul li {
    margin-bottom: 17px;
    font-size: 18px;
    color: #0f4778;
    text-align: left;
    /* margin-top: 20px; */
    position: relative;
    margin-bottom: 50px;
    text-indent: 30px;
}
.upgradepoints a {
    background: #87a3bb;
    color: #FFF;
    padding: 10px 20px;
    font-size: 16px;
}
.upgradepoints ul {
    list-style: none;
    font-size: 16px;
    color: #a7a9ac;
}
.upgradepoints h1 {
    margin-bottom: 10px !important;
    padding-top: 0px;
    text-align: center;
    font-size: 24px !important;
    border-bottom: 2px dotted #0f4778;
    margin-bottom: 20px;
}
.upgradeinner {
    padding: 30px 0px;
    margin: 20px 0px;
}
.floaticons {
    position: absolute;
    width: 150px;
    
}
.upgradepoints {
	position:relative;
	}
.floaticons img {
    width: 130px;
}
.upgrademiddle h2 {
    font-size: 18px;
    color: #003c70;
    font-weight: normal;
}
.upgradetitlewrap {
    padding-bottom:20px;
}

.upgradepoints p {
    font-size: 16px;
    color: #74787f;
    /* margin-top: 83px; */
    padding: 20px 0px;
    text-align: center;
    background: #0f4778;
}
.upgradepoints p a {
	color:#FFF;
	font-size:20px;
	}
.upgardebutton {
	background:#edf2f6;
	padding:20px 0px;
	}
.upgardebutton h1 {
   
    padding: 20px 0px;
    text-align: center;
    margin-bottom: 0px !important;
    padding-top: 0px;
    padding-bottom: 0px !important;
    
}	
.upgradeimageinner {
    height: 110px;
    width: 110px;
    text-align: center;
    background:rgba(255,255,255,0.8);
    display: table-cell;
    vertical-align: middle;
    border-radius: 100px;
}
.upgradeimageinner p {
	margin-top: 0px;
    color: #0f4776;
    font-weight: bold;
    padding: 0px;
	font-size:16px;
	}
.popupinner {
    width: 500px;
    margin: 0 auto;
    top: 35%;
    position: relative;
    background: #edf2f6;
    text-align: center;
}
.popupform {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(15, 71, 118,0.8);
    z-index: 11111;
}
.popupfield form input[type=text] {
    width: 100%;
    border: none;
    height: 35px;
    margin-bottom: 10px;
	text-indent:10px;
}
.popupfield {
    padding: 20px 40px;
    text-align: left;
}
.popupfield form input[type=submit] {
    background: #0f4778;
    padding: 10px 30px;
    border: none;
    color: #FFF;
    font-size: 16px;
}
.popupfield h2 {
    font-size: 22px;
    color: #0f4778;
}
.popupfield p {
    color: #969696;
    font-size: 16px;
    padding-top: 0px;
    margin: 10px 0px;
}
.popupclose {
    position: absolute;
    right: 10px;
    top: 5px;
    font-size: 26px;
    color: #0f4778;
}
.popupform {
	display:none;
	}
.upgardebutton {
	cursor:pointer;
	}
.upgradethankyoupop{
    display:none;
    position: fixed;
    height: 100%;
    width: 100%;
    background: rgba(16, 70, 116, 0.67);
    top: 0px;
    z-index: 50;
    left: 0px;
}	
.thanksinner {
    position: relative;
    top: 35%;
    height: 130px;
    max-width: 350px;
    background: #FFF;
    left: auto;
    right: auto;
    margin: 0 auto;
    padding: 20px 20px;
    width: 100%;
    text-align: left;
}
.thanksinner h2 {
   
    padding-bottom: 15px;
    color: rgb(16, 70, 116);
    font-size: 20px;
}	
.thanksinner p {
	font-size:17px;
	font-weight:normal;
	color:#969696;
	}
.upgradeimage {
	position:relative;
	}
.stickerimage {
    position: absolute;
    bottom: 10px;
    left: 10px;
}
.upgradeimage .note {
    font-size: 16px;
    margin: 0px;
    padding: 0px;
    color: #0f4778;
}
.upgrainline {
    width: 24.7%;
    text-align: center;
    display: inline-block;
    padding: 20px 0px;
	position:relative;
}
.upgradetitle h2 {
    color: #0f4778;
    font-size: 18px;
}
.upgradedesc p {
    color: #9f9f9f;
    font-size: 16px;
}
.upgrainline:after {
    content: "";
    width: 1px;
    height: 120px;
    position: absolute;
    border-left: 2px dotted #9f9f9f;
    top: 35px;
    right: 0px;
}
.upgrainline:nth-child(4):after {
	display:none;
	}
.bottomshortcut {
	max-width:980px;
	margin:0 auto;
	}
.shortcutinline {
    float: left;
    width: 22.2%;
}
.shortcutinline:nth-child(3) {
    margin-right: 3%;
}
.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
    margin-left: 3%;
}
.shortcutdesc {
    background: rgba(255,255,255,0.5);
    padding: 10px;
    text-align: center;
    color: #0f4679;
    position: absolute;
    bottom: 0px;
    width: 100%;
    font-weight: bold;
    font-size: 18px;
	height:100%;
}
.shortcutwarp {
    position: relative;
    border-radius: 100px;
    overflow: hidden;
}
.shortcutwarp img {
    max-width: 100%;
    border-radius: 100%;
}
.shortcutdesc span {
	top: 38%;
    position: relative;
    right: 10px;
	}
.tenderprocess {
    background: #ecebeb;
    margin: 50px 0px;
}
.tenderpprocesstitle h1 {
    font-weight: normal;
    padding: 0px;
}
.upgrageicons .upgradeimage img {
    max-width: 90px;
    margin-bottom: 10px;
}	
.moreinfo h1 {
    font-size: 20px !important;
    border-bottom: 2px solid #0f4778;
}
.upgradepoints ul li:before {
    content: "";
    background: url(/images/nationwide/arrow.jpg) no-repeat center;
    height: 40px;
    width: 20px;
    position: absolute;
    left: 0;
    background-size: contain;
    top: -7px;
}
.upgrademiddle {
    border-bottom: 7px dotted #b9bbbf;
    margin-bottom: 50px;
}
@media (max-width:640px) {
	.upgradetitlewrap h1 {
		font-size:22px !important;
		}
	.upgradetitlewrap p {
		font-size:18px; 
		}
	.upgradeleft, .upgraderight {
		width:100%;
		}
	.floaticons {
		display:none;
		}
	.upgradepoints p {
		margin-top:0px;
		}
	.upgradepoints ul li {
		font-size:18px;
		}
	.upgradepoints h1 {
		font-size:22px !important;
		margin-top:20px;
		}				
	.upgardebutton h1 {
		font-size:22px !important;
		}
	.popupinner {
		width:90%;
		}		
	.popupfield {
		padding:20px 10px;
		}
	.upgradetitlewrap {
		padding-bottom:20px;
		}
	.upgradepoints ul li {
		font-size:14px !important;
		}	
	.shortcutinline {
		width: 60%;
		margin: 0px auto !important;
		float: none;
	}			
	}	
</style>
<div class="upgradewrap">
	<div class="upgradetitlewrap">
    	<h1>Nationwide Design Service</h1>
        <p>While the majority of our work is within London, we do work with homeowners all over the country. Our Nationwide Design Services enables homeowners to benefit from our fixed fee Design Service, and we can even help you find a local contractor for your build if required. The Design Phase process is exactly the same as it is for our London clients, and you are assigned one Architectural Designer who travels to meet you at your property to discuss your project and measure up in detail. <a href="https://www.buildteam.com/architectural-design-phase.html ">Learn more about our Design Process here.</a></p>
    </div>
    
    
    <div class="upgrademiddle">
    	<h2>We have a special pricing structure for clients of our Nationwide Design Service. We charge an initial fee of £495 + VAT to visit the property, undertake a full design consultation and carry out the measured survey. This visit typically lasts around half a day. We will respond with concept plans, and work with you to refine your preferred scheme. Should you decide not to proceed beyond the concepts phase, you are free to pause the work at this juncture.</h2>
    	<div class="upgradeinner">
            <div class="upgradeleft">
            	<div class="upgradepoints">
                	<ul>
                    	<li>Measured Survey at your property</li>
                    	<li>Production of Architectural drawings & planning pack</li>
                    	<li>Preparation and submission of planning application</li>   
                        <li>Structural calculations</li>
                        <li>Building Regulation Drawings</li>
                        <li>Detailed Schedule of Works</li>
                        <li>Indicative pricing of the itemised scheme to help with your tender process</li>                  	
                    </ul>
                    <a href="/contact.html">Enquire Now</a>
                </div>
            </div>
            <div class="upgraderight">
            	
                <div class="upgradeimage">
                	<img src="images/nationwide/map.jpg" alt="">
                   	
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
    </div>
    <div class="moreinfo">
    	
    </div>
    <div class="bottomshortcut">
	<div class="shortcutinline">
    	<a href="https://www.buildteam.com/contact.html ">
            <div class="shortcutwarp">
                <img src="images/nationwide/1.jpg" alt="">
                <div class="shortcutdesc"><span>Enquire <br>About This <br>Service</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/design-planning-database.html">
            <div class="shortcutwarp">
                <img src="images/nationwide/2.jpg" alt="">
                <div class="shortcutdesc"><span>View<br>Example<br>Plans</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/project-gallery/gallery.html ">
            <div class="shortcutwarp">
                <img src="images/nationwide/3.jpg" alt="">
                <div class="shortcutdesc"><span>Explore<br>Our<br>Gallery</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
        <a href="http://www.buildteam.com/news.html">
            <div class="shortcutwarp">
                <img src="images/nationwide/4.jpg" alt="">
                <div class="shortcutdesc"><span>Latest<br>News & <br>Press</span></div>	
            </div>
         </a>
    </div>
</div>
</div>
<div class="popupform">
	<div class="popupinner">
    	<div class="popupfield">
        	<h2>Want to Upgrade to a Premium Site Visit?</h2>
            <p>Please enter a few details</p>
        	<form method="post" action>
            	<input type="text" name="name" placeholder="Full name*" required>
                <input type="text" name="address"  placeholder="Address*" required>
                <input type="submit">
            </form>
            <div class="popupclose"><span>x</span></div>
        </div>
    </div>
</div>
<div class="upgradethankyoupop">
    	<div class="thanksinner">
        	<h2>Thank you!</h2>
            <p>Thank you for upgrading to a premium site visit. A member
of the team will be in touch to confirm.</p>
			<a class="closeit" href="javascript:void(0);">Ok</a>
            
        </div>
    </div>

<script>
$(".upgardebutton h1").click(function(){
	$(".popupform").fadeIn(300);
	});
$(".popupclose").click(function(){
	$(".popupform").fadeOut(300);
	});	
$(".closeit").click(function(){
	$(".upgradethankyoupop").fadeOut(300);
	});	
</script>
<script>
$(document).ready(function(e) {
    <? if($upgradesend == true){?>
$(".upgradethankyoupop").fadeIn(200);
<? } ?>
});

</script>
<?php require_once('./inc/footer.inc.php'); ?>