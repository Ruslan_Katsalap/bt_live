<style>
    .home-gallery-p {
        width: 51%;
    }
</style>
<div class="home-gallery">
  <div class="home-gallery-text">
	<div class="home-gallery-h2">
  		<h2>Explore<br>
		  Our Site.</h2>
	</div>
<div class="home-gallery-p">
  <p>Build Team offers a wide variety of services with in-depth guides on how
you can get started. Explore further and see how we can help you with your
dream extension.</p>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:30px">
				
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 space-site">
				
</div>
<div class="col-xs-8 col-sm-8 col-md-2 col-lg-2" style="border:2px solid #183e70;padding:25px;margin-right:30px">
	<h3 class="our-site-title">Meet the Team.</h3>
	<div class="event-button">
			<a href="https://buildteam.com/our-team/meet-the-team.html">Find out more</a>
			<div class="event-button-arrow"></div>
	</div>
</div>
<div class="col-xs-8 col-sm-8 col-md-2 col-lg-2" style="border:2px solid #183e70;padding:25px;margin-right:30px">
	<h3 class="our-site-title">Design Database.</h3>
	<div class="event-button">
			<a href="https://buildteam.com/design-planning-database.html">Find out more</a>
			<div class="event-button-arrow"></div>
	</div>
</div>
<div class="col-xs-8 col-sm-8 col-md-2 col-lg-2" style="border:2px solid #183e70;padding:25px">
	<h3 class="our-site-title">Why Choose Us.</h3>
	<div class="event-button">
			<a href="https://buildteam.com/about-us/why-choose-page.html">Find out more</a>
			<div class="event-button-arrow"></div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:30px">
				
</div>

</div>

</div>