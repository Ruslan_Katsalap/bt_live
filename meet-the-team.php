<?php
require_once('./inc/header.inc.php');
?>

<style>
.col_two_fifth.col_last {
    display: none;
}
.meet-team {
	padding: 70px 0 140px 0 !important;
background-color: #E6E6E7;
	}
.main {
	overflow:hidden;
	padding: 97px 0 0 0;
	}	

div.img_team_mem {
	border-radius:0px !important;
}		

div.img_team_mem-new {
    margin-bottom: 0px;
    height:400px;
    width: 400px;
    overflow: hidden;
}

div.img_team_mem-new img {
	
	width:100%;
	height:auto;
	}
.team_member_desc {
	position: relative;
	top: 0px;
	left: 0;
	padding: 0px;
	font-size: 16px;
	text-align: left;
	line-height: 1.47;
	color: #183e70;
	}
.team_mem_details {
    text-align: center;
}	
.team_member_desc {
	
	}
.openinfo {
    position: relative;
    width: 100%;
    top: 0px;
    height: 0px;
	transition:ease-in-out 0.3s;
	overflow:hidden;
	
}	
.infoopened {
	height:auto;
	transition:ease-in-out 0.3s;
	}
#the-team .newteam:first-child {
	}
.openplus:after {
    /*content: "+";
    position: absolute;
    height: 20px;
    width: 20px;
    bottom: 15px;
    color: #000;
    z-index: 14;
    font-size: 22px;
    display: block;
    
    right: 0;
    text-align: center;
    cursor: pointer;
	display:none;*/
}

span.closeplus:after {
    content: "-";
    cursor:pointer;
    font-size: 40px;
    line-height: 0px;
	display:none;
}
.closeplus {
	position: absolute;
    top: -5px;
    right: 12px;
}
.ceotitle {
	margin-bottom:40px;
	}
.leftimg {
	width:32.5%;
	float:left;
	margin-right:2%;
	}
.righttext {
	width:65.5%;
	float:left;
	}
.ceoimg img {
	max-width:100%;
	}
.ceoimg h5 {
	font-size:20px;
	color:#0f4778;
	margin-top:5px;
	}		
.ceoimg h6{
	font-size:18px;
	color:#6f6f6f;
	font-weight:lighter;
	}	
.ceoimg p {
	font-size:18px;
	line-height:20px;
	margin-top:5px;
	color:#6f6f6f;
	text-align:justify;
	}
.img_team_mem-new:after {
    /*content: "+";*/
    position: absolute;
    bottom: 65px;
    right: 0px;
    background: rgba(255,255,255,0.6);
    padding: 5px 10px;
    font-size: 20px;
}
/*h1.teampg .designimages, h1.teampg .buildimages {
    float: right;
}*/
#lowopacity {
	opacity:0.5;
	float:right;
	}
span#lowopacity:before {
    content: ">";
    margin-right: 5px;
    font-family: monospace;
}	
@media (min-width:640px) {
	
	.popupdetails {
		display:none !important;
		}
	.ceotitlemob {
	display:none;
	}
.ceotitledesk {
	display:block;
	}			
	}		
@media (max-width:640px) {
	.main {
		padding: 98px 0 0 0 !important;
	}
	.meet-team {
    padding: 0px 0 0px 0 !important;
	}
	.team_mem_details {
		height: 75px;
	}
	.linkedin {
	width:320px !important;
	height:27px;
	background-color:#183E70;
	opacity:0.7;
	position: absolute;
	top: 240px;
}
.limob {
	top: 273px !important;
}
.linkedin a img {
	width:16px !important;
	height:16px  !important;
}
.linkedin a {
	float:right;
	margin: 6px 7px 6px 0;
}
.linkedin .arrow-link {
	height: 27px;
	margin: 8px 10px 8px 0;
}
.linkedin a img.arrow {
	width:10px !important;
	height:10px !important;
}
}
	@media (min-width:641px) {
	.col-lg-5 {
    width: 38.5%;
	}
	.content-block {
		margin: 0 auto 55px auto;
	}
	.col-lg-1 {
    width: 12.3%;
}
.linkedin {
	width:400px;
	height:35px;
	background-color:#183E70;
	opacity:0.7;
	position: absolute;
	top: 365px;
}
.dan {
	top: 265px !important;
}
.dan-img {
	height: 300px !important;
}
.linkedin a img {
	width:20px;
	height:20px;
}
.linkedin a {
	float:right;
	margin: 8px 7px 8px 0;
}
.linkedin .arrow-link {
	height: 35px;
	margin: 12px 10px 12px 0;
}
.linkedin a img.arrow {
	width:12px;
	height:12px;
}
	}


</style>
</div>
<div class="full meet-team">
	<div class="content-block">
	<h2 class="teampg">Meet our Team</h2>
	<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1" style="padding:0">
</div>
<div class="col-xs-10 col-sm-10 col-md-5 col-lg-5" style="padding:0">
<div class="img_team_mem-new dan-img">
	  <img class="imagenormal imagenormal1" src="https://www.buildteam.com/images/meet/dan.jpg" data-img1="/team/david57a1c66a8362b.jpg" data-img2="/team/high-court-india-i574ea49ee52e6.jpg" data-ng="m1" width="">
  <div class="linkedin dan">
	
	<a href="https://www.linkedin.com/in/davidsondan/" class="arrow-link" target="_blank">
		<img class="arrow" src="/images_new_design/linkedin-link-arrow-desktop-01.svg">
	</a>
	<a href="https://www.linkedin.com/in/davidsondan/" target="_blank">
		<img src="/images_new_design/linkedin-icon_desktop-01.svg">
	</a>
  </div>
	</div>
	<div class="team_mem_details" id="m1c">
		<h3 class="team_member_title">Dan Davidson</h3>
		<div class="jobtitle">Chief Executive</div>
		
	</div>
</div>
<div class="col-xs-10 col-sm-10 col-md-5 col-lg-5 newteam" style="padding:0">

  
		
	
		<p class="team_member_desc">Dan began his career as a marketing graduate at Sainsbury's.
He then went on to further his banking career at Barclays for a
period of 10 years which culminated in his role of Corporate
Director in Business Services. In 2007, Dan said goodbye to a
career in banking and founded Build Team, a home extension
specialist that offers a Design & Build service to homeowners
across London.<br>
Dan lives in Dulwich with his wife and two sons and enjoys
cycling, tennis and skiing. He is a trustee to the Board of
Groundwork London, a charity established in 1982, which aims
to support local economies to promote environmental
sustainability, and to provide young people with support and
employment opportunities to further their careers.	</p>
	


</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding:0;height:60px">
</div>
</div>


<div class="content-block">
<h2 class="teampg">Design Team</h2>
<div id="the-team">
	
<?php
$rs = getRs("SELECT * FROM team WHERE is_enabled = 1 AND team_design = 1 AND is_active = 1 ORDER BY sort");

$i = 0;
while ( $row = mysqli_fetch_assoc($rs) ) {
  $i++;//376
  $ng="m".$i;
	echo '	<div class="team-member-new newteam" id="designteam">

  <div class="img_team_mem-new"><img class="imagenormal imagenormal1" src="/team/' . $row['imagefile_lg'] . '" width=""  data-img1="/team/' . $row['imagefile_lg'] . '" data-img2="/team/' . $row['imagefile_sm'] . '" data-ng="' . $ng . '" />
  <div class="linkedin limob">
	
  <a href="' . $row['linkedin'] . '" class="arrow-link" target="_blank">
	  <img class="arrow" src="/images_new_design/linkedin-link-arrow-desktop-01.svg">
  </a>
  <a href="' . $row['linkedin'] . '" target="_blank">
	  <img src="/images_new_design/linkedin-icon_desktop-01.svg">
  </a>
</div>
 
  </div>
  
   
<div class="team_mem_details" id="' . $ng . 'c"><h3 class="team_member_title">' . $row['name'] . '</h3>'.'<div class="jobtitle">' . $row['title'] .'</div><span class="openplus">+ Bio</span></div>';



echo '<div class="openinfo"><p class="team_member_desc">' . $row['description'] . '<span class="closeplus"></span></p></div>';

echo '</div>';
}
?>
</div>
<div class="clear"></div>	
</div>

<div class="testimonialscontainer" style="background-color:#fff">
  <div class="mainpadding">
    <div class="testimonialswrap">
      <div class="sectiontitle">
        <h2 class="mainnewtitle-border">Client Testimonials</h2>

      </div>
      
    </div>
    
</div>
<section class="lazy slider" data-sizes="50vw">
  
    <?php
    $i = 0;
    $rs = getRs("SELECT testimonial_id, quote, name, title, project_url, video_url, thumbnailImg,category FROM testimonials WHERE is_active = 1 AND is_enabled = 1 ORDER BY sort");
    $total = mysqli_num_rows($rs);
    $half = false;
    
    while ( ($row = mysqli_fetch_assoc($rs)) && ($i <= 4) ) {
      $i++;
      if($row['video_url'] != '' && $row['video_url'] != null) {
      }
      else {
    ?>
    <div>
    <div class="threetestimonials testbottom">
    <div class="nameclient"><?php echo $row['name']; ?><br>
      <span style="font-weight:300"><?php echo $row['title']; ?></span></div>
      <p>
        <?php echo $row['quote']; ?>
      </p>
      
    </div>
  </div>
  
    <?php 
      }
    } ?>  
    
</section>
</div>


<div class="content-block" style="margin:110px auto -90px">
<h2 class="teampg">Build Team</h2>
<div id="the-team">
<?php
$rs = getRs("SELECT * FROM team WHERE is_enabled = 1 AND team_build = 1 AND is_active = 1 ORDER BY sort ASC");
//$rs = getRs("SELECT * FROM team WHERE is_enabled = 1 AND team_part = 'build' AND is_active = 1 ORDER BY sort");
$i = 0;
while ( $row = mysqli_fetch_assoc($rs) ) {
  $i++;//376
  
  $ng="n".$i;
	echo '	<div class="team-member-new newteam" id="buidteam">

  <div class="img_team_mem-new"><img class="imagenormal imagenormal1" src="/team/' . $row['imagefile_lg'] . '" width=""  data-img1="/team/' . $row['imagefile_lg'] . '" data-img2="/team/' . $row['imagefile_sm'] . '" data-ng="' . $ng . '" />
  <div class="linkedin limob">
	
  <a href="' . $row['linkedin'] . '" class="arrow-link" target="_blank">
	  <img class="arrow" src="/images_new_design/linkedin-link-arrow-desktop-01.svg">
  </a>
  <a href="' . $row['linkedin'] . '" target="_blank">
	  <img src="/images_new_design/linkedin-icon_desktop-01.svg">
  </a>
</div> 
  </div>
  
   
<div class="team_mem_details" id="' . $ng . 'c"><h3 class="team_member_title">' . $row['name'] . '</h3>'.'<div class="jobtitle">' . $row['title'] .'</div><span class="openplus">+ Bio</span></div>';


echo '<div class="openinfo"><p class="team_member_desc">' . $row['description'] . '<span class="closeplus"></span></p></div>';

echo '</div>';
}
?>
</div>
</div>
<div class="clear"></div>
</div>
<div style="width:100%;max-width:1920px;background-color:#fff;">
<?php
        require_once('explore-our-site.php');
    ?>
</div>
<div class="popupdetails">
	<div class="popuphtml">
    	
    </div>
    <div class="cosepopup"></div>
</div>

<script>
$(document).ready(
	function(e) {
    	$(".newteam").hover(function(){
		$(this).children(".openinfo", this).addClass("infoopened")
		},function () {
                  $(this).children(".openinfo", this).removeClass("infoopened");
        
	});
});

</script>
<script>
	$(".newteam").click(function(){
		$(".popupdetails").fadeIn(300);
		var getinfo = $(this).children(".openinfo").html();
		$(".popupdetails .popuphtml").html(getinfo);
		});
	$(".cosepopup").click(function(){
		$(".popupdetails").fadeOut(300);
		});
</script>
<script>
$('.teampg a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top - 200
            }, 1000);
            return false;
        }
    }
});
</script>
<?php
require_once('./inc/footer.inc.php');
?>