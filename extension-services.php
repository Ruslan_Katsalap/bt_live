<?php
	require_once('./inc/header.inc.php');
?>
<style type="text/css">
	.tour_dates {
		width: 25%;
		margin-bottom: 18px;
		float: left;
		border: 3px solid #183e70;
		padding: 38px 25px 25px 25px;
		margin-right: 30px;
}

.date_box {
    width: 17%;
    background: #a0a0a0 !important;
    float: left;
    text-align: center;
    padding: 15px;
	color: #fff !important;
	margin-right:2%;
	min-height: 30px;
}
.form_row {
  float: left;
  margin: 10px 0;
  width: 100%;
}
.form_row > label {
  display: block;
    font-weight: regular;
    line-height: 1em;
    margin-bottom: 0;
    margin-top: 12px;
    font-size: 20px;
    float: left;
	color:#183e70 !important;
}
.form_input{
  width: 100%;
    margin-top: 5px;
    border: 1px solid #8b8f94;
    margin-bottom: 10px;
    background: none;
    max-height: 32px;
	height:32px;
}
.form_textarea {
  width: 100%;
    margin-top: 5px;
    border: 1px solid #8b8f94;
    margin-bottom: 10px;
    background: none;
}
 .register_details {
 background: #0f4778;
    padding: 15px 30px 15px 30px;
    color: #fff;
    cursor: pointer;
    width: 100px;
    border: none;
    font-size: 18px;
    margin-top: 20px;
}
.success_msg {
  border: 2px solid green;
  float: left;
  margin-bottom: 25px;
  padding: 6px 8% 6px 10px;
  width: auto;
  color: green;
}
.error_msg {
  background: #fca1a1 none repeat scroll 0 0;
  border: 2px solid #f00;
  color: #B71D1D;
  float: left;
  margin-bottom: 25px;
  padding: 5px 12px;
  width: 48%;
}
#housetourbg {
	position: relative;
max-width: 1920px;
margin: 0 auto;
background-color: #E7E7E8;
width: 100%;
	}
#housetourbg video {
	max-width:1140px;
	margin: 70px 20% 90px;
	}
.playpause {
    background-image:url(../housetour/play_arrow.png);
    background-repeat:no-repeat;
    width: 1140px;
	height: 607px;
	position: absolute;
	left: 0%;
	right: 3px;
	top: -5px;
    bottom:0%;
    margin:auto;
    background-size:cover;
	background-position: top;
	
}
.next_tour_arrow img {
    position: absolute;
    right: -100px;
    /* bottom: 0px; */
    margin-top: -20px;
	max-width: 90px;
}
.groud_flour_arrow img {
	 position: absolute;
    left: -145px;
    /* bottom: 0px; */
    margin-top: -40px;
    max-width: 140px;
	}
.needmoreinfo {
	display:none;
	}
.needmoreinfo img {
	position: absolute;
    right: -155px;
    /* bottom: 0px; */
    margin-top: 0px;
    max-width: 170px;
	}
.bookbutton {
    position: absolute;
    right: 20px;
}
.bookbutton a {
    padding: 5px 10px;
    border: 1px solid #a0a0a0;
}
.tour_dates {
	position:relative;
	}
.forbiggerfonts {
	font-size:15px;
	line-height: 17px;
	}
.date_box .bigfonts {
    font-size: 16px;
	padding-bottom: 5px;
    display: inline-block;
}
.details_box {
    position: relative;
    top: 15px;
	line-height: 18px;
	text-transform:capitalize;
}
.glyphicon-chevron-left:before, .glyphicon-chevron-right:before {
	content:"" !important;
	}
.carousel-control span img {
    max-width: 40px;
}
.col_full h1 {
	color: #f5641e !important;
	font-size:22px !important;
	}
#new_page_house_tours_new {
	font-size: 30px !important;
    color: #003c70;
    text-transform: capitalize !important;
    font-weight: bold;
    font-family: calibri !important;
	padding-bottom: 10px !important;
    margin-bottom: 20px !important;
	}
.forbiggerfonts p:first-child {
	padding-top:0px !important;
	}
.forbiggerfonts p {
    color: #75787f;
	font-weight: 400;
}
.forbiggerfonts p strong {
	    color: #0f4678;
	}
.form_row label {
	color: #104771;
	}
.broform100 input[type=checkbox] {
    width: auto;
    height: auto;
    vertical-align: middle;
    width: 20px;
    height: 20px;
    margin-right: 5px;
    padding: 5px;
}
.checkboxbro span {
    font-size: 16px;
    display: inline-block;
    width: 90%;
	vertical-align: top;
	line-height:1.5;
}
@media (max-width:640px) {
	.next_tour_arrow img, .groud_flour_arrow img, .needmoreinfo img{
		display:none !important;
		}
	.tour_dates {
		width:100%;
		}
	.bookbutton {
		position: relative;
		 right:0px !important;
		/* bottom: 4px; */
		margin-top: 25px;
}
.page_house_tours_new {
  padding-top: 260px !important;
}
#bookform h1 {
	font-size:18px;
	margin-top:20px
	}
#carousel-example-generic {
	display:none;
	}
.register_details {
	margin-bottom:20px;
	}

	}
@media (min-width:640px) {
	.details_box {
		line-height: 18px;
		}
		.bookbutton {
	bottom: 25px;
  }
  .mobile-title {
  width:36%;margin-left: 140px;
}
.home-gallery-h2 h2 {
  margin-left: 0px;
}
	}
@media (min-width:641px) and (max-width:770px){
.col_last {
	float:right;
	}
form .col_half {
    width: 100%;
}
}
</style>
<section class="open-hrs-header">
	<div class="services-header-bg"></div>
	<h1 class="page_house_tours_new">Home Extension Services</h1>
	
</section>
</div>
<div class="mainouterwrapper">
  <div class="mainpadding">
    <div class="howcanwehelp">
      <div class="howcaninner">
        <div class="sectiontitle">
          <h1 class="mainnewtitle">How Can We Help?</h1>
        </div>
        <div class="sectiondesc">
          <p>We offer a variety of solutions for you to choose from, because every project is unique and we understand that our clients
would like different levels of involvement from our team. Whether you are looking for design, build, design & build or
anything else in-between, we are here to help.</p>
        </div>
        <div class="howcangrid">
          <div class="inlinegrid">
            <div class="innerinlinegrid">
              
              <div class="gridimage"> <img src="images_new_design/icon_map.png" alt=""> </div>
              <div class="gridtitle">
                <h2><a href="https://buildteam.com/architectural-design-phase.html">Design Phase</a></h2>
              </div>
              </div>
              <div class="verticletext">
                <p><span>Design and Planning</span></p>
                <p><span>Building Reg. Drawings</span></p>
                <p><span>Detailed Schedule of Works </span></p>
                <p><span>Interior & Landscape Design (optional bolt-on)</span></p>
                <div class="clearfix"></div>
              </div>
            
          </div>
          <div class="inlinegrid">
            <div class="innerinlinegrid">
              
              <div class="gridimage"> <img src="images_new_design/icon_hat.png" alt=""> </div>
              <div class="gridtitle">
                <h2><a href="https://www.buildteam.com/the_build_phase.html">Build Phase</a></h2>
              </div>
              </div>
              <div class="verticletext">
                <p><span>Full Build Service</span></p>
                <p><span>Project Management</span></p>
                <p><span>Payment Stage Certification</span></p>
                <p><span>10 Year Structural Guarantee</span></p>
                <div class="clearfix"></div>
              </div>
            
          </div>
          <div class="inlinegrid" style="border-bottom:none;">
            <div class="innerinlinegrid" style="border-right:none;">
              
              <div class="gridimage"> <img src="images_new_design/icon_house.png" alt=""> </div>
              <div class="gridtitle">
                <h2><a href="https://www.buildteam.com/online_quote_calculator.html">Additional Services</a></h2>
              </div>
              </div>
              <div class="verticletext">
                <p><span>Party Wall Service</span></p>
                <p><span>Approved Inspector</span></p>
                <p><span>Thames Water</span></p>
                <p><span>Fast Track Service</span></p>
                <div class="clearfix"></div>
              </div>
            
          </div>
          <div id="expandit"><img src="images_new_design/desktop_blue_arrow_down.png"></div>
        </div>
        <div class="howcangrid nobgdrid">
          <div class="inlinegrid">
            <div class="moreinforarrow"> <img src="images/newhome/more_arrow.png" alt=""> </div>
          </div>
          <div class="inlinegrid">
            <div class="moreinforarrow"> <img src="images/newhome/more_arrow.png" alt=""> </div>
          </div>
          <div class="inlinegrid">
            <div class="moreinforarrow"> <img src="images/newhome/more_arrow.png" alt=""> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>


<div class="home-gallery">
  <div class="home-gallery-text">
	<div class="home-gallery-h2 mobile-title" style="">
  		<h2>Our Fixed-Fee<br>
		  Design Packages</h2>
	</div>
<div class="services-p">
  <p>We offer a variety of Design Packages which can be adapted to
suit you, simple select your project and start to build your bundle.</p>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:30px">
				
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2 space-site">
				
</div>
<div class="col-xs-8 col-sm-8 col-md-2 col-lg-2" style="border:2px solid #183e70;padding:25px;margin-right:30px">
	<h3 class="our-site-title">Ground
Floor Ext.</h3>
	<div class="event-button">
			<a href="#ground">Find out more</a>
			<div class="event-button-arrow"></div>
	</div>
</div>
<div class="col-xs-8 col-sm-8 col-md-2 col-lg-2" style="border:2px solid #183e70;padding:25px;margin-right:30px">
	<h3 class="our-site-title">Loft
Conversion.</h3>
	<div class="event-button">
			<a href="#loft">Find out more</a>
			<div class="event-button-arrow"></div>
	</div>
</div>
<div class="col-xs-8 col-sm-8 col-md-2 col-lg-2" style="border:2px solid #183e70;padding:25px">
	<h3 class="our-site-title">Ground +
Loft Conv.</h3>
	<div class="event-button">
			<a href="#ground-loft">Find out more</a>
			<div class="event-button-arrow"></div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:30px">
</div>

</div>
<div style="width:100%;max-width:1920px;background-color:#e6e6e7;">
</div>

<section class="upcoming-dates-bg" style="padding: 110px 0 125px 0;">
		<div class="upcoming-dates">
		    <div class="col_full">
            <h2>Why choose Build Team?</h2>
            <div class="title-border" style="margin-bottom: 35px;"></div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
            </div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 services-descr-first">
			<p class="services-descr">We pride ourselves in our flexible choices for
regular homeowners, thinking of starting to plan
and build their new extension.</p>
<p class="services-descr">If you have further questions, or would like to talk
directly with one of our members, please follow the
link below:</p>
<div class="project-button" style="margin-top:35px"><a href="https://www.buildteam.com/book-visit.html?type=standard">Enquire now
        <div class="project-button-arrow"></div></a>
        </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-5 col-lg-5 services-descr-last">
			<ul class="why-services-ul">
                <li>Founded in 2007 with 10+ years of experience in Design and Build</li>
                <li>900+ successful planning consents across London</li>
                <li>Two-Stage Process: no obligation to build with us</li>
                <li>Unlimited amendments prior to Planning Submission</li>
                <li>In-house design and project management expertise</li>
            </ul>
            </div>
			
		</div>
</section>

<div style="width:100%;max-width:1920px;background-color:#fff;">
<div id="ground" class="home-gallery flex-grid" style="margin-top: 50px;">
	<div class="ext-gal-h2">
  		<h2>Ground Floor<br>
		  Extension</h2>
	</div>
<div class="gf-ext-p">
  <span class="gf-price">FROM £2,995</span>
<div class="project-button" style="float:right"><a href="https://www.buildteam.com/book-visit.html?type=standard">Enquire now
        <div class="project-button-arrow"></div></a>
        </div>
</div>
    <img src="/images_new_design/services_ground_image.jpg" style="width:100%;margin-bottom:35px" class="order-img">
    <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6 list-first">
			<ul class="why-services-ul">
                <li>Full Measured Survey.</li>
                <li>Design Options and Planning Application.</li>
                
            </ul>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5 list-last">
			<ul class="why-services-ul">
                <li>Building Regulation Drawing and Schedule of Works.</li>
                <li>Structural Engineering.</li>
                
            </ul>
            </div>

</div>
</div>

<section class="upcoming-dates-bg" style="padding: 110px 0 125px 0;">
		<div class="upcoming-dates">
		    <div class="col_full">
            <h2>Our Two-Stage Process</h2>
            <div class="title-border" style="margin-bottom: 35px;"></div>
            <div class="col-xs-12 col-sm-12 col-md-1 col-lg-1">
            </div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 services-descr-first" style="order:1">
			<p class="services-descr">Clients who instruct Build Team also benefit from our
Two-Stage Process; meaning there is no obligation to
commit beyond the Design Phase and you are free to
tender your designs to other contractors if you wish.</p>

            </div>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
			<p class="services-descr">Regardless of how you decide to proceed at this stage,
you will automatically receive a fixed price tender from
Build Team for the construction phase of your project.</p>
            </div>
			
		</div>
</section>


<div style="width:100%;max-width:1920px;background-color:#fff;">
<div id="loft" class="home-gallery flex-grid" style="margin-top: 50px;">
	<div class="ext-gal-h2">
  		<h2>Loft<br>
		  Conversion</h2>
	</div>
<div class="gf-ext-p">
  <span class="gf-price">FROM £1,995</span>
<div class="project-button" style="float:right"><a href="https://www.buildteam.com/book-visit.html?type=standard">Enquire now
        <div class="project-button-arrow"></div></a>
        </div>
</div>
    <img src="/images_new_design/services_loft_image.jpg" style="width:100%;margin-bottom:35px" class="order-img">
    <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6 list-first">
			<ul class="why-services-ul">
                <li>Full Measured Survey.</li>
                <li>Design Options and Planning Application.</li>
                
            </ul>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5 list-last">
			<ul class="why-services-ul">
                <li>Building Regulation Drawing and Schedule of Works.</li>
                <li>Structural Engineering.</li>
                
            </ul>
            </div>

</div>
</div>

<div class="testimonialscontainer">
  <div class="mainpadding">
    <div class="testimonialswrap">
      <div class="sectiontitle">
        <h2 class="mainnewtitle-border">Client Testimonials</h2>

      </div>
      
    </div>
    
</div>
<section class="lazy slider" data-sizes="50vw">
<?php
    $i = 0;
    $rs = getRs("SELECT testimonial_id, quote, name, title, project_url, video_url, thumbnailImg,category FROM testimonials WHERE is_active = 1 AND is_enabled = 1 ORDER BY sort");
    $total = mysqli_num_rows($rs);
    $half = false;
    
    while ( ($row = mysqli_fetch_assoc($rs)) && ($i <= 4) ) {
      $i++;
      if($row['video_url'] != '' && $row['video_url'] != null) {
      }
      else {
    ?>
    <div>
    <div class="threetestimonials testbottom">
    <div class="nameclient"><?php echo $row['name']; ?><br>
      <span style="font-weight:300"><?php echo $row['title']; ?></span></div>
      <p>
        <?php echo $row['quote']; ?>
      </p>
      
    </div>
  </div>
  
    <?php 
      }
    } ?>
</section>
</div>

<div style="width:100%;max-width:1920px;background-color:#fff;">
<div id="ground-loft" class="home-gallery flex-grid" style="margin-top: 50px;">
	<div class="ext-gal-h2">
  		<h2>Ground + Loft<br>
		  Conversion</h2>
	</div>
<div class="gf-ext-p">
  <span class="gf-price">FROM £4,995</span>
<div class="project-button" style="float:right"><a href="https://www.buildteam.com/book-visit.html?type=standard">Enquire now
        <div class="project-button-arrow"></div></a>
        </div>
</div>
    <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6" style="padding-right:12px;padding-left:12px;order:4">
    <img src="/images_new_design/services_ground_loft_image-1.jpg" style="width:100%;margin-bottom:35px">
    </div>
    <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6" style="padding-left:12px;padding-right:0px;order:4">
    <img src="/images_new_design/services_ground_loft_image-2.jpg" style="width:100%;margin-bottom:35px">
    </div>
    <div class="col-xs-10 col-sm-10 col-md-6 col-lg-6 list-first">
			<ul class="why-services-ul">
                <li>Full Measured Survey.</li>
                <li>Design Options and Planning Application.</li>
                
            </ul>
            </div>
            <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5 list-last">
			<ul class="why-services-ul">
                <li>Building Regulation Drawing and Schedule of Works.</li>
                <li>Structural Engineering.</li>
                
            </ul>
            </div>

</div>
</div>


<div style="width:100%;max-width:1920px;background-color:#e6e6e7;height:140px;">
</div>

<?php
        require_once('explore-our-site.php');
    ?>

 <script>
 $('.video').parent().click(function () {
    if($(this).children(".video").get(0).paused){
        $(this).children(".video").get(0).play();
        $(this).children(".playpause").fadeOut();
    }else{
       $(this).children(".video").get(0).pause();
        $(this).children(".playpause").fadeIn();
    }
});
 </script>
 <script>
 $(".bookbutton a").click(function(){
	 var getarea = $(this).parents(".tour_dates").find(".details_box").children("span").html();
	 $("#bookform input[name=tour_sttending]").val(getarea);
	 $(".needmoreinfo").fadeIn(2000);
	 //alert(getarea);
	// $(window).scrollTop($('#bookform').offset().top);
	 $("html, body").animate({
        scrollTop: $('#bookform').offset().top - 100
    }, 1000);

	 });
 </script>
<script>
$('.carousel').carousel();
</script>
<?php require_once('./inc/footer.inc.php'); ?>