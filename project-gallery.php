<?php

require_once('./inc/util.inc.php');

define('hidePageMedia', true);

$project_code = '';

if (isset($_GET['code'])) $project_code = (string)$_GET['code'];

$sql = "SELECT c.project_category_code, c.project_category_name, i.filename, p.project_code, p.project_name FROM project_category c INNER JOIN (project_image i INNER JOIN project_new p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 AND p.project_code = '" . formatSql($project_code) . "' ORDER BY p.sort, p.project_id, i.sort, i.project_image_id";


//echo $sql.'<br/>';#debug

$rs = getRs($sql);

if (!$project_code || !mysqli_num_rows($rs)) {
  header('Location: index.php');
  exit;
}

//echo $project_code.' - '.mysqli_num_rows($rs).'<br/>';
//exit;#debug

$row = mysqli_fetch_assoc($rs);

$project_category_name = $row['project_category_name'];
$project_category_code = $row['project_category_code'];

$meta_title = $row['project_name'].' '.$project_category_name.' Project | BuildTeam';

$meta_keywords = 'Side Return Extension, London builders, office refurbishment London, property management London, london property management, hotel refurbishment London, building refurbishment London, builder London, London builders, builders, London development, london renovator, london revovating, london renovation, renovation london, london decorators, london decorator, london decorators, you rang builders, yourang, you rang london, real estate management london, kitchen installation london, bathroom installation london, commercial builders london, commercial builder london, londons best builders, london loft conversions, loft convert, kitchen extensions, kitchen extension ideas, side return extension, side extensions, side return kitchen extension, victorian side return extension, side return ideas, side return costs, design and build extension, loft conversions, kitchen design ideas, small kitchen design, house extensions, loft conversion ideas, new kitchen, house designs, interior designers, builders london, home design, loft conversions london, modern kitchens, loft conversion london, contemporary kitchens, design and build, home extensions, extension costs, loft conversion plans, basement conversion, house builders, architect your home, beautiful kitchens, architectural design, garage conversion cost, living room decorating ideas, design your own house, modern house designs, modern interior design, modern house plans, london kitchen, loft extensions, london loft conversions, mansard loft conversion, permitted development extensions, house designs uk, house decorating ideas, construction company london, loft room ideas, house building, side return kitchen extensions, Victorian terraced houses';

$meta_description = 'Build Team, a professional construction company in London, offers solutions with great architectural design to increase the space in your home. Visit our gallery for '.('Loft Conversions'==$project_category_name?'loft conversions, loft extensions and loft room ideas and learn more about modern house designs.':$project_category_name.' and learn more about modern house designs.');

$project_name = $row['project_name'];

mysqli_data_seek($rs, 0);

$a_project = array();
while ($row = mysqli_fetch_assoc($rs) ) {
  $a_project[] = $row;
}


require_once('./inc/header.inc.php');
?>

<div class="full" id="project-gallery">

<div id="bc"><a href="/">Home</a> &rsaquo; <a href="/project-gallery/projects-<?php echo $project_category_code ?>.html"><?php echo htmlentities($project_category_name) ?></a> &rsaquo; <b> <?php echo htmlentities($project_name) ?> </b></div>

<h1><?php echo htmlentities($project_name) ?></h1>
<!--
<?php echo "slider-1".$_REQUEST['slider']; ?>
-->
<?php

$i = 0;
$ret = '';

$cols = 3;


foreach ($a_project AS $row) {
    $i++;	
		
		$ret .= '<'.'div class="col_one_third'.((0==($i % $cols))?' col_last':'').'" style="text-align:center';
    
		$ret .= '"><a href="/projects/' . $row['filename'] . '" rel="shadowbox[project_' . $row['project_code'] . ']" title="' . htmlentities($row['project_name']) . '"><img src="'.'/projects/' . $row['filename'] . '" style="width:100%;max-width:210px" alt="' . htmlentities($row['project_name']) . '" /></a></div>'; // debug: ' '.$project_count
    
}

echo $ret;

?>

<br clear="all"/>
<p style="text-align:justify">
<strong><?php echo $project_category_name ?> - a great way to create more space for your house building</strong><br/>
If you have already browsed our website, you probably already know by now that we specialise in side return kitchen extensions and loft conversions. This page is dedicated to the project named &quot;<?php echo htmlentities($project_name) ?> (<?php echo $project_category_name ?>)&quot;. The images speak for themselves and it is not just us who thinks we did a great job - the owners are more than happy to recommend our architectural design as well. As a professional construction company in London, Build Team is happy to offer you London loft conversion solutions and to help you architect your home or find out what is needed for permitted development extensions. Our loft room ideas can change based on what you envisage as ideal modern house designs. Whether it is loft extensions or loft conversions that you are interested in, you can be sure we will do a good job. Just have a look at our London loft conversions in the following photographs. 
</p>
  
</div>

<?php

require_once('./inc/footer.inc.php');

?>