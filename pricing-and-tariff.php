<?php



require_once('./inc/header.inc.php');



?>

  	<div class="left">

    	<div id="bc"><a href="/" title="Home">Home</a> &rsaquo; <a href="/what-we-do.html" title="What We Do">What We Do</a> &rsaquo; <b>Pricing &amp; Tariff</b></div>

    	<h1>Pricing &amp; Tariff</h1>

      <p>

      <table class="t_pricing">

      	<tr>

        	<th></th>

        	<th>Mon - Fri<br />7am - 6pm</th>

          <th>Mon - Fri<br />6pm - 12 Midnight</th>

          <th>Saturday<br />7am - 12am</th>

          <th>Sunday<br />7am - 12am</th>

        </tr>

        <tr>

        	<th>General Trades</th>

          <td>&pound;80</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

        </tr>

        <tr>

        	<th>Roofing Repairs</th>

          <td>&pound;80</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

        </tr>

        <tr>

        	<th>General Plumbing Repairs</th>

          <td>&pound;80</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

        </tr>

        <tr>

        	<th>General Electrical Repairs</th>

          <td>&pound;80</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

        </tr>

        <tr>

        	<th>Gas Safe Engineers</th>

          <td>&pound;90</td>

          <td>&pound;130</td>

          <td>&pound;130</td>

          <td>&pound;130</td>

        </tr>

        <tr>

        	<th>NICEIC Certified Electrician</th>

          <td>&pound;90</td>

          <td>&pound;130</td>

          <td>&pound;130</td>

          <td>&pound;130</td>

        </tr>

        <tr>

        	<th>Gas Safety Cert (Gas Safe)</th>

          <td>&pound;120</td>

          <td>&pound;150</td>

          <td>&pound;150</td>

          <td>&pound;150</td>

        </tr>

      </table>

      </p>

<p></p>
<h3>Minimum Charge</h3>
<p>For General Trades, we charge a minimum of 1.5 hours per visit. For all other trades, we charge a minimum of 1 hour per visit. Our time thereafter is chargeable at 15 minute increments.</p>
<p></p>
<h3>No extras</h3>
<p>The client will not be charged for our time travelling to or away from their job, there will also be no
charge for parking and congestion charge.</p>
<p></p>
<h3>Materials</h3>
<p>All materials which are supplied by Build Team will be charged as cost plus a 20% handling fee. Collection time (if required) will be kept to a minimum and will not exceed 45 minutes on any one booking.</p>
<p></p>
<h3>Cancellation Policy</h3>
<p>Please let us know in advance of the scheduled appointment time if you wish to cancel the booking. In the event that a booking is cancelled less than 3 hours before the scheduled appointment time, the minimum charge will apply.</p>
<p>You can view a copy of our standard <a href="/about-us/guarantees-and-insurance.html">Terms &amp; Conditions here</a>.</p>


		</div>

    <div class="right">

    	<img src="/images/pricingabacus.jpg" width="326" height="436" alt="BuildTeam" />

    </div>

<?php



require_once('./inc/footer.inc.php');



?>