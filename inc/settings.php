<?php

$path = dirname(__FILE__);
$path = str_replace('\\', '/', $path);

$path = explode('/', $path);
array_pop($path); // inc
$path = implode('/', $path) . '/';

define('BT_PATH', $path);

global $a_settings, $dbname;

require_once(BT_PATH . 'inc/util.inc.php');

define('WISE_DB_NAME', $dbname);

$key_field = 'id';
$value_field = 'value';

$sql = "SELECT $key_field AS `name`, `$value_field` AS `value` FROM btcrm_settings";
$res = getRs($sql);
while ($row = mysqli_fetch_assoc($res)) {
    $a_settings[ $row['name'] ] = $row['value'];
}

// DB functions:

function q($s)
{
    /*global $link_id;
    if (!$link_id) connectDB();*/
    return mysql_real_escape_string((string)$s);
}

function query($sql, $link_id = false)
{
    global $link_id;
    static $utf8fix;
  
    //if (!$link_id) connectDB();
  
    if (!isset($utf8fix)) {
        // UTF-8 fix
        $sql0 = "SET NAMES 'utf8'";
        //echo $sql0.'<br/>';
        if ($link_id) {
            $res = @mysqli_query($dbconn,$sql0, $link_id);
        } else {
            $res = @mysqli_query($dbconn,$sql0);
        }
        $utf8fix = true;
    }
  
    //echo $sql.'<br/>';
    if ($link_id) {
        $res = @mysqli_query($dbconn,$sql, $link_id);
    } else {
        $res = @mysqli_query($dbconn,$sql);
    }
    return $res;
}

function SQL2Array($sql, &$A_RES, $clear_res=true)
{
    global $smarty, $a_sql_c;
  
    $cache = false;
    /*
    /*
    //echo $sql.'<br/>';
    foreach ($a_sql_c AS $k => $v) if ($sql==$v) {
      $cache = true;
      $filename = WISE_SMARTY.'templates_c/sql_cache_'.$k;
      if (file_exists($filename)) {
        $fp = fopen($filename, 'r');
        $A_RES = unserialize(fread($fp, filesize($filename)));
        fclose($fp);
        return true;
      }
      break;
    }
    */
  
    if (!($res=query($sql))) {
        $A_RES = false;
        return;
    }
    $A_RES = array();
    while ($v = mysqli_fetch_assoc($res)) {
        $A_RES[] = $v;
    }
    if ($clear_res) {
        mysql_free_result($res);
    }
  
    if ($cache) {
        $fp = fopen($filename, 'w');
        fwrite($fp, serialize($A_RES));
        fclose($fp);
    }
  
    return $res;
}

// update log, used coupon, thank you session, send email(s)
function placeVisit($visit, $Message)
{
    global $a_settings;
  
    $paid = ($visit['amount']==0)?1:0;
  
    if (!$visit['log']) {
        $sql = "UPDATE visit SET log='".q($Message)."', date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=$paid WHERE visit_id=".(int)$visit['visit_id'];
    } else {
        $sql = "UPDATE visit SET log=CONCAT(log, '".q($Message)."'), date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=$paid WHERE visit_id=".(int)$visit['visit_id'];
    }
    query($sql);
  
    // update used coupon code, if exists
    if ($visit['coupon_code']) {
        $sql = "UPDATE coupon SET used=used+1 WHERE coupon_code='".q($visit['coupon_code'])."'";
        query($sql);
    }
  
    // clear book_visit session and store thank_you session
  
    $_SESSION[WISE_DB_NAME]['thank_you']['visit_id'] = $_SESSION[WISE_DB_NAME]['book_visit']['visit_id'];
    $_SESSION[WISE_DB_NAME]['thank_you']['full_name'] = substr($_SESSION[WISE_DB_NAME]['book_visit']['fname'], 0, 150);
    $_SESSION[WISE_DB_NAME]['thank_you']['availibility'] = $visit['availibility'];
    $_SESSION[WISE_DB_NAME]['thank_you']['postcode'] = $_SESSION[WISE_DB_NAME]['book_visit']['postcode'];
    $_SESSION[WISE_DB_NAME]['thank_you']['product_code'] = $_SESSION[WISE_DB_NAME]['book_visit']['product_code'];
  
    $_SESSION[WISE_DB_NAME]['thank_you']['price'] = $visit['amount'];
  
    // disable re-solicitation email if needed:
    if (isset($_SESSION[WISE_DB_NAME]['qid']) && $_SESSION[WISE_DB_NAME]['qid']) {
        $sql = "INSERT INTO btcrm_auto_email SET rfq_id=".(int)$_SESSION[WISE_DB_NAME]['qid'].(isset($_SESSION['userid'])?', userid='.(int)$_SESSION['userid']:'').", repeat_ts=0, log='0 ".time()."', switch_off=1";
    
        query($sql);
    
        unset($_SESSION[WISE_DB_NAME]['qid']);
    }
  
    unset($_SESSION[WISE_DB_NAME]['book_visit']);
  
    // send email
  
    $subj = $a_settings['confirm_subj'];
    $msg = $a_settings['confirm_email'];
  
    $is_html = false;
    if (is_int(strpos($msg, '<p>'))) {
        $is_html = true;
    }
  
    $msg = str_replace('[full name]', $visit['full_name'], $msg);
    $msg = str_replace('[comments]', $visit['comments'], $msg);
  
    if ($is_html) {
        $msg = str_replace('[availibility]', nl2br($visit['availibility']), $msg);
    } else {
        $msg = str_replace('[availibility]', $visit['availibility'], $msg);
    }
  
    $msg = str_replace('[postcode]', $visit['postcode'], $msg);
    if ($visit['amount']) {
        $msg = str_replace('[price]', '&pound;'.$visit['amount'].' (VAT included)', $msg);
    } else {
        $msg = str_replace('[price]', 'FREE', $msg);
    }
    $msg = str_replace('[ref number]', str_pad($visit['visit_id'], 5, '0', STR_PAD_LEFT), $msg);
    $msg = str_replace('[visit type]', $visit['visit_type'], $msg);
  
  
    SendSysMail($visit['email'], $subj, $msg);
    // copy to admin
    SendSysMail($a_settings['admin_email'], $subj, $msg);
}

// Send Email
function SendSysMail($to, $subj, $body)
{
    //global $smarty;
  
    include_once BT_PATH.'phpmailer/PHPMailerAutoload.php';
  
    $mail = new PHPMailer();
  
    $mail->From = 'hello@buildteam.com';//.$_SERVER['HTTP_HOST'];
  $mail->FromName = 'Build Team'; // $_SERVER['HTTP_HOST']
  $to = explode(',', $to);
    foreach ($to as $email) {
        $email = trim($email);
        if (!$email) {
            continue;
        }
        $mail->AddAddress($email);
    }
    $to = implode('-', $to);

    $mail->AddReplyTo($mail->From);
  
    $mail->Subject = $subj;
    $mail->Body = $body;
    $mail->CharSet = 'utf-8';
  
    $is_html = false;
    if (is_int(strpos($mail->Body, '<p>'))) {
        $is_html = true;
    }
  
    if ($is_html) {
        $mail->IsHTML(true);
        $mail->Body = '<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset='.$mail->CharSet.'" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>'.$mail->Subject.'</title></head>
  <body style="font-family:Arial,sans-serif">' .
    $mail->Body .
'  </body>
</html>';
    }
  
    if (!$to) {
        return false;
    } // if user hasn't email
  
    $ok = $mail->Send();
  
    $debug = true;
    //if (!$ok) echo $mail->ErrorInfo.'!!!';#debug
  
    if ('localhost' == $_SERVER['HTTP_HOST'] || $debug) {
        $to = str_replace('@', '_at_', $to);
        $fp = @fopen(BT_PATH.'phpmailer/'.($ok?'sent':'not_sent').'_'.$to.'_'.date('Y-m-d_H-i-s').'.'.($is_html?'html':'txt'), 'w');
        if ($fp) {
            @fwrite($fp, (!$is_html?$mail->Subject."\n\n":'').$mail->Body);
            @fclose($fp);
        }
    }
  
    $mail->ClearAddresses();
  
    return $ok;
}

// default values
$a_settings['a_visit_types'] = array(
  'standard' => array('name' => 'Standard', 'price' => '50', 'price_vat' => '60'),
  'premium' => array('name' => 'Premium', 'price' => '125', 'price_vat' => '150'),
  'concept' => array('name' => 'Concept', 'price' => '200', 'price_vat' => '200'),
  '3dconcept' => array('name' => '3D Concept', 'price' => '250', 'price_vat' => '300'), // 3dpremium
  'prepurchase' => array('name' => 'Pre-Purchase', 'price' => '125', 'price_vat' => '150'),
);

if (isset($a_settings['standard_price'])) {
    $a_settings['a_visit_types']['standard']['price'] = $a_settings['standard_price'];
}
if (isset($a_settings['standard_price_vat'])) {
    $a_settings['a_visit_types']['standard']['price_vat'] = $a_settings['standard_price_vat'];
}

if (isset($a_settings['premium_price'])) {
    $a_settings['a_visit_types']['premium']['price'] = $a_settings['premium_price'];
}
if (isset($a_settings['premium_price_vat'])) {
    $a_settings['a_visit_types']['premium']['price_vat'] = $a_settings['premium_price_vat'];
}

if (isset($a_settings['concept_price'])) {
    $a_settings['a_visit_types']['concept']['price'] = $a_settings['concept_price'];
}
if (isset($a_settings['concept_price_vat'])) {
    $a_settings['a_visit_types']['concept']['price_vat'] = $a_settings['concept_price_vat'];
}

if (isset($a_settings['3dpremium_price'])) {
    $a_settings['a_visit_types']['3dconcept']['price'] = $a_settings['3dpremium_price'];
}
if (isset($a_settings['3dpremium_price_vat'])) {
    $a_settings['a_visit_types']['3dconcept']['price_vat'] = $a_settings['3dpremium_price_vat'];
}

if (isset($a_settings['prepurchase_price'])) {
    $a_settings['a_visit_types']['prepurchase']['price'] = $a_settings['prepurchase_price'];
}
if (isset($a_settings['prepurchase_price_vat'])) {
    $a_settings['a_visit_types']['prepurchase']['price_vat'] = $a_settings['prepurchase_price_vat'];
}

//echo '<pre>'.print_r($a_settings, 1).'</pre>';#debug
