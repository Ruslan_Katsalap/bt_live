<?php
$book_styles = false;
if(isset($page_name)){
  if($page_name == "dc_booking" || $page_name = "pp_booking"){
    $book_styles = true;
  } 
}


ob_start();
if (strstr($_SERVER["SCRIPT_NAME"], '_'))
{
  header($_SERVER['SERVER_PROTOCOL'].' 301 Moved Permanently');
///header('Status: 404 Not Found'); 
  $redir = str_replace('_', '-', $_SERVER["SCRIPT_NAME"]);
  header('Location: ' . $redir); 
  exit;
}

require_once('./inc/util.inc.php');

$bc_trail = '';
$curr_url = getCurrentUrl();
$parent_url = getParentUrl();

//require_once('save-request-call.php');

if ( strlen($bc_trail) > 0 ) {
  $bc_trail = '<div id="bc">' . $bc_trail . '</div>';
}


if ( !isset($meta_title) ) {
  $meta_title = "Kitchen Extensions, Side Return Extensions, House Extensions and Loft Conversions in London";
//echo "SELECT page_name AS title, meta_title, meta_description, meta_keywords, page_content, hide_page_media FROM page WHERE page_code = '{$curr_url}'";
  $rs = getRs("SELECT page_name AS title, meta_title, meta_description, meta_keywords, page_content, hide_page_media FROM page WHERE page_code = '{$curr_url}'");
/*
Why some links are not working?
Here is a list:
//www.buildteam.com/project-gallery/gallery.html
//www.buildteam.com/extension-services.php

I see the 404 error.
*/
if (!mysqli_num_rows($rs)) {
/*$a_curr_url = explode('-', $curr_url);
$a_curr_url = array_pop($a_curr_url);
if ('page' == $a_curr_url) */

  header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
///header('Status: 404 Not Found');
  if(!$book_styles) {
      header('Location: /404.php');
      exit;
  }

}
  if ( $row = mysqli_fetch_assoc($rs) ) {

    if (!isset($title) || !$title) $title = $row['title'];
    if (!isset($page_content) || !$page_content) $page_content = $row['page_content'];
    if ($row['hide_page_media']) define('hidePageMedia', 1);

    if ( strlen($row['meta_title']) > 0 ) {
      $meta_title = $row['meta_title'];
    }
    if ( strlen($row['meta_description']) > 0 ) {
      $meta_description = $row['meta_description'];
    }
    if ( strlen($row['meta_keywords']) > 0 ) {
      $meta_keywords = $row['meta_keywords'];
    }
  }


}
else {
//$meta_title = "BuildTeam - {$meta_title}";
}
if($curr_url =='index')
  $meta_title = "Kitchen extensions, house extensions and loft conversions in London";

if ( !isset($meta_description) ) {
  $meta_description = "Build Team London's Side Return Extension Company (Kitchen Extensions and Loft Conversions)";
}

$rs = getRs("SELECT * FROM setting WHERE setting_id = 1");
$row = mysqli_fetch_assoc($rs);
$banner_fee = 3995;
$banner_date = '31.7.12';
$carousel_images = array();
if (isset($row['setting_id'])) {
  $banner_fee = (float)$row['design_cost'] - (float)$row['design_discount'];
  $banner_date = $row['offer_exp_days'];
  $carousel_images = unserialize($row['carousel_images']);
  if (!$carousel_images) $carousel_images = array();
}
$front_slogan =  mysqli_fetch_assoc(getRs("SELECT name FROM front_slogan WHERE id = 1"));

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN">
<html lang="en"><head>
<!-- site header begins -->
<!-- <?=$curr_url;?> -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
  new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KG9JT5C');</script>
<!-- End Google Tag Manager -->
<meta charset="UTF-8">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="format-detection" content="telephone=no">
<meta name="p:domain_verify" content="c52cba0ac966191692e7f5ddd109ba78" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
<title><?php echo $meta_title; ?> | BuildTeam London</title>
<meta name="description" content="<?php echo $meta_description; ?>" />
<link rel="shortcut icon" href="/images/favicon.ico" type="image/x-icon">
<link rel="icon" href="/images/favicon.ico" type="image/x-icon">




<?php if ($_SERVER['PHP_SELF'] == '/book-visit.php' || $_SERVER['PHP_SELF'] == '/book-confirm.php' || $_SERVER['PHP_SELF'] == '/initial-visit.php'  ||  $_SERVER['PHP_SELF'] == '/contact-us.php') { ?>
<?php } ?>
<?php if ($_SERVER['PHP_SELF'] == '/initial-visit.php' || $_SERVER['PHP_SELF'] == '/about-us/our-story-page.html' || $_SERVER['PHP_SELF'] == '/thank-you-for-submit.php') { ?>
<meta name="robots" content="noindex">
<?php } ?>
  
<?php if ($curr_url == 'newbyp-quote') { ?>  
<!--link href="//fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Tinos:400,700" rel="stylesheet"-->
<?php } ?>
<link href="/css/jquery.bxslider.css" rel="stylesheet" />
<?php if ($curr_url != 'blog') { ?>  
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
<?php } ?>

<!-- styles for booking -->
<?php if($book_styles){ ?>
 
  <!-- <script src="/assets_b/js/script.js"></script> -->
 
<?php } ?>


<script  type="text/javascript" src="/js/jquery.bxslider.js"></script>
<link rel="stylesheet" type="text/css" href="/css/reset.css" />
<?php if ($curr_url != 'newbyp-quote' && $curr_url != 'index') { ?> 
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<?php } else { ?>
<link rel="stylesheet" type="text/css" href="/css/site.css" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="/css/new_design.css" />
<link rel="stylesheet" type="text/css" href="/css/responsive.css" />
<link rel="stylesheet" type="text/css" href="/css/default.css" />
<?php if ($curr_url != 'newbyp-quote' && $curr_url != 'index') { ?> 
<link rel="stylesheet" type="text/css" href="/css/site.css" />
<?php } ?>

<link rel="stylesheet" type="text/css" href="/css/bootstrap.css" />

<link rel="stylesheet" type="text/css" href="/css/component.css" />

<link rel="stylesheet" href="/css/home2018.css">
<link rel="stylesheet" href="/css/new_style.css">
<link rel="stylesheet" href="/css/home_page.css">
<?php if ($curr_url != 'blog') { ?>  
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<?php } ?>
<link rel="stylesheet" type="text/css" href="/css/horizon-swiper.min.css" />
<?php if ($curr_url == 'newbyp-quote' || $curr_url == 'index') { ?> 
<link rel="stylesheet" type="text/css" href="/css/style.css" />
<?php } ?>

<link href="//fonts.googleapis.com/css2?family=Mulish:ital,wght@0,300;0,400;0,700;1,300;1,400;1,700&display=swap" rel="stylesheet">
<?php if ($curr_url == 'newbyp-quote') { ?> 
<!--link href='//fonts.googleapis.com/css?family=Arimo' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'-->
<?php } ?>
  <link href="/mobilemenu/menustyle.css" rel="stylesheet">
  <?php //if (!strpos($_SERVER['REQUEST_URI'], 'design-bundles')): ?>
  <script type="text/javascript" src="/js/bootstrap.min.js"></script>
  <?php //endif; ?>
  <?php if ($curr_url != 'blog') { ?>  
  <!--script src="/js/jquery-ui.js"></script-->
  <script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js" integrity="sha512-JGKswjfABjJjtSUnz+y8XUBjBlwM1UHNlm2ZJN7A2a9HUYT3Mskq+SacsI35k4lok+/zetSxhZjKS3r3tfAnQg==" crossorigin="anonymous"></script>
  <?php } ?>

  <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.min.js"></script>
  <script type="text/javascript" src="/js/jquery.easing.1.3.js"></script>
  <script type="text/javascript" src="/shadowbox/shadowbox.js?v=6"></script>

  <?php if (strpos($_SERVER['REQUEST_URI'], 'build-your-price-app')): ?>

  <?php endif; ?>
  <script language="javascript" type="text/javascript" src="/js/default.js"></script>
  <?php if ($curr_url == 'newbyp-quote') { ?> 
  <!--script src="/js/lazysizes.min.js" async></script-->
  <?php } ?>
  <?php if (strpos($_SERVER['REQUEST_URI'], 'byp_latest') || strpos($_SERVER['REQUEST_URI'], 'saved_quote') || strpos($_SERVER['REQUEST_URI'], 'byp-latest') || strpos($_SERVER['REQUEST_URI'], 'saved-quote')): ?>
    <script type="text/javascript" src="/js/jscript_numberformat.js"></script>
    <script type="text/javascript" src="/js/jquery.form.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/byp.css?v=78" />
    <script type="text/javascript" src="/js/byp_lightbox.js?v=2"></script>
    <script type="text/javascript" src="/js/jquery.newsticker.js"></script>

  <?php endif; ?>



  <?php if (strpos($_SERVER['REQUEST_URI'], 'build-your-price')): ?>
    <script type="text/javascript" src="/js/jscript_numberformat.js"></script>
    <script type="text/javascript" src="/js/jquery.form.js"></script>
    <script type="text/javascript" src="/js/byp_lightbox.js?v=2"></script>
    <script type="text/javascript" src="/js/jquery.newsticker.js"></script>
  <?php endif; ?>
  <script>

</script>
<script type="text/javascript">
if (window.addEventListener) { // W3C standard
window.addEventListener('load', initResizeWindow, false); // NB **not** 'onload'
window.addEventListener('resize', initResizeWindow, false);
}
else if (window.attachEvent) { // Microsoft
  window.attachEvent('onload', initResizeWindow);
  window.attachEvent('onresize', initResizeWindow);
}

var js_load = false;
var mq = {};

function initResizeWindow() {
  if (document.getElementById("main")) var curr_width = document.getElementById("main").offsetWidth;

var a_map = [];

a_map['932_480'] = '<area shape="rect" coords="20,14,448,177" href="/build-your-price-app.html" alt="byp_online"  />'+
'<area shape="rect" coords="19,194,326,219" href="//itunes.apple.com/en/app/build-team-price-calculator/id561678740" alt="byp_download" target="_blank" />'+
'<area shape="rect" coords="335,181,437,218" href="//itunes.apple.com/en/app/build-team-price-calculator/id561678740" alt="byp_app" target="_blank" />';

a_map['768'] = '<area shape="rect" coords="17,13,369,146" href="/build-your-price-app.html" alt="byp_online" />'+
'<area shape="rect" coords="15,158,267,181" href="//itunes.apple.com/en/app/build-team-price-calculator/id561678740" alt="byp_download" target="_blank" />'+
'<area shape="rect" coords="275,149,357,178" href="//itunes.apple.com/en/app/build-team-price-calculator/id561678740" alt="byp_app" target="_blank" />';

a_map['320'] = '<area shape="rect" coords="14,11,310,123" href="/build-your-price-app.html" alt="byp_online" alt="byp_online" />'+
'<area shape="rect" coords="13,132,224,150" href="//itunes.apple.com/en/app/build-team-price-calculator/id561678740" alt="byp_download" target="_blank" />'+
'<area shape="rect" coords="231,126,299,149" href="//itunes.apple.com/en/app/build-team-price-calculator/id561678740" alt="byp_app" target="_blank" />';

a_map['book_visit932'] = '<area shape="rect" coords="432,222,525,240" href="/premium_site_visit.html" />'+
'<area shape="rect" coords="781,128,871,147" href="/the_design_phase.html" />';

a_map['book_visit768'] = '<area shape="rect" coords="349,108,478,210" href="/premium_site_visit.html" />'+
'<area shape="rect" coords="642,61,766,135" href="/the_design_phase.html" />';

jQuery("#byp_link").empty();
if (document.getElementById("img_book_visit")) {
  var book_visit = true;
  jQuery("#linkmap").empty();
}
else book_visit = false;

if (document.getElementById("b_marquee")) {
  var marquee = document.getElementById("b_marquee").innerHTML;
  jQuery(".bottom-bar div").remove();
}
else var marquee = false;

if (curr_width >= 932) global_mob_screen = false;
else global_mob_screen = true;

if (932 == curr_width || 480 == curr_width) {
  jQuery("#byp_link").append(a_map['932_480']);
  if (marquee) {
    if (932 == curr_width) jQuery(".bottom-bar").append('<marquee behavior="scroll" scrollamount="1" direction="left" width="750">'+marquee+'</marquee>');
    else jQuery(".bottom-bar").append('<marquee behavior="scroll" scrollamount="1" direction="left" width="340">'+marquee+'</marquee>');
  }
  if (book_visit) {
    if (932 == curr_width) {
      jQuery("#linkmap").append(a_map['book_visit932']);
      document.getElementById("img_book_visit").src = "/images/book-site-visit.png";
    }
  }

  if (latest_quotes) {
    jQuery("#latest_quotes_container").append("<ul id=\"latest-quotes-slide\">" + document.getElementById("latest_quotes_src").innerHTML + "</ul>");
    $('#latest-quotes-slide').totemticker({
row_height  :   '30px' // 50px
});
  }

}
if (740 == curr_width) {
  jQuery("#byp_link").append(a_map['768']);
  if (marquee) jQuery(".bottom-bar").append('<marquee behavior="scroll" scrollamount="1" direction="left" width="600">'+marquee+'</marquee>');

  if (book_visit) {
    jQuery("#linkmap").append(a_map['book_visit768']);
    document.getElementById("img_book_visit").src = "/images/book-site-visit768.png";
  }

  if (latest_quotes) {
    jQuery("#latest_quotes_container").append("<ul id=\"latest-quotes-slide\">" + document.getElementById("latest_quotes_src").innerHTML + "</ul>");
    $('#latest-quotes-slide').totemticker({
      row_height  :   '50px'
    });
  }

}
if (292 == curr_width) {
  jQuery("#byp_link").append(a_map['320']);
  if (marquee) jQuery(".bottom-bar").append('<marquee behavior="scroll" scrollamount="1" direction="left" width="180">'+marquee+'</marquee>');

  if (latest_quotes) {
    jQuery("#latest_quotes_container").append("<ul id=\"latest-quotes-slide\">" + document.getElementById("latest_quotes_src").innerHTML + "</ul>");
    $('#latest-quotes-slide').totemticker({
row_height  :   '30px' // 50px
});
  }

}

if (marquee) jQuery('marquee').marquee();
}

function gotoPage(url) {
  if (""==url) {
    document.getElementById("select_menu").selectedIndex = 0;
  }
  else {
    document.location = url;
  }
}

function initPage() {

  $(document).ready(function(){

    if (document.getElementById("main")) var curr_width = document.getElementById("main").offsetWidth;
    var gv_dot_width = 14;
    var gv_dot_height = 14;
    var gv_dot_gap = 14;
    if (292 == curr_width) {
      gv_dot_width = 7;
      gv_dot_height = 7;
    }

    if (document.getElementById("gallery_carousel")) {

      $('#gallery_carousel').galleryView({
        panel_width: $('#gallery_carousel').width(),
panel_height: $('#gallery_carousel').height(), //375,
transition_speed: 600,
transition_interval: 5000,
panel_animation: 'fade',
autoplay: true,
frame_width: gv_dot_width,
frame_height: gv_dot_height,
frame_gap: gv_dot_gap,
padding: 0,
show_filmstrip_nav: false,
show_infobar: false
});

    }

    if (document.getElementById("gallery_carousel_top")) {

      $('#gallery_carousel_top').galleryView({
        panel_width: $('#gallery_carousel_top').width(),
panel_height: $('#gallery_carousel_top').height(), //375,
transition_speed: 600,
transition_interval: 5000,
panel_animation: 'fade',
autoplay: true,
frame_width: gv_dot_width,
frame_height: gv_dot_height,
frame_gap: gv_dot_gap,
padding: 0,
show_filmstrip_nav: false,
show_infobar: false
});

    }

    if (document.getElementById("byp_slide_box")) {
      bypInit();
    }

    $('#sb-body-inner').click(function(){
      if (Shadowbox.current < (Shadowbox.gallery.length-1)) Shadowbox.next();
      else Shadowbox.change(0);
    });

    var slides = jQuery('#sb-body-inner');


    slides.on('swipeleft', function(e) {
      Shadowbox.next();
    })
    .on('swiperight', function(e) {
      Shadowbox.previous();
    });


    jQuery('#sb-body-inner').on('movestart', function(e) {
if ((e.distX > e.distY && e.distX < -e.distY) ||
  (e.distX < e.distY && e.distX > -e.distY)) {
  e.preventDefault();
}
});

    jQuery('#sb-nav-previous').mouseover(function() {
      $(this).css("opacity", 0.5);
    }).mouseout(function(){
      $(this).css("opacity", 0);
    });
    jQuery('#sb-nav-next').mouseover(function() {
      $(this).css("opacity", 0.5);
    }).mouseout(function(){
      $(this).css("opacity", 0);
    });

    jQuery(".lightblue-box").click(function() {
      document.location = $(this).find("a").attr("href");
    });

    jQuery(".byp-btn").click(function() {
      document.location = $(this).find("a").attr("href");
    });



    $("#mobmenu a.qmparent").each(function(index){
      var href = $(this).attr("href");
      if ("javascript:void(0)" != href) {
        $(this).parent().css("backgroundImage", "url(/images/bg_menu_right_parent.gif)");
      }
    });

    $("#mobmenu a.qmparent-on").each(function(index){
      var href = $(this).attr("href");
      if ("javascript:void(0)" == href) {
        $(this).parent().addClass("parent-on");
      }
      else {
        $(this).parent().css("backgroundColor", "white");
        $(this).parent().css("backgroundImage", "none");
      }
    });

    $("#mobmenu a.qmparent-on").click(function(){
      var href = $(this).attr("href");
      if ("javascript:void(0)" == href) {
        if (!$(this).parent().hasClass("open_on")) {
          $(this).parent().find("ul").css("display", "block");
          $(this).parent().addClass("open_on");
        }
        else {
          $(this).parent().removeClass("open_on");
          $(this).parent().find("ul").css("display", "none");
        }
      }
    });

    $("#mobmenu a.qmparent").click(function(){
      var href = $(this).attr("href");
      if ("javascript:void(0)" == href) {
        if (!$(this).parent().hasClass("open")) {
          $(this).parent().find("ul").css("display", "block");
          $(this).parent().addClass("open");
        }
        else {
          $(this).parent().removeClass("open");
          $(this).parent().find("ul").css("display", "none");
        }
      }
    });

  });

}
</script>


<?php
$rs = getRs("SELECT * FROM setting WHERE setting_id = 1");
$row = mysqli_fetch_assoc($rs);
$rsPage = getRs("SELECT * FROM page WHERE page_id = 80");
$rowPage = mysqli_fetch_assoc($rsPage);

$carousel_images_new = unserialize($row['carousel_images']);

$rsSetting = mysqli_query($dbconn,"SELECT * FROM btcrm_settings WHERE (id = 'selected_date' OR id = 'dateFrom' OR id = 'dateTo')");
while($rowSetting = mysqli_fetch_assoc($rsSetting)){
  if($rowSetting['id']=='selected_date')
    $dataArr = unserialize($rowSetting['value']);
  if($rowSetting['id']=='dateFrom')
    $dateFrom = $rowSetting['value'];
  if($rowSetting['id']=='dateTo')
    $dateTo = $rowSetting['value'];
}
$dateStr = '';
foreach($dataArr AS $dataArrDate){
  $comma = ',';
  if($dateStr == '')
    $comma = '';
  $dateStr .= $comma.'"'.date('j-n-Y', strtotime($dataArrDate)).'"';
}
?>
<script>
  $( function() {
    var dialog,
    dialog = $( ".dialog-request" ).dialog({
      autoOpen: false,
      height: 560,
      width: 450,
      modal: true,
      buttons: {
        Cancel: function() {
          dialog.dialog( "close" );
        }
      },
    });
    $( ".request-call" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });
    $( "#request-call2" ).button().on( "click", function() {
      dialog.dialog( "open" );
    });
  } );
</script>
<script>





  $( function() {
//var enableDays = ["2-3-2017", "3-3-2017"];
var enableDays = [<?=$dateStr?>];

function enableAllTheseDays(date) {
  var sdate = $.datepicker.formatDate( 'd-m-yy', date)
  if($.inArray(sdate, enableDays) != -1) {
    return [true];
  }
  else {
    return [false];
  }
}
$( ".datepicker1" ).datepicker({
  dateFormat: 'dd-mm-yy',
  beforeShowDay: enableAllTheseDays,
  onSelect: function(dateText, inst) {
    var date = $(this).datepicker('getDate'),
    day  = date.getDate(),
    day2  = date.getDate(),
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],

    year =  date.getFullYear();
    function getMonthShortName(month) {
      return months[month];
    }

//alert(day + '-' + months);
$('.date1').html(day);
//$('.date2').html(day2);
$('.month').html(getMonthShortName(date.getMonth()));
},

});
$( ".datepicker2" ).datepicker({
  dateFormat: 'dd-mm-yy',
  beforeShowDay: enableAllTheseDays,
  onSelect: function(dateText, inst) {
    var date = $(this).datepicker('getDate'),
    day  = date.getDate(),
    day2  = date.getDate(),
    months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],

    year =  date.getFullYear();
    function getMonthShortName(month) {
      return months[month];
    }

//alert(day + '-' + months);
//$('.date1').html(day);
$('.date2').html(day2);
$('.month').html(getMonthShortName(date.getMonth()));
},

});

});
</script>
<script  src="/js/jquery.event.move.js"></script>
<script  src="/js/jquery.event.swipe.js"></script>
<script  type="text/javascript" src="/js/jquery.newsticker.js"></script>
<script language="javascript" type="text/javascript">
  Shadowbox.init({    language: 'en',    players:  ['img','iframe']});
  $(document).ready(function() {
    $('img[alt="we collage"]').css('display', 'none');
    $("#quotes").newsTicker();
  });
</script>
<!-- <link rel="stylesheet" type="text/css" href="/css/site.css?v=15" /> -->
<style>
  footer .bott_menu ul:last-child
  {
    border-right:2px solid #fff;	
  }
  #popupform .broform100.checkboxbro.broleft span {
    display: inline-block;
    width: 93%;
    text-align: justify;
    vertical-align: top;
    font-size: 16px;
  }
  #popupform .broform100.checkboxbro.broleft input {
    height: 20px;
    display: inline-block;
    max-width: 5%;
  }
</style>
<?php if (strpos($_SERVER['REQUEST_URI'], 'build-your-price')): ?>

<?php endif; ?>
<?php if ('/'==$_SERVER['REQUEST_URI'] || 0===strpos($_SERVER['REQUEST_URI'], '/index.php') ) : ?>
<style type="text/css">
  .main {
    display:none;
  }

  #new_sales_menu {
    float: left;
    margin-top: -23px;
    text-align: right;
    width: 100%;
  }
  #new_sales_menu > a {
    background: #d61d13 none repeat scroll 0 0;
    color: #fff;
    margin-bottom: 6px;
    padding: 6px 15px;
  }
  #new_sales_menu > a:hover {
    text-decoration:none;
  }
  .january-sale {
    display:block;
    position:relative;
    bottom: -35px;
    width:100%;
  }
  .january-sale a {
    background: red;
    color: #FFF !important;
    padding: 10px 25px;
  }
  .salemobile {
    display:none;
  }

  .salered {
    color: #db0000 !important;

  }
  .saleabso {
  }

  .salesadded{
    width: 665px !important;
  }
  .salesadded .cbp-hrsub.aboutdropdown .foredge {
    width: 64% !important;
  }
  .salesadded .cbp-hrsub.designdropdown .foredge {
    width: 56.8% !important;
  }
  .salesadded .cbp-hrsub.gallerydropdown .foredge {
    width: 39.8% !important;
  }
  .salesadded .cbp-hrsub.builddropdown .foredge {

    width: 49% !important;
  }
  .salesadded .cbp-hrsub.newsdropdown .foredge {
    width: 37% !important;
    left: 105px;
    position: relative;
  }

  .jcarousel-container{position:relative;}
  .jcarousel-clip{z-index:2;padding:0;margin:0;overflow:hidden;position:relative;}
  .jcarousel-list{z-index:1;overflow:hidden;position:relative;top:0;left:0;margin:0;padding:0;}
  .jcarousel-list li,
  .jcarousel-item{float:left;list-style:none;width:75px;height:75px;}
  .jcarousel-next{z-index:3;display:none;}
  .jcarousel-prev{z-index:3;display:none;}
  @media (max-width:640px) {
    .january-sale {
      bottom:-10px !important;
    }
    .saledesktop {
      display:none;
    }
    .salemobile {
      display:block !important;
    }
  }
</style>

<style type="text/css">
  #fm_find_on_map {
    position:absolute;
    top:78%;
    right:1px;
  }

  #fm_find_on_map form {
    display:block;
    position:absolute;
    top:9px;
    left:281px;
  }

  #fm_find_on_map form input[type=text] {
    background-color:transparent;
    display:block;
    float:left;
    font-size:16px;
    border:none;
    width:107px;
  }

  #fm_find_on_map form input[type=submit] {
    background-color:transparent;
    border:none;
    color:white;
    cursor:pointer;
    display:block;
    float:left;
    font-size:16px;
    margin-left:8px;
    padding-top:0px;
    width:70px;
  }

  @media (max-width: 768px) {
    #fm_find_on_map {
      top:72%;
      right:0;
    }
  }

  @media (max-width:481px) {
    #fm_find_on_map {
      top:58%;
      overflow:hidden;
      width:292px;
    }

    #fm_find_on_map img {
      margin-left:-190px;
    }

    #fm_find_on_map form {
      left:92px;
    }

  }


</style>
<?php endif; ?>
<style>

  @media (min-width:641px) {
    .topwhite {
      display:none !important;
    }
    .desktopimage {
      display:block !important;
    }
    .mobileimage {
      display:none !important;
    }
    .flexbox.formobileonly {
      display:none;
    }
    .bott_menu ul .clickexpand {
      display: block !important;
      color: #FFF !important;
      padding: 0px 13px !important;
      font-size: 14px !important;
    }
    .bott_menu ul {
      height:215px !important;
      overflow:visible !important;
      width:19%;
    }

  }
  @media (max-width:640px) {
    body {
      width:100% !important;
      overflow-x:hidden !important;
    }
    a#closePageslide {
      position: relative;
      display: block;
      padding: 30px 0px;
      text-align:right;
    }
    a#closePageslide span {
      font-size: 30px;
      color: #ccc;
      z-index: 1000000;
      padding:27px 20px;
      position: relative;
      margin-top: 21px;
      background:#FFF;
    }
    #openPageslide span {
      background: #cccccc !important;
    }
    .small {
      background-size:cover !important;
    }
    .search {
      width:100% !important;
    }
    .search form p {
      font-size:12px !important;
    }
    .search form {
      width:92% !important;
      left:0 !important;
      right:0 !important;
    }
    .inlineform {
      display:inline-block !important;
      width:49% !important;
      vertical-align:middle;
    }
    .search form #field {
      width:70px !important;
    }
    .search {
      top:33% !important;
      height:65% !important;
    }
    .topwhite {
      width:100% !important;
      padding:25px 0px !important;
      background:rgba(255,255,255,0.9) !important;
      position:fixed !important;
      top:0px !important;
      left:0px !important;
      text-align:center !important;
    }
    .topwhite img {
      max-width:200px !important;
      margin:0 auto !important;
    }
    .middelimg img {
      opacity:1 !important;
      display: block !important;
    }
    #pageslide {
      padding-top:50px !important;
    }
    .navBox ul {
      border-top:1px solid #959595 !important;
      border-bottom:1px solid #959595 !important;
    }
    .navBox ul li ul {
      border:none !important;
    }
    .navBox>ul>li:last-child {
      border-bottom:none !important;
    }
    .up_down {
      z-index:2 !important;
    }
    .explore .seccond {
      width: 49% !important;
      display: inline-block !important;
      padding: 0px !important;
      float:left !important;
      margin:0px !important;
    }
    .explore .first {
      width: 49% !important;
      padding: 0px !important;
      display: inline-block !important;
      float:left !important;
      margin:0px !important;
    }

    .explore .first:after, .explore .seccond:after {
      content:"" !important;
      clear:both !important;
    }
    .explore {
      margin:70px auto 40px !important;
    }

    .explore .first p, .explore .seccond p, .explore .third p, .forth p {
      font-size:14px !important;
    }
    .explore .first span, .explore .seccond span, .explore .third span, .explore .forth span {
      font-size:16px !important;
    }
    .explore .first img {
      border-right:dotted;
      border-right-width:5px;
      border-image-slice: 33% 33%;
      border-image-repeat: round;
      border-image-source: url(../images/dots.svg);
      padding: 0px 9px;
    }
    .explore .seccond img  {
      padding: 0px 10px;
      position:relative;
      left:8px;
    }
    .explore .third img {
      /* position: relative; */
      max-height: 150px !important;
      width: 100% !important;
    }
    .explore .third {
      max-height: 210px;
      margin-top: 0px !important;
      overflow: hidden !important;
      margin-bottom: 20px !important;
    }
    .big_wrapp {
      display:none;
    }
    .big_wrapp img {
      height: 100px !important;
      width: 100% !important;
    }

    .medium_wrapper .meet h2 {
      font-size:22px !important;
    }
    .medium_wrapper .meet a {
      font-size:14px !important;
      margin:0px auto !important;
    }
    section.module.parallax-2 {
      height:auto !important;
      border-radius: 0 !important;
      background-attachment:scroll !important;
    }
    section.module.parallax-1 {
      height:auto !important;
      border-radius: 0 !important;
    }
    .extension h3 {
      font-size:18px !important;
      margin-bottom: 0px !important;
    }
    .extension p {
      font-size:14px !important;
      text-align:justify !important;
    }
    .background.container {
      height:auto !important;
    }

    footer .bott_menu ul {
      height: 25px;
      overflow: hidden;
      width: 100% !important;
      text-align: left !important;
      border:none !important;
    }
    footer ul li {
      padding:0px 5px !important;
    }
    footer .bott_menu ul a {
      color:#FFF !important;
      font-size:16px;
      position:relative;
      display:block;
    }
    .bott_menu ul a:before {
      position: absolute;
      top: 5px;
      right: 15px;
      content: "";
      width: 0;
      height: 0;
      border-left: 6px solid transparent;
      border-right: 6px solid transparent;
      border-top: 6px solid #fff;
    }
    .bott_menu ul li a:before {
      display:none !important;
    }
    .bott_menu ul {
      width:48% !important;
    }
    #mc_embed_signup {
      margin-bottom:30px !important;
    }
    section.module.parallax-1 {
      z-index:1 !important;
    }
    .module.container, .container.background {
      z-index:1 !important;
    }
    .parallax-2 .sec_wrapp {
      width:100% !important;
    }
    section.module.parallax-2 {
      z-index:1 !important;
    }
    .desktopimage {
      display:none !important;
    }
    .mobileimage {
      display:block !important;
    }
    .medium_wrapper .images img {
      width: 48% !important;
      margin: 0px !important;
      padding: 0px !important;
      float:none !important;
    }
    .medium_wrapper .images div {
      float: none !important;
    }

  }
  @media (min-width:640px) {
    .sitevisittab p {
      position: relative;
      left: 11px;
    }
    .dateholder p {
      left:inherit !important;
    }
    .dateholder {
      display: table;
      width: 105% !important;
      text-align: center;
      left: -4px;
      position: relative;
    }
  }
</style>
<style type="text/css">
  input.datepicker {
    opacity: 0;
    position: absolute;
    height: 100%;
    width: 100%;
    top: 0px;
    left: 0px;
  }
  .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
    border:none !important;
    background:none;
    text-align:center;
  }
  .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
    background:#374173;
  }

  .sitevisittab {
    position: fixed;
    top: 20%;
    padding: 20px 15px 5px 20px;
    background: rgba(255,255,255,0.9);
    border-radius: 10px 0px 0px 10px;
    height: 160px;
    overflow: hidden;
    z-index: 1000;
    right: 0;
    width: 170px;
  }
  .sitevisittab p img {
    max-width: 15px;
    vertical-align: middle;
    position: relative;
    margin-right: 7px;
    top: -4px;
  }

  .sitevisittab p {
    margin-bottom: 21px;
    margin-top: 0px;
    padding-top: 0px;
    cursor:pointer;
    color:#fff;
  }
  .sitevisittab .linktab a {
    display: inline-block;
    margin: 10px 0px;
    background: #2e386c;
    padding: 5px 0px !important;
    border-radius: 10px;
    color: #FFF;
    width: 90%;
    text-align: center;
  }
  .onlinecalc {
    position: fixed;
    bottom: 20%;
    padding: 15px 40px 15px 25px;
    background: rgba(255,255,255,0.9);
    border-radius: 0px 10px 10px 0px;
    height: 60px;
    overflow: hidden;
    z-index:1000;
    /* right: 0; */
  }
  @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
    /* IE10+ CSS styles go here */
    .onlinecalc {
      min-height:90px !important;
    }
    .sitevisittab .linktab a {
      padding:5px 22px !important;
    }
    .mobilewrap a {
      font-size:22px !important;
    }
    .sitevisittab p {
      font-size:16px !important;
    }
    .sitevisittab {
      min-height:180px;
    }
  }
  .onlinecalc p {
    width: 90px;
    margin-top: 0px;
    font-size: 32px;
    padding-top: 0px;
    color: #2e386c;
  }
  div#toggleswitch span {
    margin-top:20px !important;
  }
  .onlinecalc span {
    display: block;
    width: 100px;
    font-size: 18px;
    margin-top: 10px;
    color: #7b797e;
  }
  .onlinecalc span a {
    color: #f5641e;
    font-size: 20px !important;
    display: block;
    margin-top: 5px;
  }
  div#toggleswitch span {
    position: absolute;
    right: 10px;
    font-size: 32px;
    display: inline-block !important;
    cursor: pointer;
    text-align: right;
    color:#f5641e;

  }
  .onlinecalc a {
    font-size: 28px;
  }
  .parallax-window {
    min-height:400px !important;
  }
  .bx-wrapper .bx-viewport {
    background:transparent !important;
  }
  .dateholder {
    display: table;
    width: 100%;
    text-align: center;
  }
  .dateinline {
    display: table-cell;
    position: relative;
  }
  .fromdate p, .todate p {
    margin:0px;
    padding:0px;
  }
  .fromdate .date, .todate .date {
    font-size: 24px;
    margin: 0px;
    padding: 0px;
  }
  .mobilewrap a {
    display: block;
    text-align: center;
    padding-top: 10px;
    font-size: 24px;
  }
  .linktab {
    text-align: center;
  }
  .ordiv.dateinline:after {
    content: "";
    position: absolute;
    height: 15px;
    width: 2px;
    background: #7b797e;
    bottom: -5px;
    left: 22px;
  }
  .ordiv.dateinline:before {
    content: "";
    position: absolute;
    height: 15px;
    width: 2px;
    background: #7b797e;
    top: -5px;
    left: 22px;
  }
  .ordiv span {
    position:relative;
    top:7px;
  }
  @media (max-width:640px) {
    .slider .sitevisittab {
      display:none;
    }
  }
</style>
<style>
  @media (min-width:641px) and (max-width:767px){
    body {
      overflow-x:hidden;
    }
    .header .logo img {
      width:190px !important;
    }
    .cbp-hrmenu > ul, .cbp-hrmenu .cbp-hrsub-inner {
      text-align:left;
    }
    .cbp-hrmenu {
      width: 500px !important;
      margin: 3em 0 0;
    }
    .cbp-hrmenu > ul > li > a {
      font-size:14px;
    }
    .header .logo img {
      left:10px;
    }
    .cookieinner p {
      padding:0px 10px;
    }
    .mainpadding {
      max-width:980px;
      width:inherit !important;
    }
    .verticletext {
      top:-40px !important;
    }
    .howcanwehelp, .pressfeature, .outer-wrapper {
      padding: 30px 20px 55px 20px !important;
    }
    div#expandit {
      right:-10px !important;
    }
    .pressbanner img {
      max-width:100%;
    }
    .block {
      height:169px !important;
      width:169px !important;
    }
    .block-scroller .next, .generic-slider .next, .wtg-super .next {
      left: 680px !important;
    }
    .medium_wrapper .images img {
      display: inline-block;
      /* width: 45%; */
      margin: .7%;
      margin: 0 auto !important;
      float: none !important;
    }
    .meetwithourteemouter div {
      float: none !important;
      text-align: center;
    }
    .imagesec img {
      width:250px !important;
      height:250px;
    }
    .threetestimonials p:after {
      bottom: 35px !important;
    }
    .threetestimonials p:before {
      top: 30px !important;
    }
    .mainouterwrapper {
      padding:0px 10px;
    }
    .wrapper {
      max-width:980px;
      width:inherit !important;
    }
    .outer-wrapper {
      padding-bottom: 0px !important;
    }
    #mc_embed_signup {
      top: 50px !important;
      position: relative;
    }
    footer ul {
      width:19% !important;
    }
    footer .copyright ul {
      width:100% !important;
    }
    .stepsul {
      display:none !important;
    }
    .innerpagewrap .pages {
      display:none;
    }
    .storywidth {

      padding: 0px 10px;
    }
    .whereisbegun {
      background: #dedede;
      padding: 50px 10px !important;
    }
    .wcu-container {
      display:none !important;
    }
    .parallax-3 .wcu-mobile-view {
      display:block !important;
    }
    .parallax-3 .wcu-mobile-view .m-wcu-sec {
      text-align: center;
      width: 100%;
      margin: 10px auto;
      border-bottom: 2px dotted #cedbe6;
      padding: 30px 0;
    }
    .parallax-3 .m-wcu-container .customer-service {
      height: 35px;
      margin-bottom: 12px;
    }
    .parallax-3 .wcu-mobile-view .m-wcu-sec h3 {
      font-size: 24px;
      color: #0a4f7c;
      margin-bottom: 0px;
    }
    .parallax-3 .wcu-mobile-view .m-wcu-sec p {
      color: #969696;
      font-size: 14px;
      padding: 15px 0;
      padding-top: 5px;
      line-height: 20px;
      text-align: justify;
    }
  }
</style>  
<?php 
if ($curr_url != 'newbyp-quote')
require_once("./inc/common-site-blog-header1.inc.php");
?>
<?php 
if ($curr_url == 'newbyp-quote') { ?>
<style>
body, .main { background-color: #e6e6e7;}
</style>
<?php } ?>

<?php if ($_SERVER['PHP_SELF'] == '/book-visit.php' || $_SERVER['PHP_SELF'] == '/book-confirm.php'  || $_SERVER['PHP_SELF'] == '/book-thankyou.php' || $_SERVER['PHP_SELF'] == '/initial-visit.php') { ?>
<!--link rel="stylesheet" type="text/css" href="/css/book-visit.css?v=1" /-->
<?php } ?>
<meta name="google-site-verification" content="Yrol3mRdnnNuW090dhJbuIgwuKF-v50uJx_5m0ve8zc" />
<meta name="google-site-verification" content="MSOhgUXBci9Nl-bj5JMP6qmxErkcbex5b9VwW-qu5Oc" />



<!-- styles for booking -->
<?php if($book_styles){ ?>
  <!-- style -->
  <link rel="stylesheet" href="/assets_b/css/style.css" />
  <script src="/assets_b/js/script.js"></script>
  <?php if($page_name == "pp_booking") {//pp form page?>
    <script src="https://polyfill.io/v3/polyfill.min.js?version=3.52.1&features=fetch"></script>
    <script src="https://js.stripe.com/v3/"></script> 
<?php }} ?>

<style>
  @media (max-width: 700px){
    .book_wrapper .prev_day_btn {
      z-index: 100;
    }
  }

</style>

</head>

<body onload="initPage()"<?php if (strpos($_SERVER['REQUEST_URI'], 'build-your-price-app')) echo ' onKeyPress="return disableTabKey(event);" onKeyDown="return disableTabKey(event);"' ?>>
  <!-- Google Tag Manager (noscript) -->
  <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KG9JT5C"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
<?php require_once("./inc/common-site-blog-header2.inc.php");?>
      </div>
    </div>
    <!--STATIC BANNER-->
    <style>
      .top-banner-bg {
        background-color: #6482A4C7; 
        width: 100%; 
        height: 60px; 
        color: #fff;
        position: relative;
        z-index: 999;
        text-align: center;
        top: 100px;
      }
      .top-banner-text-desktop {
        display: inline;font-family: Calibri; text-align: center; font-size: 25px;
      }
      .top-banner-text-mob {display: none;}
      .top-banner-left-icon {
        height: 40px;position: relative;top: 10px;left: -10px;
      }
      .top-banner-right-icon {
        height: 40px;position: relative;top: 10px;right: -10px;
      }
      @media screen and (max-width:480px) {
        .top-banner-bg {
          background-color: #6482A4C7; 
          width: 100%; 
          height: 40px; 
          color: #fff;
          position: relative;
          z-index: 999;
          text-align: center;
          top: 95px;
        }
        .top-banner-text-mob {
          display: block; font-family: Calibri; text-align: center; font-size: 14px;width: 85%;margin: 0 auto;padding-top: 5px;
        }
        .top-banner-text-desktop {display: none;}
        .top-banner-left-icon {
          height: 20px; width: 20px; position: absolute;top: 10px;left: 8px;
        }
        .top-banner-right-icon {
          height: 20px; width: 20px;position: absolute;top: 10px;right: 8px;
        }
      }
    </style>
    <div class="main"><?php __DIR__; ?><!-- site header end -->