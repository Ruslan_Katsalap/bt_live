<?php
        if($curr_url != 'house-tours')
        {
            if ( !defined('hidePageMedia') ) {
                showPageMedia();
            }
        }

        ?>
<div class="cookiebar">
	<div class="cookieinner">
	<p>We use cookies to provide you with a user-friendly, safe and effective website. Carry on browsing if you're happy with this, or find out how to manage your cookies <a href="/cookie-policy.html">here</a>.</p>
    <span class="mainouterclose"><span>x</span></span>
    <div class="clearfix"></div>
    </div>
</div>
<div class="thankyoupop">
    	<div class="thanksinner">
        	<h4>Thank you for your request.</h4>
            <p>We will contact you at your preferred time.</p>
			<a href="javascript:void(0);">Ok</a>
        </div>
    </div>

<script type="text/javascript" src="/js/miscjs.js"></script>
<script>
$(document).ready(function(e) {
		if($.cookie("cookiebar") != 'seen'){
			 // Set it to last a year, for example.
			setTimeout(function() {
				$(".cookiebar").fadeIn(300);
				//$(".autopopup").animate({"opacity":1})
			}, 3000);


		};
		$(".mainouterclose").click(function(){
				$(".cookiebar").fadeOut(300);
				$.cookie("cookiebar", "seen", { expires: 7 });
				});

});
</script>

<script>
$(document).ready(function(e) {
    <? if($flage == true){?>
$(".thankyoupop").fadeIn(200);
<? } ?>
});

</script>
<script>
$(document).ready(function(e) {
    <? if($flagenew == true){?>
$(".outsidepoup").fadeIn(200);
<? } ?>
});

</script>
<script>
$(document).ready(function(e) {
    <? if($newflaging == true){?>
$(".bookthanks").fadeIn(200);
<? } ?>
});

</script>
<?php if ('/book-visit.php'==$_SERVER['PHP_SELF']) : ?>

<!--script src="/js/jquery-ui.js"></script-->
<script src="//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js" integrity="sha512-JGKswjfABjJjtSUnz+y8XUBjBlwM1UHNlm2ZJN7A2a9HUYT3Mskq+SacsI35k4lok+/zetSxhZjKS3r3tfAnQg==" crossorigin="anonymous"></script>
<script src="/js/jquery-ui.multidatespicker.js?v=1"></script>
<script src="/js/book-visit.js?v=1"></script>
<script type="text/javascript">
  <?php if (isset($a_pref_dates) && $a_pref_dates): ?>
  DesignTeam.setPrefDates(<?php echo $_POST['availibility'] ?>);
  <?php endif; ?>
  jQuery(document).ready(DesignTeam.initBookCal);
</script>
<?php endif; ?>
<script>
	$(document).ready(function(e) {
        var d = new Date();
		var n = d.getDay();
		var gt = d.getHours() + "." + d.getMinutes();
		console.log(gt);
		if (n == 6) {
			$(".automatedbanner").css("display","inherit");
			$(".SliderLayered").addClass("SliderLayeredtop");

			}

		else if (n == 0) {
			if (gt <= "15.30") {

				$(".automatedbanner").css("display","inherit");
				$(".SliderLayered").addClass("SliderLayeredtop");

				}
			//var gt = n.getTime();
			//if (gt == )
			}
		else {
			$(".automatedbanner").css("display","none");
			$(".SliderLayered").removeClass("SliderLayeredtop");
			}
    });

		//document.getElementById("demo").innerHTML = n;

	//setInterval(loop, 10000);
</script>
<?php if ('/book-confirm.php'==$_SERVER['PHP_SELF']) : ?>
<script src="/js/book-visit.js?v=1"></script>
<script type="text/javascript">
  jQuery(document).ready(DesignTeam.initCouponCode);
</script>
<?php endif; ?>

<!-- here -->
<?php if ('buildteam.ua'!=$_SERVER['HTTP_HOST']) : ?>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "https://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<!-- active campaign -->
<script type="text/javascript">
(function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
vgo('setAccount', '1001345068');
vgo('setTrackByDefault', true);
vgo('process');
</script>
<?PHP /*<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5628250-1");
pageTracker._trackPageview();
} catch(err) {}</script> */?>
<?php if ('/brochure_flipbook.php'!= $_SERVER['PHP_SELF']) ?>

<?php endif; ?>
<?php if ('/book-site-visit.php'!= $_SERVER['PHP_SELF'] && '/book-visit.php'!=$_SERVER['PHP_SELF']) : ?>
<!--<script type="text/javascript">
	$(document).ready(function(e) {
		if($.cookie("popup") != 'seen'){
			 // Set it to last a year, for example.
			setTimeout(function() {
				$(".autopopup").css("display","table")
				$(".autopopup").animate({"opacity":1})
			}, 10000);


		};
		$(".popupclose").click(function(){
				$(".autopopup").css("display","none");
				$.cookie("popup", "seen", { expires: 7 });
				});
		$(".popuptext a").click(function(){
				$.cookie("popup", "seen", { expires: 7 });
				});
});
</script> -->
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function(){
		//Check to see if the window is top if not then display button
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
		if ($(window).width() < 640) {
		$("#byp_slide_box ul li").width($(window).width());
		}
	});
</script>
<script>
$(document).ready(function(e) {

	$(".bott_menu ul .clickexpand").click(function(){

		var getheight = $(this).parent('ul').children('li').length * 28;
		//alert(getheight);
		if ($(this).hasClass('collapsednav')) {
				$(this).parent("ul").animate({height:getheight},200);
				$(this).removeClass('collapsednav');

			}
		else {
				$(this).parent("ul").animate({height:25},200);
				$(this).addClass('collapsednav');

			}

		});


});
</script>
<!--<script src="/js/jquery-3.1.1.min.js"></script>-->
<script async src="/js/jquery.corner.js"></script>

<script>
$('.dropNav').parent('li').click(function(e) {
  if(!_widthResize){
    $(this).children('.dropNav').stop(true, true).slideToggle(300);
    //e.preventDefault();
  }
});
(function($){
  $.fn.extend({

    sideNav : function(){
		$("#closePageslide").hide();

      if( $('.pageslideBg').length == 0 ) {
            $('<div />').attr( 'class', 'pageslideBg' ).appendTo( $('body') );
        }
        var $btn = $(this),
			$pageslide = $($btn.attr("href")),
			$pageslideBg = $('.pageslideBg'),

			_width = $pageslide.width();
		$btn.click(function(){
			$pageslideBg.show();
			$("#closePageslide").show();
			$(this).hide();
			$pageslide.css({ 'display':'block'}).animate({'left':0 });
        	return false;
      		});
      $pageslideBg.click(function() {
        $(this).hide();
        $pageslide.animate({'left': _width*-1 + 'px' },function(){
          $(this).hide();
		  $("#openPageslide").show();
		  $("#closePageslide").hide();
        });

        return false;
      });
	 $("#closePageslide").click(function() {
		 $(this).hide();
			$("#openPageslide").show();
		$pageslide.animate({'left': _width*-1 + 'px' },function(){
          $(this).hide();
		  $pageslideBg.hide();
        });

		 return false;

		});

      return this;
    }

  });
})(jQuery);

$("#openPageslide").sideNav();
</script>

<!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
<?php if ('/byp_latest.php' == $_SERVER['PHP_SELF'] || '/build-your-price-app.php' == $_SERVER['PHP_SELF']): ?>
<script type="text/javascript" src="/js/byp.js?v=106"></script>
<?php endif; ?>
<?php if ('/byp_landing.php' == $_SERVER['PHP_SELF']): ?>
<script type="text/javascript" src="/js/new-byp.js"></script>
<?php endif; ?>
<?php if ('/build-your-price-app-akash.php' == $_SERVER['PHP_SELF']): ?>
<script type="text/javascript" src="/js/new-byp.js"></script>
<?php endif; ?>
<script src="/js/main.js"></script>
<script async src="/js/cbpHorizontalMenu.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="/js/jquery.bxslider.min.js"></script>

<!-- bxSlider CSS file -->
<link href="/css/jquery.bxslider.css" rel="stylesheet" />

        <script>
            $(function() {
                //cbpHorizontalMenu.init();
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
				var controls = document.getElementById("project-gallery") != null ? true : false;
				$('.bxslider').bxSlider({
					auto: true,
					controls: controls
				  });
			});
        </script>
		<script>
			$(document).ready(function(e) {
			  $("#contactdrop").hover(function(e) {
				  //alert();
					$("#cbp-hrmenu .qwe").each(function(index, element) {

						//alert($(this).attr("class"));
						$(this).removeClass("cbp-hropen");
                    });
				});
			});
        </script>
        <script type="text/javascript">


            var delay = 1000; // ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ´ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¶ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ° ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸

            $('#button-up').click(function () { // ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµ "ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ" ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ´ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ² ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ
                /* ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ° ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ */
                $('body, html').animate({
                    scrollTop: 0
                }, delay);
            });
            $('#button-down').click(function () { // ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµ "ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ" ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ´ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ² ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ
                /* ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ° ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ */
               $("html, body").animate({ scrollTop: $(document).height() }, "slow");
                return false;
            });


        </script>
        <script type="text/javascript">
        jQuery(document).ready(function($){

            /* ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ´ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ³ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ */
            $('#nav-wrap').prepend('<div id="menu-icon"><img style="width:32px; height:32px;" src="//s3-eu-west-1.amazonaws.com/weld-tutorials/getting-started/icon-hamburger-light.svg"></div>');

            /* ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ³ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ */
            $("#menu-icon").on("click", function(){
                $(this).toggleClass("active");
                $("#nav").slideToggle();
            });

        });
        </script>
        <script>
        	$(document).ready(function(e) {
                // lightbox for dialog popup
			jQuery('.dialog').click(function (e) {
			  e.preventDefault();

			  var id = $(this).attr('id');

			  id = id.replace("_link", "");
			  id += "_dialog";

			  $.lightbox({
				content: document.getElementById(id).innerHTML
			  });
			});
            });
        </script>
        <script>
        $(function(){

							$(".ui-datepicker-calendar tr").each(function(index, element) {
								 $($(this).children('td')).each(function(index, element) {
                                    if (!$(this).hasClass('ui-state-disabled')) {
										var getchildern = $(this).children('a').text();
										console.log(getchildern);
										if ($.cookie('fromdate') == getchildern) {

											$(this).addClass('dt_pick1 ui-state-highlight');
											}
										else if ($.cookie('gettodate') == getchildern)	{
											$(this).addClass('dt_pick2 ui-state-highlight');
											}
									}

                                });
                            });

							});
        </script>
<script>
$("#cbp-hrmenu .qwe").hover(function(){
	$(".cbp-hrmenu .cbp-hrsub").css("background", "rgba(255,255,255,1)");
	$(".top_part").css("background", "rgba(255,255,255,1)");
	});
$("#cbp-hrmenu").mouseleave(function(){
	$(".cbp-hrmenu .cbp-hrsub").css("background", "rgba(255,255,255,0.9)");
	$(".top_part").css("background", "rgba(255,255,255,0.9)");
	});
</script>
<script>
$(document).ready(function(e) {
    var date = new Date();
	var nextdate;
if(date.getDay() == 2) {
	console.log("tuesday");
	nextdate = 3;
	date.setDate(date.getDate() + 3);
	}
else if(date.getDay() == 3) {
	console.log("Wed");
	nextdate = 3;
	date.setDate(date.getDate() + 2);
	}
else if(date.getDay() == 4) {
	console.log("Wed");
	nextdate = 2;
	date.setDate(date.getDate() + 4);
	}
else if(date.getDay() == 5) {
	nextdate = 2;
	date.setDate(date.getDate() + 4);
	}
else if(date.getDay() == 6) {
	nextdate = 2;
	date.setDate(date.getDate() + 3);
	}
else if(date.getDay() == 0) {
	nextdate = 2;
	date.setDate(date.getDate() + 3);
	}
else {
	nextdate = 2;
	date.setDate(date.getDate() + 2);
	}


var dateMsg = ("0" + date.getDate("00")).slice(-2);

var dateMsg2 = date.getDate('DD');
var datetwo = dateMsg2 + nextdate;
var trimmeddate = ("0" + datetwo).slice(-2);
console.log(dateMsg2)
var monthMsg = date.getMonth()+1;
if (dateMsg <= 31) {
$(".date1").html(dateMsg);
//$(".date1").html("29");
	if (dateMsg2 + nextdate >= 31) {
		$(".date2").html("0" + (trimmeddate) - 30);
		//$(".date2").html("01");
	}
	else {
		$(".date2").html(trimmeddate);
		//$(".date2").html("30");
		}
}

var months = ['Jan','Feb','March','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'];
var nowmonth = new Date();
var checkdate = nowmonth.getDate()
//console.log(checkdate);
//console.log("ok");
if (checkdate >= 29 ) {
	//console.log("ok")
var thisMonth = months[nowmonth.getMonth() + 1];
var thisMonth2 = months[nowmonth.getMonth() + 1]; // getMonth method returns the month of the date (0-January :: 11-December)
}
else if (checkdate >= 27) {
	console.log("ok");
	var thisMonth2 = months[nowmonth.getMonth() + 1];
	var thisMonth = months[nowmonth.getMonth()];
	console.log(thisMonth2);
	}
else {
var thisMonth = months[nowmonth.getMonth()];
var thisMonth2 = months[nowmonth.getMonth()];
//console.log(thisMonth);
}
$(".dateinline .month1").text(thisMonth);
$(".dateinline .month2").text(thisMonth2);
});


</script>
<script>
$(document).ready(function(e) {
	var today = new Date();
   var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
   var day = days[today.getDay()];
   var getime = today.getHours();
   //alert(getime);
	//First of month
	//days[firstOfMonth.getDay()];
	//last of month
	//days[lastOfMonth.getDay()];
	//alert(day);
	$(".daytimewrap .dayinline").each(function(index, element) {
        var getdays = $(this).children(".contactday").text();
		//alert(getdays);
		if (getdays == day) {
			$(this).addClass("today");
			var gethour = $(this).find("span").text();
			var getclose = gethour - 12;
			$(".contactblock #openuntil").text(gethour - 12);
			$("#contactdrop h4 span").text(gethour - 12);

			if (day == 'Monday' || day == 'Tuesday' || day == 'Wednesday' || day == 'Thursday') {
				//alert(getime);
				if (getime < '8') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("8");
					$(".closetext #menuopentomo").text("8");
					}

				else if (getime >= '20') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("8");
					$(".closetext #menuopentomo").text("8");
					}
			}
			else if (day == 'Friday'){
				//alert(getime);
				if (getime < '8') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("8");
					$(".closetext #menuopentomo").text("8");
					}
				else if (getime >= '20') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("9");
					$(".closetext #menuopentomo").text("9");
					}
				}
			else if (day == 'Saturday'){
				if (getime < '9') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("9");
					$(".closetext #menuopentomo").text("9");
					}
				else if (getime >= '17') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("10");
					$(".closetext #menuopentomo").text("10");
					}
				}

			else if (day == 'Sunday'){
				//alert(getime);
				if (getime < 10) {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("10");
					$(".closetext #menuopentomo").text("10");
					}

				else if (getime >= '16') {
					//alert(getime);
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("8");
					$(".closetext #menuopentomo").text("8");
					}
				}



			}
    });
});
</script>
<script>
 $(document).ready(function(e) {
	 //alert();
    //$("ul.news").css("right", "0px");
});
 </script>
<script>

/*$('.dialog-request #popupform .carousel').carousel({
    interval: false,
}); */
$(document).ready(function(e) {

});
</script>
<script>

</script>
<script>
 $(document).ready(function(e) {
	 //alert();
	var getwidth = $(".news").height();
	$(".catfilter li:first").addClass("active");
	//var setwidth = $(".news").length * getwidth;
	$(".data-post-wrap").height(getwidth);
    $(".news").each(function(index, element) {
        var get2017 = $(this).attr("data-post");
		// console.log(get2017);
		if (get2017 == "2019") {
			$(this).css("right", "-20px");
			$(this).addClass("active");
			}
		else {
			$(this).css("right", "-1020px");
			}
    });
		$(".catfilter li").click(function(e){
			//e.preventDefault();
		var getyear = $(this).attr("data-year");
		//alert(getyear);
		var getwidth = $(".news.active").height();
		//var setwidth = $(".news").length * getwidth;
		$(".catfilter li").removeClass("active");
		$(this).addClass("active");
		//alert(getyear);
		$(".news").each(function(index, element) {
			var getpost = $(this).attr("data-post");
			if (getpost == getyear) {
				//$(this).show("slide", { direction: "right" }, 1200);
				//e.stopPropagation();
				$(this).css("right", "-20px");
				$(this).addClass("active");
				getwidth = $(".news.active").height();
				$(".data-post-wrap").height(getwidth);
				}
			else {
				$(this).css("right", "-1020px");
				$(this).removeClass("active");
				}
			});
		});
});
 </script>
<script>
$(document).ready(function(e) {
    $(".dialog-request .timewrap .slide:first-child").show();
	$('.dialog-request .daycont input').on('change', function() {
		$('.dialog-request .daycont label').css("color","#75787f");
		$(".dialog-request .timewrap .slide").hide();
		var getvalue = $(this).val();
		//alert(getvalue);
		$('.dialog-request input[name=day]:checked').parent(".pertiday").children("label").css("color","#f5641e");
		$(".dialog-request .timewrap .slide").each(function(index, element) {
		 if($(this).hasClass(getvalue)) {
			$(this).show();
			}
});
});
$(document.body).on('click', '.dialog-request .timecont .pertime label' ,function(){

	//alert();
//var savethis = 	$(this).parent(".pertime").find("label");
   $('.dialog-request .pertime label').css("color","#75787f");
   $(this).css('color', '#f5641e');
   //alert($(this).parent(".pertime").find("label").text());
   //alert()
});
});

</script>

<script>
$(".thanksinner a").click(function(){
	$(".thankyoupop").fadeOut(200);
	});
</script>
<script>
$(".outsideinner a").click(function(){
	$(".outsidepoup").fadeOut(200);
	});
</script>
<script>
$(".bookthanksinner a").click(function(){
	$(".bookthanks").fadeOut(200);
	});
</script>
<script src='//www.google.com/recaptcha/api.js'></script>
    <script>
		 var CaptchaCallback = function() {
        $('.g-recaptcha').each(function(index, el) {
          grecaptcha.render(el, {'sitekey' : $(el).data('6LcZGKYUAAAAAHxPrL02cGYHuDzEfpUBpWCvNRgt'), 'expired-callback': expCallback});
        });
      };
      var expCallback = function() {
          grecaptcha.reset();
       };
	/*var verifyCallback = function(response) {
        alert(response);
      };
      var recaptcha1;
      var recaptcha2;
	  var recaptcha3;
      var myCallBack = function() {
        //Render the recaptcha1 on the element with ID "recaptcha1"
       if ( $('#recaptcha1').length ) {
        grecaptcha.render('recaptcha1', {'sitekey' : '6Lc8QigUAAAAAEUyusx4BYsgGA5cnlynDk-Ni-ML'
        });
		}
		if ( $('#recaptcha2').length ) {
		   grecaptcha.render('recaptcha2', {'sitekey' : '6Lc8QigUAAAAAEUyusx4BYsgGA5cnlynDk-Ni-ML'
		   });
		}
		if ( $('#recaptcha3').length ) {
		   grecaptcha.render('recaptcha3', {'sitekey' : '6Lc8QigUAAAAAEUyusx4BYsgGA5cnlynDk-Ni-ML'
		   });
		}


      };*/
    </script>
<script>
/*$(".newcontentpush .qwe").hover(


	function() {
		var getheight = $(this).children(".cbp-hrsub").height();
	  $(".header").css("padding-bottom",getheight);

    },
    function() {
	$(".header").css("padding-bottom","0px");

    }
);*/
</script>


<script type='text/javascript' src="/mobilemenu/modernizr-custom.min.js"></script>
<script type='text/javascript' src='/mobilemenu/core.min.js'></script>
<script type='text/javascript' src='/mobilemenu/plugins.js'></script>
<script type='text/javascript' src='/mobilemenu/menumain.js'></script>
<script src="//code.tidio.co/kpozhs4acmhlk8jtothqvkc3qwcp5ydd.js"></script>
</body>

</html>