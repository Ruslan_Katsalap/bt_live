<div style="float:left">
  <div style="float:left;width:81%">
    <div>
       Kitchen Extensions | Side Return Extensions | Side Return Extension Ideas | Kitchen Side Return Extension | Design and Build Extension | Side Return and Loft Conversion | Side Return Costs | Basement Builder | Building Refurbishment London | Side Return Kitchen Extension | Side Return Ideas | Victorian Side Return Extension
    </div>
    <div>
      Unit 3D | 26-32 Voltaire Road | Clapham | SW4 6DH Tel: 020 7495 6561 | <a href="/terms-and-conditions.html" title="Terms &amp; Conditions">Terms &amp; Conditions</a> | <a href="/privacy-policy.html" title="Privacy Policy">Privacy Policy</a> | <a href="/sitemap.html" title="Sitemap">Sitemap</a><br/>
      &copy; <?php echo date('Y', time()); ?> | All Rights reserved - Build Team Holborn Ltd
    </div>
  </div>
  
  <div style="float:right;width:19%">
    <img src="/images/groups/velux-certified-installer-logo-small.png?v=1" alt="Velux Certified Installer" width="52" height="53" style="float:left;margin-left:10px" />
    <!--
    <img src="/images/groups/ebc_small.png" alt="European Builders Confederation EBC" width="50" height="50" style="float:left;margin-left:10px;margin-top:5px" />
    <img src="/images/groups/trustmark_small.png" alt="TrustMark Government Endorsed Standards" width="75" height="38" style="float:left;margin-left:12px;margin-top:10px" />-->
    <img src="/images/groups/fmb_b_qual_small.jpg" alt="Federation Of Master Builders FMB" width="45" height="64" style="float:left;margin-left:8px" />
    
    <img src="/images/groups/10-years-guarantee.png" alt="10 years Guarantee" width="52" height="52" style="float:left;margin-left:8px" />
  </div>
</div>
  <div class="clear"></div>
  
</div>

</div>

<?php if ('/book-visit.php'==$_SERVER['PHP_SELF']) : ?>
<script src="/js/jquery-ui.multidatespicker.js?v=1"></script>
<script src="/js/book-visit.js?v=1"></script>
<script type="text/javascript">
  <?php if (isset($a_pref_dates) && $a_pref_dates): ?>
  DesignTeam.setPrefDates(<?php echo $_POST['availibility'] ?>);
  <?php endif; ?>
  jQuery(document).ready(DesignTeam.initBookCal);
</script>
<?php endif; ?>

<?php if ('/book-confirm.php'==$_SERVER['PHP_SELF']) : ?>
<script src="/js/book-visit.js?v=1"></script>
<script type="text/javascript">
  jQuery(document).ready(DesignTeam.initCouponCode);
</script>
<?php endif; ?>

<?php if ('buildteam.ua'!=$_SERVER['HTTP_HOST']) : ?>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5628250-1");
pageTracker._trackPageview();
} catch(err) {}</script>

<?php endif; ?>

</body>

</html>