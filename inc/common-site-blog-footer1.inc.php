<?php ?>
<div class="thankyoupop">
    	<div class="thanksinner">
        	<h4>Thank you for your request.</h4>
            <p>We will contact you at your preferred time.</p>
			<a href="javascript:void(0);">Ok</a>
        </div>
    </div>        

<footer class="round-2 ">

<!--div id="dialog-request" class="dialog-request" title="">
	<div class="lightbox_inner">
      <div class="lightbox_content">
        <h3 class="callbacktitle">Request a call back</h3>
        <form id="popupform" method="post" action="">
        <div class="popformfields fullpop">
        	<label for="name"><strong>Full name</strong></label>
            <input type="text" required name="popfullname" value="" placeholder="" />
        </div>
        <div class="popformfields halfpop">
        	<label for="number"><strong>Contact Number</strong>
            </label>
            <input type="tel" required name="popnumber" value="" placeholder="" />
        </div>
        <div class="popformfields halfpop">
        	<label for="email"><strong>Email</strong></label>
            <input type="email" required name="popemail" value="" placeholder="" />
        </div>
        <div class="popformfields fullpop">
        	<label><strong>Preferred call time</strong></label> 
            <div class="daycont">
            	<div class="pertiday">
                	<label for="mon">MON</label>
                    <input type="radio" name="day" id="mon" value="Monday">
                </div>
                <div class="pertiday">
                	<label for="tue">TUE</label>
                    <input type="radio" name="day" id="tue" value="Tuesday">
                </div>
                <div class="pertiday">
                	<label for="wed">WED</label>
                    <input type="radio" name="day" id="wed" value="Wednesday">
                </div>
                <div class="pertiday">
                	<label for="thu">THU</label>
                    <input type="radio" name="day" id="thu" value="Thursday">
                </div>
                <div class="pertiday">
                	<label for="fri">FRI</label>
                    <input type="radio" name="day" id="fri" value="Friday">
                </div>
                <div class="pertiday">
                	<label for="sat">SAT</label>
                    <input type="radio" name="day" id="sat" value="Saturday">
                </div>
                <div class="pertiday">
                	<label for="sun">SUN</label>
                    <input type="radio" name="day" id="sun" value="Sunday">
                </div>
                
            </div>
            <div class="timewrap">
            <div id="carousel-example-generic-1" data-interval="false" class="carousel slide Monday Tuesday Wednesday Thursday Friday" data-ride="carousel">
               <div class="timecont">
              <div class="carousel-inner" role="listbox">
           
            <div class="item active">
                <div class="pertime">
                	<label for="time-nine">09:30</label>
                    <input type="radio" name="time" id="time-nine" value="9:30">
                </div>
                <div class="pertime">
                	<label for="time-ten">10:00</label>
                    <input type="radio" name="time" id="time-ten" value="10:00">
                </div>
                <div class="pertime ">
                	<label for="time-ele">11:00</label>
                    <input type="radio" name="time" id="time-ele" value="11:00">
                </div>
                <div class="pertime ">
                	<label for="time-twe">12:00</label>
                    <input type="radio" name="time" id="time-twe" value="12:00">
                </div>
                <div class="pertime ">
                	<label for="time-thir">13:00</label>
                    <input type="radio" name="time" id="time-thir" value="13:00">
                </div>
                <div class="pertime ">
                	<label for="time-four">14:00</label>
                    <input type="radio" name="time" id="time-four" value="14:00">
                </div>
                <div class="pertime ">
                	<label for="time-fif">15:00</label>
                    <input type="radio" name="time" id="time-fif" value="15:00">
                </div>
                </div>
                <div class="item">
                
                <div class="pertime ">
                	<label for="time-sixt">16:40</label>
                    <input type="radio" name="time" id="time-sixt" value="16:40">
                </div>
                </div>
                
            </div>
             <a class="left carousel-control" href="#carousel-example-generic-1" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only"><</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic-1" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">></span>
  </a>
            </div>
           
            </div>
            <div id="carousel-example-generic-2" data-interval="false"  class="carousel slide Saturday" data-ride="carousel">
               <div class="timecont">
              <div class="carousel-inner" role="listbox">
           
            <div class="item active">
            	
                <div class="pertime ">
                	<label for="time-nine">09:00
                    <input type="radio" name="time" id="time-nine" value="9:00"></label>
                </div>
                <div class="pertime ">
                	<label for="time-ten">10:00</label>
                    <input type="radio" name="time" id="time-ten" value="10:00">
                </div>
                <div class="pertime ">
                	<label for="time-ele">11:00</label>
                    <input type="radio" name="time" id="time-ele" value="11:00">
                </div>
                <div class="pertime ">
                	<label for="time-twe">12:00</label>
                    <input type="radio" name="time" id="time-twe" value="12:00">
                </div>
                <div class="pertime ">
                	<label for="time-thir">13:00</label>
                    <input type="radio" name="time" id="time-thir" value="13:00">
                </div>
                <div class="pertime ">
                	<label for="time-four">14:00</label>
                    <input type="radio" name="time" id="time-four" value="14:00">
                </div>
                <div class="pertime ">
                	<label for="time-fif">15:00</label>
                    <input type="radio" name="time" id="time-fif" value="15:00">
                </div>
                </div>
                <div class="item">
                
                <div class="pertime ">
                	<label for="time-sixt">16:00</label>
                    <input type="radio" name="time" id="time-sixt" value="16:00">
                </div>
                <div class="pertime ">
                	<label for="time-sevent">17:00</label>
                    <input type="radio" name="time" id="time-sevent" value="17:00">
                </div>
                
                
                </div>
                
            </div>
             <a class="left carousel-control" href="#carousel-example-generic-2" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only"><</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic-2" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">></span>
  </a>
            </div>
           
            </div>
            <div id="carousel-example-generic-3" data-interval="false"  class="carousel slide Sunday" data-ride="carousel"-->
              <!-- Wrapper for slides -->
               <!--div class="timecont">
              <div class="carousel-inner" role="listbox">
           
            <div class="item active">
            	
                <div class="pertime ">
                	<label for="time-ten">10:00</label>
                    <input type="radio" name="time" id="time-ten" value="10:00">
                </div>
                <div class="pertime ">
                	<label for="time-ele">11:00</label>
                    <input type="radio" name="time" id="time-ele" value="11:00">
                </div>
                <div class="pertime ">
                	<label for="time-twe">12:00</label>
                    <input type="radio" name="time" id="time-twe" value="12:00">
                </div>
                <div class="pertime ">
                	<label for="time-thir">13:00</label>
                    <input type="radio" name="time" id="time-thir" value="13:00">
                </div>
                <div class="pertime ">
                	<label for="time-four">14:00</label>
                    <input type="radio" name="time" id="time-four" value="14:00">
                </div>
                <div class="pertime ">
                	<label for="time-fif">15:00</label>
                    <input type="radio" name="time" id="time-fif" value="15:00">
                </div>
                <div class="pertime ">
                	<label for="time-sixt">16:00</label>
                    <input type="radio" name="time" id="time-sixt" value="16:00">
                </div>
                </div>
               
            </div>
             <a class="left carousel-control" href="#carousel-example-generic-3" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only"><</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic-3" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">></span>
  </a>
            </div>
           
            </div>
            </div>
        </div>
        <div class="popformfields fullpop">
        	<label><strong>Enquiry</strong></label>
            <div class="halfpop">
            <div class="enquirywrap">
            	
               		<input type="radio" name="enquiry" id="sitevisit" value="Site Visit">
               <label for="sitevisit"><p>Site Visit</p> </label>
            </div>
            
            <div class="enquirywrap">
            	
           		<input type="radio" name="enquiry" id="quote" value="Quote">
                <label for="quote"><p>Quote</p></label>
            </div>
           <div class="enquirywrap">
            	
           		<input type="radio" name="enquiry" id="house" value="House Tour">
                <label for="house"><p>House Tour</p></label>
            </div>
            </div>
            <div class="halfpop">
            <div class="enquirywrap">
            	
            	<input type="radio" name="enquiry" id="designserv" value="Deign services">
                <label for="designserv"><p>Design Services</p></label>
            </div>
            <div class="enquirywrap">
            	
           		<input type="radio" name="enquiry" id="buildser" value="Build services">
           	 	<label for="buildser"><p>Build Services</p></label>
            </div>
            <div class="enquirywrap">
            	
                <input type="radio" name="enquiry" id="otherser" value="Other">
                <label for="otherser"><p>Other</p></label>
            </div>
            </div>
            <div class="clearfix"></div>
         </div> 
         <div class="popformfields">
        	
       		<input id="popsubmitbutton" type="submit" value="Submit" /> 
       </div>
       </form>
      </div>
    </div>
</div-->   

<?php 
        if($curr_url == 'newbyp-quote')
{?>
<?php if (isMobile() === FALSE) { ?>
    <div style="margin-top: -138px;background-color:#f4f3f3;">
<?php } else { ?>
    <div style="margin-top: -27px;background-color:#f4f3f3;">
<?php } ?>
        <img  src="/images_new_design/logos/5logos.png" style="width:100%;max-width:1140px">
    </div>

<?php } ?>


<div class="up_down">
    <a  id="button-down">
        <img src="/images_new_design/Chevron-down2.png">
    </a>
    <a  id="button-up">
        <img src="/images_new_design/Chevron-up.png">
    </a>
</div>

    <div class="wrapper" style="max-width:1920px;width:100%;">


<?php
if ($curr_url != 'newbyp-quote') { ?>
<div class="bott_menu">
<?php } else { ?>
<div class="bott_menu" style="margin-top:-6px;">
<?php } ?>

        



<div class="top_footer">

            

            <!-- Begin MailChimp Signup Form -->
            <!-- <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css"> -->
				
            <div id="mc_embed_signup">
            <form action="https://buildteam.us7.list-manage.com/subscribe/post?u=2460f4828cafef8bf4e6ec3d1&amp;id=292211ee50" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                        <h4>SIGN UP FOR OUR NEWSLETTER</h4>

                        <div class="mc-field-group">

<input type="email" value="" name="EMAIL" placeholder="Enter Your Email" class="required email" id="mce-EMAIL" style="margin:0;">
<input type="submit" value="" name="subscribe" id="mc-embedded-subscribe" class="button">

</div>
<div id="mce-responses" class="clear">
    <div class="response" id="mce-error-response" style="display:none;"></div>
    <div class="response" id="mce-success-response" style="display:none;"></div>
</div>  
</div>
  
<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0477c5b0ee37e35f5e572f946_cb22113f6b" tabindex="-1" value=""></div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->

</div>
<div class="clearfix"></div>

<div class="col_full" style="max-width: 1140px;width:100%;margin: 50px auto;height: 450px;">
    <div class="col_half" style="width: 46%;margin-right: 40px;padding-right: 40px;border-right: 4px solid #DFDFE1;height: 500px;">
        <h4 class="footer-title-left">WHAT WE OFFER</h4>
        <div class="footer-line-left"></div>
        <br>
        <ul class="left">
        <li><a href="/design-bundles.html">Design</a></li>
        <li><a href="/the-build-phase.html">Build</a></li>
        <li><a href="/what-we-do/3d-visualisation-service-page.html">3D Visualiation Service</a></li>
        <li><a href="/about-us/guarantees-and-insurance-page.html">10 Year Guarantee</a></li>
        <li><a href="/what-we-do/faq.html">FAQ's</a></li>
        <li><a href="/build-your-price-start.html">Instant Online Quote</a></li>
        </ul>
        <br>
        <h4 class="footer-title-left">About Us</h4>
        <div class="footer-line-left"></div>
        <br>
        <ul class="left">
        <li><a href="/our-team/meet-the-team.html">Meet The Team</a></li>
        <li><a href="/about-us/why-choose-page.html">Why Choose us</a></li>
        <li><a href="/about-us/new-testimonials.html">Client Testimonials</a></li>
        <li><a href="/about-us/cover-areas.html">Area We Cover</a></li>
        <li><a href="/about-us/house-tours.html">Open House</a></li>
        <li><a href="/our-team/join-our-team.html">Join our Team</a></li>
        </ul>
    </div>

        <div class="col_half" style="width: 40%;margin-right: 0%;">
        <h4 class="footer-title-right">KNOWLEDGE BASE</h4>
        <div class="footer-line-right"></div>
        <br>
        <ul class="right">
        <li><a href="/blog/">Blog</a></li>
        <li><a href="/news.html">Latest News</a></li>
        <li><a href="/news/press-releases.html">Press Coverage</a></li>
        <li><a href="/about-us/greener-living-page.html">Greener Living</a></li>
        <li><a href="/what-we-do/why-choose-specialist-page.html">Why Choose a Specialist?</a></li>
        
        </ul>
        <br>
        <h4 class="footer-title-right" style="margin-top: 34px;">GALLERY</h4>
        <div class="footer-line-right"></div>
        <br>
        <ul class="right">
        <li><a href="/project-gallery/projects-side-return-extensions.html">Ground Floor Extensions</a></li>
        <li><a href="/project-gallery/projects-loft-conversions.html">Loft Conversions</a></li>
        <li><a href="/about-us/house-tours.html#previous">Virtual House Tours</a></li>

        </ul>
        <div class="social">
                <a target="_blank" href="https://www.instagram.com/buildteamldn/" class="insta"></a>

                <a target="_blank" href="https://www.facebook.com/Build-Team-391933624520838/" class="fb"></a>

                <a target="_blank" href="https://twitter.com/BuildTeamLDN" class="twi"></a>

                <a target="_blank" href="https://uk.pinterest.com/buildteam/" class="pinterest"></a>
        </div>
        </div>
</div>

    
    
</div>
</div>
