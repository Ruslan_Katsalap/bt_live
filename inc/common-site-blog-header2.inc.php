<?php ?>
<!-- mb blog-site-common nav 2 start -->
    <div class="header">
      <div class="top_part">
        <div class="wrapper">
<?php if ($curr_url != 'newbyp-quote') { ?>
          <div class="header-line">
<?php } ?>          
            <div class="logo"><a href="/"><img src="/images_new_design/Artboard 1.svg"></a></div>
<?php if ($curr_url != 'newbyp-quote') { ?>
            <nav id="cbp-hrmenu" class="cbp-hrmenu"> <!-- salesadded -->
              <ul class="dropdown grey menus" style="margin-top:8px;">
                <li class="qwe nourl <?php if ($curr_url == 'nationwide' || $curr_url == 'design-planning-database' || $curr_url == 'interior-landscape-design' || $curr_url == 'party-wall' || $curr_url == 'pricing' || $curr_url == 'architectural-design-phase' || $curr_url == 'meet-with-us' || $curr_url == 'pre-purchase-feasibility' || $curr_url == 'nationwide-design-service' || $curr_url == 'design-bundles') {echo "active"; }?>">Design
                  <div class="cbp-hrsub designdropdown">
                    <div class="cbp-hrsub-inner">
                      <div class="foredge" style="padding: 0 2.45em 0;">
                        <ul class="sub_menu z3">
                          <li><a href="/design-bundles.html" class="descr <?php if ($curr_url == 'design-bundles') {echo "active"; }?>" data-description="">Design Bundles</a></li>
                          <li><a href="/book-design-consultation.html" class="descr" data-description="">Free Design Consultation</a></li>
                          <li><a href="/getting-started/meet-with-us.html" class="descr <?php if ($curr_url == 'meet-with-us') {echo "active"; }?>" data-description="">Meet With Us</a></li>
                          <li><a href="/getting-started/pre-purchase-feasibility.html" class="descr <?php if ($curr_url == 'pre-purchase-feasibility') {echo "active"; }?>">Pre-Purchase Feasibility</a></li>
                          <li><a href="/architectural-design-phase.html" class="descr <?php if ($curr_url == 'architectural-design-phase') {echo "active"; }?>" data-description="">Architectural Design Phase</a></li>
                          <li><a href="/nationwide-design-service.html " class="descr <?php if ($curr_url == 'nationwide') {echo "active"; }?>" data-description="">Nationwide Design Service</a></li>
                          <li><a href="/party-wall.html" class="descr <?php if ($curr_url == 'party-wall') {echo "active"; }?>" data-description="">Party Wall Service</a></li>
                          <li><a href="/interior-landscape-design.php" class="descr <?php if ($curr_url == 'interior-landscape-design') {echo "active"; }?>" data-description="">Interior & Landscape Design</a></li>
                          <li><a class="<?php if ($curr_url == 'design-planning-database') {echo "active"; }?>" href="/design-planning-database.html">Design Database</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </li>
                <li class="qwe nourl <?php if ($curr_url == 'house-tours' || $curr_url == 'the-build-phase' || $curr_url == 'faq' || $curr_url == 'process' || $curr_url == 'ready-to-build' || $curr_url == 'watch-a-build' || $curr_url == 'build-bundles') {echo "active"; }?>">Build
                  <div class="cbp-hrsub builddropdown">
                    <div class="cbp-hrsub-inner">
                      <div class="foredge" style="padding: 0 2.3em 0;">
                        <ul class="sub_menu z3" style="max-width:121px;">
                          <li><a href="/the-build-phase.html" class="descr <?php if ($curr_url == 'build-phase') {echo "active"; }?>" data-description="">The Build Phase</a></li>
                          <li><a href="/what-we-do/faq.html" class="descr <?php if ($curr_url == 'faq') {echo "active"; }?>" data-description="">FAQ's</a></li>
                          <li><a href="/ready-to-build.html" class="descr <?php if ($curr_url == 'ready-to-build') {echo "active"; }?>" data-description="">Ready to Build</a></li>
                          <li><a href="/watch-a-build.html" class="descr <?php if ($curr_url == 'watch-a-build') {echo "active"; }?>" data-description="">Watch a Build</a></li>
                          <li><a href="/build-bundles.html" class="descr <?php if ($curr_url == 'build-bundles') {echo "active"; }?>" data-description="">Build Bundles</a></li>
                          <li><a href="/about-us/house-tours.html" class="descr <?php if ($curr_url == 'house-tours') {echo "active"; }?>" data-description="">House Tours</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </li>

                <li class="qwe"><a href="/project-gallery/gallery.html" class="<?php if ($curr_url == 'gallery') {echo "active"; }?>">Gallery</a></li>

                <li class="qwe"><a class="<?php if ($curr_url == 'newbyp-quote' || $curr_url == 'online-quote-calculator') {echo "active"; }?>" href="/online-quote-calculator.html">Instant Quote</a></li>

                <li class="qwe nourl <?php if ($curr_url == 'contact' || $curr_url == 'download-our-brochure' || $curr_url == 'new-testimonials' || $curr_url == 'why-choose-page' || $curr_url == 'our-story-page' || $curr_url == 'cover-areas' || $curr_url == 'why-choose' || $curr_url == 'our-story' || $curr_url == 'meet-the-team' || $curr_url == 'news' || $curr_url == 'blog' || $curr_url == 'press-releases' || $curr_url == 'design-planning-database') {echo "active"; }?>">More+
                  <div class="cbp-hrsub newsdropdown">
                    <div class="cbp-hrsub-inner">
                      <div class="foredge lastitem" style="padding: 0 3.05em 0;">
                        <ul class="sub_menu" style="max-width:131px;">
                          <li><a href="/about-us/our-story.html" class="descr <?php if ($curr_url == 'our-story-page') {echo "active"; }?>" data-description="">Our Story</a></li>
                          <li><a href="/our-team/meet-the-team.html" class="descr <?php if ($curr_url == 'meet-the-team') {echo "active"; }?>" data-description="">Meet The Team</a></li>
                          <li><a href="/about-us/why-choose-page.html" class="descr <?php if ($curr_url == 'why-choose-page') {echo "active"; }?>" data-description="">Why Choose Us</a></li>
                          <li><a href="/about-us/new-testimonials.html" class="descr <?php if ($curr_url == 'new-testimonials') {echo "active"; }?>" data-description="">Testimonials</a></li>
                          <li><a href="/about-us/cover-areas.html" class="descr <?php if ($curr_url == 'cover-areas') {echo "active"; }?>" data-description="">Areas We Cover</a></li>
                          <li><a href="/news/latest-news.html" class="descr <?php if ($curr_url == 'news') {echo "active"; }?>" data-description="">News</a></li>
                          <li><a href="/blog/" class="descr <?php if ($curr_url == 'blog') {echo "active"; }?>" data-description="">Blog</a></li>
                          <li><a href="/news/press-releases.html" class="descr <?php if ($curr_url == 'press-releases') {echo "active"; }?>" data-description="">Press Coverage</a></li>
                          <li><a class="<?php if ($curr_url == 'download-our-brochure') {echo "active"; }?>" href="/download-our-brochure.html">Brochure</a></li>
                          <li><a class="<?php if ($curr_url == 'contact') {echo "active"; }?>" href="/contact.html">Contact</a></li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </li>
              </ul>
            </nav>
<?php } ?>
            <div class="topwhite">
              <a href="/"><img src="/images_new_design/Artboard 1.svg" alt=""></a>
            </div>
          </div>
          <div class="flexbox formobileonly">
            <div class="flexbox__item">
              <button class="nav-trigger  js-nav-trigger"> <span class="nav-icon icon--lines"></span> </button>
            </div>

            <div class="flexbox__item navBox">
              <nav class="navigation  navigation--main" id="js-navigation--main">
                <ul>
                  <li><a class="<?php if ($curr_url == '/') {echo "active"; }?>" href="/">Home</a></li>
                  <li>
                    <a class="<?php if ($curr_url == 'nationwide' || $curr_url == 'design-planning-database' || $curr_url == 'interior-landscape-design' || $curr_url == 'party-wall' || $curr_url == 'pricing' || $curr_url == 'architectural-design-phase' || $curr_url == 'meet-with-us' || $curr_url == 'pre-purchase-feasibility' || $curr_url == 'nationwide-design-service' || $curr_url == 'design-bundles') {echo "active"; }?>">Design</a>
                    <ul class="dropNav">
                      <li><a href="/design-bundles.html" class="descr <?php if ($curr_url == 'design-bundles') {echo "active"; }?>" data-description="">Design Bundles</a></li>
                          <li><a href="/getting-started/meet-with-us.html" class="descr <?php if ($curr_url == 'meet-with-us') {echo "active"; }?>" data-description="">Meet With Us</a></li>
                          <li><a href="/getting-started/pre-purchase-feasibility.html" class="descr <?php if ($curr_url == 'pre-purchase-feasibility') {echo "active"; }?>">Pre-Purchase Feasibility</a></li>
                          <li><a href="/architectural-design-phase.html" class="descr <?php if ($curr_url == 'architectural-design-phase') {echo "active"; }?>" data-description="">Architectural Design Phase</a></li>
                          <li><a href="/nationwide-design-service.html " class="descr <?php if ($curr_url == 'nationwide') {echo "active"; }?>" data-description="">Nationwide Design Service</a></li>
                          <li><a href="/party-wall.html" class="descr <?php if ($curr_url == 'party-wall') {echo "active"; }?>" data-description="">Party Wall Service</a></li>
                          <li><a href="/interior-landscape-design.php" class="descr <?php if ($curr_url == 'interior-landscape-design') {echo "active"; }?>" data-description="">Interior & Landscape Design</a></li>
                          <li><a class="<?php if ($curr_url == 'design-planning-database') {echo "active"; }?>" href="/design-planning-database.html">Design Database</a></li>
                    </ul>
                  </li>
                  <li>
                    <a class="<?php if ($curr_url == 'house-tours' || $curr_url == 'the-build-phase' || $curr_url == 'faq' || $curr_url == 'process' || $curr_url == 'ready-to-build' || $curr_url == 'watch-a-build' || $curr_url == 'build-bundles') {echo "active"; }?>">Build</a>
                    <ul class="dropNav">
                      <li><a href="/the-build-phase.html" class="descr <?php if ($curr_url == 'build-phase') {echo "active"; }?>" data-description="">The Build Phase</a></li>
                      <li><a href="/what-we-do/faq.html" class="descr <?php if ($curr_url == 'faq') {echo "active"; }?>" data-description="">FAQ's</a></li>
                      <li><a href="/ready-to-build.html" class="descr <?php if ($curr_url == 'ready-to-build') {echo "active"; }?>" data-description="">Ready to Build</a></li>
                      <li><a href="/watch-a-build.html" class="descr <?php if ($curr_url == 'watch-a-build') {echo "active"; }?>" data-description="">Watch a Build</a></li>
                      <li><a href="/build-bundles.html" class="descr <?php if ($curr_url == 'build-bundles') {echo "active"; }?>" data-description="">Build Bundles</a></li>
                      <li><a href="/about-us/house-tours.html" class="descr <?php if ($curr_url == 'house-tours') {echo "active"; }?>" data-description="">House Tours</a></li>
                    </ul>
                  </li>
                  <li><a href="/project-gallery/gallery.html" class="<?php if ($curr_url == 'gallery') {echo "active"; }?>">Gallery</a></li>
                  <li><a class="<?php if ($curr_url == 'newbyp-quote' || $curr_url == 'online-quote-calculator') {echo "active"; }?>" href="/online-quote-calculator.html">Instant Quote</a></li>
                  <li><a class="<?php if ($curr_url == 'contact' || $curr_url == 'download-our-brochure' || $curr_url == 'new-testimonials' || $curr_url == 'why-choose-page' || $curr_url == 'our-story-page' || $curr_url == 'cover-areas' || $curr_url == 'why-choose' || $curr_url == 'our-story' || $curr_url == 'meet-the-team' || $curr_url == 'news' || $curr_url == 'blog' || $curr_url == 'press-releases' || $curr_url == 'design-planning-database') {echo "active"; }?>">More+</a>
                    <ul class="dropNav">
                      <li><a href="/about-us/our-story.html" class="descr <?php if ($curr_url == 'our-story-page') {echo "active"; }?>" data-description="">Our Story</a></li>
                        <li><a href="/our-team/meet-the-team.html" class="descr <?php if ($curr_url == 'meet-the-team') {echo "active"; }?>" data-description="">Meet The Team</a></li>
                        <li><a href="/about-us/why-choose-page.html" class="descr <?php if ($curr_url == 'why-choose-page') {echo "active"; }?>" data-description="">Why Choose Us</a></li>
                        <li><a href="/about-us/new-testimonials.html" class="descr <?php if ($curr_url == 'new-testimonials') {echo "active"; }?>" data-description="">Testimonials</a></li>
                        <li><a href="/about-us/cover-areas.html" class="descr <?php if ($curr_url == 'cover-areas') {echo "active"; }?>" data-description="">Areas We Cover</a></li>
                        <li><a href="/news/latest-news.html" class="descr <?php if ($curr_url == 'news') {echo "active"; }?>" data-description="">News</a></li>
                        <li><a href="/blog/" class="descr <?php if ($curr_url == 'blog') {echo "active"; }?>" data-description="">Blog</a></li>
                        <li><a href="/news/press-releases.html" class="descr <?php if ($curr_url == 'press-releases') {echo "active"; }?>" data-description="">Press Coverage</a></li>
                        <li><a class="<?php if ($curr_url == 'download-our-brochure') {echo "active"; }?>" href="/download-our-brochure.html">Brochure</a></li>
                        <li><a class="<?php if ($curr_url == 'contact') {echo "active"; }?>" href="/contact.html">Contact</a></li>
                    </ul>
                  </li>
                </ul>
                <div class="sitevisittab">
                  <div>
                    <div id="togglesiteswitch"><span></span></div>
                    <p class="mobiletabtitle">Meet With Us</p>
                    <div class="tabmaxwidth">
                      <div class="dateholder">
                        <div class="fromdate dateinline">
                          <p class="date date1_aad">
                            <? //=date('d',strtotime($dateFrom))?>
                            <?php
                            $CURRENTTIME = new DateTime($data['current_time']);
                            $OFFICETIME  = new DateTime('04:00:00');
                            $aad_date = date("m-d-Y");
                            $aad_day=date("D"); 
                            $aad_cur_day=date("d");
                            $date1 = str_replace('-', '/', $aad_date);
                            $aad_cur_day_1=date('d',strtotime($date1 . "+0 days"));
                            if($aad_day=="Sat")
                            {

                              if ($CURRENTTIME  > $OFFICETIME) 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+3 days"));
                              }
                              else
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+2 days"));
                              }
                            }else
                            if($aad_day=="Sun")
                            {
                              if ($CURRENTTIME  > $OFFICETIME) 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+2 days"));
                              }
                              else
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+1 days"));
                              }
                            }else
                            {
                              if ($CURRENTTIME  > $OFFICETIME) 
                              {
                                $aad_show_day=$aad_cur_day_1;
                              }
                              else
                              {
                                $aad_show_day=$aad_cur_day;
                              }
                            }
                            echo $aad_show_day;
                            ?>
                          </p>
                          <p class="month month1">
                            <?=date('M',strtotime($date1))?>
                          </p>
                          <input type='button' name="dateFrom" class="datepicker datepicker1" />

                        </div>
                        <div class="ordiv dateinline"> <span>OR</span> </div>
                        <div class="todate dateinline">
                          <p class="date date2_aad">
                            <?//=date('d', strtotime($dateTo))?>
                            <?php 

                            if($aad_day=="Fri")
                            {					
                              if ($CURRENTTIME  > $OFFICETIME) 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+4 days"));
                              }  else 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+3 days"));
                              }
                            }else
                            if($aad_day=="Sat")
                            {
                              if ($CURRENTTIME  > $OFFICETIME) 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+4 days"));
                              }  else 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+3 days"));
                              }
                            }else
                            if($aad_day=="Sun")
                            {
                              if ($CURRENTTIME  > $OFFICETIME) 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+3 days"));
                              }  else 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+2 days"));
                              }
                            }else
                            {
                              if ($CURRENTTIME  > $OFFICETIME) 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+1 days"));
                              }  else 
                              {
                                $aad_show_day = date('d',strtotime($date1 . "+0 days"));
                              }
                            }
                            echo $aad_show_day;
                            ?>
                          </p>
                          <p class="month month2">
                            <?=date('M',strtotime($date1))?>
                          </p>
                          <input type='button' name="dateTo" class="datepicker datepicker2" />
                        </div>
                      </div>
                      <div class="mobilewrap"> <a href="tel:020 7495 6561">02074956561</a> </div>
                      <div class="linktab"> <a href="/book-design-consultation.html" onclick="savedates();">Book Now</a></span>
                      </div>
                    </div>
                    <!--</form>-->
                  </div>
                </div>
              </nav>

              <div class="nav-overlay"></div>
<?php if ($curr_url != 'newbyp-quote') { ?>
            </div>
<?php } ?>
          </div>

        </div>
<!-- mb blog-site-common nav 2 end -->
