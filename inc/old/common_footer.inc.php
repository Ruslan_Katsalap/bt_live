<?php
        if($curr_url != 'house_tours')
        {
            if ( !defined('hidePageMedia') ) {
                showPageMedia();
            }
        }

        ?>
<div class="cookiebar">
	<div class="cookieinner">
	<p>We use cookies to provide you with a user-friendly, safe and effective website. Carry on browsing if you're happy with this, or find out how to manage your cookies <a href="/cookie_policy.html">here</a>.</p>
    <span class="mainouterclose"><span>x</span></span>
    <div class="clearfix"></div>
    </div>
</div>
<div class="thankyoupop">
    	<div class="thanksinner">
        	<h4>Thank you for your request.</h4>
            <p>We will contact you at your preferred time.</p>
			<a href="javascript:void(0);">Ok</a>
        </div>
    </div>

<footer class="round-2" >

<div id="dialog-request" class="dialog-request" title="">
	<div class="lightbox_inner">
      <div class="lightbox_content">
        <h3 class="callbacktitle">Request a call back</h3>
        <form id="popupform" method="post" action="">
        <div class="popformfields fullpop">
        	<label for="name"><strong>Full name</strong></label>
            <input type="text" required name="popfullname" value="" placeholder="" />
        </div>
        <div class="popformfields halfpop">
        	<label for="number"><strong>Contact Number</strong>
            </label>
            <input type="tel" required name="popnumber" value="" placeholder="" />
        </div>
        <div class="popformfields halfpop">
        	<label for="email"><strong>Email</strong></label>
            <input type="email" required name="popemail" value="" placeholder="" />
        </div>
        <div class="popformfields fullpop">
        	<label><strong>Preferred call time</strong></label>
            <!--<input class="number" name="prefred_time" type="text" />-->
            <div class="daycont">
            	<div class="pertiday">
                	<label for="mon">MON</label>
                    <input type="radio" name="day" id="mon" value="Monday" required>
                </div>
                <div class="pertiday">
                	<label for="tue">TUE</label>
                    <input type="radio" name="day" id="tue" value="Tuesday">
                </div>
                <div class="pertiday">
                	<label for="wed">WED</label>
                    <input type="radio" name="day" id="wed" value="Wednesday">
                </div>
                <div class="pertiday">
                	<label for="thu">THU</label>
                    <input type="radio" name="day" id="thu" value="Thursday">
                </div>
                <div class="pertiday">
                	<label for="fri">FRI</label>
                    <input type="radio" name="day" id="fri" value="Friday">
                </div>
               <!-- <div class="pertiday">
                	<label for="sat">SAT</label>
                    <input type="radio" name="day" id="sat" value="Saturday">
                </div>
                <div class="pertiday">
                	<label for="sun">SUN</label>
                    <input type="radio" name="day" id="sun" value="Sunday">
                </div>-->

            </div>
            <div class="timewrap">
            <div id="carousel-example-generic-1" data-interval="false" class="carousel slide Monday Tuesday Wednesday Thursday Friday" data-ride="carousel">
              <!-- Wrapper for slides -->
               <div class="timecont">
              <div class="carousel-inner" role="listbox">

            <div class="item active">
            	<!--<div class="pertime">
                	<label for="time-eight">08:00</label>
                    <input type="radio" name="time" id="time-eight" value="8:00">
                </div>-->
                <div class="pertime">
                	<label for="time-nine">09:30</label>
                    <input type="radio" name="time" id="time-nine" value="9:30" required>
                </div>
                <div class="pertime">
                	<label for="time-ten">10:00</label>
                    <input type="radio" name="time" id="time-ten" value="10:00">
                </div>
                <div class="pertime ">
                	<label for="time-ele">11:00</label>
                    <input type="radio" name="time" id="time-ele" value="11:00">
                </div>
                <div class="pertime ">
                	<label for="time-twe">12:00</label>
                    <input type="radio" name="time" id="time-twe" value="12:00">
                </div>
                <div class="pertime ">
                	<label for="time-thir">13:00</label>
                    <input type="radio" name="time" id="time-thir" value="13:00">
                </div>
                <div class="pertime ">
                	<label for="time-four">14:00</label>
                    <input type="radio" name="time" id="time-four" value="14:00">
                </div>
                <div class="pertime ">
                	<label for="time-fif">15:00</label>
                    <input type="radio" name="time" id="time-fif" value="15:00">
                </div>
                </div>
                <div class="item">

                <div class="pertime ">
                	<label for="time-sixt">16:30</label>
                    <input type="radio" name="time" id="time-sixt" value="16:30">
                </div>
                <!--<div class="pertime ">
                	<label for="time-sevent">17:00</label>
                    <input type="radio" name="time" id="time-sevent" value="17:00">
                </div>
                <div class="pertime ">
                	<label for="time-eighteen">18:00</label>
                    <input type="radio" name="time" id="time-eighteen" value="18:00">
                </div>
                <div class="pertime ">
                	<label for="time-ninetee">19:00</label>
                    <input type="radio" name="time" id="time-ninetee" value="19:00">
                </div>
                <div class="pertime ">
                	<label for="time-twent">20:00</label>
                    <input type="radio" name="time" id="time-twent" value="20:00">
                </div>-->
                </div>

            </div>
             <a class="left carousel-control" href="#carousel-example-generic-1" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only"><</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic-1" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">></span>
  </a>
            </div>

            </div>
            <div id="carousel-example-generic-2" data-interval="false"  class="carousel slide Saturday" data-ride="carousel">
              <!-- Wrapper for slides -->
               <div class="timecont">
              <div class="carousel-inner" role="listbox">

            <div class="item active">

                <div class="pertime ">
                	<label for="time-nine">09:30
                    <input type="radio" name="time" id="time-nine" value="9:30" required></label>
                </div>
                <div class="pertime ">
                	<label for="time-ten">10:00</label>
                    <input type="radio" name="time" id="time-ten" value="10:00">
                </div>
                <div class="pertime ">
                	<label for="time-ele">11:00</label>
                    <input type="radio" name="time" id="time-ele" value="11:00">
                </div>
                <div class="pertime ">
                	<label for="time-twe">12:00</label>
                    <input type="radio" name="time" id="time-twe" value="12:00">
                </div>
                <div class="pertime ">
                	<label for="time-thir">13:00</label>
                    <input type="radio" name="time" id="time-thir" value="13:00">
                </div>
                <div class="pertime ">
                	<label for="time-four">14:00</label>
                    <input type="radio" name="time" id="time-four" value="14:00">
                </div>
                <div class="pertime ">
                	<label for="time-fif">15:00</label>
                    <input type="radio" name="time" id="time-fif" value="15:00">
                </div>
                </div>
                <div class="item">

                <div class="pertime ">
                	<label for="time-sixt">16:00</label>
                    <input type="radio" name="time" id="time-sixt" value="16:00">
                </div>
               <!-- <div class="pertime ">
                	<label for="time-sevent">17:00</label>
                    <input type="radio" name="time" id="time-sevent" value="17:00">
                </div>-->


                </div>

            </div>
             <a class="left carousel-control" href="#carousel-example-generic-2" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only"><</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic-2" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">></span>
  </a>
            </div>

            </div>
            <div id="carousel-example-generic-3" data-interval="false"  class="carousel slide Sunday" data-ride="carousel">
              <!-- Wrapper for slides -->
               <div class="timecont">
              <div class="carousel-inner" role="listbox">

            <div class="item active">

                <div class="pertime ">
                	<label for="time-ten">10:00</label>
                    <input type="radio" name="time" id="time-ten" value="10:00" required>
                </div>
                <div class="pertime ">
                	<label for="time-ele">11:00</label>
                    <input type="radio" name="time" id="time-ele" value="11:00">
                </div>
                <div class="pertime ">
                	<label for="time-twe">12:00</label>
                    <input type="radio" name="time" id="time-twe" value="12:00">
                </div>
                <div class="pertime ">
                	<label for="time-thir">13:00</label>
                    <input type="radio" name="time" id="time-thir" value="13:00">
                </div>
                <div class="pertime ">
                	<label for="time-four">14:00</label>
                    <input type="radio" name="time" id="time-four" value="14:00">
                </div>
                <div class="pertime ">
                	<label for="time-fif">15:00</label>
                    <input type="radio" name="time" id="time-fif" value="15:00">
                </div>
                <div class="pertime ">
                	<label for="time-sixt">15:30</label>
                    <input type="radio" name="time" id="time-sixt" value="15:30">
                </div>
                </div>

            </div>
             <a class="left carousel-control" href="#carousel-example-generic-3" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only"><</span>
  </a>
  <a class="right carousel-control" href="#carousel-example-generic-3" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">></span>
  </a>
            </div>

            </div>
            </div>
        </div>
        <div class="popformfields fullpop">
        	<label><strong>Enquiry</strong></label>
            <div class="halfpop">
            <div class="enquirywrap">

               		<input type="radio" name="enquiry" id="sitevisit" value="Site Visit">
               <label for="sitevisit"><p>Site Visit</p> </label>
            </div>

            <div class="enquirywrap">

           		<input type="radio" name="enquiry" id="quote" value="Quote" required>
                <label for="quote"><p>Quote</p></label>
            </div>
           <div class="enquirywrap">

           		<input type="radio" name="enquiry" id="house" value="House Tour">
                <label for="house"><p>House Tour</p></label>
            </div>
            </div>
            <div class="halfpop">
            <div class="enquirywrap">

            	<input type="radio" name="enquiry" id="designserv" value="Design services">
                <label for="designserv"><p>Design Services</p></label>
            </div>
            <div class="enquirywrap">

           		<input type="radio" name="enquiry" id="buildser" value="Build services">
           	 	<label for="buildser"><p>Build Services</p></label>
            </div>
            <div class="enquirywrap">

                <input type="radio" name="enquiry" id="otherser" value="Other">
                <label for="otherser"><p>Other</p></label>
            </div>
            </div>
			
            <div class="broform100 checkboxbro broleft">
                                <input type="checkbox" name="requestSubscribe" value="Yes"><span>Sign up to our newsletter and receive updates on our latest blogs, news items and special offers. see our <a href="https://www.buildteam.com/privacy-policy.html">privacy policy</a> for more information on how we use your personal data.</span>
                            </div>
            <div class="clearfix"></div>
			<div class="enquirywrap" style="width:100%;float:left;">
               
				 <div class="g-recaptcha" data-sitekey="6LcZGKYUAAAAAHxPrL02cGYHuDzEfpUBpWCvNRgt"></div>
            </div>
			<div class="clearfix"></div>
         </div>
         <div class="popformfields">
         <div class="halfpop">
          
            </div>
         <div class="halfpop">
       		<input id="popsubmitbutton" type="submit" value="Submit" />
           </div>
       </div>
       </form>
      </div>
    </div>
</div>

<div class="up_down">
    <a  id="button-down">
        <img src="https://www.buildteam.com/images_new_design/Chevron-down2.png">
    </a>
    <a  id="button-up">
        <img src="https://www.buildteam.com/images_new_design/Chevron-up.png">
    </a>
</div>

    <div class="wrapper">



<div class="bott_menu">


<div class="top_footer">

            <div class="social">
                <a target="_blank" href="https://www.instagram.com/buildteamldn/" class="insta">
                </a>

                <a target="_blank" href="https://www.facebook.com/Build-Team-391933624520838/" class="fb">
                </a>

                <a target="_blank" href="https://twitter.com/BuildTeamLDN" class="twi">
                </a>

                <a target="_blank" href="https://uk.pinterest.com/buildteam/" class="pinterest">
                </a>
            </div>

            <!-- Begin MailChimp Signup Form -->
            <!-- <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css"> -->

            <div id="mc_embed_signup">
                <!--<form action="https://tickledpet.us14.list-manage.com/subscribe/post?u=0477c5b0ee37e35f5e572f946&amp;id=cb22113f6b" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>-->
				<form action="https://buildteam.us7.list-manage.com/subscribe/post?u=2460f4828cafef8bf4e6ec3d1&amp;id=292211ee50" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
						<div class="col-md-5" style="padding-left:  0;padding-right:  0">
							<p style="padding-left: 0;padding-right: 0;padding-top:  14px;line-height: 16px;">SIGN UP FOR OUR NEWSLETTER</p>
						</div>
						<div class="col-md-4" style="padding-left:  0;">
                        <div class="mc-field-group">

<input type="email" value="" name="EMAIL" placeholder="Enter Email" class="required email" id="mce-EMAIL">
</div>
		</div>
		<div class="col-md-2">

<input type="submit" value="SUBMIT" name="subscribe" id="mc-embedded-subscribe" class="book-now" style="border: none;color: #fff;padding: 10px 18px;background: #0f4778;font-size: 14px;border-radius: 10px;margin-left: 10px;;">
</div>
</div>
<div id="mce-responses" class="clear">
    <div class="response" id="mce-error-response" style="display:none;"></div>
    <div class="response" id="mce-success-response" style="display:none;"></div>
</div>
<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_0477c5b0ee37e35f5e572f946_cb22113f6b" tabindex="-1" value=""></div>
</form>
</div>
<script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='FNAME';ftypes[1]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
<!--End mc_embed_signup-->

</div>
<div class="clearfix"></div>



    <ul>
        <a href="javascript:void(0);" class="clickexpand collapsednav" style="text-transform: uppercase;">About Us</a>

        <li><a href="https://www.buildteam.com/about-us/our_story.html">Our Story</a></li>
        <li><a href="https://www.buildteam.com/about-us/why-choose-page.html">Why Choose us</a></li>
        <li><a href="https://www.buildteam.com/about-us/service-level-agreement.html">Service Level Agreement</a></li>
        <li><a href="https://www.buildteam.com/about-us/testimonials.html">Testimonials</a></li>
        <li><a href="https://www.buildteam.com/about-us/greener-living-page.html">Greener Living</a></li>
        <li><a href="https://www.buildteam.com/about-us/cover_areas.html">Areas We Cover</a></li>
        <li><a href="https://www.buildteam.com/about-us/house_tours.html">House Tours</a></li>
       <!-- <li><a href="https://www.buildteam.com/getting-started/how-it-works.html">How It Works</a></li>-->
        <li><a href="https://www.buildteam.com/our-team/meet-the-team.html">Meet the Team</a></li>

    </ul>


    <ul>
        <a href="javascript:void(0);" class="clickexpand collapsednav" style="text-transform: uppercase;">Design</a>
        <li><a href="https://www.buildteam.com/pricing.html">Pricing</a></li>
        <li><a href="https://www.buildteam.com/getting-started/meet-with-us.html">Meet With Us</a></li>
        <li><a href="https://www.buildteam.com/getting-started/pre-purchase-feasibility.html">Pre Purchase Feasibility</a></li>
        <li><a href="https://www.buildteam.com/premium_site_visit.html">Premium Site Visit</a></li>

        <li><a href="https://www.buildteam.com/getting-started/nationwide_design_service.html">Nationwide Design Service</a></li>
        <li><a href="https://www.buildteam.com/what-we-do/the_design_phase.html">Architectural Design Phase</a></li>
        <!--<li><a href="https://www.buildteam.com/what-we-do/design-options.html">Design Options</a></li>-->
        <li><a href="https://www.buildteam.com/design_doubledesign.html">Side Return & Loft Package</a></li>
        <li><a href="https://www.buildteam.com/why_choose_a_specialist.html">Why Choose a Specialist?</a></li>

        <li><a href="/design_bundles.html">Design Bundles</a></li>
        <!--<li><a href="/design-bundles.html">Design Bundles</a></li>-->

    </ul>

    <ul>
        <a href="javascript:void(0);" class="clickexpand collapsednav" style="text-transform: uppercase;">Build</a>

        <li><a href="https://www.buildteam.com/what-we-do/the_build_phase.html">The Build Phase</a></li>
        <li><a href="https://www.buildteam.com/insurance_and_guarantee.html">10 Year Guarantee</a></li>
        <li><a href="https://www.buildteam.com/what-we-do/faq.html">FAQ's</a></li>
        <!--<li><a href="https://www.buildteam.com/what-we-do/process.html">The Process</a></li>-->
        <li><a href="https://www.buildteam.com/build_bundles.html" data-description="">Build Bundles</a></li>
    </ul>
    <ul>
        <a href="javascript:void(0);" class="clickexpand collapsednav" style="text-transform: uppercase;">Knowledge Base</a>

        <li><a href="https://www.buildteam.com/blog/">Blog</a></li>
        <li><a href="https://www.buildteam.com/online_quote_calculator.html">Instant Online Quote</a></li>
        <li><a href="https://www.buildteam.com/what-we-do/3d-visualisation-service-page.html">3D Visualisation Service</a></li>
        <li><a href="https://www.buildteam.com/news.html">Latest News</a></li>
        <li><a href="https://www.buildteam.com/news/press_releases.html">Press Coverage</a></li>
        <li><a href="https://www.buildteam.com/our-team/join-our-team.html">Join our Team</a></li>

    </ul>
    <ul>
        <a href="https://www.buildteam.com/project-gallery/gallery.html" class="clickexpand collapsednav" style="text-transform: uppercase;">Gallery</a>

        <!--<li><a href="https://www.buildteam.com/project-gallery/projects-side-return-extensions.html">Kitchen Extensions</a></li>
        <li><a href="https://www.buildteam.com/project-gallery/projects-loft-conversions.html">Loft Conversions</a></li>
        <li><a href="https://www.buildteam.com/project-gallery/tv.html">Build Team TV</a></li>
        <li><a href="https://www.buildteam.com/project-gallery/projects-refurbishment.html">Refurbishment Projects</a></li>-->
		<img src="https://www.buildteam.com/images/payment_black_white.jpg" />
    </ul>
</div>
</div>
<div class="copyright">
 <ul>
    <li>342 Clapham Road  </li>
    <li>London</li>
    <li>SW9 9AJ</li>
    <li>Tel: 020 7495 6561</li>
    <li><a href="https://www.buildteam.com/terms-and-conditions.html">Terms & Conditions</a></li>
    <li><a href="https://www.buildteam.com/privacy-policy.html">Privacy Policy</a></li>
    <li><a href="https://www.buildteam.com/cookie_policy.html">Cookie Policy</a></li>
    <li><a href="https://www.buildteam.com/sitemap.html">Sitemap</a></li>

</ul>
<ul>
    <li>© <?php echo date("Y"); ?></li>
    <li>All Rights Reserved - Build Team (Holdings) Ltd</li>
</ul>
</div>
</footer>
<script type="text/javascript" src="/js/miscjs.js"></script>
<script>
$(document).ready(function(e) {
		if($.cookie("cookiebar") != 'seen'){
			 // Set it to last a year, for example.
			setTimeout(function() {
				$(".cookiebar").fadeIn(300);
				//$(".autopopup").animate({"opacity":1})
			}, 3000);


		};
		$(".mainouterclose").click(function(){
				$(".cookiebar").fadeOut(300);
				$.cookie("cookiebar", "seen", { expires: 7 });
				});

});
</script>

<script>
$(document).ready(function(e) {
    <? if($flage == true){?>
$(".thankyoupop").fadeIn(200);
<? } ?>
});

</script>
<script>
$(document).ready(function(e) {
    <? if($flagenew == true){?>
$(".outsidepoup").fadeIn(200);
<? } ?>
});

</script>
<script>
$(document).ready(function(e) {
    <? if($newflaging == true){?>
$(".bookthanks").fadeIn(200);
<? } ?>
});

</script>
<?php if ('/book-visit.php'==$_SERVER['PHP_SELF']) : ?>

<script src="https://www.buildteam.com/js/jquery-ui.js"></script>
<script src="https://www.buildteam.com/js/jquery-ui.multidatespicker.js?v=1"></script>
<script src="https://www.buildteam.com/js/book-visit.js?v=1"></script>
<script type="text/javascript">
  <?php if (isset($a_pref_dates) && $a_pref_dates): ?>
  DesignTeam.setPrefDates(<?php echo $_POST['availibility'] ?>);
  <?php endif; ?>
  jQuery(document).ready(DesignTeam.initBookCal);
</script>
<?php endif; ?>
<script>
	$(document).ready(function(e) {
        var d = new Date();
		var n = d.getDay();
		var gt = d.getHours() + "." + d.getMinutes();
		console.log(gt);
		if (n == 6) {
			$(".automatedbanner").css("display","inherit");
			$(".SliderLayered").addClass("SliderLayeredtop");

			}

		else if (n == 0) {
			if (gt <= "15.30") {

				$(".automatedbanner").css("display","inherit");
				$(".SliderLayered").addClass("SliderLayeredtop");

				}
			//var gt = n.getTime();
			//if (gt == )
			}
		else {
			$(".automatedbanner").css("display","none");
			$(".SliderLayered").removeClass("SliderLayeredtop");
			}
    });

		//document.getElementById("demo").innerHTML = n;

	//setInterval(loop, 10000);
</script>
<?php if ('/book-confirm.php'==$_SERVER['PHP_SELF']) : ?>
<script src="https://www.buildteam.com/js/book-visit.js?v=1"></script>
<script type="text/javascript">
  jQuery(document).ready(DesignTeam.initCouponCode);
</script>
<?php endif; ?>

<?php if ('buildteam.ua'!=$_SERVER['HTTP_HOST']) : ?>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "https://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<?PHP /*<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5628250-1");
pageTracker._trackPageview();
} catch(err) {}</script> */?>
<?php if ('/brochure_flipbook.php'!= $_SERVER['PHP_SELF']) ?>
<!-- Piwik -->
<script type="text/javascript">
  var _paq = _paq || [];
  _paq.push(["setDomains", ["buildteam.com", "www.buildteam.com"]]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  _paq.push(["setCookieDomain", "*.buildteam.com"]);
  (function() {
    var u=(("https:" == document.location.protocol) ? "https" : "http") + "://buildteam.com/inc/piwik/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', 1]);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
    g.defer=true; g.async=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();

  var theToggle = document.getElementById('toggle');

		// based on Todd Motto functions
		// https://toddmotto.com/labs/reusable-js/

		// hasClass
		function hasClass(elem, className) {
			return new RegExp(' ' + className + ' ').test(' ' + elem.className + ' ');
		}
		// addClass
		function addClass(elem, className) {
			if (!hasClass(elem, className)) {
				elem.className += ' ' + className;
			}
		}
		// removeClass
		function removeClass(elem, className) {
			var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, ' ') + ' ';
			if (hasClass(elem, className)) {
				while (newClass.indexOf(' ' + className + ' ') >= 0 ) {
					newClass = newClass.replace(' ' + className + ' ', ' ');
				}
				elem.className = newClass.replace(/^\s+|\s+$/g, '');
			}
		}
		// toggleClass
		function toggleClass(elem, className) {
			var newClass = ' ' + elem.className.replace( /[\t\r\n]/g, " " ) + ' ';
			if (hasClass(elem, className)) {
				while (newClass.indexOf(" " + className + " ") >= 0 ) {
					newClass = newClass.replace( " " + className + " " , " " );
				}
				elem.className = newClass.replace(/^\s+|\s+$/g, '');
			} else {
				elem.className += ' ' + className;
			}
		}

		if(typeof theToggle != "undefined" && theToggle != null) {
			theToggle.onclick = function() {
			   toggleClass(this, 'on');
			   return false;
			}
		}

</script>

<noscript><p><img src="https://buildteam.com/inc/piwik/piwik.php?idsite=1" style="border:0" alt="" /></p></noscript>
<!-- End Piwik Code -->
<?php endif; ?>
<?php if ('/book-site-visit.php'!= $_SERVER['PHP_SELF'] && '/book-visit.php'!=$_SERVER['PHP_SELF']) : ?>
<!--<script type="text/javascript">
	$(document).ready(function(e) {
		if($.cookie("popup") != 'seen'){
			 // Set it to last a year, for example.
			setTimeout(function() {
				$(".autopopup").css("display","table")
				$(".autopopup").animate({"opacity":1})
			}, 10000);


		};
		$(".popupclose").click(function(){
				$(".autopopup").css("display","none");
				$.cookie("popup", "seen", { expires: 7 });
				});
		$(".popuptext a").click(function(){
				$.cookie("popup", "seen", { expires: 7 });
				});
});
</script> -->
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function(){
		//Check to see if the window is top if not then display button
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
		if ($(window).width() < 640) {
		$("#byp_slide_box ul li").width($(window).width());
		}
	});
</script>
<script>
$(document).ready(function(e) {

	$(".bott_menu ul .clickexpand").click(function(){

		var getheight = $(this).parent('ul').children('li').length * 28;
		//alert(getheight);
		if ($(this).hasClass('collapsednav')) {
				$(this).parent("ul").animate({height:getheight},200);
				$(this).removeClass('collapsednav');

			}
		else {
				$(this).parent("ul").animate({height:25},200);
				$(this).addClass('collapsednav');

			}

		});


});
</script>
<!--<script src="https://www.buildteam.com/js/jquery-3.1.1.min.js"></script>-->
<script async src="https://www.buildteam.com/js/jquery.corner.js"></script>

<script>
$('.dropNav').parent('li').click(function(e) {
  if(!_widthResize){
    $(this).children('.dropNav').stop(true, true).slideToggle(300);
    //e.preventDefault();
  }
});
(function($){
  $.fn.extend({

    sideNav : function(){
		$("#closePageslide").hide();

      if( $('.pageslideBg').length == 0 ) {
            $('<div />').attr( 'class', 'pageslideBg' ).appendTo( $('body') );
        }
        var $btn = $(this),
			$pageslide = $($btn.attr("href")),
			$pageslideBg = $('.pageslideBg'),

			_width = $pageslide.width();
		$btn.click(function(){
			$pageslideBg.show();
			$("#closePageslide").show();
			$(this).hide();
			$pageslide.css({ 'display':'block'}).animate({'left':0 });
        	return false;
      		});
      $pageslideBg.click(function() {
        $(this).hide();
        $pageslide.animate({'left': _width*-1 + 'px' },function(){
          $(this).hide();
		  $("#openPageslide").show();
		  $("#closePageslide").hide();
        });

        return false;
      });
	 $("#closePageslide").click(function() {
		 $(this).hide();
			$("#openPageslide").show();
		$pageslide.animate({'left': _width*-1 + 'px' },function(){
          $(this).hide();
		  $pageslideBg.hide();
        });

		 return false;

		});

      return this;
    }

  });
})(jQuery);

$("#openPageslide").sideNav();
</script>

<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>-->
<?php if ('/byp_latest.php' == $_SERVER['PHP_SELF'] || '/build-your-price-app.php' == $_SERVER['PHP_SELF']): ?>
<script type="text/javascript" src="/js/byp.js?v=106"></script>
<?php endif; ?>
<?php if ('/byp_landing.php' == $_SERVER['PHP_SELF']): ?>
<script type="text/javascript" src="/js/new-byp.js"></script>
<?php endif; ?>
<?php if ('/build-your-price-app-akash.php' == $_SERVER['PHP_SELF']): ?>
<script type="text/javascript" src="/js/new-byp.js"></script>
<?php endif; ?>
<script src="https://www.buildteam.com/js/main.js"></script>
<script async src="https://www.buildteam.com/js/cbpHorizontalMenu.min.js"></script>
<!-- bxSlider Javascript file -->
<script src="https://www.buildteam.com/js/jquery.bxslider.min.js"></script>

<!-- bxSlider CSS file -->
<link href="https://www.buildteam.com/css/jquery.bxslider.css" rel="stylesheet" />

        <script>
            $(function() {
                //cbpHorizontalMenu.init();
            });
        </script>

        <script type="text/javascript">
            $(document).ready(function(){
				var controls = document.getElementById("project-gallery") != null ? true : false;
				$('.bxslider').bxSlider({
					auto: true,
					controls: controls
				  });
			});
        </script>
		<script>
			$(document).ready(function(e) {
			  $("#contactdrop").hover(function(e) {
				  //alert();
					$("#cbp-hrmenu .qwe").each(function(index, element) {

						//alert($(this).attr("class"));
						$(this).removeClass("cbp-hropen");
                    });
				});
			});
        </script>
        <script type="text/javascript">


            var delay = 1000; // ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ´ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¶ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ° ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸

            $('#button-up').click(function () { // ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµ "ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ" ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ´ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ² ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ
                /* ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ° ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ */
                $('body, html').animate({
                    scrollTop: 0
                }, delay);
            });
            $('#button-down').click(function () { // ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµ "ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ" ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ´ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ² ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ
                /* ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¿ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ° ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ */
               $("html, body").animate({ scrollTop: $(document).height() }, "slow");
                return false;
            });


        </script>
        <script type="text/javascript">
        jQuery(document).ready(function($){

            /* ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ´ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ³ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¾ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ */
            $('#nav-wrap').prepend('<div id="menu-icon"><img style="width:32px; height:32px;" src="https://s3-eu-west-1.amazonaws.com/weld-tutorials/getting-started/icon-hamburger-light.svg"></div>');

            /* ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂºÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ»ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂµÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¼ ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ½ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ²ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ³ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ°ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¸ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ */
            $("#menu-icon").on("click", function(){
                $(this).toggleClass("active");
                $("#nav").slideToggle();
            });

        });
        </script>
        <script>
        	$(document).ready(function(e) {
                // lightbox for dialog popup
			jQuery('.dialog').click(function (e) {
			  e.preventDefault();

			  var id = $(this).attr('id');

			  id = id.replace("_link", "");
			  id += "_dialog";

			  $.lightbox({
				content: document.getElementById(id).innerHTML
			  });
			});
            });
        </script>
        <script>
        $(function(){

							$(".ui-datepicker-calendar tr").each(function(index, element) {
								 $($(this).children('td')).each(function(index, element) {
                                    if (!$(this).hasClass('ui-state-disabled')) {
										var getchildern = $(this).children('a').text();
										console.log(getchildern);
										if ($.cookie('fromdate') == getchildern) {

											$(this).addClass('dt_pick1 ui-state-highlight');
											}
										else if ($.cookie('gettodate') == getchildern)	{
											$(this).addClass('dt_pick2 ui-state-highlight');
											}
									}

                                });
                            });

							});
        </script>
<script>
$("#cbp-hrmenu .qwe").hover(function(){
	$(".cbp-hrmenu .cbp-hrsub").css("background", "rgba(255,255,255,1)");
	$(".top_part").css("background", "rgba(255,255,255,1)");
	});
$("#cbp-hrmenu").mouseleave(function(){
	$(".cbp-hrmenu .cbp-hrsub").css("background", "rgba(255,255,255,0.9)");
	$(".top_part").css("background", "rgba(255,255,255,0.9)");
	});
</script>
<script>
$(document).ready(function(e) {
    var date = new Date();
	var nextdate;
if(date.getDay() == 2) {
	console.log("tuesday");
	nextdate = 3;
	date.setDate(date.getDate() + 3);
	}
else if(date.getDay() == 3) {
	console.log("Wed");
	nextdate = 3;
	date.setDate(date.getDate() + 2);
	}
else if(date.getDay() == 4) {
	console.log("Wed");
	nextdate = 2;
	date.setDate(date.getDate() + 4);
	}
else if(date.getDay() == 5) {
	nextdate = 2;
	date.setDate(date.getDate() + 4);
	}
else if(date.getDay() == 6) {
	nextdate = 2;
	date.setDate(date.getDate() + 3);
	}
else if(date.getDay() == 0) {
	nextdate = 2;
	date.setDate(date.getDate() + 3);
	}
else {
	nextdate = 2;
	date.setDate(date.getDate() + 2);
	}


var dateMsg = ("0" + date.getDate("00")).slice(-2);

var dateMsg2 = date.getDate('DD');
var datetwo = dateMsg2 + nextdate;
var trimmeddate = ("0" + datetwo).slice(-2);
console.log(dateMsg2)
var monthMsg = date.getMonth()+1;
if (dateMsg <= 31) {
$(".date1").html(dateMsg);
//$(".date1").html("29");
	if (dateMsg2 + nextdate >= 31) {
		$(".date2").html("0" + (trimmeddate) - 30);
		//$(".date2").html("01");
	}
	else {
		$(".date2").html(trimmeddate);
		//$(".date2").html("30");
		}
}

var months = ['Jan','Feb','March','Apr','May','June','July','Aug','Sep','Oct','Nov','Dec'];
var nowmonth = new Date();
var checkdate = nowmonth.getDate()
//console.log(checkdate);
//console.log("ok");
if (checkdate >= 29 ) {
	//console.log("ok")
var thisMonth = months[nowmonth.getMonth() + 1];
var thisMonth2 = months[nowmonth.getMonth() + 1]; // getMonth method returns the month of the date (0-January :: 11-December)
}
else if (checkdate >= 27) {
	console.log("ok");
	var thisMonth2 = months[nowmonth.getMonth() + 1];
	var thisMonth = months[nowmonth.getMonth()];
	console.log(thisMonth2);
	}
else {
var thisMonth = months[nowmonth.getMonth()];
var thisMonth2 = months[nowmonth.getMonth()];
//console.log(thisMonth);
}
$(".dateinline .month1").text(thisMonth);
$(".dateinline .month2").text(thisMonth2);
});


</script>
<script>
$(document).ready(function(e) {
	var today = new Date();
   var days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
   var day = days[today.getDay()];
   var getime = today.getHours();
   //alert(getime);
	//First of month
	//days[firstOfMonth.getDay()];
	//last of month
	//days[lastOfMonth.getDay()];
	//alert(day);
	$(".daytimewrap .dayinline").each(function(index, element) {
        var getdays = $(this).children(".contactday").text();
		//alert(getdays);
		if (getdays == day) {
			$(this).addClass("today");
			var gethour = $(this).find("span").text();
			var getclose = gethour - 12;
			$(".contactblock #openuntil").text(gethour - 12);
			$("#contactdrop h4 span").text(gethour - 12);

			if (day == 'Monday' || day == 'Tuesday' || day == 'Wednesday' || day == 'Thursday') {
				//alert(getime);
				if (getime < '8') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("8");
					$(".closetext #menuopentomo").text("8");
					}

				else if (getime >= '20') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("8");
					$(".closetext #menuopentomo").text("8");
					}
			}
			else if (day == 'Friday'){
				//alert(getime);
				if (getime < '8') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("8");
					$(".closetext #menuopentomo").text("8");
					}
				else if (getime >= '20') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("9");
					$(".closetext #menuopentomo").text("9");
					}
				}
			else if (day == 'Saturday'){
				if (getime < '9') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("9");
					$(".closetext #menuopentomo").text("9");
					}
				else if (getime >= '17') {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("10");
					$(".closetext #menuopentomo").text("10");
					}
				}

			else if (day == 'Sunday'){
				//alert(getime);
				if (getime < 10) {
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("10");
					$(".closetext #menuopentomo").text("10");
					}

				else if (getime >= '16') {
					//alert(getime);
					$(".opentext").hide();
					$(".closetext").show();
					$(".closetext #opentomo").text("8");
					$(".closetext #menuopentomo").text("8");
					}
				}



			}
    });
});
</script>
<script>
 $(document).ready(function(e) {
	 //alert();
    //$("ul.news").css("right", "0px");
});
 </script>
<script>

/*$('.dialog-request #popupform .carousel').carousel({
    interval: false,
}); */
$(document).ready(function(e) {

});
</script>
<script>

</script>
<script>
 $(document).ready(function(e) {
	 //alert();
	var getwidth = $(".news").height();
	$(".catfilter li:first").addClass("active");
	//var setwidth = $(".news").length * getwidth;
	$(".data-post-wrap").height(getwidth);
    $(".news").each(function(index, element) {
        var get2017 = $(this).attr("data-post");
		// console.log(get2017);
		if (get2017 == "2019") {
			$(this).css("right", "-20px");
			$(this).addClass("active");
			}
		else {
			$(this).css("right", "-1020px");
			}
    });
		$(".catfilter li").click(function(e){
			//e.preventDefault();
		var getyear = $(this).attr("data-year");
		//alert(getyear);
		var getwidth = $(".news.active").height();
		//var setwidth = $(".news").length * getwidth;
		$(".catfilter li").removeClass("active");
		$(this).addClass("active");
		//alert(getyear);
		$(".news").each(function(index, element) {
			var getpost = $(this).attr("data-post");
			if (getpost == getyear) {
				//$(this).show("slide", { direction: "right" }, 1200);
				//e.stopPropagation();
				$(this).css("right", "-20px");
				$(this).addClass("active");
				getwidth = $(".news.active").height();
				$(".data-post-wrap").height(getwidth);
				}
			else {
				$(this).css("right", "-1020px");
				$(this).removeClass("active");
				}
			});
		});
});
 </script>
<script>
$(document).ready(function(e) {
    $(".dialog-request .timewrap .slide:first-child").show();
	$('.dialog-request .daycont input').on('change', function() {
		$('.dialog-request .daycont label').css("color","#75787f");
		$(".dialog-request .timewrap .slide").hide();
		var getvalue = $(this).val();
		//alert(getvalue);
		$('.dialog-request input[name=day]:checked').parent(".pertiday").children("label").css("color","#f5641e");
		$(".dialog-request .timewrap .slide").each(function(index, element) {
		 if($(this).hasClass(getvalue)) {
			$(this).show();
			}
});
});
$(document.body).on('click', '.dialog-request .timecont .pertime label' ,function(){

	//alert();
//var savethis = 	$(this).parent(".pertime").find("label");
   $('.dialog-request .pertime label').css("color","#75787f");
   $(this).css('color', '#f5641e');
   //alert($(this).parent(".pertime").find("label").text());
   //alert()
});
});

</script>

<script>
$(".thanksinner a").click(function(){
	$(".thankyoupop").fadeOut(200);
	});
</script>
<script>
$(".outsideinner a").click(function(){
	$(".outsidepoup").fadeOut(200);
	});
</script>
<script>
$(".bookthanksinner a").click(function(){
	$(".bookthanks").fadeOut(200);
	});
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
    <script>
		 var CaptchaCallback = function() {
        $('.g-recaptcha').each(function(index, el) {
          grecaptcha.render(el, {'sitekey' : $(el).data('6Lc8QigUAAAAAEUyusx4BYsgGA5cnlynDk-Ni-ML'), 'expired-callback': expCallback});
        });
      };
      var expCallback = function() {
          grecaptcha.reset();
       };
	/*var verifyCallback = function(response) {
        alert(response);
      };
      var recaptcha1;
      var recaptcha2;
	  var recaptcha3;
      var myCallBack = function() {
        //Render the recaptcha1 on the element with ID "recaptcha1"
       if ( $('#recaptcha1').length ) {
        grecaptcha.render('recaptcha1', {'sitekey' : '6Lc8QigUAAAAAEUyusx4BYsgGA5cnlynDk-Ni-ML'
        });
		}
		if ( $('#recaptcha2').length ) {
		   grecaptcha.render('recaptcha2', {'sitekey' : '6Lc8QigUAAAAAEUyusx4BYsgGA5cnlynDk-Ni-ML'
		   });
		}
		if ( $('#recaptcha3').length ) {
		   grecaptcha.render('recaptcha3', {'sitekey' : '6Lc8QigUAAAAAEUyusx4BYsgGA5cnlynDk-Ni-ML'
		   });
		}


      };*/
    </script>
<script>
/*$(".newcontentpush .qwe").hover(


	function() {
		var getheight = $(this).children(".cbp-hrsub").height();
	  $(".header").css("padding-bottom",getheight);

    },
    function() {
	$(".header").css("padding-bottom","0px");

    }
);*/
</script>


<script type='text/javascript' src="/mobilemenu/modernizr-custom.min.js"></script>
<script type='text/javascript' src='/mobilemenu/core.min.js'></script>
<script type='text/javascript' src='/mobilemenu/plugins.js'></script>
<script type='text/javascript' src='/mobilemenu/menumain.js'></script>
</body>

</html>