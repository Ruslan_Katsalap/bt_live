<div class="FooterSection">
	<div>
       Kitchen Extensions | Side Return Extensions | Side Return Extension Ideas | Kitchen Side Return Extension | Design and Build Extension | Side Return and Loft Conversion | Side Return Costs | Basement Builder | Building Refurbishment London | Side Return Kitchen Extension | Side Return Ideas | Victorian Side Return Extension
    </div>
</div><div class="clear"></div>
</div>
<div id="footer2">
<div class="FooterSection">
	<div>
		<img src="/images/groups/velux.jpg" alt="Velux Certified Installer" style="float:left;margin-left:10px" />
		<img src="/images/groups/fmb.jpg" alt="Federation Of Master Builders FMB" style="float:left;margin-left:8px" />
		<img src="/images/groups/10year.jpg" alt="10 years Guarantee" style="float:left;margin-left:8px" />
	</div>
	<div>
		342 Clapham Road | London | SW9 9AJ | Tel: 020 7495 6561 | <a href="/terms-and-conditions.html" title="Terms &amp; Conditions">Terms &amp; Conditions</a> | <a href="/privacy-policy.html" title="Privacy Policy">Privacy Policy</a> | <a href="/sitemap.html" title="Sitemap">Sitemap</a><br/>
		&copy; <?php echo date('Y', time()); ?> | All Rights reserved - Build Team (Holdings) Ltd
	</div>
</div>
  <div class="clear"></div>
 
</div>
<div id="autopopup" class="autopopup">
	<div class="popupcontent">
    	<div class="pupupwrap">
    	<div class="popupleft">
        	<img src="../images/popup/buildteam-popup.jpg" alt="">
        </div>
        <div class="popupright">
        	<div class="popupclose">
            	<img src="../images/popup/close-popup.jpg" alt="Close">
            </div>
        	<div class="popuptext">
            	<h2>Book a Site Visit</h2>
                <p>Meet a member of our Architectural Design Team at your property to discuss your plans and ideas</p>
                <a href="/getting-started/book-site-visit.html">Click Here</a>
            </div>
            <div class="bottomlogo">
            	<img src="../images/popup/buildteam-logo.jpg" alt="">
            </div>
        </div>
        </div>
    </div>
</div>
  </div></div>

<?php if ('/book-visit.php'==$_SERVER['PHP_SELF']) : ?>
<script src="http://www.designteam.co.uk/js/jquery-ui.min.js"></script>
<script src="/js/jquery-ui.multidatespicker.js?v=1"></script>
<script src="/js/book-visit.js?v=1"></script>
<script type="text/javascript">
  <?php if (isset($a_pref_dates) && $a_pref_dates): ?>
  DesignTeam.setPrefDates(<?php echo $_POST['availibility'] ?>);
  <?php endif; ?>
  jQuery(document).ready(DesignTeam.initBookCal);
</script>
<?php endif; ?>
<script>
	$(document).ready(function(e) {
        var d = new Date();
		var n = d.getDay();
		var gt = d.getHours() + "." + d.getMinutes();
		console.log(gt);
		if (n == 6) {
			$(".automatedbanner").css("display","inherit");
			$(".SliderLayered").addClass("SliderLayeredtop");
			
			}
			
		else if (n == 0) {
			if (gt <= "15.30") {
				
				$(".automatedbanner").css("display","inherit");
				$(".SliderLayered").addClass("SliderLayeredtop");
				
				}
			//var gt = n.getTime();
			//if (gt == )
			}	
		else {
			$(".automatedbanner").css("display","none");
			$(".SliderLayered").removeClass("SliderLayeredtop");
			}
    });
			
		//document.getElementById("demo").innerHTML = n;
	
	//setInterval(loop, 10000);
</script>
<?php if ('/book-confirm.php'==$_SERVER['PHP_SELF']) : ?>
<script src="/js/book-visit.js?v=1"></script>
<script type="text/javascript">
  jQuery(document).ready(DesignTeam.initCouponCode);
</script>
<?php endif; ?>

<?php if ('buildteam.ua'!=$_SERVER['HTTP_HOST']) : ?>

<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5628250-1");
pageTracker._trackPageview();
} catch(err) {}
</script>
<!-- active campaign -->
<script type="text/javascript">
(function(e,t,o,n,p,r,i){e.visitorGlobalObjectAlias=n;e[e.visitorGlobalObjectAlias]=e[e.visitorGlobalObjectAlias]||function(){(e[e.visitorGlobalObjectAlias].q=e[e.visitorGlobalObjectAlias].q||[]).push(arguments)};e[e.visitorGlobalObjectAlias].l=(new Date).getTime();r=t.createElement("script");r.src=o;r.async=true;i=t.getElementsByTagName("script")[0];i.parentNode.insertBefore(r,i)})(window,document,"https://diffuser-cdn.app-us1.com/diffuser/diffuser.js","vgo");
vgo('setAccount', '1001345068');
vgo('setTrackByDefault', true);
vgo('process');
</script>
<?php endif; ?>
<?php if ('/book-site-visit.php'!= $_SERVER['PHP_SELF'] && '/book-visit.php'!=$_SERVER['PHP_SELF']) : ?>
<script type="text/javascript">
	$(document).ready(function(e) {
		if($.cookie("popup") != 'seen'){
			 // Set it to last a year, for example.
			setTimeout(function() {
				$(".autopopup").css("display","table")
				$(".autopopup").animate({"opacity":1})
			}, 10000);
			
			
		};
		$(".popupclose").click(function(){
				$(".autopopup").css("display","none");
				$.cookie("popup", "seen", { expires: 7 });
				});
		$(".popuptext a").click(function(){
				$.cookie("popup", "seen", { expires: 7 });
				});		
});
</script>
<?php endif; ?>
<script type="text/javascript">
$(document).ready(function(){
		//Check to see if the window is top if not then display button
		$(window).scroll(function(){
			if ($(this).scrollTop() > 100) {
				$('.scrollToTop').fadeIn();
			} else {
				$('.scrollToTop').fadeOut();
			}
		});
		//Click event to scroll to top
		$('.scrollToTop').click(function(){
			$('html, body').animate({scrollTop : 0},800);
			return false;
		});
		if ($(window).width() < 640) {
		$("#byp_slide_box ul li").width($(window).width());
		}
	});
</script>
<script type="text/javascript" src="/js/jcarousellite_1.0.1b.js"></script>
<script type="text/javascript" src="/js/byp-innctech.js?v=106"></script>
</body>

</html>