<?php

error_reporting (0); // real mode
//error_reporting (E_ALL); // real mode
//ini_set('display_errors', 'On');
umask(0);
ini_set('session.save_handler', 'files');
ini_set('session.save_path', __DIR__ . '/../tmp-sessions');

//echo(ini_get('display_errors'));

$path = str_replace('\\', '/', dirname(__FILE__));

define('WISE_INC_PATH',$path.'/');

$path = explode('/',$path);
$inc = array_pop($path);

define('WISE_PATH', implode('/',$path).'/');

if(is_int(strpos($_SERVER['REQUEST_URI'], "design_planning_database"))){
  //include WISE_INC_PATH.'config2.php';
  include WISE_INC_PATH.'config.php';
}
else{
  include WISE_INC_PATH.'config.php';
}
//include WISE_INC_PATH.'config.php';

if (is_int(strpos($_SERVER['REQUEST_URI'], $_SERVER['PHP_SELF'])) && !strpos($_SERVER['REQUEST_URI'], 'index.php') && !is_int(strpos($_SERVER['REQUEST_URI'], 'PaymentForm.php'))) {
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: /'.WISE_RELURL.'index.php');
  exit;
}

$page = str_replace('/'.WISE_RELURL, '', $_SERVER['REQUEST_URI']);
$page = str_replace('../', '', $page); // restrict include from upper dir level (allow from nested subdirs only)
$page = preg_replace('/\?.+$/', '', $page);
$page = preg_replace('/^(inc|phpthumb|media|pdf|project_photos|team_photos)\//', '', $page); // restrict inc/ library subdir from pages including, phpthumb/ and upload dirs
//echo $page.'!!!!'.print_r($_GET,1);#debug

$special_pages = array(
    'kitchen-extension-design', 'studio-page', 'home-extension-design', 'loft-conversion-Ideas',
    'on-site-support'
);
if (strpos($page, '-') && !in_array($page, $special_pages)) {
  $a_page = explode('-', $page);
  $page = array_pop($a_page);
  $_GET['id'] = implode('-', $a_page);
}


$last_symbol = substr($page, -1);
if ('/' == $last_symbol) {
  $page = substr($page, 0, -1);
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: /'.WISE_RELURL.(isset($_GET['id'])?$_GET['id'].'-':'').$page);
  exit;
}

define('WISE_PAGE', $page);

//echo WISE_PAGE.'!!!';#debug

@session_start();

// Database functions

$link_id = 0;

function connectDB() {
  global $link_id;
  $link_id=mysql_connect(WISE_DB_HOST, WISE_DB_USER, WISE_DB_PWD);
  if (!$link_id) die("Can't connect to the DB server");
  if (!mysql_select_db(WISE_DB_NAME, $link_id)) die("Can't select the ".WISE_DB_NAME.' database!');
}

// DB quote - addslashes - escape string
function q($s) {
  global $link_id;
  if (!$link_id) connectDB();
  return mysql_real_escape_string((string)$s);
}

// functions area
function DbTbl($name,$mod_name='') {
  return WISE_DB_PREFIX.strtolower(($mod_name?$mod_name.'_':'').$name);
}

function query($sql, $link_id = false) {
  global $link_id;
  static $utf8fix;
  
  if (!$link_id) connectDB();
  
  if (!isset($utf8fix)) {
    // UTF-8 fix
    $sql0 = "SET NAMES 'utf8'";
    //echo $sql0.'<br/>';
    if ($link_id) $res = @mysql_query($sql0, $link_id);
    else $res = @mysql_query($sql0);
    $utf8fix = true;
  }
  
  //echo $sql.'<br/>';
  if ($link_id) $res = @mysql_query($sql, $link_id);
  else $res = @mysql_query($sql);
  return $res;
}

function SQL2Array($sql, &$A_RES, $clear_res=true) {
  global $smarty, $a_sql_c;
  
  $cache = false;
  
  if (!$res = query($sql)) {
    $A_RES = false;
    return;
  }
  $A_RES = array();
  while ($v = mysql_fetch_assoc($res)) $A_RES[] = $v;
  if ($clear_res) mysql_free_result($res);
  
  if ($cache) {
    $fp = fopen($filename,'w');
    fwrite($fp, serialize($A_RES));
    fclose($fp);
  }
  
  return $res;
}

function SQLRes2Array($res, &$A_RES, $clear_res=true) {
  $A_RES = array();
  while ($v = mysql_fetch_assoc($res)) $A_RES[] = $v;
  if ($clear_res) mysql_free_result($res);
}

// Send Email

function SendSysMail($to, $subj, $body) {
  //global $smarty;
  
  include_once WISE_INC_PATH.'phpmailer/PHPMailerAutoload.php';
  
  $mail = new PHPMailer();
  
  
//$mail->SMTPDebug = 2;
//$mail->Debugoutput = 'html';
    $mail->IsSMTP();
    $mail->SMTPDebug = 1;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'TLS';
    $mail->Host = "smtp.sendgrid.net";
    $mail->Port = 587;
    $mail->IsHTML(true);
    $mail->Charset = 'UTF-8';
    $mail->Username = "apikey";
    $mail->Password = "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";

    $mail->setFrom('hello@buildteam.com', 'Design Team');

    $mail->addReplyTo('hello@buildteam.com', 'Design Team');

  $to = explode(',', $to);
  foreach ($to as $email) {
    $email = trim($email);
    if (!$email) continue;
    $mail->AddAddress($email);
  }
  $to = implode('-', $to);

  
  $mail->Subject = $subj;
  $mail->Body = $body;
  $mail->CharSet = 'utf-8';
  
  $is_html = false;
  if (is_int(strpos($mail->Body,'<p>'))) $is_html = true;
  
  if ($is_html) {
      $mail->IsHTML(true);
      $mail->Body = '<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset='.$mail->CharSet.'" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>'.$mail->Subject.'</title></head>
  <body style="font-family:Arial,sans-serif">' .
    $mail->Body . 
'  </body>
</html>';
  }
  
  if (!$to) return false; // if user hasn't email
  
  $ok = $mail->Send();
  $mail->ClearAddresses();
  
  //if (!$ok) echo $mail->ErrorInfo.'!!!';#debug
  
  return $ok;
}

$a_pages = array();

function get_side_menu(&$side_nav) {
  global $a_pages;
  
  $i = 0;
  foreach ($side_nav AS $k => $v) {

    $i++;

    echo '<li>';

    

    if (WISE_PAGE == $k)

      echo '<span>';

    else

      echo'<a href="/'.WISE_RELURL.$k.'"'.(1==$i?' class="first"':(count($side_nav)==$i?' class="last"':'')).'>';

    if (!is_array($v)) {
      echo $v;
      
      $a_pages[$k] = $v;
      
    }
    else {
      $parent = array_shift($v);
      echo $parent;
      
      $a_pages[$k] = $parent;
    }
    

    if (WISE_PAGE == $k)

      echo '</span>';

    else

      echo'</a>';

    if (is_array($v)) {
      echo '<ul>';
      get_side_menu($v);
      echo '</ul>';
    }

    echo '</li>';

  }

}

function get_breadcrumb(&$side_nav) {
  global $a_pages;
  
  $i = 0;
  
  foreach ($side_nav AS $k => $v) {

    $i++;
    $a_parents = array();
    
    
    if (WISE_PAGE == $k) {
      return array_merge($a_parents, array($k));
    }
    
   
    
    if (is_array($v)) {
    
     echo '<pre>!!'.print_r($v,1).'**</pre>';#debug
      
      $a_parents[] = $k;
      
      array_shift($v);
      
      $a_breadcrumb = get_breadcrumb($v);
      
      if (is_array($a_breadcrumb))
      
        return array_merge($a_parents, $a_breadcrumb);
      else return $a_parents;
    }
    
  }
  
}

function show404page() {
  header($_SERVER['SERVER_PROTOCOL'].' 404 Not Found');
  header('Status: 404 Not Found');
  include WISE_PATH.'404-article.php';
}

// fill $a_settings
global $a_settings;

$key_field = 'setting_key';
$value_field = 'setting_value';

$sql = "SELECT setting_id, $key_field AS name, $value_field AS value FROM setting WHERE setting_id > 1";
$res = query($sql);
while ($row = mysql_fetch_assoc($res)) {
  $a_settings[ $row['name'] ] = $row['value'];
}

// default values
$a_settings['a_visit_types'] = array(
  'standard' => array('name' => 'Standard', 'price' => '50', 'price_vat' => '60'),
  'premium' => array('name' => 'Premium', 'price' => '125', 'price_vat' => '150'),
  '3dpremium' => array('name' => '3D Premium', 'price' => '250', 'price_vat' => '300'),
  'prepurchase' => array('name' => 'Pre-Purchase', 'price' => '125', 'price_vat' => '150'),
);

if (isset($a_settings['standard_price'])) $a_settings['a_visit_types']['standard']['price'] = $a_settings['standard_price'];
if (isset($a_settings['standard_price_vat'])) $a_settings['a_visit_types']['standard']['price_vat'] = $a_settings['standard_price_vat'];

if (isset($a_settings['premium_price'])) $a_settings['a_visit_types']['premium']['price'] = $a_settings['premium_price'];
if (isset($a_settings['premium_price_vat'])) $a_settings['a_visit_types']['premium']['price_vat'] = $a_settings['premium_price_vat'];

if (isset($a_settings['3dpremium_price'])) $a_settings['a_visit_types']['3dpremium']['price'] = $a_settings['3dpremium_price'];
if (isset($a_settings['3dpremium_price_vat'])) $a_settings['a_visit_types']['3dpremium']['price_vat'] = $a_settings['3dpremium_price_vat'];

if (isset($a_settings['prepurchase_price'])) $a_settings['a_visit_types']['prepurchase']['price'] = $a_settings['prepurchase_price'];
if (isset($a_settings['prepurchase_price_vat'])) $a_settings['a_visit_types']['prepurchase']['price_vat'] = $a_settings['prepurchase_price_vat'];

// update log, used coupon, thank you session, send email(s)
function placeVisit($visit, $Message) {
  global $a_settings;
  
  if (!$visit['log']) {
    $sql = "UPDATE visit SET log='".q($Message)."', date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=1  WHERE visit_id=".(int)$visit['visit_id'];
  }
  else {
    $sql = "UPDATE visit SET log=CONCAT(log, '".q($Message)."'), date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=1 WHERE visit_id=".(int)$visit['visit_id'];
  }
  query($sql);
  
  // update used coupon code, if exists
  if ($visit['coupon_code']) {
    $sql = "UPDATE coupon SET used=used+1 WHERE coupon_code='".q($visit['coupon_code'])."'";
    query($sql);
  }
  
  // clear book_visit session and store thank_you session
  
  $_SESSION[WISE_DB_NAME]['thank_you']['visit_id'] = $_SESSION[WISE_DB_NAME]['book_visit']['visit_id'];
  $_SESSION[WISE_DB_NAME]['thank_you']['full_name'] = substr($_SESSION[WISE_DB_NAME]['book_visit']['fname'] . ' ' . $_SESSION[WISE_DB_NAME]['book_visit']['lname'], 0, 150);
  $_SESSION[WISE_DB_NAME]['thank_you']['availibility'] = $visit['availibility'];
  $_SESSION[WISE_DB_NAME]['thank_you']['postcode'] = $_SESSION[WISE_DB_NAME]['book_visit']['postcode'];
  $_SESSION[WISE_DB_NAME]['thank_you']['product_code'] = $_SESSION[WISE_DB_NAME]['book_visit']['product_code'];
  
  $_SESSION[WISE_DB_NAME]['thank_you']['price'] = $visit['amount'];
  
  unset($_SESSION[WISE_DB_NAME]['book_visit']);
  
  // send email
  
  $subj = $a_settings['confirm_subj'];
  $msg = $a_settings['confirm_email'];
  
  $is_html = false;
  if (is_int(strpos($msg,'<p>'))) $is_html = true;
  
  $msg = str_replace('[first name]', $visit['first_name'], $msg);
  $msg = str_replace('[comments]', $visit['comments'], $msg);
  
  if ($is_html) {
    $msg = str_replace('[availibility]', nl2br($visit['availibility']), $msg);
  }
  else {
    $msg = str_replace('[availibility]', $visit['availibility'], $msg);
  }
  
  $msg = str_replace('[postcode]', $visit['postcode'], $msg);
  if ($visit['amount']) {
    $msg = str_replace('[price]', '&pound;'.$visit['amount'].' (VAT included)', $msg);
  }
  else {
    $msg = str_replace('[price]', 'FREE', $msg);
  }

  $msg = str_replace('[address]', $visit['addr'], $msg);
  
  $msg = str_replace('[ref number]', str_pad($visit['visit_id'], 5, '0', STR_PAD_LEFT), $msg);
  $msg = str_replace('[visit type]', $visit['visit_type'], $msg);
  
  
  SendSysMail($visit['email'], $subj, $msg);

  SendSysMail($a_settings['booking_confirmation'], 'DesignTeam: New Site Visit Booked!', $msg);
  
}


?>