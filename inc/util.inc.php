<?php

/* Config parameters */
global $abspath;

$abspath = str_replace('\\','/',dirname(__FILE__));
$abspath = explode('/', $abspath);
array_pop($abspath);
$abspath = implode('/', $abspath);
$abspath .= '/';

require_once ($abspath.'admin/inc/config.inc.php');

define('SkipAuth',1);

$dbconn; /* connecting var */



$IP = $_SERVER['REMOTE_ADDR'];
$USER_AGENT = substr($_SERVER['HTTP_USER_AGENT'], 0, 244);



/* Constants */

$PHP_LONG_DATE_FORMAT = 'M d, Y g:i A';
$IMG_FOLDER = 'C:/Www/dev.wantadigital.com/TCHeathcote/portfolio/';
$IMG_URL = 'http://dev.wantadigital.com/TCHeathcote/portfolio/';
$passMask = '&lt;encrypted&gt;';
$passMask2 = '<encrypted>';

/************************************************
*************** Database functions **************
*************************************************/



function dbConnect() {

	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;
	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass) or die ('Error connecting to database');

	//mysql_select_db($dbname);
  
  mysqli_query($dbconn,"SET NAMES 'utf8'");
}
function dbClose() {
	global $dbconn;
	if (isset($dbconn)) {
		if ($dbconn != "") {
			mysql_close($dbconn);
		}
	}
}
function setRs($sql) {
	$statusQuery = mysqli_query($dbconn,$sql) or die ('Error running query ' .  $sql);
}
function getRs($sql) {
	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;
	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die ('Error connecting to database');
if (!mysqli_query($dbconn,$sql)) {
	echo("Error description: " . $dbconn -> error);
  }
	$rs = mysqli_query($dbconn,$sql) or die ('Error running query ' .  $sql);
	return $rs;
}
function dboDropDown($_ddTable, $_ddValue, $_ddDisplay, $_ddSelected = '0', $_ddSort = '', $_ddField = '', $_ddClass = 'input_sm', $AddNewURL = '', $AltText = '', $AllowNull = false) {
	$arr_ddDisplay = split(',', $_ddDisplay);
	$strDD = '';
	$sQuery = '';
	$i = 0;

	if (strlen($_ddField) == 0) {
		$_ddField = $_ddValue;
	}
	$sQuery =  'SELECT ' . $_ddValue . ', ' . $_ddDisplay . ' FROM ' . $_ddTable;
	if ($_ddTable != 'profiles') {
		//$sQuery .= ' WHERE is_active = 1 AND is_enabled = 1';
	}
	else {
		$sQuery .= ' WHERE FeaturedGuru = 0 AND PrimPhoto > 0';	
	}
	if (strlen($_ddSort) > 0) {
		$sQuery .= ' ORDER BY ' . $_ddSort;
	}
	
	$strDD = '<select id="id_' . $_ddField . '" name="' . $_ddField . '" class="' . $_ddClass . '">';
	if ($AllowNull == true) {
		$strDD .= '<option value="0">- none -</option>';
	}
	
	//echo $sQuery;
	$query = getRs($sQuery);// or die ('Error executing list query');
	while ($row = mysqli_fetch_assoc($query)) {
		$strDD .= '<option value="' . $row[$_ddValue] . '"';
		if ($_ddSelected == $row[$_ddValue]) {
			$strDD .= ' selected';
		}
		$strDD .= '>';
		for ($i = 0; $i < sizeof($arr_ddDisplay); $i += 1) {
			$strDD .= $row[$arr_ddDisplay[$i]];
			if ($i < sizeof($arr_ddDisplay) - 1) {
				$strDD .= ' ';
			}
		}
		$strDD .= '</option>';
	}
	$strDD .= '</select>';
	if (strlen($AddNewURL) > 0) {
		$strDD .= ' <a href="' . $AddNewURL . '"><img src="pics/icon_edit.gif" width="15" height="15" border="0" alt="Edit / Add New" /></a>';
	}

	return $strDD;
}

function dboNavDropDown($sel) {
	$ret = '<select name="navid" id="id_navid" class="input">';
	$ret .= showNav(0, 0, $sel);
	$ret .= '</select>';
	return $ret;
}

function showNav($id, $level, $sel) {
	$i = 0;
	$ret = '';
	$rs = getRs("SELECT * FROM pd_nav WHERE active = 1 AND enabled = 1 AND parentnavid = '" . $id . "' ORDER BY sort");
	while ($row = mysqli_fetch_assoc($rs)) {	
		$ret .= '<option';
		if ($sel == $row['navid']) {
			$ret .= ' selected';
		}
		$ret .= ' value="' . $row['navid'] . '">';
		if ($level == 3)
			$ret .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;&nbsp;&nbsp;';
		elseif ($level == 2)
			$ret .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;&nbsp;&nbsp;';
		elseif ($level == 1)
			$ret .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;&nbsp;&nbsp;';
		$ret .= $row['caption'] . '</option>';
		$ret .= showNav($row['navid'], $level + 1, $sel);
	}
	return $ret;
}


function getSqlValue($sql, $val) {
	$str = '';
	$arr_val = explode(',', $val);
	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;
	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die ('Error connecting to database');
	$query = mysqli_query($dbconn,$sql);// or die ('Error executing list query');
	if ($row = mysqli_fetch_assoc($query)) {
		for ($i = 0; $i < sizeof($arr_val); $i++) {
			$str .= $row[$arr_val[$i]] . ' ';
		}
	}
	return $str;
}

function showPageMedia() {
  global $abspath;
  
	$sql = "SELECT m.page_media_name, m.filename FROM page_media m INNER JOIN page p ON p.page_id = m.page_id WHERE m.is_active = 1 AND m.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND p.page_code = '" . getCurrentUrl() . "' ORDER BY m.sort";
  $rs = getRs($sql);
	//
  //echo '<!-- '.$sql."\n".mysql_num_rows($rs).' -->';#debug
  echo '<div class="col_two_fifth col_last" style="text-align:center">';
	while ( $row = mysqli_fetch_assoc($rs) ) {
    //print_r($row);#debug
		$ext = explode('.', $row['filename']);
    $ext = strtolower(array_pop($ext));
    if ('png'==$ext || 'gif'==$ext || 'jpg'==$ext || 'jpeg'==$ext) 
      
      $a_size = getimagesize($abspath.'/media/'.$row['filename']);
      
      echo '<img src="/media/'. $row['filename'] . '" alt="' . $row['page_media_name'] . '" title="' . $row['page_media_name'] . '" style="width:100%;max-width:'.$a_size[0].'px" />';
	}
  echo '</div>';
}

/************************************************
**************** Helper functions ***************
*************************************************/

function getCurrentUrl() {
  if (isset($_GET['code']) && $_GET['code']) { // custom backend editable content pages
    $curr_url = $_GET['code'].'-page';
  }
	else {
    $curr_url = $_SERVER["SCRIPT_NAME"];
    $curr_url = strtolower($curr_url);
    $arr_curr_url = explode("/", $curr_url);
    $curr_url = $arr_curr_url[1]; //str_replace("/", "", $curr_url);
    $curr_url = str_replace(".php", "", $curr_url);
    $parent_url = explode("_", $curr_url);
    $parent_url = $parent_url[0];
  }
	return $curr_url;
}

function getParentUrl($url = '') {
	global $bc_trail; //<a href="index.php">Home</a> &rsaquo; <b>Our Experience</b></div>';
	
	$initBc = false;
	if ( strlen($url) == 0 ) {
		$url = getCurrentUrl();
		$initBc = true;
	}
	$rs = getRs("SELECT pp.page_id, pp.page_code, pp.page_name, pp.is_link, p.page_name AS c_page_name FROM page p INNER JOIN page pp ON pp.page_id = p.parent_page_id AND pp.is_active = 1 AND pp.is_enabled = 1 WHERE p.is_enabled = 1 AND p.is_active = 1 AND p.page_code = '{$url}'");
	if ( $row = mysqli_fetch_assoc($rs) ) {
		if ( $initBc ) {
			$bc_trail = "<b>" . htmlspecialchars($row['c_page_name']) . '</b>';
		}
		if ( $row['page_id'] > 0 ) {
			if ( $row['is_link'] == 1 ) {
				$bc_trail = '<a href="' . $row['page_code'] . '.html" title="' . htmlspecialchars($row['page_name']) . '">' . htmlspecialchars($row['page_name']) . '</a> &rsaquo; ' . $bc_trail;
			}			
			
			else {
				$bc_trail = '<a href="javascript:void(0)" title="' . htmlspecialchars($row['page_name']) . '">' . htmlspecialchars($row['page_name']) . '</a> &rsaquo; ' . $bc_trail;
			}
			$url = getParentUrl( $row['page_code'] );
		}
	}
	return $url;
}

function formatPassword($pass) {
	return sha1($pass);
}

function safe($posted) {
	if (!get_magic_quotes_gpc()) {
		return addslashes($posted);
	} else {
		return $posted;
	}
}

function yesNoFormat($str) {
	if ($str == 1) {
		return '<span class=txt_gb>Yes</span>';
	}
	else {
		return '<span class=txt_rb>No</span>';
	}
}

function toLink($str) {
	$v_str = strtolower(trim(substr($str, 0, 50)));
	$specialchars = array('~','!','@','#','$','%','^','&','*','(',')','_','+','=','|','}','{','"',':','?','>','<',',','.','/',';','\'','`');
	$v_str = str_replace($specialchars, '', $v_str);
	$v_str = str_replace("  ","-",$v_str);
	$v_str = str_replace(" ","-",$v_str);
	//$v_str = preg_replace('[\W]', '-', $v_str);
	//$v_str = preg_replace('[\W_]', '-', $v_str);
	/*
	$v_str = str_replace("'","",$v_str);
	$v_str = str_replace("<","",$v_str);
	$v_str = str_replace(">","",$v_str);
	$v_str = str_replace("&","",$v_str);
	$v_str = str_replace(" ","-",$v_str);
	*/
	return $v_str;
}

function formatSql($str) {
	$v_str = trim($str);
	//$v_str = str_replace("'","''",$v_str);
	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;
	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die ('Error connecting to database');
  $v_str = mysqli_real_escape_string($dbconn,$v_str);
	//$v_str = str_replace("<","",$v_str);
	//$v_str = str_replace(">","",$v_str);
	//$v_str = str_replace("&","",$v_str);
	return $v_str;
}

function getUniqueFilename ($str) {
	$str_name = getFilename($str); //substr($str,0,strlen($str)-4);
	$str_ext = getExt($str);
	
	return strtolower(uniqid($str_name) . '.' . $str_ext);
}

function getFilename($str) {
	return strtolower(preg_replace('/\.[^.]*$/', '', $str));
}

function getExt($str) {
	return strtolower(substr(strrchr($str, '.'), 1));
}

function isGif($str) {
	if (getExt($str) == 'gif')

		return true;

	else

		return false;

}



function isPng($str) {

	if (getExt($str) == 'png')

		return true;

	else

		return false;

}



function getUniqueID () {

	return uniqid();

}



function isValidEmail($email) {
	if(strlen($email) > 0 && eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {
		return true;
	}
	else {
		return false;
	}
}



function dateFormat($d) {
	$arr_d = split('-', $d);
	if (sizeof($arr_d) == 3) {
		//return $arr_d[1] . '/' . $arr_d[2] . '/' . $arr_d[0];
	}
	else {
		//return '';
	}
	$php_date_format = 'M d, Y H:nn';
	$php_date_format = 'n/j/Y g:nn A';
	return date($php_date_format, $d);
}

function insertHtmlWrapper ($ret) {
	return '<html><head><style>body{font-family:Arial;font-size:14px;}table{border:1px solid #ccc;}table th{padding:5px;background:#eee;border:1px solid #fff;text-align:right;color:#aaa}table td{padding:5px;background:#f1f1f1;border:1px solid #fff;color:#333;}.sm{padding:10px;color:#aaa;font-size:11px}</style></head></body>' . $ret . '</body></html>';
}

function currency_format($amount) {
  $new_amount = sprintf("%.2f",$amount);
	return $new_amount;
}

//session_start();

// connect to db
dbConnect();
?>