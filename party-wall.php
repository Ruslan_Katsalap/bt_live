<?php require_once('./inc/header.inc.php'); ?>
<style>
body {
	background:url(images/party_wall/party_wall_bg.jpg) center;
	background-attachment:fixed;
	}
.partywalldesctitle p span {
    display: block;
    text-align: center;
    font-size: 16px;
	color:#969696;
}
h1.page-title {
    border: none;
    text-align: center;
}
.partywalldesctitle p {
    padding-top: 5px;
}
.partywalldesctitle h2 {
    font-size: 18px;
    font-weight: bold;
    color: #104678;
    text-align: center;
}
.partywalldesctitle p span {
    display: block;
    text-align: center;
    font-size: 16px;
}
.partywallinner {
    text-align: center;
}
.partywalldouble .partywalldesctitle {
    max-width: 49.7%;
    display: inline-block;
    width: 100%;
}
.smallline {
    width: 68%;
    margin: 0 auto;
    height: 1px;
    background: #969696;
	margin-bottom:30px;
}

.partywallthree .partywalldesctitle {
    max-width: 32%;
    display: inline-block;
    width: 100%;
}
.timlineimage img {
    max-width: 495px;
}
.partywallthree img {
    width: 50px;
}
.partywalldesctitle {
    margin-bottom: 40px;
}
.smallline {
	width:68%;
	margin:0 auto;
	height:1px;
	background:#969696;
	margin-bottom:50px;
	}
.partywallthree {
    padding-bottom: 100px;
}	
footer.round-2 {
	margin:0 auto 0 !important;
	}
.col_two_fifth.col_last {
	display:none !important;
	}
.partymobile, .partymobilearrow {
		display:none;
		}
	
@media (max-width:640px) {
	.partywalldouble .partywalldesctitle {
		max-width:100%;
		}
	.timlineimage  {
		display:none;
		}
	.partywalldesctitle p span {
		font-size:13px;
		}
	.partywallthree .partywalldesctitle {
		max-width:100%;		
		}
	.partymobilearrow {
		display: block;
    position: relative;
    top: -20px;
		}
	.partymobilearrow img {
    width: 20px;
}		
	.partymobile {
			display: inherit !important;
			background: url(images/party_wall/or.png) no-repeat center;
			background-size: contain;
			margin-bottom:60px;
		}
	.partydesk {
		display:none;
		}
	.partymobile2 {
		float: left;
		width: 50%;
		height:80px;
	}						
	}			
</style>
<div class="partywallout">
	<div class="partywallinner">
    	<div class="partywalltoptext">
        <div class="partywalldesctitle">
        	<h1 class="page-title">Party Wall Service</h1>
            <p>
            <span>We have created a straightforward and efficient process to guide you through the Party Wall phase of</span>
<span> your extension project. As with all professional services provided by Build Team, our fees are fully fixed</span>
<span>up front so you know how much to expect and budget for in advance.
            </span>
            
            </p>
            </div>
        </div>
        <div class="partywallsingle">
            <div class="partywalldesctitle">
                <h2>Party Wall Pack</h2>
                <p> <span>We gather the relevant drawings,</span>
                    <span>evaluate the notifiable works pertaining to</span>
                    <span>your adjoining owners and identify the</span>
                    <span>appropriate notice type for each.</span>
                </p>
            </div>
         </div>   
         <div class="parywallsingle timlineimage">
         	<img src="images/party_wall/party_timeline.jpg" alt="">
         </div>
         <div class="partymobilearrow">
         	<img src="images/party_wall/downarrow.jpg" alt="">
         </div>
         <div class="partywalldouble partydesk">
         	<div class="partywalldesctitle">
                <h2>Panel Surveyor</h2>   
                <p> <span>We will introduce you to</span>
                    <span>  an approved Party Wall</span>
                    <span> Surveyor from our panel.</span>
                 </p>
            </div>
            <div class="partywalldesctitle">
                <h2>Your Surveyor</h2>
                <p> <span>Choose to use your own</span>
                    <span>Party Wall Surveyor to serve</span>
                    <span>the notices.</span>
                </p>
            </div>
         </div>
         <div class="partymobile">
         	<div class="partymobileinner">
                 <div class="partymobile2">
                    <div class="partywalldesctitle">
                        <h2>Panel Surveyor</h2>
                <p> <span>We will introduce you to</span>
                    <span>  an approved Party Wall</span>
                    <span> Surveyor from our panel.</span>
                         </p>
                    </div>
                 </div>
             
             <div class="partymobile2"></div>
             <div class="clearfix"></div>
             </div>
             
             <div class="partymobileinner">
             	 <div class="partymobile2"></div>
                 <div class="partymobile2">
                    <div class="partywalldesctitle">
                        <h2>Your Surveyor</h2>
                <p> <span>Choose to use your own</span>
                    <span>Party Wall Surveyor to serve</span>
                    <span>the notices.</span>
                        </p>
                    </div>
                 </div>
             </div>
             <div class="clearfix"></div>
         </div>
         <div class="partymobilearrow">
         	<img src="images/party_wall/downarrow.jpg" alt="">
         </div>
         <div class="parywallsingle timlineimage">
         	<img src="images/party_wall/party_wall_down.jpg" alt="">
         </div>
         <div class="partywallsingle partydesk">
         	<div class="partywalldesctitle">
                <h2>Ongoing Facilitation</h2>
                <p> <span>We will provide any further</span>
                    <span>drawing details or structural</span>
                    <span>information requested by the</span>
                    <span>Party Wall Surveyor.</span>
                 </p>
            </div>
         </div>
         <div class="partywallsingle partymobile" style="background:none;">
         	<div class="partywalldesctitle">
                <h2>Ongoing Facilitation</h2>
                <p> <span>We will provide any further</span>
                    <span>drawing details or structural</span>
                    <span>information requested by the</span>
                    <span>Party Wall Surveyor.</span>
                 </p>
            </div>
         </div>
         <div class="smallline">
         	
         </div>
<!--         <div class="partywallsingle">
         	<div class="partywalldesctitle">
                <h2>What Makes Our Party Wall Service Unique</h2>
                <p> <span>We have created a customer focused process which has been designed specifically to save you time and money.</span>
                    <span> We adopt a conciliatory approach with your neighbours and our objective is to obtain their consent at an early stage.</span>
                   
                </p>
            </div>
         </div>-->
         
         <div class="partywallthree">
         	<div class="partywalldesctitle">
            	<img src="images/party_wall/party_we_benifit.jpg" alt="">
                <h2>Who are our
<br>Nominated Surveyors</h2>
                <p> <span>We have a panel of </span>
                    <span>trusted Surveyors who have</span>
                    <span>worked with us on many of</span>
                    <span>our extension</span>
                    <span>projects.</span>
                </p>
            </div>
            <div class="partywalldesctitle">
            	<img src="images/party_wall/party_experienced.jpg" alt="">
                <h2>Experienced<br>& Informed</h2>
                <p> <span>A benefit of employing a qualified</span>
                    <span>Surveyor from our panel is that</span>
                    <span>they have a wealth of experi-</span>
                    <span>ence and are able to deal with</span>
                    <span>dissents effectively.</span>
                </p>
            </div>
            
            <div class="partywalldesctitle">
            	<img src="images/party_wall/party_double.jpg" alt="">
                <h2>How do we<br>Benefit</h2>
                <p> <span>We want to make the process</span>
                    <span>as straightforward as possible.</span>
                    <span>Build Team offer and end-to-</span>
                    <span>end service and we hope you</span>
                    <span>choose to invite us to tender.</span>
                </p>
            </div>
            
         </div>
         
    </div>
</div>

<?php require_once('./inc/footer.inc.php'); ?>