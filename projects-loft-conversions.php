<?php

require_once('./inc/header.inc.php');
?>
<style>
/* Css for gallery */

.col_half a {
    position: relative;
    width: 100%;
    display: block;
}
.col_half img {
    border: 0;
    /* max-width: 450px !important; */
    width: 100% !important;
	max-width: inherit !important;
}
</style>

<div class="full">

<h1>loft conversions – a great way to create more space</h1><br/>

<?php

$i = 1;
$ret = '';
$project_category_code = 'loft-conversions';
$project_code = '';

if ( isset($_GET['p']) ) {
	//$project_code = $_GET['p'];
}


$rs = getRs("SELECT c.project_category_code, c.project_category_name, i.filename, p.project_code, p.project_name FROM project_category c INNER JOIN (project_image i INNER JOIN project p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 AND c.project_category_code = '" . formatSql($project_category_code) . "' ORDER BY p.sort, p.project_id, i.sort, i.project_image_id");

$cols = 2;
$project_count = 0;
$prev_count = 0;

while ($row = mysqli_fetch_assoc($rs) ) {
	if ( $i == 1 ) {
		// echo '<div id="bc"><a href="/index.html">Home</a> &rsaquo; <a href="/our-experience.html">Our Experience</a> &rsaquo; <b>' . $row['project_category_name'] . '</b></div>';
	}
		
		if ($project_code != $row['project_code']) {
			$project_code = $row['project_code'];
      $project_count++;
		}
		else {
			continue;
      //$ret .= 'display:none;';
		}
		
    $ret .= '<'.'div class="col_half'. ($i%2==1 ? '' : ' col_last') .'" style="text-align:center';
		
		$ret .= '"><a href="/'.$row['project_code'].'-'.$project_category_code.'.html" title="' . htmlentities($row['project_name']) . '"><img src="/phpthumb/phpThumb.php?src='.urlencode('/projects/' . $row['filename']) . '&amp;w=450&amp;h=400" style="width:100%;max-width:450px" alt="' . htmlentities($row['project_name']) . '" /><span style="display:block;margin-top:5px">'.htmlentities($row['project_name']).'</span></a></div>'; // debug: ' '.$project_count
    
    if (0==($project_count % $cols) && $project_count!=$prev_count) {
      $ret .= '<br clear="all" />'; // debug: ($project_count % $cols).'!!!
      $prev_count = $project_count;
    }    
	$i += 1;
}

echo $ret;

?>

<br clear="all"/>
<p style="text-align:justify">
If you have already browsed our website, you probably already know by now that we specialise in side return kitchen extensions and loft conversions. This page is dedicated to loft conversions projects.
</p>
  
</div>

<?php

require_once('./inc/footer.inc.php');

?>