<?php

// Ajax verify of coupon code

//sleep(1);//debug

include_once 'inc/settings.php';
@session_start();

if (isset($_GET['qid']) && $_GET['qid'] && isset($_SESSION['userid']) && $_SESSION['userid']) {
  $_GET['qid'] = (int)$_GET['qid'];
  $_SESSION['userid'] = (int)$_SESSION['userid'];
  if ($_SESSION['userid'] && $_GET['qid']) {
    
    // get quote details of logged in user:
    
    $sql = "SELECT * FROM rfq WHERE rfq_id=".$_GET['qid']." AND userid=".$_SESSION['userid'];
    
    SQL2Array($sql, $quote);
    
    if (isset($quote[0])) {
      
      $quote = $quote[0];
      
      //echo '<pre>'.print_r($quote, 1).'</pre>';//debug
      //exit;
      
      $_SESSION[WISE_DB_NAME]['book_visit']['fname'] = $quote['name'];
      $_SESSION[WISE_DB_NAME]['book_visit']['email'] = $quote['email'];
      $_SESSION[WISE_DB_NAME]['book_visit']['phone'] = $quote['phone'];
      $_SESSION[WISE_DB_NAME]['book_visit']['postcode'] = $quote['postcode'];
      $_SESSION[WISE_DB_NAME]['book_visit']['addr'] = $quote['address'];
      $_SESSION[WISE_DB_NAME]['book_visit']['comments'] = $quote['message'];
      
      // to disable re-solicitation email (auto email) later:
      $_SESSION[WISE_DB_NAME]['qid'] = $quote['rfq_id'];
      
      // redirect depends on current status:
      
      if ('pre-purchase' == $quote['status']) {
        $location = '/book-visit.html?type=prepurchase';
      }
      else {
        $location = '/initial_visit.html';
      }
      
      header('Location: '.$location);
      exit;
      
    }
    else {
      header('Location: /getting-started/book-site-visit.html');
      exit;
    }
    
  }
  else {
    header('Location: /getting-started/book-site-visit.html');
    exit;
  }
}

header('Content-type: application/json'); // ; charset=utf8;

if (isset($_POST['code']) && ($_POST['code']=trim($_POST['code']))) {
  
  if (!isset($a_settings['coupon_enabled']) || !$a_settings['coupon_enabled']) dieCoupon('"denied"');
  else {
    
    if (!isset($_SESSION[WISE_DB_NAME]['book_visit'])) dieCoupon('"no_session"');
    
    if (get_magic_quotes_gpc()) $_POST['code']=stripslashes($_POST['code']);
    
    $sql = "SELECT * FROM coupon WHERE coupon_code='".q($_POST['code'])."'";
    SQL2Array($sql, $coupon);
    if (isset($coupon[0]['coupon_id'])) {
      $coupon = $coupon[0];
      
      if (!$coupon['is_enabled'] || !$coupon['is_active']) dieCoupon('"disabled"');
      
      if ( !isset($_SESSION[WISE_DB_NAME]['book_visit']['product_code']) || $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['name'] != $coupon['visit_type'] ) {
        dieCoupon('"wrong_product"');
      }
      
      $time = time();
      
      if ($coupon['valid_from'] && $coupon['valid_from'] > $time) dieCoupon('"not_valid"');
      if ($coupon['expiry_date'] && $time > $coupon['expiry_date']) dieCoupon('"expired"');
      
      $out = array();
      
      $out['price'] = round($a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price'] * (100-(int)$coupon['discount']) / 100);
      
      $out['price_vat'] = round($a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat'] * (100-(int)$coupon['discount']) / 100);
      
      if (isset($_SESSION[WISE_DB_NAME]['book_visit']['visit_id'])) {
        $sql = "UPDATE visit SET coupon_code='".q(strtoupper($_POST['code']))."', amount='".$out['price_vat']."' WHERE visit_id=".(int)$_SESSION[WISE_DB_NAME]['book_visit']['visit_id'];
        query($sql);
      }
      
      echo json_encode($out);
      exit;
      
    }
    else dieCoupon('"not_found"');
    
  }
  
}
else dieCoupon('""');

function dieCoupon($msg) {
  global $a_settings;
  
  if (isset($_SESSION[WISE_DB_NAME]['book_visit']['visit_id'])) {
    $sql = "UPDATE visit SET coupon_code='', amount='".(int)$a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat']."' WHERE visit_id=".(int)$_SESSION[WISE_DB_NAME]['book_visit']['visit_id'];
    query($sql);
  }
  
  die($msg); // please reset hash to an init value in the pay_now.php code
}

?>