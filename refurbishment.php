<?php

header('HTTP/1.1 301 Moved Permanently');
header('Location: /what-we-do/refurbishment-page.html');
exit;

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    	
			<?php echo $bc_trail; ?>
      
    	<h1>Refurbishment</h1>
      <p>As a company with a diverse background in both commercial and residential refurbishments and maintenance, Build Team offers a unique combination of value for money, personal attention, and tailored services. </p>
      <p>If you already have a set of plans, or indeed an architect, we can make an immediate start by responding with a detailed quotation and outline project programme. If you require assistance with design and regulatory matters, we can assign one of our in-house architects via our <a href="/build-your-price.html">turn-key design and build service</a>.</p>
      <p>We genuinely enjoy and take a great deal of pride in our work, and our reputation has been earned by our professional approach and complete customer satisfaction.</p>
      <p>With a dedicated and experienced team to hand, refurbishing properties in London is carried out quickly, efficiently and always to a high quality standard.</p>
      
      <ul>
      	<li>Detailed quote itemising each item on the build</li>
        <li>Dedicated Project Manager assigned to every build</li>
        <li>Professional and reliable tradesmen</li>
        <li>Comprehensive project plan produced and updated for every build</li>
        <li>Regular on-site meetings with clients, project manager, design manager (if applicable) and construction team</li>
        <li>Fair and transparent approach to pricing</li>
        <li>Experience of working on listed buildings</li>
      </ul>
      
      <p>If you would like to find out more about our refurbishment division, please get in touch to book an appointment for a Project Manager to visit, or email us on <a href="/contact.html">contact page</a>.</p>
		</div>
<?php

require_once('./inc/footer.inc.php');

?>