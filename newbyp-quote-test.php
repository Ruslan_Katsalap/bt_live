<?php require_once('./inc/header.inc.php'); 
define("totalcost",$_GET['totalcost']);

function getExt1($str) {
	return strtolower(substr(strrchr($str, '.'), 1));
}

function reArrayFiles($file)
{
    $file_ary = array();
    $file_count = count($file['name']);
    $file_key = array_keys($file);
   
    for($i=0;$i<$file_count;$i++)
    {
        foreach($file_key as $val)
        {
            $file_ary[$i][$val] = $file[$val][$i];
        }
    }
    return $file_ary;
}
if($_FILES['imagefile']){
	$img = $_FILES['imagefile'];
	if(!empty($img))
	{
		$img_desc = reArrayFiles($img);
		foreach($img_desc as $val)
		{
			$ext = getExt1($val['name']);
			$newname = date('YmdHis',time()).mt_rand().'.'.$ext;
			move_uploaded_file($val['tmp_name'],'./bypAttaimage/'.$newname);
			setRs("insert into byp_Quote_images(byp_Quote_id,imagename) value('".$_GET['id']."','".$newname."')");
				  
		}
	}
echo 'Images has been uploaded';
}

$sqlData = mysqli_fetch_assoc(mysqli_query($dbconn,"SELECT refCode FROM byp_Quote WHERE id='".$_GET['id']."'"));
  
?>
<link rel="stylesheet" href="byp-css/custombyp.css">

<script>
function isenabled(ids){
	$.ajax({url: "newbyp-enabled.php?id="+ids, success: function(result){
		alert('Your Quote Has been saved please login and see.');
	}});
}
</script>

<?php 
  $aad_rs = getRs("SELECT * FROM setting WHERE setting_id = 1");
  $aad_row = mysqli_fetch_assoc($aad_rs);
?>
<div class="setp_summary">
                   <div class="pagetitle">
                    	<h1 class="commonpagetitlenobor">Your Extension Quote</h1>
               		</div>  
                    <div class="summarysubtitle">
                    	<p>Quote reference</p>
                        <p class="quoterefnumber"><?=$sqlData['refCode']?></p>
                        <div class="clearfix"></div>
                    </div>
                    <div class="summarymain">
                    	<div class="summaryinline summaryimage">
                        	<div class="imageinnner">
                            	<img src="img/summmary.jpg" alt="">
                            </div>
                        </div>
                        <div class="summaryinline summarytext">
                        	<div class="mainsummarytext">
                            	<div class="designsummary">
                                	<p>Design Phase</p>
                                    <ul>
                                    	<li>Architectural design phase</li>
                                    	<li>Planning permission</li>
                                    	<li>Structural engineering</li>
                                    </ul>
                                    <span id="designphasecost">£<?=number_format($aad_row['design_cost']); ?></span>                                   	
                                </div>
                                <div class="cinstsummary">
                                	<p>Construction Phase</p>
                                    <ul>
                                    	<li>Full project management</li>
                                    	<li>Electrical work and plumbing</li>
                                    	<li>Roof Lights and doors</li>
                                    </ul>
                                    <span id="constphasecost">£<?=number_format(totalcost)?></span>                                   	
                                </div>
                            </div>
                        </div>
                       </div> 
                        <div class="summarydesc">
                        	<p>This is a computer generated image of how your extension could look based on your specific requirements</p>
                        	<p style="font-size:14px;">All figures exclude VAT at the prevailing rate.</p>
                        </div>
                        <div class="summarybutton">
                        	<ul>
                        		<li><a href="https://www.buildteam.com/online_quote_calculator.html">Revise Quote</a></li>
                        		<li><a id="openattachment" href="javascript:void(0);">Attach images</a></li>
                        		<li><a href="#" onclick="isenabled('<?=$_GET['id']?>');">Save Quote</a></li>
                        		<li><a href="https://www.buildteam.com/getting-started/book-site-visit.html">Book a Design Consultation</a></li>
                        	</ul>
                        </div>
                        <div id="attachpopup" class="commonnewpopup">
                            <div class="newpoupinner">
                            	<a class="closeiitpopup" href="javascript:void(0);">x</a>
                            	<h1>Upload</h1>
                                <form action="?totalcost=<?=$_GET['totalcost']?>&id=<?=$_GET['id']?>" enctype="multipart/form-data" method="post">
                                <div class="popupcontentmiddle">
                                   
                                       <input id="fileupload" name="imagefile[]" type="file" multiple />
                                       <input type="submit" value="submit" id="submit" />
                                   
                                </div>    
                                
                                </form>
                                </div>
                            </div>
                    </div>
<script>
$("#openattachment").click(function(){
	$("#attachpopup").fadeIn(300);
	});
$(".closeiitpopup").click(function(){
	$(".commonnewpopup").fadeOut(300);
	});	
</script>                    
<?php require_once('./inc/footer.inc.php');?>