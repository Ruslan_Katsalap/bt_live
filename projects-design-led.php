<?php

require_once('./inc/header.inc.php');
?>

<div class="full">

<?php

$i = 1;
$ret = '';
$project_category_code = 'design-led';
$project_code = '';

if ( isset($_GET['p']) ) {
	//$project_code = $_GET['p'];
}


$rs = getRs("SELECT c.project_category_code, c.project_category_name, i.filename, p.project_code, p.project_name FROM project_category c INNER JOIN (project_image i INNER JOIN project p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 AND c.project_category_code = '" . formatSql($project_category_code) . "' ORDER BY p.sort, p.project_id, i.sort, i.project_image_id");


while ($row = mysqli_fetch_assoc($rs) ) {
	if ( $i == 1 ) {
		echo '<div id="bc"><a href="/">Home</a> &rsaquo; <a href="/our-experience.html">Our Experience</a> &rsaquo; <b>' . $row['project_category_name'] . '</b></div>
    <h1>Design-Led Projects</h1>';
	}
	$ret.= 
	'
		<li';
		
		if ($project_code != $row['project_code']) {
			$project_code = $row['project_code'];
		}
		else {
			$ret .= ' style="display:none;"';
		}
		
		
		$ret .= '><a href="/projects/' . $row['filename'] . '" alt="" rel="shadowbox[project_' . $row['project_code'] . ']" title="' . $row['project_name'] . '"><img src="/projects/' . $row['filename'] . '" width="150" alt="' . $row['project_name'] . '" /></a></li>';
	$i += 1;
}

echo '

<ul id="mycarousel" class="jcarousel-skin-tango">
			' . $ret . '
</ul>
';

?>

</div>

<?php

require_once('./inc/footer.inc.php');

?>