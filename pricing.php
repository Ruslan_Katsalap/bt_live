<?php require_once('./inc/header.inc.php'); ?>

<style>

body {

	background:url(images/party_wall/party_wall_bg.jpg) center;

	background-attachment:fixed;

	}

.partywalldesctitle p span {

    display: block;

    text-align: center;

    font-size: 16px;

	color:#969696;

}

h1.page-title {

    border: none;

    text-align: center;

}

.partywalldesctitle p {

    padding-top: 5px;

}

.partywalldesctitle h2 {

    font-size: 18px;

    font-weight: bold;

    color: #104678;

    text-align: center;

}

.partywalldesctitle p span {

    display: block;

    text-align: center;

    font-size: 16px;

}

.partywallinner {

    text-align: center;

}

.partywalldouble .partywalldesctitle {

    max-width: 49.7%;

    display: inline-block;

    width: 100%;

}

.smallline {

    width: 68%;

    margin: 0 auto;

    height: 1px;

    background: #969696;

	margin-bottom:30px;

}



.partywallthree .partywalldesctitle {

    max-width: 32%;

    display: inline-block;

    width: 100%;

}

.timlineimage img {

    max-width: 495px;

}

.partywallthree img {

    width: 50px;

}

.partywalldesctitle {

    margin-bottom: 40px;

}

.smallline {

	width:68%;

	margin:0 auto;

	height:1px;

	background:#969696;

	margin-bottom:50px;

	}

.partywallthree {

    padding-bottom: 100px;

}	

footer.round-2 {

	margin:0 auto 0 !important;

	}

.col_two_fifth.col_last {

	display:none !important;

	}

.partymobile, .partymobilearrow {

		display:none;

		}

	

@media (max-width:640px) {

	.partywalldouble .partywalldesctitle {

		max-width:100%;

		}

	.timlineimage  {

		display:none;

		}

	.partywalldesctitle p span {

		font-size:13px;

		}

	.partywallthree .partywalldesctitle {

		max-width:100%;		

		}

	.partymobilearrow {

		display: block;

    position: relative;

    top: -20px;

		}

	.partymobilearrow img {

    width: 20px;

}		

	.partymobile {

			display: inherit !important;

			background: url(images/party_wall/or.png) no-repeat center;

			background-size: contain;

			margin-bottom:60px;

		}

	.partydesk {

		display:none;

		}

	.partymobile2 {

		float: left;

		width: 50%;

		height:80px;

	}						

	}			

    .aad_left_sec p .aad_span_light

    {

        font-size:24px;

        font-weight:400;

        color:#afafaf;

    }

    .aad_left_sec p .aad_span_dark

    {

        font-size:20px;

        color:#003c70;  

        font-weight:400;

    }

    .aad_first_top_text

    {

        margin:20px 0 0 0;

    }

    .aad_second_top_text

    {

        margin:80px 0 0 0;

    }

    .aad_li_section

    {

        float:left;

        margin:20px 0 0 0;

    }

    .aad_li_section li

    {

        text-align:left;

        font-size:16px;

        color:#afafaf;

        line-height:18.5px;

    }
@media only screen and (max-width:640px)
{
    .aad_pricing_arrow_image
    {
        display:none;
    }
    .aad_left_sec
    {
        width:100% !important;
    }
    .aad_li_section
    {
        width:100%;
    }
    .aad_li_section2
    {
        margin:40px 0 0 0 !important;
        border-bottom:2px dotted #0F4778;
        padding-bottom:25px;
    }
    .aad_li_section ul
    {
        width: 70%;
        margin: 0 auto;
    }
    .aad_dotted_border
    {
        border-bottom:2px dotted #0F4778;
    }
    .aad_second_top_text
    {
        border-bottom:2px dotted #0F4778;
        padding-bottom:25px;
    }
    .aad_hide_desktop
    {
        display:block;
        margin-bottom:25px !important;
    }
    .aad_hide_mobile
    {
        display:none;
    }
    .aad_left_sec
    {
        margin-top:25px;
    }
    .aad_second_top_text
    {
        margin:30px 0 0 0;
    }
    .aad_bottom_section
    {
        padding :30px 0 0 0 !important;
    }
    .aad_hide_desktop
    {
        display:block !important;
    }
}
.aad_hide_desktop
{
    display:none;
}
.aad_mobile_icons
{
    margin-bottom:0px;
    width:80px;
    margin:0 auto;
    margin-bottom:0px !important;
}
</style>

<div class="partywallout">

	<div class="partywallinner">

    	<div class="partywalltoptext">

        	<h1 class="page-title">Pricing</h1>

        </div>

        <div class="partywallsingle aad_dotted_border">

            <div class="partywalldesctitle aad_hide_mobile">

                <p> <span>With over 900 projects under our belt, we are confident we can obtain planning permission for your</span>

                <span> scheme. Since launch, we have always operated a simple, fixed-fee pricing structure for ground floor </span>

                <span>  extension and loft projects. Contained within this package are the key elements you will need to build </span>

                <span> your project: planning consent, building regulations pack and full structural engineering.</span>

                </p>

            </div>
            <div class="partywalldesctitle aad_hide_desktop">

                <p> <span>Since launch, we have always operated a simple, fixed-</span>

                <span> fee pricing structure for all ground floor extension and  </span>

                <span> loft projects. Contained within this package are the key </span>

                <span>elements you will need to build your project.</span>

                </p>

            </div>

         </div>   

         <div class="parywallsingle timlineimage" style="float:left;width:100%;background:#fff;display:block;">

            <div class="aad_left_sec" style="float:left;width:46%;">
<img class="aad_hide_desktop aad_mobile_icons" src="http://buildteam.com/images/aad_icon1.gif"  />
                <h1>Fixed Fee Design Packages </h1>

                <p class="aad_first_top_text">

                <img class="aad_hide_desktop aad_mobile_icons" src="http://buildteam.com/images/aad_icon5.gif"  />
                    <span class="aad_span_light">Ground Floor <br> </span>

                    <span class="aad_span_light">Extensions <br> </span>

                    <br>

                    <span class="aad_span_dark">from £2,995</span>

                </p>

                <p class="aad_second_top_text">

                <img class="aad_hide_desktop aad_mobile_icons" src="http://buildteam.com/images/aad_icon4.gif"  />
                    <span class="aad_span_light"> Loft Conversions <br> </span>

                    <br>

                    <span class="aad_span_dark">from £1,995 </span>

                </p>
<img style="width: 125px;margin:50px 0 0 00px;" src="images/payment_logo.png">
            </div>

         	<img  class="aad_pricing_arrow_image" src="images/pricing_timeline.jpg" alt="" style="float:left;height:500px;">

         	<div class="partywalldesctitle aad_li_section" style="margin:22px 0 0 0">

             <img class="aad_hide_desktop aad_mobile_icons" src="http://buildteam.com/images/aad_icon2.gif" style="width:100px;margin-bottom:15px !important;" />   
                <h2>What’s included? </h2>

                <p> 

                <ul >

                <li>Full Measured Survey, Design</li>

                <li>Options and Planning Application</li>

                <li>Building Regulation Drawing and <br> Schedule of Works</li>

                <li>Structural Engineering</li>

                </ul>

                 </p>

            </div>

            <div class="partywalldesctitle aad_li_section aad_li_section2" style="margin:110px 0 0 0;">

                <h2>Why choose Build Team?</h2>

                <p> 

                <ul>

                <li>Founded in 2007 with 10+ years of <br> experience in Design and Build</li>

                <li>900+ successful planning consents <br> across London</li>

                <li>Two-Stage Process: no obligation to <br> build with us</li>

                <li>Unlimited amendments prior to <br> planning submission</li>

                <li>In-house design and project <br> management expertise</li>

                </ul>

                </p>

            </div>

         </div>

         <div class="smallline">

         </div>

         <div class="partywallsingle aad_bottom_section" style="float:left;width:100%;background:#fff;padding:80px 0 0 0;">

         	<div class="partywalldesctitle ">

                <p> <span>Clients who instruct Build Team also benefit from our Two-Stage Process – meaning there is no obligation</span>

                    <span> to commit beyond the Design Phase and you are free to tender your designs to other contractors if you</span>

                    <span> wish. Regardless of how you decide to proceed at this stage, you will automatically receive a fixed price </span>

                    <span> tender from Build Team for the construction phase of your project. </span>

                   

                </p>

                <br>

                <p><span>Excludes VAT at the prevailing rate.</span></p>

            </div>

         </div>

         

         

    </div>

</div>



<?php require_once('./inc/footer.inc.php'); ?>