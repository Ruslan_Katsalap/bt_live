<?php
require_once('./inc/header.inc.php');

?>

<style type="text/css">
  p {
    padding-top: 10px;
  }
</style>

<link href="css/newbypmobile.css" rel="stylesheet">
<script type="text/javascript" src="/js/jquery.totemticker.min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	$("#latest-quotes-slide").totemticker({ row_height  :   '30px'  });
	/*jQuery('#startKitchenQuote').submit(function(){
		var value = jQuery('#postcode_start').val();
		if(value == '')
		{
			jQuery('#postcode_start').addClass('error_msg');
			jQuery('#Required_postcode').css('display', 'block');
			return false;
		}
		else
		{
			setTimeout(function() {
			   window.location.href = "http://buildteam.com/test.html"
			  }, 100);
		}
	});*/
});
</script>
<style>
.form_input.error_msg[name="postcode_start"] {
  border: 2px solid #f00;
}
.post_code_blue {
  background: rgba(35, 74, 125, 0.7) none repeat scroll 0 0;
  color: #fff;
  padding: 8px 0 20px;
  text-align: center;
  width: 60%;
  float:left;
}
.post_code_blue > label {
  float: left;
  margin-bottom: 10px;
  text-align: center;
  width: 100%;
}
.post_code_blue > .form_input {
  height: 32px;
  padding-left: 10px;
  width: 85%;
}
#startKitchenQuote > input[type="submit"] {
  background: #f4641e none repeat scroll 0 0;
  border: medium none;
  color: #fff;
  cursor: pointer;
  float: left;
  font-size: 20px;
  padding: 31px 35px;
}
.clearfix {
	clear:both;
	}
.lightblue-bg .darkblue-txt {
	
	}
#latest_quotes_container {
	padding:0px !important;
	}		
</style>
<div class="full">

  <div class="lightblue-bg newhidden-xs" id="byp_landing_top">
 <!--	<div class="col_full">
		<div class="col_one_fourth">
		 <img src="/images/byp-logo.png" class="byp-logo" />
		</div>
		<div class="col_three_fourth col_last">
			<div id="byp_start_title" class="byp-title">100% Online Kitchen Extension Quote</div>
			<form action="" method="POST" id="startKitchenQuote">
				<div class="post_code_blue">
					<label>Please enter your postcode</label>
					<input type="text" name="postcode_start" id="postcode_start" value="" class="form_input"  class="validate[required]" required="true" pattern ="[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}" placeholder="Enter your postcode"/>
				</div>
				<input type="submit" name="start_quote_here" value="Start Here" />
				<div id="Required_postcode" style="display:none;">Please enter your postcode</div>
			</form>
		</div>
	</div>-->
   <div class="col_three_fourth">
      
      <img src="/images/byp-logo.png" class="byp-logo" />
      <div class="byp-title" id="mob_byp_start_title">100% online side return quote</div>
      <div class="byp_top_wrap">
        <div class="byp-title" id="byp_start_title">100% online side return quote</div>
        <ul class="check byp_landing_list orange">
          <li>Quote in under 60 seconds</li>
          <li>Detailed quote by email</li>
          <li>Computer generated image of your extension</li>
          <li>Booked appointment within 48hrs*</li>
        </ul>
        <div class="orange" style="font-size: 10px;">*Subject to availability</div>
      </div>
    </div>
    <div class="col_one_fourth col_last">
      <div class="byp-title">Side return<br>extensions</div>
      <div class="byp-btn byp-btn-new"><a href="/build-your-price-app.html">Start here</a></div>
      <div class="darkblue-txt" style="margin-top: 10px;"><a <?php if (isset($_SESSION['userid']) && $_SESSION['userid']) echo 'href="/saved_quote.html"'; else echo 'href="javascript:;" class="dialog" '; ?>id="login_link">Retrieve a saved quote</a></div>
    </div>
	<div class="col_full" style="margin-top: 20px;">
    <div id="byp_landing_middle_left"><img src="/images/BYP-kitchen.jpg" /></div>
    <div id="byp_landing_middle_right">
      <a href="/what-we-do/faq.html" class="linknohover"><div class="lightblue-bg"><span class="darkblue-txt">any questions?</span></div></a>
      <div class="" id="latest-quotes-wrap">
        <div class="byp-btn" style="margin: 0 auto;"><a href="/build-your-price-app.html" class="darkblue-txt">Latest quotes</a></div>
        <!--<div style="margin-top: 10px; font-size: 16px; color: #fff"><span class="darkblue-txt">Latest quotes:</span>-->
        <div id="latest_quotes_container" class="lightblue-bg">   
          <ul id="latest-quotes-slide">
          <?php
            $rs = getRs("SELECT postcode, amount_quote, room_size FROM rfq WHERE is_active = 1 AND is_enabled = 1 ORDER BY date_modified DESC LIMIT 0, 10");
            $buf = '';
            while ( $row = mysqli_fetch_assoc($rs) ) {
              $buf .= '<li style="padding-top: 10px;"><a href="/build-your-price-app.html" style="color: #fff; text-decoration: none;">'.$row['postcode'].' '.$row['room_size'].'m2 &pound;'.number_format($row['amount_quote'],0).'</a></li>';
            }
            echo $buf;
          ?>
          </ul>
        </div>
		<!--
        <div id="latest_quotes_src" style="display:none">
			<?php echo $buf; ?>
        </div>
		-->
        </div>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>
  
  <div class="newvisible-xs" id="byp_landing_top">
 <!--	<div class="col_full">
		<div class="col_one_fourth">
		 <img src="/images/byp-logo.png" class="byp-logo" />
		</div>
		<div class="col_three_fourth col_last">
			<div id="byp_start_title" class="byp-title">100% Online Kitchen Extension Quote</div>
			<form action="" method="POST" id="startKitchenQuote">
				<div class="post_code_blue">
					<label>Please enter your postcode</label>
					<input type="text" name="postcode_start" id="postcode_start" value="" class="form_input"  class="validate[required]" required="true" pattern ="[A-Za-z]{1,2}[0-9Rr][0-9A-Za-z]? [0-9][ABD-HJLNP-UW-Zabd-hjlnp-uw-z]{2}" placeholder="Enter your postcode"/>
				</div>
				<input type="submit" name="start_quote_here" value="Start Here" />
				<div id="Required_postcode" style="display:none;">Please enter your postcode</div>
			</form>
		</div>
	</div>-->
   <div class="col_three_fourth">
      <div class="byp-title titlefontfamily" id="mob_byp_start_title">online side return quote</div>
      <div class="mobilelogo"><img src="/images/byp-image.jpg" class="byp-logo" /></div>
      <div class="byp-btn"><a href="/build-your-price-app.html"><img src="/images/start-btn.png" alt="Start"></a></div>
      <div class="byp_top_wrap">
        <div class="byp-title" id="byp_start_title">100% online side return quote</div>
        <ul class="check byp_landing_list orange">
          <li><div class="darkblue-txt" style="margin-top: 10px;"><a <?php if (isset($_SESSION['userid']) && $_SESSION['userid']) echo 'href="/saved_quote.html"'; else echo 'href="javascript:;" class="dialog" '; ?>id="login_link">Retrieve a saved quote</a></div></li>
          <li class="darkgrey-txt">Quote in under 60 seconds</li>
          <li class="darkblue-txt">Detailed quote by email</li>
          <li class="darkgrey-txt">Booked appointment within 48hrs<span class="turnorange">*</span></li>
          <li class="darkblue-txt">Computer generated <br>image of your extension</li>
          <li><div class="orange" style="font-size: 10px;">*Subject to availability</div></li>
        </ul>
        
      </div>
    </div>
   <!-- <div class="col_one_fourth col_last">
      <div class="byp-title">Side return<br>extensions</div>
      
      
    </div> -->

  </div>
    
  <div class="col_full newvisible-xs" style="margin-top: 20px;">
    <div id="byp_landing_middle_left"><img src="/images/BYP-kitchen.jpg" /></div>
    <div id="byp_landing_middle_right">
      <a href="/what-we-do/faq.html" class="linknohover titlefontfamily"><div class="lightblue-bg"><span class="darkblue-txt topandbottom">any questions?</span></div></a>
      <div class="paddedwidth" id="latest-quotes-wrap">
        <div class="byp-btn" style="margin: 0 auto;"><a href="/build-your-price-app.html" class="darkblue-txt">Latest quotes</a></div>
        <!--<div style="margin-top: 10px; font-size: 16px; color: #fff"><span class="darkblue-txt">Latest quotes:</span>-->
        <div id="latest_quotes_container">   
          <ul id="latest-quotes-slide">
          <?php
            $rs = getRs("SELECT postcode, amount_quote, room_size FROM rfq WHERE is_active = 1 AND is_enabled = 1 ORDER BY date_modified DESC LIMIT 0, 10");
            $buf = '';
            while ( $row = mysqli_fetch_assoc($rs) ) {
              $buf .= '<li style="padding-top: 10px;"><a href="/build-your-price-app.html" style="color: #fff; text-decoration: none;">'.$row['postcode'].' '.$row['room_size'].'m2 &pound;'.number_format($row['amount_quote'],0).'</a></li>';
            }
            echo $buf;
          ?>
          </ul>
        </div>
		<!--
        <div id="latest_quotes_src" style="display:none">
			<?php echo $buf; ?>
        </div>
		-->
        </div>
      </div>
      
    </div>
  </div>
  
<div id="login_dialog" style="display:none">
  <div class="lightbox_popup">
    <div class="lightbox_inner">
      <div class="lightbox_content">
        <h3 class="login_title">Log In</h3>
        <p id="user_email_msg" class="user_email_msg">Please log in if you have already registered, if not - please <a href="/build-your-price-app.html">get your first online quote</a>!</p>
        <p><label>Email:</label> <input type="text" class="login_email" value="" /></p>
        <p class="login_pwd_p"><label>Password:</label> <input class="login_pwd" type="password" onkeypress="if(event.keyCode==13){loginUserRequest('saved_quote');}" />
        </p>
        <p><input type="button" value="Log In" class="login_btn" onclick="loginUserRequest('saved_quote')" /> <img src="/images/byp/loading2.gif" alt="Loading" class="login_loading" style="display:none" /></p>
        <p style="clear:both;text-align:center"><a href="javascript:;" onclick="forgotPassword()" class="login_forgot_link">Forgot password?</a></p>
      </div>
    </div>
  </div>
</div>

</div>

<?php

require_once('./inc/footer.inc.php');

?>