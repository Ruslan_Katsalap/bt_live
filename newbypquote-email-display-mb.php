<?php

require_once ('./inc/util.inc.php');
//header('Location: newbyp-quote.html?totalcost='.$_GET['totalcost']);
if (isset($_GET['id']) && ($_GET['id']=(int)$_GET['id']) && isset($_GET['code'])) {
  
  $out = array();
  
  $sql = "SELECT * FROM byp_Quote WHERE id={$_GET['id']} AND refCode='".mysqli_real_escape_string($dbconn,$_GET['code'])."'";
  
  $rs = getRs($sql);
  
  if (!mysqli_num_rows($rs)) {
    $out['msg'] = 'ERR';
    $out['err'] = 'Incorrect quote ID!';
  }
  else {
    $a_rfq = mysqli_fetch_assoc($rs);
    $out['msg'] = 'OK';
    $out['id'] = $a_rfq['id'];
    
    $qid = (int)$a_rfq['id'];
    
    $sql = "SELECT admin_email, design_discount, design_cost, offer_exp_days, url_images, is_notify_admin, poa_message, notification_message, territory_options FROM setting WHERE setting_id = 1";
    $rs = getRs($sql);

    $a_cfg = mysqli_fetch_assoc($rs);
    
    $a_cfg['territory_options'] = unserialize($a_cfg['territory_options']);
    
    ob_start();
    
    include ('./newbypquatep.php');
    
    $buf = ob_get_contents();
    ob_end_clean();
    
    $buf = str_replace('src="/images/', 'src="images/', $buf);
    $buf = str_replace('src="/byp/', 'src="byp/', $buf);
    
    include_once("./dompdf/dompdf_config.inc.php");
    
    $dompdf = new DOMPDF();
    $dompdf->load_html($buf);
    $dompdf->set_paper('letter', 'portrait');
    $dompdf->render();
    
    $path = dirname(__FILE__).'/';
    
    $filename = 'Quote_'.str_replace('/', '-', $a_rfq['refCode']).'.pdf';
    echo $outfile = $path.'pdf/'.$filename;
    
    file_put_contents($outfile, $dompdf->output( array("compress" => 0) ));
    
    $a_user = array();
    
    $a_user['firstname'] = $a_rfq['fname'];
   $a_user['email'] = $a_rfq['email'];
    
    /* if (isset($a_rfq['userid']) && $a_rfq['userid']) {
      $sql = "SELECT firstname, email FROM `user` WHERE id=".(int)$a_rfq['userid'];
      $rs = getRs($sql);
      if (mysqli_num_rows($rs)) $a_user = mysqli_fetch_assoc($rs);
    } */
    
    require_once ('./phpmailer/PHPMailerAutoload.php'); // class.phpmailer.php

    $email_subject = "Build Team: Side Return Extension Quote {$a_rfq['refCode']}";
    
    $mail = new PHPMailer();

    $mail->From = $a_cfg['admin_email'];

    $mail->FromName = 'Build Team';
    
    $mail->AddAddress($a_rfq['email'], $a_user['firstname']);
	//echo $a_rfq['email'];
   //echo  $a_user['email'];
   //$mail->AddAddress("ironias41@gmail.com", $a_user['firstname']);
    //$mail->AddAddress("ironias@yandex.ru", $a_user['firstname']);
    
    // Territory custom:
    $territory = $a_cfg['territory_options'];
    
    if ($territory) {
      $postpart = $a_rfq['postcode'];
      $postpart = str_replace(' ', '', $postpart);
      if (strlen($postpart)<6) $postpart = substr($postpart, 0, 2);
      elseif (strlen($postpart)==6) $postpart = substr($postpart, 0, 3);
      else $postpart = substr($postpart, 0, 4);
      
      foreach ($territory AS $k => $v) {
        $v['postcodes'] = explode(';', $v['postcodes']);
        foreach ($v['postcodes'] AS $k2 => $v2) {
          $v['postcodes'][$k2] = trim($v2);
        }
        
        if (in_array($postpart, $v['postcodes'])) {
          $active_territory = $k;
          break;
        }
        
      }
      
    }
    


    $mail->IsHTML(true); // send as HTML

    $mail->Subject = $email_subject;
    
    if (!$active_territory) {
      
      if (!is_array($a_cfg['notification_message'])) {
        
        if (!strpos($a_cfg['notification_message'], '<div style="page-break-after:')) {
        
          $mail->Body = $a_cfg['notification_message'];
        
        }
        else {
          $a_cfg['notification_message'] = preg_split('/\<div style\="page\-break\-after\:.+?\<\/div\>/s', $a_cfg['notification_message']);
          
          $mail->Body = $a_cfg['notification_message'][0];
        }
        
      }
      else {
        
        $mail->Body = $a_cfg['notification_message'][0];
        
      }
      
    }
    else {
      
      $mail->Body = $territory[$active_territory]['notification_message'];
      
    }
    
    $mail->Body = str_replace('[firstname]', $a_user['firstname'], $mail->Body);
    
    preg_match_all('/ src="(.+?)"/i', $mail->Body, $found, PREG_SET_ORDER);
    
    // embedded images
    /*if ($found) {
      foreach ($found AS $k => $v) {
        $v[1] = str_replace('http://'.$_SERVER['HTTP_HOST'], '', $v[1]);
        
        $mail->Body = str_replace($v[0], ' src="cid:image'.($k+1).'"', $mail->Body);
        
        $imagename = explode('/', $v[1]);
        $imagename = array_pop($imagename);
        
        $mail->AddEmbeddedImage('.'.$v[1], 'image'.($k+1), $imagename);
      }
    }*/
    
    $mail->Body = '<!DOCTYPE html><html xmlns:v="urn:schemas-microsoft-com:vml"><head><meta http-equiv="X-UA-Compatible" content="IE=edge" /><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head><body>' . $mail->Body . '</body></html>';
    
    echo $mail->Body;
    exit;
    
    
	}
    
    
  }
  



?>