<?php

require_once('./inc/header.inc.php');
?>
<style>
/* Css for gallery */

.col_half a {
    position: relative;
    width: 100%;
    display: block;
}
.col_half img {
    border: 0;
    /* max-width: 450px !important; */
    width: 100% !important;
	max-width: inherit !important;
}
</style>
<div class="full">
<h1>gallery of kitchen extensions</h1><br/>

<?php

$i = 1;
$ret = '';
$project_category_code = 'side-return-extensions';
$project_code = '';

if ( isset($_GET['p']) ) {
	//$project_code = $_GET['p'];
}


$rs = getRs("SELECT c.project_category_code, c.project_category_name, i.filename, p.project_code, p.project_name FROM project_category c INNER JOIN (project_image i INNER JOIN project p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 AND c.project_category_code = '" . formatSql($project_category_code) . "' ORDER BY p.sort, p.project_id, i.sort, i.project_image_id");

$cols = 2;
$project_count = 0;
$prev_count = 0;

while ($row = mysqli_fetch_assoc($rs) ) {
	if ( $i == 1 ) {
		//echo '<div id="bc"><a href="/index.html">Home</a> &rsaquo; <a href="/our-experience.html">Our Experience</a> &rsaquo; <b>' . $row['project_category_name'] . '</b></div>';
	}
	
    
		if ($project_code != $row['project_code']) {
			$project_code = $row['project_code'];
      $project_count++;
		}
		else {
			//$ret .= 'display:none;';
      continue;
		}
		
		$ret .= '<'.'div class="col_half'. ($i%2==1 ? '' : ' col_last') .'" style="text-align:center';
    
    // href="/projects/' . $row['filename'] . '" rel="shadowbox[project_' . $row['project_code'] . ']"
		$ret .= '"><a href="/'.$row['project_code'].'-side-return-extension.html" title="' . htmlentities($row['project_name']) . '"><img src="/phpthumb/phpThumb.php?src='.urlencode('/projects/' . $row['filename']) . '&amp;w=450&amp;h=400" style="width:100%;max-width:450px" alt="' . htmlentities($row['project_name']) . '" /><span style="display:block;margin-top:5px">'.htmlentities($row['project_name']).'</span></a></div>'; // debug: ' '.$project_count
    
    if (0==($project_count % $cols) && $project_count!=$prev_count) {
      $ret .= '<br clear="all" />'; // debug: ($project_count % $cols).'!!!
      $prev_count = $project_count;
    }
	$i += 1;
}

echo $ret;

?>


<br clear="all"/>
<p style="text-align:justify">
We warmly welcome you to have a look at some of our side extension projects which have been completed with amazing results. We are very proud to present you with the side return kitchen extensions which have made our clients content. Happy to have been trusted with proposing these modern house designs and to be able to design and build them as well, we hope that you too will let us architect your home. Our construction company in London, Build Team, is a professional, dedicated group of people with amazing skills and talent for building permitted development extensions with great architectural design at competitive prices. We offer &quot;turn key&quot; solutions, which save our clients time and money and, as you can see, the side extensions presented in our gallery speak for themselves.
</p>

</div>

<?php

require_once('./inc/footer.inc.php');

?>