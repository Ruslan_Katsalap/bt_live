<?php

if (is_int(strpos($_SERVER['REQUEST_URI'], '/news/latest-news.html')) || is_int(strpos($_SERVER['REQUEST_URI'], '/news/news.html'))) {
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: http://'.$_SERVER['HTTP_HOST'].'/news.html');
  exit;
}

require_once('./inc/util.inc.php');

// Pagination custom
include './inc/my_pagina_class.php';

class Pagina extends MyPagina {
  function __Pagina() {
    // to prevent DB connection new creation
  }
}

define('QS_VAR', 'p'); // the number of records on each page
define('NUM_ROWS', 1000); // the number of records on each page
define('STR_FWD', '&raquo;'); // the string is used for a link (step forward)
define('STR_BWD', '&laquo;'); // the string is used for a link (step backward)
define('NUM_LINKS', 10); // the number of links inside the navigation (the default value)
$nav = new Pagina;


$show_detail = false;

$news_code = '';

$title = '';

$imagefile = '';

$description = '';



if ( isset($_GET['c']) ) {

	$c = safe($_GET['c']);

	$rs = getRs("SELECT n.news_id, n.news_code, n.title, n.imagefile, n.description, n.date_release FROM news n INNER JOIN news_category c ON c.news_category_id = n.news_category_id WHERE c.is_enabled = 1 AND c.is_active = 1 AND n.is_enabled = 1 AND n.is_active = 1 AND n.news_code = '" . formatSql($c) . "' order by n.news_category_id DESC");

	while ( $row = mysqli_fetch_assoc($rs) ) {

		$show_detail = true;

		$news_code = $row['news_code'];

		$title = $row['title'];

		$imagefile = $row['imagefile'];

		$description = $row['description'];

		$meta_title = $row['title'];

		$date_release = $row['date_release'];

	}

}



require_once('./inc/header.inc.php');



if ( $show_detail == false ) {



?>
<!--<link href="http://lopatin.github.io/sliderTabs/styles/jquery.sliderTabs.min.css" rel="stylesheet">
<script src="http://lopatin.github.io/sliderTabs/jquery.sliderTabs.min.js"></script>
-->


<style>
.full {
	font-weight:normal;
	}
ul.news li {
	display:inline-block;
	width:335px;
	}
.catfilter {
	list-style:none;
	margin:0px;
	padding:0px;
	border-bottom:2px solid #e5e5e5;
	text-align:left;
	margin-top:20px;
	margin-bottom: 10px;
	}
.catfilter li {
	display:inline-block;
	padding:10px 0px;
	padding-right: 40px;
	text-align:left;
	}
.catfilter li:nth-child(10) {
	}
.catfilter li a{
	font-size:20px;
	color:#2c4976;
}
ul.news li .titleofblog {
	font-size:18px !important;
	font-weight:normal;
	display: inline-block;
    margin: 10px 0px 10px 0px;
	}
ul.news li {
	border-bottom:2px solid #e5e5e5 !important;
	padding-bottom:30px;
	}
ul.news li .datebadge {
	display:block;
	padding:0px 0px 5px 0px;
	margin-bottom:10px;
	border-bottom:2px solid #e5e5e5;
	font-size:18px;
	text-align:right;
	color:#b1b1b1 !important;
	font-weight:normal;
	}
.blogwraps {
    width: 90%;
    margin: 0 auto;
}
.blogwraps .readMore{
	color:#f5641e;
	font-weight:normal;
	}
li.catDispay.active a {
    color:#f5641e;
	}
div#slidertabs {
    margin-bottom: 50px;
}
@media (min-width:640px) {
	.full {
		overflow:hidden;
		}
	.blogwraps img {
    height: 200px;
    width: 300px;
}
li.catDispay:first-child {
    padding-left: 0px;
}
.scroll_tab_inner {
	position:relative !important;
	}

	}
@media (max-width:640px) {
	.full {
    overflow: hidden;
}
.data-post-wrap {
    position: relative;
    width: 100%;
    overflow: hidden;
}
.scroll_tab_left_button {
    left: 10px !important;
}
	}
</style>

  	<div class="full">



			<?php echo $bc_trail; ?>


      <div class="col_five_fifth">
        <h1 class="newblogtitledesk"><strong>Latest News</strong></h1>
	  <div class="mobileheader">
       <h1 class="newblogtitle"><strong>Latest News</strong></h1>

      <ul class="catfiltermobile">
			<?
				$num_rec_per_page=1;
				if (isset($_GET["page"])) { $page  = $_GET["page"]; } else { $page=1; };
				$start_from = ($page-1) * $num_rec_per_page;

				$news_category = mysqli_query($dbconn,"select * from news_category where news_category_id!=10 order by news_category_name DESC LIMIT $start_from, $num_rec_per_page");
			?>
				<li>
				<? $sql = "SELECT * FROM news_category where news_category_id!=10 order by news_category_name DESC";
					$idsArr[] = 0;
					$rs_result = mysqli_query($dbconn,$sql); //run the query
					while($datarow = mysqli_fetch_assoc($rs_result)){
						$idsArr[] = $datarow['news_category_id'];
					}
					$total_records = mysqli_num_rows($rs_result);  //count number of records
					$total_pages = ceil($total_records / $num_rec_per_page/1);

					//echo "<a href='?&page=1'>".'< '."</a> "; // Goto 1st page
					$flag = 0;
					for ($i=$_GET['page']; $i<=$_GET['page']+1; $i++) {
								if($flag == 0 && $_GET['page'] !=1){
									if($_GET['page']!='')
									echo " <a href='?catID=".$idsArr[$i-1]."&page=".($i-1)."'> <</a>  ";
								}
								if($total_pages>=$_GET['page']){
									if($total_pages!=$_GET['page']){
										if($_GET['page']=='')
											$listID = $idsArr[1];
										else
											$listID = $idsArr[$i];
										$nameCat = mysqli_fetch_assoc(mysqli_query($dbconn,"select news_category_name from news_category where news_category_id=$listID"));
									}else{
										$nameCat = mysqli_fetch_assoc(mysqli_query($dbconn,"select news_category_name from news_category where news_category_id=$_GET[catID]"));
									}
									if($flag == 0)
										$nameCat_anchor = $nameCat['news_category_name'];
									else
										$nameCat_anchor = ' >';
									if($total_pages!=$_GET['page'])
									echo " <a href='?catID=".$idsArr[$i]."&page=".$i."'> $nameCat_anchor </a> ";
									else if($flag==1)
									echo " <a href='?catID=".$idsArr[$i]."&page=".$i."'> $nameCat[news_category_name] </a> ";
								}
						$flag = 1;
					};
					//if($total_pages>=$_GET['page'])
					//echo "<a href='?catID=".$idsArr[$total_pages]."&page=$total_pages'>".'>'."</a> "; // Goto last page
?>
				</li>
		</ul>
        <div class="clearfix"></div>
        </div>

	  	 <div id="slidertabs">
		<ul class="catfilter scroll_tabs_theme_dark" id="tabs5">
			<?
			$news_category1 = mysqli_query($dbconn,"select * from news_category where news_category_id!=10 order by news_category_name DESC");
				$a=0;while($ncRow1 = mysqli_fetch_assoc($news_category1)){ $a++;
			?>
					<? /*<li class="catDispay <? if($_GET['catID'] == $ncRow1['news_category_id']){ echo ' active';}?>"><a href="?catID=<?=$ncRow1['news_category_id']?>&page=<?=$a?>"><?=$ncRow1['news_category_name']?></a></li>*/ ?>
					<li data-year="<?=$ncRow1['news_category_name']?>" class="catDispay <? if($_GET['catID'] == $ncRow1['news_category_id']){ echo ' active';}?>"><a href="javascript:void(0)"><?=$ncRow1['news_category_name']?></a></li>
				<? } ?>
		</ul>

	<div class="data-post-wrap">
      <ul class="news <? if($_GET['catID'] == $ncRow1['news_category_id']){ echo ' active';}?>" data-post="<?=date('Y')?>" id="<?=date('Y')?>">

				<?php


        // custom: LIMIT 20
        //$rs = getRs("SELECT c.news_category_code, c.news_category_name, n.news_id, n.news_code, n.title, n.imagefile, n.description, n.date_release FROM news n INNER JOIN news_category c ON c.news_category_id = n.news_category_id WHERE c.is_enabled = 1 AND c.is_active = 1 AND n.is_enabled = 1 AND n.is_active = 1 ORDER BY n.date_release DESC LIMIT 20");
		if($_GET['catID']!='')
			$catByID = " AND n.news_category_id = '".$_GET['catID']."'";
		else if($_GET['catID']=='')
			$catByID = " AND n.news_category_id = '".$idsArr[1]."'";
         //$sql = "SELECT c.news_category_code, c.news_category_name, n.news_id, n.news_code, n.title, n.imagefile,n.smallImage, n.description, n.date_release FROM news n INNER JOIN news_category c ON c.news_category_id = n.news_category_id WHERE c.is_enabled = 1 AND c.is_active = 1 AND n.is_enabled = 1 AND n.is_active = 1 $catByID ORDER BY n.date_release DESC";
		$categoryName =date('Y');
		$newsCat=mysqli_query($dbconn,"select * from news_category ORDER BY news_category_name DESC");
		while($rowN = mysqli_fetch_assoc($newsCat)){
		//$categoryName = $rowN['news_category_name'];
		$sql = "SELECT c.news_category_code, c.news_category_name, n.news_id, n.news_code, n.title, n.imagefile,n.smallImage, n.description, n.date_release FROM news n INNER JOIN news_category c ON c.news_category_id = n.news_category_id WHERE c.is_enabled = 1 AND c.is_active = 1 AND n.is_enabled = 1 AND n.is_active = 1  AND c.news_category_id='".$rowN['news_category_id']."' ORDER BY n.news_id DESC";

        /////////////////////////////////////////////////////
        $nav->sql = $sql; // the (basic) sql statement (use the SQL whatever you like)
        $rs = $nav->get_page_result(); // result set
        $rs = mysqli_query($dbconn,$sql); // result set
        //$num_rows = $nav->get_page_num_rows(); // number of records in result set
        //$nav_links = $nav->navigation(' | ', 'page_style'); // the navigation links (define a CSS class selector for the current link)
        //$nav_info = $nav->page_info('to'); // information about the number of records on page ("to" is the text between the number)
        //$simple_nav_links = $nav->back_forward_link(); // the navigation with only the back and forward links
        //$total_recs = $nav->get_total_rows(); // the total number of records

        // if ($this->is_tpl_exists) $this->nav->free_page_result();

        ///////////////////////////////////////////////////////////////////////


        $i = 0;
        if (isset($_GET[QS_VAR])) $i = $_GET[QS_VAR] * NUM_ROWS;
        while ( $row = mysqli_fetch_assoc($rs) ) {
				//if($i==0)
				//$categoryName = $row['news_category_name'];
				if($categoryName !=$row['news_category_name']){
					$categoryName = $row['news_category_name'];
					echo '</ul><ul class="news" id="'.$categoryName.'" data-post="'.$categoryName.'">';
				}
          $i++;



			if ( !strlen($imagefile) > 0 ) {

        $abspath = str_replace('\\','/',dirname(__FILE__)).'/';

        $a_size = getimagesize($abspath.'/media/'.$imagefile);

				//echo '<img src="/media/' . $imagefile . '" alt="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" title="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" style="width:100%;max-width:'.$a_size[0].'px" />'; // Office Refurbishment London

			}
			$todate = '';
			if ( $row['date_release'] > 0 ) {
				$todate = date('d/m', $row['date_release']);//date('d/m', strtotime($row['date_release']));
			}
			if($row['smallImage'])
				$smallImage = $row['smallImage'];
			else
				$smallImage = "noimage.jpg";
          echo '<li ><div class="blogwraps"><span class="datebadge">'.$todate.'</span><img src="/media/' . $smallImage . '" alt="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" title="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" />
		  <a class="titleofblog" href="/news/' . $row['news_category_code'] . '/' . $row['news_code'] . '.html" title="' . $row['title'] . '" style="font-size: 16px;">' . $row['title'] . '</a><br />
          <span style="font-size:10px;">';
					echo '<a class="readMore" href="/news/' . $row['news_category_code'] . '/' . $row['news_code'] . '.html" title="' . $row['title'] . '" style="font-size: 16px;">Read more</a> ';
					if ( $row['date_release'] > 0 ) {

						//echo date('j F Y', $row['date_release']);

					}

					else {

						echo $row['news_category_name'];

					}

					echo '</span></div></li>';

        }

}

        ?>

			</ul>
            </div>
      </div>
      <div style="margin-top:1em;font-size:1.1em;text-align:left;">
      <?php //echo $nav_links ?>
      </div>

      </div>
      <!--<div class="col_three_fifth col_last" style="text-align:center">
      <style type="text/css">
      	.lightblue-box, .darkblue-box {
      		width: 108px;
      		margin-right: 10px;
      	}

      </style>

      		<img src="/images/news.jpg" style="width: 100%;" />
      		<p>&nbsp;</p>
      <div style="text-align:left">
      		<div class="darkblue-box">find out<br /><span class="orange">more</span></div>
          <div class="lightblue-box"><a href="/build-your-price-start.html">build your<br /><span class="orange">price</span></a></div>
          <div class="lightblue-box"><a href="/what-we-do/guarantees-and-insurance-page.html">10 year<br /><span class="orange">guarantee</span></a></div>
          <div class="lightblue-box" style="margin-right: 0;"><a href="/what-we-do/process.html">8 step<br /><span class="orange">process</span></a></div>
      </div>

    </div>-->

    </div>

<?php

}



else {



define('hidePageMedia', true);



?>
<style>
.newblogtitle {
	max-width:95%;
	}
.dateandtitle .newblogtitle {
	max-width:80% !important;

	}
</style>
<div class="full newfull">
	<div class="dateandtitle">
        <h1 class="newblogtitle"><strong>Latest News: </strong><?php echo $title; ?></h1>
        <div class="dateofpost">
        	<?php
			echo date('d/m', $date_release);
			?>
        </div>
        <div class="clearfix"></div>
        </div>
	<div class="newcolone" style="">


        <div class="blogpostimage">
    	<?php

			if ( strlen($imagefile) > 0 ) {

        $abspath = str_replace('\\','/',dirname(__FILE__)).'/';

        $a_size = getimagesize($abspath.'/media/'.$imagefile);

				echo '<img src="/media/' . $imagefile . '" alt="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" title="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" style="width:100%;max-width:'.$a_size[0].'px" />'; // Office Refurbishment London

			}

			?>
</div>
    </div>
  	<div class="newcolthree">

			<!--<div id="bc"><a href="/">Home</a> &rsaquo; <a href="/news.html">Latest News</a> &rsaquo; <b><?php echo $title; ?></b></div>-->

    	<div class="nweblogdesc">
        	<?php
			//echo date('d/m', $date_release);
			  if (is_int(strpos($description, '<p>'))) echo $description;
			  else echo '<p>'.nl2br($description).'</p>';
			  ?>
		</div>

      <!-- AddThis Button BEGIN -->
      <div class="addthis_toolbox addthis_default_style addthis_16x16_style shareittool">
      <a class="addthis_button_facebook"><img src="http://www.buildteam.com/images/social/facebook.jpg" alt=""></a>
      <a class="addthis_button_google_plusone_share"><img src="http://www.buildteam.com/images/social/insta.jpg" alt=""></a>
      <a class="addthis_button_twitter"><img src="http://www.buildteam.com/images/social/twitter.jpg" alt=""></a>
      <a class="addthis_button_email"><img src="http://www.buildteam.com/images/social/gmail.jpg" alt=""></a>

      <a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
      </div>
      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=dmbiko"></script>
      <!-- AddThis Button END -->

    </div>

    <div class="clearfix"></div>

<div class="latest-posts">
    <h1 class="newblogtitle">Latest Posts</h1>


	<ul class="horizon-swiper">
		<?
			$sql = "SELECT c.news_category_code, c.news_category_name, n.news_id, n.news_code, n.title, n.imagefile,n.smallImage, n.description, n.date_release FROM news n INNER JOIN news_category c ON c.news_category_id = n.news_category_id WHERE c.is_enabled = 1 AND c.is_active = 1 AND n.is_enabled = 1 AND n.is_active = 1 ORDER BY n.date_release DESC limit 8";
        	$rs = mysqli_query($dbconn,$sql);
			while ( $row = mysqli_fetch_assoc($rs) ) {
			  $i++;
			  echo '<li class="horizon-item"><div class="blogwraps"><span class="datebadge">'. date('d/m', $row['date_release']).'</span><img src="/media/' . $row['smallImage'] . '" alt="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" title="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'"/>
		  <a class="titleofblog" href="/news/' . $row['news_category_code'] . '/' . $row['news_code'] . '.html" title="' . $row['title'] . '" style="font-size: 16px;">' . $row['title'] . '</a><br />
          <span style="font-size:10px;">';



						echo '</span></div></li>';

			}

		?>
	</ul>

	</div>
</div>

    </div>

		<script type="text/javascript" src="https://www.buildteam.com/js/horizon-swiper.js"></script>
<script type="text/javascript">
( function ( $ ) {
  'use strict';
			$('.horizon-swiper').horizonSwiper( {
    	showItems:3
  	});
  })( jQuery );
		</script>





<?php } ?>
 <script src="https://www.jqueryscript.net/demo/Awesome-Scrolling-For-Wide-Tab-Interface-Applications-ScrollTabs/examples/js/bootstrap.min.js"></script>
<script src="https://www.jqueryscript.net/demo/Awesome-Scrolling-For-Wide-Tab-Interface-Applications-ScrollTabs/js/jquery.scrolltabs.js"></script>
<script src="https://www.jqueryscript.net/demo/Awesome-Scrolling-For-Wide-Tab-Interface-Applications-ScrollTabs/js/jquery.mousewheel.js"></script>

<script>
$(document).ready(function() {
     // $('#tabs5').scrollTabs();
});
$(window).on('load', function() {
 // $("div.scroll_tab_inner .active a").click();
});
</script>
<? require_once('./inc/footer.inc.php');?>

