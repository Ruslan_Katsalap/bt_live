<?php require_once('./inc/header.inc.php'); ?>

<?php require_once('save_do_it_with_our_help.php'); ?>
<link rel="stylesheet" href="css/quiz.css" type="text/css">
<link rel="stylesheet" href="css/slick.css" type="text/css">
<style>
	.titlehowwecan h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
}	
.howwedesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #74787f;
    line-height: 24px;
    font-weight: normal;
}
.fliboocwrap {
	background:#f2f5f8;
	height:400px;
	widows:100%;
	margin-top:50px;
	}
.tenderprocessinner {
    max-width: 980px;
    margin: 0 auto;
}
.tenderpprocesstitle {
    text-align: center;
}
.tenderpprocesstitle h1 {
    font-weight: normal;
}
.tenderpprocesstitle h1 span {
    font-weight: bold;
}
.tenderprocess {
    padding: 30px 0px;
}
.tenderslider {
    text-align: center;
}
.processnumber p {
    color: #74787f;
    font-size: 16px;
}
.processnumber p span {
    font-weight: bold;
    color: #0e4776;
}
.procestitle h3 {
    font-size: 20px;
    font-weight: bold;
    color: #0f4778;
}
.processdesc span {
    font-size: 16px;
    color: #9e9e9e;
    margin-top: 5px;
    display: inline-block;
}	
.processpic {
    margin: 10px 0px;
	text-align:center;
}
.processpic img {
    width: 130px;
    border-radius: 100px;
    margin: 0 auto;
}
.helpmechoose a {
    background: #f2f5f8;
    padding: 15px 31px;
    font-size: 20px;
    color: #0a4f7c;
    letter-spacing: 1px;
}
button.slick-prev.slick-arrow {
    position: absolute;
    top: 30%;
    background: none;
    color: #FFF;
    border: none;
    height: 30px;
    /* width: 30px; */
    left: -30px;
    background: #0f4777;
}
button.slick-next.slick-arrow {
    position: absolute;
    top: 30%;
    background: none;
    color: #FFF;
    border: none;
    height: 30px;
    /* width: 30px; */
    right: -30px;
    background: #0f4777;
}
.helpmechoose a span {
    font-weight: bold;
}
.helpmechoose {
    position: absolute;
    left: -83px;
	top:200px;
    transform: rotate(-90deg);
}
.up_down {
	display:none;
	}
.contractmanage h2 {
    font-size: 22px !important;
    padding-bottom: 00px !important;
    margin-top: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
}
.contractmanage p span {
    color: #003c70;
}
.contractmanage p {
    padding-top: 5px;
    font-size: 20px;
    text-align: justify;
    color: #74787f;
    line-height: 24px;
    font-weight: normal;
}	
.formssection .cols-sm-3:nth-child(2) {
    margin-left: 12px;
    margin-right: 12px;
}
.formssection .cols-sm-3 {
    float: left;
    width: 32.33%;
}
.formssection input {
    width: 100%;
    padding: 0px;
    height: 40px;
    background: #f2f5f8;
    border: none;
    border-radius: 5px;
    text-indent: 10px;
    margin-bottom: 10px;
}
.formssection input[type=submit] {
	 background: #003c70;
	 color:#FFF;
	 text-indent:0px;
	}
.contractform h2 {
    color: #114577;
    font-size: 18px;
    margin-top: 30px;
    margin-bottom: 20px;
}	
.broform100.checkboxbro.broleft input {
    width: 15px;
    height: 15px;
    display: inline-block;
	vertical-align:middle;
}
.broform100.checkboxbro.broleft span {
    display: inline-block;
    width: 96%;
    text-align: justify;
    vertical-align: middle;
    font-size: 13px;
}
.thanksinner p {
    font-size: 14px;
    padding-top: 15px;
}
.thanksinner {
	height:100px;
	}
.formssection .cols-sm-6 {
    float: left;
    width: 66%;
    margin-left: 1%;
}	

.cols-sm-12 {
    width: 100%;
}
@media (max-width:768px){
	.formssection .cols-sm-3 {
    float: left;
    width: 100%;
}
.formssection .cols-sm-3:nth-child(2) {
    margin-left:0px;
    margin-right:0px;
}
.broform100.checkboxbro.broleft {
    margin-bottom: 10px;
}
.formssection .cols-sm-6 {
	width:100%;
	}
.broform100.checkboxbro.broleft span {
    display: inline-block;
    width: 91%;
    text-align: justify;
    vertical-align: top;
    font-size: 13px;
}	
	}	
</style>
<div class="howwecanhelp">
    	<div class="titlehowwecan">
        	<h2>Contract Management</h2>
        </div>
        <div class="howwedesc">
        	<p>If you want to invest your own time in Project Managing your own build to keep the costs down, we can help. In this case we simply offer our professional guidance and assist with the complex stuff, like getting the details of a contract agreed. We also inspect the build at those key stages to ensure the quality is up to scratch.</p>
        </div>
    </div>

<div class="contractmanage">
	<div class="contractinner">
    	<h2>What’s included?</h2>
        <p><span>•</span> Management of the contract during the Build</p>
        <p><span>•</span> Quality inspections and progress reports</p>
    </div>
</div> 
<div class="contractform">
	<h2>Want more information? Book a meeting with our experts:</h2>
    <form method="post">
    	<div class="formssection">
    		<div class="cols-sm-3">
            	<input type="text" name="name" required placeholder="Name*">
            </div>
            <div class="cols-sm-3">
            	<input type="text" name="address" required placeholder="Address*">
            </div>
            <div class="cols-sm-3">
            	<input type="text" name="postcode" required placeholder="Postcode*">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="formssection">
    		<div class="cols-sm-3">
            	<input type="email" name="email" required placeholder="Email*">
            </div>
            <div class="cols-sm-3">
            	<input type="tel" name="telephone" required placeholder="Telephone*">
            </div>
            <div class="cols-sm-3">
            	<input type="text" name="status" required placeholder="Status*">
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="formssection">
    		<div class="cols-sm-3">
            	<input type="text" name="message" placeholder="Message">
            </div>
            <div class="cols-sm-6">
            	<input type="submit" value="Submit">
            </div>
            
            <div class="clearfix"></div>
        </div>
        <div class="formssex=ction">
        	<div class="cols-sm-12">
            	<div class="broform100 checkboxbro broleft">
                       <input type="checkbox" name="ourhelpSubscribe" value="Yes"><span>Sign up to our newsletter and receive updates on our latest blogs, news items and special offers – see our <a href="https://www.buildteam.com/privacy-policy.html">privacy policy</a> for more information on how we use your personal data.</span>
                    </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </form>
</div>
   

<div class="helpmechoose">
	<a href="#"><span>HELP ME</span> CHOOSE</a>
</div>  
	<div class="quizwrap">
	<div class="quizinner">
    	<div class="quizesection">
        	<form id="example-advanced-form" action="#">
                <h3></h3>
                <fieldset>
                	<div class="innerfeild">
                    	<div class="fieldtablecell">
                        	 <legend>Have you got planning permission and detailed drawings?</legend>
                                <p>Please pick one to continue</p>
                                <label>
                                <input type="radio" id="opt1" name="planningpermission" value="Yes">
                                <span>Yes</span>
                                </label>
                                <label>
                                <input type="radio" id="opt2" name="planningpermission" value="No">
                                <span>No</span>
                                </label>
                        </div>
                    </div>
                   
                </fieldset>
                
                 <h3></h3>
                <fieldset>
                	<div class="innerfeild">
                    	<div class="fieldtablecell">
                        	 <legend>Who did you use for your drawings? </legend>
                                
                                <label>
                                <input type="radio" name="fordrawing" value="Design Team (Build Team's sister Company)">
                                <span>Design Team (Build Team's sister Company)</span>
                                </label>
                                <label>
                                <input type="radio" name="fordrawing" value="An Architect">
                                <span>An independant Architect</span>
                                </label>
                                <label>
                                <input type="radio" name="fordrawing" value="Other">
                                <span>Other</span>
                                </label>
                        </div>
                    </div>
                   
                </fieldset>
             
                             
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                    <legend>Have you completed a home extension project before?</legend>
                    <label>
                    <input type="radio" name="experience" value="No, this is my first time">
                    <span>No, this is my first time</span>
                    </label>
                    <label>
                    <input type="radio" name="experience" value="Yes, but I wasn't heavily involved in the process">
                    <span>Yes, but I wasn't heavily involved in the process</span>
                    </label>
                    <label>
                    <input type="radio" name="experience" value="IYes, I was very involved in the process and know what to expect">
                    <span>Yes, I was very involved in the process and know what to expect</span>
                    </label>
                   
                    </div>
                    </div>
                </fieldset>
                
                 <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                    <legend>Do you want adequate insurance during the build?</legend>
                    <label>
                    <input type="radio" name="insurance" value="No">
                    <span>No</span>
                    </label>
                    <label>
                    <input type="radio" id="someof1" name="insurance" value="If it keeps cost down, I don't need it">
                    <span>If it keeps cost down, I don't need it</span>
                    </label>
                    <label>
                    <input type="radio" id="yesinsurance2" name="insurance" value="Yes">
                    <span>Yes</span>
                    </label>
                    
                    </div>
                    </div>
                </fieldset>
             
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>Where do you intend to live during the build?</legend>										  					<label>
                     <input id="1no" type="radio" name="live" value="In the property">
                     <span>In the property</span>
                    </label>
                    
                   <label>
                    <input id="2no" type="radio" name="live" value="I'll move out for the worst of it">
                    <span>I'll move out for the worst of it</span>
                   </label>
                   <label> 
                    <input id="3yes" type="radio" name="live" value="I'll move out for the entire build">
                    <span>I'll move out for the entire build</span>
                   </label>
                   </div>
                   </div> 
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>What level of involvement would you like during the Build?</legend>										  					
                     <label>
                     <input type="radio" name="involvement" id="involvment1" value="None at all, I'm very busy and just want to be updated of progress">
                     <span>None at all, I'm very busy and just want to be updated of progress</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment2" value="Some involvement, I'd like to visit site once a week to see how it's going">
                    <span>Some involvement, I'd like to visit site once a week to see how it's going</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment3" value="Lots of involvement, I have time to manage the project day to day">
                    <span>Lots of involvement, I have time to manage the project day to day</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment4" value="I live abroad, so I won't be around at all">
                    <span>I live abroad, so I won't be around at all</span>
                    </label>
                    </div>
                    </div>
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>What is your budget?</legend>										  					
                     <label>
                     <input type="radio" name="budget" id="budget1" value="Less than £30,000">
                     <span>Less than £30,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget2" value="Between £30,000 and £50,000">
                    <span>Between £30,000 and £50,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget3" value="Between £50,000 and £100,000">
                    <span>Between £50,000 and £100,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget4" value="Between £100,000 and £150,000">
                    <span>Between £100,000 and £150,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget5" value="Over £100,000">
                    <span>Over £150,000</span>
                   </label>
                   </div>
                   </div>
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>Our Recommendation</legend>										  					
                     
                     <div class="ourrecomendationdiv selectedreco">
                     	<div class="leftreco">
                        	<p id="reccomendation"></p>
                        </div>
                        <div class="descreco">
                        	<p>Sit back and relax! We will take care of everything. You’ll be assigned one dedicated Project Manager who will keep you updated throughout the process and ensure things are running smoothly. They’ll be onsite most days to ensure things are pushing along nicely. We have public liability insurance and a 10 year guarantee, so you have absolute reassurance both during and after the build is complete.</p>
                        	<!--<div class="detailereco">
                            	<h2>• 10 Year Guarantee</h2>
                                <p>we offer a 10 year guarantee</p>
                            </div>
                            <div class="detailereco">
                            	<h2>• Project Management</h2>
                                <p>we handle all of the hassle</p>
                            </div>
                            <div class="detailereco">
                            	<h2>• Public Liability Insurance</h2>
                                <p>giving you peace of mind</p>
                            </div>-->
                        </div>
                        <div class="rightreco">
                        	<a href="#">FIND OUT <span>MORE</span></a>
                        </div>
                        
                     </div>
                    <div class="otherbundles">
                    	<p>Our other bundles:</p>
                    </div> 
                     <div class="ourrecomendationdiv otherbudles">
                     	<div class="leftreco">
                        	<div class="inlineone"><p>• <span id="otherbudle2">Do it with our help</span> </p>
                            	<div id="otherllink2" class="linkright">
                                	<a href="#">FIND OUT <span>MORE</span></a>
                                 </div>
                            </div>
                        </div>
                     </div>
                     <div class="ourrecomendationdiv otherbudles">
                     	<div class="leftreco">
                        	<div class="leftreco">
                        	<div class="inlineone">
                            <p>• <span id="otherbudle3">Build with us</span></p>
                            	<div id="otherllink3" class="linkright">
                                	<a href="#">FIND OUT <span>MORE</span></a>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                     </div>
                     <!--div class="recoprefer">
                     	<p>Prefer to speak with a member of the team?</p>
                        <a href="#" class="request-call">REQUEST A CALL BACK</a>
                     </div-->
                     </div>
                     </div>
                </fieldset>
                
            </form>
        </div>
        <div class="closebuttonquiz"><i>CLOSE AND RETURN TO THE SITE</i> <span>x</span></div>
        <div class="backtostepone"><img src="/images/build_phase/Back-Button.png" alt=""><i>TAKE ME BACK TO THE BEGINNING</i></div>
    </div>
</div>
<div class="thankyoupop thanksdoit">
    	<div class="thanksinner">
        	<h4>Thank you for your request.</h4>
            <p>We will contact you within 1-2 working days.</p>
			<a onClick="$("thankyoupop").fadeOut(300);" href="javascript:void(0);">Ok</a>
            
        </div>
    </div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script src="js/jquery.steps.min.js"></script>

<script>
	var form = $("#example-advanced-form").show();
 
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
	transitionEffectSpeed:30,
    onStepChanging: function (event, currentIndex, newIndex)
    {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex)
        {
            return true;
        }
        // Forbid next action on "Warning" step if the user is to young
        if (newIndex === 3 && Number($("#age-2").val()) < 18)
        {
            return false;
        }
        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex)
        {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
		
		//else if (currentIndex === 0 && (!$('#opt1').is(':checked'))) return false;
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
		//return true;
    },
    onStepChanged: function (event, currentIndex, priorIndex)
    {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
        {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3)
        {
            form.steps("previous");
        }
		if (currentIndex === 7 ){
			
			$(document).find(".actions").hide();
			$(document).find(".steps").hide();
			$(".backtostepone").fadeIn(300);
			$(".backtostepone").click(function(){
				//form.steps("setStep", 1);
				$(this).fadeOut(300);
				$( "#example-advanced-form" ).steps('reset');
				$(document).find(".actions").show();
				$(document).find(".steps").show();
				});
			}
		if (currentIndex === 1 && $('#opt2').is(":checked")) {
			steping = 1;
			for(var i=0; i<steping; i++) {
				console.log(i);
					form.steps("next");
				}
			} 
				
		if (currentIndex === 1 && $('#opt2').is(":checked")) {
			//form.steps("previous");
			$('#opt2').prop('checked', false);
			//form.steps("previous");
		}
		
		if ($("#yesinsurance2").is(":checked")) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if ($("#involvment1").is(":checked") || $("#involvment4").is(":checked")) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}	
		if ($("#yesinsurance2").prop('checked') == false && $("#involvment2").is(":checked")) {
			$("#reccomendation").text("Do it with our help");
			$(".rightreco a").attr("href", "/do_it_with_our_help.html");
			$("#otherbudle2").text("Do it yourself");
			$("#otherbudle3").text("Build with us");
			$("#otherllink2 a").attr("href", "/do_it_yourself.html");
			$("#otherllink3 a").attr("href", "/build_with_us.html");
			}	
		if ($("#yesinsurance2").prop('checked') == false && $("#involvment3").is(":checked")) {
			$("#reccomendation").text("Do it yourself");
			$(".rightreco a").attr("href", "/do_it_yourself.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Build with us");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/build_with_us.html");
			}
		if (($("#involvment2").prop('checked') == true && $("#budget3").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget3").is(":checked")) || 
			($("#involvment2").prop('checked') == true && $("#budget4").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget4").is(":checked")) || 
			($("#involvment2").prop('checked') == true && $("#budget5").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget5").is(":checked"))
			) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if (($("#involvment2").prop('checked') == true && $("#budget1").is(":checked") && $("#yesinsurance2").prop('checked') == false)) {
			$("#reccomendation").text("Do it with our help");
			$(".rightreco a").attr("href", "/do_it_with_our_help.html");
			$("#otherbudle2").text("Build with us");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/build_with_us.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if (($("#involvment3").prop('checked') == true && $("#budget2").is(":checked") && $("#yesinsurance2").prop('checked') == false)) {
			$("#reccomendation").text("Do it yourself");
			$(".rightreco a").attr("href", "/do_it_yourself.html");
			$("#otherbudle2").text("Build with us");
			$("#otherbudle3").text("Do it with our help");
			$("#otherllink2 a").attr("href", "/build_with_us.html");
			$("#otherllink3 a").attr("href", "/do_it_with_our_help.html");
			}									
	
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
		
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
		
    }
}).validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        planningpermission: {
		   required: true
		}
		,budget: {
			required: true
			},
		involvement: {
			required: true
			},
		live: {
			required: true
		},
		insurance: {
			required: true
		},
		experience: {
			required: true
		},
		fordrawing: {
			required: false
		}	
    },
	messages: {
		planningpermission: {
		  //minlength: jQuery.format("Zip must be {0} digits in length"),
		  //maxlength: jQuery.format("Please use a {0} digit zip code"),
		  required: "Please select an option to continue."
		},
		budget: {
			required: "Please Select option to continue"
			},
		involvement: {
			required: "Please Select option to continue"
			},
		live: {
			required: "Please select an option to continue."
			},
		insurance: {
			required: "Please select an option to continue."
				},	
		experience: {
			required: "Please select an option to continue."
			},	
		fordrawing: {
			required: "Please select an option to continue."
			}	
		  }
});

	//alert(currentIndex + " ok");
	/*$('a[href="#next"]').click(function(){
		
		function checkindex(event, currentIndex, newIndex){
			alert(currentIndex + " ok");
			}
		
		});
		if (currentIndex === 2 && $('#opt2').is(":checked")) {
			
				$('#opt2').prop('checked', false);
				steping2 = 2;
				
				for(var i=0; i<steping2; i++) {
					console.log(i);
					form.steps("previous");
				}
				
				}*/

	
	

 //$("#example-advanced-form-p-1").hide();
 //$("#example-advanced-form").steps("remove", Number(1));
 


</script>
<script>
$(".helpmechoose").click(function(){
	$(".quizwrap").animate({left:"0%"});
	});
$(".closebuttonquiz").click(function(){
	$(".quizwrap").animate({left:"-100%"});
	});	
</script>
<script>
$(document).ready(function(e) {
    <? if($doitflage == true){?>
$(".thanksdoit").fadeIn(200);
<? } ?>
});

</script>


<?php require_once('./inc/footer.inc.php'); ?>