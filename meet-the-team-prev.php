<?php

require_once('./inc/util.inc.php');

$show_detail = false;
$team_code = '';
$name = '';
$title = '';
$imagefile_lg = '';
$description = '';


if ( isset($_GET['c']) ) {

	$c = safe($_GET['c']);
  
  /*$sql = "SET NAMES 'utf8'";
  setRs($sql);*/
  
	$rs = getRs("SELECT team_id, team_code, name, title, imagefile_lg, description,fav_bu FROM team WHERE is_enabled = 1 AND is_active = 1 AND team_code = '" . formatSql($c) . "'");

	if ( $row = mysqli_fetch_assoc($rs) ) {		

		$show_detail = true;

		$team_code = $row['team_code'];

		$name = $row['name'];

		$title = $row['title'];

		$imagefile_lg = $row['imagefile_lg'];

		$description = $row['description'];
		$favbu = $row['fav_bu'];
	}
}


if ( $show_detail ) {

header('Content-Type: text/html; charset=UTF-8');

?><!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8" />

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes" />
<title>Build Team &bull; <?php echo $name; ?></title>

<link rel="stylesheet" type="text/css" href="/css/default.css" />
<link rel="stylesheet" type="text/css" href="/css/responsive_team.css?v=2" />
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>

</head>

<body class="team">
<style type="text/css">
.up_down {
	z-index:-1 !important; 
	}
#the-team {
	position:relative;
	z-index:2;
	}	
</style>


<div class="col_two_third">
  <div style="float:left;width:93%;margin-left:2%;margin-right:5%">
  <h1><?php echo $name; ?> - <?php echo $title; ?></h1>
  <p>
  <?php //echo nl2br($description); ?></p>
  </div>
</div>
<div class="col_one_third col_last" style="text-align:center">
  <img src="/team/<?php echo $imagefile_lg; ?>"  style="width:100%;max-width:250px" alt="<?php echo $name; ?>" />
</div>


</body>

</html>



<?php



}
else {


//$meta_title = 'Meet The Team';



require_once('./inc/header.inc.php');



?>



<div class="full meet-team">
<h2>Meet the Team</h2>
<h1 class="teampg">Design Team</h1>
<div id="the-team">
<?php
$rs = getRs("SELECT * FROM team WHERE is_enabled = 1 AND team_design = 1 AND is_active = 1 ORDER BY sort");
//$rs = getRs("SELECT * FROM team WHERE is_enabled = 1 AND team_part = 'build' AND is_active = 1 ORDER BY sort");
/*code for link on team images <a href="/meet-the-team/' . $row['team_code'] . '.html" rel="shadowbox[team];height=700;width=760" title="' . $row['name'] . ' - ' . $row['title'] . '"></a> */
$i = 0;
while ( $row = mysqli_fetch_assoc($rs) ) {
  $i++;//376
  $ng="m".$i;
	echo '	<div class="team-member newteam">

  <div class="img_team_mem"><img class="imagenormal imagenormal1" src="/team/' . $row['imagefile_lg'] . '" width="" height="197" data-img1="/team/' . $row['imagefile_lg'] . '" data-img2="/team/' . $row['imagefile_sm'] . '" data-ng="' . $ng . '" /></div>
  
  
<div class="team_mem_details" id="' . $ng . 'c"><h3 class="team_member_title">' . $row['name'] . '</h3>'.'<div class="jobtitle">' . $row['title'] .'</div></div>';
echo '<div class="fav_building" id="' . $ng . 'd"><h3 class="team_member_title">' . $row['fav_bu'] . '</h3>'.'<div class="jobtitle">'.'Favourite Building'.'</div></div></div>';

}
?>
</div>
<div class="clear"></div>
<br/>
<h1 class="teampg">Build Team</h1>
<div id="the-team">
<?php
$rs = getRs("SELECT * FROM team WHERE is_enabled = 1 AND team_build = 1 AND is_active = 1 ORDER BY sort");
//$rs = getRs("SELECT * FROM team WHERE is_enabled = 1 AND team_part = 'build' AND is_active = 1 ORDER BY sort");
$i = 0;
while ( $row = mysqli_fetch_assoc($rs) ) {
  $i++;//376
  
  $ng="n".$i;
	echo '	<div class="team-member newteam">

  <div class="img_team_mem"><img class="imagenormal" src="/team/' . $row['imagefile_lg'] . '" width="" height="197" data-img1="/team/' . $row['imagefile_lg'] . '" data-img2="/team/' . $row['imagefile_sm'] . '"  data-ng="' . $ng . '" /></div>
  
  
<div class="team_mem_details" id="' . $ng . 'a"><h3 class="team_member_title" >' . $row['name'] . '</h3>'.'<div class="jobtitle">' . $row['title'] .'</div></div>';
echo '<div class="fav_building" id="' . $ng . 'b"><h3 class="team_member_title">' . $row['fav_bu'] . '</h3>'.'<div class="jobtitle">'.'Favourite Building'.'</div></div></div>';
}
?>
</div>

<div class="clear"></div>
</div>
<script>
$(document).ready(function() {     
    $('img').mouseover(function(){     
	var src=$(this).attr("data-img2");
	///alert(src);
	$(this).attr("src", src);
	var ng=$(this).attr("data-ng");
	
	 $("#"+ng+"a").addClass("none1");
	  $("#"+ng+"b").addClass("block1");
	  $("#"+ng+"c").addClass("none1");
	  $("#"+ng+"d").addClass("block1");
    });
	
	$('img').mouseout(function(){     
	var src=$(this).attr("data-img1");
	$(this).attr("src", src);
	var ng=$(this).attr("data-ng");
	
	 $("#"+ng+"a").removeClass("none1");
	  $("#"+ng+"b").removeClass("block1"); 
	   $("#"+ng+"c").removeClass("none1");
	  $("#"+ng+"d").removeClass("block1"); 

    });
	
	
	/*$('img').mouseover(function(){     
	var src=$(this).attr("data-img2");
	///alert(src);
	$(this).attr("src", src);
	var ng=$(this).attr("data-ng");
	
	 $("#"+ng+"c").addClass("none1");
	  $("#"+ng+"d").addClass("block1");
    });
	
	$('.imagenormal1').mouseout(function(){     
	var src=$(this).attr("data-img1");
	$(this).attr("src", src);
	var ng=$(this).attr("data-ng");
	
	 $("#"+ng+"c").removeClass("none1");
	  $("#"+ng+"d").removeClass("block1"); 

    });*/
	
	
	
	
});      

</script>

<?php
require_once('./inc/footer.inc.php');
}
?>