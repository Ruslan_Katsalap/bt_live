var DesignTeam = {

  book_dates:[], // used in multidatespicker (because it's own output dates array is ordered by ASC) 
  
  setPrefDates: function(pref_dates) {
    this.pref_times = pref_dates;//debug
    //this.components["addr2"] = document.fm_book_visit.addr2.value;
  },
  add_dates: "",
  
  submit_disable: false,
  
  onDateSelect: function(dt, inst) {
          var a_d = jQuery('#avail_cal').multiDatesPicker('getDates');
          //console.log(inst);//debug
          var key = 0;
         
          if ($.inArray(dt, a_d) == -1) {
            // remove from array
            key = $.inArray(dt, DesignTeam.book_dates);
            DesignTeam.book_dates.splice(key, 1);
            delete DesignTeam.pref_times[dt];
          }
          else {
            // add to the end of array
            //if ($.inArray(dt, DesignTeam.book_dates) == -1)
            
            if (typeof inst != "undefined") {
              DesignTeam.book_dates.push(dt);
              DesignTeam.pref_times[dt] = "am";
            }
          }
          
          //console.log("Catcha!:)"+dt+" : "+DesignTeam.book_dates);
          //console.log(DesignTeam.pref_times);
          $("#pref_dates").empty();
          var buf = '';
          key = 0;
          var i = '';
          for (i in DesignTeam.pref_times) {
            key++;
            a_d = i.split("/");
            a_d[2] = a_d[2].slice(2, 4);
            a_d = a_d[1] + '.' + a_d[0] + '.' + a_d[2];
            buf += '<li class="fordynamic" id="pref_date_' + key + '">' +
                  '<i>' + key + '</i>' +
                  '<span>' +
                    '<em>' + a_d + '</em>'+
                    '<ul>' +
                      '<li class="date_am'+(DesignTeam.pref_times[i]==='am'?' selected':'')+'">am</li>' +
                      '<li class="date_pm'+(DesignTeam.pref_times[i]==='pm'?' selected':'')+'">pm</li>' +
                    '</ul>' +
                  '</span>' +
                '<span class="removedates">x</span></li>';
          }
          $("#pref_dates").html(buf);
          $("#pref_dates li span ul li").click(DesignTeam.selectTime);

        },
  
  
  initBookCal: function(){
    
      jQuery('#avail_cal').multiDatesPicker({
        firstDay: 1,
        dayNamesMin: [ "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat" ],
        prevText: "&laquo;&nbsp;prev",
        nextText: "next&nbsp;&raquo;",
        minDate: 2,
	    maxDate: 30,
        maxPicks: 3,
        //addDates: DesignTeam.add_dates,
        onSelect: DesignTeam.onDateSelect,
        beforeShowDay: function(date) {
		   //$(this).datepicker( "option", "minDate", "+2" );
          var a_dt = jQuery('#avail_cal').multiDatesPicker('getDates'); // DesignTeam.book_dates;
          var td = date.getDay();
          if(td == 0 || td == 6){
            //console.log(date.getDay());
			
            return false;
          }
          return [true, '', '', a_dt.length?'dt_pick'+a_dt.length:null];
        }
      });
    
    
      //if (DesignTeam.pref_times) alert(DesignTeam.pref_times);//debug
      
      if (DesignTeam.pref_times) {

        for(var i in DesignTeam.pref_times) {    
          DesignTeam.book_dates.push(i);
          jQuery('#avail_cal').multiDatesPicker('addDates', i);
          
          DesignTeam.onDateSelect(i);
        }
        
      }
    	$(document).on('click', '#pref_date_1 span.removedates', function(){
		$(".dt_pick1").removeClass("dt_pick1");
		
		$(this).parents("li").empty();
		//a_d = jQuery('#avail_cal').multiDatesPicker('getDates');
		});
		
		$(document).on('click', '#pref_date_2 span.removedates', function(){
		$(".dt_pick2").removeClass("dt_pick2");
		
		$(this).parents("li").empty();
		//a_d = jQuery('#avail_cal').multiDatesPicker('getDates');
		});
		
		$(document).on('click', '#pref_date_3 span.removedates', function(){
		$(".dt_pick3").removeClass("dt_pick3");
		
		$(this).parents("li").empty();
		//a_d = jQuery('#avail_cal').multiDatesPicker('getDates');
		})
     
      $('.book-now').click(function(event){
		var newa_D = $("#pref_dates li").length;
		if (newa_D < 3) {
			alert("Please select 3 dates");
         	 return false;
			}
        var a_d = jQuery('#avail_cal').multiDatesPicker('getDates');
        if(a_d.length < 3){
          alert("Please select 3 dates");
          return false;
        }
		
		
		//changes 06.03.19
		if($(".coupon-header p.c-msg").length && $(".coupon-header p.c-msg").hasClass('success-msg') == false) {
			alert('Please enter the valid coupon code');
			return false;
		}
		//changes 06.03.19
		
		
		
        $("#fm_book_visit").submit();
        event.preventDefault();
      });
    
      $( "#fm_book_visit" ).submit(function( event ) {
        var error = "";
        
        var s = "";
        /*
        s = document.fm_book_visit.fname.value;
        s = s.trim();
        if (!s) error += "Enter First name\n";
        */
        
        if (error) {
          alert( "Please fill out the following information:\n\n" + error );
          event.preventDefault();
        }
        else {
          // save pref dates in hidden input
          document.fm_book_visit.availibility.value = JSON.stringify(DesignTeam.pref_times);
        }
        
      });
    
      
    
    },
    pref_times: {
      // "01/30/2015": "am", "01/31/2015": "pm", "02/01/2015": "am"
    },
    selectTime: function(){
        
        if (!$(this).hasClass("selected")) {
          // update DesignTeam.pref_times and update UI
          var ap = $(this).attr("class");
          ap = ap.replace("date_", "");
          
          var parent = $(this).parents("li");
          
          var id = parent.attr("id");
          id = parseInt(id.replace("pref_date_", ""));
          
          parent.find("ul li").removeClass("selected");
          $(this).addClass("selected");
          
          var i = '';
          var k = 0;
          for (i in DesignTeam.pref_times) {
            k++;
            if (k===id) {
              DesignTeam.pref_times[i] = ap;
            }
          }
          
          //console.log(ap+":"+id+":"+DesignTeam.pref_times);//debug
        }
    },

    initCouponCode: function() {
      // Coupon init:
      $("#coupon_code").change(function(){
      
          DesignTeam.submit_disable = true;
            
          var data = {};

          this.value = this.value.trim();

          this.value = this.value.toUpperCase();

          data.code = this.value;

          $(this).addClass("loading");

          var jqxhr = $.post("/verify-code.php", data, DesignTeam.checkCodeResponse, 'json');

          jqxhr.error(function() {
            console.log("Can't verify offer code! Please contact us");
          });

          //alert(this.value);//debug
        });
    
        $('#fm_pay_now .book-now').click(function(event){
          if (!DesignTeam.submit_disable) $("#fm_pay_now").submit(); 
          event.preventDefault();
        });
      
    },
    
    checkCodeResponse: function(data) {
      
      DesignTeam.submit_disable = false;
      
      $("#coupon_code").removeClass("loading");
      if (typeof data.price == "undefined") {
        if (!data) {
          $("#coupon_msg").html("");
          $("#coupon_msg").hide();
        }
        else {
          $("#coupon_msg").removeClass("correct");
          $("#coupon_msg").addClass("error");
          $("#coupon_msg").html("An invalid discount code has been entered");
          $("#coupon_msg").show();
        }
        $("#price").hide();
        $("#init_price").show();
        
        //DesignTeam.showHideCCdetails(true);
        
        // reset HashDigest
        //document.Form.HashDigest.value = document.getElementById("init_HashDigest").innerHTML;
      }
      else {
        $("#coupon_msg").removeClass("error");
        $("#coupon_msg").addClass("correct");
        $("#coupon_msg").html("Discount Code Applied");
        $("#coupon_msg").show();
        
        if (data.price_vat==0 && data.price==0) {
          //DesignTeam.showHideCCdetails(false);
          
          $("#price").html("FREE");
        }
        else {
          //DesignTeam.showHideCCdetails(true);
          
          $("#price").html("&pound;" + data.price_vat + " (&pound;" + data.price + " + VAT)");
        }
        
        
        $("#price").show();
        $("#init_price").hide();
        
        //if (typeof data.HashDigest != "undefined") document.Form.HashDigest.value = data.HashDigest;
        
        //console.log(data.HashDigest+ "\n" + document.Form.HashDigest.value);
        
      }
    }
};


String.prototype.trim = function() {
  var s = this;
  s = s.replace(/\s+$/, '');
  s = s.replace(/^\s+/, '');
  return s;
}