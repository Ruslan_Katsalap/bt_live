var global_err_config = "";

var global_currency = "&pound;";

var global_byp_went = 0;

var global_mob_screen = false;

var global_mob_curr_slider = 0;
var global_mob_curr_screen = 0;

var mslider = [];
var mslider_count = [];

var global_byp_config = {
    ready: false,
    postcode: "",
    postcode_valid: 0,
    postcode_addr: [],
    try_step: 0,
    get_costing: false,
    submit_details: false,
    submit_user: true,
    block_nav: false,
    
    url_costing: "",
    url_details: "",
    url_images: "",
    
    dim_m: {
      is_default: 1,
      default_width : 5.00,
      default_length : 5.00,
      dimension_increment: 0.50,
      max_dimension: 10.00,
      min_dimension: 1.00,
    },
    dim_in: {
      is_default: 0,
      default_width : 24.00,
      default_length : 24.00,
      dimension_increment: 1.00,
      max_dimension: 120.00,
      min_dimension: 1.00,
    },
    
    estimator: false,
    
    room_size_estimate: [],
    tmp: '',
    prev: [],
    counter: 0,
    
    panel: {
      byp_step2_col2: 'cb_wall',
      byp_step2_col3: 'cb_roof',
      byp_step2_col4: 'cb_door',
      byp_step3_col2: 'cb_heating',
      byp_step3_col3: 'cb_kitchen',
      byp_step3_col4: 'cb_floor'
    },
    
    component: [],
    
    qid: 0, // get quote ID with costing
    
    userid: 0, // set after/if login
    
    block_login: false,
    lightbox: 0,
    login_act: "login",
    
    // slow down bypGetCosting
    costing_timer : false,
    costing_ready: false,
    interval_id: 0,
    data: "",
    
    getInfo: function () {
    //  alert(this.postcode + ' ' + this.dim);
    },
    alertErr: function () {
      alert("Config error: " + global_err_config + "!");
    }
}

/*
global_byp_config.component["cb_wall"] = {
  title: '',
  descr: '',
  options: []
};
*/

function bypInit() {
  MM_preloadImages("/images/byp/bg_navigation1.png", "/images/byp/bg_navigation2.png", "/images/byp/bg_navigation3.png", "/images/byp/bg_navigation4.png", "/images/byp/loading2.gif", "/images/byp/loading.gif");
  
  $("#byp_slide_box").jCarouselLite({
    visible: 1,
    speed: 500,
    circular: false,
    beforeStart: function(a) {
      global_byp_went = global_go;
    },
    afterEnd: function(a) {
      if (3==global_byp_went) {
        document.getElementById("fm_byp1").style.display = "block";
        document.getElementById("fm_byp2").style.display = "block";
        document.getElementById("fm_byp3").style.display = "block";
      }
    }
  });
  
  if (document.getElementById("fm_byp1")) {
    
    // load BYP config
    var jqxhr = $.get('xml/btbyp_xml.php', bypLoadConfig, 'xml');
      
    jqxhr.error(function() { 
      global_err_config = "Can'2t load file";
      global_byp_config.alertErr();
    });
    
  }
  
  $("#byp_email").change(function(){validateUserEmail();});
  
  if (0!=global_byp_config.userid) {
    document.getElementById("byp_register_info").style.display = "none";
  }

  mslider[0] = $('#fm_byp1').carousel({ // .carousel
    interval:false
  });

  mslider[1] = $('#fm_byp2').carousel({ // .carousel
    interval:false
  });

  mslider[2] = $('#fm_byp3').carousel({ // .carousel
    interval:false
  });
  
  mslider[3] = $('#fm_byp4').carousel({ // .carousel
    interval:false
  });
  
  mslider_count[0] = 2;
  mslider_count[1] = 3;
  mslider_count[2] = 3;
  mslider_count[3] = 2;
  
}

function bypLoadConfig(xml) {

  global_byp_config.url_costing = $(xml).find("btbyp").attr("url_costing");
  global_byp_config.url_details = $(xml).find("btbyp").attr("url_details");
  global_byp_config.url_images = $(xml).find("btbyp").attr("url_images");

  $(xml).find("unit").each(function() {
    if ("m"==$(this).attr("abbreviation")) {
      global_byp_config.dim_m.is_default = parseInt($(this).attr("is_default"));
      global_byp_config.dim_m.dimension_increment = parseFloat($(this).attr("dimension_increment"));
      global_byp_config.dim_m.default_width = parseFloat($(this).attr("default_width"));
      global_byp_config.dim_m.default_length = parseFloat($(this).attr("default_length"));
      global_byp_config.dim_m.max_dimension = parseFloat($(this).attr("max_dimension"));
      global_byp_config.dim_m.min_dimension = parseFloat($(this).attr("min_dimension"));
    }
    else { // in
      global_byp_config.dim_in.is_default = parseInt($(this).attr("is_default"));
      global_byp_config.dim_in.dimension_increment = parseFloat($(this).attr("dimension_increment"));
      global_byp_config.dim_in.default_width = parseFloat($(this).attr("default_width"));
      global_byp_config.dim_in.default_length = parseFloat($(this).attr("default_length"));
      global_byp_config.dim_in.max_dimension = parseFloat($(this).attr("max_dimension"));
      global_byp_config.dim_in.min_dimension = parseFloat($(this).attr("min_dimension"));
    }
  });
  
  if (1==global_byp_config.dim_in.is_default) {
    document.getElementById("fm_byp1").dim[0].checked = true;
  }
  
  bypLoadDimensions();
  
  $(xml).find("room_size_estimate").each(function() {
    
    var bedrooms = parseInt($(this).attr("num_bedrooms"));
    var option = {};
    
    if (""==global_byp_config.tmp) {
      document.getElementById("byp_sqe").innerHTML = $(this).attr("room_size");
    }
    
    if (global_byp_config.tmp != bedrooms) {
      option = new Option(bedrooms, bedrooms);
      document.getElementById("fm_byp1").bedrooms.options[ global_byp_config.counter ] = option;
      global_byp_config.counter++;
    }
    
    global_byp_config.tmp = bedrooms;
    
    global_byp_config.room_size_estimate[ parseInt($(this).attr("id")) ] = {
      num_bedrooms: bedrooms,
      terrace_type_id : parseInt($(this).attr("terrace_type_id")),
      room_size : parseFloat($(this).attr("room_size"))
    };
  });
  
  
  global_byp_config.counter = 1;
  $(xml).find("terrace_type").each(function() {
    document.getElementById("lbl_terrace_type"+global_byp_config.counter).innerHTML = $(this).attr("name");
    
    document.getElementById("terrace_type"+global_byp_config.counter).value = $(this).attr("id");
    
    if (1 == $(this).attr("is_default")) document.getElementById("terrace_type"+global_byp_config.counter).checked = true;
    
    global_byp_config.counter++;
  });
  
  // panels: subcategories and components
  $(xml).find("panel").each(function() {
    
    global_byp_config.tmp = $(this).attr("code");
    
    global_byp_config.component[$(this).attr("code")] = {
      title: $(this).attr("title"),
      descr: $(this).attr("subtitle"),
      options: []
    };
  
    $(this).find("option").each(function() {
      
      global_byp_config.component[ global_byp_config.tmp ].options[ parseInt($(this).attr("id")) ] = 
      {
        text: $(this).attr("text"),
        image: $(this).attr("image"),
        image_w: parseInt($(this).attr("image_w")),
        image_h: parseInt($(this).attr("image_h")),
        is_default: parseInt($(this).attr("is_default"))
      };
      
    });
    
  });  
  
  //alert(global_byp_config.component[ "cb_wall" ].options.length + "!yea");//debug
  
  var key = "";
  var buf = "";
  
  var subcat_code = "";
  
  for (var i in global_byp_config.panel) {
    if (true) {
    
    /* byp_step3_col4: 'cb_floor'*/
    
      key = global_byp_config.panel[i];
      //alert(i + " : " + key);//debug
      
      $("#"+i).empty();
      
      $("#"+i).append("<h2>" + global_byp_config.component[key].title + "</h2>");
      if (!(global_byp_config.component[key].title.length>12 && global_byp_config.component[key].title.indexOf(" ")!=-1 && !global_mob_screen)) {
        $("#"+i).append("<p"+("cb_heating"==key?' style="padding-bottom:5px"':'')+">" + (global_byp_config.component[key].descr?global_byp_config.component[key].descr:'') + "</p>");
      }
      
      buf = "";
      
      var x = 0;
      for (var j in global_byp_config.component[ key ].options) {
        // load panels
        buf += "<div class='limitedwidth'>";
        buf += "<div"+(x?"":'')+'><div class="newlabel"><label for="'+key+'_'+j+'">'+global_byp_config.component[ key ].options[j].text+'</label><input type="radio" name="'+key+'" value="'+j+'" id="'+key+'_'+j+'" onchange="bypSelectComponent(this.id)"'+(global_byp_config.component[ key ].options[j].is_default?' checked="checked"':'')+' /><div class="check"></div></div></div>';
        //alert(j + " " + global_byp_config.component[ key ].options[j].text + " " + global_byp_config.component[ key ].options[j].image + " " + global_byp_config.component[ key ].options[j].is_default);
       
        buf+="</div>";
        
        x++;
      }
      
      subcat_code = key.replace('cb_', '');
      $("#byp_selected").append("<p><img src=\"/images/blank.gif\" id=\"byp_sel_img_"+subcat_code+"\" alt=\"\" /><span id=\"byp_sel_subcat_"+subcat_code+"\"></span></p>");
      
      buf += '</div><img src="/images/blank.gif" alt="'+global_byp_config.component[key].title+'" id="byp_'+subcat_code+'_sel" /><br/>';
      $("#"+i).append(buf);
      
      buf = "";
      for (var j in global_byp_config.component[ key ].options) {
        if (""!=global_byp_config.component[ key ].options[j].image)
          buf += '<img src="'+ global_byp_config.url_images + global_byp_config.component[ key ].options[j].image +'" alt="'+global_byp_config.component[ key ].options[j].text+'" id="'+key+'_'+j+'_img" class="'+(global_byp_config.component[ key ].options[j].image_w > global_byp_config.component[ key ].options[j].image_h ? 'byp_col_img_w':'byp_col_img_h')+'" onclick="bypSelectComponent(this.id)" />';
      }
      
      $("#"+i).append(buf+'<br/><input type="button" value=" Next " class="btn btn-orange mob_btn_next commonnextbtn" onclick="bypMobSwipeNextScreen()" />');
    }
  }
  
  global_byp_config.tmp = "";
  
  global_byp_config.ready = true;
  
  preloadAfterInit();
}

function bypLoadDimensions() {
  // clear options
  document.getElementById("fm_byp1").depth.options.length = 0;
  document.getElementById("fm_byp1").width.options.length = 0;
  
  var option_d = {}; //new Option("Red", "color_red");
  var option_w = {};
  
  if (document.getElementById("fm_byp1").dim[0].checked) { // Inches
    var min_dimension = global_byp_config.dim_in.min_dimension;
    var max_dimension = global_byp_config.dim_in.max_dimension;
    var dimension_increment = global_byp_config.dim_in.dimension_increment;
    var default_length = global_byp_config.dim_in.default_length;
    var default_width = global_byp_config.dim_in.default_width;
  }
  else { // Meters
    var min_dimension = global_byp_config.dim_m.min_dimension;
    var max_dimension = global_byp_config.dim_m.max_dimension;
    var dimension_increment = global_byp_config.dim_m.dimension_increment;
    var default_length = global_byp_config.dim_m.default_length;
    var default_width = global_byp_config.dim_m.default_width;    
  }
    
  var j=0;
  //alert(global_byp_config.dim_in.min_dimension + " ; " + global_byp_config.dim_in.max_dimension + " ; " + global_byp_config.dim_in.dimension_increment);
  
  for(var i=min_dimension;i<=max_dimension;i+=dimension_increment) {
    option_d = new Option(i, i);
    option_w = new Option(i, i);
    document.getElementById("fm_byp1").depth.options[j]=option_d;
    document.getElementById("fm_byp1").width.options[j]=option_w;
    
    if (i==default_length) {
      document.getElementById("fm_byp1").depth.options[j].selected=true;
    }
    
    if (i==default_width) {
      document.getElementById("fm_byp1").width.options[j].selected=true;
    }
    
    j++;
  }
  
  bypCalcRoomArea();
}

function bypCalcRoomArea() {
	
	var depth=$('#depth').val();
	var width=$('#width').val();
	var total=depth * width;
	$('.size_box').css('width',width*15);
	$('.size_box').css('height',depth*15);
	$('.depth_size').html("Depth ("+width+"m)");
	$('.width_size').html("Width ("+depth+"m)");
	$('.total').html(total+"m<label class='sup'>2</label>");   
 
	
  var depth = parseFloat(document.getElementById("fm_byp1").depth.value);
  var width = parseFloat(document.getElementById("fm_byp1").width.value);
  
  document.getElementById("byp_depth").innerHTML = depth;
  var dim = document.getElementById("fm_byp1");
  document.getElementById("byp_depth_dim").innerHTML = dim;
  document.getElementById("byp_width").innerHTML = width;
  document.getElementById("byp_width_dim").innerHTML = dim;
  
  var square = depth * width;
  
  square = Math.round(square * 10000)/10000;
  
  document.getElementById("byp_sq").innerHTML = square;
  document.getElementById("byp_mob_sq").innerHTML = square;
  document.getElementById("byp_sq_dim").innerHTML = dim;
  document.getElementById("byp_sq2").innerHTML = square;
  document.getElementById("byp_sq_dim2").innerHTML = dim;
  document.getElementById("byp_sq3").innerHTML = square;
  document.getElementById("byp_sq_dim3").innerHTML = dim;
  document.getElementById("byp_sq4").innerHTML = square;
  document.getElementById("byp_sq_dim4").innerHTML = dim;
  
  // Area animation
  if (width > depth) {
    var width_percent = 100;
    var depth_percent = depth * 100 / width;
    
    var width_px = 160;
    var depth_px = Math.round(depth_percent * width_px / 100);   
  }
  else {
    var depth_percent = 100;
    var width_percent = width * 100 / depth;
    
    var depth_px = 160;
    var width_px = Math.round(width_percent * depth_px / 100);
  }
  
  var width_table = width_px+50;
  
  var top = Math.round(160/2 - depth_px/2);
  
  $("#byp_sub_col3 table").animate({width: width_table+"px", marginTop: top+"px"});
  $("#byp_td_room").animate({height: depth_px+"px"});
  
}
	
function bypEstimateRoomArea() {
  var terrace_type = 0;
  for(var i=0;i<document.getElementById("fm_byp1").terrace_type.length;i++) {
    if (document.getElementById("fm_byp1").terrace_type[i].checked) {
      terrace_type = document.getElementById("fm_byp1").terrace_type[i].value;
      break;
    }
  }
  //alert(terrace_type);

  for (var i in global_byp_config.room_size_estimate) {
    if ( global_byp_config.room_size_estimate[i].num_bedrooms == document.getElementById("byp_bedrooms").value && global_byp_config.room_size_estimate[i].terrace_type_id == terrace_type) {
      document.getElementById("byp_sqe").innerHTML = global_byp_config.room_size_estimate[i].room_size;
      
      document.getElementById("byp_sq2").innerHTML = document.getElementById("byp_sqe").innerHTML;
      document.getElementById("byp_sq3").innerHTML = document.getElementById("byp_sqe").innerHTML;      
      document.getElementById("byp_sq4").innerHTML = document.getElementById("byp_sqe").innerHTML;
      break;
    }
  }
}

function bypMobSizeInfo(turn) {
  if (turn) {
    document.getElementById("byp_sub_col1").style.display = "inline";
    document.getElementById("byp_sub_col2").style.display = "none";
    document.getElementById("mob_next_2").style.display = "none";
  }
  else {
    document.getElementById("byp_sub_col1").style.display = "none";
    document.getElementById("byp_sub_col2").style.display = "inline-block";
    document.getElementById("mob_next_2").style.display = "block";
  }
}

function bypSwitchEstimator(turn) {
  
  if (turn && global_byp_config.estimator) return false;

  if (1==global_byp_config.dim_in.is_default) {
    document.getElementById("fm_byp1").dim[0].checked = true;
  }
  else document.getElementById("fm_byp1").dim[1].checked = true;
  bypLoadDimensions();
  
  if (global_mob_screen) $("#byp_sub_col3 table").css("display", "none");
  else $("#byp_sub_col2").css("display", "block");
  
  if (turn) {
    document.getElementById("byp_dim_unknown").blur();
    
    if (!global_mob_screen) $("#byp_sub_col2").css("opacity", "0.5");
    else $("#byp_sub_col2").css("display", "none");
    
    global_byp_config.estimator = true;
    
    if (!global_mob_screen) $("#byp_sub_col3 table").css("display", "none");
    
    $("#byp_sub_col3").css("marginTop", "15px");
    
    if (!global_mob_screen) document.getElementById("byp_estimator").style.display = "block";
    else document.getElementById("byp_estimator").style.display = "inline-block";
    
    $("#byp_dim_unknown").css("cursor", "default");
    
    document.getElementById("fm_byp1").depth.disabled = true;
    document.getElementById("fm_byp1").width.disabled = true;
    document.getElementById("fm_byp1").dim[0].disabled = true;
    document.getElementById("fm_byp1").dim[1].disabled = true;
    
    document.getElementById("byp_sq2").innerHTML = document.getElementById("byp_sqe").innerHTML;
    document.getElementById("byp_sq_dim2").innerHTML = "m";
    document.getElementById("byp_sq3").innerHTML = document.getElementById("byp_sqe").innerHTML;
    document.getElementById("byp_sq_dim3").innerHTML = "m";
    
  }
  else {
    global_byp_config.estimator = false;

    document.getElementById("byp_estimator").style.display = "none";
    
    $("#byp_sub_col2").css("opacity", "1");
    if (global_mob_screen) $("#byp_sub_col2").css("display", "block");
    
    $("#byp_dim_unknown").css("cursor", "pointer");
    
    if (!global_mob_screen) $("#byp_sub_col3 table").css("display", "block");
    
    $("#byp_sub_col3").css("marginTop", "30px");
    document.getElementById("fm_byp1").depth.disabled = false;
    document.getElementById("fm_byp1").width.disabled = false;
    document.getElementById("fm_byp1").dim[0].disabled = false;
    document.getElementById("fm_byp1").dim[1].disabled = false;
  }
  
  return false;
}

function bypValidatePostcode(xml) {
         console.log(xml);
  var valid = $(xml).find("postcode").attr("valid");
  //alert(valid);//debug
  document.getElementById("byp_img_loading").style.display = "none";
  if (valid!=0) {
    document.getElementById("byp_postcode").value = document.getElementById("byp_postcode").value.toUpperCase();
    
    global_byp_config.postcode_addr = [];
    document.getElementById("byp_addr").options.length = 0;
    var option_x = new Option("- or select address -", "");
    document.getElementById("byp_addr").options[0] = option_x;
    
    $(xml).find("addr").each(function(){

      var text=$(this).text();
      
      global_byp_config.postcode_addr.push(text);
    });
    
    for (var i=0;i<global_byp_config.postcode_addr.length;i++) {
      option_x = new Option(global_byp_config.postcode_addr[i], global_byp_config.postcode_addr[i]);
      document.getElementById("byp_addr").options[i+1] = option_x;
    }
    
    if (0==global_byp_config.postcode_addr.length) document.getElementById("byp_addr").style.display = "none";
    else document.getElementById("byp_addr").style.display = "block";
    
  }
  else alert("Please enter valid postcode!");
  
  global_byp_config.postcode_valid = valid;
  global_byp_config.postcode = document.getElementById("byp_postcode").value;
  document.getElementById("byp_postcode_f").value = global_byp_config.postcode;
  
  if ($(window).width() > 640) {
	  
  	if (valid!=0) { bypStep(global_byp_config.try_step); }
  }
  else {
  	if (valid!=0 && global_mob_screen) { bypMobShowScreen(0, 1)}
  }
}

function bypMobShowScreen(slider, screen) {
  
  global_mob_curr_slider = slider;
  global_mob_curr_screen = screen;
  bypMobSwipeScreen();
  
}

function bypMobSwipeScreen() {
	  if (0 == global_mob_curr_slider) {
		if (0==global_mob_curr_screen) {
		  $("#mob_screen_back a").css("display", "none");
		  document.getElementById("mob_screen_title").innerHTML = "Post code";
		}
		else {
		  $("#mob_screen_back a").css("display", "inline");
		  document.getElementById("mob_screen_title").innerHTML = "Room size";
		}
	  }
  
  if (1 == global_mob_curr_slider) {
    if (0==global_mob_curr_screen) document.getElementById("mob_screen_title").innerHTML = $("#byp_step2_col2 h2").text();
    if (1==global_mob_curr_screen) { document.getElementById("mob_screen_title").innerHTML = $("#byp_step2_col3 h2").text();  alert("in show 1 1");}
    if (2==global_mob_curr_screen) document.getElementById("mob_screen_title").innerHTML = $("#byp_step2_col4 h2").text();
  }
  
  if (2==global_mob_curr_slider) {
    if (0==global_mob_curr_screen) document.getElementById("mob_screen_title").innerHTML = $("#byp_step3_col2 h2").text();
    if (1==global_mob_curr_screen) document.getElementById("mob_screen_title").innerHTML = $("#byp_step3_col3 h2").text();
    if (2==global_mob_curr_screen) document.getElementById("mob_screen_title").innerHTML = $("#byp_step3_col4 h2").text();
  }
  
  if (3==global_mob_curr_slider) {
    if (0==global_mob_curr_screen) document.getElementById("mob_screen_title").innerHTML = "Confirm Details";
    if (1==global_mob_curr_screen) document.getElementById("mob_screen_title").innerHTML = "Show My Quote";
  }

  if (4==global_mob_curr_slider) {
    document.getElementById("mob_screen_title").innerHTML = "Your Quote";
    $("#mob_screen_next a").css("display", "none");
  }
  
  mslider[global_mob_curr_slider].carousel(global_mob_curr_screen);
}

function bypMobSwipeNextScreen() {
  if (global_byp_config.block_nav) return false;
  
  console.log("global_mob_curr_slider: " + global_mob_curr_slider + " global_mob_curr_screen:" +global_mob_curr_screen);//debug

  if (0==global_mob_curr_slider && 0==global_mob_curr_screen) {
    bypMobPostcode();
    return false;
  }
  
  // check
  if (global_mob_curr_slider > 0 && global_mob_curr_slider < 3) {
    
    if (1==global_mob_curr_slider) var a_check_screen = ["byp_step2_col2", "byp_step2_col3", "byp_step2_col4"];
    if (2==global_mob_curr_slider) var a_check_screen = ["byp_step3_col2", "byp_step3_col3", "byp_step3_col4"];
    
    var no_select = "";
    
    for (var i in global_byp_config.panel) {
      //if ("byp_step2_col2"==i || "byp_step2_col3"==i || "byp_step2_col4"==i) {
      if (i==a_check_screen[global_mob_curr_screen]) {
        
        key = global_byp_config.panel[i];
        option_selected = 0;
        for (var j in global_byp_config.component[key].options) {
          if (document.getElementById(key+"_"+j).checked) {
            option_selected = j;
            break;
          }
        }
        
        if (!option_selected) {
          no_select = global_byp_config.component[key].title;
          break;
        }

      }
    }
    
    if (""!=no_select) {
      alert("Please select " + no_select);
      return false;
    }
  }
  
  var user_valid = false;
  if (global_mob_curr_slider===3) {
    if (0===global_mob_curr_screen) user_valid = validateUserForm(1);
    if (1===global_mob_curr_screen) {
      user_valid = validateUserForm(2);
    }
    if (!user_valid) return false;
  }
  
  if (global_mob_curr_screen < (mslider_count[global_mob_curr_slider]-1)) { 
    global_mob_curr_screen++;  
    console.log("Will try screen: "+global_mob_curr_screen+" in slider: "+global_mob_curr_slider);//debug
    bypMobSwipeScreen();
  
  }
  else {
    bypMobNextSlider(global_mob_curr_slider+1);
  }
}

function bypMobSwipeBackScreen() {
  if (global_byp_config.block_nav) return false;
  
  console.log("global_mob_curr_slider: " + global_mob_curr_slider + " global_mob_curr_screen:" +global_mob_curr_screen);//debug
  
  if (global_mob_curr_screen > 0) {
    global_mob_curr_screen--;
    bypMobSwipeScreen();
  }
  else {
    
    if (global_mob_curr_slider > 0) { // global_go
      global_mob_curr_slider--;
      global_mob_curr_screen = mslider_count[global_mob_curr_slider]-1;
      bypStep(global_mob_curr_slider);
      
      if (1==global_mob_curr_slider) {
        document.getElementById("mob_screen_title").innerHTML = $("#byp_step2_col4 h2").text();
      }
      
      if (2==global_mob_curr_slider) {
        document.getElementById("mob_screen_title").innerHTML = $("#byp_step3_col4 h2").text();
      }
      
      if (3==global_mob_curr_slider) {
        document.getElementById("mob_screen_title").innerHTML = "Show My Quote";
        $("#mob_screen_next a").css("display", "inline");
      }
      
    }
    
  }
}

function bypMobNextSlider(step) {
  
  global_mob_curr_slider = step;
  global_mob_curr_screen = 0;
  
  //alert("Step: "+step);//debug
  
  if (1==step) {
    document.getElementById("mob_screen_title").innerHTML = $("#byp_step2_col2 h2").text(); 
    //"<a href='javascript:;' onclick='mslider[1].carousel(0)'>1</a> | <a href='javascript:;' onclick='mslider[1].carousel(1)'>2</a> | <a href='javascript:;' onclick='mslider[1].carousel(2)'>3</a>";//debug
  }
  if (2==step) {
    document.getElementById("mob_screen_title").innerHTML = $("#byp_step3_col2 h2").text();
  }
  if (3==step) {
    document.getElementById("mob_screen_title").innerHTML = "Confirm Details";
  }
  
  if (4==step) {
    document.getElementById("mob_screen_title").innerHTML = "Your Quote";
    $("#mob_screen_next a").css("display", "none");
  }
  
  if (5==step) {
    document.getElementById("mob_screen_title").innerHTML = "Thank You";
    $("#mob_screen_back a").css("display", "none");
    $("#mob_screen_next a").css("display", "none");
  }  
  
  bypStep(step);
}

function bypRequestPostcode(step) {
  // validate postcode
  document.getElementById("byp_postcode").value = trim(document.getElementById("byp_postcode").value);
  var postcode = document.getElementById("byp_postcode").value;
 // alert(postcode)  ;
  if (!postcode) {
    alert('Please enter your postcode2');
    return false;
  }
  else {
	  //added by 
	 /* var a=$('.srch_input').val();

	$('#byp_postcode').val(a); */
	
	
	//added
      //alert(global_byp_config.postcode);
    if (global_byp_config.postcode =='') {
      global_byp_config.try_step = step;

       if (global_mob_screen) document.getElementById("byp_img_loading").style.display = "inline-block";
      else document.getElementById("byp_img_loading").style.display = "block"; 

      var jqxhr = $.post('xml/uk_postcode.php', { p: postcode }, bypValidatePostcode, 'xml');

      jqxhr.error(function() {
        global_err_config = "Can't request postcode";
        global_byp_config.alertErr();
      });

      return false;
    }
    else {
      if (global_byp_config.postcode_valid == 0) {
        alert("Please enter valid postcode!");
        return false;
      }
      else {
		 show_next_step();
        if (global_mob_screen) {
          if (0 == global_mob_curr_slider) { //bypMobShowScreen(0, 1); 
		  }
        }
      }
    }
	 
  }
  
  return postcode;
  show_next_step();
}

function bypMobPostcode() {
  global_mob_screen = true;
  bypRequestPostcode(0);
}

function bypReviseQuote() {
  if (global_mob_screen) {
    bypStep(0);
    global_mob_curr_slider = 0;
    global_mob_curr_screen = 0;
    mslider[3].carousel(0);
    mslider[2].carousel(0);
    mslider[1].carousel(0);
    bypMobSwipeScreen();
    $("#mob_screen_next a").css("display", "inline");
  }
  else {
    bypStep(0);
  }
}


function bypStep(step) {
  /*if (!global_byp_config.ready || global_byp_config.block_nav) return false;

  if (global_byp_config.submit_details) { // to prevent submit details twice
    if (confirm("Do you need to start BuildYourPrice again?"))
      document.location = "/build-your-price-app.html";
    return false;
  }

  if ("" != global_err_config) {
    global_byp_config.alertErr();
    return false;
  }*/
  
  // validate postcode
  console.log(step);
  var postcode = bypRequestPostcode(step);
  console.log("test"+postcode);
  if (!postcode) return false;
  
  var key = "";
  var no_select = "";
  var option_selected = 0;
  
  if (step>=2) {
    global_byp_config.getInfo();
    
    for (var i in global_byp_config.panel) {
      if ("byp_step2_col2"==i || "byp_step2_col3"==i || "byp_step2_col4"==i) {
       
        key = global_byp_config.panel[i];
		alert("key "+key);
		
        option_selected = 0;
        for (var j in global_byp_config.component[key].options) {
		//	alert("global "+j);
          if (document.getElementById(key+"_"+j).checked) {
            option_selected = j;
            break;
          }
        }
        
        if (!option_selected) {
          no_select = global_byp_config.component[key].title;
          break;
        }

      }
    }
    
    if (no_select!="") {
      alert("Please select " + no_select);
      global_go = 1;
      
      for (var i=1; i<6; i++) document.getElementById("byp_step"+i).className = "byp_item";
      
      document.getElementById("byp_step2").className = "byp_item orange-bg";
      return false;
    }
    
  }
  
  if (step>=3) {
    
    for (var i in global_byp_config.panel) {
      if ("byp_step3_col2"==i || "byp_step3_col3"==i || "byp_step3_col4"==i) {
        
        key = global_byp_config.panel[i];
        option_selected = 0;
        for (var j in global_byp_config.component[key].options) {
          if (document.getElementById(key+"_"+j).checked) {
            option_selected = j;
            break;
          }
        }
        
        if (!option_selected) {
          no_select = global_byp_config.component[key].title;
          break;
        }

      }
    }
    
    if (no_select!="") {
      alert("Please select " + no_select);
      global_go = 2;
      
      for (var i=1; i<6; i++) document.getElementById("byp_step"+i).className = "byp_item";
      
      document.getElementById("byp_step3").className = "byp_item orange-bg";
      return false;
    }
    
  }
  
  
  if (4==step) {
    
    if (validateUserForm()) {
      
      // Get costing: refresh totals
      if (!global_byp_config.get_costing) {
        global_byp_config.block_nav = true;
        document.getElementById("byp_slide_box").style.display = "none";
        document.getElementById("byp_loading_text").innerHTML = "Building Your Price";
        document.getElementById("byp_get_costing").style.display = "block";
        
        setTimeout("bypCostingTimer()", 3400);
        global_byp_config.interval_id = setInterval("bypGetCosting()", 1000);
        
        var data = {
          postcode: global_byp_config.postcode,
          room_length: document.getElementById("fm_byp1").depth.value, 
          room_width: document.getElementById("fm_byp1").width.value,
          room_size: (global_byp_config.estimator?document.getElementById("byp_sqe").innerHTML:document.getElementById("byp_sq").innerHTML),
          measure_unit_id: (document.getElementById("fm_byp1").dim[0].checked?"in":"m"), // or 1 or 2
          use_size_estimator: (global_byp_config.estimator?'true':'false'),
          bedrooms: document.fm_byp1.bedrooms.value,
          terrace_type: (document.fm_byp1.terrace_type[0].checked ? 1 : (document.fm_byp1.terrace_type[1].checked ? 2 : 0) ),
          
          property_type: (document.getElementById("fm_byp1").property_type[0].checked?'house':'flat'),
          cb_wall: 0, // OR subcategory_3_component_id
          cb_roof: 0, // OR subcategory_2_component_id
          cb_door: 0, // OR subcategory_1_component_id
          cb_kitchen: 0, // OR subcategory_5_component_id
          cb_heating: 0, // OR subcategory_6_component_id
          cb_floor: 0, // OR subcategory_4_component_id
          
          // for not logged in user, for logged in use userid and empty these fields
          email: !global_byp_config.userid ? document.getElementById("byp_email").value : "", 
          password: !global_byp_config.userid ? document.getElementById("byp_password").value : "",
          userid: global_byp_config.userid,
          
          name: document.getElementById("byp_name").value, 
          surname: document.getElementById("byp_surname").value,
          phone: document.getElementById("byp_phone").value,
          house: document.getElementById("byp_house").value,
          addr: document.getElementById("byp_addr").value,
          message: document.getElementById("byp_msg").value,
          start: document.getElementById("when_to_start").value,
          status: document.getElementById("current_status").value,
          qid: global_byp_config.qid
        };
        
        for (i in global_byp_config.panel) {
          key = global_byp_config.panel[i];
          option_selected = 0;
          for (var j in global_byp_config.component[key].options) {
            if (document.getElementById(key+"_"+j).checked) {
              option_selected = j;
              break;
            }
          }
          switch (key) {
           case "cb_wall":
              data.cb_wall = option_selected;
              break;
           case "cb_roof":
              data.cb_roof = option_selected;
              break;
           case "cb_door":
              data.cb_door = option_selected;
              break;
           case "cb_kitchen":
              data.cb_kitchen = option_selected;
              break;
           case "cb_heating":
              data.cb_heating = option_selected;
              break;
           case "cb_floor":
              data.cb_floor = option_selected;
          }
        }
        
        /*var s = "";//debug
        for (i in data) s+= i +":"+ data[i]+"; "; 
        alert(s);*/
        
        var jqxhr = $.post(global_byp_config.url_costing, data, bypCostingReady, 'text');
        
        jqxhr.error(function() { 
          global_err_config = "Can't get costing";
          global_byp_config.alertErr();
        });
        
        return false;
      }
      
    }
    else step = 3;
  }
  else {
    global_byp_config.get_costing = false;
  }
  
  if (4==step) {
    var href = document.location + "";
    if (7==href.indexOf("buildteam.com") || 7==href.indexOf("www.buildteam.com")) {
      document.getElementById("conversion_track").src = "//www.googleadservices.com/pagead/conversion/980275045/?value=0&label=tgaaCKPOrgYQ5Z630wM&guid=ON&script=0";
    }
  }
  
  if (5==step) { // not in navigation
    if (!global_byp_config.submit_details) {
      global_byp_config.submit_details = true;
    }
  }
  
  global_go = step;
 //alert("Step "+step);
  if (step<5) {
	  //alert("step "+step);
   // for (var i=1; i<6; i++) document.getElementById("byp_step"+i).className = "byp_item";
//alert("bystep "+i);
    //document.getElementById("byp_step"+(step+1)).className = "byp_item orange-bg";
	
  }
  
  if (0 == step) global_go = -1;
  
}

function submitUserInfo() {
  if (global_byp_config.submit_user) bypStep(4);
}

function forgotPassword() {
  if (!global_byp_config.block_login) {
    
    if ("login" == global_byp_config.login_act) {
      
      global_byp_config.login_act = "forgot"; // was login
      
      $(".login_pwd").attr("value", "");
      $(".login_pwd_p").css("display", "none");
      $(".login_title").text("Forgotten your password?");
      $(".user_email_msg").html("Please enter your email below and we will send you a message with password");
      $(".login_btn").attr("value", "Submit");
      
      $(".login_forgot_link").text("Log In");
      
    }
    else {
      
      global_byp_config.login_act = "login";
      $(".login_pwd_p").css("display", "block");
      $(".login_title").text("Log In");
      $(".user_email_msg").html("");
      $(".login_btn").attr("value", "Log In");
      
      $(".login_forgot_link").text("Forgot Password?");
      
    }
  }
}

function logoutUser(func_resp) {

  var data = {
    act: "logout"
  };  
  
  $(".logout_link").css("display", "none");
  
  var jqxhr = jQuery.post("cals/register.php", data, logoutUserResponse, 'json');
  
  // if ("saved_quote"!=func_resp) 
  
  jqxhr.error(function() { 
    alert("Can't logout, server error!");
  });
}

function logoutUserResponse(data) {

  $(".logout_link").css("display", "inline");

  if (data && (typeof data.msg !== "undefined")) {
    
    if ("ERR" == data.msg) {
      
      alert(data.err);
      
    }
    if ("OK" == data.msg) {
      
      document.location = "/saved_quote.html";
      
    }
    
  }
  else {
    if (!data || ""==data) alert("Unknown server error!");
    else alert("Server error: " + data);
  }
  
}

function loginUserRequest(func_resp) {
  
  var email = "";
  var pwd = "";
  
  $(".login_email").each(function(){
    if (""!=this.value) email = trim(this.value);
  });
  
  if ("login" == global_byp_config.login_act) {
  
    $(".login_pwd").each(function(){
      if (""!=this.value) pwd = trim(this.value);
    });
    
    if (""==email || ""==pwd) {
      alert("Please enter email and password");
      return false;
    }
    
    if (email.search(/^[a-z0-9]+([-_\.]?[a-z0-9])+@[a-z0-9]+([-_\.]?[a-z0-9])+\.[a-z]{2,4}$/i) == -1) {
      alert("Please enter correct email address");
      return false;
    }
    
    $(".login_btn").prop("disabled", true);
    
    global_byp_config.block_login = true;
    
    $(".login_loading").css("display", "block");
    
    var data = {
      act: "login",
      "email": email,
      "pwd": pwd
    };
    
    
    if ("user_module"==func_resp) var jqxhr = jQuery.post("calc/register.php", data, loginUserResponse, 'json');
    if ("saved_quote"==func_resp) var jqxhr = jQuery.post("/register.php", data, savedQuoteResponse, 'json');
    
    jqxhr.error(function() { 
      alert("Can't login, server error!");
    });
  
  }
  if ("forgot"==global_byp_config.login_act) {
    
    if (""==email || email.search(/^[a-z0-9]+([-_\.]?[a-z0-9])+@[a-z0-9]+([-_\.]?[a-z0-9])+\.[a-z]{2,4}$/i) == -1) {
      alert("Please enter correct email");
      return false;
    }
    
    $(".login_btn").prop("disabled", true);
    
    global_byp_config.block_login = true;
    
    $(".login_loading").css("display", "block");
    
    var data = {
      act: "forgot",
      "email": email
    };
    
    var jqxhr = jQuery.post("calc/register.php", data, forgotPasswordResponse, 'json');
    
    jqxhr.error(function() { 
      alert("Can't submit, server error!");
    });
    
  }
  
}

function forgotPasswordResponse(data) {
  if (data && (typeof data.msg !== "undefined")) {
    
    if ("ERR" == data.msg) {
      
      $(".user_email_msg").html("<strong style=\"color:red\">"+data.err+"</strong>");
      
    }
    if ("OK" == data.msg) {
      
      $(".user_email_msg").html("<strong style=\"color:#f5641e\">We have sent your password to the e-mail address specified.<br/><br/>You should receive your password shortly, please use this to log in.</strong>");
      
    }
    
  }
  else {
    if (!data || ""==data) alert("Unknown server error!");
    else alert("Server error: " + data);
  }
  
  global_byp_config.block_login = false;
  $(".login_btn").prop("disabled", false);
  $(".login_loading").css("display", "none");
}

function savedQuoteResponse(data) {
  if (data && (typeof data.msg !== "undefined")) {
    
    if ("ERR" == data.msg) {
      
      $(".user_email_msg").html("<strong style=\"color:red\">"+data.err+"</strong>");
      
    }
    if ("OK" == data.msg) {
      
      document.location = "/saved_quote.html";
      
    }
    
  }
  else {
    if (!data || ""==data) alert("Unknown server error!");
    else alert("Server error: " + data);
  }
  
  global_byp_config.block_login = false;
  $(".login_btn").prop("disabled", false);
  $(".login_loading").css("display", "none");
}

function loginUserResponse(data) {
  if (data && (typeof data.msg !== "undefined")) {
    
    if ("ERR" == data.msg) {
      
      $(".user_email_msg").html("<strong style=\"color:red\">"+data.err+"</strong>");
      
    }
    if ("OK" == data.msg) {
      
      global_byp_config.userid = data.userid;
      if (0!=global_byp_config.userid) {
        document.getElementById("byp_email").value = "";
        document.getElementById("byp_password").value = "";
        document.getElementById("byp_repassword").value = "";
        
        document.getElementById("byp_register_info").style.display = "none";
      }
      
      document.getElementById("byp_name").value = data.firstname;
      document.getElementById("byp_surname").value = data.surname;
      document.getElementById("byp_phone").value = data.phone;
      document.getElementById("byp_house").value = data.address;
      
      global_byp_config.lightbox[0].remove();//remove lightbox
      global_byp_config.lightbox[1].remove();//remove overlay
      
    }
    
  }
  else {
    if (!data || ""==data) alert("Unknown server error!");
    else alert("Server error: " + data);
  }
  
  global_byp_config.block_login = false;
  $(".login_btn").prop("disabled", false);
  $(".login_loading").css("display", "none");
}

function validateUserEmail() {
  
  var email_addr = trim(document.getElementById("byp_email").value);
  
  if (""!=email_addr && email_addr.search(/^[a-z0-9]+([-_\.]?[a-z0-9])+@[a-z0-9]+([-_\.]?[a-z0-9])+\.[a-z]{2,4}$/i) != -1) {
    
    global_byp_config.submit_user = false;
    $("#byp_email").addClass("el_loading");
    
    
    var data = {
      act: "validate_email",
      email: email_addr
    };
    
    var jqxhr = jQuery.post("calc/register.php", data, bypEmailValidated, 'json');
    
    jqxhr.error(function() { 
      alert("Can't validate Email, server error!");
    });
    
  }
  
}

function bypEmailValidated(data) {
  
  if (data && (typeof data.msg !== "undefined")) {
    
    if ("ERR" == data.msg) {
      
      document.getElementById("user_email_msg").innerHTML = "<strong>"+data.err + "</strong><br/>Please register with other email or if it's yours please Log in below:";
      
      var email = document.getElementById("byp_email").value
      
      document.getElementById("byp_email").value = "";
      
      $(".login_email").attr("value", email);
      
      global_byp_config.lightbox = $.lightbox({
				content: document.getElementById("login_dialog").innerHTML
		  });
      
      
    }
    if ("OK" == data.msg) {
      
    }
    
  }
  else {
    if (!data || ""==data) alert("Unknown server error!");
    else alert("Server error: " + data);
    document.getElementById("byp_email").value = "";
  }
  
  global_byp_config.submit_user = true;
  $("#byp_email").removeClass("el_loading");

}

function bypSelectComponent(id) {
	
//	alert("id"+id);
  var a_id = id.split("_");
  //alert("a_id[1]"+a_id[1]);
  //  alert("a_id[2]"+a_id[2]);
  show_image(a_id[2]);
  
  if (4==a_id.length) {
    document.getElementById(a_id[0]+"_"+a_id[1]+"_"+a_id[2]).checked = true;
  }
  

  var max_width = 90;
  var max_height = 75;

  
  var image_w = 0;
  var image_h = 0;

  if (global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image!="") {
    
    if ( global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_w > global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_h ) {
      image_w = max_width;
      image_h = Math.round( image_w * global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_h / global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_w );
    }
    else {
      image_h = max_height;
      image_w = Math.round( image_h * global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_w / global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_h );
    }
    
  }
  
  if ("" == global_byp_config.prev[a_id[1]]) {
    
    if (global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image) {
      
      $("#byp_" + a_id[1] + "_sel").css("width", image_w+"px");
      $("#byp_" + a_id[1] + "_sel").css("height", image_h+"px");
      
      document.getElementById("byp_" + a_id[1] + "_sel").src = global_byp_config.url_images + global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image;
    
      
      $("#"+a_id[0]+"_"+a_id[1]+"_"+a_id[2]+"_img").css("display", "none");
      
      $("#byp_" + a_id[1] + "_sel").animate({width:image_w+"px", height:image_h+"px"}, 700, function() {
        //alert(image_w+"x"+image_h);//debug
      });
      
      /*
      $("#byp_" + a_id[1] + "_sel").animate({width:global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_w+"px", height:global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_h+"px"}, 700, function() {
      
      });
      */
      
      global_byp_config.prev[a_id[1]] = "#"+a_id[0]+"_"+a_id[1]+"_"+a_id[2]+"_img";
      
    }
    else {
      global_byp_config.prev[a_id[1]] = "#byp_"+a_id[1]+"_sel";
    }
    
  }
  else {
    
    if (global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image!="") {
    
    //$("#byp_" + a_id[1] + "_sel").animate({width:image_w+"px", height:image_h+"px"}, 700, function() {
      
    $("#byp_" + a_id[1] + "_sel").css("width", image_w+"px");
    $("#byp_" + a_id[1] + "_sel").css("height", image_h+"px");
      
      if (global_byp_config.prev[a_id[1]] != "#byp_"+a_id[1]+"_sel")     
        $(global_byp_config.prev[a_id[1]]).css("display", "inline");
      
      document.getElementById("byp_" + a_id[1] + "_sel").src = global_byp_config.url_images + global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image;
      
      $("#"+a_id[0]+"_"+a_id[1]+"_"+a_id[2]+"_img").css("display", "none");
      
      
      if (global_mob_screen) {
        var curr_width = document.getElementById("main").offsetWidth;
        if (curr_width > 292) {
          max_width = 290;
          max_height = 320;
        }
        else {
          max_width = 200;
          max_height = 200;
        }
      }
      else {
        max_width = 141;
        max_height = 130;
      }
      
      if ( global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_w > global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_h ) {
        image_w = max_width;
        image_h = Math.round( image_w * global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_h / global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_w );
      }
      else {
        image_h = max_height;
        image_w = Math.round( image_h * global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_w / global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_h );
      }
      
      
      $("#byp_" + a_id[1] + "_sel").animate({width:image_w+"px", height:image_h+"px"}, 700, function() {
        //alert(image_w+"x"+image_h);//debug
        if (global_byp_config.prev[a_id[1]] == "#byp_"+a_id[1]+"_sel") {
          $("#byp_" + a_id[1] + "_sel").css("width", "1px");
          $("#byp_" + a_id[1] + "_sel").css("height", "1px");
        }
      });
      
      /*
      $("#byp_" + a_id[1] + "_sel").animate({width:global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_w+"px", height:global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image_h+"px"}, 700, function() {
        if (global_byp_config.prev[a_id[1]] == "#byp_"+a_id[1]+"_sel") {
          $("#byp_" + a_id[1] + "_sel").css("width", "1px");
          $("#byp_" + a_id[1] + "_sel").css("height", "1px");
        }
      });
      */
      
      global_byp_config.prev[a_id[1]] = "#"+a_id[0]+"_"+a_id[1]+"_"+a_id[2]+"_img";
  
    //});
    
    }
    else {
      
      $("#byp_" + a_id[1] + "_sel").css("width", "1px");
      $("#byp_" + a_id[1] + "_sel").css("height", "1px");
      
      document.getElementById("byp_" + a_id[1] + "_sel").src = "/images/blank.gif";
 
      $(global_byp_config.prev[a_id[1]]).css("display", "inline");
      
      global_byp_config.prev[a_id[1]] = "#byp_"+a_id[1]+"_sel";
    }
    
  }
  
  if ("wall" == a_id[1]) {
    document.getElementById("byp_room2").src = global_byp_config.url_images + global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image;
    document.getElementById("byp_room3").src = global_byp_config.url_images + global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image;
  }
  
  document.getElementById("byp_sel_img_"+a_id[1]).src = global_byp_config.url_images + global_byp_config.component[ "cb_" + a_id[1] ].options[ a_id[2] ].image;
  document.getElementById("byp_sel_img_"+a_id[1]).width = image_w;
  document.getElementById("byp_sel_img_"+a_id[1]).height = image_h;
  
  document.getElementById("byp_sel_subcat_"+a_id[1]).innerHTML = $("label[for='cb_"+a_id[1]+"_"+a_id[2]+"']").text();
  
  if ("wall"==a_id[1]) document.getElementById("byp_sel_subcat_"+a_id[1]).innerHTML += " will be removed";
  
  if ("kitchen"==a_id[1]) {
    // && "Not Required"==document.getElementById("byp_sel_subcat_"+a_id[1]).innerHTML
    //document.getElementById("byp_sel_img_"+a_id[1]).src = "/images/blank.gif";
    
    document.getElementById("byp_sel_subcat_"+a_id[1]).innerHTML = document.getElementById("byp_sel_subcat_"+a_id[1]).innerHTML + " Chimney Breast";
  }
  
  if ("floor"==a_id[1] && "Not Required"==document.getElementById("byp_sel_subcat_"+a_id[1]).innerHTML) {
    document.getElementById("byp_sel_img_"+a_id[1]).src = "/images/blank.gif";
    document.getElementById("byp_sel_subcat_"+a_id[1]).innerHTML = "Ground Floor WC Is " + document.getElementById("byp_sel_subcat_"+a_id[1]).innerHTML;
  }
  
}

function bypCostingReady(data) {
  global_byp_config.data = data;
  global_byp_config.costing_ready = true;
}

function bypCostingTimer() {
  global_byp_config.costing_timer = true;
}

function bypGetCosting() {
  
  if (!global_byp_config.costing_ready || !global_byp_config.costing_timer) return false;
  
  global_byp_config.costing_timer = false;
  global_byp_config.costing_ready = false;  
  clearInterval(global_byp_config.interval_id);
  data = global_byp_config.data;
  global_byp_config.data = "";
  
  document.getElementById("fm_byp1").style.display = "none";
  document.getElementById("fm_byp2").style.display = "none";
  document.getElementById("fm_byp3").style.display = "none";
  document.getElementById("fm_byp4").style.display = "none";
  
  document.getElementById("byp_slide_box").style.display = "block";
  document.getElementById("byp_get_costing").style.display = "none";  
  
  var err_msg = "";
  var text_err_response = "Unknown server response!";
  
  data = trim(data);
  
  if (""==data) err_msg = text_err_response + " 1";
  else {
    var data = data.split("&");
    var a_resp = [];
    if (data.length<2) err_msg = text_err_response  + " 2 " + data;//debug
    else {
      for(var i=0;i<data.length;i++) {
        data[i] = data[i].split("=");
        a_resp[data[i][0]] = trim(data[i][1]);
      }
      
      if (!a_resp["status"]) err_msg = text_err_response  + " 3 " + data;//debug
      else {
        if ("error"==a_resp["status"]) {
          if (a_resp["err_msg"]) err_msg = a_resp["err_msg"];
          else err_msg = "Unknown server error!";
        }
        else {
          if ("success"==a_resp["status"]) {
            // all is Ok, check if other vars exists
          }
          else {
            err_msg = "Unknown server error: " + a_resp["status"];
          }
        }
      }
    }
  }
  
  if (""==err_msg) {
    
    /*var s = "";//debug
    for (i in a_resp) s+= i +":"+ a_resp[i]+"; "; 
    alert(s);*/
    
    // status:success; qid:5055; qn:BYP/2JBW8/SE22; costing:33323; costing_2:36318; design_discount:1000.00; offer_exp_days:31.7.12; duration:10; cgi_url:/images/cgi1.png;
    
    // format totals
    
    document.getElementById("byp_quote_ref").innerHTML = a_resp["qn"];
    global_byp_config.qid = a_resp["qid"];
    
    global_byp_config.userid = a_resp["userid"];
    if (0!=global_byp_config.userid) {
      document.getElementById("byp_email").value = "";
      document.getElementById("byp_password").value = "";
      document.getElementById("byp_repassword").value = "";
      
      document.getElementById("byp_register_info").style.display = "none";
    }
    
    a_resp["costing"] = parseFloat(a_resp["costing"]);
    a_resp["costing_2"] = parseFloat(a_resp["costing_2"]);
    a_resp["design_discount"] = parseFloat(a_resp["design_discount"]);
    
    var design_total = a_resp["costing_2"] - Math.abs(a_resp["costing"]);
    
    var costing_3 = new NumberFormat(design_total);
    
    costing_3.setPlaces(0);
    document.getElementById("byp_design_total").innerHTML = global_currency+costing_3.toFormatted();
    var design_discount = new NumberFormat(a_resp["design_discount"]);
    design_discount.setPlaces(0);
    document.getElementById("byp_design_discount").innerHTML = global_currency+design_discount.toFormatted();
    var design_cost = new NumberFormat(design_total+a_resp["design_discount"]);
    design_cost.setPlaces(0);
    document.getElementById("byp_design_cost").innerHTML = global_currency + design_cost.toFormatted();
    
    document.getElementById("byp_offer_date").innerHTML = a_resp["offer_exp_days"];
    
    if (a_resp["costing"] > 0) {
      var costing = new NumberFormat(a_resp["costing"]);
      costing.setPlaces(0);
      document.getElementById("byp_construction_total").innerHTML = global_currency + costing.toFormatted();
      document.getElementById("byp_duration_term").innerHTML = a_resp["duration"] + " weeks";
    }
    else {
      bypPopupPOA(1);
      document.getElementById("byp_construction_total").innerHTML = "<span onclick=\"bypPopupPOA(1)\">Please Call</span>"; //with note popup
      document.getElementById("byp_duration_term").innerHTML = "-";
      if (a_resp["poa_message"] && ""!=a_resp["poa_message"]) document.getElementById("byp_poa_msg").innerHTML = a_resp["poa_message"];
    }
    
    document.getElementById("byp_cgi").src = a_resp["cgi_url"];
    
    global_byp_config.get_costing = true;

    global_byp_config.block_nav = false;
  
    document.getElementById("fm_byp1").style.display = "block";
    document.getElementById("fm_byp2").style.display = "block";
    document.getElementById("fm_byp3").style.display = "block";
    document.getElementById("fm_byp4").style.display = "block";
    
    if ("Out of Territory" == a_resp["cover"]) {
      document.getElementById("byp_cover").style.display = "block";
    }
    else document.getElementById("byp_cover").style.display = "none";
    
    if (1==a_resp['new_user']) {
      /*$("#btn_main_action").attr("onclick", "bypStep(0)");
      $("#btn_main_action div").text("REVISE QUOTE");*/
      
      $("#btn_action1").attr("onclick", "bypSubmitDetails('save')");
      $("#btn_action1").text("Save Quote");
      
      $("#btn_action2").attr("onclick", "bypStep(0)");
      $("#btn_action2").text("REVISE QUOTE");
      $("#btn_action3_wrap").addClass("btn_revise_back");
      
      $("#byp_quote_tip").css("display", "none");
      $("#byp_mob_quote_tip").css("display", "none");
      $("#byp_arrow_tip").css("display", "none");
    }
    
    bypStep(4);
    
  }
  else {
    
    alert(err_msg);
    global_byp_config.block_nav = false;
    document.getElementById("fm_byp1").style.display = "block";
    document.getElementById("fm_byp2").style.display = "block";
    document.getElementById("fm_byp3").style.display = "block";
    document.getElementById("fm_byp4").style.display = "block";
    bypStep(0);
    
  }
}

function validateUserForm(part) {
  var err_msg = "";
  
  var name = trim(document.getElementById("byp_name").value);
  var surname = trim(document.getElementById("byp_surname").value);
  
  // for not logged user only:
  var email = trim(document.getElementById("byp_email").value);
  var password = trim(document.getElementById("byp_password").value);
  var repassword = trim(document.getElementById("byp_repassword").value);
  
  var when_to_start = document.getElementById("when_to_start").value;
  var current_status = document.getElementById("current_status").value;
  var id_postcode = trim(document.getElementById("byp_postcode_f").value);
  
  var phone = trim(document.getElementById("byp_phone").value);
  var house = trim(document.getElementById("byp_house").value);
  var addr = trim(document.getElementById("byp_addr").value);
  
  if (typeof part === "undefined" || part === 1) {
    
    if (""==name || "name"==name.toLowerCase()) {
      err_msg += "Please enter your Name\n";
    }
    if (""==surname || "surname"==surname.toLowerCase()) {
      err_msg += "Please enter your Surname\n";
    }
    
    if (!global_byp_config.userid) {
    
      if (""==email || "email"==email.toLowerCase()) { // for not logged user only
        err_msg += "Please enter your Email\n";
      }
      else {
        if (email.search(/^[a-z0-9]+([-_\.]?[a-z0-9])+@[a-z0-9]+([-_\.]?[a-z0-9])+\.[a-z]{2,4}$/i) == -1) {
          err_msg += "Please enter correct Email address\n";
        }
      }
    
      if (""==password) {
        err_msg += "Please enter Password\n";
      }
      
      if (""==repassword) {
        err_msg += "Please retype Password\n";
      }
      
      if (password!=repassword) {
        err_msg += "Passwords doesn't match\n";
      }
    
    }
    
    if ("" == phone) {
      err_msg += "Please enter your Contact Number\n";
    }
    else {
      var test_phone = phone.replace(/[^0-9]/g, "");
      if (test_phone.search(/[0-9]{10,}/i) == -1) {
        err_msg += "Please enter correct Contact Number (at least 10 digits)\n";
      }
    }
    
    if ("" == house && "" == addr) {
      err_msg += "Please type your House or flat number or select address\n";
    }
  
  }
  
  if (typeof part === "undefined" || part === 2) {
    
    if (when_to_start == '0') {
      err_msg += "Please select when to start\n";
    }
    
    if (current_status == '0') {
      err_msg += "Please select current status\n";
    }
    
  }
  
  if (id_postcode == '') {
    err_msg += "Please enter your postcode\n";
  }
  
  if (""!==err_msg) {
    //alert(trim(err_msg));
    return false;
  }
  
  return true;
  
}

function bypSubmitDetails(type) {
  // block if popup
  alert("1");
  //if ("block"==document.getElementById("byp_poa").style.display || "block"==document.getElementById("attach_dialog").style.display) return false;
  
  var err_msg = ""; 
  
  if (""!=err_msg) {
    alert(err_msg);
    
  }
  else {
	alert("2");  
    // Submitting.., before submit
    //global_byp_config.block_nav = true;
    //document.getElementById("byp_slide_box").style.display = "none";
    //document.getElementById("byp_loading_text").innerHTML = "Submitting Your Details";
    //document.getElementById("byp_get_costing").style.display = "block";
    
    document.getElementById("fm_byp5").qid.value = global_byp_config.qid;
    document.getElementById("fm_byp5").userid.value = global_byp_config.userid;
    document.getElementById("fm_byp5").quote_type_id.value = type;
    document.getElementById("fm_byp5").action = global_byp_config.url_details;
    
    //after submit: bypEmailQuote(data)
    
    var options = { 
      target:        '#byp_submit_quote',   // target element(s) to be updated with server response 
      //success:       bypEmailQuote  // post-submit callback
      complete: function(xhr) {
        bypEmailQuote(xhr.responseText);
      }
    };
  
    $("#fm_byp5").ajaxSubmit(options);
    
  }
  
  return false;

}

function bypEmailQuote(data) { //, statusText, xhr, $form #debug
  var error = "";
  alert("email")
  //if (statusText!="success" || ""==data) error = "Server error";#debug
  if (""==data) error = "Server error";
  
  if (-1 == data.indexOf("status=success")) {
    error = data.replace("<textarea>", "");
    error = error.replace("</textarea>", "");
  }
  
  if (""!=error) {
    global_byp_config.block_nav = false;
    document.getElementById("byp_slide_box").style.display = "block";
    document.getElementById("byp_get_costing").style.display = "none";
    alert("Error occurred while sending the form: " + error + ("Server error"==error?" please contact the site administrator.":''));
  }
  else {
    document.getElementById("fm_byp5").style.display = "none";
    global_byp_config.block_nav = false;
    document.getElementById("byp_slide_box").style.display = "block";
    document.getElementById("byp_get_costing").style.display = "none";
    
    if ("email"==document.getElementById("fm_byp5").quote_type_id.value) {
      // We will be in touch shortly to arrange an appointment for a site visit.
      document.getElementById("thank_you_msg").innerHTML = 'Your quote has been emailed and should arrive very shortly. You will be redirected <a href="/verify-code.php?qid=' + document.getElementById("fm_byp5").qid.value + '">here to book site visit</a>.';
      
      document.location = '/verify-code.php?qid=' + document.getElementById("fm_byp5").qid.value;
      
    }
    else document.getElementById("thank_you_msg").innerHTML = "Your quote has been saved. You can retrieve it at any time in <a href=\"/saved_quote.html\">your account page</a>.";
    
    if (global_mob_screen) bypMobNextSlider(5);
    else bypStep(5);
  }
  
}

function bypPopupPOA(open) {
  if ("block"!=document.getElementById("attach_dialog").style.display) {
    
    if (open) {
      document.getElementById("byp_poa").style.display = "block";
      global_byp_config.block_nav = true;
    }
    else {
      document.getElementById("byp_poa").style.display = "none";
      global_byp_config.block_nav = false;
    }
  
  }
}

function bypPopupAttach(open) {
  if ("block"!=document.getElementById("byp_poa").style.display) {
    
    if (open && "block"!=document.getElementById("attach_dialog").style.display) {
      document.getElementById("attach_dialog").style.display = "block";
      global_byp_config.block_nav = true;
    }
    else {
      //alert(open + " : " + document.getElementById("byp_attach_box").style.display);//debug
      document.getElementById("attach_dialog").style.display = "none";
      global_byp_config.block_nav = false; 
    }
    
  }
}

function bypAddFile(el) {
  var filename = el.value;
  if (""!=filename) {
    var num = el.name.replace("photo", "");
    // get filename
    if (-1!=filename.indexOf("\\")) {
      filename = filename.split("\\");
      filename = filename.pop();
    }
    else {
      if (-1!=filename.indexOf("/")) {
        filename = filename.split("/");
        filename = filename.pop();
      }
    }
    // get extension
    var ext = filename.split(".");
    ext = ext.pop();
    ext = ext.toLowerCase();
    if ("jpg"!=ext && "jpeg"!=ext && "png"!=ext && "gif"!=ext) {
      alert("Only the following extensions are allowed: JPG, JPEG, PNG, GIF.");
      var newInput = document.createElement("input");
      newInput.type = "file";
      newInput.id = el.id;
      newInput.name = el.name;
      newInput.className = el.className;
      newInput.onchange = el.onchange;
      // copy any other relevant attributes
      el.parentNode.replaceChild(newInput, el);
    }
    else {
      el.style.display = "none";
      //alert(filename + " " + num);
      var files_count = document.getElementById("byp_files_count").innerHTML;
      files_count = files_count.split(" / ");
      var count = parseInt(files_count[0]);
      count++;
      document.getElementById("byp_files_count").innerHTML = count + " / " + files_count[1];
      
      if ("[Files to be uploaded]"==trim(document.getElementById("byp_attach_files").innerHTML)) {
        document.getElementById("byp_attach_files").innerHTML = "<span id=\"byp_filename\">" + filename + " <a href=\"javascript:bypRemoveFile("+(num?num:0)+")\" title=\"remove from list\">(X)</a></span>";
      }
      else {
        document.getElementById("byp_attach_files").innerHTML += ", <span id=\"byp_filename"+num+"\">" + filename + " <a href=\"javascript:bypRemoveFile("+(num?num:0)+")\" title=\"remove from list\">(X)</a></span>";
      }
    }
  }
}

function bypRemoveFile(num) {
  //alert(num);
  document.getElementById("photo"+(num?num:"")).style.display = "block";
  var oldInput = document.getElementById("photo"+(num?num:""));
  var newInput = document.createElement("input");
  newInput.type = "file";
  newInput.id = oldInput.id;
  newInput.name = oldInput.name;
  newInput.className = oldInput.className;
  newInput.onchange = oldInput.onchange;
  // copy any other relevant attributes
  oldInput.parentNode.replaceChild(newInput, oldInput);
  
  $("#byp_filename"+(num?num:"")).remove();
  if (""==trim(document.getElementById("byp_attach_files").innerHTML)) document.getElementById("byp_attach_files").innerHTML = "[Files to be uploaded]";
  
  // clear commas
  var str2 = trim(document.getElementById("byp_attach_files").innerHTML.substr(0,2));
  if (","==str2) {
    document.getElementById("byp_attach_files").innerHTML = document.getElementById("byp_attach_files").innerHTML.substr(2);
  }
  var str2 = trim(document.getElementById("byp_attach_files").innerHTML.substr(-2,2));
  if (","==str2) {
    document.getElementById("byp_attach_files").innerHTML = document.getElementById("byp_attach_files").innerHTML.substr(0, document.getElementById("byp_attach_files").innerHTML.length-2);
  }
  document.getElementById("byp_attach_files").innerHTML = document.getElementById("byp_attach_files").innerHTML.replace(", ,", ",");
  
  //decrease counter
  var files_count = document.getElementById("byp_files_count").innerHTML;
  files_count = files_count.split(" / ");
  var count = parseInt(files_count[0]);
  count--;
  document.getElementById("byp_files_count").innerHTML = count + " / " + files_count[1];  
}

function disableTabKey(e) {
  if (window.event) {
    key = window.event.keyCode;  // IE
    if (9==key) {
      return doTabWork();
      
    }
  }
  else {
    key = e.which;   // Firefox
    if (9==key) {
      return doTabWork();
    }
  }
  return true;
}

function doTabWork() {
  if (3==global_byp_went) {
    var focused = document.activeElement
    if ("byp_showmyquote_link" == focused.id) return false;
    return true;
  }
  else return false;
}

function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
  var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
  if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
  
function trim(s) {
  s = s.replace(/\s+$/, '');
  s = s.replace(/^\s+/, '');
  return s;
} 