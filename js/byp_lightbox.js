(function ($) {



    $.lightbox = function (opts) {

        opts = $.extend(true, {

            content: '',

            classes: {

                overlay: 'overlay',

                lightbox: 'lightbox'

            },

            windowTop: false,

            closeContent: 'Close',

            remove: true



        }, opts);



        /*Overlay creation*/

        var $overlay = $('<div class="' + opts.classes.overlay + '" />')

        .appendTo(document.body)

        .css({

            height: $(document).height(),

            width: $(document).width()

        })

        .click(function () {

            close();

        });



        /*on window resizing*/

        $(window).resize(function () {

            $overlay.css({

                height: $(document).height(),

                width: $(document).width()

            });

             setPosition();

        });



        /*lightbox creation*/

        var $lightbox = $(

            '<div class="' + opts.classes.lightbox + '">' +

                '<div class="lightboxInner01">' +

                    opts.content +

                '</div>' +

            '</div>'

        )

        .appendTo(document.body)

        .bind('closelightbox', function () { close(); })

        .bind('resizelightbox', function (e, width) {

            $lightbox.width(width);

            setPosition();

        });



        /*Close link creation*/

        $('<a href="#" class="close">' + opts.closeContent + '</a>')

        .prependTo($lightbox)

        .click(function (e) {

            e.preventDefault();

            close();

        });



        setPosition();



        /**

        *Set the top and left position of the light box

        **/

        function setPosition() {

            $lightbox.css({

                left: ($(window).width() - $lightbox.width()) / 2,

                top: getTop()

            });

        }



        /**

        *Get the top value based on lightbox and window height

        *@return top (int)

        **/

        function getTop() {

            var top = $(window).scrollTop() + 20;

            if ($lightbox.height() < $(window).height()) {

                if (!opts.windowTop) {

                    top += ($(window).height() - $lightbox.height()) / 2;

                }

            }

            return top;

        }



        /**

        * Triggers 'onclose.lightbox' and removes $overlay and $lightbox 

        **/

        function close() {

            $lightbox.trigger('oncloselightbox');

            if (opts.remove) {

                $overlay.remove();

                $lightbox.remove();

            } else {

                $overlay.hide();

                $lightbox.hide();

            }

        }



        return [$lightbox, $overlay];



    }



})(jQuery);