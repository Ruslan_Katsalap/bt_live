jQuery(document).ready(function($) {
	
	$('input[type="radio"][name="live_in"]').click(function(){
		if($(this).val() == 'flat')
		{
			$('#flat').css('background', '#6B9FDB');
			$('#flat').css('border', '1px dotted rgb(57, 95, 141)');
			$('#house').css('background', '#F79646');
			$('#house').css('border', '2px solid #b66d31');
		}
		else if($(this).val() == 'house')
		{
			$('#house').css('background', '#F7A96A');
			$('#house').css('border', '1px dotted #b66d31');
			$('#flat').css('background', '#4F81BD');
			$('#flat').css('border', '2px solid rgb(57, 95, 141)');
		}
	});
	$('input[type="radio"][name="house_type"]').click(function(){
		if($(this).val() == 'terrace-house')
		{
			$('#terrace_house').attr('src','images/click.jpg');
			$('#semi_attached_house').attr('src','images/round.jpg');
			$('#detached_house').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'semi-attached-house')
		{
			$('#semi_attached_house').attr('src','images/click.jpg');
			$('#terrace_house').attr('src','images/round.jpg');
			$('#detached_house').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'detached-house')
		{
			$('#detached_house').attr('src','images/click.jpg');
			$('#semi_attached_house').attr('src','images/round.jpg');
			$('#terrace_house').attr('src','images/round.jpg');
		}
	});
	$('input[type="radio"][name="extension_type"]').click(function(){
		if($(this).val() == 'side-infill-extension')
		{
			$('#side_infill_extension').attr('src','images/click.jpg');
			$('#rear_extension').attr('src','images/round.jpg');
			$('#side_infill_rear').attr('src','images/round.jpg');
			$('#wraparound_extension').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'rear-extension')
		{
			$('#side_infill_extension').attr('src','images/round.jpg');
			$('#rear_extension').attr('src','images/click.jpg');
			$('#side_infill_rear').attr('src','images/round.jpg');
			$('#wraparound_extension').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'side-infill-rear')
		{
			$('#side_infill_extension').attr('src','images/round.jpg');
			$('#rear_extension').attr('src','images/round.jpg');
			$('#side_infill_rear').attr('src','images/click.jpg');
			$('#wraparound_extension').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'wraparound-extension')
		{
			$('#side_infill_extension').attr('src','images/round.jpg');
			$('#rear_extension').attr('src','images/round.jpg');
			$('#side_infill_rear').attr('src','images/round.jpg');
			$('#wraparound_extension').attr('src','images/click.jpg');
		}
	});
	/*test*/
	$('input[type="radio"][name="roof_type"]').click(function(){
		if($(this).val() == 'pitched')
		{
			$('#pitched').attr('src','images/click.jpg');
			$('#flatt').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'flatt')
		{
			$('#pitched').attr('src','images/round.jpg');
			$('#flatt').attr('src','images/click.jpg');
		}
	});
	$('input[type="radio"][name="skylight"]').click(function(){
		if($(this).val() == 'skylight')
		{
			$('#skylight').attr('src','images/click.jpg');
			$('#all_glass').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'all-glass')
		{
			$('#skylight').attr('src','images/round.jpg');
			$('#all_glass').attr('src','images/click.jpg');
		}
	});
	$('input[type="radio"][name="door_type"]').click(function(){
		if($(this).val() == 'sliding-doors')
		{
			$('#sliding_doors').attr('src','images/click.jpg');
			$('#bi_fold_doors').attr('src','images/round.jpg');
			$('#french_doors').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'bi-fold-doors')
		{
			$('#sliding_doors').attr('src','images/round.jpg');
			$('#bi_fold_doors').attr('src','images/click.jpg');
			$('#french_doors').attr('src','images/round.jpg');
		}
		else if($(this).val() == 'french-doors')
		{
			$('#sliding_doors').attr('src','images/round.jpg');
			$('#bi_fold_doors').attr('src','images/round.jpg');
			$('#french_doors').attr('src','images/click.jpg');
		}
	});
	$('input[type="radio"][name="remove_chimney"]').click(function(){
		if($(this).val() == 'remove-chimney')
		{
			$('#remove_Chimney').attr('src','images/click.jpg');
		}
	});
	$('input[type="radio"][name="underfloor_heating"]').click(function(){
		if($(this).val() == 'yes')
		{
			$('#Underfloor_Heating').attr('src','images/click.jpg');
		}
	});
	$('input[type="radio"][name="ad_wc"]').click(function(){
		if($(this).val() == 'yes')
		{
			$('#Ad_WC').attr('src','images/click.jpg');
		}
	});
	
	jQuery('.live_in').click(function(){
		var currentVal = jQuery(this).val();
		if(currentVal == 'side-infill-extension')
		{
			jQuery('#dynamic_extension > img').attr('src','images/click.jpg');
		}
		else
		{
			jQuery('#dynamic_extension > img').attr('src','images/thelenth.jpg');
		}
	});
	
	
	$("#multistep").validationEngine();
	if($.browser.msie && $.browser.version < 10)
		$("#multistep input").not(":hidden").each(function(){
			$(this).css({color:"#666666"}).val($(this).attr("placeholder")).focus(function(){if($(this).val() == $(this).attr("placeholder")) $(this).val("");}).blur(function(){if(!$(this).val()) $(this).val($(this).attr("placeholder"));});
		});
	/*
		$('#credit_card').customSelect();
		$('#state').customSelect();
	*/
	
	 var formFlex = $('#formFlex').flexslider({
		animation: 'slide',
		animationLoop: false,
		selector: 'form > fieldset',
		easing: 'easeInOutQuad',
		smoothHeight: true,
		useCSS: false,
		controlNav: false,
		directionNav: false,
		animationLoop: false,
		slideshow: false,
		pauseOnAction: false,
		touch: false,
		keyboard: false,
		pauseOnHover: false
	}).stop();


	$('form#multistep .start_progress').click(function(){
		if($('#postcode_start').val() != '')
		{
			var postalCode = $('#postcode_start').val();
			var regPostcode = /^([a-zA-Z]){1}([0-9][0-9]|[0-9]|[a-zA-Z][0-9][a-zA-Z]|[a-zA-Z][0-9][0-9]|[a-zA-Z][0-9]){1}([ ])([0-9][a-zA-z][a-zA-z]){1}$/;
			var result = regPostcode.test(postalCode);
			if(!result)
			{
				$('#Required_postcode').css('display', 'block');
				$('#Required_postcode').html('Please Enter Valid UK Postcode !');
				return false;	
			}
			else
			{
				formFlex.flexslider("next");
				$(this).blur();
			}
		}
		else
		{
			$('#postcode_start').addClass('error_msg');
			$('#Required_postcode').css('display', 'block');
			return false;
		}
	})
	
	$('form#multistep .back_process').click(function(){
		formFlex.flexslider("prev");
		$(this).blur();
	})

});

