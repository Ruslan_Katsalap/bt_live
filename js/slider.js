$(function() {
	
	var totalPanels			= $(".scrollContainer").children().size();
		
	var regWidth			= $(".panel").css("width");
	var regImgWidth			= $(".panel img").css("width");
	var regTitleSize		= $(".panel h2").css("font-size");
	var regParSize			= $(".panel p").css("font-size");
	
	var movingDistance	    = 480;
	var movingItems					= 3;
	
	var curWidth			= 160;
	var curImgWidth			= 100;
	var curTitleSize		= "12px";
	var curParSize			= "11px";

	var $panels				= $('#slider .scrollContainer > div');
	var $container			= $('#slider .scrollContainer');

	$panels.css({'float' : 'left','position' : 'relative'});
    
	$("#slider").data("currentlyMoving", false);

	/*
	$container
		.css('width', ($panels[0].offsetWidth * $panels.length) + 100 )
		.css('left', "-350px");
	*/

	$container
		.css('width', (141 * $panels.length) )
		.css('left', "0");

	var scroll = $('#slider .scroll').css('overflow', 'hidden');

	function returnToNormal(element) {/*
		$(element)
			.animate({ width: regWidth })
			.find("img")
			.animate({ width: regImgWidth })
		    .end()
			.find("h2")
			.animate({ fontSize: regTitleSize })
			.end()
			.find("p")
			.animate({ fontSize: regParSize });*/
	};
	
	function growBigger(element) {/*
		$(element)
			.animate({ width: curWidth })
			.find("img")
			.animate({ width: curImgWidth })
		    .end()
			.find("h2")
			.animate({ fontSize: curTitleSize })
			.end()
			.find("p")
			.animate({ fontSize: curParSize });*/
	}
	
	//direction true = right, false = left
	function change(direction) {
	   
	  //if not at the first or last panel
		//if((direction && !(curPanel < totalPanels)) || (!direction && (curPanel <= 1))) { return false; }
		var leftValue = parseFloat($(".scrollContainer").css("left"), 10);
		var widthValue = parseFloat($(".scrollContainer").css("width"), 10);
    if ( direction && (widthValue + leftValue) <= 800 ) { return false; }
    if ( !direction && (leftValue) >= 0 ) { return false; }	
		
      //if not currently moving
      if (($("#slider").data("currentlyMoving") == false)) {
            
			$("#slider").data("currentlyMoving", true);
			
			var next         = direction ? curPanel + movingItems : curPanel - movingItems;
			var movement	 = direction ? parseFloat(leftValue, 10) - movingDistance : parseFloat(leftValue, 10) + movingDistance;
		
			$(".scrollContainer")
				.stop()
				.animate({
					"left": movement
				}, function() {
					$("#slider").data("currentlyMoving", false);
				});
			
			returnToNormal("#panel_"+curPanel);
			growBigger("#panel_"+next);
			
			curPanel = next;
			
			//remove all previous bound functions
			$("#panel_"+(curPanel+1)).unbind();	
			
			//go forward
			$("#panel_"+(curPanel+1)).click(function(){ change(true); });
			
            //remove all previous bound functions															
			$("#panel_"+(curPanel-1)).unbind();
			
			//go back
			$("#panel_"+(curPanel-1)).click(function(){ change(false); }); 
			
			//remove all previous bound functions
			$("#panel_"+curPanel).unbind();
		}
	}
	
	// Set up "Current" panel and next and prev
	growBigger("#panel_3");	
	var curPanel = 3;
	
	$("#panel_"+(curPanel+1)).click(function(){ change(true); });
	$("#panel_"+(curPanel-1)).click(function(){ change(false); });
	
	//when the left/right arrows are clicked
	$(".right_pad").click(function(){ change(true); });	
	$(".left_pad").click(function(){ change(false); });
	
	$(window).keydown(function(event){
	  switch (event.keyCode) {
			case 13: //enter
				$(".right_pad").click();
				break;
			case 32: //space
				$(".right_pad").click();
				break;
	    case 37: //left arrow
				$(".left_pad").click();
				break;
			case 39: //right arrow
				$(".right_pad").click();
				break;
	  }
	});
	
});