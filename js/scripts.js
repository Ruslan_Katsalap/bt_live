jQuery(document).ready(function($) {
	
	jQuery('#step_four').click(function(){
		if(jQuery('input[name="ad_wc"]').is(':checked'))
		{
			var addWC = '<p>Install WC</p>';
		}
		else
		{
			var addWC = '';
		}
		if( jQuery('input[name="remove_chimney"]').is(':checked') )
		{
			var remove = '<p>Remove Chimney Breast</p>';
		}
		else
		{
			var remove = '';
		}
		if( jQuery('input[name="underfloor_heating"]').is(':checked') )
		{
			var underheat = '<p>Underfloor Heating</p>';
		}
		else
		{
			var underheat = '';
		}
		if( jQuery('input[name="skylight"]').is(':checked') )
		{
			var val_sky = jQuery('input[name="skylight"]:checked').val();
			var skyylight = '<p>'+val_sky.replace(/\-/g, " ")+'</p>';
		}
		else
		{
			var skyylight = '';
		}
		if( jQuery('input[name="door_type"]').is(':checked') )
		{
			var val_door = jQuery('input[name="door_type"]:checked').val();
			var doorss = '<p>'+val_door.replace(/\-/g, " ")+'</p>';
		}
		else
		{
			var doorss = '';
		}
		if( jQuery('input[name="extension_type"]').is(':checked') )
		{
			var val_xten = jQuery('input[name="extension_type"]:checked').val();
			var xtensn = val_xten.replace(/\-/g, " ");
		}
		else
		{
			var xtensn = '';
		}
		jQuery('#final_size').html(jQuery('#full_length').val()+ '&nbsp;m &nbsp; * &nbsp;' +jQuery('#depth').val()+ '&nbsp;m');
		jQuery('#final_extension_type').html(xtensn);
		jQuery('#final_specifications').html( skyylight +' '+ doorss +' '+ underheat +' '+ addWC +' '+ remove );
	})
	
	$("#multistep").validationEngine();
	if($.browser.msie && $.browser.version < 10)
		$("#multistep input").not(":hidden").each(function(){
			$(this).css({color:"#666666"}).val($(this).attr("placeholder")).focus(function(){if($(this).val() == $(this).attr("placeholder")) $(this).val("");}).blur(function(){if(!$(this).val()) $(this).val($(this).attr("placeholder"));});
		});
	/*
		$('#credit_card').customSelect();
		$('#state').customSelect();
	*/
	
	 var formFlex = $('#formFlex').flexslider({
		animation: 'slide',
		animationLoop: false,
		selector: 'form > fieldset',
		easing: 'easeInOutQuad',
		smoothHeight: true,
		useCSS: false,
		controlNav: false,
		directionNav: false,
		animationLoop: false,
		slideshow: false,
		pauseOnAction: false,
		touch: false,
		keyboard: false,
		pauseOnHover: false
	}).stop();


	$('form#multistep .start_progress').click(function(){
		formFlex.flexslider("next");
		$(this).blur();
		
		/*if($('#postcode_start').val() != '')
		{
			var postalCode = $('#postcode_start').val();
			var regPostcode = /^([a-zA-Z]){1}([0-9][0-9]|[0-9]|[a-zA-Z][0-9][a-zA-Z]|[a-zA-Z][0-9][0-9]|[a-zA-Z][0-9]){1}([ ])([0-9][a-zA-z][a-zA-z]){1}$/;
			var result = regPostcode.test(postalCode);
			if(!result)
			{
				$('#Required_postcode').css('display', 'block');
				$('#Required_postcode').html('Please Enter Valid UK Postcode !');
				return false;	
			}
			else
			{
				formFlex.flexslider("next");
				$(this).blur();
			}
		}
		else
		{
			$('#postcode_start').addClass('error_msg');
			$('#Required_postcode').css('display', 'block');
			return false;
		}*/
		
	})
	
	$('form#multistep .back_process').click(function(){
		formFlex.flexslider("prev");
		$(this).blur();
	})
	
	$('form#multistep #revise_back').click(function(){
		formFlex.flexAnimate(1);
		$(this).blur();
	})

});