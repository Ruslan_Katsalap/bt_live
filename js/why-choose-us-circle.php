<?php
global $ROOT_FOLDER;
global $meta_title;
$meta_title = "test";
require_once('./inc/header.inc.php');

define('WISE_PATH', $ROOT_FOLDER);
define('WISE_RELURL', '');
require_once('save_postcode.php');
?>
<script src="js/progressbar.js"></script>
<style type="text/css">
.main {
	padding: 20px 0 0 0;
}

.fm_bar {
  float:left;
  width:30%;
}
.newdesigntitle {
	color:#0f4778 !important;
	font-size:30px;
	text-transform:none !important;
	font-weight:bold;
	padding-bottom:10px;
	}
.newdesignsub {
	color:#75787f;
	font-size:22px;
	padding-bottom:30px;
	font-weight:normal;
	display:block;
	border-bottom:5px solid #e5e5e5;
	margin-bottom:20px;
	}	
.formtitlebor {
	border-bottom:2px solid #e5e5e5;
	color:#4b759a;
	font-size:20px;
	display:block;
	padding:10px 0px;
	font-weight:500;
	}
.fm_bar fieldset {
  border:none;
  padding-top:0;
}
.fm_bar input[type=submit], .fm_bar input[type=button] {
  background-color:#4b759a;
  border:none;
  color:#fff;
  cursor:pointer;
  font-size:16px;
  font-weight:normal;
  padding:10px 5px;
  width:100px;
  text-align:center;
  text-transform:capitalize;
}

.fm_bar select, .fm_bar input[type=text] {
  color:#75787f;
  font-size:16px;
  font-weight:normal;
  margin-top:21px;
  width:95%;
  border: 2px solid #dcddde;
  padding-left: 10px;
  height: 40px;
  text-transform: capitalize;
  font-style: normal;
}

.fm_bar input[type=submit] {
  margin-top:21px;
  margin-bottom:25px;
}

select#sel_floor {
    margin-bottom: 20px;
}
#sel_borough {

}

.wrap_results {
  float: left;
  margin-top: 1em;
  width: 68%;
  padding-left: 15px;
}
.nav-tabs {
	margin-top:-20px;
	}

.found_results {
  padding-top: 17px !important;
    color: #0e4778;
	font-size:18px;
}
 .bookthanksinner {
	 height:135px !important;
	 }
p.center {
  text-align:center;
}

.found_results a {
  margin-top:20px;
}

.found_results .total {
  font-size:27px;
  margin-bottom:7px;
}

.found_results .clear {
  clear:both;
  margin-top:17px;
}

.found_results .item {
  /*border:1px dotted red;*/
  color:gray;
  font-size:14px;
}

.found_results .item img {
  width:100%;
  max-width:286px;
}

.found_results .item .title {
  /*clear:both;
  float:left;*/
  color:gray;
  display:inline-block;
  margin-left:1em;
}

.freeflow {
  clear:both;
  float:left;
  width:100%;
}

#road_name {
  width:95%;
  min-width:200px;
}
  
#map-canvas {
  clear:both;
  height: 512px; 
  width 100%; 
  margin: 0; 
  padding: 0;
}

.dt-info {  
  clear:both;
  float:left;
  font-size:12px;
  margin-bottom:11px;
  min-height:100px;
  width:286px;
}

.dt-info h3 {
  color:#292929;
  font-size:14px;
  margin-top:7px;
  margin-bottom:7px;
}

.dt-info img {
  float:left;
  margin-right:3px;
  width:143px;
  height:101px;
}

/* Map */

#map-canvas .infoBox {
  background-color: #ffffff;
  border: 3px solid #234d9d;
  border-radius:7px;
  box-shadow: 2px 2px 4px #888888;
  -webkit-box-shadow: 2px 2px 4px #888888;
  -moz-box-shadow: 2px 2px 4px #888888;
  position: relative;
  min-width:300px;
}


#map-canvas .infoBox:after {
  background-image: url(/images/design_planning/map_info_corner5.png);
  background-repeat: no-repeat;
  background-position:0 0;
  content: " ";
  position: absolute;
  width: 30px;
  height: 18px;
  top:100%;
  left:140px;
}

.nav-tabs {
  list-style:none;
}

.nav-tabs li {
  border-top-left-radius:5px;
  border-top-right-radius:5px;
  background-color:#C8D0DE;
  display:block;
  float:left;
}
.nav-tabs li.active { 
  background-color:#e1eafa;
}
  
.nav-tabs li a { 
  display:block;
  font-size:14px;
  padding:14px;
  text-decoration:none;
}

.nav-tabs, .tab-content, .tab-pane {
  clear:both;
  float:left;
  width:100%;
}
  
.tab-pane {
  display:none;
}
  
#matrix.tab-pane {
  border:3px solid #e1eafa;
}
  
.tab-pane.active {
  display:block;
}
.closethis {
    position: absolute;
    right: 0px;
    top: 10px;
}
.closethis a {
    background: none;
    color: #104674;
    font-size: 36px;
    font-weight: 500;
    line-height: 18px;
}
.closethis a:hover{
	text-decoration:none;
	}	
@media (max-width:640px) {
	.fm_bar, .wrap_results {
		width:100%;
		}
	.fm_bar select {
		margin-top:0px;
		border:none;
		border-top:2px solid #dcddde;
		height: 30px;
		}
	.bottompartform h2 {
		border-bottom:none;
		border-top:2px solid #dcddde;
		display: inline-block;
    	width: 95%;
		
		}
	.formtitlebor {
		border:none;
		}	
	.newdesignsub {
		border-bottom: 2px solid #e5e5e5;
    	margin-bottom: 0px;
		font-size:18px;
		}	
	#road_name {
		margin-top:5px;
		}			
	#sel_floor {
		border-bottom: 2px solid #e5e5e5;
		}	
	} 
@media only screen and (max-width: 768px) {
	.main {
		padding: 54px 0 0 0 !important;
		width: 94%;
	}
}
</style>
<style type="text/css">
	.parallax-3 .wcu-container{
		width: 100%;
		max-width: 980px;
		//height: 1230px;
		//padding: 0 35px;
		margin: 0 auto;
	}
	.parallax-3 .wcu-mobile-view{
		display: none;
	}
   .parallax-3 .wcu-container #sec-why-choose-us
   {
       position: relative;
       width: 43%;
       height: auto;
       border: 5px solid #c3d1dd;
       border-radius: 50%;
       margin: 280px auto;
       padding-top: 43%;
	   transform: rotate(11deg);
   }
	.parallax-3 .wcu-container .guarantanee {
		width: 210px;
		height: 219px;
		margin: 0 auto;
		background: url(buildIcons/guarantee-work.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 40px;
	}
	.parallax-3 .wcu-container .value-for-money {
		width: 210px;
		height: 75px;
		margin: 0 auto;
		background: url(buildIcons/value-for-money.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 56px;
	}
	.parallax-3 .wcu-container .house-tours {
		width: 210px;
		height: 100px;
		margin: 0 auto;
		background: url(buildIcons/house-tours.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 69px;
		/* float: left; */
		margin-bottom: 8px;
	}
	.parallax-3 .wcu-container .unparalled {
		width: 210px;
		height: 80px;
		margin: 0 auto;
		background: url(buildIcons/unparalleled-support.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 59px;
		/* float: left; */
		margin-bottom: 0px;
	}
	.parallax-3 .wcu-container .experienced {
		 width: 210px;
		height: 80px;
		margin: 0 auto;
		background: url(buildIcons/experience.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 59px;
		/* float: left; */
		margin-bottom: 0px;
	}
	.parallax-3 .wcu-container .customer-service {
		width: 210px;
		height: 80px;
		margin: 0 auto;
		background: url(buildIcons/customer-service.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 82px;
		/* float: left; */
		margin-bottom: 6px;
	}
	.parallax-3 .wcu-container .customer-loyality {
		width: 210px;
		height: 80px;
		margin: 0 auto;
		background: url(buildIcons/company-loyalty.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 50px;
		/* float: left; */
		margin-bottom: 6px;
    }
	.parallax-3 .wcu-container .one-stop {
		width: 210px;
		height: 90px;
		margin: 0 auto;
		background: url(buildIcons/one-stop-shop.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 124px;
		/* float: left; */
		margin-bottom: 0px;
    }
	.parallax-3 .wcu-container .flexible {
		width: 210px;
		height: 132px;
		margin: 0 auto;
		background: url(buildIcons/flexible.png);
		background-position: center;
		background-repeat: no-repeat;
		background-size: 44px;
		/* float: left; */
		margin-bottom: 11px;
    }
   .parallax-3 .wcu-container .div2
   {
       position: absolute;
       //width: 226px;
       //height: 226px;
       width: 35%;
       height: 35%;
	   border: 2px dotted #c3d1dd;
       border-radius: 50%;
	   padding: 0;
	   margin: 0;
   }
   .parallax-3 .wcu-container .option-content{
		transform: rotate(-11deg);
		padding: 10%;
		width: 100%;
		height: 100%;
		box-sizing: border-box !important;
		position: absolute;
		top: 8px;
		display: flex;
		align-items: center;
		flex-direction: column;
		justify-content: center;
		text-align: center;
		opacity: 1 !important;
   }
   .parallax-3 .wcu-container .option-content img{
		margin-bottom: 10px;
		margin-top: -26px;
   }
   .parallax-3 .wcu-container .option-content h2{
		color: #0f4778;
		font-size: 16px;
		text-transform: capitalize;
   }
   .parallax-3 .wcu-container .wcu-content{
		opacity: 0;
		//-webkit-transition: opacity 0.2s linear;
		//transition:opacity 0.2s linear;
		position: absolute;
		top: 0;
		transform: rotate(-11deg);
		padding: 10%;
		text-align: center;
		height: 100%;
		box-sizing: border-box !important;
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
   }
   .parallax-3 .wcu-container .wcu-content.active{
		opacity: 1;
   }
   .parallax-3 .wcu-container .wcu-content img {
		transform: scale(1.5);
		-webkit-transform: scale(1.5);
		margin-bottom: 25px
   }
   .parallax-3 .wcu-container .wcu-content p {
		color: #9d9d9d;
		font-size: 18px;
		padding: 15px 47px;
	}
   .parallax-3 .wcu-container .wcu-content h2{
		color: #0f4778;
		font-size: 26px;
		text-transform: capitalize;
   }
   
@media (max-width:640px) {
	section.parallax-3{
		width: 100% !important;
		margin: 0 auto;
	}
	.parallax-3 .wcu-container{
		display: none;
	}
	.parallax-3 .wcu-mobile-view{
		display: block;
	}
	.parallax-3 .wcu-mobile-view .m-wcu-sec{
		text-align: center;
		width: 100%;    
		margin: 10px auto;
		margin-bottom: 20px;
		border-bottom: 2px dotted #0a4f7c;
	}
	.parallax-3 .wcu-mobile-view .m-wcu-sec.m-wcu-sec0{
		text-align: left;
		width: 100%;
	}
	.parallax-3 .wcu-mobile-view .m-wcu-sec h2{
		font-size: 28px;
		color: #0a4f7c;
		margin-bottom: 0;
	}
	.parallax-3 .wcu-mobile-view .m-wcu-sec img{
		margin-bottom: 15px;
	}
	.parallax-3 .wcu-mobile-view .m-wcu-sec h3{
		font-size: 24px;
		color: #0a4f7c;
		margin-bottom: 10px;
	}
   .parallax-3 .wcu-mobile-view .m-wcu-sec p{
		color: #969696;
		font-size: 14px;
		padding: 15px 0;
		padding-top: 5px;
		line-height: 20px;
   }
	
} 
</style>
<?php
  // Controller (logic, vars) part:

  $a_property = array('h' => 'House', 'f' => 'Flat');
  
  $a_borough = array();
  
  $sql = 'SELECT DISTINCT(d.borough), b.id, b.name FROM design_planning d LEFT JOIN `borough` b ON (d.borough=b.id)';
  $rs = getRs($sql);
  while ($row = mysqli_fetch_assoc($rs)) {
    $a_borough[ $row['id'] ] = $row['name'];
  }
  
  $a_roof = array('sv' => 'Slate & Velux', 'f' => 'Flat Roof', 'g' => 'All Glass');
  
  $a_floor = array(
    '-25' => '&lt; 25 SQM', // m<sup>2</sup>
    '25-29' => '25-29 SQM',
    '30-34' => '30-34 SQM',
    '35-39' => '35-39 SQM',
    '40-44' => '40-44 SQM',
    '45-' => '> 45 SQM'
  );
  
  if (!isset($_GET['road_name'])) $_GET['road_name'] = '';
  else $_GET['road_name'] = trim(substr(trim((string)$_GET['road_name']), 0, 30));
  
  if ($_GET['road_name']) {
    $_GET['property'] = '';
    $_GET['borough'] = '';
    $_GET['roof'] = '';
    $_GET['floor'] = ''; 
  }
  
  if (!isset($_GET['property'])) $_GET['property'] = '';
  if (!isset($_GET['borough'])) $_GET['borough'] = '';
  if (!isset($_GET['roof'])) $_GET['roof'] = '';
  if (!isset($_GET['floor'])) $_GET['floor'] = '';

  $where = '';
  // do not use page navigation for both views (map and matrix) per now
  
  //echo '!!!!!!!!!!!!!!!!<pre>'.print_r($_GET,1).'</pre>';#debug
  $out_of_territory = false;
  if ($_GET['road_name']) {
    //$where .= " AND title LIKE '%".mysql_real_escape_string($_GET['road_name'])."%'";
    $where .= " AND 1=1"; // show all on map, but centered close to search criteria point (by road name or postcode)
    
    // check if road name is possible postcode and if it's in territory
    
    $rs = getRs("SELECT map_postcodes FROM setting s WHERE s.setting_id = 1");
    
    $map_postcodes = array();
    $out_of_territory = false;
    $row = mysqli_fetch_assoc($rs);
    if (isset($row['map_postcodes'])) {
      $map_postcodes = explode(',', $row['map_postcodes']);
    }
    
    $postpart = $_GET['road_name'];
    $postpart = str_replace(' ', '', $postpart);
    
    if ($map_postcodes && strlen($postpart)<8) {
      
      if (strlen($postpart)<6 && strlen($postpart)>4) $postpart = substr($postpart, 0, 2);
      elseif (strlen($postpart)==6) $postpart = substr($postpart, 0, 3);
      else $postpart = substr($postpart, 0, 4);
      
      //echo print_r($map_postcodes, 1).' '.$postpart.':)';#debug
      
      $cover = false;
      foreach ($map_postcodes AS $v) {
        $v = trim($v);
        if (strtoupper($postpart) === strtoupper($v)) {
          $cover = true;
          break;
        }
      }
      
      if (!$cover) {
        $out_of_territory = true;
        //echo '<br/> out_of_territory!:)';#debug
      }
      
      
    }
    
  }
  
  $is_filter = false;
  
  // disable $where to get all results for map, no pagination navigation

  if ($_GET['property'] && in_array($_GET['property'], array_keys($a_property))) {
    //$where .= " AND property='".mysql_real_escape_string($_GET['property'])."'";
    $is_filter = true;
  }
  else $where .= " AND property<>''";
  
  if ($_GET['borough'] && in_array($_GET['borough'], array_keys($a_borough))) {
    //$where .= " AND borough='".(int)$_GET['borough']."'";
    $is_filter = true;
  }
  else $where .= " AND borough<>0";
  
  if ($_GET['roof'] && in_array($_GET['roof'], array_keys($a_roof))) {
    //$where .= " AND roof='".mysql_real_escape_string($_GET['roof'])."'";
    $is_filter = true;
  }
  else $where .= " AND roof<>''";
  
  $floor = '';
  if ($_GET['floor'] && in_array($_GET['floor'], array_keys($a_floor))) {
    $floor = explode('-', $_GET['floor']);
    if (2==count($floor)) {
      $floor[0] = (float)$floor[0];
      $floor[1] = (float)$floor[1];
      if ($floor[0] && $floor[1]) {
        //$where .= " AND floor > ".(float)$floor[0]." AND floor <= ".(float)$floor[1];
      }
      elseif (!$floor[0] && $floor[1]) {
        //$where .= " AND floor <= ".(float)$floor[1];
      }
      elseif ($floor[0] && !$floor[1]) {
        //$where .= " AND floor > ".(float)$floor[0];
      }
      $is_filter = true;
    }
    else $floor = '';
  }
  //else $where .= " AND floor<>0";#last

  $sql = 'SELECT id, title, property, borough, roof, floor, pdffile, postcode, date_created FROM design_planning WHERE 1=1'.$where.' ORDER BY id DESC';
  
  //echo $sql . "<br />";#debug
  
  $rs = mysqli_query($dbconn,$sql); // result set
  $total_recs = mysqli_num_rows($rs);
  $nav_links = '';

  $a_postcodes = array();
  $a_projects = array();
  $i = 0;

  if ($total_recs) {
    $total_recs = 0; // will be amount of search criteria matched (but all available on map) 
    while ($row = mysqli_fetch_assoc($rs)) {

      $row['postcode'] = strtoupper(str_replace(' ', '', $row['postcode']));

      $test_postcode = strtoupper(str_replace(' ', '', $_GET['road_name']));

      $a_projects[$i] = $row;
  
      if ($_GET['road_name'] || $is_filter) {
        if ($_GET['road_name']) {
          if (strpos(strtolower($row['title']), strtolower($_GET['road_name']))===0 || $row['postcode'] == $test_postcode) {
            $total_recs++;
            $a_projects[$i]['match'] = true;
          }
        }
        else { // $is_filter

          if (  (($_GET['property'] && $row['property']==$_GET['property']) || !$_GET['property'] )  

             && ( ($_GET['borough'] && $row['borough']==$_GET['borough']) || !$_GET['borough'] )

             && ( ($_GET['roof'] && $row['roof']==$_GET['roof']) || !$_GET['roof'] )

             && (($floor && (
               ($floor[0] && $floor[1] && $row['floor'] > $floor[0] && $row['floor'] <= $floor[1])
               || 
               (!$floor[0] && $floor[1] && $row['floor'] <= $floor[1])
               || 
               ($floor[0] && !$floor[1] && $row['floor'] > $floor[0])
             )) || !$floor)

            ) {
            $total_recs++;
            $a_projects[$i]['match'] = true;

          }

        }
      }
      else {
        $total_recs++;
        $a_projects[$i]['match'] = true;
      }
      
      $a_postcodes[] = $row['postcode'];

      $i++;
    }

    // use the trick: when postcode not found, try this:
    if (!$total_recs && $_GET['road_name']) {
      $test_postcode = strtoupper(str_replace(' ', '', $_GET['road_name']));

      do {
        $test_postcode = substr($test_postcode, 0, -1);

        foreach ($a_projects AS $k => $row) {
          if ($row['postcode'] && $test_postcode && strpos($row['postcode'], $test_postcode)===0) {
            $total_recs++;
            $a_projects[$k]['match'] = true;
          }
        }

      } while (!$total_recs && $test_postcode);

    }

    //echo "Projects:" . count($a_projects)."<br />";


    //echo $test_postcode.'!!!';

    //mysql_data_seek($rs, 0);#not required

    // get latitude, longitude

    global $dbhost, $dbuser, $dbpass, $dbname, $dbname_uk_postcode, $dbconn;
    //echo $dbname_uk_postcode.'!!!';#debug
    if (!mysql_select_db($dbname_uk_postcode, $dbconn)) die("Can't select the ".$dbname_uk_postcode.' database!');

    $sql = "SELECT postcode, longitude, latitude FROM uk_postcode WHERE postcode IN('" . implode("','", $a_postcodes) . "')";
    $rs = mysqli_query($dbconn,$sql);
    while ($row = mysqli_fetch_assoc($rs)) {
      foreach ($a_projects AS $k => $v) {
        if ($v['postcode']==$row['postcode']) {
          $a_projects[$k]['latitude'] = $row['latitude'];
          $a_projects[$k]['longitude'] = $row['longitude'];
        }
      }
    }

    //echo '<pre>!!!'.print_r($a_projects, 1).'<pre>';#debug


    $center_lat = 0;
    $center_lng = 0;

    // fix $total_recs (to make sure all have $row['latitude'] to be visible on map)
    $total_recs = 0; // reset

    foreach ($a_projects AS $k => $row) {

      if (!isset($row['latitude'])) continue;

      if ($_GET['road_name'] || $is_filter) {

        if ($_GET['road_name']) {

          if ($row['title'] && $test_postcode && ((isset($row['match']) && $row['match']) || strpos($row['postcode'], $test_postcode)===0)) {
            $center_lat += $row['latitude'];
            $center_lng += $row['longitude'];
            $a_projects[$k]['match'] = true;
            $total_recs++;
          }

        }
        else {

          if (isset($row['match']) && $row['match']) {

            $center_lat += $row['latitude'];
            $center_lng += $row['longitude'];
            $total_recs++;

          }


        }

      }
      else { // show all


          $center_lat += $row['latitude'];
          $center_lng += $row['longitude'];
          $total_recs++;


      }

    } // end foreach

  } // endif $total_recs


  // get geo center of all points
  if ($total_recs) {
     $center_lat = $center_lat / $total_recs;
     $center_lng = $center_lng / $total_recs;
     //echo $center_lat . ' ::: ' .  $center_lng;#debug
  }
  
  // get more points near by
  if ($_GET['road_name'] && $total_recs) {
    $center_lat = $center_lat / $total_recs;
    $center_lng = $center_lng / $total_recs;
    
    $miles2km = 1.60934; // miles to kilomeeters
    $miles = 1; // 0.5, 1, 2 : default distance
    $kmRange = $miles * $miles2km;

    foreach ($a_projects AS $k => $row) {

      if (!isset($row['latitude'])) continue;
      if (!isset($row['longitude'])) continue;

      $lat = $row['latitude'];
      $lng = $row['longitude'];

      $distance = (
        3959 * acos (
          cos (  deg2rad ($center_lat) )
          * cos(  deg2rad ( $lat ) )
          * cos(  deg2rad ( $lng ) -  deg2rad ($center_lng) )
          + sin (  deg2rad ($center_lat) )
          * sin(  deg2rad ( $lat ) )
        )
      );

      if ($distance < $kmRange && !(isset($row['match']) && $row['match'])) {
        //echo $distance .'!!!<br/>';#debug
        $a_projects[$k]['match'] = true;
        $total_recs++;
      }

    }
  

  }


  if ($total_recs) {

    // map view :

    // prepare records for map view

    $a_map = array();

    foreach ($a_projects AS $k => $v) {

      if (!isset($v['latitude'])) {
        echo '<!-- error postcode: '.print_r($v, 1).' (no lat, lng) -->';
        continue;
      }

      $key = $v['latitude'].' '.$v['longitude']; // unique key

      if (!isset($a_map[$key])) {

        $a_map[$key] = array(
          'postcode' => array(),
          'lat' => $v['latitude'],
          'lng' => $v['longitude'],
          'match' => isset($v['match'])?true:false,
          'title' => array(), 
          'property' => array(), 
          //'project_type' => array(), // not used per now
          'roof' => array(), 
          'floor' => array(), 
          'pdffile' => array(),
          'date_created' => array(),
        );
      }

      $a_map[ $key ]['postcode'][] = $v['postcode'];
      $a_map[ $key ]['title'][] = $v['title'];
      $a_map[ $key ]['property'][] = $v['property'];
      //$a_map[ $key ]['project_type'][] = $v['project_type'];
      $a_map[ $key ]['roof'][] = $v['roof'];
      $a_map[ $key ]['floor'][] = $v['floor'];
      $a_map[ $key ]['pdffile'][] = $v['pdffile'];
      $a_map[ $key ]['date_created'][] = $v['date_created'];

    }

    $a_map = array_values($a_map);

    $a_map = json_encode($a_map);


    // endif map view
    
    // matrix view:
    
////////////////////////////////////////////////////
    
    $i = 0;
    $pdf_exists = false;
    $matrix_view = '';
    foreach ($a_projects AS $row) {

      if (!isset($row['latitude'])) continue;
      if (!isset($row['longitude'])) continue;


      if ($_GET['road_name'] || $is_filter) {

        if ($_GET['road_name']) {

          if (isset($row['match']) && $row['match']) {

            $i++;

            //echo '<div class="item"><pre>'.print_r($row,1).'</pre></div>';#debug
            $basename = $row['pdffile'];
            $basename = explode('.', $basename);
            array_pop($basename);
            $basename = implode('.', $basename);

            if (file_exists(WISE_PATH.'pdf/design-planning/'.$row['pdffile'])) $pdf_exists = true;
            else $pdf_exists = false;

            $matrix_view .= '<div class="col_one_third'.(!($i%3)?' col_last':'').' item"><a href="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design-planning/'.$row['pdffile']:'http://buildteam.com/pdf/design-planning/'.$row['pdffile']).'" target="_blank"><img src="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design-planning/thumb/'.$basename.'.png?v='.$row['date_created']:'http://buildteam.com/pdf/design-planning/thumb/'.$basename.'.png?v='.$row['date_created']).'" alt="'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'" /><br/><div class="title">'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'</div></a></div>'.(!($i%3)?' <br clear="all" />':'');

          }

        }
        else {


          if (isset($row['match']) && $row['match']) {

            $i++;

            //echo $i.'!!!!!!!!!!!!!!!!!<br/>';#debug

            //echo '<div class="item"><pre>'.print_r($row,1).'</pre></div>';#debug
            $basename = $row['pdffile'];
            $basename = explode('.', $basename);
            array_pop($basename);
            $basename = implode('.', $basename);

            if (file_exists(WISE_PATH.'pdf/design-planning/'.$row['pdffile'])) $pdf_exists = true;
            else $pdf_exists = false;
            
            $matrix_view .= '<div class="col_one_third'.(!($i%3)?' col_last':'').' item"><a href="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design-planning/'.$row['pdffile']:'http://buildteam.com/pdf/design-planning/'.$row['pdffile']).'" target="_blank"><img src="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design-planning/thumb/'.$basename.'.png?v='.$row['date_created']:'http://buildteam.com/pdf/design-planning/thumb/'.$basename.'.png?v='.$row['date_created']).'" alt="'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'" /><br/><div class="title">'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'</div></a></div>'.(!($i%3)?' <br clear="all" />':'');
            

          }


        }

      }
      else {


        $i++;

        //echo '<div class="item"><pre>'.print_r($row,1).'</pre></div>';#debug
        $basename = $row['pdffile'];
        $basename = explode('.', $basename);
        array_pop($basename);
        $basename = implode('.', $basename);

        if (file_exists(WISE_PATH.'pdf/design-planning/'.$row['pdffile'])) $pdf_exists = true;
        else $pdf_exists = false;

        $matrix_view .= '<div class="col_one_third'.(!($i%3)?' col_last':'').' item"><a href="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design-planning/'.$row['pdffile']:'http://buildteam.com/pdf/design-planning/'.$row['pdffile']).'" target="_blank"><img src="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design-planning/thumb/'.$basename.'.png?v='.$row['date_created']:'http://buildteam.com/pdf/design-planning/thumb/'.$basename.'.png?v='.$row['date_created']).'" alt="'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'" /><br/><div class="title">'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'</div></a></div>'.(!($i%3)?' <br clear="all" />':'');

      }

    }
    
    

    // endif matrix view (gotten by filter) //////////////////////////////

  } // endif $total_recs (means search criteria found)  

  
  // Template (view) part:
?>
<div class="main">  	
	<div class="full">
		<section class="module content parallax-3">
			<div class="wcu-container">
				<div id="sec-why-choose-us">
					<div class='wcu-content inner-0 active'><h2>Why Choose Us</h2><p>Uptat. Iduscil iquaspedio omnimen imaiost asFernam laborat escitatur, sum enis aut opta nonseque consequiam si necto officto taspere peris desciuquconsecerion enitisuscil iquasped escitatur, sum enis aut opta nonseque consequiam si necto offictoiokkkfk</p></div>
					<div class='wcu-content inner-1 '>
						<div class='guarantanee'></div><h2>We Guarantee Our Work</h2><p>We are so confident in the quality of our work, we offer a 10 year guarantee. We also welcome our prospective clients the opportunity to meet our Project Management Team and visit an on-going Build prior to committing to the Build Phase.</p>
					</div>
					<div class='wcu-content inner-2 '><div class='value-for-money'></div><h2>Value For Money</h2><p>Space is valuable, particularly in London, so all of our Designs are developed with this in mind. We work with you to ensure we create the best design for you, all in-line with your budget.</p></div>
					<div class='wcu-content inner-3 '><div class='house-tours'></div><h2>House Tours</h2><p>Our House Tours not only offer a fantastic opportunity to check out our quality of work, but they also help our Design clients visualise options before committing to their own design. We hold a new House Tour every couple of months, and you can come to as many as you would like.</div>
					<div class='wcu-content inner-4 '><div class='unparalled'></div><h2>Unparalleled Support</h2><p>Building works can be stressful, as we’re talking about a project which might disrupt your day to day routine for a couple of months. We are here every step of the way, and will explain each step so you have a good idea of what to expect, when.</p></div>
					<div class='wcu-content inner-5 '><div class='experienced'></div><h2>Experience</h2><p>Uptat. Iduscil iquaspedio omnimen imaiost asFernam laborat escitatur, sum enis aut opta nonseque consequiam si necto officto taspere peris desciuquconsecerion enitisuscil iquasped escitatur, sum enis aut opta nonseque consequiam si necto offictoiokkkfk</p></div>
					<div class='wcu-content inner-6 '><div class='customer-service'></div><h2>Customer Service</h2><p>We are thrilled to work in an industry which requires close communication from our clients, and we develop fantastic relationships as a result. You receive a dedicated support for both the Design & Build Phase and our expert team are always on hand to answer questions and guide you through the process.</p></div>
					<div class='wcu-content inner-7 '><div class='customer-loyality'></div><h2>Customer Loyalty</h2><p>Lots of our prospective clients find us through recommendations, and we feel it’s important to recognise customer loyalty which we do via our Refer a Friend program. If you recommend us, we give you a little something to say thank you! For more details, contact our Team.</p></div>
					<div class='wcu-content inner-8 '><div class='one-stop'></div><h2>One Stop Shop</h2><p>We offer a two-stage process (Design & Build), so you maintain flexibility throughout the entire project. Every client is different, so we also have a selection of bolton services available to ensure we can handle everything you might need, if you want us to so.</p></div>
					<div class='wcu-content inner-9 '><div class='flexible'></div><h2>Flexible And Hassel Free</h2><p>The Design & Build process is typically new to our clients, so we understand that it is our job to ensure the project runs smoothly. We handle all of the hassle and simply keep you informed on the developments. You can be as involved as you would like to be, it’s completely up to you.</p></div>
				</div>
			</div>
			<div class="wcu-mobile-view">
				<div class="m-wcu-container">
					<div class="m-wcu-sec m-wcu-sec0 active">
						<h2>Why Choose Us</h2>
						<p>Uptat. Iduscil iquaspedio omnimen imaiost as Fernam laborat escitatur, sum enis aut opta nonseque consequiam si necto officto taspere peris desciuquconsecerion enitisuscil iquasped escitatur, sum enis aut optahdk</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec1">
						<img src='buildIcons/6.png' />
						<h3>Customer Service</h3>
						<p>We are thrilled to work in an industry which requires close communication from our clients, and we develop fantastic relationships as a result. You receive a dedicated support for both the Design & Build Phase and our expert team are always on hand to answer questions and guide you through the process.</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec2">
						<img src='buildIcons/7.png' />
						<h3>Customer Loyalty</h3>
						<p>Lots of our prospective clients find us through recommendations, and we feel it’s important to recognise customer loyalty which we do via</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec2">
						<img src='buildIcons/8.png' />
						<h3>One Stop Shop</h3>
						<p>We offer a two-stage process (Design & Build), so you maintain flexibility throughout the entire project. Every client is different, so we also have a selection of bolton services available to ensure we can handle everything you might need, if you want us to so.</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec2">
						<img src='buildIcons/9.png' />
						<h3>Flexible And Hassel Free</h3>
						<p>The Design & Build process is typically new to our clients, so we understand that it is our job to ensure the project runs smoothly. We handle all of the hassle and simply keep you informed on the developments. You can be as involved as you would like to be, it’s completely up to you.</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec2">
						<img src='buildIcons/1.png' />
						<h3>We Guarantee Our Work</h3>
						<p>We are so confident in the quality of our work, we offer a 10 year guarantee. We also welcome our prospective clients the opportunity to meet our Project Management Team and visit an on-going Build prior to committing to the Build Phase.</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec2">
						<img src='buildIcons/2.png' />
						<h3>Value For Money</h3>
						<p>Space is valuable, particularly in London, so all of our Designs are developed with this in mind. We work with you to ensure we create the best design for you, all in-line with your budget.</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec2">
						<img src='buildIcons/3.png' />
						<h3>House Tours</h3>
						<p>Our House Tours not only offer a fantastic opportunity to check out our quality of work, but they also help our Design clients visualise options before committing to their own design. We hold a new House Tour every couple of months, and you can come to as many as you would like.</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec2">
						<img src='buildIcons/4.png' />
						<h3>Unparalleled Support</h3>
						<p>Building works can be stressful, as we’re talking about a project which might disrupt your day to day routine for a couple of months. We are here every step of the way, and will explain each step so you have a good idea of what to expect, when.</p>
					</div>
					<div class="m-wcu-sec m-wcu-sec2">
						<img src='buildIcons/5.png' />
						<h3>Experience</h3>
						<p>Uptat. Iduscil iquaspedio omnimen imaiost asFernam laborat escitatur, sum enis aut opta nonseque consequiam si necto officto taspere peris desciuquconsecerion enitisuscil iquasped escitatur, sum enis aut opta nonseque consequiam si necto offictoiokkkfk</p>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>
<script type="text/javascript">
	var documentWidth = window.innerWidth;
	var dataJson = {
		options: [
			{
				"html": "<div class='option-content' data-attr='inner-1'><img src='buildIcons/1.png' /><h2>We guarantee our work</h2></div>"
			},
			{
				"html": "<div class='option-content' data-attr='inner-2'><img src='buildIcons/2.png' /><h2>value for money</h2></div>"
			},
			{
				"html": "<div class='option-content' data-attr='inner-3'><img src='buildIcons/3.png' /><h2>house tours</h2></div>"
			},
			{
				"html": "<div class='option-content' data-attr='inner-4'><img src='buildIcons/4.png' /><h2>unparalleled support</h2></div>"
			},
			{
				"html": "<div class='option-content' data-attr='inner-5'><img src='buildIcons/5.png' /><h2>Experience</h2></div>"
			},
			{
				"html": "<div class='option-content' data-attr='inner-6'><img src='buildIcons/6.png' /><h2>customer service</h2></div>"
			},
			{
				"html": "<div class='option-content' data-attr='inner-7'><img src='buildIcons/7.png' /><h2>customer loyalty</h2></div>"
			},
			{
				"html": "<div class='option-content' data-attr='inner-8'><img src='buildIcons/8.png' /><h2>one stop shop</h2></div>"
			},
			{
				"html": "<div class='option-content' data-attr='inner-9'><img src='buildIcons/9.png' /><h2>flexible and hassel free</h2></div>"
			}
		]
	}

	var div = 360 / 9;
	var radius = (documentWidth > 1024)? 320 : 360; // set this value to offset outer cirles from main circle
	var parentdiv = document.getElementById('sec-why-choose-us');
	var offsetToParentCenter = parseInt(parentdiv.offsetWidth / 2);  //assumes parent is square
	var offsetToChildCenter = (documentWidth > 1024)? 82: 95; // this offset to make outer circles at center point of main circle.
	var totalOffset = offsetToParentCenter - offsetToChildCenter;
	var circleBar = [];
	for (var i = 1; i <= 9; ++i)
	{
	   var childdiv = document.createElement('div');
	   childdiv.id = "wcu-option-"+i;
	   childdiv.className = 'div2 circleHover';
	   childdiv.style.position = 'absolute';
	   var y = Math.sin((div * i) * (Math.PI / 180)) * radius;
	   var x = Math.cos((div * i) * (Math.PI / 180)) * radius;
	   childdiv.style.top = (y + totalOffset).toString() + "px";
	   childdiv.style.left = (x + totalOffset).toString() + "px";
	   $(childdiv).append(dataJson.options[i-1].html);
	   $(parentdiv).append(childdiv);
	   
	   //parentdiv.appendChild(childdiv);
	   circleBar["wcu-option-"+i] = new ProgressBar.Circle("#wcu-option-"+i, {
		  strokeWidth: 3,
		  easing: 'easeInOut',
		  duration: 1400,
		  color: '#0f4778',
		  trailColor: '#eee',
		  trailWidth: 0.1,
		  svgStyle: null
		});
		//$(this).animate(1.0)
	}
	$('.circleHover').hover(function() {
		var id = $(this)[0].id;
		circleBar[id].animate(1.0);
	}, function() {
		var id = $(this)[0].id;
		circleBar[id].animate(0);
	});
	$(document).ready(function() {
		$('.option-content').hover(
			function(){
				var curElem = $(this).attr('data-attr');
				$('.wcu-content').removeClass("active");
				$('.'+curElem).addClass("active");
			}, function(){
				var curElem = $(this).attr('data-attr');
				$('.'+curElem).removeClass("active");
				$($('#sec-why-choose-us .wcu-content')[0]).addClass("active");
			}
		);
	});
</script>

<?php if ($total_recs) : // $_GET['road_name'] &&  ?>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&amp;sensor=false"></script>
<script src="/js/infobox.js"></script>

<script type="text/javascript">
    var map;
    var center_lat = <?php echo $center_lat ?>;
    var center_lng = <?php echo $center_lng ?>;
    
    var a_map = <?php echo $a_map ?>;
    
    var map_state = {
      init: false,
      extended: false,
      zoom: 0
    };
    
    //alert('Hi! '+ center_lat + " " + center_lng);//debug
  
    function initialize() {
      var centerLatLng = new google.maps.LatLng( center_lat, center_lng );

      var bounds = new google.maps.LatLngBounds();

      var mapOptions = {
        //disableDefaultUI: false,
        scrollwheel: false,
        zoomControl: true,
        /*minZoom: 8,*/
        maxZoom: 14, // 12
        zoom: 9, /*10*/
        center: centerLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      var blue_icon = { 
        url: '/images/design_planning/blue_pin1.png',
      };

      
      if (a_map) {
        var a_marker_map = [];
        var a_marker_pan = [];
        
        var position = {};

        //var infoWindow = new google.maps.InfoWindow(), i, position;

        var infoWindow = new InfoBox({
                content: "",
                map: map,
                disableAutoPan: false,
                maxWidth: 0, // 150
                alignBottom: true,
                pixelOffset: new google.maps.Size(-150, -65),
                zIndex: null,
                boxClass: "infoBox",
                closeBoxURL: '/images/design_planning/map_info_close.gif',
                closeBoxMargin: "3px", // "12px 4px 2px 2px",
                pane: "floatPane",
                enableEventPropagation: false,
                infoBoxClearance: "10px" // new google.maps.Size(1, 1) new google.maps.Size(1, 1)
        });


        for (i=0; i < a_map.length; i++) {
          position = new google.maps.LatLng( a_map[i].lat, a_map[i].lng );
          
          if (a_map[i].match) { // show matched at first and other by map drag or zoom event
            bounds.extend(position);

            a_marker_map[i] = new google.maps.Marker({
              position: position,
              map: map,
              icon: blue_icon,
              title: a_map[i].title.join("; ")
            });
            
            // Allow each marker to have an info window

            google.maps.event.addListener(a_marker_map[i], 'click', (function(marker, i) {
                return function() {
                    var buf = '';
                    var basename = "";
                    for (var j=0;j<a_map[i].title.length;j++) {
                      basename = a_map[i].pdffile[j];
                      basename = basename.split('.');
                      basename.pop();
                      basename = basename.join('.');

                      buf += '<div class="dt-info">';

                      buf += '<img src="/pdf/design-planning/thumb/' + basename + '.png?v=' + a_map[i].date_created[j] + '" />';
                      buf += '<h3>' + a_map[i].title[j] + '</h3>';
                      buf += (a_map[i].property[j]=='f'?'Flat':'House') + ', ' + a_map[i].postcode[j] + '<br/>';
                      buf += (a_map[i].roof[j]=='f'?'Flat Roof':(a_map[i].roof[j]=='sv'?'Slate &amp; Velux Roof':'All Glass Roof')) + '<br/>';
                      buf += 'Floor area: ' + a_map[i].floor[j] + ' m<sup>2</sup><br/>';
                      buf += '<a href="/pdf/design-planning/' + a_map[i].pdffile[j] + '" target="_blank">See PDF drawing</a>';
                      buf += '</div>';
                    }

                    infoWindow.setContent(buf);
                    infoWindow.open(map, marker);
                }
            })(a_marker_map[i], i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
          } // endif a_map[i].map
        } // end for
        
        /*google.maps.event.addListener(map, 'dragend', function() { 
          if (map_state.init && !map_state.extended) {
            extendMap();
          }
        });*/
        google.maps.event.addListener(map, 'zoom_changed', function() {
          if (map_state.init && !map_state.extended && map_state.zoom && map.getZoom() < map_state.zoom) {
            extendMap();
          }
        });

        
      } // endif a_map
      
  } // end initialize() function
  
  function extendMap() {
    
      var blue_icon = { 
        url: '/images/design_planning/blue_pin1.png',
      };
      
      if (a_map) {
        var a_marker_map = [];
        var a_marker_pan = [];
        
        var position = {};

        //var infoWindow = new google.maps.InfoWindow(), i, position;

        var infoWindow = new InfoBox({
                content: "",
                map: map,
                disableAutoPan: false,
                maxWidth: 0, // 150
                alignBottom: true,
                pixelOffset: new google.maps.Size(-150, -65),
                zIndex: null,
                boxClass: "infoBox",
                closeBoxURL: '/images/design_planning/map_info_close.gif',
                closeBoxMargin: "3px", // "12px 4px 2px 2px",
                pane: "floatPane",
                enableEventPropagation: false,
                infoBoxClearance: "10px" // new google.maps.Size(1, 1) new google.maps.Size(1, 1)
        });


        for (i=0; i < a_map.length; i++) {
          position = new google.maps.LatLng( a_map[i].lat, a_map[i].lng );
          
          if (!a_map[i].match) { // show other by map drag or zoom event
            //bounds.extend(position); // without extending

            a_marker_map[i] = new google.maps.Marker({
              position: position,
              map: map,
              icon: blue_icon,
              title: a_map[i].title.join("; ")
            });

            // Allow each marker to have an info window

            google.maps.event.addListener(a_marker_map[i], 'click', (function(marker, i) {
                return function() {
                    var buf = '';
                    var basename = "";
                    for (var j=0;j<a_map[i].title.length;j++) {
                      basename = a_map[i].pdffile[j];
                      basename = basename.split('.');
                      basename.pop();
                      basename = basename.join('.');

                      buf += '<div class="dt-info">';

                      buf += '<img src="/pdf/design-planning/thumb/' + basename + '.png?v=' + a_map[i].date_created[j] + '" />';
                      buf += '<h3>' + a_map[i].title[j] + '</h3>';
                      buf += (a_map[i].property[j]=='f'?'Flat':'House') + ', ' + a_map[i].postcode + '<br/>';
                      buf += (a_map[i].roof[j]=='f'?'Flat Roof':(a_map[i].roof[j]=='sv'?'Slate &amp; Velux Roof':'All Glass Roof')) + '<br/>';
                      buf += 'Floor area: ' + a_map[i].floor[j] + ' m<sup>2</sup><br/>';
                      buf += '<a href="/pdf/design-planning/' + a_map[i].pdffile[j] + '" target="_blank">See PDF drawing</a>';
                      buf += '</div>';
                    }

                    infoWindow.setContent(buf);
                    infoWindow.open(map, marker);
                }
            })(a_marker_map[i], i));

            // not center the map fitting all markers on the screen
            //map.fitBounds(bounds);
            
          } // endif a_map[i].map
        } // end for
    
      } // indif a_map
      
      map_state.extended = true; // one time extend
        
  } // end extendMap() function
  
  if (center_lat && center_lng) {
    google.maps.event.addDomListener(window, 'load', initialize);
    
    window.setTimeout(function() {
      map_state.init = true;
      map_state.zoom = map.getZoom();
    }, 1000);
    
  }
  
</script>
<?php endif; ?>


<script type="text/javascript">
  var road_name_placeholder = "road name or postcode";

  jQuery(document).ready(function(){
    //jQuery('.jClever').jClever(); 
  
    $("#road_name").blur(function(){
      if (''==this.value) {
        this.value=road_name_placeholder;
        this.style.fontStyle = "italic";
      }
    });
    
    $("#road_name").focus(function(){
      if (road_name_placeholder==this.value) {
        this.value='';
        this.style.fontStyle = "normal";
      }
    });
   
    $(".nav-tabs li a").click(function(e){
      var id = this.href + "";
      id = id.split("#");
      id = id[1];
      
      $(".nav-tabs li").removeClass("active");
      $(this).parent().addClass("active");
      
      $(".tab-pane").removeClass("active");
      $("#"+id).addClass("active");
      
      e.preventDefault();
    });
    
  });
  
  function fixPlaceholder() {
    if (road_name_placeholder==document.getElementById("road_name").value) {
      document.getElementById("road_name").value = "";
    }
  }
</script>
<script>
$(".closethis").click(function(){
	$(".bookthanks").fadeOut(200);
	});
</script>
<?php
  require_once('./inc/footer.inc.php');
?>