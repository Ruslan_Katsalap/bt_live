<?php

require_once('./inc/util.inc.php');

$c = $_GET['c'];
$name = '';
$title = '';
$photo_lg = '';
$description = '';
	
$rs = getRs("SELECT * FROM team WHERE is_active = 1 AND team_code = '{$c}'");

while ( $row = mysqli_fetch_assoc($rs) ) {
	$name = $row['name'];
	$title = $row['title'];
	$photo_lg = $row['photo_lg'];
	$description = $row['description'];
}
?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Build Team &bull; Dan Davidson</title>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<script type="text/javascript" src="js/jquery-1.2.6.min.js"></script>
<link rel="stylesheet" type="text/css" href="shadowbox/shadowbox.css" />
<script type="text/javascript" src="shadowbox/shadowbox.js"></script>
<script type="text/javascript"> 
Shadowbox.init({    language: 'en',    players:  ['iframe']});
</script>
</head>
<body class="team">

<table>
	<tr>
  	<th><h1><?php echo $name; ?> - <?php echo $title; ?></h1>
		<?php echo nl2br($description); ?>
    </th>
    <td><img src="team/<?php echo $photo_lg; ?>" width="253" height="376" alt="<?php echo $name; ?>" /></td>
  </tr>
</table>

</body>
</html>