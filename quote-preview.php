<?php

require_once ('./inc/util.inc.php');

if (isset($_SESSION['userid']) && $_SESSION['userid']) {
  if (isset($_GET['id']) && ($_GET['id']=(int)$_GET['id'])) {
    
    $sql = "SELECT * FROM rfq WHERE rfq_id={$_GET['id']} AND userid=".(int)$_SESSION['userid'];
    $rs = getRs($sql);
    if (mysqli_num_rows($rs)) {
      
      $qid = $_GET['id'];
      
      $a_rfq = mysqli_fetch_assoc($rs);
      
      $is_pdf = true;
      if (isset($_GET['html'])) $is_pdf = false;
      
      if ($is_pdf) {
        ob_start();
      }
      
      include ('./email_template.php');
      
      if ($is_pdf) {
        $buf = ob_get_contents();
        ob_end_clean();
        
        $buf = str_replace('src="/images/', 'src="images/', $buf);
        $buf = str_replace('src="/byp/', 'src="byp/', $buf);
        
        include_once("./dompdf/dompdf_config.inc.php");
        
        $dompdf = new DOMPDF();
        $dompdf->load_html($buf);
        $dompdf->set_paper('letter', 'portrait');
        $dompdf->render();

        $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));
      }
      
      //echo 'Found!)';#debug
    
      exit;
    }
  }
}

header('Location: saved_quote.html');
exit;

?>