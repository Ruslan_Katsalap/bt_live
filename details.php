<html>
<head>
<title>Confirmation Details</title>


	<link rel="stylesheet" media="all" href="css/new_calculator.css">
	<link rel="stylesheet" media="all" href="css/validationEngine.css">
	<link rel="stylesheet" media="all" href="css/detail-style.css">
	<link rel="stylesheet" href="css/stylesheet-pure-css.css">
	<link rel="stylesheet" href="css/tooltip.css">
	<link rel="stylesheet" media="screen and (max-width: 722px)" href="css/flexslider.css">
	
	
</head>

<body>
	
	<div class="main1" style="border: 1px black solid;">
		<div style="padding: 10px;">
			<div style="padding: 12px;">
				<img src="logo1.png" style="height: 100px; width: 100px;">
				<img src="buildteam.jpg" style="height: 70px; width: 200px; float: right;">				
			</div>				
		</div>
		
		<div style="text-align: right;
	font-family: arial;
	font-style: bold;
	font-size: 12px;
	background-color: #F8A865;
	color: white;
	padding: 6px;
	padding-right: 8px !important;">
			QUOTE REF : HJ/361/SW4
		</div>
		<div style="font-size: 12px;
	font-family: arial;
	line-height: 20px;
	padding: 12px;
	font-weight: bold;">
			Dear Documentry,<br>

			Thank you for using our online price calculator. I have pleasure in confirming your quotation based on your individual 

			requirements. I hope the information provided is useful, and please do not hesitate to contact me should you wish to arrange a 

			FREE, no obligation visit to your property.
		</div>
		<div class="stg">
		STAGE 0 : DESIGN & STRUCTURAL ENGINEERING PHASE		
			<span style="float: right;">COST : 2,995.00</span>
		</div>
		<div class="txt">
			THIS STAGE INCLUDES:<br>

			Measured Survey<br>

			Production of Design Options<br>

			Planning Application<br>

			Structural Engineers Assessment<br>

			Detailed Schedule of Works, line-priced by Build Team<br>

			Party Wall Matters (Additional Fee)<br>
		</div>
		<div class="stg">
			STAGE 1 : CONSTRUCTION OF CORE SHELL
		</div>
		<div class="txt">
			YOU SPECIFIED:<br>

			25.00 sqm room<br>

			Side Wall to be removed<br>
		</div>
		<div class="stg">
			STAGE 1 : EXTERIOR FIT OUT
		</div>
		<div class="txt">
			YOU SPECIFIED:<br>

			All Glass<br>

			Folding/Sliding Doors<br>
		</div>
		<div class="stg">
			STAGE 3 : INTERIOR FIT OUT
		</div>
		<div class="txt">
			YOU SPECIFIED: <br>

			Do not remove Chimney Breast<br>

			Standard Radiators<br>

			Ground Floor WC is not required<br>
		</div>
		<div class="stg">
			TOTAL CONDTRUCTION PHASE (Quote valid for 60 days)
			<span style="float: right;">COST : 35,862.50</span>
		</div>
		<div class="total">
			Subject to site survey and relevant permissions. Excludes VAT
		</div>
		
	</div>
	
	
	<div style="width: 60%;
	margin: 0 auto;">
									
									<div style="padding: 10px;">
										<div style="padding: 12px;">
											<img src="logo1.png" style="height: 100px; width: 100px;">
											<img src="buildteam.jpg" style="height: 70px; width: 200px; float: right;">				
										</div>				
									</div>
									<div style="text-align: right;
											font-family: arial;
											font-style: bold;
											font-size: 12px;
											background-color: #F8A865;
											color: white;
											padding: 6px;
											padding-right: 8px !important;">
										QUOTE REF : HJ/361/SW4
									</div>
									<div style="font-size: 12px; font-family: arial; line-height: 20px; padding: 12px; font-weight: bold;"> 
														<h2>Hi</h2>
						Build Team has received BYP Calculator Quote from a new user, Please check below mentioned details:
									</div>
						
						<table>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Postcode</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">postcode</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Live IN</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">livin</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">House Type</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">house</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Extension Type</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">extension</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Full Length</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">fulllength</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Depth</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">depth</td>
										
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Roof Type</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['roof_type'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Type of Sky Light</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['skylight'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Type of Doors</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['door_type'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Remove Chimney Breast</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$remove_chimney.'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Underfloor Heating</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$heating.'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Ad WC</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$adwc.'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">First Name</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['fname'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Surname</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['surname'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">House/Flat Number, Street</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['flat_num'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Contact Number</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['con_number'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Address</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['address'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Post Code</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['post_code'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Comments</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['comments'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Want to Start Design</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['start_design'].'</td>
							</tr>
							<tr>
								<td style="background-color: gray;	
											padding: 6px;
											padding-left: 10px !important;
											padding-right: 10px !important;
											margin-top: 2px;
											color: white;
											font-family: arial;
											font-size: 12px;
											letter-spacing: 1px;">Current Status</td>
								<td style="font-size: 12px;
										font-family: arial;
										line-height: 20px;
										padding: 12px;
										font-weight: bold;">'.$_REQUEST['current_status'].'</td>
							</tr>
						</table>
					</div>
</body>
</html>