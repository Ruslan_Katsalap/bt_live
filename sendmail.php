<?php

if (!isset($qid)) die("Access denied!");

ob_start();
include ('./email_template.php');
$ret = ob_get_contents();
ob_end_clean();

$ret = str_replace('src="'.$cgi_url.'"', 'src="cid:cgi"', $ret);

$ret = str_replace('src="/images/byp/byp_logo_email.png"', 'src="cid:byp_logo_email"', $ret);

$ret = str_replace('src="/images/byp/builteam_logo_email.png"', 'src="cid:builteam_logo_email"', $ret);

$ret = str_replace('src="/'.$url_images.$subcategory_1_component_image.'"', 'src="cid:subcategory_1_component"', $ret);
$ret = str_replace('src="/'.$url_images.$subcategory_2_component_image.'"', 'src="cid:subcategory_2_component"', $ret);
$ret = str_replace('src="/'.$url_images.$subcategory_3_component_image.'"', 'src="cid:subcategory_3_component"', $ret);
$ret = str_replace('src="/'.$url_images.$subcategory_4_component_image.'"', 'src="cid:subcategory_4_component"', $ret);
$ret = str_replace('src="/'.$url_images.$subcategory_5_component_image.'"', 'src="cid:subcategory_5_component"', $ret);
$ret = str_replace('src="/'.$url_images.$subcategory_6_component_image.'"', 'src="cid:subcategory_6_component"', $ret);

$ret = str_replace('src="/images/byp/dan_sign_email.png"', 'src="cid:dan_sign_email"', $ret);

$ret = str_replace('src="/images/byp/this_cgi_email.png"', 'src="cid:this_cgi_email"', $ret);

$ret = str_replace('url(/images/byp/bg_hline1_email.png)', 'url(cid:bg_hline1_email)', $ret);
$ret = str_replace('url(/images/byp/bg_hline2_email.png)', 'url(cid:bg_hline2_email)', $ret);

// Mask for Outlook
$ret = str_replace('src="/images/byp/bg_hline1_email.png"', 'src="cid:bg_hline1_email"', $ret);
$ret = str_replace('src="/images/byp/bg_hline2_email.png"', 'src="cid:bg_hline2_email"', $ret);

$ret = str_replace('background="/images/byp/bg_hline1_email.png"', 'background="cid:bg_hline1_email"', $ret);
$ret = str_replace('background="/images/byp/bg_hline2_email.png"', 'background="cid:bg_hline2_email"', $ret);

require_once ('./phpmailer/class.phpmailer.php');

$email_subject = "Build Team: BuildYourPrice Quote Ref: {$rfq_code}";

$mail = new PHPMailer();

$mail->IsSMTP();
$mail->SMTPDebug = 0;
$mail->SMTPAuth = true;
$mail->SMTPSecure = 'TLS';
$mail->Host = "smtp.sendgrid.net";
$mail->Port = 587;
//$mail->IsHTML(true);
$mail->Charset = 'UTF-8';
$mail->Username = "apikey";
$mail->Password = "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";

$mail->From = $admin_email;

$mail->FromName = 'Build Team';

$mail->AddAddress($email, $name);

if ($is_notify_admin) {
  //debug, delete after testing:
  /*if ('dev.buildteam.com'==$_SERVER['HTTP_HOST']) $mail->AddBCC('dmbiko@gmail.com');
  else*/ $mail->AddBCC($admin_email);
}


//$mail->AddReplyTo($email,$name);


$mail->AddEmbeddedImage('./images/byp/byp_logo_email.png', 'byp_logo_email', 'byp_logo_email.png');

$mail->AddEmbeddedImage('./images/byp/builteam_logo_email.png', 'builteam_logo_email', 'builteam_logo_email.png');

$mail->AddEmbeddedImage('./'.$url_images.$subcategory_1_component_image, 'subcategory_1_component', $subcategory_1_component_image);
$mail->AddEmbeddedImage('./'.$url_images.$subcategory_2_component_image, 'subcategory_2_component', $subcategory_2_component_image);
$mail->AddEmbeddedImage('./'.$url_images.$subcategory_3_component_image, 'subcategory_3_component', $subcategory_3_component_image);
$mail->AddEmbeddedImage('./'.$url_images.$subcategory_4_component_image, 'subcategory_4_component', $subcategory_4_component_image);
$mail->AddEmbeddedImage('./'.$url_images.$subcategory_5_component_image, 'subcategory_5_component', $subcategory_5_component_image);
$mail->AddEmbeddedImage('./'.$url_images.$subcategory_6_component_image, 'subcategory_6_component', $subcategory_6_component_image);

$mail->AddEmbeddedImage('./images/byp/dan_sign_email.png', 'dan_sign_email', 'dan_sign_email.png');

$mail->AddEmbeddedImage('./images/byp/this_cgi_email.png', 'this_cgi_email', 'this_cgi_email.png');

$mail->AddEmbeddedImage('./images/byp/bg_hline1_email.png', 'bg_hline1_email', 'bg_hline1_email.png');
$mail->AddEmbeddedImage('./images/byp/bg_hline2_email.png', 'bg_hline2_email', 'bg_hline2_email.png');

$mail->AddEmbeddedImage('.'.$cgi_url, 'cgi', $cgi_fn);

$mail->IsHTML(true);                               // send as HTML


$mail->Subject  =  $email_subject;

$mail->Body     = $ret ;



/**/

if ($mail->Send()) {

	//$resp_status = 1;

	//$resp_msg = 'Message sent successfully';

}

else {

	//$resp_msg = 'Message could not be sent';

}





//echo $ret;

//echo 'resp_status=' . $resp_status . '&resp_msg=' . $resp_msg;


?>