<?php require_once('./inc/header.inc.php'); ?>
<style>
.enteremailwrap {
	border-bottom:2px solid #e9ecf1;
	border-top:2px solid #e9ecf1;
	margin-bottom:20px;
	}
.videopagewrap h2{
	font-size: 30px;
    padding-bottom: 10px;
	margin-bottom:20px;
    color: #003c70;
    font-family: calibri;
	}
.videopagewrap p {
	padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #74787f;
    line-height: 24px;
    font-weight: normal;
	padding-bottom:20px;
	}
.videoleft {
	float:left;
	}	
.videoright {
	float:right;
	}
.mainleft.videoleft {
	width:67%;
	}		
.mainleft.videoright {
	width:30%;
	}
.enteremailwrap {
    padding: 20px 0px;
}
.enteremailwrap h3 {
    color: #003c70;
    font-size: 22px;
    font-weight: bold;
    margin-bottom: 5px;
}
.enteremailwrap p {
    padding: 0px;
    font-size: 18px;
    color: #b1b2b4;
}	
.enteremailwrap input {
    height: 45px;
    vertical-align: top;
    font-weight: bold;
}
.enteremailwrap input[type=email] {
    background: #eef2f5;
    border: none;
    padding: 0px 14px;
    font-size: 16px;
    height: 47px;
}
.enteremailwrap input[type=submit] {
    background: #003c70;
    color: #FFF;
    border: none;
    width: 70px;
    font-size: 16px;
}
.videoinnner img {
    width: 100%;
}
.videoinline.videoimage img {
    width: 100%;
}
.videoinline.videoimage {
    width: 63%;
}
.videoinline {
    display: inline-block;
	vertical-align:top;
    /* width: 49%; */
}
.videolistinner {
    margin-bottom: 17px;
    margin-top: 10px;
}
.videoinline.videodetail p {
    padding-top: 5px;
    color: #969696;
}
.videodetail h4 {
    color: #003c70;
    font-size: 12px;
    margin-bottom: 5px;
}
.videoinline.videodetail h3 {
    color: #003c70;
    font-size: 14px;
}
.videoinline.videodetail p {
    padding-top: 5px;
    color: #969696;
}
.videodetails p strong {
    color: #003c70;
}
.videodetails p {
    color: #b1b2b4;
    font-size: 18px;
}
.showmorebutton a {
    background: #eef2f5;
    width: 100%;
    display: block;
    padding: 10px 0px;
    text-align: center;
    color: #2c4976;
    font-weight: normal;
    font-size: 16px;
}
.videodetails {
    margin-top: 17px;
}	
.videosection {
    padding-bottom: 30px;
    border-bottom: 2px solid #e9ecf1;
	margin-bottom:20px;
}
.sharevideo h2 {
    font-size: 20px;
    color: #003c70;
    margin-bottom: 10px;
}
.socialshare span {
    display: inline-block;
    vertical-align: top;
    margin-right: 30px;
    color: #b1b2b4;
    font-size: 18px;
}
.socialshare a {
    margin-right: 20px;
}
.enteremailwrap .videoleft {
	position:relative;
	}
.opacityvideo {
	opacity:0.5;
	}
.aarrow {
    position: absolute;
    right: -120px;
    top: 11px;
}
.aarrow img {
    width: 100px;
}
.videolistinner {
	display:none;
	}
.upgradethankyoupop{
    display:none;
    position: fixed;
    height: 100%;
    width: 100%;
    background: rgba(16, 70, 116, 0.67);
    top: 0px;
    z-index: 50;
    left: 0px;
}	
.thanksinner {
    position: relative;
    top: 35%;
    height: 130px;
    max-width: 350px;
    background: #FFF;
    left: auto;
    right: auto;
    margin: 0 auto;
    padding: 20px 20px;
    width: 100%;
    text-align: left;
}
.thanksinner h2 {
   
    padding-bottom: 15px;
    color: rgb(16, 70, 116);
    font-size: 20px;
}	
.thanksinner p {
	font-size:17px;
	font-weight:normal;
	color:#969696;
	}
.broform100.checkboxbro.broleft span {
    display: inline-block;
    width: 97%;
    text-align: justify;
    vertical-align: middle;
    font-size: 13px;
}
.broform100.checkboxbro.broleft input {
    width: 15px;
    height: 15px;
    display: inline-block;
    vertical-align: middle;
}
.broform100.checkboxbro.broleft {
    position: relative;
    top: 10px;
}			
@media (max-width:640px) {
	.mainleft.videoleft, .mainleft.videoright, .videodetails .videoleft, .videodetails .videoright {
		width:100%;
		}
	.enteremailwrap input[type=email] {
		width:64%;
		}
	.videoright {
		
		width: 100%;
	}		
	.enteremailwrap p {
		margin:10px 0px;
		}
	.videodetails {
		margin-top:20px;
		padding-bottom:10px;
		margin-bottom:20px;
		border-bottom:2px solid #b1b2b4;
		}
	.videodetails p {
		padding-top:0px;
		}	
	.videoinline.videoimage {
		width:58%;
		}	
	.formobileblock {
		display:block;
		margin-top:20px;
		}		
	.sharevideo h2 {
		margin-bottom:0px;
		}		
	.formobileblock a {
		display:inline-block;
		}
	.socialshare a {
		margin-right:10px;
		}	
	.aarrow {display:none;}		
	}	
</style>
<?php session_start(); // place it on the top of the script ?>

<div class="upgradethankyoupop">
    	<div class="thanksinner">
        	<h2>Thank you!</h2>
            <p>You have successfully subscribed to our mailing list.</p>
			<a class="closeit" href="javascript:void(0);">Ok</a>
            
        </div>
    </div>
<?php


/*
   * Add a 'member' to a 'list' via mailchimp API v3.x
   * @ http://developer.mailchimp.com/documentation/mailchimp/reference/lists/members/#create-post_lists_list_id_members
   *
   * ================
   * BACKGROUND
   * Typical use case is that this code would get run by an .ajax() jQuery call or possibly a form action
   * The live data you need will get transferred via the global $_POST variable
   * That data must be put into an array with keys that match the mailchimp endpoints, check the above link for those
   * You also need to include your API key and list ID for this to work.
   * You'll just have to go get those and type them in here, see README.md
   * ================
   */
   //$api_key = '069da5cedea075584bad9ad2e29ea619-us7';
  //$list_id = '7ed8f67699';
  $newsuccess = false;
  if($_POST['emailname'] != ''){
  @session_start();
	
    
	
	
    $email = $_POST['emailname'];//$_POST['$emailname'];
	
    if(!empty($email) && !filter_var($email, FILTER_VALIDATE_EMAIL) === false){
        // MailChimp API credentials
        $apiKey = '8ea5ffbe45eb9fd5af19b600cc44ed3b-us7';
        $listID = '292211ee50';
        
        // MailChimp API URL
        $memberID = md5(strtolower($email));
        $dataCenter = substr($apiKey,strpos($apiKey,'-')+1);
        $url = 'https://' . $dataCenter . '.api.mailchimp.com/3.0/lists/' . $listID . '/members/' . $memberID;
        
        // member information
        $json = json_encode(array(
            'email_address' => $email,
            'status'        => 'subscribed'
         ));
        
        // send a HTTP POST request with curl
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_USERPWD, 'user:' . $apiKey);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen($json)));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);
        // print_r($result);
        // echo "<br>";
        // print_r($url);
        // echo "<br>";
        // echo $httpCode;
        // store the status message based on response code
        if ($httpCode == 200) {
            $_SESSION['msg'] = 'success';
			$newsuccess = true;
			//<p style="color: #34A853; font-size:20px; ">You have successfully subscribed to Buildteam.</p>
        } else {
            switch ($httpCode) {
                case 214:
                    $msg = 'You are already subscribed.';
                    break;
                default:
                    $msg = 'Some problem occurred, please try again.';
                    break;
            }
            $_SESSION['msg'] = '<p style="color: #EA4335">'.$msg.'</p>';
        }
    }
	
	else{
        $_SESSION['msg'] = '<p style="color: #EA4335">Please enter valid email address.</p>';
    }
//}


//header('location:/build_with_buildteam.html');



    $statusMsg = !empty($_SESSION['msg'])?$_SESSION['msg']:'';
    unset($_SESSION['msg']);
    echo $statusMsg;
	}
	//$newsuccess == false;
?>    
 <script>
$(document).ready(function(e) {
    <? if($newsuccess == true){?>
$(".upgradethankyoupop").fadeIn(200);
<? } ?>
});

</script>
<div class="videowrap">
    <div class="videopagewrap">
        <h2 class="page-title-video">Watch a Build</h2>
        
        <p>Ever wondered what goes on ‘behind the scenes’ of a Build Team Build? If so, now is your chance to find out. David Abimbola (Architectural Designer) guides you through an ongoing build.</p>
    </div>
    
    <div class="enteremailwrap">
    	<div class="videoleft">
        	<h3>Want to watch the progress as we upload? </h3>
            <p>Sign up to our newsletter and we will notify you with updates from site</p>
            <div class="aarrow">
            	<img src="images/video/arrow.png" alt="">
            </div>
        </div>
        <div class="videoright">
        	<form method="post" action>
            	<input type="email" required name="emailname" placeholder="Enter you email here">
				<input type="submit" value="Submit">
            </form>
        </div>
        
        <div class="clearfix"></div>
        <div class="broform100 checkboxbro broleft">
                                <input type="checkbox" name="NewsSubscribe" value="Yes"><span>Sign up to our newsletter and receive updates on our latest blogs, news items and special offers – see our <a href="https://www.buildteam.com/privacy-policy.html">privacy policy</a> for more information on how we use your personal data.</span>
                            </div>
    </div>
    <?
		$rs = getRs("SELECT * FROM video WHERE is_active = 1 ORDER BY id DESC LIMIT 1");
		$row = mysqli_fetch_assoc($rs);
		$link = $row['link'];
		$siteVideoRemove = "";
		if($_GET['videoLink']!=''){
			$link = $_GET['videoLink'];
			$siteVideoRemove = " AND link!='".$_GET['videoLink']."'";
			getRs("update video SET views='".($_GET['views']+1)."' WHERE link='".$_GET['videoLink']."'");
		}else{
			$siteVideoRemove = " AND link!='".$row['link']."'";
			$_GET['project'] = $row['project'];
			$_GET['duration'] = $row['duration'];
		}
	?>
	
    <div class="videosection">
    	<div class="mainleft videoleft">
			<?php ?>
        	<div class="videoinnner">
            	<!--<img src="images/video/videoplay.jpg" alt="">-->
				<iframe src="<?=$link?>" width="656" height="384" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
            </div>	
            <div class="videodetails">
            	<div class="videoleft">
                	<p><strong>Project:</strong> <?=$_GET['project']?></p>
                </div>
                <div class="videoright">
                	<p><strong>Visit:</strong> <?=$_GET['duration']?></p>
                </div>
            	
                <div class="clearfix"></div>
            </div>
           
        </div>
        <div class="mainleft videoright">
        	<div class="videolist">
				<?php
				$rs1 = getRs("SELECT * FROM video WHERE is_active = 1 $siteVideoRemove ORDER BY id DESC");
				while($rows = mysqli_fetch_assoc($rs1)){?>
                
                <a href="?videoLink=<?=$rows['link'].'&views='.$rows['views'].'&project='.$rows['project'].'&duration='.$rows['duration']?>" >
                
            	<div class="videolistinner">
                	<div class="videoinline videoimage">
                    	<!--<iframe src="<?=$rows['link']?>" width="180" height="110" ></iframe>-->
						<?php if($rows['imagefile']){?>
						<img src="media/<?=$rows['imagefile']?>" width="180" height="110" />
						<?php } ?>
				    </div>
                    <div class="videoinline videodetail">
                    	<h4><?=$rows['duration']?></h4>
                        <h3><?=$rows['project']?></h3>
                        <p><?=$rows['views']?> Views</p>
                    </div>
                </div>
                
                </a>
				<? } ?>
                
            </div>
            <div class="showmorebutton">
            	<a id="loadMore" href="javascript:void(0)">SHOW MORE VIDEOS</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
     <div class="sharevideo">
            	<h2>Share</h2>
                <div class="socialshare">
                	<span>Spread the word!</span>
                    <span class="formobileblock">
                    <a target="_blank" href="https://www.facebook.com/Build-Team-391933624520838/"><img src="images/video/facebook.png" alt=""></a>
                    <a target="_blank" href="https://www.instagram.com/buildteamldn/"><img src="images/video/insta.png" alt=""></a>
                    <a target="_blank" href="https://twitter.com/BuildTeamLDN"><img src="images/video/twitter.png" alt=""></a>
                    <a target="_blank" href="https://uk.pinterest.com/buildteam/"><img src="images/video/pint.png" alt=""></a>
                    <a target="_blank" href="https://www.houzz.co.uk/pro/btadmin/build-team"><img src="images/video/homely.png" alt=""></a>
                    <a target="_blank" href="https://plus.google.com/+Buildteam"><img src="images/video/google.png" alt=""></a>
                    </span>
                </div>    
            </div>	
</div>

<script>
$(".closeit").click(function(){
	$(".upgradethankyoupop").fadeOut(200);
	});
</script>
<script>
$(document).ready(function () {
    size_li = $(".videolist a").size();
    x=3;
    $('.videolistinner:lt('+x+')').show();
    $('#loadMore').click(function () {
        x= (x+5 <= size_li) ? x+5 : size_li;
        $('.videolistinner:lt('+x+')').show();
    });
    
});
</script>

<?php require_once('./inc/footer.inc.php'); ?>