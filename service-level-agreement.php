<?php

require_once('./inc/header.inc.php');
?>
<style type="text/css">
.star-wrapper{
    font-size: 15px;
    display:inline-block;
	padding-top:0;
}
.compare-table tr td{
    position: relative;
}
.text-center{
	text-align:center;
}
.serviceinfo p {
	padding-top:0px;
	font-size:18px;
	color:#969696;
	font-weight:normal;
	text-align: justify;
	}
.servcietitle .last_updated {
    float: right;
    display: inline-block;
    vertical-align: bottom;
    top: 25px;
    position: relative;
    font-weight: bold;
}
.servcietitle h1 {
    display: inline-block;
    vertical-align: bottom;
	    margin-bottom: 10px !important;
}
.main.nopaddingmain {
    padding: 0px !important;
}
.main.thead {
    padding: 0px !important;
}
.seervicebluestripe {
	background:#2f6990;
	}
.compare-table th, .byp-btn {
    background: #2f6990;
    color: #fff;
	text-align:left;
}		
.compare-table .odd td, .compare-table .even td {
    background-color: #fff;
    border-bottom: 2px solid #2f6990;
    padding: 15px 10px;
    color: #2f6990;
    font-weight: normal;
	text-align:left;
}
.compare-table td, .compare-table th {
	border:none;
	border-left:1px solid #FFF;
	border-right:1px solid #FFF;
	}
.servicecondition span sup {
    color: #2f6990;
}
.servicecondition span {
    display: block;
    color: #969696;
}
.servicecondition {
    margin-top: 30px;
}	
@media (max-width:640px) {
	.servcietitle .last_updated {
		    top: 0px;
    display: block;
    float: none;
    text-align: right;
    margin-bottom: 10px;
		}
	}
</style>

	<div class="servcietitle">
	<h1>service level agreement</h1>
    <div class="last_updated">
		<span>Last Updated: </span>
		<span>9:00, <?php echo date("M d Y", strtotime( "previous monday" ));?>
		</span>
	</div>
    </div>
    <div class="serviceinfo">
	<p>Client satisfaction is our top priority, and to achieve this we feel it's important to keep an honest path of communication between our Teams and 	our Clients. For this reason, we are proud to offer you our Service Level Agreement, which shows what our targets are and where we are currently running in comparison to those targets. We will always do what we can to beat our targets, and we hope our current run rates will show you how dedicated we are 	to ensuring complete client satisfaction throughout both the Design and the Build process. </p>
    
	</div>
	<br/><br/>
	</div>
    <table class="seervicebluestripe">
    	<tr>
    		<td>
            <div class="main thead">
            	<table class="compare-table">
                    <thead class="">
                        <tr>
                            <th class="compare-first text-center">Service</th>
                            <th class="compare-second text-center">Target</th>
                            <th class="compare-third text-center">Current</th>
                        </tr>
                    </thead>
                </table>
                </div>
            </td>
    	</tr>
    </table>
		 
     <div class="main nopaddingmain">       
            <div class="col_full">
             <table class="compare-table">
				<tbody>
						<tr class="odd">
							<td class="compare-first">Percentage of planning applications that are granted
								<p class="star-wrapper">*
								</p>
							</td>
							<td class="compare-second">80%</td>
							<td class="compare-third">92%</td>
						</tr>  
					   <tr class="even">
							<td>Number of days between initial meeting and submitting design scheme to the council</td>
							<td class="">35</td>
							<td class="">27.5</td>
						</tr>  
						<tr class="odd">
							<td>
								Number of days in the Design Phase   
								<p class="star-wrapper">**
								</p>
							</td>
							<td class="">84</td>
							<td class="">76</td>
						</tr>  
						<tr class="even">
							<td>
								Percentage difference between initial quote and
								final agreed contract sum 
								<p class="star-wrapper">***
									
								</p>
							</td>
							<td class="">+ 15%</td>
							<td class="">+ 7%</td>
						</tr>  
						<tr class="odd">
							<td>Response time to client queries </td>
							<td class="">24 hrs</td>
							<td class="">3.5 hrs</td>
						</tr>  
						<tr class="even">
							<td>
								Number of hours between initial site visit and
								receiving the quote 
							</td>
							<td class="">24 hrs</td>
							<td class="">9 hrs</td>
						</tr>  
						<tr class="odd">
							<td>Turnaround time for drawing amendments </td>
							<td class="">72 hrs</td>
							<td class="">48 hrs</td>
						</tr>  
				</tbody>
         </table>
         
		<div class="servicecondition">
		 <span><sup>*</sup>based on clients who took our advice during the application process.</span>
		 <span><sup>**</sup>this includes 8 week planning determination period.</span>
		 <span><sup>***</sup>based on clients who did not make significant changes to their scheme.</span>
         </div>
	</div>
	
 


<?php

require_once('./inc/footer.inc.php');

?>