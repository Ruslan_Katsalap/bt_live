<?php require_once('./inc/header.inc.php'); ?>

<style>
	.titlehowwecan h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	border-bottom: 1px solid #003c70;
}	
.howwedesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #003c70;
    line-height: 24px;
    font-weight: normal;
}
.howdeschalf {
	width:58%;
	float:left;
	}
.howdeschalf {
	padding-right:10px;
	}	
.howdeschalf2 {
	width:40%;
	float:right;
	}
.howdeschalf2 img {
	width:100%;
	}
.howwedesc ul {
    list-style: none;
    padding: 0;
    margin: 0;
    margin-top: 29px;
}
.howwedesc ul li p {
    display: inline-block;
    width: 88%;
    vertical-align: top;
    font-size: 18px;
}
.howwedesc ul li p:first-child {
    width: 8%;
    margin-right: 10px;
    margin-bottom: 20px;
}
.howwedesc ul li p img {
    max-width: 100%;
}
.howwedesc ul li p span {
    font-weight: bold;
}
.case-study {
    background: #eaeaea;
    padding: 20px 0px;
	margin-top:30px;
}
.casestudywrap {
    max-width: 980px;
    margin: 0 auto;
}
h1.casetitle {
    font-weight: normal;
    font-size: 24px !important;
}
.casetitle2 {
	font-size: 28px !important;
	font-weight: normal;
	}
.case20 {
    float: left;
    width: 21%;
}
.case20 img {
    max-width: 100%;
}
.case80 {
    width: 76%;
    float: left;
    padding-left: 29px;
}
.topimages img {
    display: inline-block;
    vertical-align: top;
    max-height: 184px;
}
.topimages img:nth-child(2) {
    margin: 0px 10px;
}	
.casedesc p {
    font-size: 20px;
    color: #003c70;
    text-align: justify;
}
.planinfo {
	padding:30px 0px; 
	}
.planimg1 h2 {
    font-size: 26px;
    transform: rotate(-90deg);
    /* height: 271px; */
    display: inline-block;
    position: absolute;
    top: 170px;
    left: -110px;
	color: #0e4776;
}
.planimg2 h2 {
    font-size: 26px;
    transform: rotate(-90deg);
    /* height: 271px; */
    display: inline-block;
    position: absolute;
    top: 170px;
    left: -70px;
	color: #0e4776;
}
.planimg img {
    /* max-width: 90%; */
    display: inline-block;
    float: right;
}
.planimg:after {
    content: "";
    display: table;
    clear: both;
}
.planimg {
    position: relative;
	margin-bottom:20px;
}	
.designbudleswrap {
    max-width: 980px;
    margin: 0 auto;
}
.design4 .designinline {
    display: inline-block;
    width: 24.5%;
    text-align: center;
    vertical-align: top;
}	
.designtext .desc {
    font-size: 16px;
    width: 90%;
    margin: 0 auto;
	color:#003c70;
}
.titleplan {
    color: #003c70;
    font-weight: bold;
    font-size: 20px;
    margin: 15px 0px;
}
.pricing {
    margin-top: 20px;
}
.pricing span {
    display: block;
    font-size: 20px;
    font-weight: bold;
    color: #003c70;
    margin-bottom: 5px;
}
.design2 {
    margin-top: 30px;
}
.design2 .designinline {
    width: 49%;
    display: inline-block;
}
.noteinfo p {
    color: #003c70;
    font-size: 18px;
    padding: 0px;
    margin-bottom: 5px;
}
.design2 .designinline {
    width: 49%;
    display: inline-block;
}
.btndesign a {
    padding: 10px;
    background: #eaeaea;
    color: #003c70;
    font-size: 20px;
}
.btndesign {
    float: right;
}
.designbudles {
    padding-top: 40px;
}
.howwedesc.howdeschalf ul li {
    margin-bottom: 10px;
}
@media (max-width:768px) {
	.howdeschalf, .howdeschalf2 {
		width:100%;
		}
	.howwedesc p {
		text-align:left;
		}
	.case20 {
		width:100%;
		}
	.case20 img {
		max-width: 100%;
		margin-bottom: 10px;
	}	
	.case80 {
		width:100%;
		padding-left:0px;
		}
	.casestudywrap {
		padding:0px 20px;
		}	
	.topimages img:nth-child(2) {
		margin: 10px 0px !important;
	}	
	.planimg1 h2, .planimg2 h2{
    font-size: 25px;
    transform: rotate(0deg);
    /* height: 271px; */
    display: inline-block;
    position: relative;
    top: 0;
    left: 0;
    color: #0e4776;
}	
.design4 .designinline {
	width:100% !important;
	}
.designimg img {
    max-width: 100px;
}	
.design4 .designinline {
    width: 100% !important;
    margin-bottom: 50px;
}
.designbudleswrap {
    padding: 0px 20px;
}	
.btndesign {
    float: none;
}
.design2 .designinline {
    width: 100%;
    display: block;
	margin-bottom: 20px;
}	
.noteinfo p {
	font-size:16px;
	}
	}
</style>
<div class="howwecanhelp">
    	<div class="titlehowwecan">
        	<h2>Double Your Design Package</h2>
        </div>
        <div class="howwedesc howdeschalf">
        	<p>Home extensions offer a wonderful window of opportunity to carry out other improvements within the property, and in many cases our clients add a second type of extension onto their scope of works. One of our most popular bundles is our Ground Floor Extension and Loft Conversion package. There are a number of advantages to undertaking both of these projects at the same time:</p>
            <ul>
            	<li><p><img src="images/double-design/plus.png" alt=""></p><p><span>Complement Your Designs: </span>Understanding the entire scope of works from the outset allows our Architectural Team to create designs that complement one another</p></li>
            	<li><p><img src="images/double-design/plus.png" alt=""></p><p><span>Maximise Efficiency: </span>There is a logical sequence to executing the works simultaneously in terms of structural specification, scaffolding and access requirements</p></li>
				<li><p><img src="images/double-design/plus.png" alt=""></p><p><span>Cost Saving: </span>The more you do, the more cost effective it gets! Save £995 + VAT on the Design Phase and even more on your total build cost </p></li>
				<li><p><img src="images/double-design/plus.png" alt=""></p><p><span>Limit the Disruption: </span>Building work is disruptive, and getting all of it over and done with in one period is much less of an annoyance for you (and your neighbours!)</p></li>
            </ul>
        </div>
        <div class="howdeschalf2">
        	<img src="images/double-design/right-home.jpg" alt="">
        </div>
        <div class="clearfix"></div>
    </div>
   </div> 
<div class="case-study">
	<div class="casestudywrap">
    	<h1 class="casetitle">Case Study: <strong>Melgund Road: Ground Floor Extension & Loft Conversion</strong></h1>
        <div class="caseinfo">
        	<div class="caseinline">
            	<div class="case20"><img src="images/double-design/verticle-home.jpg" alt=""></div>
                <div class="case80">
                	<div class="topimages">
                    	<img src="images/double-design/home-1.jpg" alt="">
                    	<img src="images/double-design/home-2.jpg" alt="">
                    	<img src="images/double-design/home-3.jpg" alt="">
                    </div>
                    <div class="casedesc">
                    	<p>The homeowner wanted to create a family orientated kitchen and dining area which had a strong visual connection to the outdoors. They also wanted to add a large family bathroom to their home, as well as a separate utility area to hide away clutter and keep their noisy washing machine out of their social spaces. </p>
                    </div>	
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="planinfo">
        	<div class="planimg planimg1">
            	<h2>Ground Floor Extension</h2>
                <img src="images/double-design/design-1.jpg" alt="">
            </div>
            <div class="planimg planimg2">
            	<h2>Loft Conversion</h2>
                <img src="images/double-design/design-2.jpg" alt="">
            </div>
            <div class="casedesc">
                    	<p>Our Architectural Team suggested that we tuck the utility area into their loft extension, so it doesn’t take up valuable space on the ground floor. The ground floor is typically used for entertaining, so it made sense to remove this cluttered room completely and tuck it away upstairs. This allowed for a wonderful free flow of space on the ground floor, allowing for a living area, dining area and large kitchen. </p>
                    </div>	
        </div>
        
    </div>
</div>
<div class="designbudles">
	<div class="designbudleswrap">
    	<h1 class="casetitle2">Design Bundles: <strong>Ground Floor & Loft</strong></h1>
    	<div class="design4">
        	<div class="designinline">
            	<div class="designimg">
								<img src="/images/get-quote/new_icons.png" class="o-image">
								
							</div>
							<div class="designtext">
								<div class="titleplan">Pre Planning Advice</div>
								<div class="desc">Explore feasibility options via your local council
</div>
							</div>
                            <div class="pricing">
                            	<span>£695</span>
                                <span>Save: £295</span>
                            </div>
            </div>
        	<div class="designinline">
            	<div class="designimg">
								<img src="/images/get-quote/new_icons2.png" class="o-image">
								
							</div>
							<div class="designtext">
								<div class="titleplan">Design &amp; Planning </div>
							<div class="desc">Finalise your design and obtain planning consent</div>
							</div>
                            <div class="pricing">
                            	<span>£2,995</span>
                                <span>Save: £495</span>
                            </div>
            </div>
        	<div class="designinline">
            	<div class="designimg">
								<img src="/images/get-quote/new_icons3.png" class="o-image">
								
							</div>
							<div class="designtext">
								<div class="titleplan">Ready to Build</div>
								<div class="desc">Get ready to build with full Building Control drawings</div>
							</div>
                            <div class="pricing">
                            	<span>£4,995</span>
                                <span>Save: £995</span>
                            </div>
                            
            </div>
        	<div class="designinline">
            	<div class="designimg">
								<img src="/images/get-quote/new_icons4.png" class="o-image">
								
							</div>
							<div class="designtext">
								<div class="titleplan">Fast Track To Build </div>
							<div class="desc">Fast Track your Tender Pack and begin collecting contracts</div>
							</div>
                            <div class="pricing">
                            	<span>£6,495</span>
                                <span>Save: £995</span>
                            </div>
            </div>
        </div>
        <div class="design2">
        	<div class="designinline">
            	<div class="noteinfo">
<!--                	<p>* Subject to confirmation and issuance of quote</p>
                    <p>* Excluding VAT at prevailing rate</p> -->
                    <p>Prices quoted exclude VAT at prevailing rate.</p>   
                </div>
            </div>
            <div class="designinline">
            	<div class="btndesign">
                	<a href="https://www.buildteam.com/design_bundles.html"><strong>Learn more</strong> about our Design Bundles</a>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require_once('./inc/footer.inc.php'); ?>