<?php
require_once('./inc/header.inc.php');
?>
<link href="/css/horizontal.css" rel="stylesheet">
<link href="/css/examples.css" rel="stylesheet">
<link href="/css/ospb.css" rel="stylesheet">

	<style>
.salesadded {
    /*width: 546px !important;*/
}	
.storywidth {
	max-width:980px;
	margin:0 auto;
	}	
.our-story h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
	font-family: calibri !important;
}
.storydesc p {
	padding-top:0px;
	font-size:20px;
	text-align:justify;
	color: #74787f;
	line-height:24px;
	font-weight:normal;
	}
.whereisbegun {
	background:#dedede;
	padding:50px 0px;
	}
.wherinner {
	max-width:980px;
	margin:0 auto;
	}	
.wherinner .width_50 {
	width:50%;
	float:left;
	}
.danintro {
    padding-left: 20px;
}	
.danintro h1 {
    margin: 0px !important;
    padding: 0px;
	text-transform:none !important;
	color:#000;
	/*font-family: 'Tinos', serif !important;*/
}
.danimage img {
    width: 100%;
}
.danintro p {
	font-size: 18px;
    text-align: justify;
    padding-top: 0px;
    line-height: 20px;
    font-weight: normal;
    color: #000;
    letter-spacing: 1px;
    /*font-family: 'Tinos', serif !important;*/
	}	
.storylinkstitle h1{
	border-bottom:5px solid #DEDEDE;
	padding-bottom: 15px !important;
    margin-bottom: 30px !important;
	}	
.storylinks .width_20 {
	width:24%;
	float:left;
	margin-right:1%;
	position:relative;
	}
.imagename {
	position:absolute;
	bottom:0px;
	width:100%;
	background:rgba(255,255,255,0.8);
	padding:10px 0px;
	text-align:center;
	font-size:18px;
	}	
.width_20 img {
	max-width:100%;
	}
.width_20 a {
	color:#003c70;
	font-weight:normal;
	}
.mainlinks {
	max-width:980px;
	margin:0 auto;
	padding-top:50px;
	}	
	
.stepsul {
    list-style: none;
    display: table;
    width: 100%;
    text-align: center;
    margin: 0px;
    padding: 0px 0px;
}
.stepsul li {
    float: left;
    width: 76px;
    color: #003c70;
    font-size: 18px;
    font-weight: normal;
    margin-left: 4px;
	margin-right:4px;
}
.stepsul li:nth-child(9) {
	
	}
.stepsul li:nth-child(10) {
	
	}	
.stepsul li:nth-child(11) {
	
	}	
.oneperframe li h4 {
    text-align: center;
    max-width: 880px;
    margin: 0 auto;
    margin-bottom: 10px;
    margin-top: 20px;
    color: #003c70;
    font-size: 26px;
    max-height: 20px;
}	
.oneperframe li p {
	font-size: 20px;
    color: #74787f;
    max-width: 690px;
    margin: 0 auto;
    text-align: center;
    font-weight: normal;
	padding-top:0px;
	margin-bottom:20px;
	}
.oneperframe li {
	background:none !important;
	}
.controls {
    position: absolute;
    width: 100%;
    top: 210px;
}	
 .controls .prev {
    left: 0px;
    position: absolute;
}	
.controls .next {
    right: 0px;
    position: absolute;
}	
.pages li {
    margin: 0px 35px;
	position:relative;
	background: #fff;
    box-shadow: inset 0 0 0 1px rgba(132,167,189,1);
	color:transparent !important;
}	
.pages li:first-child {
	margin-left:0px;
	}

.pages li.active:before {
    content: "";
    height: 42px;
    width: 50px;
    position: absolute;
    left: -15px;
    top: -25px;
    background: url(../images/story/homeicon.jpg);
    background-size: contain;
}

ul.pages:before {
    height: 3px;
    width: 94%;
    content: "";
    background: #c3d1dd;
    position: absolute;
    margin: 0 auto;
    margin-top: 6px;
    left: 0;
    /* margin: 0 auto; */
    right: 0;
}
.yearwrap img {
    max-width: 600px;
    max-height: 250px;
}
.oneperframe {
    height: auto;
    max-height: 550px;
	line-height:inherit;
}	
.controls img {
    max-width: 30px;
}
.controls .btn {
    background: none;
    border: none;
	box-shadow: none !important;
}
.noimage {
	height:300px;
	}

.pages li.active:nth-child(10):before, .pages li.active:nth-child(11):before, .pages li.active:nth-child(13):before, .pages li.active:nth-child(15):before {
	background:none;
	}
.lineyear {
	margin-bottom:20px;
	}		
.lineyear:after {
    position: absolute;
    height: 2px;
    width: 100%;
    background: #c3d1dd;
    content: "";
    left: 0;
    top: 30px;
    z-index: -1;
}
.lineyear span {
    background: #FFF;
     color: #003c70;
    display: inline-block;
    padding: 10px 20px;
	font-size:30px;
	font-weight:normal;
}	
.middleimagestory {
	display:table;
	width:100%;
	height:300px;
	text-align:center;
	}
.innertablestory {
    max-height: 300px;
    display: table-cell;
    vertical-align: middle;
    overflow: hidden;
}	
.designinineimage img {
	display:inline-block;
	}	
.completequotebtn a {
    text-align: center;
    color: #FFF;
    font-size: 18px;
    text-decoration: none;
}
.completequotebtn {
    position: fixed;
    right: 100px;
    text-align: center;
    bottom: 0px;
    padding: 40px 10px;
    background: #73797f;
    border-radius: 100px;
}	
@media (min-width:640px) {
	.pages li:nth-child(10), .pages li:nth-child(11), .pages li:nth-child(13), .pages li:nth-child(15) {
		margin: 0px 5px;
		width: 8px;
		height: 8px;
		vertical-align: top;
		position: relative;
		top: 3px;
	}
	.lastnomargin {
		margin-right:0px !important;
	}	
	.pages li:nth-child(9)  {
	margin-right: 17px !important;
	}
.pages li:nth-child(12)	{
	margin-left: 17px !important;
    margin-right: 25px !important;
	}
.pages li:nth-child(14) {
	margin-right: 17px !important;
    margin-left: 25px !important;
	}
.pages li:nth-child(16) {
	    margin-left: 25px !important;
	}
.forcenterocket {
	position:relative;
	left:14px;
	}
.yearwrap p span {
	display:block;
	}
	.pages li.active {
    overflow: visible;
}		
	}	
@media (max-width:640px) {
	.our-story {
		padding: 0px 15px;
	}
	.controls {
		top:160px;
		}
	.controls img {
		max-width:10px !important; 
		}
	.middleimagestory, .noimage {
		max-height:150px;
		}	
	.storydesc p {
		font-size:18px;
		line-height:20px;
		}
	.oneperframe li p {
		padding-top:10px;
		margin-bottom:0px;
		}
	.innertablestory {
		max-height:150px;
		}		
	.oneperframe ul li {
		max-width:280px !important;
		margin:0 auto !important;
		}
	.oneperframe {
		max-width:280px;
		margin:0 auto;
		max-height:450px;
		}	
	.yearwrap img {
		max-width: 250px;
    width: auto;
    max-height: 130px;
		}	
	.oneperframe li h4 {
		font-size:18px;
		}	
	.oneperframe li p {
		font-size:16px;
		}
	.pages li {
		margin:0px 3px;
		width: 10px;
    	height: 10px;
		}
	ul.stepsul, ul.pages:before {
   		display: none;
		}
	.pages li.active:before {
		display:none;
		}	
	.wherinner .width_50 {
		width:100%;
		}
	.whereisbegun {
		margin:0px 15px;
		}	
	.danintro {
		padding-left:0px;
		}
	.wherinner {
		padding:0px 15px;
		}
	.danintro h1 {
			margin-top: 15px !important;
			font-size: 22px !important;
		}
	.danintro p {
		font-size:16px !important;
		}	
	.storylinkstitle h1 {
		font-size:20px !important;
		}	
	.storylinks {
    	padding: 0px 15px;
		}	
	.imagename {
    	padding: 5px 0px;
   		font-size: 12px;
	}	
	.lineyear span {
		font-size:26px;
		}
	.storylinks .width_20 {
		width:32%;
		margin-bottom:3px;
		}
	.storylinks .mobile_50 {
		width:48.8% !important;
		}	
	.completequotebtn {
		display:none;
		}	
	.pages li.active {
    overflow: hidden;
}									
	}	
</style>

</div>
<div class="storywidth">
<div class="storywrap">
	<div class="our-story">
    	<div class="titlestory">
        	<h2>Our Story</h2>
        </div>
        <div class="storydesc">
        	<p>At Build Team, we have been building home extensions across London for over a decade. In an industry where there are multiple components, we aim to reduce the confusion and simplify the process. We offer both a Design & Build service, as well as everything you might need in-between. During the past 10 years, we have perfected our two-stage process to allow clients total flexibility. We understand that every project is different, so we ensure that we have everything you might need for a full turn-key service.</p>
        </div>
    </div>
    
    <div class="innerpagewrap">
  <div class="container wrap-container">
    <div class="row">
      <div class="wrap">
      	<div class="toptitle">
        	
        </div>
        
        
        
        <div class="frame oneperframe design-process" id="oneperframe">
          <ul class="clearfix">
            <li class="active">
              <div class="lineyear"><span>2007</span></div>
              <div class="yearwrap">
              <div class="middleimagestory">
              <div class="innertablestory">
              <img class="forcenterocket" src="/images/story/2007.jpg" alt="">
		      </div>
              </div>
              <h4>Build team is Launched</h4>
              <p>Dan leaves a career in Corporate Banking and sets up Build Team.</p>
              </div>
            </li>
            <li>
            	<div class="lineyear"><span>2008</span></div>
              <div class="yearwrap">
              <div class="middleimagestory">
              <div class="innertablestory">
              <img src="/images/story/2008.jpg" alt="">
              </div>
              </div>
              <h4>Online Price Calculator Goes Live</h4>
              <p>Build Team launch an online price calculator, which enables <span>homeowners to generate a quote online which is specific to their requirements.</span></p>
              </div>
            </li>
            <li>
            	<div class="lineyear"><span>2009</span></div>
              <div class="yearwrap">
              <div class="middleimagestory">
              <div class="innertablestory">
              <img src="/images/story/2009.jpg" alt="">	
              </div>
              </div>
              <h4>Move to Holborn</h4>
              <p>As the team continues to grow, we need <span>more space and move to a larger office in Holborn.</span></p>
            	</div>
            </li>
            <li>
            	<div class="lineyear"><span>2010</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
              <div class="innertablestory">
              <img src="/images/story/markjoin.jpg" alt="">	
              </div>
              </div>
              <h4>Mark Eden Joins</h4>
              <p>Mark joined Build Team as Construction Director, to oversee the Build Department.</p>
              </div>
            </li>
            <li>
            	<div class="lineyear"><span>2011</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
                <div class="innertablestory">
              <img src="/images/story/2011.jpg" alt="">
              </div>
              </div>
              <h4>Move to Clapham</h4>
              <p>We outgrow our Holborn Office in just 2 short years,<span>so we move to Clapham (South West London) to accommodate our growing team.</span></p>
              </div>
            </li>
            <li>
            	<div class="lineyear"><span>2012</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
                <div class="innertablestory">
              <img src="/images/story/2012.jpg" alt="">
              </div>
              </div>
              <h4>Website Redesign</h4>
              <p>As our client base continues to grow, we redesign our<span>website to ensure our information is easy to access and informative.</span></p>
              </div>
            </li>
            <li>
            <div class="lineyear"><span>2013</span></div>
            <div class="yearwrap">
            <div class="middleimagestory">
            <div class="innertablestory">
              <img src="/images/story/2013.jpg" alt="">
              </div>
              </div>
              <h4>Two Stage Process</h4>
              <p>We separate our Design & Build Service to enable our client’s further flexibility.</p>
              </div>
            </li>
            <li>
            <div class="lineyear"><span>2014</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
                <div class="innertablestory">
                
              <img src="/images/story/2014_3.png" alt="">
              </div>
              </div>
              <h4>Design Team is launched</h4>
              <p>With the two-stage process launch, we <span>recognise the demand for an architectural design service.</span></p>
              </div>
            </li>
            <li>
            	<div class="lineyear"><span>2015</span></div>
            	<div class="yearwrap">
               <div class="middleimagestory">
                <div class="innertablestory">
              <img src="/images/story/managers.jpg" alt="">
              </div>
              </div>
            	<h4>Expansion of Project Management Division</h4>
            	<p>Our Build department grows and we hire some wonderful Project Managers to accommodate the demand.</p>
                </div>
            </li>
            <li>
            <div class="lineyear"><span>2015</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
                <div class="innertablestory">
              <img src="/images/story/magazine.jpg" alt="">
              </div>
              </div>
            	<h4>(February) Front cover of Real Homes</h4>
            	<p>Real homes feature one of our favourite projects which is in SW4 on the front cover of their February edition.</p>
                </div>
            </li>
            <li>
            	<div class="lineyear"><span>2015</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
                <div class="innertablestory">
              <img src="/images/story/2015_3.jpg" alt="">
              </div>
              </div>
            	<h4>(December) House Tours are launched</h4>
            	<p>We have our first House Tour and 25 of our prospective<span>clients come and have a look around one of our completed Builds.</span></p>
                </div>
            </li>
            <li>
            <div class="lineyear"><span>2016</span></div>
            	<div class="yearwrap">
              <div class="middleimagestory">
                <div class="innertablestory">
              <img src="/images/story/office.jpg" alt="">
              </div>
              </div>
            	<h4>New Head Office</h4>
            	<p>With a team of 20 people, we move to a brand new, purpose built office in SW9.</p>
                </div>
            </li>
            <li>
            	<div class="lineyear"><span>2016</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
                <div class="innertablestory">
                <img src="/images/story/2016_4.jpg" alt="">
              
              </div>
              </div>
            	<h4>Interior Design Service Launched</h4>
            	<p>As our Design Team continues to grow, we accommodate the<span>more design related demands of our clients and launch our Interior Design Service.</span></p>
                </div>
            </li>
            <li>
            <div class="lineyear"><span>2017</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
                <div class="innertablestory">
              <img src="/images/story/2017.jpg" alt="">
              </div>
              </div>
            	<h4>Celebrating Ten Years</h4>
            	<p>Build Team have been providing home extension solutions for a decade.</p>
                </div>
            </li>
            <li>
            	<div class="lineyear"><span>2017</span></div>
            	<div class="yearwrap">
                <div class="middleimagestory">
                <div class="innertablestory">
              <img src="/images/story/2017_2.jpg" alt="">
              </div>
              </div>
            	<h4>(May) Diversity Day</h4>
            	<p>With a team of over 20, we celebrate everyone’s cultures with our first Diversity Day;<span> a day where everyone cooks a dish from their culture and shares it with our team.</span></p>
                </div>
            </li>
            <!--<li>
            	<div class="lineyear"><span>2018</span></div>
            	<h4></h4>
            	<p></p>
            </li>-->
          </ul>
        </div>
        <div class="controls center">
          <button class="btn prev" ><img src="/images/story/prev_btn.jpg"></button>
          <button class="btn next"><img src="/images/story/next_btn.jpg"></button>
        </div>
        <!--<div class="scrollbar">
          <div class="handle">
            <div class="mousearea"><img src="/images/design-pocess/house-drag.png"></div>
          </div>
        </div>-->
        
        <ul class="pages">
          <li class="active"><span></span></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <li class=""></li>
          <!--<li class=""></li>-->
          
        </ul>
        <ul class="stepsul">
          <li>2007</li>
          <li>2008</li>
          <li>2009</li>
          <li>2010</li>
          <li>2011</li>
          <li>2012</li>
          <li>2013</li>
          <li>2014</li>
          <li>2015</li>
          <li>2016</li>
          <li>2017</li>
          <!--<li>2018</li>-->
        </ul>
      </div>
      
    </div>
  </div>
 
</div>

</div>
</div>
 <div class="whereisbegun">
  		<div class="wherinner">
        	<div class="width_50">
            	<div class="danimage">
                	<img src="/images/story/dan_image.jpg" alt="">
                </div>
            </div>
        	<div class="width_50">
            	<div class="danintro">
                	<h1>Where it all began</h1>
                    <p>Dan designed and built his first extension in 1999 for his family home in Dulwich. At this stage, it was very much an interest and a hobby and it wasn’t until he built his second extension five years later that he recognised there was scope for a company that specialised in home extensions. In 2007, he said goodbye to a career in Corporate Banking to set up Build Team. Mark Eden joined the business shortly after and we’ve been growing ever since.</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
  </div>
<div class="mainlinks">
	<div class="storylinks">
    	<div class="storylinkstitle">
        	<h1>Learn more about Build Team</h1>
        </div>
    	<div class="width_20">
        	<a href="https://www.buildteam.com/our-team/meet-the-team.html">
                <img src="/images/story/meet_team.jpg" alt="">
                <div class="imagename">Meet Our Team</div>
            </a>
    	</div>
    	<div class="width_20">
        	<a href="https://www.buildteam.com/about-us/why-choose-page.html">
                <img src="/images/story/why_choose.jpg" alt="">
                <div class="imagename">Why Choose Us</div>
            </a>
    	</div>
    	<div class="width_20">
        	<a href="https://www.buildteam.com/news/latest-news.html">
                <img src="/images/story/news.jpg" alt="">
                <div class="imagename">News</div>
            </a>
    	</div>
    	<div class="width_20">
        	<a href="https://www.buildteam.com/design-bundles.html">
                <img src="/images/story/our_services.jpg" alt="">
                <div class="imagename">Our Services</div>
            </a>
    	</div>
    	<!--<div class="width_20 lastnomargin mobile_50">
        	<a href="https://www.buildteam.com/about-us/why-choose-page.html">
                <img src="/images/story/how_can.jpg" alt="">
                <div class="imagename">Why choose us</div>
            </a>
    	</div>-->
        <div class="clearfix"></div>
    </div>
    <div class="completequotebtn"><a href="/build-your-price-start.html">Complete<br>a Quote Now</a></div>
    
</div>

<script type="text/javascript" src="/js/plugins.js"></script>
    <script type="text/javascript" src="/js/horizontal.js"></script>
    <script type="text/javascript" src="/js/sly.min.js"></script>   
<?php require_once('./inc/footer.inc.php'); ?>