<?php

require_once('./inc/header-innctech.inc.php');

?>
<script>
/*
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-5628250-1', 'auto');
ga('send', 'pageview');

*/
    jQuery(document).ready(function () {
      
            jQuery('#byp_showmyquote_link').on('click', function () {
                //you should first check if ga is set
                if (typeof ga !== 'undefined') {console.log('ga.ok');
                    ga('send', 'event', 'quote', 'submit');
                 }
                //check if _gaq is set too
               else if (typeof _gaq !== 'undefined') {console.log('_gaq.ok');
                    _gaq.push(['_trackEvent', 'quote', 'submit']);
                }
             });
    });

</script>
<style type="text/css">
  #byp-final-table div {
    width: 100%;
  }
  .new_byp-btn-tn {
	  text-align:center;
	  margin-top: 20px;
	  }
  .new_byp-btn-tn .finishbtn {
	  	color:#FFF !important;
		padding: 10px 40px;
		background: #808385;
	  }
</style>

<div class="full">

  <div id="byp_logo_wrap_in" class="byp_logo_wrap lightblue-bg">
    <img src="/images/byp-logo.png" alt="Build Your Price" />
  </div>



  <div class="byp-right" >
  <div id="byp_navigation">
    <div class="byp_item orange-bg" id="byp_step1" onclick="bypStep(0)" style="margin-left: 0;">Step 1: Room Size</div>
    <div class="byp_item" id="byp_step2" onclick="bypStep(1)">Step 2: Exterior</div>
    <div class="byp_item" id="byp_step3" onclick="bypStep(2)">Step 3: Interior</div>
    <div class="byp_item" id="byp_step4" onclick="bypStep(3)">Confirm details</div>
    <div class="byp_item" id="byp_step5" onclick="bypStep(4)">Your free quote</div>
  </div>
  
  <div id="byp_response_nav">
    <div id="mob_screen_back">
      <a href="javascript:;" onclick="bypMobSwipeBackScreen()" style="display:none"><span><!--&#x276E;-->&lt;</span> Back</a>&nbsp;
    </div>
    <div id="mob_screen_title">
      Post code
    </div>
    <div id="mob_screen_next">
      &nbsp;<a href="javascript:;" onclick="bypMobSwipeNextScreen()">Next <span><!--&#x276F;-->&gt;</span></a>
    </div>
  </div>



                                      <!-- Step 1-->


  
  <div id="byp_slide_box">
    <ul>
 
      <li>
      									<!-- Step 1 Form -->

        <form method="post" action="/thank-you-for-submit.html" name="fm_byp1" id="fm_byp1" onsubmit="return false" class="carousel slide">
        <div class="carousel-inner">
          <div class="byp_col item active" id="byp_col1">
            <h1 class="uppercase bold">Step 1<br/><p>Room Size</p></h1>

            <p>
              Please enter your postcode
            </p>

            <input type="text" name="postcode" id="byp_postcode" size="10" maxlength="10" value="" />
            <span id="byp_img_loading"><img src="/images/byp/wr-animation.gif" alt="Loading" /></span>
            <p>
              Is your property a:
            </p>
            <div id="property_type_wrap">
              <label><input type="radio" name="property_type" value="house" checked="checked" /> House</label>
              <label><input type="radio" name="property_type" value="flat" /> Flat</label>
              <input type="button" value="Next" class="btn btn-orange" onclick="bypMobPostcode()" id="mob_postcode_go" />
            </div>
            
          </div>
          <div class="byp_col item" id="byp_col2">
            <p>Please give us the dimensions of the finished space you wish to create</p>
            <div class="byp_sub_col" id="byp_sub_col1">
              <img src="/images/byp/byp_room_plan.png" id="byp_room_plan" alt="Room Plan" />
              <input type="button" value=" OK " class="btn btn-orange" onclick="bypMobSizeInfo(0)" id="mob_size_info0" />
            </div>
            <div class="byp_sub_col" id="byp_sub_col2">
              <input type="button" value=" ? " class="btn btn-orange" onclick="bypMobSizeInfo(1)" id="mob_size_info1" />
              <label>Depth <select name="depth" onchange="bypCalcRoomArea()"><option value="5">5</option></select> m</label>
              <label>Width <select name="width" onchange="bypCalcRoomArea()"><option value="5">5</option></select> m</label>
              <div id="mob_sq">
                = <div id="byp_mob_sq">25</div> sqm
              </div>
              <div id="byp_dim">
                <label id="byp_in"><input type="radio" name="dim" value="in" onchange="bypLoadDimensions()" /> in</label>
                <label id="byp_m"><input type="radio" name="dim" value="m" checked="checked" onchange="bypLoadDimensions()" /> m</label>
              </div>
              <a href="#" id="byp_dim_unknown" onclick="return bypSwitchEstimator(1)">(If you don't know the dimensions, click here.)</a>
            </div>
            <div class="byp_sub_col" id="byp_sub_col3">
              <table>
              <tr>
                <td class="first_col">&nbsp;</td>
                <td class="second_col"><div id="byp_width">5</div> <div id="byp_width_dim">m</div></td>
              </tr>
              <tr>
                <td id="byd_td_width"><div id="byp_depth">5</div> <div id="byp_depth_dim">m</div></td>
                <td id="byp_td_room">Finished<br/>Room Area =<p><div id="byp_sq">25</div> <div id="byp_sq_label">sq</div><div id="byp_sq_dim">m</div></p></td>
              </tr>
              </table>
              
              <div id="byp_estimator">
                <h3>Room Size Estimator</h3>
                <div id="byp_estimator_content">
                  <div class="byp_left1">
                    How many bedrooms do you have
                  </div>
                  <div class="byp_right1">
                    <select name="bedrooms" id="byp_bedrooms" onchange="bypEstimateRoomArea()" />
                    </select>
                  </div>
                  <div class="byp_right2">
                    <label for="terrace_type1" id="lbl_terrace_type1">Terraced</label> <input type="radio" name="terrace_type" id="terrace_type1" value="1" onchange="bypEstimateRoomArea()" /><br clear="all"/>
                    <label for="terrace_type2" id="lbl_terrace_type2">Detached</label> <input type="radio" name="terrace_type" id="terrace_type2" value="2" onchange="bypEstimateRoomArea()" />
                  </div>
                  <div class="byp_center">
                  Based on this information we estimate your extension should be:
                  </div>
                  <div class="byp_center">
                    <div class="byp_left3">
                      <a href="#" onclick="return bypSwitchEstimator(0)">( Cancel )</a>
                    </div>
                    <div class="byp_right3">
                      <div id="byp_sqe">120.25</div> <strong>sqm</strong>
                    </div>
                  </div>
                </div>
              </div>
              
              <input type="button" value=" Next " class="btn btn-orange" onclick="bypMobNextSlider(1)" id="mob_next_2" />
            </div>
          </div>
          <div class="byp_col" id="byp_col3">
            <img src="/images/byp/btn_next.png" alt="Next" onclick="bypStep(1)" id="byp_btn_next1" />
          </div>
        </div>
        </form>					<!-- Form End Here -->
      </li>
      


                    <!-- Step 2-->




      <li>
        <form method="post" action="#" name="fm_byp2" id="fm_byp2" onsubmit="return false" class="carousel slide">
        <div class="byp_col border-right" id="byp_step2_col1">
          <h1 class="uppercase bold">Step 2<br/><p>Exterior Finish</p></h1>
        </div>
        <div class="carousel-inner">
        <div class="byp_col byp_step_col border-right item active" id="byp_step2_col2">
          <h2>Walls</h2>
          <p>Wich walls will be removed?</p>
          <table>
            <tr>
              <td width="10%"><input type="radio" name="wall" value="5" id="byp_wall_5" onchange="bypSelectComponent(this.id)" /></td>
              <td width="90%"><label for="byp_wall_5">Side Wall</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="wall" value="6" id="byp_wall_6" onchange="bypSelectComponent(this.id)" /></td>
              <td><label for="byp_wall_6">Rear Wall</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="wall" value="7" id="byp_wall_7" onchange="bypSelectComponent(this.id)" /></td>
              <td><label for="byp_wall_7">Side and Rear Wall</label></td>
            </tr>
          </table>
          
          <img src="/images/blank.gif" alt="Walls" id="byp_wall_sel" />
          <br/>
          <img src="/byp/bypWall_side.png" alt="Side Walls" id="byp_wall_5_img" class="byp_col_img_w" onclick="bypSelectComponent(this.id)" />
          <img src="/byp/bypWall_rear.png" alt="Rear Walls" id="byp_wall_6_img" class="byp_col_img_w" onclick="bypSelectComponent(this.id)" />
          <img src="/byp/bypWall_both.png" alt="Side and Rear Wall" id="byp_wall_7_img" class="byp_col_img_w" onclick="bypSelectComponent(this.id)" />
          <br/>
          <input type="button" value=" Next " class="btn btn-orange mob_btn_next" onclick="bypMobSwipeNextScreen()" />
        </div>
        <div class="byp_col byp_step_col border-right item" id="byp_step2_col3">
          <h2>Roof</h2>
          <p>&nbsp;</p>
          <table>
            <tr>
              <td width="10%"><input type="radio" name="roof" value="slate" id="byp_roof_slate" /></td>
              <td width="90%"><label for="byp_roof_slate">Slate Roof with Velux Roof Lights</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="roof" value="glass" id="byp_roof_glass" /></td>
              <td><label for="byp_roof_glass">All Glass</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="roof" value="flat" id="byp_roof_flat" /></td>
              <td><label for="byp_roof_flat">Flat Roof with Architectural Roof Lights</label></td>
            </tr>
          </table>
          
          <img src="/images/byp/bypRoof_glass.png" alt="Roof" id="byp_roof_sel" />
          <br/>
          <img src="/images/byp/bypRoof_slate.png" alt="Slate Roof with Velux Roof Lights" id="byp_roof_slate_img" class="byp_col_img_h" />
          <img src="/images/byp/bypRoof_glass.png" alt="All Glass" id="byp_roof_glass_img" class="byp_col_img_h" />
          <img src="/images/byp/bypRoof_flat.png" alt="Flat Roof with Architectural Roof Lights" id="byp_roof_flat_img" class="byp_col_img_h" />
          
        </div>
        <div class="byp_col byp_step_col border-right item" id="byp_step2_col4">
          <h2>Doors</h2>
          <p>&nbsp;</p>
          <table>
            <tr>
              <td width="10%"><input type="radio" name="doors" value="standard" id="byp_doors_standard" /></td>
              <td width="90%"><label for="byp_doors_standard">Standard Patio Doors</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="doors" value="folding" id="byp_doors_folding" /></td>
              <td><label for="byp_doors_folding">Folding/Sliding Doors</label></td>
            </tr>
          </table>
          
          <img src="/images/byp/bypDoors_standard.png" alt="Doors" id="byp_door_sel" />
          <br/>
          <img src="/images/byp/bypDoors_standard.png" alt="Standard Patio Doors" id="byp_doors_standard_img" class="byp_col_img_h" />
          <img src="/images/byp/bypDoors_folding.png" alt="Folding/Sliding Doors" id="byp_doors_folding_img" class="byp_col_img_w" />
          
        </div>
        </div>
        <div class="byp_col byp_step_col" id="byp_step2_col5">
          <h3>FINISHED ROOM AREA=<span id="byp_sq2">25</span>&nbsp;sq<span id="byp_sq_dim2">m</span></h3>
          
          <img src="/byp/bypWall_no.png" id="byp_room2" alt="Room" />
          <div class="byp_buttons">
            <img src="/images/byp/btn_back.png" alt="Back" class="byp_btn_back" onclick="bypStep(0)" style="width: 72px;" />
            <img src="/images/byp/btn_next.png" alt="Next" class="byp_btn_next" onclick="bypStep(2)" style="width: 72px;" />
          </div>
        </div>
        </form>
      </li>
      
      <li>
        <form method="post" action="#" name="fm_byp3" id="fm_byp3" onsubmit="return false" class="carousel slide">
        <div class="byp_col" id="byp_step3_col1">
          <h1 class="uppercase bold">Step 3<br/><p>Interior Finish</p></h1>
        </div>
        <div class="carousel-inner">
        <div class="byp_col byp_step_col border-right item active" id="byp_step3_col2">
          <h2 style="text-align: left;">Kitchen<br>Installation</h2>
          
          <table>
            <tr>
              <td width="10%"><input type="radio" name="kitchen" value="not" id="byp_kitchen_not" /></td>
              <td width="90%"><label for="byp_kitchen_not">Not Required</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="kitchen" value="standard" id="byp_kitchen_standard" /></td>
              <td><label for="byp_kitchen_standard">Standard Kitchen</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="kitchen" value="island" id="byp_kitchen_island" /></td>
              <td><label for="byp_kitchen_island">Kitchen with Island</label></td>
            </tr>
          </table>
          
          <img src="/images/byp/bypKitchen_standard.png" alt="Kitchen" id="byp_kitchen_sel" />
          <br/>
          <img src="/images/byp/bypKitchen_standard.png" alt="Standard Kitchen" id="byp_kitchen_standard_img" class="byp_col_img_h" />
          <img src="/images/byp/bypKitchen_island.png" alt="Kitchen with Island" id="byp_kitchen_island_img" class="byp_col_img_w" />
          
        </div>
        <div class="byp_col byp_step_col border-right item" id="byp_step3_col3">
          <h2 style="text-align: left;">Heating</h2>
          <p>&nbsp;</p>
          <table>
            <tr>
              <td width="10%"><input type="radio" name="heating" value="standard" id="byp_heating_standard" /></td>
              <td width="90%"><label for="byp_heating_standard">Standard Radiators</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="heating" value="underfloor" id="byp_heating_underfloor" /></td>
              <td><label for="byp_heating_underfloor">Underfloor Heating</label></td>
            </tr>
          </table>
          
          <img src="/images/byp/bypHeating_standard.png" alt="Heating" id="byp_heating_sel" />
          <br/>
          <img src="/images/byp/bypHeating_standard.png" alt="Standard Radiators" id="byp_heating_standard_img" class="byp_col_img_h" />
          <img src="/images/byp/bypHeating_underfloor.png" alt="Underfloor Heating" id="byp_heating_underfloor_img" class="byp_col_img_w" />
          
          
        </div>
        <div class="byp_col byp_step_col border-right item" id="byp_step3_col4">
          <h2 style="text-align: left;">Floor<br>installation</h2>
          <p>&nbsp;</p>
          <table>
            <tr>
              <td width="10%"><input type="radio" name="floor" value="ceramic" id="byp_floor_ceramic" /></td>
              <td width="90%"><label for="byp_floor_ceramic">Ceramic Tile</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="floor" value="slate" id="byp_floor_slate" /></td>
              <td><label for="byp_floor_slate">Slate</label></td>
            </tr>
            <tr>
              <td><input type="radio" name="floor" value="wood" id="byp_floor_wood" /></td>
              <td><label for="byp_floor_wood">Natural Wood</label></td>
            </tr>
          </table>
          
          <img src="/images/blank.gif" alt="Floor" id="byp_floor_sel" />
          <br/>
          <img src="/images/byp/bypFloor_ceramic.png" alt="Ceramic Tile" id="byp_floor_ceramic_img" class="byp_col_img_w" />
          <img src="/images/byp/bypFloor_slate.png" alt="Slate" id="byp_floor_slate_img" class="byp_col_img_w" />
          <img src="/images/byp/bypFloor_wood.png" alt="Natural Wood" id="byp_floor_wood_img" class="byp_col_img_w" />
          
        </div>
        </div>
        <div class="byp_col byp_step_col" id="byp_step3_col5">
          <h3>FINISHED ROOM AREA=<span id="byp_sq3">25</span>&nbsp;sq<span id="byp_sq_dim3">m</span></h3>
          
          <img src="/byp/bypWall_no.png" id="byp_room3" alt="Room" />
          <div class="byp_buttons">
            <img src="/images/byp/btn_back.png" alt="Back" class="byp_btn_back" onclick="bypStep(1)" style="width: 72px;" />
            <img src="/images/byp/btn_next.png" alt="Next" class="byp_btn_next" onclick="bypStep(3)" style="width: 72px;" />
          </div>
        </div>
        </form>
      </li>
      
      <li>
        <form method="post" action="#" name="fm_byp4" id="fm_byp4" class="carousel slide">
        <div id="byp_step4_col1">
          <span class="orange uppercase bold">You selected: </span>
          <p>
            <span id="byp_sq4">25</span>&nbsp;sq<span id="byp_sq_dim4">m</span> of finished room area
          </p>
          <div id="byp_selected">
          
          </div>
        </div>
        
        <div id="byp_step4_col2">
        <div id="byp_desktop_userform_info"><p style="font-size: 18px;">YouÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¢ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂre just one step away from your free quotation</p>
        <p>Why we ask this information? <span class="icircle dialog" id="why_ask_link"><i>i</i></span></p></div>
          <div class="carousel-inner">
          <div id="byp_step4_subcol1" class="item active">
            <div id="byp_mob_userform_info">
              <p>YouÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¢ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂre just one step away from your free quotation</p>
              <p>Why we ask this information? <span class="icircle dialog" id="why_ask_link"><i>i</i></span></p>
            </div>
            <label for="byp_name">First name*</label>
            <input type="text" name="name" id="byp_name" maxlength="50" />
            <label for="byp_surname">Surname*</label>
            <input type="text" name="surname" id="byp_surname" maxlength="100" />
            
            <div id="byp_register_info">
              <label for="byp_email">Email*</label>
              <input type="text" name="email" id="byp_email" maxlength="100" />
              <label for="byp_password">Password* (to log in and see saved quotes later)</label>
              <input type="password" name="password" id="byp_password" maxlength="100" />
              <label for="byp_repassword">Retype Password*</label>
              <input type="password" name="repassword" id="byp_repassword" maxlength="100" />            
            </div>
            
            <label for="byp_phone">Contact number*</label>
            <input type="text" name="phone" id="byp_phone" maxlength="20" />
            <label for="byp_house">House or flat number*</label>
            <input type="text" name="house" id="byp_house" />
            <select name="addr" id="byp_addr">
              <option value="">- or select address -</option>
            </select>
            <label for="byp_postcode_f">Postcode*</label>
            <input type="text" name="postcode_f" value="SE22 8PY" id="byp_postcode_f" readonly />

            <p>* Required fields</p>
            <input type="button" value=" Next " class="btn btn-orange mob_btn_next" onclick="bypMobSwipeNextScreen()" />
            
          </div>
          <div id="byp_step4_subcol2" class="item">
            <label for="byp_msg">Comments</label>
            <textarea name="message" cols="31" rows="3" id="byp_msg"></textarea>

            <label for="when_to_start">When are you looking to start the Design Phase?*</label>
            <select id="when_to_start">
              <option value="0">Please select</option>
              <option value="immediately">immediately</option>
              <option value="1-3 months">1-3 months</option>
              <option value="3-6 months">3-6 months</option>
              <option value="1 year">1 year</option>
            </select>

            <label for="current_status">Please describe your current status*</label>
            <select id="current_status">
              <option value="0">Please select</option>
              <option value="pre-purchase">pre-purchase</option>
              <option value="just moved in">just moved in</option>
              <option value="resident for less than 1 year">resident for less than 1 year</option>
              <option value="resident for more than 1 year">resident for more than 1 year</option>
            </select>

            <a href="/images/byp/example_quote.pdf" class="pdf_link" target="_blank"><span>View example quote</span></a>

<!-- Google Code for Build Your Price Quote Conversion Page
In your html page, add the snippet and call
goog_report_conversion when someone clicks on the
chosen link or button. -->
<script type="text/javascript">
  /* <![CDATA[ */
  goog_snippet_vars = function() {
    var w = window;
    w.google_conversion_id = 938671590;
    w.google_conversion_label = "LNARCNiS1WEQ5vvLvwM";
    w.google_remarketing_only = false;
  }
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
    goog_snippet_vars();
    window.google_conversion_format = "3";
    window.google_is_call = true;
    var opt = new Object();
    opt.onload_callback = function() {
    if (typeof(url) != 'undefined') {
      window.location = url;
    }
  }
  var conv_handler = window['google_trackConversion'];
  if (typeof(conv_handler) == 'function') {
    conv_handler(opt);
  }
}
/* ]]> */
</script>
<script type="text/javascript"
  src="//www.googleadservices.com/pagead/conversion_async.js">
</script>


<!-- <a class="byp-btn-lg" href="javascript:;" onclick="submitUserInfo();goog_report_conversion();" id="byp_showmyquote_link">Show my quote</a> -->
<a class="byp-btn-lg" href="javascript:;" onclick="submitUserInfo()" id="byp_showmyquote_link">Show my quote</a>
<!--<input type="button" class="byp-btn-lg" onclick="submitUserInfo(); ga('send', 'event', 'quote', 'submit');" id="byp_showmyquote_link" value="Show my quote">-->
            <input type="button" value="Show my quote" class="btn btn-orange" onclick="bypMobSwipeNextScreen()" id="byp_mob_showmyquote" />
            
            <p class="byp_terms_agree">By clicking on the ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¢ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂShow my quoteÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¢ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ button you confirm that you agree to our <a href="#" id="terms_link" class="dialog">terms and conditions</a> 
             statement and you have read and understand the <a href="#" id="privacy_link" class="dialog">privacy policy</a></p>
          </div>
          </div>
          
        </div>
            
            <div id="why_ask_dialog" style="display:none">
              <div class="lightbox_popup">
                <div class="lightbox_inner">
                  <div class="lightbox_content">
                    <h3>Why we ask for this information?</h3>
                    <p>You will get a personalised PDF of your quotation sent directly to your email.</p>
                    <p>Your personal information will only be used as set out in Build Team's <a href="/privacy-policy.html" target="_blank">privacy policy</a>.</p>
                    <p>Our team may use your information to contact you about your Side Return needs. This service can help you at both the Design and Build phase of the project, including costs, design options and planning considerations.</p>
                    <p>We may also contact you via post, phone or email. You can unsubscribe at any time by clicking on the unsubscribe link or by using the contact details in our <a href="/privacy-policy.html" target="_blank">privacy policy</a>.</p>
                  </div>
                </div>
              </div>
            </div>
            
            <div id="login_dialog" style="display:none">
              <div class="lightbox_popup">
                <div class="lightbox_inner">
                  <div class="lightbox_content">
                    <h3 class="login_title">Log In</h3>
                    <p id="user_email_msg" class="user_email_msg"></p>
                    <p><label>Email:</label> <input type="text" class="login_email" value="" /></p>
                    <p class="login_pwd_p"><label>Password:</label> <input class="login_pwd" type="password" onkeypress="if(event.keyCode==13){loginUserRequest('user_module');}" />
                    </p>
                    <p><input type="button" value="Log In" class="login_btn" onclick="loginUserRequest('user_module')" /> <img src="/images/byp/loading2.gif" alt="Loading" class="login_loading" style="display:none" /></p>
                    <p style="clear:both;text-align:center"><a href="javascript:;" onclick="forgotPassword()" class="login_forgot_link">Forgot password?</a></p>
                  </div>
                </div>
              </div>
            </div>
            
            <div id="terms_dialog" style="display:none">
              <div class="lightbox_popup">
                <div class="lightbox_inner">
                  <div class="lightbox_content">
                    <h3>Terms and conditions</h3>
                    <p>Prices are subject to the addition of VAT at the prevailing rate.</p>
                    <p>The estimate is based on the drawings and detail therein, as provided by the client and Build Team Holborn Limited cannot accept any responsibility for errors contained in these except for those drawings and detail prepared by Build Team Holborn Limited.</p>
                    <p><a href="/terms-and-conditions.html" target="_blank">More info</a></p> 
                  </div>
                </div>
              </div>
            </div>

            <div id="privacy_dialog" style="display:none">
              <div class="lightbox_popup">
                <div class="lightbox_inner">
                  <div class="lightbox_content">
                    <h3>Privacy policy</h3>
                    <p>Build Team is committed to safeguarding the privacy of our users while providing the highest possible quality of service. We will only use the information that we collect about you lawfully (in accordance with the Data Protection Act 1998).</p>
                    
                    <p>We will not pass your information to any third parties.</p>
                    <p>
                    <a href="/privacy-policy.html" target="_blank">More info</a>
                    </p>
                  </div>
                </div>
              </div>
            </div>
   
      </form>
      </li>
      
      
      <li>
      <form method="post" action="#" name="fm_byp5" id="fm_byp5" enctype="multipart/form-data">
        <input name="MAX_FILE_SIZE" type="hidden" value="10000000" />
        <input name="qid" type="hidden" value="" />
        <input name="quote_type_id" type="hidden" value="" />
        <input name="userid" type="hidden" value="" />

          <div class="byp-final-page">
            <div id="byp_cgi_wrap">
              <img src="/byp/OPTION_21.jpg" alt="Room" id="byp_cgi" />
              <div id="byp_cover"><p>Your address falls outside the coverage area for our full design &amp; build
service, however we can still help. Please <a href="/getting-started/nationwide.html" target="_blank">click here</a> for more information.</p></div>
              <div id="byp_mob_cgi_tip">This is a computer generated image of how your extension could look based on your specific requirements</div>
            </div>
            <div class="inner">
              <p class="byp_quote_title">Your side return quote</p>
              <div class="byp_quote_label">Quote Ref: <div id="byp_quote_ref">BYP/SC5D6/SE22</div></div>
              <div id="design_phase" class="quote_phase">
              <input type="checkbox" name="phase_design" value="1" id="byp_phase_design" checked="checked" style="display: none;" />
              <label for="byp_phase_design">Design Phase</label>
              <div id="byp_design_features">
               <p><img src="/images/thick.png" class="byp-thick" /> Architectural plans</p>
               <p><img src="/images/thick.png" class="byp-thick" /> Planning permission</p>
               <p><img src="/images/thick.png" class="byp-thick" /> Structural engineering</p>
              </div>
              <div id="byp_savings">
                <p><strong id="byp_design_cost">&pound;3,995</strong> <span id="byp_save">SAVE</span> <span id="byp_design_discount">&pound;1,000</span>!</p>
                Offer valid for instructions
                received by <span id="byp_offer_date">30.6.12</span>
              </div>
              <div class="byp_phase_total" id="byp_design_total">&pound;2,995</div>
            </div>
            <div id="construction_phase" class="quote_phase">
              <input type="checkbox" name="phase_construction" value="1" id="byp_phase_construction" checked="checked" style="display: none;" />
              <label for="byp_phase_construction_" style="display: inline-block; margin-top: 15px;">Construction Phase</label>
              <div id="byp_construction_features">
               <p><img src="/images/thick.png" class="byp-thick" /> Full project management</p>
               <p><img src="/images/thick.png" class="byp-thick" /> Electrical work &amp; plumbing</p>
               <p><img src="/images/thick.png" class="byp-thick" /> Roof lights &amp; doors</p>
               <div class="byp_phase_total" id="byp_construction_total">&pound;49,777</div>
              </div>
            </div>
            <div id="byp_duration_wrap">
              <div id="byp_duration_label"><label>Duration of Works</label></div>
              <div id="byp_duration_term">14 weeks</div>
              <p>Quotation subject to site survey and VAT at prevailing rate</p>
            </div>
            
            <div id="byp-final-btns">
              <div id="btn_action1_wrap"><div class="byp-btn-tn"><a href="javascript:;" onclick="bypReviseQuote()" id="btn_action1">Revise quote</a></div></div>
              
              <div id="btn_action2_wrap"><div class="byp-btn-tn"><a href="javascript:bypPopupAttach(1)" id="byp_attach_link">Attach Images</a></div></div>
              
              <div id="btn_action3_wrap"><div class="byp-btn-tn"><a href="javascript:;" onclick="bypSubmitDetails('save')" id="btn_action2">Save quote</a></div></div>
              
              <div id="byp_main_action_wrap">
                <div onclick="bypSubmitDetails('email')" id="btn_main_action"><div id="btn_main_action_text">BOOK SITE VISIT</div></div>
              </div>
            </div>

          </div>
           
          <img src="/images/byp_final_2.png?v=3" alt="Click here to email the detailed quotation and book an appointment for a site visit" id="byp_quote_tip" />
          <div id="byp_mob_quote_tip">Click &quot;Email Quote&quot; to email the detailed quotation and book an appointment for a site visit</div>
           
          <img src="/images/byp_final_arrow.png?v=3" alt="Arrow" id="byp_arrow_tip" />
           <div class="new_byp-btn-tn">
           <a class="finishbtn" href="/thank_you.php">Finish</a>
           </div>
        <div class="byp_popup" id="byp_attach_box">
          <input type="button" value="X" class="byp_popup_btn" id="byp_close_attach_box" onclick="bypPopupAttach(0)" />
          <br clear="all" />
          <img src="/images/byp/photo.jpg" alt="Rear Photo" width="220" />
          It would help us to assess your project more accurately if you could attach a photo of the rear of your property like this one.
          <br/><br/>
          You can upload up to 5 photos. Please click Attach to return to the quote summary.
          <br/>
          <input type="button" value="Upload" class="byp_popup_btn" id="byp_upload_btn" onclick="" />
          <div id="byp_files_wrap">
            <input type="file" name="photo" id="photo" class="byp_file" onchange="bypAddFile(this)" />
            <input type="file" name="photo2" id="photo2" class="byp_file" onchange="bypAddFile(this)" />
            <input type="file" name="photo3" id="photo3" class="byp_file" onchange="bypAddFile(this)" />
            <input type="file" name="photo4" id="photo4" class="byp_file" onchange="bypAddFile(this)" />
            <input type="file" name="photo5" id="photo5" class="byp_file" onchange="bypAddFile(this)" />
            <a href="javascript:alert('You can upload a maximum of 5 photos')" id="byp_uploadmax_hint">&nbsp;</a>
          </div>
          <div id="byp_files_count">0 / 5</div>
          <div id="byp_attach_files">
          [Files to be uploaded]
          </div>
          <input type="button" value="Attach" class="byp_popup_btn" id="byp_close_attach" onclick="bypPopupAttach(0)" />
        </div>
        
        <div class="byp_popup" id="byp_poa">
          <p id="byp_poa_msg">Your project is of a size which is too difficult for BuildYourPrice&trade; to provide an accurate quotation. Please call 020 7495 6561 to arrange a quotation.</p>
            <input type="button" value="Close" class="byp_popup_btn" id="byp_close_poa" onclick="bypPopupPOA(0)" />
        </div>
        
        <div style="display:inline;">
          <img height="1" width="1" style="border-style:none;" alt="" src="/images/blank.gif" id="conversion_track" />
        </div>
        
        </div>
        </form>


      </li>

      <li>
        <div id="byp_thank_you">
          
          <div class="lightblue-bg" style="display:inline-block;width: 100px; height: 100px; padding: 10px; text-align: center;">

            <img style="width: 80px;" alt="Build Your Price" src="/images/byp-logo.png"></img>

          </div>
          
          <p style="margin-left:auto;margin-right:auto;width:292px">Thank you for using BuildYourPrice<sup>&trade;</sup>
          <span id="thank_you_msg"></span>
          </p>
          
        </div>
      </li>
    </ul>
  </div>
  
  <div id="byp_get_costing">
    <img src="/images/byp/loading.gif" class="imgload" alt="Loading..." />
    <div id="byp_loading_text">
      Building Your Price
    </div>
  </div>
  <div id="byp_submit_quote"></div>
</div>
</div>

<script type="text/javascript">
  $( document ).ready(function() {
    
    <?php // auto login 
      if (isset($_SESSION['userid']) && $_SESSION['userid']) {
        echo 'global_byp_config.userid = parseInt("'.$_SESSION['userid'].'");';
        
        if (isset($_SESSION['user_info']['firstname'])) {
          echo 'document.getElementById("byp_name").value = "'.htmlspecialchars($_SESSION['user_info']['firstname'], ENT_QUOTES, 'UTF-8').'";';
        }
        
        if (isset($_SESSION['user_info']['surname'])) {
          echo 'document.getElementById("byp_surname").value = "'.htmlspecialchars($_SESSION['user_info']['surname'], ENT_QUOTES, 'UTF-8').'";';
        }
        
        if (isset($_SESSION['user_info']['phone'])) {
          echo 'document.getElementById("byp_phone").value = "'.htmlspecialchars($_SESSION['user_info']['phone'], ENT_QUOTES, 'UTF-8').'";';
        }

        if (isset($_SESSION['user_info']['address'])) {
          echo 'document.getElementById("byp_house").value = "'.htmlspecialchars($_SESSION['user_info']['address'], ENT_QUOTES, 'UTF-8').'";';
        }
        
        if (isset($_SESSION['quote_info'])) {
          
          if (isset($_SESSION['quote_info']['postcode'])) {
            echo 'document.getElementById("byp_postcode").value = "'.$_SESSION['quote_info']['postcode'].'";';
          }
          
          if (isset($_SESSION['quote_info']['property_type']) && 'flat'==$_SESSION['quote_info']['property_type']) {
            echo 'document.fm_byp1.property_type[1].checked=true;';
          }
          
          if (isset($_SESSION['quote_info']['measure_unit_id']) && 2 == $_SESSION['quote_info']['measure_unit_id']) {
            echo 'document.fm_byp1.dim[0].checked=true;';
          }          
          
        }
      }
    ?>
    
  //debug
  /*
  document.getElementById("byp_slide_box").style.display = "none";
  document.getElementById("byp_get_costing").style.display = "block";
  //document.getElementById("byp_get_costing").style.textAlign = "left";
  
  document.getElementById("fm_byp5").quote_type_id.value = "email";
  
  if ("email"==document.getElementById("fm_byp5").quote_type_id.value) document.getElementById("thank_you_msg").innerHTML = "Your quote has been emailed and should arrive very shortly. We will be in touch shortly to arrange an appointment for a site visit.";
  else document.getElementById("thank_you_msg").innerHTML = "Your quote has been saved. You can retrieve it at any time in <a href=\"#\">your account page</a>.";
  
  document.getElementById("byp_get_costing").innerHTML = document.getElementById("byp_thank_you").innerHTML;
  
  
  global_byp_config.lightbox = $.lightbox({
    content: document.getElementById("login_dialog").innerHTML
  });
  //debug
  */

  });
  
  function preloadAfterInit() {
    <?php if (isset($_SESSION['quote_info'])) {
    
      if (isset($_SESSION['quote_info']['room_length']) && (int)$_SESSION['quote_info']['room_length'] && isset($_SESSION['quote_info']['room_width']) && (int)$_SESSION['quote_info']['room_width']) {
      echo 'var depth=parseInt("'.$_SESSION['quote_info']['room_length'].'");';
      echo 'var width=parseInt("'.$_SESSION['quote_info']['room_width'].'");';
      ?>
      for(var i=0;i<document.fm_byp1.depth.options.length;i++) {
        if (document.fm_byp1.depth.options[i].value==depth) {
          document.fm_byp1.depth.options.selectedIndex = i;
          break;
        }
      }
      for(i=0;i<document.fm_byp1.width.options.length;i++) {
        if (document.fm_byp1.width.options[i].value==width) {
          document.fm_byp1.width.options.selectedIndex = i;
          break;
        }
      }
      bypCalcRoomArea();
      <?php
      } // endif set length and width 
      
      if (isset($_SESSION['quote_info']['use_room_size_estimator']) && $_SESSION['quote_info']['use_room_size_estimator']) {
        
        echo 'bypSwitchEstimator(1);';
        
        $a_est = explode('|', $_SESSION['quote_info']['use_room_size_estimator']);
        echo 'var est_br = parseInt("'.$a_est[0].'");';
        echo 'var est_tt = parseInt("'.$a_est[1].'");';
        ?>
        for (i=0;i<document.fm_byp1.bedrooms.options.length;i++) {
          if (document.fm_byp1.bedrooms.options[i].value==est_br) {
            document.fm_byp1.bedrooms.options.selectedIndex = i;
            break;
          }
        }
        if (1==est_tt) document.fm_byp1.terrace_type[0].checked=true;
        if (2==est_tt) document.fm_byp1.terrace_type[1].checked=true;
        bypEstimateRoomArea();
        <?php
      }
      
      if (isset($_SESSION['quote_info']['subcategory_1_component_id']) && isset($_SESSION['quote_info']['subcategory_2_component_id']) && isset($_SESSION['quote_info']['subcategory_3_component_id']) && isset($_SESSION['quote_info']['subcategory_4_component_id']) && isset($_SESSION['quote_info']['subcategory_5_component_id']) && isset($_SESSION['quote_info']['subcategory_6_component_id'])) {
        
        $a_component = array(
          'cb_door' => 1,     // subcategory_1_component_id
          'cb_roof' => 2,     // subcategory_2_component_id
          'cb_wall' => 3,     // subcategory_3_component_id
          'cb_floor' => 4,    // subcategory_4_component_id
          'cb_kitchen' => 5,  // subcategory_5_component_id
          'cb_heating' => 6   // subcategory_6_component_id
        );
        
        echo 'var a_comp=[];';
        
        foreach ($a_component AS $k => $v) {
          echo 'a_comp["'.$k.'"]=parseInt("'.$_SESSION['quote_info']['subcategory_'.$v.'_component_id'].'");';
        }
      
      ?>
      var j = 0;
      for (var i in global_byp_config.panel) {
    
        key = global_byp_config.panel[i];
        
        var a_p = i.split("_"); // byp_step3_col4
        
        a_p = a_p[1].replace("step", "");
        
        eval("var fm_el = document.fm_byp" + a_p + "."+key+";");
        
        for(var y=0;y<fm_el.length;y++) {
          
          if (fm_el[y].value == a_comp[key]) {
            fm_el[y].checked = true;
            a_p = key.replace("cb_", "byp_");
            a_p += "_"+a_comp[key];
            bypSelectComponent(a_p);
            break;
          }
        }

        j++;
      }
      
      
      
      <?php
      } // end if subcategories components
      
      if ( isset($_SESSION['quote_info']['name']) && isset($_SESSION['user_info']['firstname']) && isset($_SESSION['user_info']['surname']) && $_SESSION['quote_info']['name'] != $_SESSION['user_info']['firstname'].' '.$_SESSION['user_info']['surname'] ) {
        
        $a_name = explode(' ', $_SESSION['quote_info']['name']);
        $firstname = array_shift($a_name);
        $surname = implode(' ', $a_name);
        
        echo 'document.getElementById("byp_name").value = "'.htmlspecialchars($firstname, ENT_QUOTES, 'UTF-8').'";';

        echo 'document.getElementById("byp_surname").value = "'.htmlspecialchars($surname, ENT_QUOTES, 'UTF-8').'";';
        
      }
      
      if (isset($_SESSION['quote_info']['phone'])) {
        echo 'document.getElementById("byp_phone").value = "'.htmlspecialchars($_SESSION['quote_info']['phone'], ENT_QUOTES, 'UTF-8').'";';
      }

      if (isset($_SESSION['quote_info']['address'])) {
        echo 'document.getElementById("byp_house").value = "'.htmlspecialchars($_SESSION['quote_info']['address'], ENT_QUOTES, 'UTF-8').'";';
      }
      
      if (isset($_SESSION['quote_info']['message'])) {
        echo 'document.getElementById("byp_msg").value = "'.htmlspecialchars($_SESSION['quote_info']['message'], ENT_QUOTES, 'UTF-8').'";';
      }
      
      if (isset($_SESSION['quote_info']['start'])) {
        echo 'var a_p="'.$_SESSION['quote_info']['start'].'";';
        ?>
        document.getElementById("when_to_start").value = a_p;
        <?php
      } // endif start
      
      if (isset($_SESSION['quote_info']['status'])) {
        echo 'var a_p="'.$_SESSION['quote_info']['status'].'";';
        ?>
        document.getElementById("current_status").value = a_p;
        <?php
      }
      
      // set qid!
      if (isset($_SESSION['quote_info']['qid'])) echo 'global_byp_config.qid=parseInt("'.$_SESSION['quote_info']['qid'].'");';
	  
	  echo "<script type='text/javascript'> ga('send', 'event', 'quote', 'submit'); </script>";
      
      unset($_SESSION['quote_info']);
      
    } // endif set quote_info  ?>
  }  
</script>


<?php

require_once('./inc/footer-innctech.inc.php');

?>