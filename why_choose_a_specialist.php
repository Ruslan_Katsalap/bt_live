<?php require_once('./inc/header.inc.php'); ?>

<?php //require_once('save_how_can_help_form.php'); ?>
<link rel="stylesheet" href="css/slick.css" type="text/css">
<link rel="stylesheet" type="text/css" href="/Flipbook/font-awesome.css">
<script src="/Flipbook/flipbook.min.js"></script>

<style>
	.titlehowwecan h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	border-bottom: 1px solid #003c70;
}	
.howwedesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #003c70;
    line-height: 24px;
    font-weight: normal;
}
.middleinner {
    max-width: 980px;
    margin: 0 auto;
	padding:50px 0px;
}
.innerrow .insiderow {
    float: left;
    width: 33%;
    text-align: center;
	padding:15px 0px;
}
.insiderow:nth-child(2) {
	    border-left: 1px dotted #003c70;
    border-right: 1px dotted #003c70;
	
	}
.forborder {
	 border-bottom: 1px dotted #003c70;
	}	
.innerrow:after {
    content: "";
    clear: both;
    display: table;
}
.upgradetitle h2 {
    color: #0f4778;
    font-size: 18px;
	margin-top:15px;
}
.upgradedesc p {
    color: #9f9f9f;
    font-size: 16px;
}

.insiderow img {
    height: 40px;
}
.bottomshortcut {
	max-width:980px;
	margin:0 auto;
	}
.shortcutinline {
    float: left;
    width: 24.2%;
}
.shortcutinline:nth-child(3) {
    margin-right: 1%;
}
.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
    margin-left: 1%;
}
.shortcutdesc {
    background: rgba(255,255,255,0.7);
    padding: 10px;
    text-align: center;
    color: #0f4679;
    position: absolute;
    bottom: 0px;
    width: 100%;
    font-weight: bold;
    font-size: 18px;
}
.shortcutwarp {
    position: relative;
}
.shortcutwarp img {
    max-width: 100%;
}
.tenderprocess {
    background: #ecebeb;
    margin: 50px 0px;
}
.tenderpprocesstitle h1 {
    font-weight: normal;
    padding: 0px;
}
.noteinfo {
    text-align: right;
    position: relative;
    top: 30px;
}
.noteinfo p {
    background: #bdbdbd;
    display: inline-block;
    padding: 0;
    color: #547088;
    padding-right: 10px;
    font-size: 16px;
}
.noteinfo span {
    background: #9f9f9f;
    display: inline-block;
    width: 20px;
    text-align: center;
    padding: 5px 5px;
    color: #FFF;
    font-size: 20px;
    vertical-align: middle;
    margin-right: 10px;
}
.noteinfo.noteinfo2 {
    top: -20px;
    right: 3px;
	margin-bottom:30px;
}
.moreinfo {
	max-width:980px;
	margin:0 auto;
	}
.moreinfo h1 {
    font-size: 20px !important;
    border-bottom: 2px solid #0f4778;
}
@media (max-width:768px) {
	button.slick-prev.slick-arrow {
		left:0px;
		}
	button.slick-next.slick-arrow{
		right:0px;
		}
	.tenderslider:before {
		display:none;
		}	
	.inlineflipbook {
			margin-left: 0px;
			width: 100% !important;
			max-width: 100%;
		}	
	.fliboocwrap {
		background: #f2f5f8;
		height: 825px;
		widows: 100%;
		margin-top: 50px;
	}
	.inlineflipbook.flipmargin {
		margin-bottom: 30px;
	}	
	.shortcutinline {
		float: none;
		width: 100%;
		margin-bottom:5px;
	}		
	.shortcutinline img {
		width:100%;
		}
	.moreinfo, .bottomshortcut {
		padding:0px 20px;
		}
	.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
		margin-left:0px !important;
		}
	.innerrow .insiderow {
		width:100%;
		}	
	.insiderow img {
		height: 40px;
		width: auto;
	}		
	.insiderow, .forborder{
		border:none !important; 
		}	
	}	
</style>
<div class="howwecanhelp">
    	<div class="titlehowwecan">
        	<h2>Why Choose a Specialist</h2>
        </div>
        <div class="howwedesc">
        	<p>The Design & Build process is far from simple, there are a lot of moving components which require experience and professional expertise. With over a decade of experience under our belt, our team has created a seamless, easy to follow process which guides you each step of the way. Here are a few reasons why you should consider appointing a specialist when undertaking a home extension.</p>
        </div>
    </div>
</div>

<div class="middlerow">
	<div class="middleinner">
    	<div class="innerrow forborder">
        	<div class="insiderow">
            	<div class="upgradeimage">
                    <img src="/images/upgradepremium/3.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>In-Depth Site Survey</h2>
                </div>
                <div class="upgradedesc">
                    <p>Our Architectural Team undertake a<br>thorough on-site survey. This includes<br>site measurements, general ground conditions,<br>as well as nearby amenities that might<br>affect planning (eg. rivers and trees).</p>
                </div>
            </div>
            <div class="insiderow">
            	<div class="upgradeimage">
                    <img src="/images/upgradepremium/4.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>Experienced Planning Team</h2>
                </div>
                <div class="upgradedesc">
                    <p>Experiance is crucial when it comes<br> to planning. With experience of over<br> 600 planning applications, our Design<br> Team is well equipped to advise<br> on a variety of home extension schemes.</p>
                </div>
            </div>
            <div class="insiderow">
            	<div class="upgradeimage">
                    <img src="images/specialist/home.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>Party Wall Agreements</h2>
                </div>
                <div class="upgradedesc">
                    <p>Party Wall agreements require technical<br>knowledge to understand foundation<br>details. If you get it wrong, it can<br>have repercussions for your build start date so<br> we recommend instructing a professional.</p>
                </div>
            </div>
        </div>
        <div class="innerrow">
        	<div class="insiderow">
            	<div class="upgradeimage">
                    <img src="images/specialist/sign.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>Building Control Approval</h2>
                </div>
                <div class="upgradedesc">
                    <p>Our Architectural Team ensure Building<br>Control standards are adhered to during<br>the Design Phase, and your Project<br>Manager will liaise with your assigned<br>inspector during the Build to ensure<br>compliance with Building Regulations.</p>
                </div>
            </div>
            <div class="insiderow">
            	<div class="upgradeimage">
                    <img src="/images/specialist/10.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>Quality Check</h2>
                </div>
                <div class="upgradedesc">
                    <p>We understand quality and always<br>strive to achieve the best. Not many<br>homeowners know the ins and outs<br>of insulation, plasterboard, brickwork<br>(and much more) and it’s our job to<br>make sure it’s all correct.</p>
                </div>
            </div>
            <div class="insiderow">
            	<div class="upgradeimage">
                    <img src="/images/upgradepremium/1.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>Completion Pack</h2>
                </div>
                <div class="upgradedesc">
                    <p>We issue a completion pack for both<br>the Design Phase & Build Phase which<br>includes all of the information you<br>need. We also offer professional<br>photography and a 10 year <br>structural guarantee on our Build Phase.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="moreinfo">
    	<h1>Find Out More</h1>
    </div>   
<div class="bottomshortcut">
	<div class="shortcutinline">
    	<a href="https://www.buildteam.com/about-us/why-choose-page.html ">
            <div class="shortcutwarp">
                <img src="images/specialist/1.jpg" alt="">
                <div class="shortcutdesc"><span>Why Choose Build Team</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/news/press_releases.html">
            <div class="shortcutwarp">
                <img src="images/specialist/2.jpg" alt="">
                <div class="shortcutdesc"><span>Read Our Latest Feature</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/download_our_brochure.html">
            <div class="shortcutwarp">
                <img src="images/specialist/3.jpg" alt="">
                <div class="shortcutdesc"><span>Download Our Brochure</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
        <a href="https://www.buildteam.com/what-we-do/faq.html">
            <div class="shortcutwarp">
                <img src="images/specialist/4.jpg" alt="">
                <div class="shortcutdesc"><span>Design & Build FAQs</span></div>	
            </div>
         </a>
    </div>
</div>
<script src="js/slick.min.js"></script>

<script>
$(".helpmechoose").click(function(){
	$(".quizwrap").animate({left:"0%"});
	});
$(".closebuttonquiz").click(function(){
	$(".quizwrap").animate({left:"-100%"});
	});	
</script>
<script>
$('.slick-slider').slick({
  slidesToShow: 4,
  slidesToScroll: 4,
  autoplay: false,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '10px',
        slidesToShow: 1
      }
    }
	]
});
</script>
<?php require_once('./inc/footer.inc.php'); ?>