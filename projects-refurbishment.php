<?php

require_once('./inc/header.inc.php');
?>
<style>
/* Css for gallery */

.col_half a {
    position: relative;
    width: 100%;
    display: block;
}
.col_half img {
    border: 0;
    /* max-width: 450px !important; */
    width: 100% !important;
	max-width: inherit !important;
}
</style>
<div class="full">

<h1>Building refurbishment in London</h1><br/>

<?php

$i = 1;
$ret = '';
$project_category_code = 'refurbishment';
$project_code = '';

if ( isset($_GET['p']) ) {
	//$project_code = $_GET['p'];
}


$rs = getRs("SELECT c.project_category_code, c.project_category_name, i.filename, p.project_code, p.project_name FROM project_category c INNER JOIN (project_image i INNER JOIN project p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 AND c.project_category_code = '" . formatSql($project_category_code) . "' ORDER BY p.sort, p.project_id, i.sort, i.project_image_id");

$cols = 2;
$project_count = 0;
$prev_count = 0;

while ($row = mysqli_fetch_assoc($rs) ) {
	if ( $i == 1 ) {
		// echo '<div id="bc"><a href="/index.html">Home</a> &rsaquo; <a href="/our-experience.html">Our Experience</a> &rsaquo; <b>' . $row['project_category_name'] . '</b></div>';
	}
		
		if ($project_code != $row['project_code']) {
			$project_code = $row['project_code'];
      $project_count++;
		}
		else {
			continue;
      //$ret .= 'display:none;';
		}
		
    $ret .= '<'.'div class="col_half'. ($i%2==1 ? '' : ' col_last') .'" style="text-align:center';
		
		$ret .= '"><a href="/'.$row['project_code'].'-refurbishment.html" title="' . htmlentities($row['project_name']) . '"><img src="/phpthumb/phpThumb.php?src='.urlencode('/projects/' . $row['filename']) . '&amp;w=450&amp;h=400" style="width:100%;max-width:450px" alt="' . htmlentities($row['project_name']) . '" /><span style="display:block;margin-top:5px">'.htmlentities($row['project_name']).'</span></a></div>'; // debug: ' '.$project_count
    
    if (0==($project_count % $cols) && $project_count!=$prev_count) {
      $ret .= '<br clear="all" />'; // debug: ($project_count % $cols).'!!!
      $prev_count = $project_count;
    }    
	$i += 1;
}

echo $ret;

?>

<br clear="all"/>
<p style="text-align:justify">
London homes are constantly in need of refurbishment services because everyone wants to keep their houses up to date, spacious and welcoming and London does encourage a tasteful modern life style. Our construction company in London specialises in London property management and we are ready to answer your call for building refurbishment in London. Our gallery displays some of the stylish house decorating ideas, elegant loft room ideas and novel architectural design used in the refurbishment of various spaces. Our interior designers and house builders work with your existing home design but improve it based on your own requirements. They come up with living room decorating ideas, kitchen extensions, loft conversions plans and always come up with amazing house designs in UK. We want to provide you with house changes, exactly as you envisaged them and aim to have that garage conversion, basement conversion or mansard loft conversion exactly as you dreamed of having it. Building refurbishment in London is what we do best, but we are also open to working on office refurbishment London or hotel refurbishment London. Don’t hesitate to contact us!
</p>

</div>

<?php

require_once('./inc/footer.inc.php');

?>