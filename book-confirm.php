<?php

global $a_settings;
$newflaging = false;
require_once('./inc/settings.php');

@session_start();

if (!isset($_SESSION[WISE_DB_NAME]['book_visit']['product_code'])) {
  header('Location: /getting-started/book-site-visit.html');
  exit();
}

$name = htmlentities(substr($_SESSION[WISE_DB_NAME]['book_visit']['fname'], 0, 150), ENT_QUOTES, 'UTF-8');

$postcode = htmlentities($_SESSION[WISE_DB_NAME]['book_visit']['postcode'], ENT_QUOTES, 'UTF-8');
$couponcode = htmlentities($_SESSION[WISE_DB_NAME]['book_visit']['coupon_code'], ENT_QUOTES, 'UTF-8');
$availibility = $_SESSION[WISE_DB_NAME]['book_visit']['availibility'];
$o_avail = json_decode($availibility);
$a_pref_dates = array();
$availibility = '';
if ($o_avail) {
  foreach ($o_avail AS $k => $v) {
    $k = explode('/', $k);
    $k[2] = substr($k[2], 2, 3);
    $k = $k[1] . '.' . $k[0] . '.' . $k[2];
    $availibility .= $k.' - '.strtoupper($v)."\n";
    $a_pref_dates[$k] = $v;
  }
}
$availibility = trim($availibility);

$sql = "SELECT * FROM visit WHERE visit_id=".(int)$_SESSION[WISE_DB_NAME]['book_visit']['visit_id'];
SQL2Array($sql, $visit);

if (isset($visit[0]['visit_id'])) $visit = $visit[0];
else {
  header('Location: /getting-started/book-site-visit.html');
  exit();
}


//if ('POST'==$_SERVER['REQUEST_METHOD'] && $visit) {
  
  $Message = '';
  
  if ($visit['amount']==0) {
    if ($visit['coupon_code']) {
      $Message = "Zero amount (100% discount coupon)";
    }
    else {
      $Message = "Zero amount (FREE)";
    }
  }
  
  placeVisit($visit, $Message); // update log, used coupon, thank you session, send email(s)
  
  //header('Location: /book-thankyou.html');
  //exit();
  $newflaging = true;
//}

require_once('./inc/header.inc.php');
?>
<style>
body {
	background:url(http://buildteam.com/images/contact/new_sv_bg.jpg) no-repeat center;
	background-size:cover;
	}
.top_part {
    background:rgba(255,255,255,1);
}
	
	.main {
    background: #fff;
	background:rgba(255,255,255,0.9);
	padding-left: 50px !important;
    padding-right: 50px !important;
    box-sizing: border-box !important;
	padding-bottom:50px !important;
	max-width:1080px;
	}
.booksummary {
		
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    border-bottom: 2px solid #d9d9d9 !important;
    
	}
.book-summary {
	border-bottom:none !important;
	}	
.booksummary h4 {
	font-size: 30px;
	color: #003c70;
	font-weight:bold;
	text-align:left;
	}	
.booksummary h4 span {
	font-size: 30px;
	color:#f5641e !important;
	font-weight:normal !important;
	}
.wrapitsum p{
	border-bottom: 1px solid #d9d9d9 !important;
	padding: 5px 0px !important;
    margin: 0px;
	width:auto;
	font-size:18px;
	float:none !important;
	}
.wrapitsum p span {
	vertical-align:top;
	}	
.book-summary p span {
	float:none !important;	
	}	
.book-summary {
	margin-top:0px !important;
	}
.book-summary p .firsspan {
	min-width:139px;
	text-align:left;
	font-weight:bold;
	color: #003c70 !important;
	}
a.book-now:hover {
    opacity: 0.81;
	text-decoration:none !important;
}	
.book-summary p span:nth-child(2) {
	text-align:right;
	min-width:190px;
	}	
.book-summary em {
	float:none !important;
	margin:0 !important;
	display:inline-block !important;
	}
.editdates {
    width: 50%;
    float: right;
    text-align: right;
}
.editdates p {
	padding:0px !important;
	font-size:18px;
	}
.wrapitsum {
    width: 335px;
    float: left;
}
.book-summary .secspan, .book-summary .firsspan {
	display:inline-block;
	}	
.book-summary .secspan{
	display:inline-block !important;
	color:#74787f;
	font-weight:normal;
	}		
.newboobutton {
	text-align:left;
	}	
.newboobutton a {
    background: #6687a6 !important;
    padding: 10px 30px;
    color: #FFF;
    font-size: 18px;
    border-radius: 15px;
    margin-top: 25px;
	height:auto !important;
	width: auto !important;
	text-align:center;
}	
.closethis {
    position: absolute;
    right: 0px;
    top: 10px;
}
.closethis a {
    background: none;
    color: #104674;
    font-size: 36px;
    font-weight: 500;
    line-height: 18px;
}
.closethis a:hover{
	text-decoration:none;
	}
@media (max-width:640px) {
	.book-summary .secspan, .book-summary .firsspan {
		display:inline-block !important;
		}
	.book-summary p span:nth-child(2) {
		text-align:left !important;
		}
	.main {
		padding-left:20px !important;
		padding-right:20px !important;		
		}
	.editdates {
    width: 100% !important;
    float: left !important;
    text-align: left !important;
}			
}				

</style>
  <div class="full pay_now-page">
		
    <div class="col_one book-visit"><!--two_third three_fifth-->
    
      <div id="bc" style="display:none;"><a href="/">Home</a> &rsaquo; <a href="/getting-started/book-site-visit.html">Book a Site Visit</a> &rsaquo; <b>Confirm <?php echo $a_settings['a_visit_types'][  $_SESSION[WISE_DB_NAME]['book_visit']['product_code']  ]['name'] ?> Visit</b>
      </div>
      
      <h1 style="display:none;">Booking Summary : <span><?php echo $a_settings['a_visit_types'][  $_SESSION[WISE_DB_NAME]['book_visit']['product_code']  ]['name'] ?></span></h1>
      <div class="booksummary">
      		<h4>Booking Summary : <span><?php echo $a_settings['a_visit_types'][  $_SESSION[WISE_DB_NAME]['book_visit']['product_code']  ]['name'] ?></span></h4>
      </div>
      <div class="book-wrap-new">
        <form method="POST" id="fm_pay_now" name="Form" action="/book-confirm.html">
      
          <!-------------------------------------------------------------------------->
          
            <div class="book-summary">
              <div class="wrapitsum">
                <p><span class="firsspan"><em>Name: </em></span><span class="secspan"><?php /*Lucas Docherty*/ echo $name ?></span></p>
                <p><span class="firsspan"><em>Postcode: </em></span> <span class="secspan" id="visit_postcode"><?php /*SW4 6DH*/ echo $postcode ?></span></p>
                <p><span class="firsspan"><em>Dates selected: </em></span> <span class="secspan"><?php echo nl2br($availibility);
  /*29.01.15 - AM<br/>
  28.01.15 - PM<br/>
  23.01.15 - AM*/
  ?></span></p>
                <p id="visit_addr" style="display:none"><?php echo htmlentities($visit['addr'], ENT_QUOTES, 'UTF-8') ?></p>
                <p id="visit_addr2" style="display:none"><?php echo htmlentities($visit['addr2'], ENT_QUOTES, 'UTF-8') ?></p>
              
              <div class="">
                
                <!--<p><em>Booking <?php echo $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['name'] ?> Visit</em></p>-->
                
                <p><span class="firsspan"><em>Price: </em></span><span class="secspan" id="price"><?php if (!isset($visit['amount']) || !isset($visit['amount'])): ?>&pound;<?php echo $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat'] ?> (&pound;<?php echo $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price'] ?> + VAT)<?php else: 
                  
                  if ($visit['amount']) {
                    echo '&pound;'.$visit['amount'].' (&pound;'.round($visit['amount']/1.2).' + VAT)';
                  }
                  else {
                    echo 'FREE';
                  }
                  /* if exists take price from visit record */
                  
                  ?>
                  
                  <?php endif; ?></span>
                <span id="init_price">&pound;<?php echo $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat'] ?> (&pound;<?php echo $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price'] ?> + VAT)</span>
                </p>
               
                <?php if (isset($a_settings['coupon_enabled']) && (int)$a_settings['coupon_enabled'] && $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat']) : ?>
                <p class="input"><span class="firsspan"><em>Offer Code: </em></span> <span class="secspan"><input type="text" name="coupon_code" id="coupon_code" value="<?php if (isset($visit['coupon_code'])) echo htmlspecialchars($visit['coupon_code'], ENT_QUOTES, 'UTF-8'); ?>" /></span></p>
                
                <?php endif; ?>
                <?php //if (isset($couponcode['coupon_code'])): ?>
                	<!--<p class="discountapplied" style="text-align:left;">Discount code applied (-£150.00)</p>-->
                <?php //endif; ?>
                <p id="coupon_msg" class="error"></p>
               <!-- <div class="newboobutton">
                <a href="#" class="book-now">Book Now</a>
              </div> -->
                </div>
                
              </div>
              <div class="editdates">
                <p><em><a href="/book-visit.html?type=<?php echo $_SESSION[WISE_DB_NAME]['book_visit']['product_code']; ?>">Edit selected dates/times</a></em></p>
                </div>
              
              
              
              
            </div>
           <div class="clearfix"></div>
          <?php
          //echo '<br clear="all" /><pre>'.print_r($_SESSION[WISE_DB_NAME]['book_visit'], 1).'</pre>';#debug
          ?>
        </form>
      </div>
   
    </div>

    <div class="col_one_third col_last" style="display:none;">
      <div class="v_progress">
          <h3>how it works</h3>
          <div class="step">
            <h4>choose</h4>
            <p>you select dates that are convenient for you</p>
          </div>
          <div class="step">
            <h4>confirm</h4>
            <p>confirmation of your site visit request</p>
          </div>
          <h4>site visit</h4>
          <p>a member of our team will contact you to schedule the visit <?php if ($_SESSION[WISE_DB_NAME]['book_visit']['product_code'] != 'standard') : ?>and arrange payment<?php endif; ?></p>
      </div>
    </div>
    <div class="clearfix"></div>
  </div>
  <div class="bookthanks">
    			<div class="bookthanksinner">
              <div class="lightbox_content">
              <div class="closethis">
                      <a href="http://www.buildteam.com/getting-started/book-site-visit.html">x</a>
                  </div>   
             <h4>Thank you for your booking</h4>
             <p>You should receive a confirmation email within 24 hours. While you wait why not check out our gallery.</p>
             <a href="http://www.buildteam.com/project-gallery/projects-side-return-extensions.html">Ok</a>
            </div>
            </div>
            </div>
<?php

require_once('./inc/footer.inc.php');
  
?>