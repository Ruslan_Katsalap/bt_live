<?php

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    	
			<?php echo $bc_trail; ?>
      
    	<h1>Design Services</h1>
      <p>Sharing our offices with an architect's practice has been most beneficial to many of our contracts that involve awkward planning issues and complicated designs. However most importantly they operate as an in-house design service for Build Team where required, bringing a highly professional addition to our services that most competitors can not compete with.</p>
		</div>
    <div class="right">
    	<img src="/images/construction.gif" width="326" height="436" alt="BuildTeam" />
    </div>
<?php

require_once('./inc/footer.inc.php');

?>