<?php require_once './inc/header.inc.php';?>
<style>
    #aad_payment_options {
        width: 95%;
        margin: -20px 0 0 0;
        float: left;
        padding: 50px 0px 50px 49px;
        background: #5B9BD4;
    }

    #aad_payment_options_inner {
        background: #fff;
        padding: 50px;
        margin: 50px auto;
        width: 50%;
        height: 400px;
    }

    #aad_payment_options p {
        width: 100%;
        float: left;
        margin: 20px 0;
    }

    #aad_payment_options img {
        width: 100px;
        position: absolute;
        margin: 0px 0 0 20px;
    }

    #aad_payment_options span {
        float: left;
        margin: 0 0 0 20px;
    }

    #aad_payment_options strong {
        font-size: 24px;
    }

    #aad_confirm_payment_option span {
        border: 1px solid #000;
        padding: 10px;
        font-size: 24px;
        cursor: pointer;
    }

    #aad_confirm_payment_option span:hover {
        background: #000;
        color: #fff;
    }

    #aad_validate_ccode {
        border: 1px solid #000;
        padding: 5px;
        font-size: 18px;
        cursor: pointer;
    }

    #aad_validate_ccode:hover {
        background: #000;
        color: #fff;
    }

    .main {
        background: #5B9BD4;
    }

    .aad_text_white {
        color: #fff;
    }

    #aad_ccode {
        float: left;
        height: 25px;
    }

</style>
<?php if ($_REQUEST['vid'] != '' && $_REQUEST['vid'] != null && is_numeric($_REQUEST['vid'])) {
    $visit_sql = getRs('SELECT * FROM visit WHERE visit_id=' . (int) $_REQUEST["vid"]);
    $vs = mysqli_fetch_assoc($visit_sql);
    if ($vs) {
        if ($vs['amount'] != '0' && $vs['amount'] != null && $vs['amount'] != '') {
            //echo "<pre>";
            //var_dump($vs);
            //echo "</pre>"; ?>
<h1 class="aad_text_white" style="text-align:center;">Thank you for your request to book a [<?=$vs["visit_type"]; ?>] Site Visit.</h1>
<h1 class="aad_text_white" style="text-align:center;">The cost of this visit is [Â£<?=$vs["amount"]; ?> inc VAT]</h1>
<div id="aad_payment_options">
    <div id="aad_payment_options_inner">
        <h1>Payment Options</h1>
        <p><input type="radio" name="aad_payment_options" value="paypal" /> <img
                src="https://buildteam.com/img/paypal-logo.png" /></p>
        <p><input style="float:left;" name="aad_payment_options" value="bacs" type="radio" /> <span><strong>Bacs
                    transfer</strong><br> (we will send you an invoice)</span></p>
        <p><input style="float:left;" <?php if ($_REQUEST['ccode']=='' || $_REQUEST['ccode']!='') {
                echo "checked";
            } ?>
            name="aad_payment_options" value="ccode" type="radio" /> <span><input type="text"
                    value="<?=$_REQUEST['ccode']; ?>"
                    name="aad_ccode" id="aad_ccode" /> <span id="aad_validate_ccode">Validate</span>

                <?php
if (isset($_REQUEST['ccode']) && ($_REQUEST['ccode']=='' || $_REQUEST['ccode']!='')) {
                $couponsql = getRs('SELECT * FROM coupon WHERE coupon_code="' . $_REQUEST["ccode"] . '" and is_active="1" and is_enabled="1"');
                $cc = mysqli_fetch_assoc($couponsql);
                if ($cc) {
                    ?>
                <span style="color:green;font-size:18px">Coupon Code Validated. </span>
                <input type="hidden" id="hidden_ccode" value="setted" />
                <?php
                } else {
                    ?>
                <span style="color:red;font-size:18px">Invalid Coupon Code</span>
                <input type="hidden" id="hidden_ccode" value="none" />
                <?php
                }
            } else {
                ?>
                <input type="hidden" id="hidden_ccode" value="none" />
                <?php
            } ?>

        </p>
        <p id="aad_confirm_payment_option"><span>Confirm</span></p>
    </div>
</div>
<form id="vppf" style="display:none;" action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post">
    <input type="hidden" name="cmd" value="_xclick">
    <input type="hidden" name="business" value="adahlawat91-facilitator@gmail.com">
    <input type="hidden" name="item_name"
        value="<?php echo $vs['visit_type']; ?>">
    <input type="hidden" name="item_number"
        value="<?php echo $vs['visit_id']; ?>">
    <input type="hidden" id="aad_paypal_amount" name="amount"
        value="<?php echo $vs['amount']; ?>">
    <input type="hidden" name="custom"
        value="<?php echo md5($vs['amount']); ?>">
    <input type="hidden" name="no_note" value="1">
    <input type="hidden" name="currency_code" value="INR">
    <input type="hidden" name="cancel_return"
        value="https://buildteam.com/getting-started/book_visit_payment.html?vbc=vcr">
    <input type="hidden" name="return"
        value='https://buildteam.com/getting-started/book_visit_payment.html?vbpd=vbpd&vvid=<?php echo $vs["visit_id"]; ?>'>
    <input type="hidden" name="notify_url"
        value="https://buildteam.com/getting-started/book_visit_payment.html?vpip=bvp">
    <input type="image" src="https://www.paypal.com/en_AU/i/btn/btn_buynow_LG.gif" border="0" name="submit"
        alt="PayPal - The safer, easier way to pay online.">
    <img alt="" border="0" src="https://www.paypal.com/en_AU/i/scr/pixel.gif" width="1" height="1">
</form>
<script type="text/javascript">
    $(document).ready(function() {
        jQuery("#aad_validate_ccode").click(function() {
            var eccode_val = jQuery("#aad_ccode").val();
            window.location.href =
                "https://buildteam.com/getting-started/book_visit_payment.html?vid=1332&ccode=" +
                eccode_val;
        });
        $("#aad_confirm_payment_option").click(function() {
            var radioValue = $("input[name='aad_payment_options']:checked").val();
            if (radioValue == "paypal") {
                jQuery("#vppf").submit();
            }
            if (radioValue == "bacs") {
                window.location.href =
                    'https://buildteam.com/getting-started/book_visit_payment.html?vbpo=bacs&vvid=<?=$vs["visit_id"]; ?>&cust=<?=md5($vs['date_created']); ?>';
            }
            if (radioValue == "ccode") {
                var eccode_val = jQuery("#hidden_ccode").val();
                var ccode_val = jQuery("#aad_ccode").val();
                if (eccode_val == "setted") {
                    window.location.href =
                        'https://buildteam.com/getting-started/book_visit_payment.html?vbpo=ccode&vvid=<?=$vs["visit_id"]; ?>&cust=<?=md5($vs['date_created']); ?>&cc=' +
                        ccode_val;
                } else {
                    window.location.href =
                        "https://buildteam.com/getting-started/book_visit_payment.html?vid=1332&ccode=" +
                        ccode_val;
                }
            }
        });
    });

</script>
<?php
        } else {
            echo '<h1 class="aad_text_white"  style="text-align:center;">Thank you for your Site Visit request.
</h1>
<h1 class="aad_text_white"  style="text-align:center;">A member of our team will be in touch shortly to confirm your appointment. </h1>';
        }
    } else {
        echo '<h1 class="aad_text_white"  style="text-align:center;">Something went wrong. Please try again later. </h1>';
    }
}?>
<?php if ($_REQUEST['vpip'] == 'bvp') {
    $payment_status = $_POST['payment_status'];
    $item_number = $_POST['item_number'];
    $item_txn = $_POST['txn_id'];
    $paypal_code = $_POST['custom'];
    $visit_sql = getRs('SELECT * FROM visit WHERE visit_id=' . $item_number);
    $vs = mysqli_fetch_assoc($visit_sql);
    $initial_amount = $vs['amount'];
    $initial_code = md5($initial_amount);
    if ($initial_code == $paypal_code && $payment_status == 'Completed') {
        getRs('UPDATE visit set  is_enabled="16"  WHERE visit_id=' . $item_number);
        // $sql='UPDATE visit set  is_enabled="16"  WHERE visit_id='.$item_number;
        $to = "mloura112@gmail.com";
        $subject = "Book Visit Payment : Successfull";
        $message = "
            <html>
            <head>
            <title>Book Visit</title>
            </head>
            <body>
            <p>payment status : " . $payment_status . " </p>
            <p>Booked Visit Id : " . $item_number . " </p>
            <p>Paypal Transaction Id    : " . $item_txn . "</p>
            </body>
            </html>";
        $headers1 = "MIME-Version: 1.0" . "\r\n";
        $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers1 .= 'Content-type: text/html; charset=iso-8859-1';
        $headers1 .= 'From: <webmaster@buildteam.com>' . "\r\n";
        //$headers .= 'Cc: info@buildteam.com' . "\r\n";
        mail($to, $subject, $message, $headers1);
    }
    if ($initial_code != $paypal_code && $payment_status != 'Completed') {
        $to = "mloura112@gmail.com";
        $subject = "Book Visit Payment : Failed";
        $message = "
            <html>
            <head>
            <title>Book Visit</title>
            </head>
            <body>
            <p>Paypal Transaction Id    : " . $item_txn . "</p>
            </body>
            </html>";
        $headers1 = "MIME-Version: 1.0" . "\r\n";
        $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers1 .= 'Content-type: text/html; charset=iso-8859-1';
        $headers1 .= 'From: <webmaster@buildteam.com>' . "\r\n";
        //$headers .= 'Cc: info@buildteam.com' . "\r\n";
        mail($to, $subject, $message, $headers1);
    }
}?>
<?php if ($_REQUEST['vbpd'] == 'vbpd' && $_REQUEST['vvid'] != '' && $_REQUEST['vvid'] != null) {
    $visit_sql = getRs('SELECT * FROM visit WHERE visit_id=' . $_REQUEST['vvid']);
    $vs = mysqli_fetch_assoc($visit_sql);
    echo '<h1 class="aad_text_white"  style="text-align:center;">Thank you for booking a [' . $vs["visit_type"] . '] Site Visit.
    </h1>
    <h1 class="aad_text_white"  style="text-align:center;">A member of our team will be in touch shortly to confirm your appointment. </h1>';
}?>
<?php if ($_REQUEST['vbc'] == 'vcr') {
    echo '<h1 class="aad_text_white"  style="text-align:center;color:#ff0000">Booking Canceled</h1>';
}
if ($_REQUEST['vbpo'] == 'bacs' && $_REQUEST['vvid'] != '' && $_REQUEST['vvid'] != null) {
    $visit_sql = getRs('SELECT * FROM visit WHERE visit_id=' . $_REQUEST['vvid']);
    $vs = mysqli_fetch_assoc($visit_sql);
    $vtp = md5($vs["date_created"]);
    if ($vtp == $_REQUEST['cust']) {
        getRs('UPDATE visit set  is_enabled="18"  WHERE visit_id=' . $_REQUEST['vvid']);
        $to = "mloura112@gmail.com";
        $subject = "Book Visit Payment : Bacs";
        $message = "
            <html>
            <head>
            <title>Book Visit</title>
            </head>
            <body>
            <p>Payment Option   : Bacs</p>
            </body>
            </html>";
        $headers1 = "MIME-Version: 1.0" . "\r\n";
        $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
        $headers1 .= 'Content-type: text/html; charset=iso-8859-1';
        $headers1 .= 'From: <webmaster@buildteam.com>' . "\r\n";
        //$headers .= 'Cc: info@buildteam.com' . "\r\n";
        mail($to, $subject, $message, $headers1);
        echo '<h1 class="aad_text_white"  style="text-align:center;">Thank you for your Site Visit request.
    </h1>
    <h1 class="aad_text_white"  style="text-align:center;">A member of our team will be in touch shortly to confirm your appointment. </h1>';
    } else {
        echo '<h1 class="aad_text_white"  style="text-align:center;">Something went wrong. Please try again later. </h1>';
    }
}

if ($_REQUEST['vbpo'] == 'ccode' && $_REQUEST['vvid'] != '' && $_REQUEST['vvid'] != null) {
    $couponsql = getRs('SELECT * FROM coupon WHERE coupon_code="' . $_REQUEST["cc"] . '" and is_active="1" and is_enabled="1"');
    $cc = mysqli_fetch_assoc($couponsql);
    if ($cc) {
        $visit_sql = getRs('SELECT * FROM visit WHERE visit_id=' . $_REQUEST['vvid']);
        $vs = mysqli_fetch_assoc($visit_sql);
        $vtp = md5($vs["date_created"]);
        if ($vtp == $_REQUEST['cust']) {
            getRs('UPDATE visit set  is_enabled="20",coupon_code="'.$_REQUEST["cc"].'"  WHERE visit_id=' . $_REQUEST['vvid']);
            $to = "mloura112@gmail.com";
            $subject = "Book Visit Payment : Coupon Code";
            $message = "
                    <html>
                    <head>
                    <title>Book Visit</title>
                    </head>
                    <body>
                    <p>Payment Option   : Coupon Code</p>
                    </body>
                    </html>";
            $headers1 = "MIME-Version: 1.0" . "\r\n";
            $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";
            $headers1 .= 'Content-type: text/html; charset=iso-8859-1';
            $headers1 .= 'From: <webmaster@buildteam.com>' . "\r\n";
            //$headers .= 'Cc: info@buildteam.com' . "\r\n";
            mail($to, $subject, $message, $headers1);
            echo '<h1 class="aad_text_white"  style="text-align:center;">Thank you for your Site Visit request.
            </h1>
            <h1 class="aad_text_white"  style="text-align:center;">A member of our team will be in touch shortly to confirm your appointment. </h1>';
        } else {
            echo '<h1 class="aad_text_white"  style="text-align:center;">Something went wrong. Please try again later. </h1>';
        }
    } else {
        echo '<h1 class="aad_text_white"  style="text-align:center;">Something went wrong. Please try again later. </h1>';
    }
}
?>

<script>
    /*
  $(document).ready(function() {
    $("html").on("contextmenu",function(){
       return false;
    });
});

var exit = true;

$(window).on("keydown",function(e){//begin window on keydown event
    if(e.which === 116){ exit = true; }
    var unicode=e.keyCode? e.keyCode : e.charCode
    if(unicode==123)
    {
        //alert("You are not allowed to use functional keys.");
        return false;
    }
    if(unicode==16)
    {
       // alert("You are not allowed to use functional keys.");
        return false;
    }
    if(unicode==17)
    {
      //  alert("You are not allowed to use functional keys.");
        return false;
    }
    if(unicode==18)
    {
       // alert("You are not allowed to use functional keys.");
        return false;
    }
});
$(window).bind("beforeunload", function() {

    if (exit === true) {
        //return "Are you sure you want to leave?";
    } else {
        return;
    }
});

*/

</script>
<?php  require_once './inc/footer.inc.php';
