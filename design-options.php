<?php   
	require_once('./inc/header.inc.php');
	$curr_url = getCurrentUrl();
	$rs = getRs("SELECT * FROM page WHERE page_code = '{$curr_url}'");
	$res = mysqli_fetch_assoc($rs);
	$pid = $res['page_id'];
	$media = getRs("SELECT * FROM page_media WHERE page_id =".$pid);
	$media_res = mysqli_fetch_assoc($media);
 ?>
	<style type="text/css">
		/*#sales > h1 {
		  background: #d61d13 none repeat scroll 0 0;
		  color: #fff;
		  margin: 35px 0;
		  text-align: center;
		  padding: 0;
		}*/
		#sales > .contact_us {
		  background: #172154 none repeat scroll 0 0;
		  color: #fff;
		  float: left;
		  font-size: 22px;
		  margin: 15px 0;
		  padding: 14px 0;
		  text-align: center;
		  width: 100%;
		}
		#sales .sales_img {
		  float: left;
		  margin-right: 1%;
		}
		#sales .col_half > span > h1 {
		  font-size: 25px;
		}
		.off_tag {
		  color: #f5641e;
		  float: left;
		  font-size: 18px;
		  margin: 15px 0;
		  text-align: center;
		  width: 100%;
		}
		.off_tag > .border_line {
		  border-top: 1px solid #f5641e;
		  margin-top: 20px;
		  position: relative;
		}
		.off_tag > span {
		  background: #fff none repeat scroll 0 0;
		  margin-left: -13%;
		  margin-top: -14px;
		  padding: 0 20px;
		  position: absolute;
		}
		.contact_us > a { color: #fff; }
		.contact_us > a:hover { text-decoration:none; }
		.termsConditions > p {
		  padding: 5px 0 0;
		}
	</style>
	<section id="sales" class="sales_page">
	<h1>Design Options</h1>
		<!--<h1>SALE - Up to 50% off</h1>-->
		<div class="col_full">
			<div class="col_half">
				<div class="sales_img"><img src="../images/interior.png" alt="Interior Design" /></div>
				<span><h1>Interior Design Service</h1>Many people associate ‘interior design’ with a bit of paint or some fluffy cushions and hastily miss the opportunity to really execute the true potential of their home. Although interior design does touch on those two points, it is so much more than a carefully picked colour and a fluffed cushion. The interior design of your home can manipulate all kinds of things including light, sound and temperature.
				
				Our Interior Design Service starts with a full, in-depth consultation. The first stage is to find out what kind of style you want to achieve. Next, we will encourage you to think about what you want from the finished space (ie. lots storage or space for a TV). We will guide you through the process and offer 3D proposals to help you visualise the space you have available.
				<!--<div class="off_tag"><div class="border_line"></div><span>30% OFF</span></div></span>-->
			</div>
			<div class="col_half col_last">
				<div class="sales_img"><img src="../images/landscape.png" alt="Landscape Design" /></div>
				<span><h1>Landscape Design Service</h1>You shouldn’t underestimate the impact your outdoor space will have on your new indoor space, particularly if you are opting for lots of glass and bi-fold doors. Many of our clients choose to redesign their garden while they are redesigning their kitchen extension, mostly because they can ensure both designs will complement each other.
				
				Our Landscape Design Services includes an in-depth consultation with an experienced member of our Design Team. We will send over design and layout options which will show your existing layout and some proposed options. You are then free to amend the proposed options until you have your final design. 
				
				Have a look at our <a href="http://www.buildteam.com/blog/liven-up-your-landscape/" target="_blank">Blog</a> for further info.
				<!--<div class="off_tag"><div class="border_line"></div><span>50% OFF</span></div></span>-->
			</div>
		</div>
		<div class="col_full">
			<div class="col_half">
				<div class="sales_img"><img src="../images/party.png" alt="Party Wall" /></div>
				<span><h1>Party Wall Service</h1>If you are planning to build, it is a legal requirement that you notify your adjoining owners (those within 3 meters of the proposed works). Party Wall Matters can be notorious for being a complicated, lengthy process and for this reason we strongly recommend that you use a professional surveyor to carry out the work. We also recommend that you start the process during the Design Phase, so you can run it alongside planning.
				
				While it can be a lengthy process, no neighbour can stop you carrying out building work. They ultimately have to come to an agreement in the end. Our experienced Party Wall Surveyor is fantastic at bringing lengthy discussions to a close, and finalising all agreements and documentation so that there are no delays to starting the build.
				
				Have a look at our <a href="http://www.buildteam.com/blog/the-party-wall-process/" target="_blank">Blog</a> for further info.
				<!--<div class="off_tag"><div class="border_line"></div><span>25% OFF</span></div></span>-->
			</div>
			<div class="col_half col_last">
				<div class="sales_img"  style="margin-right: 4%;"><img src="../images/fast-track.png" alt="Fast Track" /></div>
				<span><h1>Fast Track Service</h1>For those of you who want to get the Build underway as soon as possible, we offer a specialist service which enables you to book in a build start date before planning has been determined. This service allows us to fast track your structural drawings. 

				Once we have the structural drawings, Following the meeting, we will send over our Detailed Schedule of Works which will cost up your scheme, and include any additional items you might require (ie. first floor bathroom refurb / a specific type of flooring etc). 

				Once you have this Detailed Schedule of Works, you are able to choose if you want to proceed with the Build Phase. Once instructed, we are able to offer a start date within 4 weeks.
				<!--<div class="off_tag"><div class="border_line"></div><span>25% OFF</span></div></span>-->
			</div>
		</div>
		<div class="col_full termsConditions">
			<h1 style="text-transform:capitalize;">T&C's</h1>
			<!--<p>1. These offers cannot be used in conjunction with any other offer. </p>-->
			<p>1. Ancillary services are only available as part of a Design Phase instruction.</p>
			<!--<p>3. The discount for the Party Wall Service applies to one Notice and Schedule of Condition only and can only be used if Build Team is instructed to serve all relevant Notices. </p>-->
		</div>
		<div class="contact_us"><a href="/contact.html">Contact us for more details</a></div>
	</section>
<?php require_once('./inc/footer.inc.php'); ?>