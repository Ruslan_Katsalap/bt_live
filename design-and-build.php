<?php

header('HTTP/1.1 301 Moved Permanently');
header('Location: /what-we-do/design-and-build-page.html');
exit;

require_once('./inc/header.inc.php');



?>

  	<div class="left">

    	

			<?php echo $bc_trail; ?>

      

    

    	<h1>Design and Build from Build Team</h1>

      <p>At Build Team, we offer a turn-key solution for clients who require both an architect and a builder.</p>

      <p>To deliver this service Build Team works closely with their sister company, bdAr Architects and designers (www.bdar.co.uk).</p>

      <p>A typical turn-key project includes the following stages:</p>

      <ul>

      	<li>Design options and planning feasibility</li>

        <li>Planning application preparation and submission</li>

        <li>Detailed schedule of works and cost plan</li>

        <li>Co-ordination of professional team: Structural engineer, party wall surveyor, quantity surveyor (if required), and building control</li>

        <li>On-site construction and project management</li>

      </ul>

      <p>Whatever stage of the building process you are at, we have the skills and expertise to help make the project a success. If you are yet to enter the design phase then we can help make the most out of any building in need of renovation. Once designs are in place then our construction team will take care of the building work, whether it is a residential or commercial property. Our services don’t stop there. Once construction has been completed then we can provide on-going maintenance work and future refurbishment.</p>

      <p>Benefits of our service include:</p>

      <ul>

        <li>Complete service from a single source</li>

        <li>Meticulous interpretation of design brief on site</li>

        <li>Strict adherence to agreed programmes and budgets</li>

        <li>Flexibility to alter designs and specifications to meet the clients requirements</li>

        <li>Responsibility for the total property construction process with control of all design, programme and cost issues under one roof</li>

        <li>Dedicated Project and Design Manager assigned to every project</li>

        <li>Less administrative work for the client leaving you to concentrate on the important things in life</li>

      </ul>

      <p>Build Team are particularly experienced in ground floor extensions to Victorian houses and flats, resulting in a larger open plan living area, bright and light room and an increased property value. <a href="/what-we-do/design-and-build/side-return-extensions.html">Click here</a> to find out more about a <a href="/what-we-do/design-and-build/side-return-extensions.html">Side Return Extension from Build Team</a>.</p>

      <p>If you would like to find out more about our turn-key Design and Build service please contact us on 020 7495 6561 or via email <a href="/contact.html">here</a>.</p>

		</div>

<?php



require_once('./inc/footer.inc.php');



?>