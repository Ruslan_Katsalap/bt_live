<?php require_once('./inc/header.inc.php'); ?>

<?php require_once('save_how_can_help_form.php'); ?>
<link rel="stylesheet" href="js/accordian/jquery.accordion.css" type="text/css">
<link rel="stylesheet" href="js/slidertab/jquery.sliderTabs.css" type="text/css">
<link rel="stylesheet" href="css/quiz.css" type="text/css">

<style>
	.titlehowwecan h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
}	
.howwedesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #74787f;
    line-height: 24px;
    font-weight: normal;
}
.wrapinside {
	text-align:center;
	}
.howcanwetitle h1 {
    font-size: 22px !important;
    padding-bottom: 0px !important;
    margin-bottom: 5px !important;
    letter-spacing: 1px;
}
.howcanwesub p {
    font-weight: bold;
    color: #b4b4b4;
    font-size: 18px;
    padding-top: 10px;
}
.howcanwedesc p {
    font-size: 16px;
    padding-top: 10px;
    color: #b4b4b4;
	line-height:18px;
}
.howcanwebox h1 {
    font-size: 16px !important;
    font-weight: normal;
    margin-bottom: 0px !important;
    padding-top:0px !important;
    padding-bottom: 0px !important;
	color:#86a2bb;
	font-weight:bold;
	text-transform: none !important;
}
.howcanwebox p {
    padding-top: 0px;
    margin-top: 5px;
    color: #b4b4b4;
	font-size:16px;
}
.howcanwebutton {
    padding: 15px 0px;
}
.howcanwebutton a {
    background: #0a4f7c;
    color: #FFF;
    display: inline-flex;
    padding: 10px 50px;
    border-radius: 10px;
    text-align: center;
    margin: 0 auto;
    font-size: 16px;
}
.howcanwebutton {
    padding: 25px 0px;
}
.howcanwebox {
    padding: 25px 0px;
    border-bottom: 1px dashed #b4b4b4;
    border-left: 1px solid #b4b4b4;
	min-height:85px;
}
.howcanwetop {
	border-left:1px solid #b4b4b4;
	border-bottom:1px solid #b4b4b4;
	padding-bottom:15px;
	min-height: 300px;
	padding-top:15px;
	}
.howcanwebutton {
    padding: 25px 0px;
    border-left: 1px solid #b4b4b4;
}	
.innerhowcanwe {
	margin-top:50px;
	}
.inlinehowcanwe {
	float:left;
	width:33.33%;
	}
.lasthowcan .howcanwetop{
	border-right: 1px solid #b4b4b4;
	}
.lasthowcan .howcanwebox , .lasthowcan .howcanwebutton{
    border-right: 1px solid #b4b4b4;
}
.howcanwebutton a strong {
    margin-left: 5px;
}	
.helpmechoose a {
    background: #f2f5f8;
    padding: 15px 31px;
    font-size: 20px;
    color: #0a4f7c;
    letter-spacing: 1px;
}
.helpmechoose a span {
    font-weight: bold;
}
.helpmechoose {
    position: absolute;
    left: -83px;
    transform: rotate(-90deg);
}
.up_down {
	display:none;
	}
.howcanweicon img {
	max-width:100px;
	}	
@media (max-width:768px) {
	.inlinehowcanwe {
    float: none;
    width: 100%;
}
.howcanwetop,.howcanwebox,.howcanwebutton {
	border-left:none;
	}
.lasthowcan .howcanwetop, .lasthowcan .howcanwebox, .lasthowcan .howcanwebutton {
	border-right:none;
	}	
	}	
		
</style>
<div class="howwecanhelp">
    	<div class="titlehowwecan">
        	<h2>Your Build, Your Way</h2>
        </div>
        <div class="howwedesc">
        	<p>We can help in a variety of ways when it comes to the Build Phase, and we’re here to help you choose the avenue best suited to you, your budget, and your build. Every build is unique, and the best option for you may depend on a variety of factors. If you need help choosing a bundle, try our <a class="helpmechooselink" href="javascript:void(0);">questionnaire</a>, or <a href="/contact.html">contact</a> our team.</p>
        </div>
    </div>
<div class="howwecanhelpwrpa">
	<div class="innerhowcanwe">
    	<div class="inlinehowcanwe">
        	<div class="wrapinside">
            	<div class="howcanwetop">
                <div class="howcanweicon">
                    <img src="images/build_phase/1.png" alt="">
                </div>
                <div class="howcanwetitle">
                    <h1>Tender<br>Yourself</h1>
                </div>
                <div class="howcanwesub">
                    <p>Do it yourself</p>
                </div>
                <div class="howcanwedesc"><p>Our Design Phase equips<br>
you with a full tender package,<br>including a Detailed Schedule<br> of Works.</p></div>
            </div>
            <div class="howcanwebox">
                <h1>Tender Package</h1>
                <p>Includes detailed drawings,<br>structural calculations and<br>detailed schedule of works</p>
            </div>
            <div class="howcanwebutton">
                <a href="/do_it_yourself.html">FIND OUT <strong> MORE</strong></a>
            </div>
        </div>
    </div>
    <div class="inlinehowcanwe">
        	<div class="wrapinside">
            	<div class="howcanwetop">
                <div class="howcanweicon">
                    <img src="images/build_phase/2.png" alt="">
                </div>
                <div class="howcanwetitle">
                    <h1>Contract<br>Management</h1>
                </div>
                <div class="howcanwesub">
                    <p>Do it with our help</p>
                </div>
                <div class="howcanwedesc"><p>If you have a contractor in<br>
mind and would like some<br>
professional experience to <br>guide you along the way.</p></div>
            </div>
            <div class="howcanwebox">
                <h1>Contract Administration</h1>
                <p>We help put the contract in<br>place between you and your<br>contractor</p>
            </div>
            <div class="howcanwebox">
                <h1>Inspect and Report</h1>
                <p>Five site visits during the<br>build to inspect the works</p>
            </div>
            <div class="howcanwebutton">
                <a href="/do_it_with_our_help.html">FIND OUT <strong> MORE</strong></a>
            </div>
        </div>
    </div>
    <div class="inlinehowcanwe lasthowcan">
        	<div class="wrapinside">
            	<div class="howcanwetop">
                <div class="howcanweicon">
                    <img src="images/build_phase/3.png" alt="">
                </div>
                <div class="howcanwetitle">
                    <h1>Build With<br>Build Team</h1>
                </div>
                <div class="howcanwesub">
                    <p>Build with us</p>
                </div>
                <div class="howcanwedesc"><p>Let us find the contractor <br>and
project manage<br> your build.</p></div>
            </div>
            <div class="howcanwebox">
                <h1>Project Management</h1>
                <p>We will produce a project
plan, <br>attend site regularly,
certificate <br>payments and help
with any issues which<br> may
arise during the project</p>
            </div>
            <div class="howcanwebox">
                <h1>10 Year Structural Guarantee</h1>
                <p>In addition to a 12 month
warranty<br> for the works, Build
Team will also provide<br> a 10
Year Structural Guarantee</p>
            </div>
            
            <div class="howcanwebox">
                <h1>Project Completion Pack</h1>
                <p>At the end of the project we will<br>
pull together all of the
necessary <br>documentation and
certification which will<br> be
required should you sell the<br>
property in the future</p>
            </div>
            <div class="howcanwebutton">
                <a href="/build_with_us.html">FIND OUT <strong> MORE</strong></a>
            </div>
        </div>
    </div>
</div>  
<div class="helpmechoose">
	<a href="javascript:void(0);"><span>HELP ME</span> CHOOSE</a>
</div>  
<div class="quizwrap">
	<div class="quizinner">
    	<div class="quizesection">
        	<form id="example-advanced-form" action="#">
                <h3></h3>
                <fieldset>
                	<div class="innerfeild">
                    	<div class="fieldtablecell">
                        	 <legend>Have you got planning permission and detailed drawings?</legend>
                                <p>Please pick one to continue</p>
                                <label>
                                <input type="radio" id="opt1" name="planningpermission" value="Yes">
                                <span>Yes</span>
                                </label>
                                <label>
                                <input type="radio" id="opt2" name="planningpermission" value="No">
                                <span>No</span>
                                </label>
                        </div>
                    </div>
                   
                </fieldset>
                
                 <h3></h3>
                <fieldset>
                	<div class="innerfeild">
                    	<div class="fieldtablecell">
                        	 <legend>Who did you use for your drawings? </legend>
                                
                                <label>
                                <input type="radio" name="fordrawing" value="Design Team (Build Team's sister Company)">
                                <span>Design Team (Build Team's sister Company)</span>
                                </label>
                                <label>
                                <input type="radio" name="fordrawing" value="An Architect">
                                <span>An independant Architect</span>
                                </label>
                                <label>
                                <input type="radio" name="fordrawing" value="Other">
                                <span>Other</span>
                                </label>
                        </div>
                    </div>
                   
                </fieldset>
             
                             
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                    <legend>Have you completed a home extension project before?</legend>
                    <label>
                    <input type="radio" name="experience" value="No, this is my first time">
                    <span>No, this is my first time</span>
                    </label>
                    <label>
                    <input type="radio" name="experience" value="Yes, but I wasn't heavily involved in the process">
                    <span>Yes, but I wasn't heavily involved in the process</span>
                    </label>
                    <label>
                    <input type="radio" name="experience" value="IYes, I was very involved in the process and know what to expect">
                    <span>Yes, I was very involved in the process and know what to expect</span>
                    </label>
                   
                    </div>
                    </div>
                </fieldset>
                
                 <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                    <legend>Do you want adequate insurance during the build?</legend>
                    <label>
                    <input type="radio" name="insurance" value="No">
                    <span>No</span>
                    </label>
                    <label>
                    <input type="radio" id="someof1" name="insurance" value="If it keeps cost down, I don't need it">
                    <span>If it keeps cost down, I don't need it</span>
                    </label>
                    <label>
                    <input type="radio" id="yesinsurance2" name="insurance" value="Yes">
                    <span>Yes</span>
                    </label>
                    
                    </div>
                    </div>
                </fieldset>
             
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>Where do you intend to live during the build?</legend>										  					<label>
                     <input id="1no" type="radio" name="live" value="In the property">
                     <span>In the property</span>
                    </label>
                    
                   <label>
                    <input id="2no" type="radio" name="live" value="I'll move out for the worst of it">
                    <span>I'll move out for the worst of it</span>
                   </label>
                   <label> 
                    <input id="3yes" type="radio" name="live" value="I'll move out for the entire build">
                    <span>I'll move out for the entire build</span>
                   </label>
                   </div>
                   </div> 
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>What level of involvement would you like during the Build?</legend>										  					
                     <label>
                     <input type="radio" name="involvement" id="involvment1" value="None at all, I'm very busy and just want to be updated of progress">
                     <span>None at all, I'm very busy and just want to be updated of progress</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment2" value="Some involvement, I'd like to visit site once a week to see how it's going">
                    <span>Some involvement, I'd like to visit site once a week to see how it's going</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment3" value="Lots of involvement, I have time to manage the project day to day">
                    <span>Lots of involvement, I have time to manage the project day to day</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment4" value="I live abroad, so I won't be around at all">
                    <span>I live abroad, so I won't be around at all</span>
                    </label>
                    </div>
                    </div>
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>What is your budget?</legend>										  					
                     <label>
                     <input type="radio" name="budget" id="budget1" value="Less than £30,000">
                     <span>Less than £30,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget2" value="Between £30,000 and £50,000">
                    <span>Between £30,000 and £50,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget3" value="Between £50,000 and £100,000">
                    <span>Between £50,000 and £100,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget4" value="Between £100,000 and £150,000">
                    <span>Between £100,000 and £150,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget5" value="Over £100,000">
                    <span>Over £150,000</span>
                   </label>
                   </div>
                   </div>
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>Our Recommendation</legend>										  					
                     
                     <div class="ourrecomendationdiv selectedreco">
                     	<div class="leftreco">
                        	<p id="reccomendation"></p>
                        </div>
                        <div class="descreco">
                        	<p>Sit back and relax! We will take care of everything. You’ll be assigned one dedicated Project Manager who will keep you updated throughout the process and ensure things are running smoothly. They’ll be onsite most days to ensure things are pushing along nicely. We have public liability insurance and a 10 year guarantee, so you have absolute reassurance both during and after the build is complete.</p>
                        	<!--<div class="detailereco">
                            	<h2>• 10 Year Guarantee</h2>
                                <p>we offer a 10 year guarantee</p>
                            </div>
                            <div class="detailereco">
                            	<h2>• Project Management</h2>
                                <p>we handle all of the hassle</p>
                            </div>
                            <div class="detailereco">
                            	<h2>• Public Liability Insurance</h2>
                                <p>giving you peace of mind</p>
                            </div>-->
                        </div>
                        <div class="rightreco">
                        	<a href="#">FIND OUT <span>MORE</span></a>
                        </div>
                        
                     </div>
                    <div class="otherbundles">
                    	<p>Our other bundles:</p>
                    </div> 
                     <div class="ourrecomendationdiv otherbudles">
                     	<div class="leftreco">
                        	<div class="inlineone"><p>• <span id="otherbudle2">Do it with our help</span> </p>
                            	<div id="otherllink2" class="linkright">
                                	<a href="#">FIND OUT <span>MORE</span></a>
                                 </div>
                            </div>
                        </div>
                     </div>
                     <div class="ourrecomendationdiv otherbudles">
                     	<div class="leftreco">
                        	<div class="leftreco">
                        	<div class="inlineone">
                            <p>• <span id="otherbudle3">Build with us</span></p>
                            	<div id="otherllink3" class="linkright">
                                	<a href="#">FIND OUT <span>MORE</span></a>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                     </div>
                     <!--div class="recoprefer">
                     	<p>Prefer to speak with a member of the team?</p>
                        <a href="#" class="request-call">REQUEST A CALL BACK</a>
                     </div-->
                     </div>
                     </div>
                </fieldset>
                
            </form>
        </div>
        <div class="closebuttonquiz"><i>CLOSE AND RETURN TO THE SITE</i> <span>x</span></div>
        <div class="backtostepone"><img src="/images/build_phase/Back-Button.png" alt=""><i>TAKE ME BACK TO THE BEGINNING</i></div>
    </div>
</div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script src="js/jquery.steps.min.js"></script>
<script>
	var form = $("#example-advanced-form").show();
 
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
	transitionEffectSpeed:30,
    onStepChanging: function (event, currentIndex, newIndex)
    {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex)
        {
            return true;
        }
        // Forbid next action on "Warning" step if the user is to young
        if (newIndex === 3 && Number($("#age-2").val()) < 18)
        {
            return false;
        }
        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex)
        {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
		
		//else if (currentIndex === 0 && (!$('#opt1').is(':checked'))) return false;
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
		//return true;
    },
    onStepChanged: function (event, currentIndex, priorIndex)
    {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
        {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3)
        {
            form.steps("previous");
        }
		if (currentIndex === 7 ){
			
			$(document).find(".actions").hide();
			$(document).find(".steps").hide();
			$(".backtostepone").fadeIn(300);
			$(".backtostepone").click(function(){
				//form.steps("setStep", 1);
				$(this).fadeOut(300);
				$( "#example-advanced-form" ).steps('reset');
				$(document).find(".actions").show();
				$(document).find(".steps").show();
				});
			}
		if (currentIndex === 1 && $('#opt2').is(":checked")) {
			steping = 1;
			for(var i=0; i<steping; i++) {
				console.log(i);
					form.steps("next");
				}
			} 
				
		if (currentIndex === 1 && $('#opt2').is(":checked")) {
			//form.steps("previous");
			$('#opt2').prop('checked', false);
			//form.steps("previous");
		}
		
		if ($("#yesinsurance2").is(":checked")) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if ($("#involvment1").is(":checked") || $("#involvment4").is(":checked")) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}	
		if ($("#yesinsurance2").prop('checked') == false && $("#involvment2").is(":checked")) {
			$("#reccomendation").text("Do it with our help");
			$(".rightreco a").attr("href", "/do_it_with_our_help.html");
			$("#otherbudle2").text("Do it yourself");
			$("#otherbudle3").text("Build with us");
			$("#otherllink2 a").attr("href", "/do_it_yourself.html");
			$("#otherllink3 a").attr("href", "/build_with_us.html");
			}	
		if ($("#yesinsurance2").prop('checked') == false && $("#involvment3").is(":checked")) {
			$("#reccomendation").text("Do it yourself");
			$(".rightreco a").attr("href", "/do_it_yourself.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Build with us");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/build_with_us.html");
			}
		if (($("#involvment2").prop('checked') == true && $("#budget3").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget3").is(":checked")) || 
			($("#involvment2").prop('checked') == true && $("#budget4").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget4").is(":checked")) || 
			($("#involvment2").prop('checked') == true && $("#budget5").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget5").is(":checked"))
			) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if (($("#involvment2").prop('checked') == true && $("#budget1").is(":checked") && $("#yesinsurance2").prop('checked') == false)) {
			$("#reccomendation").text("Do it with our help");
			$(".rightreco a").attr("href", "/do_it_with_our_help.html");
			$("#otherbudle2").text("Build with us");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/build_with_us.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if (($("#involvment3").prop('checked') == true && $("#budget2").is(":checked") && $("#yesinsurance2").prop('checked') == false)) {
			$("#reccomendation").text("Do it yourself");
			$(".rightreco a").attr("href", "/do_it_yourself.html");
			$("#otherbudle2").text("Build with us");
			$("#otherbudle3").text("Do it with our help");
			$("#otherllink2 a").attr("href", "/build_with_us.html");
			$("#otherllink3 a").attr("href", "/do_it_with_our_help.html");
			}								
	
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
		
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
		
    }
}).validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        planningpermission: {
		   required: true
		}
		,budget: {
			required: true
			},
		involvement: {
			required: true
			},
		live: {
			required: true
		},
		insurance: {
			required: true
		},
		experience: {
			required: true
		},
		fordrawing: {
			required: false
		}	
    },
	messages: {
		planningpermission: {
		  //minlength: jQuery.format("Zip must be {0} digits in length"),
		  //maxlength: jQuery.format("Please use a {0} digit zip code"),
		  required: "Please select an option to continue."
		},
		budget: {
			required: "Please Select option to continue"
			},
		involvement: {
			required: "Please Select option to continue"
			},
		live: {
			required: "Please select an option to continue."
			},
		insurance: {
			required: "Please select an option to continue."
				},	
		experience: {
			required: "Please select an option to continue."
			},	
		fordrawing: {
			required: "Please select an option to continue."
			}	
		  }
});

	//alert(currentIndex + " ok");
	/*$('a[href="#next"]').click(function(){
		
		function checkindex(event, currentIndex, newIndex){
			alert(currentIndex + " ok");
			}
		
		});
		if (currentIndex === 2 && $('#opt2').is(":checked")) {
			
				$('#opt2').prop('checked', false);
				steping2 = 2;
				
				for(var i=0; i<steping2; i++) {
					console.log(i);
					form.steps("previous");
				}
				
				}*/

	
	

 //$("#example-advanced-form-p-1").hide();
 //$("#example-advanced-form").steps("remove", Number(1));
 


</script>
<script>
$(".helpmechoose").click(function(){
	$(".quizwrap").animate({left:"0%"});
	});
$(".helpmechooselink").click(function(){
	$(".quizwrap").animate({left:"0%"});
	});	
$(".closebuttonquiz").click(function(){
	$(".quizwrap").animate({left:"-100%"});
	});	
</script>
<?php require_once('./inc/footer.inc.php'); ?>