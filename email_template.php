<?php

require_once ('./inc/util.inc.php');

//$qid = 5189; //debug

if (!isset($qid)) die("Access denied!");

$cgi_url = "./images/cgi1.jpg";

$admin_email = '';
$design_discount = 0;
$design_cost = 0;
$offer_exp_days = '';
$url_images = '';
$is_notify_admin = '';
$poa_message = '';

if (isset($a_cfg)) {
  $row = &$a_cfg;
}
else {
  $rs = getRs("SELECT admin_email, design_discount, design_cost, offer_exp_days, url_images, is_notify_admin, poa_message, notification_message, territory_options FROM setting WHERE setting_id = 1");

  $row = mysqli_fetch_assoc($rs);
  
  $row['territory_options'] = unserialize($row['territory_options']);
}

$end_notes = '';

if (strpos($row['notification_message'], '<div style="page-break-after:')) {

  $row['notification_message'] = preg_split('/\<div style\="page\-break\-after\:.+?\<\/div\>/s', $row['notification_message']);
  
  //die(print_r($row['notification_message'],1));#debug
  
  if (isset($row['notification_message'][1])) $end_notes = $row['notification_message'][1];
}

$territory = array(); // all available territories
$active_territory = 0; // index of matched by postpart territory or zero index territory (base) if not found

if ($row) {

	$admin_email = $row['admin_email'];
  $design_discount = $row['design_discount'];
  $design_cost = $row['design_cost'];
  $offer_exp_days = $row['offer_exp_days'];
  $url_images = $row['url_images'];
  $is_notify_admin = $row['is_notify_admin'];
  $poa_message = $row['poa_message'];
  
  $territory = $row['territory_options'];
  
}

if (isset($a_rfq)) {
  $row = &$a_rfq;
}
else {
  $rs = getRs("SELECT * FROM rfq WHERE rfq_id = {$qid}");
  $row = mysqli_fetch_assoc($rs);
}
if ($row) {
  
  if ($territory) {
    $postpart = $row['postcode'];
    $postpart = str_replace(' ', '', $postpart);
    if (strlen($postpart)<6) $postpart = substr($postpart, 0, 2);
    elseif (strlen($postpart)==6) $postpart = substr($postpart, 0, 3);
    else $postpart = substr($postpart, 0, 4);
    
    foreach ($territory AS $k => $v) {
      $v['postcodes'] = explode(';', $v['postcodes']);
      foreach ($v['postcodes'] AS $k2 => $v2) {
        $v['postcodes'][$k2] = trim($v2);
      }
      
      if (in_array($postpart, $v['postcodes'])) {
        $active_territory = $k;
        break;
      }
      
    }
    
    if ($active_territory) $poa_message = $territory[$active_territory]['poa_message'];
    
  }

	$quote_type_id = $row['quote_type_id'];

	$rfq_code = $row['rfq_code'];

	$name = $row['name'];
  $name = explode(' ', $name);
  $name = $name[0];

	$email = $row['email'];

	$room_size = $row['room_size'];

	list($subcategory_1_component_name, $subcategory_1_component_image, $subcategory_1_width, $subcategory_1_height) = getComponent($row['subcategory_1_component_id'], true);

	list($subcategory_2_component_name, $subcategory_2_component_image, $subcategory_2_width, $subcategory_2_height) = getComponent($row['subcategory_2_component_id'], true);

	list($subcategory_3_component_name, $subcategory_3_component_image, $subcategory_3_width, $subcategory_3_height) = getComponent($row['subcategory_3_component_id'], true);

	list($subcategory_4_component_name, $subcategory_4_component_image, $subcategory_4_width, $subcategory_4_height) = getComponent($row['subcategory_4_component_id'], true);

	list($subcategory_5_component_name, $subcategory_5_component_image, $subcategory_5_width, $subcategory_5_height) = getComponent($row['subcategory_5_component_id'], true);

	list($subcategory_6_component_name, $subcategory_6_component_image, $subcategory_6_width, $subcategory_6_height) = getComponent($row['subcategory_6_component_id'], true);

	$subcategory_1_cost = $row['subcategory_1_cost'];

	$subcategory_2_cost = $row['subcategory_2_cost'];

	$subcategory_3_cost = $row['subcategory_3_cost'];

	$subcategory_4_cost = $row['subcategory_4_cost'];

	$subcategory_5_cost = $row['subcategory_5_cost'];

	$subcategory_6_cost = $row['subcategory_6_cost'];

  $amount_quote  = $row['amount_quote'];
  
  $amount_quote_2 = $row['amount_quote_2'];
  
  $date_modified = $row['date_modified'];
  
	$rs_cgi = getRs("SELECT image FROM cgi WHERE subcategory_1_component_id = '" . $row['subcategory_1_component_id'] . "' AND subcategory_2_component_id = '" . $row['subcategory_2_component_id'] . "' AND subcategory_3_component_id = '" . $row['subcategory_3_component_id'] . "' AND subcategory_4_component_id = '" . $row['subcategory_4_component_id'] . "'");
	
	$design_total = $amount_quote_2-abs($amount_quote);
	
	if ($row_cgi = mysqli_fetch_assoc($rs_cgi)) {
	
		$cgi_url = "/" . $url_images . $row_cgi['image'];
		$cgi_fn = $row_cgi['image'];
	
	}
	
	else {
	
		$cgi_url = "/images/cgi1.jpg";
		$cgi_fn = "cgi1.png";
	
	}	
	

	$email_subject = "Build Team: BuildYourPrice Quote Ref: {$rfq_code}";

}

function getComponent($id, $get_image=false) {

	$rs = getRs("SELECT component_name, image, image_w, image_h FROM component WHERE component_id = {$id}");

	if ($row = mysqli_fetch_assoc($rs)) {

		if (!$get_image) return $row['component_name'];
    else return array($row['component_name'], $row['image'], $row['image_w'], $row['image_h']);

	}

}

?>
<!DOCTYPE html>
<html xmlns:v="urn:schemas-microsoft-com:vml">
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=edge" />
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>
<body style="background-color:white;color:black;font-family:Arial,Helvetica,sans-serif;font-size:12px;text-align:center">
  <div style="margin:auto;max-width:934px;width:100%;text-align:left">
    <table width="100%" cellpadding="0" cellspacing="0" border="0">
      <tr>
        <td width="100%">
          
          <table cellpadding="0" cellspacing="0" border="0" width="100%">
          <tr>
            <td width="50%">
              <div style="background-color:#d2d9e5;float:left;width:100px;height:100px;padding:10px;text-align:center">
            <img src="/images/byp-logo.png" alt="BuildYourPrice&trade; Logo" style="width:80px;" /></div>
            </td>
            <td width="50%" align="right">
              <img src="/images/logo_new3.png" alt="BuildTeam Powered" style="margin-top:2px;width:230px" hspace="10" width="230" />
            </td>
          </tr>
          </table>
          
        </td>
      </tr>
    </table>
    <br/>
    <!-- background="/images/byp/bg_hline1_email.png" background-image:url(/images/byp/bg_hline1_email.png); -->
    <table width="100%" cellpadding="5" cellspacing="0" border="0" bgcolor="#f79250" style="color:white;font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="70%" style="padding-left:10px">
          <!--img src="/images/byp/bg_hline1_email.png" alt="Mask Background 1" width="1" height="1" style="width:1px;height:1px" /-->
        </td>
        <td width="30%" align="right" style="padding-right:10px"><strong>QUOTE REF: <?php echo $rfq_code ?></strong></td>
      </tr>
    </table>
    <br/>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="100%" style="padding-left:20px;padding-right:20px">
           Dear <?php echo $name ?>,<br/>
           <br/>
           Thank you for using our online price calculator.  I have pleasure in confirming your quotation based on your individual requirements.  I hope the information provided is useful, and please do not hesitate to contact me should you wish to arrange a FREE, no obligation visit to your property.
        </td>
      </tr>
    </table>
    <br/>
    <!-- background="/images/byp/bg_hline2_email.png" background-image:url(/images/byp/bg_hline2_email.png); -->
    <table width="100%" cellpadding="5" cellspacing="0" border="0" bgcolor="#86888f" style="color:white;font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="70%" style="padding-left:10px">STAGE 0: DESIGN &amp; STRUCTURAL ENGINEERING PHASE
          <!--img src="/images/byp/bg_hline2_email.png" alt="Mask Background 2" width="1" height="1" style="width:1px;height:1px" /-->
        </td>
        <td width="30%" align="right" style="padding-right:10px"><strong>COST: &pound;<?php echo number_format($design_total, 2) ?></strong></td>
      </tr>
    </table>

    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="70%" style="padding-top:10px;padding-bottom:10px;padding-left:20px;padding-right:20px">
          THIS STAGE INCLUDES:<br/>
          Measured Survey<br/>
          Production of Design Options<br/>
          Planning Application<br/>
          Structural Engineers Assessment<br/>
          Detailed Schedule of Works, line-priced by Build Team<br/>
          Party Wall Matters (Additional Fee)
        </td>
        <td width="30%" style="font-size:14px">
          <!--
          <p style="margin:0;padding:0;font-size:18px">
          Save &pound;<?php echo number_format($design_discount); ?>!
          </p>
          We have reduced the Design cost from
          &pound;<?php echo number_format($design_total+$design_discount); ?> to &pound;<?php echo number_format($design_total); ?> + VAT for instructions
          received by <?php echo $offer_exp_days; ?>.
          -->
        </td>
      </tr>
    </table>
    <br/>
    <!-- background="/images/byp/bg_hline2_email.png" background-image:url(/images/byp/bg_hline2_email.png); -->
    <table width="100%" cellpadding="5" cellspacing="0" border="0" bgcolor="#86888f" style="color:white;font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="100%" style="padding-left:10px;padding-right:10px">STAGE 1:  CONSTRUCTION OF CORE SHELL</td>
      </tr>
    </table>
    <br/>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="30%" style="padding-bottom:10px;padding-left:20px;padding-right:20px">
          YOU SPECIFIED:<br/>
          <?php echo $room_size ?> sqm room<br/>
          <?php echo $subcategory_3_component_name; ?> to be removed
        </td>
        <td width="70%">
          <?php if ($subcategory_3_component_image) : ?>
          <img src="/<?php echo $url_images.$subcategory_3_component_image ?>" hspace="10" align="left" alt="Wall" />
          <?php endif; ?>
        </td>
      </tr>
    </table>
    <br/>
    <!-- background="/images/byp/bg_hline2_email.png" background-image:url(/images/byp/bg_hline2_email.png); -->
    <table width="100%" cellpadding="5" cellspacing="0" border="0" bgcolor="#86888f" style="color:white;font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="100%" style="padding-left:10px;padding-right:10px">STAGE 2: EXTERIOR FIT OUT</td>
      </tr>
    </table>
    <br/>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="30%" style="padding-bottom:10px;padding-left:20px;padding-right:20px">
          YOU SPECIFIED:<br/>
          <?php echo $subcategory_2_component_name; ?><br/>
          <?php echo $subcategory_1_component_name; ?>
        </td>
        <td width="70%">
          <?php if ($subcategory_2_component_image) : ?>
          <img src="/<?php echo $url_images.$subcategory_2_component_image ?>" align="middle" <?php if ($subcategory_2_width > $subcategory_2_height) echo 'width="140"'; else echo 'height="81"'; ?> hspace="10" alt="Roof" />
          <?php endif; ?>&nbsp;
          <?php if ($subcategory_1_component_image) : ?>
          <img src="/<?php echo $url_images.$subcategory_1_component_image ?>" align="middle" <?php if ($subcategory_1_width > $subcategory_1_height) echo 'width="140"'; else echo 'height="81"'; ?> hspace="10" alt="Doors" />
          <?php endif; ?>
        </td>
      </tr>
    </table>
    <br/>
    <!-- background="/images/byp/bg_hline2_email.png" background-image:url(/images/byp/bg_hline2_email.png); -->
    <table width="100%" cellpadding="5" cellspacing="0" border="0" bgcolor="#86888f" style="color:white;font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="100%" style="padding-left:10px;padding-right:10px">STAGE 3: INTERIOR FIT OUT</td>
      </tr>
    </table>
    <br/>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="30%" style="padding-bottom:10px;padding-left:20px;padding-right:20px">
          YOU SPECIFIED:<br/>
          <?php echo $subcategory_5_component_name; ?> Chimney Breast<br/>
          <?php echo $subcategory_6_component_name; ?><br/>
          <?php echo 'Not Required'!=$subcategory_4_component_name?$subcategory_4_component_name:'Ground Floor WC is not required'; ?>
        </td>
        <td width="70%">
          <?php if ($subcategory_5_component_image) : ?>
          <img src="/<?php echo $url_images.$subcategory_5_component_image ?>" align="middle" <?php if ($subcategory_5_width > $subcategory_5_height) echo 'width="140"'; else echo 'height="81"'; ?> hspace="10" alt="Kitchen" />
          <?php endif; ?>&nbsp;
          <?php if ($subcategory_6_component_image) : ?>
          <img src="/<?php echo $url_images.$subcategory_6_component_image ?>" align="middle" <?php if ($subcategory_6_width > $subcategory_6_height) echo 'width="140"'; else echo 'height="81"'; ?> hspace="10" alt="Heating" />
          <?php endif; ?>&nbsp;
          <?php if ($subcategory_4_component_image) : ?>
          <img src="/<?php echo $url_images.$subcategory_4_component_image ?>" align="middle" <?php if ($subcategory_4_width > $subcategory_4_height) echo 'width="140"'; else echo 'height="81"'; ?> hspace="10" alt="Floor" />
          <?php endif; ?>
        </td>
      </tr>
    </table>
    <br/>
    <!-- background="/images/byp/bg_hline2_email.png" background-image:url(/images/byp/bg_hline2_email.png); -->
    <table width="100%" cellpadding="5" cellspacing="0" border="0" bgcolor="#86888f" style="color:white;font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="70%" style="padding-left:10px">TOTAL CONSTRUCTION PHASE (Quote valid for 60 days)</td>
        <td width="30%" align="right" style="padding-right:10px"><strong>COST: <?php if ($amount_quote>0) echo '&pound;'.number_format($amount_quote, 2); else echo '<i>Please call</i> *' ?></strong></td>
      </tr>
    </table>
    
    <?php if ($amount_quote>0) : ?>
    <div style="float:left:width:100%;text-align:right">
      <p style="float:right;font-size:9px;margin:0;padding:0;margin-top:3px">
      Subject to site survey and relevant permissions. Excludes VAT</p>
    </div>
    <br/><br/>
    <?php else: ?>
    <div style="float:left:width:100%;text-align:right">
    <?php echo '* '.str_replace('™', '&trade;', $poa_message); ?>
    </div>
    <?php endif; ?>
    
    <table width="100%" cellpadding="0" cellspacing="0" border="0" style="font-family:Arial,Helvetica,sans-serif;font-size:12px">
      <tr>
        <td width="50%" style="padding-left:20px;padding-right:20px" valign="top">
          <?php if (isset($territory[$active_territory]['pdf_signature'])) echo trim($territory[$active_territory]['pdf_signature'], " \t\n\r<p>/"); ?>
          <h2 style="color:#f5641e;margin-top:32px;margin-bottom:0;padding-bottom:0">frequently asked questions</h2>
          
         </td>
         <td width="50%">
            <img src="/images/byp/this_cgi_email.png" alt="This is a computer-generated image of how your extension might look!" /><br/>
            <img src="<?php echo $cgi_url ?>" alt="Room" width="407" style="width:407px" />
         </td>
      </tr>
    </table>
    <div style="padding-left:20px;padding-right:20px">
    <?php echo $end_notes; ?>
    </div>
  </div>
<body>
</html>