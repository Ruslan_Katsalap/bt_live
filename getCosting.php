<?php

//sleep(3); //debug

require ('./inc/util.inc.php');
/**************This is CRM integration code*****************/
require_once($_SERVER["DOCUMENT_ROOT"]."/crm_config.php");

if(isset($_POST['name']) && isset($_POST['phone'])) {
 $now = strftime("%Y-%m-%dT%H:%M:%S");
 $comment.= "\r\nStart Design: ".$_POST['start'];
 $comment.= "\r\nStatus: ".$_POST['status'];
 $comment.= "\r\n room_length: ".$_POST['room_length'];
 $comment.= "\r\n room_width: ".$_POST['room_width'];
 $comment.= "\r\n room_size: ".$_POST['room_size'];
 $comment.= "\r\n measure_unit_id: ".$_POST['measure_unit_id'];  
 $comment.= "\r\n use_size_estimator: ".$_POST['use_size_estimator'];
 $comment.= "\r\n bedrooms: ".$_POST['bedrooms'];
 $comment.= "\r\n terrace_type: ".$_POST['terrace_type'];
 $comment.= "\r\n cb_roof: ".$_POST['cb_roof'];
 $comment.= "\r\n cb_wall: ".$_POST['cb_wall'];
 $comment.= "\r\n cb_door: ".$_POST['cb_door'];
 $comment.= "\r\n cb_kitchen: ".$_POST['cb_kitchen'];
 $comment.= "\r\n cb_heating: ".$_POST['cb_heating'];
 $comment.= "\r\n userid: ".$_POST['userid'];

 $data = array(
    "NAME" =>  $_POST['name'],
    "LAST_NAME" => $_POST['surname'],
    "UF_CRM_1490175334" => $now,
    "UF_CRM_1500629522" => 146,
    "EMAIL_HOME" => $_POST['email'],
    "PHONE_HOME" =>$_POST['phone'],
    "SOURCE_DESCRIPTION" => "BT",
    "ADDRESS"=> $_POST['house'],
    "UF_CRM_1506432431" => $_POST['postcode'],
    "TITLE" =>$_POST['name'].' '.$_POST['surname'],
     "UF_CRM_1506432178" => $_POST['message'],
     "COMMENTS" =>  $comment
  );
 CrmClient::$SOURCE_ID = 2;

 if(!CrmClient::sendToCRM($data)) {
      echo 'error send to crm';
 }
/**************This is CRM integration code*****************/

}

if ('POST'!=$_SERVER['REQUEST_METHOD']) die('status=error&err_msg=Parameters missed');

//debug
$debug = false;
if ($debug) {
  $fp = fopen('report.txt', 'a');
  fwrite($fp, print_r($_POST,1)."\n\n");
}
//print_r($_POST);
if (!isset($_POST['room_length']) || !isset($_POST['room_width']) || !isset($_POST['room_size']) || !isset($_POST['use_size_estimator']) || !isset($_POST['measure_unit_id']) || !isset($_POST['property_type']))
die('status=error&err_msg=Missed Room parameters');

$room_length = (float)$_POST['room_length'];

$room_width = (float)$_POST['room_width'];

$room_size = (float)$_POST['room_size'];

$use_size_estimator = $_POST['use_size_estimator'];

$measure_unit_id = $_POST['measure_unit_id'];

$property_type = substr($_POST['property_type'], 0, 7);

if ('house'!=$property_type && 'flat'!=$property_type) $property_type = '';

$a_component = array(
  'cb_door' => 1,     // subcategory_1_component_id
  'cb_roof' => 2,     // subcategory_2_component_id
  'cb_wall' => 3,     // subcategory_3_component_id
  'cb_floor' => 4,    // subcategory_4_component_id
  'cb_kitchen' => 5,  // subcategory_5_component_id
  'cb_heating' => 6   // subcategory_6_component_id
);

$missed = false;

foreach ($a_component AS $k => $v) {
  if (!isset($_POST['subcategory_'.$v.'_component_id'])) {
    if (!isset($_POST[$k])) {
      $missed = $k;
      break;
    }
    else {
      $_POST['subcategory_'.$v.'_component_id'] = (int)$_POST[$k];
      unset($_POST[$k]);
    }
  }
}

if ($missed) die('status=error&err_msg=Missed parameter: '.$missed);

// Validate postcode
if (!isset($_POST['postcode'])) {
  die('status=error&err_msg=Missed postcode');
}
else {
  require ('./xml/postcode.inc.php');
  
  $_POST['postcode'] = str_replace(' ', '', $_POST['postcode']);
  
  $_POST['postcode'] = trim($_POST['postcode']);
  
  $valid = bypValidatePostcode($_POST['postcode'], false);
  
  if (!$valid) { // false means do not print results
    die('status=error&err_msg=Incorrect postcode: '.$valid . ' = '.$_POST['postcode']);
  }
}

// Validate register email if set
if (isset($_POST['email']) && $_POST['email']) {
  $sql = "SELECT id, email FROM `user` WHERE email='".mysql_real_escape_string($_POST['email'])."'";
  $rs = getRs($sql);
  if (mysqli_num_rows($rs)) {
    die('status=error&err_msg=You session is expired, please re-login with your email: '.$_POST['email'].' during preparing your quote'); // Email already registered
  }
}
$userid = 0;
if (isset($_POST['userid']) && $_POST['userid']) {
  if (isset($_SESSION['userid']) && $_POST['userid']==$_SESSION['userid']) {
    $sql = "SELECT id, email, firstname, surname, address, phone FROM `user` WHERE id=".(int)$_POST['userid'];
    $rs = getRs($sql);
    if (!mysqli_num_rows($rs)) {
      die('status=error&err_msg=User is not logged in!'); // User ID:'.$_POST['userid'].' is not exists or not logged in!
    }
    else {
      $row = mysqli_fetch_assoc($rs);
      $userid = $row['id'];
      $email = $row['email'];
      $name = $row['firstname'];
      $surname = $row['surname'];
      $address = $row['address'];
      $phone = $row['phone'];
    }
  }
  else die('status=error&err_msg=Session is expired! Please start again.'); // User ID:'.$_POST['userid'].' is not exists or not logged in!
}

if ($use_size_estimator == 'true') {

	$room_length = 0;

	$room_width = 0;

	$use_room_size_estimator = 1;
  
  if (isset($_POST['bedrooms']) && isset($_POST['terrace_type']))  {
    $_POST['bedrooms'] = (int)$_POST['bedrooms'];
    if ($_POST['bedrooms']<1 || $_POST['bedrooms']>9) $_POST['bedrooms'] = 2;
    $_POST['terrace_type'] = (int)$_POST['terrace_type'];
    if ($_POST['terrace_type']<1 || $_POST['terrace_type']>2) $_POST['terrace_type'] = 1;
    $use_room_size_estimator = (int)$_POST['bedrooms'].'|'.$_POST['terrace_type'];
  }

}

else {

	$use_room_size_estimator = 0;

	$room_size = $room_length * $room_width;

}

$base_areas = '';
$base_pricing = '';

$min_quote = 0; //28000;

$max_quote = 0; //120000;

$design_cost = 0;
$design_discount = 0;
$offer_exp_days = '';
$duration_pricing = '';
$duration_weeks = '';
$postcode_pricing = '';
$flat_pricing = 100;
$poa_message = '';
$map_postcodes = '';

$rs = getRs("SELECT s.base_areas, s.base_pricing, s.min_quote, s.max_quote, s.design_cost, s.design_discount, s.offer_exp_days, s.duration_pricing, s.duration_weeks, s.postcode_pricing, s.flat_pricing, s.poa_message, s.map_postcodes FROM setting s WHERE s.setting_id = 1");

if ($row = mysqli_fetch_assoc($rs)) {

	$base_areas = $row['base_areas'];
  $base_pricing = $row['base_pricing'];

	$min_quote = (float)$row['min_quote'];

	$max_quote = (float)$row['max_quote'];

  $design_cost = (float)$row['design_cost'];
  $design_discount = (float)$row['design_discount'];
  $offer_exp_days = $row['offer_exp_days'];
  $duration_pricing = $row['duration_pricing'];
  $duration_weeks = $row['duration_weeks'];
  $postcode_pricing = $row['postcode_pricing'];
  $flat_pricing = (int)$row['flat_pricing'];
  if ($flat_pricing<0) $flat_pricing = 100;
  $poa_message = $row['poa_message'];
  $map_postcodes = explode(',', $row['map_postcodes']);
}



$rs = getRs("SELECT m.conversion_factor, m.measure_unit_id FROM measure_unit m WHERE m.measure_unit_id ='".(int)$measure_unit_id."' OR m.abbreviation='".mysql_real_escape_string($measure_unit_id)."'");

if ($row = mysqli_fetch_assoc($rs)) {

	$room_size = $room_size * $row['conversion_factor'] * $row['conversion_factor'];
  $measure_unit_id = $row['measure_unit_id'];
}

$base_price = 0;

$base_areas = explode(';', $base_areas);
$base_pricing = explode(';', $base_pricing);

$prev_area = 0;
foreach ($base_areas AS $k => $v) {
  $v = trim($v);
  if ($room_size >= $prev_area && $room_size < $v) {
    if (isset($base_pricing[$k])) $base_price = trim($base_pricing[$k]);
    break;
  }
  
  $prev_area = $v;
}

if (!$base_price) {
  $k = count($base_areas)-1;
  if ($room_size >= $base_areas[$k]) {
    if (isset($base_pricing[$k+1])) $base_price = trim($base_pricing[$k+1]);
  }
}

//debug
if ($debug) {
  fwrite($fp, "room_size:$room_size\nbase_price:$base_price\n");
}

// init cost
$total = $room_size * $base_price;

$door = 0;

$roof = 0;

$wall = 0;

$floor = 0;

$kitchen = 0;

$heating = 0;

/*
$a_component = array(
  'cb_door' => 1,     // subcategory_1_component_id
  'cb_roof' => 2,     // subcategory_2_component_id
  'cb_wall' => 3,     // subcategory_3_component_id
  'cb_floor' => 4,    // subcategory_4_component_id
  'cb_kitchen' => 5,  // subcategory_5_component_id
  'cb_heating' => 6   // subcategory_6_component_id
);
*/

foreach ($a_component AS $k => $v) {
  $rs = getRs("SELECT cost_base FROM component WHERE component_id = '".$_POST['subcategory_'.$v.'_component_id']."'");
  if ($row = mysqli_fetch_assoc($rs)) {
    $row['cost_base'] = (float)$row['cost_base'];
    $total += $row['cost_base'];
    switch ($k) { 
      case 'cb_doors':
        $door = $row['cost_base'];
        break;
      case 'cb_roof':
        $roof = $row['cost_base'];
        break;
      case 'cb_wall':
        $wall = $row['cost_base'];
        break;
      case 'cb_floor':
        $floor = $row['cost_base'];
        break;
      case 'cb_kitchen':
        $kitchen = $row['cost_base'];
        break;
      case 'cb_heating':
        $heating = $row['cost_base'];
    }
  }
}



// labour

$labour = 0;

// materials

$materials = 0;

// plant

$plant = 0;

// waste

$waste = 0;

// misc

$misc = 0;

// restrict to min

if ($total < $min_quote) {

	$total = $min_quote;

}

$duration = 0;

$duration_pricing = explode(';', $duration_pricing);
$duration_weeks = explode(';', $duration_weeks);

$prev_duration = 0;
foreach ($duration_pricing AS $k => $v) {
  $v = trim($v);
  if ($total >= $prev_duration && $total < $v) {
    if (isset($duration_weeks[$k])) $duration = trim($duration_weeks[$k]);
    break;
  }
  
  $prev_duration = $v;
}

if (!$duration) {
  $k = count($duration_pricing)-1;
  if ($total > $duration_pricing[$k]) {
    if (isset($duration_weeks[$k+1])) $duration = trim($duration_weeks[$k+1]);
  }
}

$postpart = $_POST['postcode'];
$postpart = str_replace(' ', '', $postpart);
if (strlen($postpart)<6) $postpart = substr($postpart, 0, 2);
elseif (strlen($postpart)==6) $postpart = substr($postpart, 0, 3);
else $postpart = substr($postpart, 0, 4);

// postcode pricing fix
$postcode_pricing = explode("\n", $postcode_pricing);
foreach ($postcode_pricing AS $k => $v) {
  $v = trim($v);
  $v = explode(' ', $v);
  $v[0]=trim($v[0]);
  $v[1]=trim($v[1]);
  if (strtoupper($v[0])==strtoupper($postpart)) {
    $total = (int)$v[1]/100*$total;
    break;
  }
}

// restrict to min again

if ($total < $min_quote) {

	$total = $min_quote;

}

if ('flat'==$property_type) {
  $total = $flat_pricing/100*$total;
}

$design = $design_cost - $design_discount;

$total_2 = $total + $design;

// restrict to max
if ($total > $max_quote && $max_quote > 0) {

	$total = -1 * $total; // negative value means POA

}

$prefix = 'BYP/';
$suffix = '/' . $postpart;

if (is_int(strpos($USER_AGENT, 'iPhone'))) {

  $prefix .= 'iOS';
  $quote_num = getQuotenumber(4, $prefix, $suffix);

}
else {
  $quote_num = getQuotenumber(5, $prefix, $suffix);
}


// When looking to start, Homeowner status
$start = isset($_POST['start'])?trim($_POST['start']):'';
$status = isset($_POST['status'])?trim($_POST['status']):'';

$a_start = array('immediately', '1-3 months', '3-6 months', '1 year');
$a_status = array('pre-purchase', 'just moved in', 'resident for less than 1 year', 'resident for more than 1 year');

if (!in_array($start, $a_start)) $start = $a_start[0];
if (!in_array($status, $a_status)) $status = $a_status[0];

// 'Out of Territory' or 'In Territory' ?
$cover = 'Out of Territory';

foreach ($map_postcodes AS $v) {
  $v = trim($v);
  if (strtoupper($postpart) === strtoupper($v)) {
    $cover = 'In Territory';
    break;
  }
}

// Create new user if registered
$new_user = false;
if (!$userid && isset($_POST['email']) && isset($_POST['password']) && $_POST['email'] && $_POST['password']) {
  
  $email = $_POST['email'];
  
  $address = '';
  if (isset($_POST['house']) && $_POST['house']) $address .= $_POST['house'] . ', ';
  if (isset($_POST['addr']) && $_POST['addr']) $address .= $_POST['addr'];
  $address = trim($address, ', ');
  
  $name = isset($_POST['name'])?$_POST['name']:'';
  $surname = isset($_POST['surname'])?$_POST['surname']:'';
  $phone = isset($_POST['phone'])?$_POST['phone']:'';
  
  $phone = trim(preg_replace('/[^0-9\s\+\-a-z\#]/i', '', $phone));
  
  $sql = "INSERT INTO `user` SET email='".mysql_real_escape_string($email)."', password='".mysql_real_escape_string($_POST['password'])."', firstname='".mysql_real_escape_string($name)."', surname='".mysql_real_escape_string($surname)."', phone='".mysql_real_escape_string($phone)."', address='".mysql_real_escape_string($address)."', postcode='".mysql_real_escape_string($_POST['postcode'])."'";
  
  setRs($sql);
  
  $userid = mysql_insert_id();
  $new_user = true;
  $_SESSION['userid'] = $userid; // auto login new registered user
  $_SESSION['user_info'] = array('email'=>$email, 'firstname'=>$name, 'surname'=>$surname, 'phone'=>$phone, 'address'=>$address, 'postcode'=>$_POST['postcode']);
  
  if ('buildteam.com'==$_SERVER['HTTP_HOST'] || 'www.buildteam.com'==$_SERVER['HTTP_HOST']) {
    
    // add to MailChimp list
    include ('./inc/MCAPI.class.php');
    
    // grab an API Key from http://admin.mailchimp.com/account/api/
    $api = new MCAPI('5e67ce433f85f48ff48ae08a078e742b-us7');
    
    // grab your List's Unique Id by going to http://admin.mailchimp.com/lists/
    // Click the "settings" link for the list - the Unique Id is at the bottom of that page. 
    $list_id = "7ed8f67699";

    $mergeVars = array(
              'FNAME' => $name,
              'LNAME' => $surname,
              'COVER' => $cover,
              'HOMEOWNER' => $status,
              'WHENSTART' => $start,
              'POSTCODE' => $_POST['postcode']
              );
    
    if($api->listSubscribe($list_id, $email, $mergeVars, 'html', $doubleOptIn=false) === true) {
      // It worked!	
      $in_mailchimp = 1;
    }else{
      // An error ocurred, return error message	
      $in_mailchimp = strip_tags($api->errorMessage);
      $in_mailchimp = 'Error: ' . ($in_mailchimp ? $in_mailchimp : substr($api->errorMessage, 0, 247));
    }
    
    $sql = "UPDATE `user` SET in_mailchimp='".mysql_real_escape_string($in_mailchimp)."' WHERE id=$userid";
    setRs($sql);
    
  }
}

$extra_fields = '';
$extra_values = '';

if ($userid) {
  
  $name = isset($_POST['name'])?$_POST['name']:'';
  $surname = isset($_POST['surname'])?$_POST['surname']:'';
  
  $message = isset($_POST['message'])?trim($_POST['message']):'';
  
  $address = '';
  if (isset($_POST['house']) && $_POST['house']) $address .= $_POST['house'] . ', ';
  if (isset($_POST['addr']) && $_POST['addr']) $address .= $_POST['addr'];
  $address = trim($address, ', ');
  
  if (isset($_POST['phone']) && ($_POST['phone']=trim($_POST['phone']))) $phone = $_POST['phone'];
  
  $phone = trim(preg_replace('/[^0-9\s\+\-a-z\#]/i', '', $phone));
  
  if ($debug) {
    fwrite($fp, 'phone:' . $phone. ' message: '.$message."\n\n");
  }
  
  $extra_fields .= ', userid, quote_type_id, name, email, address, phone, message, `start`, `status`';
  $extra_values .= ", $userid, 2, '".mysql_real_escape_string(trim($name.' '.$surname))."', '".mysql_real_escape_string($email)."', '".mysql_real_escape_string($address)."', '".mysql_real_escape_string($phone)."', '".mysql_real_escape_string($message)."', '".mysql_real_escape_string($start)."', '".mysql_real_escape_string($status)."'";
  
}

// New custom: I think we should automatically email all first-time generated quotes for BYP
if ($new_user) {
  $extra_fields .= ', is_email_quote, rfq_status_id';
  $extra_values .= ", 1, 2";
}

$extra_fields .= ', cover';
$extra_values .= ", '".('In Territory'==$cover?'in':'out')."'";

// old $qn:
//$qn = $prefix . $quote_num . $suffix; //uniqid(); //(rand()%1000);
// new $qn:

$a_status_key = array(
  'pre-purchase' => 'PP',
  'just moved in' => 'HJ', 
  'resident for less than 1 year' => 'H0', 
  'resident for more than 1 year' => 'H1'
);

$a_start_key = array(
  'immediately' => '0',
  '1-3 months' => '1',
  '3-6 months' => '1', 
  '1 year' => '2'
);

$qn = $a_status_key[$status] . '/' . ( round(abs($total)/1000) ). $a_start_key[$start] . '/' . $postpart;

$total = currency_format($total);
$total_2 = currency_format($total_2);

if (isset($_POST['qid']) && ($_POST['qid']=(int)$_POST['qid'])) { // update revised quote
  $sql = "UPDATE rfq SET";
  $sql .= " room_length='{$room_length}', room_width='{$room_width}', room_size='{$room_size}', property_type='{$property_type}', postcode='".formatSql($_POST['postcode'])."', measure_unit_id='{$measure_unit_id}', use_room_size_estimator='{$use_room_size_estimator}', subcategory_1_component_id='{$_POST['subcategory_1_component_id']}', subcategory_2_component_id='{$_POST['subcategory_2_component_id']}', subcategory_3_component_id='{$_POST['subcategory_3_component_id']}', subcategory_4_component_id='{$_POST['subcategory_4_component_id']}', subcategory_5_component_id='{$_POST['subcategory_5_component_id']}', subcategory_6_component_id='{$_POST['subcategory_6_component_id']}', subcategory_1_cost='{$door}', subcategory_2_cost='{$roof}', subcategory_3_cost='{$wall}', subcategory_4_cost='{$floor}', subcategory_5_cost='{$kitchen}', subcategory_6_cost='{$heating}', subcategory_7_cost='{$labour}', subcategory_8_cost='{$materials}', subcategory_9_cost='{$plant}', subcategory_10_cost='{$waste}', subcategory_11_cost='{$misc}', amount_quote='{$total}', amount_quote_2='{$total_2}', duration='{$duration}', ipaddress='{$IP}', useragent='{$USER_AGENT}', date_modified='" . time() . "'";
  $sql .= ", name='".mysql_real_escape_string(trim($name.' '.$surname))."', address='".mysql_real_escape_string($address)."', phone='".mysql_real_escape_string($phone)."', message='".mysql_real_escape_string($message)."', `start`='".mysql_real_escape_string($start)."', `status`='".mysql_real_escape_string($status)."'";
  $sql .= " WHERE rfq_id={$_POST['qid']} AND userid=".(int)$_SESSION['userid'];
  
  setRs($sql);
  
  $qid = $_POST['qid'];
  
  $sql = "SELECT rfq_code FROM rfq WHERE rfq_id={$_POST['qid']}";
  $rs = getRs($sql);
  $row = mysqli_fetch_assoc($rs);
  $qn = $row['rfq_code'];
  
}
else { // place new
  $sql = "INSERT INTO rfq (rfq_code, room_length, room_width, room_size, property_type, postcode, measure_unit_id, use_room_size_estimator, subcategory_1_component_id, subcategory_2_component_id, subcategory_3_component_id, subcategory_4_component_id, subcategory_5_component_id, subcategory_6_component_id, subcategory_1_cost, subcategory_2_cost, subcategory_3_cost, subcategory_4_cost, subcategory_5_cost, subcategory_6_cost, subcategory_7_cost, subcategory_8_cost, subcategory_9_cost, subcategory_10_cost, subcategory_11_cost, amount_quote, amount_quote_2, duration, ipaddress, useragent, date_created, date_modified".$extra_fields.") VALUES 
  ('{$qn}', '{$room_length}', '{$room_width}', '{$room_size}' , '{$property_type}', '".formatSql($_POST['postcode'])."', '{$measure_unit_id}', '{$use_room_size_estimator}', '{$_POST['subcategory_1_component_id']}', '{$_POST['subcategory_2_component_id']}', '{$_POST['subcategory_3_component_id']}', '{$_POST['subcategory_4_component_id']}', '{$_POST['subcategory_5_component_id']}', '{$_POST['subcategory_6_component_id']}', '{$door}', '{$roof}', '{$wall}', '{$floor}', '{$kitchen}', '{$heating}', '{$labour}', '{$materials}', '{$plant}', '{$waste}', '{$misc}', '{$total}', '{$total_2}', '{$duration}', '{$IP}', '{$USER_AGENT}','" . time() . "','" . time() . "'".$extra_values.")";
  
  setRs($sql);
  
  $qid = mysql_insert_id();
}

if ($debug) fwrite($fp, $sql."\n\n");

$rs = getRs("SELECT COUNT(rfq_id) AS total FROM rfq WHERE ipaddress='{$IP}'");
$row = mysqli_fetch_assoc($rs);
if ($row && isset($row['total'])) {
  setRs("UPDATE rfq SET ip_quotes=".(int)$row['total']." WHERE rfq_id=".(int)$qid);
}

$rs = getRs("SELECT image FROM cgi WHERE subcategory_1_component_id = '{$_POST['subcategory_1_component_id']}' AND subcategory_2_component_id = '{$_POST['subcategory_2_component_id']}' AND subcategory_3_component_id = '{$_POST['subcategory_3_component_id']}' AND subcategory_4_component_id = '{$_POST['subcategory_4_component_id']}'");



if ($row = mysqli_fetch_assoc($rs)) {

	$cgi_url = "/byp/" . $row['image'];

}

else {

	$cgi_url = "/images/cgi1.jpg";

}


echo "status=success&qid={$qid}&qn={$qn}&costing=" . round($total) . "&costing_2=" . round($total_2) . "&costing_3=".round($design)."&design_discount=".round($design_discount)."&offer_exp_days=".$offer_exp_days."&duration=" . $duration . "&cgi_url={$cgi_url}".($total<0?'&poa_message='.$poa_message:'').($userid?'&userid='.$userid:'').'&cover='.$cover.'&new_user='.($new_user?1:0);

//debug
if ($debug) {
  fwrite($fp, "total:$total\n");
  fclose($fp);
}

if ($qid && $new_user) { // send email
  $_POST['id'] = $qid;
  $sql = "SELECT rfq_code FROM rfq WHERE rfq_id=$qid";
  $rs = getRs($sql);
  $row = mysqli_fetch_assoc($rs);
  $_POST['code'] = $row['rfq_code'];
  
  // new PDF quote
  ob_start();
  include_once 'quote-email.php';
  ob_end_clean();
}

function getQuotenumber($max_len, $prefix='', $suffix='') {

	$str = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";

	$ret = "";

	srand(time());



	while (strlen($ret) < $max_len) {

		$random = (rand()%35);

		$ret .= substr($str, $random, 1);

		if (strlen($ret) == $max_len) {

			$rs = getRs("SELECT rfq_id FROM rfq WHERE rfq_code = '".mysql_real_escape_string($prefix.$ret.$suffix)."'");

			if ($row = mysqli_fetch_assoc($rs)) {

				$ret = "";

			}

		}

	}

	return $ret;

}



?>