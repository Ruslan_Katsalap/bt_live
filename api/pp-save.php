<?php include("src/config.php");?>
<?php
@$db = new mysqli($host, $user, $pass, $db_name);
if(mysqli_connect_errno()){
	echo "DB connection error";
	exit;
}

if(isset($_GET["f_name"])){
	$f_name = $_GET["f_name"];
}

if(isset($_GET["s_name"])){
	$s_name = $_GET["s_name"];
}

if(isset($_GET["email"])){
	$email = $_GET["email"];
}

if(isset($_GET["phone"])){
	$phone = $_GET["phone"];
}

if(isset($_GET["address"])){
	$address = $_GET["address"];
}

if(isset($_GET["postcode"])){
	$postcode = $_GET["postcode"];
}

if(isset($_GET["link"])){
	$link = $_GET["link"];
}

if(isset($_GET["discount"])){
	$discount = $_GET["discount"];
}

if(isset($_GET["session_id"])){
	$session_id = $_GET["session_id"];
}

$payment_status = "pending";


$query = "INSERT INTO pp_booking ( f_name,
									s_name,
									email,
									phone,
									address,
									postcode,
									link,
									discount,
									payment_status,
									session_id
								) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
$stmt = $db->prepare($query);
$stmt->bind_param('ssssssssss', $f_name,
								$s_name,
								$email,
								$phone,
								$address,
								$postcode,
								$link,
								$discount,
								$payment_status,
								$session_id
		);
$stmt->execute();



if($stmt->affected_rows > 0){
echo json_encode(array("result" => true));//data inserted
} else {
	echo json_encode(array("result" => false));//inserting error
}
$db->close();