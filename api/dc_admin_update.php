<?php include("src/config.php"); ?>
<?php
@$db = new mysqli($host, $user, $pass, $db_name);
if(mysqli_connect_errno()){
	echo "DB connection error";
	exit;
}



//for updating slot settings
if(isset($_GET["slot_status"])){
	$slot_status = $_GET["slot_status"];
} else {
	$slot_status = "";
}

if(isset($_GET["slot_id"])){
	$slot_id = (int) $_GET["slot_id"];
} else {
	$slot_id = "";
}

if(isset($_GET["slots"])){
	$slot_setting = $_GET["slots"];
} else {
	$slot_setting = "";
}

if(isset($_GET["slot_date"])){
	$slot_date  = $_GET["slot_date"];
}

//update slot settings from admin
if($slot_status == "add"){//add settings (new row)
		$query2 = "INSERT INTO dc_slots_settings (slot_date, slot_config ) VALUES (?,?)";
		$stmt2 = $db->prepare($query2);
		$stmt2->bind_param('ss', $slot_date, $slot_setting);
		$stmt2->execute();
		if($stmt2->affected_rows > 0){
			echo json_encode(array("result" => true));//data inserted
		} else {
			echo json_encode(array("result" => false));//data inserted
		}
} else {//update settings 
		$update_date = date("Y-m-d H:i:s");

		$query2 = "UPDATE dc_slots_settings SET slot_config=?, date_mod=?  WHERE id=?";
		//$query2 = "UPDATE dc_slots_settings SET slot_config=?, date_mod=?  WHERE slot_date=?";
		$stmt2 = $db->prepare($query2);
		$stmt2->bind_param('ssd', $slot_setting, $update_date, $slot_id);
		//$stmt2->bind_param('sss', $slot_setting, $update_date, $slot_date);
		$stmt2->execute();
		if($stmt2->affected_rows > 0){
			echo json_encode(array("result" => true));//data inserted
		} else {
			//echo $stmt2->error;
			echo json_encode(array("result" => false));//data inserted
		}
}

$db->close();

?>
