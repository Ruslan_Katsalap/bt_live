<?php

include '../ics-master/ICS.php';

header('Content-Type: text/calendar; charset=utf-8');
header('Content-Disposition: attachment; filename=invite.ics');

 $DateTime = new DateTime($_POST["date_start"]);
 $DateTime->modify('-3 hours');
 $new_time = $DateTime->format("Y-m-dTH:i:s");

 $req_type = $_POST["req_type"];
 if($req_type == "video call"){
 	$loc = "Via Zoom – details to be confirmed";
 } else {
 	$loc = "Via Phone – we will call you";
 }

//CUSTOMER ICS
$ics = new ICS(array(
  'location' => $loc,
  'description' => "",
  'dtstart' => $new_time,
  'dtend' => $new_time,
  'summary' => "Design Consultation with Build Team",
  'url' => ""
));

//$ics->to_string();

$file_name = uniqid().'.ics';
$file = '../ics_files/'.$file_name;

//$current = file_get_contents($file);

$current = $ics->to_string();


file_put_contents($file, $current);


//ADMIN ICS
if($_POST["req_type"] == "video call"){
	$r_type = "Zoom";
} else {
	$r_type = "Phone Call";
}

$admin_loc = $_POST["address"]." ".$_POST["postcode"]." ".$_POST["link"];

$summ = "DC ".$_POST["name"]." via ".$r_type;

$ics_2 = new ICS(array(
  'location' => $admin_loc,
  'description' => "",
  'dtstart' => $new_time,
  'dtend' => $new_time,
  'summary' => $summ,
  'url' => ""
));

//$ics->to_string();

$file_name_2 = uniqid().'.ics';
$file_2 = '../ics_files/'.$file_name_2;

//$current = file_get_contents($file);

$current_2 = $ics_2->to_string();


file_put_contents($file_2, $current_2);





echo json_encode(array("ics_filename" => $file_name, "ics_admin_filename"=>$file_name_2));