<?php
include("src/config.php"); //DB config
include('../sendgrid/sendgrid-php.php'); //sendgrid

//stripe payment gateway
require_once('../stripe-php-master/init.php');
// Set your secret key. Remember to switch to your live secret key in production.
// See your keys here: https://dashboard.stripe.com/account/apikeys
\Stripe\Stripe::setApiKey('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');

function print_log($val) {
  return file_put_contents('payments.txt', print_r($val, TRUE));
}

// You can find your endpoint's secret in your webhook settings
//$endpoint_secret = 'whsec_9GT0meTo8SdKSpQTAvMWqACRHY9MY7Cj'; //local key

//whsec_BXg8dQXWNrTqQ8I1p5ByWMVNpqWjqqRB



$endpoint_secret = 'whsec_rsDFvCfbdcBMW1gamHMYz7dQUiCZVTJ9'; //server key live

$payload = @file_get_contents('php://input');
$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
$event = null;

try {
  $event = \Stripe\Webhook::constructEvent(
    $payload, $sig_header, $endpoint_secret
  );
} catch(\UnexpectedValueException $e) {
  // Invalid payload
  http_response_code(400);
  exit();
} catch(\Stripe\Exception\SignatureVerificationException $e) {
  // Invalid signature
  http_response_code(400);
  exit();
}

function fulfill_order($session, $host, $user, $pass, $db_name) {
  // TODO: fill me in
  //print_log("Fulfilling order...");
  //print_log($session);

  if($session->payment_status == "paid"){
  	$session_id = $session->id;
  	$pay_status = "paid";
  	$amount = $session->amount_total / 100;

  	$sess_data = json_encode(($session));
  	

  	@$db = new mysqli($host, $user, $pass, $db_name);
	if(mysqli_connect_errno()){
		echo "DB connection error";
		exit;
	}
	//update user payment status
  	$query2 = "UPDATE pp_booking SET payment_status=?, amount=?  WHERE session_id=?";
	$stmt2 = $db->prepare($query2);
	$stmt2->bind_param('sss', $pay_status,$amount,$session_id);
	$stmt2->execute();
	if($stmt2->affected_rows > 0){
		//save copy of the session in DB
		$query = "INSERT INTO pp_session_data ( session_data ) VALUES (?)";
		$stmt = $db->prepare($query);
		$stmt->bind_param('s', $sess_data);
		$stmt->execute();
	}

	$db->close();

	send_pp_notif($session_id, $host, $user, $pass, $db_name);

  }


}

function send_pp_notif($session_id, $host, $user, $pass, $db_name){
	@$db = new mysqli($host, $user, $pass, $db_name);
	if(mysqli_connect_errno()){
		echo "DB connection error";
		exit;
	}

	$query = "SELECT f_name, s_name, email, phone, address, postcode, link, discount FROM pp_booking WHERE session_id=?";
	$stmt = $db->prepare($query);
	$stmt->bind_param('s', $session_id);
	$stmt->execute();
	$stmt->store_result();
	$stmt->bind_result($f_name, $s_name, $u_email, $phone, $address, $postcode, $link, $discount);


	while($stmt->fetch()){
	    
		//User email

		$to = $u_email; //hello@buildteam.com 
	    $replyTo = $u_email; //$_POST['email']
	    $from = $to;
	    $subject = "Your Pre-Purchase Feasibility Visit has been booked";
	        

	    $message = '

			            <!DOCTYPE html>
		<html lang="en">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, initial-scale=1.0">
			<title></title>
			<style>
				@media (max-width: 600px){
					table {
						width: 100% !important;
					}

					.pl15 {
						padding-left: 15px !important;
					}

					.pr15 {
						padding-right: 15px !important;
					}
				}
			</style>
		</head>
		<body style="margin-top: 0px; font-family: Arial, sans-serif; color: #09467A; font-size: 12px; line-height: 22px;">
			<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; width: 600px; margin-left: auto; margin-right: auto; background: #F2F1F1;">
				<tr>
					<td colspan="2" style="padding-top:45px;"></td>
				</tr>
				<tr>
					<td colspan="2" style="background-color: #09467A; padding-top: 10px; padding-bottom: 10px; text-align: center; color: #fff; font-size: 45px; line-height: 50px;"><img style="width: 188px;display: inline-block;vertical-align: middle;" src="https://www.buildteam.com/email_img/logo.png" alt="" /></td>
				</tr>
				<tr>
					<td colspan="2" style="padding-top: 67px; text-align: center; font-size: 20px; line-height: 30px; font-style: italic;">
						Pre-Purchase Feasibility Visit
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-top: 25px; font-size: 50px; line-height: 60px; font-weight: bold; text-align: center; color: #09467A;">
						Booking
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-top: 60px; padding-bottom: 5px; text-align: center; font-size: 12px; line-height: 22px;">
						Booking Details
					</td>
				</tr>
				<tr>
					<td class="pl15" style="width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
						Full Name:
					</td>
					<td class="pr15" style="width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
						'.$f_name.' '.$s_name.'
					</td>
				</tr>
				<tr>
					<td class="pl15" style="padding-bottom: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
						Email Address:
					</td>
					<td class="pr15" style="padding-bottom: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
						'.$u_email.'
					</td>
				</tr>
				<tr style="background-color: #09467A; color: #fff;">
					<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
						Property Address:
					</td>
					<td class="pr15" class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
						'.$address.', '.$postcode.'
					</td>
				</tr>
				<tr>
					<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
						Right Move Link:
					</td>
					<td class="pr15" class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
						'.$link.'
					</td>
				</tr>
				<tr>
					<td class="pl15 pr15" colspan="2" style="padding-top: 25px; padding-left:75px; padding-right: 75px; text-align: center; font-size: 18px; line-height: 25px;">
						Thank you for your booking.
						A member of our team will be in touch shortly to schedule the Pre-Purchase visit.
					</td>
				</tr>
				<tr>
					<td class="pl15 pr15" colspan="2" style="padding-top: 65px; padding-left: 75px; padding-right: 75px;">
						<p style="margin-top: 0; margin-bottom: 0; padding-left: 10px; padding-right: 10px; font-size: 18px; line-height: 25px;">
							Call us on 
							<a href="tel:020 7495 6561" style="color: #09467A; font-weight: bold; text-decoration: none; display: inline-block; vertical-align: middle;">020 7495 6561</a> or 
								<a href="mailto:hello@buildteam.com" style="color: #09467A; font-weight: bold; text-decoration: none; display: inline-block; vertical-align: middle;">hello@buildteam.com</a>
						</p>
						<span style="display: block; height: 2px; width: 100%; background: #09467A;"></span>
					</td>
				</tr>
				<tr>
					<td class="pl15 pr15" style="width: 225px; padding-top: 25px; padding-left: 75px; padding-bottom: 40px;">
						<a href="https://www.facebook.com/Build-Team-391933624520838/" target="_blank" style="margin-right: 15px; display: inline-block; vertical-align: middle; text-decoration: none;">
							<img src="https://www.buildteam.com/email_img/fb_icon.png" alt="">
						</a>
						<a href="https://www.instagram.com/buildteamldn/" target="_blank" style="margin-right: 15px; display: inline-block; vertical-align: middle; text-decoration: none;">
							<img src="https://www.buildteam.com/email_img/insta_icon.png" alt="">
						</a>
						<a href="https://twitter.com/BuildTeamLDN" target="_blank" style="margin-right: 0px; display: inline-block; vertical-align: middle; text-decoration: none;">
							<img src="https://www.buildteam.com/email_img/twitter_icon.png" alt="">
						</a>
					</td>
					<td class="pr15" style="width: 225px; padding-top: 25px; padding-bottom: 40px; padding-right: 75px; text-align: right;">
						<a href="https://www.buildteam.com/contact.html" target="_blank" style="font-size: 12px; line-height: 14px; color: #09467A;">Contact Us</a>
					</td>
				</tr>
				<tr style="background-color: #09467A; color: #fff; font-size: 10px; line-height: 15px;">
					<td colspan="2" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">
						<span style="display: inline-block; vertical-align: middle; padding-left: 5px; padding-right: 5px; border-right-color: #fff; border-right-width: 1px; border-right-style: solid;">
							&copy; 2021 Design Team
						</span>
						<span style="display: inline-block; vertical-align: middle; padding-left: 5px; padding-right: 5px; border-right-color: #fff; border-right-width: 1px; border-right-style: solid;">
							All Rights Reserved
						</span>
						<a href="https://www.buildteam.com/terms-and-conditions.html" target="_blank" style="display: inline-block; vertical-align: middle; text-decoration: none; color: #fff; padding-left: 5px;">
							Terms & Conditions
						</span>
					</td>
				</tr>
				<tr>
					<td colspan="2" style="padding-top:45px;"></td>
				</tr>
			</table>
		</body>
		</html>
	            ';

		putenv('SENDGRID_API_KEY=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');
		$email = new \SendGrid\Mail\Mail();
		$email->setFrom("hello@buildteam.com","Build Team");
	    //$email->setReplyTo($replyTo, $_POST['fname']);
	    $email->setReplyTo($replyTo, $to);
		$email->setSubject($subject);
		$email->addTo($to, "Buildteam support");
		$email->addContent("text/html", $message);

		// if($filename != ""){
		// 	$att1 = new \SendGrid\Mail\Attachment();
		// 	$att1->setContent(base64_encode(file_get_contents("../book_uploaded_files/".$filename)));
		// 	// $att1->setType("image/jpeg");
		// 	$att1->setFilename($filename); //photo.jpg
		// 	$att1->setDisposition("attachment");
		// 	$email->addAttachment( $att1 );
		// }


		$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

		try {
		    $response = $sendgrid->send($email);
		    	// print $response->statusCode() . "\n";
			    // print_r($response->headers());
			    // print $response->body() . "\n";
		} catch (Exception $e) {
		    echo 'Caught exception: '. $e->getMessage() ."\n";
		    exit;
		}

		send_admin_email($f_name, $s_name, $u_email, $phone, $address, $postcode, $link, $discount); //admin email

	}
		
	$stmt->free_result();
	$db->close();
}


function send_admin_email($f_name, $s_name, $u_email, $phone, $address, $postcode, $link, $discount){
	//Admin email

		$to2 = "hello@buildteam.com"; //hello@buildteam.com 
	    $replyTo2 = $u_email; //$_POST['email']
	    $from2 = $to2;
	    $subject2 = "Pre-Purchase Visit Booked";
	        

	        //$f_name, $s_name, $email, $phone, $address, $postcode, $link, $discount

	    $message2 = '

						           <!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<title></title>
				<style>
					@media (max-width: 600px){
						table {
							width: 100% !important;
						}

						.pl15 {
							padding-left: 15px !important;
						}

						.pr15 {
							padding-right: 15px !important;
						}
					}
				</style>
			</head>
			<body style="margin-top: 0px; font-family: Arial, sans-serif; color: #09467A; font-size: 12px; line-height: 22px;">
				<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; width: 600px; margin-left: auto; margin-right: auto; background: #F2F1F1;">
					<tr>
						<td colspan="2" style="padding-top:45px;"></td>
					</tr>
					<tr>
						<td colspan="2" style="background-color: #09467A; padding-top: 10px; padding-bottom: 10px; text-align: center; color: #fff; font-size: 45px; line-height: 50px;"><img style="width: 188px;display: inline-block;vertical-align: middle;" src="https://www.buildteam.com/email_img/logo.png" alt="" /></td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top: 67px; text-align: center; font-size: 20px; line-height: 30px; font-style: italic;">
							Pre-Purchase Feasibility Visit
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top: 25px; font-size: 50px; line-height: 60px; font-weight: bold; text-align: center; color: #09467A;">
							Booking
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top: 60px; padding-bottom: 5px; text-align: center; font-size: 12px; line-height: 22px;">
							Client’s Details
						</td>
					</tr>
					<tr>
						<td class="pl15" style="width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Full Name:
						</td>
						<td class="pr15" style="width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$f_name.' '.$s_name.'
						</td>
					</tr>
					<tr>
						<td class="pl15" style="padding-bottom: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Email Address:
						</td>
						<td class="pr15" style="padding-bottom: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$u_email.'
						</td>
					</tr>
					<tr style="background-color: #09467A; color: #fff;">
						<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Property Address:
						</td>
						<td class="pr15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$address.', '.$postcode.'
						</td>
					</tr>
					<tr>
						<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Right Move Link:
						</td>
						<td class="pr15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$link.'
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top: 65px; padding-left: 75px; padding-right: 75px;">
							<span style="display: block; height: 2px; width: 100%; background: #09467A;"></span>
						</td>
					</tr>
					<tr>
						<td class="pl15" style="width: 225px; padding-top: 25px; padding-left: 75px; padding-bottom: 40px;">
							<a href="https://www.facebook.com/Build-Team-391933624520838/" target="_blank" style="margin-right: 15px; display: inline-block; vertical-align: middle; text-decoration: none;">
								<img src="https://www.buildteam.com/email_img/fb_icon.png" alt="">
							</a>
							<a href="https://www.instagram.com/buildteamldn/" target="_blank" style="margin-right: 15px; display: inline-block; vertical-align: middle; text-decoration: none;">
								<img src="https://www.buildteam.com/email_img/insta_icon.png" alt="">
							</a>
							<a href="https://twitter.com/BuildTeamLDN" target="_blank" style="margin-right: 0px; display: inline-block; vertical-align: middle; text-decoration: none;">
								<img src="https://www.buildteam.com/email_img/twitter_icon.png" alt="">
							</a>
						</td>
						<td class="pr15" style="width: 225px; padding-top: 25px; padding-bottom: 40px; padding-right: 75px; text-align: right;">
							<a href="https://www.buildteam.com/contact.html" target="_blank" style="font-size: 12px; line-height: 14px; color: #09467A;">Contact Us</a>
						</td>
					</tr>
					<tr style="background-color: #09467A; color: #fff; font-size: 10px; line-height: 15px;">
						<td class="pl15 pr15" colspan="2" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">
							<span style="display: inline-block; vertical-align: middle; padding-left: 5px; padding-right: 5px; border-right-color: #fff; border-right-width: 1px; border-right-style: solid;">
								&copy; 2021 Design Team
							</span>
							<span style="display: inline-block; vertical-align: middle; padding-left: 5px; padding-right: 5px; border-right-color: #fff; border-right-width: 1px; border-right-style: solid;">
								All Rights Reserved
							</span>
							<a href="https://www.buildteam.com/terms-and-conditions.html" target="_blank" style="display: inline-block; vertical-align: middle; text-decoration: none; color: #fff; padding-left: 5px;">
								Terms & Conditions
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top:45px;"></td>
					</tr>
				</table>
			</body>
			</html>
	            ';

		putenv('SENDGRID_API_KEY=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');
		$email2 = new \SendGrid\Mail\Mail();
		$email2->setFrom("hello@buildteam.com","Build Team");
	    //$email->setReplyTo($replyTo, $_POST['fname']);
	    $email2->setReplyTo($replyTo2, $to2);
		$email2->setSubject($subject2);
		$email2->addTo($to2, "Buildteam support");
		$email2->addContent("text/html", $message2);

		// if($filename != ""){
		// 	$att1 = new \SendGrid\Mail\Attachment();
		// 	$att1->setContent(base64_encode(file_get_contents("../book_uploaded_files/".$filename)));
		// 	// $att1->setType("image/jpeg");
		// 	$att1->setFilename($filename); //photo.jpg
		// 	$att1->setDisposition("attachment");
		// 	$email2->addAttachment( $att1 );
		// }


		$sendgrid2 = new \SendGrid(getenv('SENDGRID_API_KEY'));

		try {
		    $response2 = $sendgrid2->send($email2);
		    	// print $response->statusCode() . "\n";
			    // print_r($response->headers());
			    // print $response->body() . "\n";
		} catch (Exception $e) {
		    echo 'Caught exception: '. $e->getMessage() ."\n";
		    exit;
		}
}

// Handle the checkout.session.completed event
if ($event->type == 'checkout.session.completed') {
  $session = $event->data->object;

  // Fulfill the purchase...
  fulfill_order($session, $host, $user, $pass, $db_name);
}

http_response_code(200);
?>