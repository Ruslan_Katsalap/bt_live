<?php 
//get slots for client booking datepicker (1 screen)
?>
<?php include("src/config.php"); ?>
<?php
@$db = new mysqli($host, $user, $pass, $db_name);
if(mysqli_connect_errno()){
	echo "DB connection error";
	exit;
}

$slot_date = $_GET["slot_date"];

$query = "SELECT id, slot_config FROM dc_slots_settings WHERE slot_date = ? ORDER by id DESC LIMIT 1";
$stmt = $db->prepare($query);
$stmt->bind_param('s', $slot_date);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($id, $slot_config);

if($stmt->num_rows == 0){
	echo json_encode(array("result" => false));
}
while($stmt->fetch()){
    echo json_encode(array("result" => true, "data" => $slot_config, "slot_id" => $id));
}
$stmt->free_result();
$db->close();

?>