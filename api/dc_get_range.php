<?php include("src/config.php"); //admin checkbox booked
?>
<?php
@$db = new mysqli($host, $user, $pass, $db_name);
if(mysqli_connect_errno()){
	echo "DB connection error";
	exit;
}



$id = 1;




$query = "SELECT config FROM dc_dp_range_settings WHERE id =?";
$stmt = $db->prepare($query);
$stmt->bind_param('d', $id);
$stmt->execute();
$stmt->store_result();
$stmt->bind_result($range);

if($stmt->num_rows == 0){
	echo json_encode(array("result" => false));
}
while($stmt->fetch()){
    echo json_encode(array("result" => true, "range" => $range));
}
$stmt->free_result();
$db->close();