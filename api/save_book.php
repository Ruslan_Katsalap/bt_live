<?php include("src/config.php"); 
	  include('../sendgrid/sendgrid-php.php'); //sendgrid
?>
<?php
@$db = new mysqli($host, $user, $pass, $db_name);
if(mysqli_connect_errno()){
	echo "DB connection error";
	exit;
}

if(isset($_GET["f_name"])){
	$f_name = $_GET["f_name"];
}

if(isset($_GET["surname"])){
	$surname = $_GET["surname"];
}

if(isset($_GET["email"])){
	$email = $_GET["email"];
}

if(isset($_GET["phone"])){
	$phone = $_GET["phone"];
}

if(isset($_GET["address"])){
	$address = $_GET["address"];
}

if(isset($_GET["postcode"])){
	$postcode = $_GET["postcode"];
}

if(isset($_GET["link"])){
	$link = $_GET["link"];
}

if(isset($_GET["contact"])){
	$contact = $_GET["contact"];
}

if(isset($_GET["start"])){
	$start = $_GET["start"];
}

if(isset($_GET["status"])){
	$status = $_GET["status"];
}

if(isset($_GET["exten_type"])){
	$exten_type = $_GET["exten_type"];
}

if(isset($_GET["events"])){
	$events = $_GET["events"];
}

if(isset($_GET["date"])){
	$slot_date  = $_GET["date"];
}

if(isset($_GET["slot"])){
	$slot = $_GET["slot"];
}


if(isset($_GET["file"])){
	$filename = $_GET["file"];
} else {
	$filename = "";
}

//for updating slot settings
if(isset($_GET["slot_status"])){
	$slot_status = $_GET["slot_status"];
} else {
	$slot_status = "";
}

if(isset($_GET["slot_id"])){
	$slot_id = (int) $_GET["slot_id"];
} else {
	$slot_id = "";
}

if(isset($_GET["slot_setting"])){
	$slot_setting = $_GET["slot_setting"];
} else {
	$slot_setting = "";
}


//ics filename
if(isset($_GET["ics_filename"])){
	$ics = $_GET["ics_filename"];
} else {
	$ics = "";
}

//ics filename admin
if(isset($_GET["ics_admin_filename"])){
	$ics_admin_filename = $_GET["ics_admin_filename"];
} else {
	$ics_admin_filename = "";
}


$query = "INSERT INTO dc_bookings ( f_name, 
									surname, 
									email, 
									phone, 
									address,
									postcode,
									link,
									contact,
									start,
									status,
									exten_type,
									events,
									slot_date,
									slot,
									filename
								) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
$stmt = $db->prepare($query);
$stmt->bind_param('sssssssssssssss', $f_name, 
									$surname,
									$email, 
									$phone, 
									$address,
									$postcode,
									$link,
									$contact,
									$start,
									$status,
									$exten_type,
									$events,
									$slot_date,
									$slot,
									$filename
		);
$stmt->execute();



if($stmt->affected_rows > 0){

	//update slot settings
	if($slot_status == "add"){//add settings (new row)
			$query2 = "INSERT INTO dc_slots_settings (slot_date, slot_config ) VALUES (?,?)";
			$stmt2 = $db->prepare($query2);
			$stmt2->bind_param('ss', $slot_date, $slot_setting);
			$stmt2->execute();
	} else {//update settings 
			$update_date = date("Y-m-d H:i:s");
			$query2 = "UPDATE dc_slots_settings SET slot_config=?, date_mod=?  WHERE id=?";
			$stmt2 = $db->prepare($query2);
			$stmt2->bind_param('ssd', $slot_setting, $update_date, $slot_id);
			$stmt2->execute();
	}
	
	$u_email = $email;

	$to = $u_email; //hello@buildteam.com 
        $replyTo = $u_email; //$_POST['email']
        $from = $to;
        $subject = "Your Design Consultation has been booked";
        

        $message = '

			            <!DOCTYPE html>
			<html lang="en">
			<head>
				<meta charset="UTF-8">
				<meta name="viewport" content="width=device-width, initial-scale=1.0">
				<title></title>
				<style>
					@media (max-width: 600px){
						table {
							width: 100% !important;
						}

						.pl15 {
							padding-left: 15px !important;
						}

						.pr15 {
							padding-right: 15px !important;
						}
					}
				</style>
			</head>
			<body style="margin-top: 0px; font-family: Arial, sans-serif; color: #09467A; font-size: 12px; line-height: 22px;">
				<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; width: 600px; margin-left: auto; margin-right: auto; background: #F2F1F1;">
					<tr>
						<td colspan="2" style="padding-top:45px;"></td>
					</tr>
					<tr>
						<td colspan="2" style="background-color: #09467A; padding-top: 10px; padding-bottom: 10px; text-align: center; color: #fff; font-size: 45px; line-height: 50px;"><img style="width: 188px;display: inline-block;vertical-align: middle;" src="https://www.buildteam.com/email_img/logo.png" alt="" /></td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top: 67px; text-align: center; font-size: 20px; line-height: 30px; font-style: italic;">
							Design Consultation
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top: 25px; font-size: 50px; line-height: 60px; font-weight: bold; text-align: center; color: #09467A;">
							BOOKED!
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top: 60px; padding-bottom: 5px; text-align: center; font-size: 12px; line-height: 22px;">
							Booking Details
						</td>
					</tr>
					<tr>
						<td class="pl15" style="width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Full Name:
						</td>
						<td class="pr15" style="width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$f_name.' '.$surname.'
						</td>
					</tr>
					<tr>
						<td class="pl15" style="padding-bottom: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Email Address:
						</td>
						<td class="pr15" style="padding-bottom: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$u_email.'
						</td>
					</tr>
					<tr style="background-color: #09467A; color: #fff;">
						<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Property Address:
						</td>
						<td class="pr15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$address.', '.$postcode.'
						</td>
					</tr>
					<tr>
						<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Right Move Link:
						</td>
						<td class="pr15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$link.'
						</td>
					</tr>
					<tr>
						<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
							Booking Date/time:
						</td>
						<td class="pr15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
							'.$slot_date.' '.$slot.'
						</td>
					</tr>
					<tr>
						<td class="pl15 pr15" colspan="2" style="padding-top: 65px; padding-left: 75px; padding-right: 75px;">
							<p style="margin-top: 0; margin-bottom: 0; padding-left: 10px; padding-right: 10px; font-size: 18px; line-height: 25px;">
								If you need to rearrange the booking, please email:  
									<a href="mailto:hello@buildteam.com" style="color: #09467A; font-weight: bold; text-decoration: none; display: inline-block; vertical-align: middle;">hello@buildteam.com</a>
							</p>
							<span style="display: block; height: 2px; width: 100%; background: #09467A;"></span>
						</td>
					</tr>
					<tr>
						<td class="pl15" style="width: 225px; padding-top: 25px; padding-left: 75px; padding-bottom: 40px;">
							<a href="https://www.facebook.com/Build-Team-391933624520838/" target="_blank" style="margin-right: 15px; display: inline-block; vertical-align: middle; text-decoration: none;">
								<img src="https://www.buildteam.com/email_img/fb_icon.png" alt="">
							</a>
							<a href="https://www.instagram.com/buildteamldn/" target="_blank" style="margin-right: 15px; display: inline-block; vertical-align: middle; text-decoration: none;">
								<img src="https://www.buildteam.com/email_img/insta_icon.png" alt="">
							</a>
							<a href="https://twitter.com/BuildTeamLDN" target="_blank" style="margin-right: 0px; display: inline-block; vertical-align: middle; text-decoration: none;">
								<img src="https://www.buildteam.com/email_img/twitter_icon.png" alt="">
							</a>
						</td>
						<td class="pr15" style="width: 225px; padding-top: 25px; padding-bottom: 40px; padding-right: 75px; text-align: right;">
							<a href="https://www.buildteam.com/contact.html" target="_blank" style="font-size: 12px; line-height: 14px; color: #09467A;">Contact Us</a>
						</td>
					</tr>
					<tr style="background-color: #09467A; color: #fff; font-size: 10px; line-height: 15px;">
						<td colspan="2" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">
							<span style="display: inline-block; vertical-align: middle; padding-left: 5px; padding-right: 5px; border-right-color: #fff; border-right-width: 1px; border-right-style: solid;">
								&copy; 2021 Design Team
							</span>
							<span style="display: inline-block; vertical-align: middle; padding-left: 5px; padding-right: 5px; border-right-color: #fff; border-right-width: 1px; border-right-style: solid;">
								All Rights Reserved
							</span>
							<a href="https://www.buildteam.com/terms-and-conditions.html" target="_blank" style="display: inline-block; vertical-align: middle; text-decoration: none; color: #fff; padding-left: 5px;">
								Terms & Conditions
							</span>
						</td>
					</tr>
					<tr>
						<td colspan="2" style="padding-top:45px;"></td>
					</tr>
				</table>
			</body>
			</html>

            ';

	putenv('SENDGRID_API_KEY=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');
	$email = new \SendGrid\Mail\Mail();
	$email->setFrom("hello@buildteam.com","Build Team"); //hello@buildteam.com
    //$email->setReplyTo($replyTo, $_POST['fname']);
    $email->setReplyTo($replyTo, $to);
	$email->setSubject($subject);
	$email->addTo($to, "Buildteam support");
	$email->addContent("text/html", $message);

	if($filename != ""){
		$att1 = new \SendGrid\Mail\Attachment();
		$att1->setContent(base64_encode(file_get_contents("../book_uploaded_files/".$filename)));
		// $att1->setType("image/jpeg");
		$att1->setFilename($filename); //photo.jpg
		$att1->setDisposition("attachment");
		$email->addAttachment( $att1 );
	}

	//ics file attachments
	if($ics != ""){
		$att2 = new \SendGrid\Mail\Attachment();
		$att2->setContent(base64_encode(file_get_contents("../ics_files/".$ics)));
		// $att1->setType("image/jpeg");
		$att2->setFilename($ics); //photo.jpg
		$att2->setDisposition("attachment");
		$email->addAttachment( $att2 );
	}


	$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));

	try {
	    $response = $sendgrid->send($email);
	    	// print $response->statusCode() . "\n";
		    // print_r($response->headers());
		    // print $response->body() . "\n";
	} catch (Exception $e) {
	    echo 'Caught exception: '. $e->getMessage() ."\n";
	    exit;
	}

	send_admin_email($f_name, $surname, $u_email, $phone, $address,$postcode, $link, $slot_date, $slot, $ics_admin_filename);

	echo json_encode(array("result" => true));//data inserted
} else {
	echo json_encode(array("result" => false));//inserting error
}
$db->close();



function send_admin_email($f_name, $surname, $u_email, $phone, $address, $postcode, $link, $slot_date, $slot, $ics_admin_filename){
	//Admin email

		$to2 = "hello@buildteam.com"; //hello@buildteam.com 
	    $replyTo2 = $u_email; //$_POST['email']
	    $from2 = $to2;
	    $subject2 = "DC Booking Request";
	        

	        //$f_name, $s_name, $email, $phone, $address, $postcode, $link, $discount

	    $message2 = '

						           
			<!DOCTYPE html>
				<html lang="en">
				<head>
					<meta charset="UTF-8">
					<meta name="viewport" content="width=device-width, initial-scale=1.0">
					<title></title>
					<style>
						@media (max-width: 600px){
							table {
								width: 100% !important;
							}

							.pl15 {
								padding-left: 15px !important;
							}

							.pr15 {
								padding-right: 15px !important;
							}
						}
					</style>
				</head>
				<body style="margin-top: 0px; font-family: Arial, sans-serif; color: #09467A; font-size: 12px; line-height: 22px;">
					<table cellpadding="0" cellspacing="0" border="0" style="border-collapse: collapse; width: 600px; margin-left: auto; margin-right: auto; background: #F2F1F1;">
						<tr>
							<td colspan="2" style="padding-top:45px;"></td>
						</tr>
						<tr>
							<td colspan="2" style="background-color: #09467A; padding-top: 10px; padding-bottom: 10px; text-align: center; color: #fff; font-size: 45px; line-height: 50px;"><img style="width: 188px;display: inline-block;vertical-align: middle;" src="https://www.buildteam.com/email_img/logo.png" alt="" /></td>
						</tr>
						<tr>
							<td colspan="2" style="padding-top: 67px; text-align: center; font-size: 20px; line-height: 30px; font-style: italic;">
								Design Consultation
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-top: 25px; font-size: 50px; line-height: 60px; font-weight: bold; text-align: center; color: #09467A;">
								Booking
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-top: 60px; padding-bottom: 5px; text-align: center; font-size: 12px; line-height: 22px;">
								Client’s Details
							</td>
						</tr>
						<tr>
							<td class="pl15" style="width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
								Full Name:
							</td>
							<td class="pr15" style="width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
								'.$f_name.' '.$surname.'
							</td>
						</tr>
						<tr>
							<td class="pl15" style="padding-bottom: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
								Email Address:
							</td>
							<td class="pr15" style="padding-bottom: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
								'.$u_email.'
							</td>
						</tr>
						<tr style="background-color: #09467A; color: #fff;">
							<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
								Property Address:
							</td>
							<td class="pr15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
								'.$address.', '.$postcode.'
							</td>
						</tr>
						<tr>
							<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
								Right Move Link:
							</td>
							<td class="pr15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
								'.$link.'
							</td>
						</tr>
						<tr>
							<td class="pl15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; padding-left: 100px; font-size: 12px; line-height: 22px;">
								Booking Date/time:
							</td>
							<td class="pr15" style="padding-bottom: 10px; padding-top: 10px; width: 200px; text-align: right; padding-right: 100px; font-size: 12px; line-height: 22px;">
								'.$slot_date.' '.$slot.'
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-top: 65px; padding-left: 75px; padding-right: 75px;">
								<span style="display: block; height: 2px; width: 100%; background: #09467A;"></span>
							</td>
						</tr>
						<tr>
							<td class="pl15" style="width: 225px; padding-top: 25px; padding-left: 75px; padding-bottom: 40px;">
								<a href="https://www.facebook.com/Build-Team-391933624520838/" target="_blank" style="margin-right: 15px; display: inline-block; vertical-align: middle; text-decoration: none;">
									<img src="https://www.buildteam.com/email_img/fb_icon.png" alt="">
								</a>
								<a href="https://www.instagram.com/buildteamldn/" target="_blank" style="margin-right: 15px; display: inline-block; vertical-align: middle; text-decoration: none;">
									<img src="https://www.buildteam.com/email_img/insta_icon.png" alt="">
								</a>
								<a href="https://twitter.com/BuildTeamLDN" target="_blank" style="margin-right: 0px; display: inline-block; vertical-align: middle; text-decoration: none;">
									<img src="https://www.buildteam.com/email_img/twitter_icon.png" alt="">
								</a>
							</td>
							<td class="pr15" style="width: 225px; padding-top: 25px; padding-bottom: 40px; padding-right: 75px; text-align: right;">
								<a href="https://www.buildteam.com/contact.html" target="_blank" style="font-size: 12px; line-height: 14px; color: #09467A;">Contact Us</a>
							</td>
						</tr>
						<tr style="background-color: #09467A; color: #fff; font-size: 10px; line-height: 15px;">
							<td class="pl15 pr15" colspan="2" style="text-align: center; padding-top: 15px; padding-bottom: 15px;">
								<span style="display: inline-block; vertical-align: middle; padding-left: 5px; padding-right: 5px; border-right-color: #fff; border-right-width: 1px; border-right-style: solid;">
									&copy; 2021 Design Team
								</span>
								<span style="display: inline-block; vertical-align: middle; padding-left: 5px; padding-right: 5px; border-right-color: #fff; border-right-width: 1px; border-right-style: solid;">
									All Rights Reserved
								</span>
								<a href="https://www.buildteam.com/terms-and-conditions.html" target="_blank" style="display: inline-block; vertical-align: middle; text-decoration: none; color: #fff; padding-left: 5px;">
									Terms & Conditions
								</span>
							</td>
						</tr>
						<tr>
							<td colspan="2" style="padding-top:45px;"></td>
						</tr>
					</table>
				</body>
				</html>
	            ';

		putenv('SENDGRID_API_KEY=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');
		$email2 = new \SendGrid\Mail\Mail();
		$email2->setFrom("hello@buildteam.com","Build Team");
	    //$email->setReplyTo($replyTo, $_POST['fname']);
	    $email2->setReplyTo($replyTo2, $to2);
		$email2->setSubject($subject2);
		$email2->addTo($to2, "Buildteam support");
		$email2->addContent("text/html", $message2);

		// if($filename != ""){
		// 	$att1 = new \SendGrid\Mail\Attachment();
		// 	$att1->setContent(base64_encode(file_get_contents("../book_uploaded_files/".$filename)));
		// 	// $att1->setType("image/jpeg");
		// 	$att1->setFilename($filename); //photo.jpg
		// 	$att1->setDisposition("attachment");
		// 	$email2->addAttachment( $att1 );
		// }

		//ics file attachments
		if($ics_admin_filename != ""){
			$att2 = new \SendGrid\Mail\Attachment();
			$att2->setContent(base64_encode(file_get_contents("../ics_files/".$ics_admin_filename)));
			// $att1->setType("image/jpeg");
			$att2->setFilename($ics_admin_filename); //photo.jpg
			$att2->setDisposition("attachment");
			$email2->addAttachment( $att2 );
		}


		$sendgrid2 = new \SendGrid(getenv('SENDGRID_API_KEY'));

		try {
		    $response2 = $sendgrid2->send($email2);
		    	// print $response->statusCode() . "\n";
			    // print_r($response->headers());
			    // print $response->body() . "\n";
		} catch (Exception $e) {
		    echo 'Caught exception: '. $e->getMessage() ."\n";
		    exit;
		}
}

?>
