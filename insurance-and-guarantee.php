<?php require_once('./inc/header.inc.php'); ?>

<?php //require_once('save_how_can_help_form.php'); ?>
<link rel="stylesheet" href="css/slick.css" type="text/css">
<link rel="stylesheet" type="text/css" href="/Flipbook/font-awesome.css">
<script src="/Flipbook/flipbook.min.js"></script>

<style>
	.titlehowwecan h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	border-bottom: 1px solid #003c70;
}	
.howwedesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #003c70;
    line-height: 24px;
    font-weight: normal;
}
.middleinner {
    max-width: 980px;
    margin: 0 auto;
	padding:30px 0px;
}
.innerrow .insiderow {
    float: left;
    width: 33%;
    text-align: center;
	padding:15px 0px;
}

.innerrow:after {
    content: "";
    clear: both;
    display: table;
}
.upgradetitle h2 {
    color: #0f4778;
    font-size: 18px;
	margin-top:15px;
}
.upgradedesc p {
    color: #0f4777;
    font-size: 16px;
}

.insiderow img {
    max-width: 60px;
}
.bottomshortcut {
	max-width:980px;
	margin:0 auto;
	}
.shortcutinline {
    float: left;
    width: 24.2%;
}
.shortcutinline:nth-child(3) {
    margin-right: 1%;
}
.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
    margin-left: 1%;
}
.shortcutdesc {
    background: rgba(255,255,255,0.7);
    padding: 10px;
    text-align: center;
    color: #0f4679;
    position: absolute;
    bottom: 0px;
    width: 100%;
    font-weight: bold;
    font-size: 18px;
}
.shortcutwarp {
    position: relative;
}
.shortcutwarp img {
    max-width: 100%;
}
.tenderprocess {
    background: #ecebeb;
    margin: 50px 0px;
}
.tenderpprocesstitle h1 {
    font-weight: normal;
    padding: 0px;
}
.noteinfo {
    text-align: right;
    position: relative;
    top: 30px;
}
.noteinfo p {
    background: #bdbdbd;
    display: inline-block;
    padding: 0;
    color: #547088;
    padding-right: 10px;
    font-size: 16px;
}
.noteinfo span {
    background: #9f9f9f;
    display: inline-block;
    width: 20px;
    text-align: center;
    padding: 5px 5px;
    color: #FFF;
    font-size: 20px;
    vertical-align: middle;
    margin-right: 10px;
}
.noteinfo.noteinfo2 {
    top: -20px;
    right: 3px;
	margin-bottom:30px;
}
.moreinfo {
	max-width:980px;
	margin:0 auto;
	}
.moreinfo h1 {
    font-size: 20px !important;
    border-bottom: 2px solid #0f4778;
}
.completionpack {
    background: #e2e2e2;
	padding-bottom:30px;
	margin-bottom:20px;
}
.completioninner {
    max-width: 980px;
    margin: 0 auto;
}
.completioninner h1 {
    text-align: center;
    padding-top: 30px;
    font-weight: normal;
}
.completioninner p {
    font-size: 22px;
    text-align: justify;
    color: #0f4778;
    margin-bottom: 30px;
    padding-top: 0px;
}
.complitioninline {
    width: 50%;
    float: left;
}
.complitionrow:after {
	content:"";
	display:table;
	clear:both;
	}
.complitioninline h2 {
    font-size: 26px;
    text-align: center;
    color: #0f4778;
	font-weight:normal !important;
	margin-bottom:20px;
}
.complitioninline li {
    font-size: 22px;
    color: #0f4778;
    margin-bottom: 10px;
}
.complitioninline ul {
    padding-left: 20px;
}
@media (max-width:768px) {
	button.slick-prev.slick-arrow {
		left:0px;
		}
	button.slick-next.slick-arrow{
		right:0px;
		}
	.tenderslider:before {
		display:none;
		}	
	.inlineflipbook {
			margin-left: 0px;
			width: 100% !important;
			max-width: 100%;
		}	
	.fliboocwrap {
		background: #f2f5f8;
		height: 825px;
		widows: 100%;
		margin-top: 50px;
	}
	.inlineflipbook.flipmargin {
		margin-bottom: 30px;
	}
	.innerrow .insiderow {
		width:100%;
		}
	.completionpack {
		padding:10px 20px;
		}	
	.complitioninline {
		width:100%;
		float:none;
		margin-bottom:30px;
		}
	.complitioninline h2 {
		text-align:left;
		}
	.shortcutinline {
    float: none;
    width: 100%;
	margin-bottom:5px;
}		
.shortcutinline img {
	width:100%;
	}
.moreinfo, .bottomshortcut {
	padding:0px 20px;
	}
.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
	margin-left:0px !important;
	}						
	}	
</style>
<div class="howwecanhelp">
    	<div class="titlehowwecan">
        	<h2>Insurance & Guarantee</h2>
        </div>
        <div class="howwedesc">
        	<p>Build Team is fully insured with £2m Public Liability Insurance and £10m Employers Liability Cover, so in the unlikely event of accident or damage caused by one of our employees, your property will be fully covered by our insurance. On top of this, we include a ten year structural guarantee for all extension projects. At the end of the Build Phase, we issue a full completion pack which includes all of the important documents you might need in the future.</p>
        </div>
    </div>
</div>

<div class="middlerow">
	<div class="middleinner">
    	<div class="innerrow forborder">
        	<div class="insiderow">
            	<div class="upgradeimage">
                    <img src="/images/guarantee/10.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>10 Year Guarantee</h2>
                </div>
                <div class="upgradedesc">
                    <p>Build Team provide a 10 year<br>structural guarantee on your<br>ground floor extension. Please contact<br>us for more information.</p>
                </div>
            </div>
            <div class="insiderow">
            	<div class="upgradeimage">
                    <img src="/images/guarantee/pigi.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>Fully Insured</h2>
                </div>
                <div class="upgradedesc">
                    <p>£2m Public Liability & £10m<br>Employers Liability. Contact us<br>if you would like to view our<br>insurance certificate.</p>
                </div>
            </div>
            <div class="insiderow">
            	<div class="upgradeimage">
                    <img src="images/guarantee/bag.jpg" alt="">
                </div>
                <div class="upgradetitle">
                    <h2>Terms & Conditions</h2>
                </div>
                <div class="upgradedesc">
                    <p>Full Terms & Conditions are<br>issued with all client proposals.<br>Please contact us if you would<br>like a copy.</p>
                </div>
            </div>
        </div>
        
    </div>
</div>
<div class="completionpack">
	<div class="completioninner">
    	<h1>The <strong>Completion Pack</strong></h1>
        <p>Both the Design Phase & Build Phase are a lengthy process, during which lots of information is issued at different intervals. To make things easy for you, we collate all of the important information at the end of each phase and issue a completion pack, so you have everything relevant in one place.</p> 
        <div class="complitionrow">
        	<div class="complitioninline">
            	<h2><strong>Design:</strong> Completion Pack</h2>
                <ul>
                	<li>Full Building Reg Pack (detailed drawings)</li>
                	<li>Structural Calculations</li>
                	<li>All planning documentation</li>
                	<li>Party wall agreements (if applicable)</li>
                	<li>Detailed Schedule of Works</li>
                	<li>CDM Regulation documentation (if applicable)</li>
                	<li>Design Risk Assessment Form (if applicable)</li>
                </ul>
            </div>
        	<div class="complitioninline">
            	<h2><strong>Build:</strong> Completion Pack</h2>
                <ul>
                	<li>Completion Certificate from Building Control</li>
                	<li>Party wall agreements (if applicable)</li>
                	<li>Gas Safety Certificate</li>
                	<li>NIC EIC Part P Electrical Certificate</li>
                	<li>Any guarantees for appliances supplied by Build Team</li>
                	<li>Any guarantree for fittings supplied by Build Team</li>
                	<li>Boiler manual (if applicable)</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="moreinfo">
    	<h1>Find Out More</h1>
    </div>   
<div class="bottomshortcut">
	<div class="shortcutinline">
    	<a href="https://www.buildteam.com/about-us/why-choose-page.html ">
            <div class="shortcutwarp">
                <img src="images/specialist/1.jpg" alt="">
                <div class="shortcutdesc"><span>Why Choose Build Team</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/news/press_releases.html">
            <div class="shortcutwarp">
                <img src="images/specialist/2.jpg" alt="">
                <div class="shortcutdesc"><span>Read Our Latest Feature</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/download_our_brochure.html">
            <div class="shortcutwarp">
                <img src="images/specialist/3.jpg" alt="">
                <div class="shortcutdesc"><span>Download Our Brochure</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
        <a href="https://www.buildteam.com/what-we-do/faq.html">
            <div class="shortcutwarp">
                <img src="images/specialist/4.jpg" alt="">
                <div class="shortcutdesc"><span>Design & Build FAQs</span></div>	
            </div>
         </a>
    </div>
</div>
<script src="js/slick.min.js"></script>

<script>
$(".helpmechoose").click(function(){
	$(".quizwrap").animate({left:"0%"});
	});
$(".closebuttonquiz").click(function(){
	$(".quizwrap").animate({left:"-100%"});
	});	
</script>
<script>
$('.slick-slider').slick({
  slidesToShow: 4,
  slidesToScroll: 4,
  autoplay: false,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '10px',
        slidesToShow: 1
      }
    }
	]
});
</script>
<?php require_once('./inc/footer.inc.php'); ?>