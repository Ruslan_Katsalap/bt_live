<?php require_once('./inc/header.inc.php'); ?>
<?php

$upgradesend = false;

$date = new DateTime('now');
$newdate = $date->format('d-m-Y H:i:s');
//$query = "INSERT INTO user_post (date) VALUES ('$date')";
//echo $newdate;
$day = $_POST['day'];
$fullday = $day . $_POST['time'];

if($_POST['name'] != '' && $_POST['address'] !=''){	
$upgradesend = true;

mysqli_query($dbconn,"insert into upgrade values(null,'".$_POST['name']."','".$_POST['address']."','" .date('Y-m-d H:i:s'). "','1')");
}
?>
<style>
@font-face {
    font-family: 'libre_baskervillebold';
    src: url('/fonts/baskerville/librebaskerville-bold-webfont.woff2') format('woff2'),
         url('/fonts/baskerville/librebaskerville-bold-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}
.upgradetitlewrap p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #74787f;
    line-height: 24px;
    font-weight: normal;
}
.upgradetitlewrap h1 {
   font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	padding-top: 0px;
}
.upgradeleft {
	float:left;
	width:50%
	}
.upgraderight {
	float:right;
	width:48%;
	}
.upgradeimage img {
    width: 100%;
}	
.upgradepoints ul li {
    margin-bottom: 5px;
	font-size:20px;
	text-indent: -.7em;
	padding-left: 1em; 
	 color: #74787f;
}
.upgradepoints ul li::before {
  content: "• ";
  color: #114776;
    font-size: 28px;
    line-height: 20px;
    vertical-align: middle;
}
.upgradepoints ul {
    list-style: none;
    font-size: 16px;
    color: #a7a9ac;
}
.upgradepoints h1 {
    margin-bottom: 0px !important;
	padding-top: 0px;
	font-family: 'libre_baskervillebold' !important;
}	
.upgradeinner {
    padding: 40px 0px;
    border-top: 2px solid #e9e9ea;
    border-bottom: 2px solid #e9e9ea;
    margin: 20px 0px;
}
.floaticons {
    position: absolute;
    width: 150px;
    top: -105px;
    right: -20px;
}
.upgradepoints {
	position:relative;
	}
.floaticons img {
    width: 130px;
}
.upgradetitlewrap {
    padding-bottom: 50px;
}

.upgradepoints p {
    font-size: 16px;
    color: #74787f;
    margin-top: 83px;
}
.upgardebutton {
	background:#edf2f6;
	padding:20px 0px;
	}
.upgardebutton h1 {
   
    padding: 20px 0px;
    text-align: center;
    margin-bottom: 0px !important;
    padding-top: 0px;
    padding-bottom: 0px !important;
    
}	
.upgradeimageinner {
    height: 130px;
    width: 130px;
    text-align: center;
    background: #edf2f6;
    display: table-cell;
    vertical-align: middle;
    border-radius: 100px;
}
.upgradeimageinner p {
	margin-top: 0px;
    color: #0f4776;
    font-weight: bold;
    padding: 0px;
	}
.popupinner {
    width: 500px;
    margin: 0 auto;
    top: 35%;
    position: relative;
    background: #edf2f6;
    text-align: center;
}
.popupform {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(15, 71, 118,0.8);
    z-index: 11111;
}
.popupfield form input[type=text] {
    width: 100%;
    border: none;
    height: 35px;
    margin-bottom: 10px;
	text-indent:10px;
}
.popupfield {
    padding: 20px 40px;
    text-align: left;
}
.popupfield form input[type=submit] {
    background: #0f4778;
    padding: 10px 30px;
    border: none;
    color: #FFF;
    font-size: 16px;
}
.popupfield h2 {
    font-size: 22px;
    color: #0f4778;
}
.popupfield p {
    color: #969696;
    font-size: 16px;
    padding-top: 0px;
    margin: 10px 0px;
}
.popupclose {
    position: absolute;
    right: 10px;
    top: 5px;
    font-size: 26px;
    color: #0f4778;
}
.popupform {
	display:none;
	}
.upgardebutton {
	cursor:pointer;
	}
.upgradethankyoupop{
    display:none;
    position: fixed;
    height: 100%;
    width: 100%;
    background: rgba(16, 70, 116, 0.67);
    top: 0px;
    z-index: 50;
    left: 0px;
}	
.thanksinner {
    position: relative;
    top: 35%;
    height: 130px;
    max-width: 350px;
    background: #FFF;
    left: auto;
    right: auto;
    margin: 0 auto;
    padding: 20px 20px;
    width: 100%;
    text-align: left;
}
.thanksinner h2 {
   
    padding-bottom: 15px;
    color: rgb(16, 70, 116);
    font-size: 20px;
}	
.thanksinner p {
	font-size:17px;
	font-weight:normal;
	color:#969696;
	}
.upgradeimage {
	position:relative;
	}
.stickerimage {
    position: absolute;
    height: 80px;
    width: 80px !important;
    bottom: 10px;
    left: 10px;
}		
@media (max-width:640px) {
	.upgradetitlewrap h1 {
		font-size:22px !important;
		}
	.upgradetitlewrap p {
		font-size:18px; 
		}
	.upgradeleft, .upgraderight {
		width:100%;
		}
	.floaticons {
		display:none;
		}
	.upgradepoints p {
		margin-top:0px;
		}
	.upgradepoints ul li {
		font-size:18px;
		}
	.upgradepoints h1 {
		font-size:22px !important;
		margin-top:20px;
		}				
	.upgardebutton h1 {
		font-size:22px !important;
		}
	.popupinner {
		width:90%;
		}		
	.popupfield {
		padding:20px 10px;
		}
	.upgradetitlewrap {
		padding-bottom:20px;
		}				
	}	
</style>
<div class="upgradewrap">
	<div class="upgradetitlewrap">
    	<h1>Upgrade to Premium Site visit</h1>
        <p>Premium Site Visits have been designed for homeowners who are seeking an advice based site visit. You’ll meet with a senior member of our Architectural Team, and they will offer advice on design & layout options, party wall considerations and planning feasibility. We take some additional measurements which enables our team to create some initial floorplans for you based on your discussion. We then invite you for a follow up meeting at our Head Office in SW9, where we begin to develop your vision with you. This is also a perfect opportunity to run through your quote and answer any remaining questions you have.</p>
    </div>
    <div class="upgrademiddle">
    	<div class="upgradeinner">
            <div class="upgradeleft">
            	<div class="upgradeimage">
                	<img src="images/upgrade/banner.jpg" alt="">
                    <img class="stickerimage" src="images/upgrade/sticker.png" alt="">
                </div>
            </div>
            <div class="upgraderight">
            	<div class="upgradepoints">
                	<h1>Premium Perks:</h1>	
                    <div class="floaticons"><div class="upgradeimageinner">
                    	<p>Refundable<br>on<br>Instruction*</p>
                    </div></div>
                    <ul>
                    	<li>In-depth design consultation</li>
                    	<li>Senior member of our Architectural Team</li>
                    	<li>Planning history research specific to your area</li>
                    	<li>Flexible times and dates</li>
                    	<li>Follow up meeting with Architectural drawings</li>
                    </ul>
                    <p>*Refundable if instructed within 30 days of site visit</p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="upgardebutton">
           	<h1>Upgrade Now For £125 + VAT*</h1>
        </div>
    </div>
</div>
<div class="popupform">
	<div class="popupinner">
    	<div class="popupfield">
        	<h2>Want to Upgrade to a Premium Site Visit?</h2>
            <p>Please enter a few details</p>
        	<form method="post" action>
            	<input type="text" name="name" placeholder="Full name*" required>
                <input type="text" name="address"  placeholder="Address*" required>
                <input type="submit">
            </form>
            <div class="popupclose"><span>x</span></div>
        </div>
    </div>
</div>
<div class="upgradethankyoupop">
    	<div class="thanksinner">
        	<h2>Thank you!</h2>
            <p>Thank you for upgrading to a premium site visit. A member
of the team will be in touch to confirm.</p>
			<a class="closeit" href="javascript:void(0);">Ok</a>
            
        </div>
    </div>

<script>
$(".upgardebutton h1").click(function(){
	$(".popupform").fadeIn(300);
	});
$(".popupclose").click(function(){
	$(".popupform").fadeOut(300);
	});	
$(".closeit").click(function(){
	$(".upgradethankyoupop").fadeOut(300);
	});	
</script>
<script>
$(document).ready(function(e) {
    <? if($upgradesend == true){?>
$(".upgradethankyoupop").fadeIn(200);
<? } ?>
});

</script>
<?php require_once('./inc/footer.inc.php'); ?>