<?php



require_once('./inc/header.inc.php');



?>

  	<div class="left">

    	<h1>Tips for Greener Living 1</h1>

      <p>At Build Team, we're committed to helping our customers to enjoy a Greener environment - and in doing so, save money on heating and energy bills. Here are just a few simple tips to help reduce your carbon footprint:</p>
      
      <p>Give your house an energy-efficiency audit. Answer a quick questionnaire on the <a href="http://energysavingtrust.org.uk/home_improvements" target="_blank" rel="nofollow">Energy Saving Trust's site.</a></p>
      
      <p><u>Switch to energy-saving bulbs</u><br />
      They cost &pound;3, but last eight times longer than ordinary bulbs. Replacing an old 100W bulb with a 20W green bulb should save you &pound;7 a year: not much, until you add up the number of bulbs in your house.</p>
      
      <p><u>Insulate</u><br />
      Installing the recommended 270mm of loft insulation alone can save you &pound;110 a year.  If your house was built after 1920, a third of heating is lost through the walls. Let us know if you need help, and we will happily provide you with a quotation to supply new insulation, or upgrade your existing.</p>
      
      <p>Put an insulating jacket on your hot water tank. This should cost less than &pound;10, yet if everyone in the UK fitted one then Carbon Dioxide emissions would be cut by 0.45 million tonnes (source: Centre for Sustainable energy). If you do have a tank jacket, fit another over the top.</p>
      
      <p><u>Draught-proof any gaps</u><br />
      There are lots of inexpensive draught-excluding measures you can use, including weather stripping, draught-excluder brushes, long, lined curtains (though don't cover warm radiators with them) and fabric "sausages" for the bottom of doors. Draught proofing your doors and windows will save you around &pound;20 and 140kg of CO<sub>2</sub> a year.</p>
      
      <p><u>Fit energy efficient doors</u><br />
      Around one fifth of the heat in a home can be lost through the doors or windows and it is vitally important that when you are replacing these you choose products that offer the best energy efficiency. Most manufacturers publish the U-Values of their doors and windows, which is the rate of heat loss through the product's surface.</p>
      
      <p><u>Double glazing</u><br />
      This can cut heat loss by half, and Build Team can supply and fit replacement double glazed windows in a wide variety of finishes - including period sash windows if your home is listed or within a conservation area. The most efficient windows carry the Energy Saving Recommended logo.</p>
      
      <p><u>Save Water</u><br />
      About 40 per cent of water used daily in the home goes down the pan. A low flow toilet reduces this, and recycling systems for rainwater can cut mains water usage by half</p>
      
      <p><u>Trap the sun</u><br />
      Solar panels will heat up your water, but photovoltaic (PV) panels can convert the sun's energy directly into electricity.</p>

		</div>

<?php



require_once('./inc/footer.inc.php');



?>