<?php
require_once('./inc/util.inc.php');
require_once('emailSentToArchitech.php');
require_once('./inc/header.inc.php');
require_once('saveArchitech.php');

// ********This is CRM integration code*********

require_once($_SERVER["DOCUMENT_ROOT"]."/crm_config.php");
							
if(isset($_POST['email']) && isset($_POST['number'])) {
$now = strftime("%Y-%m-%dT%H:%M:%S");
list($name,$surname) = explode(" ",$_POST['fullname']);

$comment = "Planning URL: ".$_POST['plannnig']."\r\n";
$comment.= "When are you looking to start?: ".$_POST['lookingtostart']."\r\n";

$comment.= "Message: \r\n".$_POST['message'];

if(isset($_FILES["file"])) {

$file.= "\r\n File upload: ";

foreach($_FILES["file"]['name'] AS $key => $value){

$file.= '<a target="_blank" href="http://www.buildteam.com/images/architectural/upload/'. $_FILES["file"]['name'][$key];

$file.= '">'.$_FILES["file"]['name'][$key]. "</a>\r\n";

}

$comment.= "\r\n ".$file;

}

$data = array(
    "NAME" =>  $name,
    "LAST_NAME" => $surname,
    "UF_CRM_1490175334" => $now,
    "EMAIL_HOME" => $_POST['email'],
    "PHONE_HOME" =>$_POST['number'],
    "TITLE" => $_POST['fullname'],
    "UF_CRM_1500629522" => 146,
    "SOURCE_DESCRIPTION" => "BT",
    "ADDRESS" => $_POST['address'],
    "COMMENTS" =>  $comment
  );
 CrmClient::$SOURCE_ID = 6;

 if(!CrmClient::sendToCRM($data)) {
      // echo 'error send to crm';
 }
}
// ********This is CRM integration code*********



?>
<style>
body {
    font-family: 'Open Sans', Arial, sans-serif;
    font-size: 16px;
	font-weight:400;
}	
.salesadded {
    width: 660px !important;
}
.main {
	max-width:980px !important;
	}
.commonthree .comoninlineblock {
    display: inline-grid;
    max-width: 30.7%;
    width: 100%;
	padding:0px 10px;
}
.topara h4 {
    font-size: 30px !important;
    color: #003c70;
    text-transform: capitalize !important;
    font-weight: bold;
    font-family: calibri !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
}
.topara p {
	line-height:20px;
	color: #75787f;
	font-size:17px;
	text-align:justify;
	}
.ringwrap img {
    max-width: 120px;
}
.ringwrap {
	text-align:center;
	height:120px;
	}
.ringinfo p {
	color: #73767d;
    font-size: 17px;
    max-width: 93%;
    margin: 0 auto;
    line-height: 20px;
    padding-top: 0px;
	}	
.ringsimage .comoninlineblock {
	text-align:center;
	}
.ringsimage {
	padding: 20px 0px;
    margin: 20px 0px;
    border-top: 2px solid #8ca5bc;
    border-bottom: 2px solid #8ca5bc;	
	}
.formfields label {
    display: block;
    margin-bottom: 5px;
	text-align:left;
	color:#6687a6;
}	
.formfields input {
    width: 94%;
    height: 35px;
	padding:0px 5px;
    border-radius: 8px;
    box-shadow: none;
    border: 1px solid #6687a6;
}
.formfields {
    margin-bottom: 15px;
	
}
.terms p {
    font-size: 12px;
    text-align: justify;
    color: #75787f;
    line-height: 15px;
    position: relative;
    top:15px;
}
.formwrap input[type=file] {
    border: none;
	position: relative;
    top: 7px;
	left:-12px;
}
.formfields textarea {
	height: 105px;
    width: 96%;
	 border-radius: 8px;
    box-shadow: none;
    border: 1px solid #6687a6;
	}
.formwrap .firstfrom {
	text-align:left;
	}
.formwrap .secondfrom {
	text-align:center;
	}
.formwrap .thirdfrom {
	text-align:right;
	}	
.formwrap input[type=button] {
	background:#294b7a !important;
	color:#FFF !important;
	font-size:16px !important;
	height:38px !important;
	}
.ringmob {
	display:none;
	}	
.thankyoupop {
	display:none;
    position: fixed;
    height: 100%;
    width: 100%;
    background: rgba(16, 70, 116, 0.67);
    top: 0px;
    left: 0px;
}	
.thanksinner {
    position: relative;
    top: 35%;
    height: 130px;
    max-width: 350px;
    background: #FFF;
    left: auto;
    right: auto;
    margin: 0 auto;
    padding: 20px 20px;
    width: 100%;
}
.thanksinner p {
	font-size:14px;
	}
.thanksinner h4 {
    border-bottom: 2px solid #6687a6;
    padding-bottom: 5px;
	color:rgb(16, 70, 116);
}		
.thanksinner a {
    background: #6687a6;
    display: inline-block;
    padding: 10px 20px;
    color: #FFF;
    margin-top: 25px;
    border-radius: 10px;
}
.ringinfo p span {
	display:block;
	}
.ringinfo {
    height: 115px;
}	
.broform100.checkboxbro.broleft span {
    display: inline-block;
    width: 90%;
    text-align: justify;
    vertical-align: top;
}
.broform100.checkboxbro.broleft input {
    width: 20px;
    height: 20px;
    display: inline-block;
}		
@media (max-width:640px) {
	.commonthree .comoninlineblock {
		width:100%;
		display:block;
		max-width:100%;
		padding:0px;
		margin-bottom:20px;
		}
	.ringwrap {
		height:120px;
		}	
	.formfields input {
		width:95%;
		}	
	.formfields textarea {
		width:97%;
		}
	.ringmob .ringwrap {
		display:table-cell;
		}
	.ringmob .comoninlineblock{
		display:table;
		width:100%;
		text-align:center;
		}	
	.ringmob .ringwrap p {
		font-weight:bolder;
		color:#75787f;
		}	
	.ringmob .ringwrap p span {
   
    position: relative;
    left: -10px;
    color: #ff5d00;
	transition:ease-in-out 0.3s;
    /* font-size: 20px; */
}	
.rotateit {
	color: #0f4676 !important;
	}
.rotateit span {
	transform:rotate(90deg);
	display:inline-block;
	transition:ease-in-out 0.3s;
	}
	.ringdesk {
		display:none;
		}	
	.ringmob {
		display:block;
		}
	.formwrap input[type=submit] {
		width:250px;
		}
	.formwrap .thirdfrom {
		text-align:left;
		}
	.ringwrap img {
    	max-width: 100px;
		}	
	.formfields {
		margin-bottom:20px;
		}	
	.rotateit {
		transform:skew(180deg);
		}	
	.thanksinner {
		width:80%;
		}
	.ringinfo {
    height: 80px;
}
				
	}				
</style>

<div class="topara">
	<h4>Do you already have architectural plans?</h4>
    <p>We recognise that some clients have already instructed an architect and therefore may already have architectural plans, planning permission and possibly a full structural drawing pack. Don't worry if you are only part way through this process; when we receive your drawings, we will audit what you already have and inform you if there are any gaps in your design information. We will also offer a fixed fee to fill in these gaps, which can also include the dispatch of neighbourly party wall matters.</p>
</div>

<div class="commonthree ringsimage ringdesk">
	<div class="comoninlineblock">
    	<div class="ringwrap"><img src="images/architectural/ring-1.png" alt=""></div>
        <div class="ringinfo">
        	<p><span>Upload your plans by</span> <span>submitting our form below.</span> <span>Please include any additional</span> <span>information that you have,</span> such as planning approval.</p>
        </div>
    </div>
	<div class="comoninlineblock">
    	<div class="ringwrap"><img src="images/architectural/ring-2.png" alt=""></div>
        <div class="ringinfo">
        	<p><span>We will contact you within</span> <span> one working day to confirm</span><span>receipt of your enquiry</span><span>and book an initial</span>visit at your property.</p>
        </div>
    </div>
	<div class="comoninlineblock">
    	<div class="ringwrap"><img src="images/architectural/ring-3.png" alt=""></div>
        <div class="ringinfo">
        	<p><span>We will prepare a detailed</span> <span>schedule of costs and invite</span><span>you to our office in Clapham</span><span>to discuss your project</span><span>in more detail.</span></p>
        </div>
    </div>
</div>

<div class="commonthree ringsimage ringmob">
	<div class="comoninlineblock">
    	<div class="ringwrap" data-desc="Upload your plans by selecting the Ã¢ÂÂfile uploadÃ¢ÂÂ button which can be found
at the foot of our form."><img src="images/architectural/ring-1.png" alt=""><p class="rotateit"><span>></span>UPLOAD</p></div>
        <div class="ringwrap" data-desc="We will contact you within 24 hours to confirm receipt of your enquiry and
book an initial site visit."><img src="images/architectural/ring-2.png" alt=""><p><span>></span>CONTACT</p></div>
        <div class="ringwrap" data-desc="Following the initial site visit, we will prepare a detailed schedule of
costs and invite you for a further meeting at our office in Clapham to
discuss further."><img src="images/architectural/ring-3.png" alt=""><p><span>></span>QUOTE</p></div>
       
    </div>
     <div class="ringinfo">
        	<p>Upload your plans by submitting our form below. Please include any additional information that you have, such as planning approval.</p>
        </div>
</div>

<div class="commonthree formwrap">
	<form action="" method="post" enctype="multipart/form-data" id="formcontact" name="formcontact">
	<div class="comoninlineblock firstfrom">
    	<div class="formfields">
            <label for="fullname">Full name*</label>
            <input type="text" name="fullname" id="fullName"  />
        </div>
        <div class="formfields">
            <label for="address">Address*</label>
            <input type="text" name="address" id="address" />
        </div>
        <div class="formfields">
            <label for="email">Email*</label>
            <input type="email" name="email" id="email" />
        </div>
        <div class="formfields">
            <label for="">Telephone</label>
            <input type="tel" name="number">
        </div>
    </div>
	<div class="comoninlineblock secondfrom">
    	<div class="formfields">
            <label for="">Planning URL</label>
            <input type="url" name="plannnig">
        </div>
        <div class="formfields">
            <label for="">When are you looking to start?</label>
            <input type="text" name="lookingtostart">
        </div>
        <div class="formfields"  id="cometoform">
            <label for="">Message</label>
            <textarea name="message" id=""></textarea>
        </div>
    </div>
	<div class="comoninlineblock thirdfrom">
    	<div class="formfields">
            <label for="">File upload</label>
            <input type="file" name="file[]" multiple />
        </div>
         <div class="enquirywrap">
                <div id="recaptcha3" class="g-recaptcha" data-sitekey="6LcZGKYUAAAAAHxPrL02cGYHuDzEfpUBpWCvNRgt"></div>
            </div>
        <div class="formfields">
            <label for="">&nbsp;</label>
            <input type="button" value="Submit" onclick="submitValidation();">
        </div>
        <div class="formfields">
            <div class="terms">
            	<div class="broform100 checkboxbro broleft">
                                <input type="checkbox" name="readySubscribe" value="Yes"><span>Sign up to our newsletter and receive updates on our latest blogs, news items and special offers – see our <a href="https://www.buildteam.com/privacy-policy.html">privacy policy</a> for more information on how we use your personal data.</span>
                            </div>
            </div>
        </div>
    </div>
    </form>
    <div class="thankyoupop">
    	<div class="thanksinner">
        	<h4>Thank you for your submission</h4>
            <p>We will contact you within 24 hours to confirm receipt of your enquiry
and book an initial site visit.</p>
			<a href="javascript:void(0);">Ok</a>
        </div>
    </div>
</div>
<script>
$(document).ready(function(e) {
    $(".ringmob .ringwrap").click(function(){
		var getdesc = $(this).attr("data-desc");
		//alert(getdesc);
		$(".ringmob .ringinfo p").html(getdesc);
		$(".comoninlineblock p").removeClass("rotateit");
		$(this).find("p").addClass("rotateit");
	});
});
function submitValidation(){
	var fName = document.getElementById("fullName").value;
	var address = document.getElementById("address").value;
	var email = document.getElementById("email").value;
	if(fName ==''){
		alert("Please fill all filed.");
		return false;
	}
	if(address ==''){
		alert("Please fill all filed.");
		return false;
	}
	if(email ==''){
		alert("Please fill all filed.");
		return false;
	}
	document.getElementById("formcontact").submit();
}
</script>
<script>
$(document).ready(function(e) {
    $('.ringwrap a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top - 150
            }, 1000);
            return false;
        }
    }
});
});
<? if($flage == true){?>
$(".thankyoupop").fadeIn(200);
<? } ?>
</script>
<script>
$(".thanksinner a").click(function(){
	$(".thankyoupop").fadeOut(200);
	});
</script>


<?php
require_once('./inc/footer.inc.php');
?>
