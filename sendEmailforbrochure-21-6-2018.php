<?
/* Request Brochure */
if( isset($_POST['firstName']) || isset($_POST['wFname']) || isset($_POST['readName']))

{
	//echo "done";
	$from_email         = 'info@buildteam.com'; //from mail, it is mandatory with some hosts
	$recipient_email    = $_POST['email'].','.$_POST['wEmail'].','.$_POST['readEmail']; //recipient email (most cases it is your personal email)
   $_POST["sender_email"] = "info@buildteam.com";
   $_POST["subject"] = "Build Team Brochure";
   $_POST["message"] = "Build Team Brochure";
    //Capture POST data from HTML form and Sanitize them,
   $sender_name    = filter_var("buildteam", FILTER_SANITIZE_STRING); //sender name
   $reply_to_email = filter_var($_POST["sender_email"], FILTER_SANITIZE_STRING); //sender email used in "reply-to" header
   $subject        = filter_var($_POST["subject"], FILTER_SANITIZE_STRING); //get subject from HTML form
   $message        = filter_var($_POST["message"], FILTER_SANITIZE_STRING); //message
   $boundary = md5("sanwebe");
        //header
        // To send HTML mail, the Content-type header must be set
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $headers .= "From:".$from_email."\r\n";
    $headers .= "Reply-To: ".$reply_to_email."" . "\r\n";
	$name = $_POST['firstName'];
	$body1 ="<html>
				<head>
					<title>Download Our Brochure</title>
				</head>
				<body>
					<table style='text-align:left; font-family:calibri;'>
						<tr>
							<td>Dear $name</td>
							
						</tr>
						<tr>
							<td><br></td>
						</tr>
						<tr>
							<td style='text-align:left;'>Thank you for visiting our website and requesting a Brochure, your form was submitted successfully and you should receive your postal copy in 2 -3 working days.</td>
						<tr>
							<td><br></td>
						</tr>
						</tr>
						<tr>
							<td style='text-align:left;'>I thought you might be interested in booking in a Site Visit with a member of our architectural design team. They will be able to advise you on the feasibility of your project and start off that initial creative discussion. Following the site visit, we will issue a quote for both the design and build phase. You can find out more about the type of site visits we offer <a href='https://www.buildteam.com/getting-started/book-site-visit.html'>here.</a></td>
						</tr>
						<tr>
							<td><br></td>
						</tr>
						<tr>
						   <td style='text-align:left;'>Please reply to this email or call 020 7495 6561 if you would like to book a site visit.</td>
						   
						</tr>
						<tr>
							<td><br></td>
						</tr>
						<tr>
						   <td style='text-align:left;'>We look forward to hearing from you.</td>
						</tr>
						<tr>
							<td><br></td>
						</tr>
						<tr>
						   <td style='text-align:left;'>Kind regards,</td>
						</tr>
						<tr>
						   <td style='text-align:left;'><strong>Liam Davies</strong></td>
						</tr>
						<tr>
						   <td style='text-align:left; color:#969696;'>Architectural Assistant</td>
						</tr>
						<tr>
						   <td style='text-align:left; color:#969696;'>Build Team</td>
						</tr>
				</table>
				</body>
				</html>";
	
	
		
		$body2 ="<html>
					<head>
						<title>Download Our Brochure</title>
					</head>
					<body>
						<table style='text-align:left; font-family:calibri;'>
							
							<tr>
								<td style='text-align:left;'>Thank you for visiting our website and downloading our Brochure.</td>
				
							</tr>
							<tr>
								<td style='text-align:left;'>I thought you might be interested in booking in a Site Visit with a member of our architectural design team. They will be able to advise you on the feasibility of your project and start off that initial creative discussion. Following the site visit, we will issue a quote for both the design and build phase. You can find out more about the type of site visits we offer <a href='https://www.buildteam.com/getting-started/book-site-visit.html'>here.</a></td>
							</tr>
							<tr>
							<td><br></td>
						</tr>
							<tr>
							   <td>
									Please reply to this email or call 020 7495 6561 if you would like to book a site visit.<br>
									We look forward to hearing from you.<br>
								</td>
							</tr>
							<tr>
							<td><br></td>
						</tr>
							<tr>
						   <td style='text-align:left;'>Kind regards,</td>
						</tr>
						<tr>
						   <td style='text-align:left;'><strong>Liam Davies</strong></td>
						</tr>
						<tr>
						   <td style='text-align:left; color:#969696;'>Architectural Assistant</td>
						</tr>
						<tr>
						   <td style='text-align:left; color:#969696;'>Build Team</td>
						</tr>
					</table>
					</body>
					</html>";
		
		
		
			$body3 ="<html>
						<head>
							<title>Download Our Brochure</title>
						</head>
						<body>
							<table style='text-align:left; font-family:calibri;'>
								
								<tr>
									<td style='text-align:left;'>Thank you for visiting our website and reading our Brochure.</td>
					</tr>
					<tr>
							<td><br></td>
						</tr>
								<tr>
								
									<td style='text-align:left;'>I thought you might be interested in booking in a Site Visit with a member of our architectural design team. They will be able to advise you on the feasibility of your project and start off that initial creative discussion. Following the site visit, we will issue a quote for both the design and build phase. You can find out more about the type of site visits we offer <a href='https://www.buildteam.com/getting-started/book-site-visit.html'>here.</a></td>
								</tr>
								<tr>
							<td><br></td>
						</tr>
								<tr>
								   <td>
										Please reply to this email or call 020 7495 6561 if you would like to book a site visit.<br>
										We look forward to hearing from you.<br>
										
								   </td>
								</tr>
								<tr>
							<td><br></td>
						</tr>
								<tr>
						   <td style='text-align:left;'>Kind regards,</td>
						</tr>
						<tr>
						   <td style='text-align:left;'><strong>Liam Davies</strong></td>
						</tr>
						<tr>
						   <td style='text-align:left; color:#969696;'>Architectural Assistant</td>
						</tr>
						<tr>
						   <td style='text-align:left; color:#969696;'>Build Team</td>
						</tr>
						</table>
						</body>
						</html>";
	
	$mail->SMTPOptions = array( 'ssl' => array( 'verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true ) );
	
	if(isset($_POST['firstName'])){
		$sentMail = @mail($recipient_email, $subject, $body1, $headers);
	}
	
	if(isset($_POST['wFname'])){
		$sentMail = @mail($recipient_email, $subject, $body2, $headers);
			}
		
	if(isset($_POST['readName'])) {
		$sentMail = @mail($recipient_email, $subject, $body3, $headers);
	}
		
    //$sentMail = mail($recipient_email, $subject, $body, $headers);
	    if($sentMail) //output success or failure messages
		
    	{      
		//echo "mail sent";
    }else{
        echo ('Could not send mail! Please check your PHP mail configuration.');  
    }
}

?>