<?php

require_once('./inc/header.inc.php');

?>
<style>
.full {
	font-size:16px;
	}
.check {
	padding-left: 20px;
    margin-top: 10px;
	}
</style>
  	<div class="full">
    	<h1>Privacy Policy</h1>
      <p>Build Team appreciates your visit to our website (“Build Team Website”) and your interest in our services. Your privacy is important to us and we want you to feel comfortable when viewing and using our website. We take care to protect the personal data collected, processed and used during visits to the Build Team Website. </p>
      <p>By accessing the Build Team Website you accept this Privacy Statement (“Statement”). </p>
      <p>If you do not agree to this Statement, please do not proceed to further web pages of the Build Team Website. </p>
      <p><b>What is the purpose of this Statement? </b></p>
      <p>Build Team, as well its subsidiaries, are committed to safeguarding the personal information that we collect from individuals who use the Build Team Website. Accordingly, Build Team has developed this Statement to describe how data will be collected from users of the Build Team Website, and the purposes for which Build Team may collect, use, share and disclose the data. </p>
      <p>This Statement applies to any information obtained by Build Team through the use of the Build Team Website. It is not applicable to any Internet websites controlled by third parties not affiliated with Build Team that the Build Team Website may link to (“Third Party Sites”). Please review the privacy policy of Third Party Sites as Build Team is not responsible for and has no influence on the content or the privacy practice of Third Party Sites.</p>
      <p>The terms of this Statement are subject to any contractual terms you have entered into with Build Team, including its subsidiaries and any applicable laws and regulations. </p>
      <p><b>What information do we collect from you and for what purpose?</b></p>
      <p>When you visit the Build Team Website, our web server automatically records details about your visit (for example, your IP address, the website from which your visit us, the type of browser used, the Build Team Pages visited including the date and duration of your visit).</p>
      <p>In addition, we collect personal data which you chose to provide during your visit. For example when you enter personal details (eg. name, address, email address, phone number, quote details) on a form or when you sign up for an email newsletter. </p>
      <p>Build Team will use your personal data:</p>
      <ul class="check">
      	<li>For the purposes of technical administration, </li>
        <li>For the purposes of research and development, </li>
        <li>For user administration and marketing,</li>
        <li>To inform you about our services and products, and </li>
        <li>For such purposes as otherwise specified. </li>
        
      </ul>
      <p>Build Team respects applicable laws and regulations in its use of personal data. </p>
      <p><b>How do we collect and store information from you?</b></p>
      <p>Build Team uses tracking technology such as cookies and tags to gather information as outlined above (see: “What information do we collect from you and for what purpose”) to understand how visitors use the Build Team Website.  </p>
      
     	<p>Tracking technology helps us manage and improve the usability of the Build Team Website, for example by detecting whether there has been any contact between your computer and us in the past and to identify the most popular sections of the Build Team Website. </p>
        
      <p><b>Use of Cookies </b></p>
      <p>Cookies are text files containing small amounts of information, which your computer or mobile devise downloads when you visit a website. When you return to the website – or visit a website that uses the same cookies – they recognise these cookies and therefore your browsing device. Like most websites, we use cookies to do lots of different jobs, like letting you navigate between pages efficiently, remembering your preferences and generally improving your browsing experience. In addition to the cookies we use on this website, we also use cookies and similar technologies in some emails. These help us to understand whether you have opened an email and how you have interacted with it. </p>
      <p>Where available, the “Cookies” link will appear at the bottom of the page, notifying you of our intention to use cookies during your visit. By continuing to browse our website, you are agreeing to our use of cookies. Technical cookies are strictly necessary for the Build Team Website to work properly.</p>
      <p>In many cases, you can control tracking technologies using your browser. Please ensure that your browser setting reflects whether you wish to be warned about and/or accept tracking technologies (such as cookies) wherever possible. The specific capabilities of your browser and instructions on how to use them can be found in the manual or help file of your browser. </p>
      <p><b>To whom do we disclose the data collected from the Build Team Website? </b></p>
      <p>Build Team may disclose your information to its affiliates and third party providers for the purposes stated above. Our affiliates and third party providers who have access to personal data obtained through the Build Team Website are obliged to respect privacy. </p>
      <p>We may also disclose your information to governmental agencies or entities, regulatory authorities, or other persons in line with any applicable law, regulations, court order or official request and for the purposes of any guidelines issued by regulatory or other authorities, or similar processes as either required or permitted by applicable law. </p>
      <p><b>What security measures have we implemented to protect the information collected? </b></p>
      <p>Build Team has implemented reasonable technical and organisational security measures to protect the personal data collected by Build Team and collected via the Build Team Website.</p>
      
      <p><b>How do we treat electronic messages sent to and from Build Team?</b></p>
      <p>All electronic messages sent to and from Build Team are automatically retained in a distinct journaling system. </p>
      
      <p><b>How long do we store your data?</b></p>
      <p>Build Team will retain your personal information for the period necessary to fulfil the purposes outlined in this Privacy Policy unless a longer retention period is required or permitted by law. </p>
      
      <p><b>What should you consider when sending data over the Internet?</b></p>
      <p>The Internet is generally not regarded as a secure environment, and information sent via the Internet may be accessed by unauthorized third parties, potentially leading to disclosures, changes in content or technical failures. Even if both sender and receiver are located in the same country, information sent via the Internet may be transmitted across international borders and be forwarded to a country with a lower data protection level than exists in your country of residence. </p>
      <p>Please note that we accept no responsibility or liability for the security of your information whilst in transit over the Internet to Build Team. In order to protect your privacy we would like to remind you that you may choose another means of communication with Build Team, where you deem appropriate. </p>
      <p><b>How do we deal with information from individuals under the age of 18?</b></p>
      <p>The Build Team Website does not seek to collect personal data from individuals under the age of 18. Individuals under the ages of 18 should receive permission from their parent or guardian before providing any personal data to Build Team via the Build Team Website. </p>
      <p><b>How can you access or review your personal data?</b></p>
      <p>You may, where permitted by applicable law or regulation:</p>
      <ul class="check">
      	<li>Check whether we hold your personal data, or</li>
        <li>Ask us to provide you with a copy of your personal data, or</li>
        <li>Request we correct any of your personal data that is inaccurate, or</li>
        <li>Request we delete any data that we hold on you. </li>
      </ul>
      <p>Should you have a request regarding the processing of your personal data, please call us on 0207 495 6561. </p>
      <p><b>Updates to this Privacy Policy</b></p>
      <p>We may update this Privacy Policy from time to time, and we note the last revision at the bottom of the page. Any change to the Privacy Policy will become effective when we post the revised Privacy Policy on the Build Team Website. Your use of the website following these changes means that you accept the revised Privacy Policy. </p>
      <p><b>Concerns</b></p>
      <p>If you have any questions or complaints regarding this Privacy Policy, please contact us as described above. We will investigate your question, respond to your enquiry, and attempt to resolve any concerns regarding your privacy in question. If you do not receive acknowledgement of your complaint, or if your complaint is not satisfactorily addressed by Build Team within 30 days, then please contact the Information Commissioner’s Office. </p>
      <p>Last updated May 2018.</p>
		</div>
<?php

require_once('./inc/footer.inc.php');

?>