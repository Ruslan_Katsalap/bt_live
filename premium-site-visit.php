<?php require_once('./inc/header.inc.php'); ?>
<style>
@font-face {
    font-family: 'libre_baskervillebold';
    src: url('/fonts/baskerville/librebaskerville-bold-webfont.woff2') format('woff2'),
         url('/fonts/baskerville/librebaskerville-bold-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

}
.upgradetitlewrap p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #74787f;
    line-height: 24px;
    font-weight: normal;
	
}
.upgradetitlewrap h1 {
   font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 10px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	padding-top: 0px;
	border-bottom: 1px solid #003c70;
}
.upgradeleft {
	float:left;
	width:60%
	}
.upgraderight {
	float:right;
	width:38%;
	}
.upgradeimage img {
    width: 100%;
}	
.upgradepoints ul li {
    margin-bottom: 22px;
    font-size: 18px;
    color: #0f4778;
    text-align: left;
    /* margin-top: 20px; */
}
.upgradepoints ul {
    list-style: none;
    font-size: 16px;
    color: #a7a9ac;
}
.upgradepoints h1 {
    margin-bottom: 10px !important;
    padding-top: 0px;
    text-align: center;
    font-size: 24px !important;
    border-bottom: 2px dotted #0f4778;
    margin-bottom: 20px;
}
.upgradeinner {
    padding: 30px 0px;
    margin: 20px 0px;
}
.floaticons {
    position: absolute;
    width: 150px;
    
}
.upgradepoints {
	position:relative;
	}
.floaticons img {
    width: 130px;
}
.upgradetitlewrap {
    padding-bottom: 40px;
}

.upgradepoints p {
    font-size: 16px;
    color: #74787f;
    /* margin-top: 83px; */
    padding: 20px 0px;
    text-align: center;
    background: #0f4778;
}
.upgradepoints p a {
	color:#FFF;
	font-size:20px;
	}
.upgardebutton {
	background:#edf2f6;
	padding:20px 0px;
	}
.upgardebutton h1 {
   
    padding: 20px 0px;
    text-align: center;
    margin-bottom: 0px !important;
    padding-top: 0px;
    padding-bottom: 0px !important;
    
}	
.upgradeimageinner {
    height: 110px;
    width: 110px;
    text-align: center;
    background:rgba(255,255,255,0.8);
    display: table-cell;
    vertical-align: middle;
    border-radius: 100px;
}
.upgradeimageinner p {
	margin-top: 0px;
    color: #0f4776;
    font-weight: bold;
    padding: 0px;
	font-size:19px;
	}
.popupinner {
    width: 500px;
    margin: 0 auto;
    top: 35%;
    position: relative;
    background: #edf2f6;
    text-align: center;
}
.popupform {
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    background: rgba(15, 71, 118,0.8);
    z-index: 11111;
}
.popupfield form input[type=text] {
    width: 100%;
    border: none;
    height: 35px;
    margin-bottom: 10px;
	text-indent:10px;
}
.popupfield {
    padding: 20px 40px;
    text-align: left;
}
.popupfield form input[type=submit] {
    background: #0f4778;
    padding: 10px 30px;
    border: none;
    color: #FFF;
    font-size: 16px;
}
.popupfield h2 {
    font-size: 22px;
    color: #0f4778;
}
.popupfield p {
    color: #969696;
    font-size: 16px;
    padding-top: 0px;
    margin: 10px 0px;
}
.popupclose {
    position: absolute;
    right: 10px;
    top: 5px;
    font-size: 26px;
    color: #0f4778;
}
.popupform {
	display:none;
	}
.upgardebutton {
	cursor:pointer;
	}
.upgradethankyoupop{
    display:none;
    position: fixed;
    height: 100%;
    width: 100%;
    background: rgba(16, 70, 116, 0.67);
    top: 0px;
    z-index: 50;
    left: 0px;
}	
.thanksinner {
    position: relative;
    top: 35%;
    height: 130px;
    max-width: 350px;
    background: #FFF;
    left: auto;
    right: auto;
    margin: 0 auto;
    padding: 20px 20px;
    width: 100%;
    text-align: left;
}
.thanksinner h2 {
   
    padding-bottom: 15px;
    color: rgb(16, 70, 116);
    font-size: 20px;
}	
.thanksinner p {
	font-size:17px;
	font-weight:normal;
	color:#969696;
	}
.upgradeimage {
	position:relative;
	}
.stickerimage {
    position: absolute;
    bottom: 25px;
    left: 10px;
}
.upgradeimage .note {
    font-size: 16px;
    margin: 0px;
    padding: 0px;
    color: #0f4778;
}
.upgrainline {
    width: 24.7%;
    text-align: center;
    display: inline-block;
    padding-bottom:20px;
	position:relative;
}
.upgradetitle h2 {
    color: #0f4778;
    font-size: 18px;
}
.upgradedesc p {
    color: #9f9f9f;
    font-size: 16px;
}
.upgrainline:after {
    content: "";
    width: 1px;
    height: 120px;
    position: absolute;
    border-left: 2px dotted #9f9f9f;
    top: 35px;
    right: 0px;
}
.upgrainline:nth-child(4):after {
	display:none;
	}
.bottomshortcut {
	max-width:980px;
	margin:0 auto;
	}
.shortcutinline {
    float: left;
    width: 24.2%;
}
.shortcutinline:nth-child(3) {
    margin-right: 1%;
}
.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
    margin-left: 1%;
}
.shortcutdesc {
    background: rgba(255,255,255,0.7);
    padding: 10px;
    text-align: center;
    color: #0f4679;
    position: absolute;
    bottom: 0px;
    width: 100%;
    font-weight: bold;
    font-size: 18px;
}
.shortcutwarp {
    position: relative;
}
.shortcutwarp img {
    max-width: 100%;
}
.tenderprocess {
    background: #ecebeb;
    margin: 50px 0px;
}
.tenderpprocesstitle h1 {
    font-weight: normal;
    padding: 0px;
}
.upgrageicons .upgradeimage img {
    max-width: 90px;
    margin-bottom: 10px;
}	
.moreinfo h1 {
    font-size: 20px !important;
    border-bottom: 2px solid #0f4778;
}
@media (max-width:640px) {
	.upgradetitlewrap h1 {
		font-size:22px !important;
		}
	.upgradetitlewrap p {
		font-size:18px; 
		}
	.upgradeleft, .upgraderight {
		width:100%;
		}
	.floaticons {
		}
	.upgradepoints p {
		margin-top:0px;
		}
	.upgradepoints ul li {
		font-size:18px;
		}
	.upgradepoints h1 {
		font-size:22px !important;
		margin-top:20px;
		}				
	.upgardebutton h1 {
		font-size:22px !important;
		}
	.popupinner {
		width:90%;
		}		
	.popupfield {
		padding:20px 10px;
		}
	.upgradetitlewrap {
		padding-bottom:20px;
		}
	.upgrainline {
		width:100%;
		}
	.upgrainline:after {
		display:none;
		}		
	.shortcutinline {
		float: none;
		width: 100%;
		margin-bottom:5px;
	}		
	.shortcutinline img {
		width:100%;
		}
	.moreinfo, .bottomshortcut {
		padding:0px 20px;
		}
	.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
		margin-left:0px !important;
		}							
	}	
</style>
<div class="upgradewrap">
	<div class="upgradetitlewrap">
    	<h1>Premium Site Visit</h1>
        <p>Premium Site Visits have been designed for homeowners who are seeking advice and guidance from an early stage. These visits not only allow for extra time, but they are booked with an experienced member of our Architectural Team. They will offer advice on design & layout options, party wall considerations and planning feasibility. During the visit, we’ll take some additional measurements which enables our team to create some initial floorplans for you. We then invite you for a follow up meeting at our Head Office in SW9, where we begin to develop your vision with you. Our Premium Site Visit includes:</p>
    </div>
    
    <div class="upgrageicons">
    	<div class="upgrainline">
        	<div class="upgradeimage">
            	<img src="images/upgradepremium/1.jpg" alt="">
            </div>
            <div class="upgradetitle">
            	<h2>Flexible Appointments</h2>
            </div>
            <div class="upgradedesc">
            	<p>Fast tracked appointment <br>with flexible options (early <br>morning and evenings)</p>
            </div>
        </div>
    	<div class="upgrainline">
        	<div class="upgradeimage">
            	<img src="images/upgradepremium/2.jpg" alt="">
            </div>
            <div class="upgradetitle">
            	<h2>Fast Tracked Quote</h2>
            </div>
            <div class="upgradedesc">
            	<p>A detailed budget Design & <br>Build estimate sent within<br>one working day</p>
            </div>
        </div>
    	<div class="upgrainline">
        	<div class="upgradeimage">
            	<img src="images/upgradepremium/3.jpg" alt="">
            </div>
            <div class="upgradetitle">
            	<h2>Design & Layout Advice</h2>
            </div>
            <div class="upgradedesc">
            	<p>Creative advice on design & <br>layout options and planning<br>feasibility</p>
            </div>
        </div>
    	<div class="upgrainline">
        	<div class="upgradeimage">
            	<img src="images/upgradepremium/4.jpg" alt="">
            </div>
            <div class="upgradetitle">
            	<h2>Experienced</h2>
            </div>
            <div class="upgradedesc">
            	<p>Specialist advice from an<br>experianced Architectural<br>Designer</p>
            </div>
        </div>
    </div>
    <div class="upgrademiddle">
    	<div class="upgradeinner">
            <div class="upgradeleft">
            	<div class="upgradeimage">
                	<img src="images/upgrade/banner.jpg" alt="">
                   	<p class="note">*Refundable against subsequent Design Instruction</p>
                    <div class="stickerimage floaticons"><div class="upgradeimageinner">
                    	<p>Fixed<br>Fee of<br>£125 + VAT*</p>
                    </div></div>
                </div>
            </div>
            <div class="upgraderight">
            	<div class="upgradepoints">
                	<h1>Frequently Asked Questions <br>at Premium Site Visits</h1>	
                    
                    <ul>
                    	<li>I can’t visualise how the extension will look, are you able to help?</li>
                    	<li>What are the local borough planning guidelines, and how will I obtain planning permission for the scheme I want?</li>
                    	<li>What are the potential minefields of the Party Wall process? How should I approach my neighbour in the first instance?</li>                     	
                    </ul>
                    <p><a href="/book-visit.html?type=premium">Book Your Visit Now</a></p>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
    </div>
    <div class="moreinfo">
    	<h1>Find Out More</h1>
    </div>
    <div class="bottomshortcut">
	<div class="shortcutinline">
    	<a href="https://www.buildteam.com/watch_a_build.html">
            <div class="shortcutwarp">
                <img src="images/archiimage/1-1.jpg" alt="">
                <div class="shortcutdesc"><span>Watch A Build</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/getting-started/book-site-visit.html ">
            <div class="shortcutwarp">
                <img src="images/archiimage/1-2.jpg" alt="">
                <div class="shortcutdesc"><span>Book a Site Visit</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/project-gallery/gallery.html">
            <div class="shortcutwarp">
                <img src="images/archiimage/1-3.jpg" alt="">
                <div class="shortcutdesc"><span>Browse Our Gallery</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
        <a href="https://www.buildteam.com/party_wall.html">
            <div class="shortcutwarp">
                <img src="images/archiimage/1-4.jpg" alt="">
                <div class="shortcutdesc"><span>Party Wall Service</span></div>	
            </div>
         </a>
    </div>
</div>
</div>
<div class="popupform">
	<div class="popupinner">
    	<div class="popupfield">
        	<h2>Want to Upgrade to a Premium Site Visit?</h2>
            <p>Please enter a few details</p>
        	<form method="post" action>
            	<input type="text" name="name" placeholder="Full name*" required>
                <input type="text" name="address"  placeholder="Address*" required>
                <input type="submit">
            </form>
            <div class="popupclose"><span>x</span></div>
        </div>
    </div>
</div>
<div class="upgradethankyoupop">
    	<div class="thanksinner">
        	<h2>Thank you!</h2>
            <p>Thank you for upgrading to a premium site visit. A member
of the team will be in touch to confirm.</p>
			<a class="closeit" href="javascript:void(0);">Ok</a>
            
        </div>
    </div>

<script>
$(".upgardebutton h1").click(function(){
	$(".popupform").fadeIn(300);
	});
$(".popupclose").click(function(){
	$(".popupform").fadeOut(300);
	});	
$(".closeit").click(function(){
	$(".upgradethankyoupop").fadeOut(300);
	});	
</script>
<script>
$(document).ready(function(e) {
    <? if($upgradesend == true){?>
$(".upgradethankyoupop").fadeIn(200);
<? } ?>
});

</script>
<?php require_once('./inc/footer.inc.php'); ?>