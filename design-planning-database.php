<?php
global $ROOT_FOLDER;

require_once('./inc/header.inc.php');

define('WISE_PATH', $ROOT_FOLDER);
define('WISE_RELURL', '');
require_once('save_postcode.php');
?>

<style type="text/css">
.fm_bar {
  float:left;
  width:30%;
}
.newdesigntitle {
	color:#0f4778 !important;
	font-size:30px;
	text-transform:none !important;
	font-weight:bold;
	padding-bottom:10px;
	}
.newdesignsub {
	color:#75787f;
	font-size:22px;
	padding-bottom:30px;
	font-weight:normal;
	display:block;
	border-bottom:5px solid #e5e5e5;
	margin-bottom:20px;
	}	
.formtitlebor {
	border-bottom:2px solid #e5e5e5;
	color:#4b759a;
	font-size:20px;
	display:block;
	padding:10px 0px;
	font-weight:500;
	}
.fm_bar fieldset {
  border:none;
  padding-top:0;
}
.fm_bar input[type=submit], .fm_bar input[type=button] {
  background-color:#4b759a;
  border:none;
  color:#fff;
  cursor:pointer;
  font-size:16px;
  font-weight:normal;
  padding:10px 5px;
  width:100px;
  text-align:center;
  text-transform:capitalize;
}

.fm_bar select, .fm_bar input[type=text] {
  color:#75787f;
  font-size:16px;
  font-weight:normal;
  margin-top:21px;
  width:95%;
  border: 2px solid #dcddde;
  padding-left: 10px;
  height: 40px;
  text-transform: capitalize;
  font-style: normal;
}

.fm_bar input[type=submit] {
  margin-top:21px;
  margin-bottom:25px;
}

select#sel_floor {
    margin-bottom: 20px;
}
#sel_borough {

}

.wrap_results {
  float: left;
  margin-top: 1em;
  width: 68%;
  padding-left: 15px;
}
.nav-tabs {
	margin-top:-20px;
	}

.found_results {
  padding-top: 17px !important;
    color: #0e4778;
	font-size:18px;
}
 .bookthanksinner {
	 height:135px !important;
	 }
p.center {
  text-align:center;
}

.found_results a {
  margin-top:20px;
}

.found_results .total {
  font-size:27px;
  margin-bottom:7px;
}

.found_results .clear {
  clear:both;
  margin-top:17px;
}

.found_results .item {
  /*border:1px dotted red;*/
  color:gray;
  font-size:14px;
}

.found_results .item img {
  width:100%;
  max-width:286px;
}

.found_results .item .title {
  /*clear:both;
  float:left;*/
  color:gray;
  display:inline-block;
  margin-left:1em;
}

.freeflow {
  clear:both;
  float:left;
  width:100%;
}

#road_name {
  width:95%;
  min-width:200px;
}
  
#map-canvas {
  clear:both;
  height: 512px; 
  width 100%; 
  margin: 0; 
  padding: 0;
}

.dt-info {  
  clear:both;
  float:left;
  font-size:12px;
  margin-bottom:11px;
  min-height:100px;
  width:286px;
}

.dt-info h3 {
  color:#292929;
  font-size:14px;
  margin-top:7px;
  margin-bottom:7px;
}

.dt-info img {
  float:left;
  margin-right:3px;
  width:143px;
  height:101px;
}

/* Map */

#map-canvas .infoBox {
  background-color: #ffffff;
  border: 3px solid #234d9d;
  border-radius:7px;
  box-shadow: 2px 2px 4px #888888;
  -webkit-box-shadow: 2px 2px 4px #888888;
  -moz-box-shadow: 2px 2px 4px #888888;
  position: relative;
  min-width:300px;
}


#map-canvas .infoBox:after {
  background-image: url(/images/design_planning/map_info_corner5.png);
  background-repeat: no-repeat;
  background-position:0 0;
  content: " ";
  position: absolute;
  width: 30px;
  height: 18px;
  top:100%;
  left:140px;
}

.nav-tabs {
  list-style:none;
}

.nav-tabs li {
  border-top-left-radius:5px;
  border-top-right-radius:5px;
  background-color:#C8D0DE;
  display:block;
  float:left;
}
.nav-tabs li.active { 
  background-color:#e1eafa;
}
  
.nav-tabs li a { 
  display:block;
  font-size:14px;
  padding:14px;
  text-decoration:none;
}

.nav-tabs, .tab-content, .tab-pane {
  clear:both;
  float:left;
  width:100%;
}
  
.tab-pane {
  display:none;
}
  
#matrix.tab-pane {
  border:3px solid #e1eafa;
}
  
.tab-pane.active {
  display:block;
}
.closethis {
    position: absolute;
    right: 0px;
    top: 10px;
}
.closethis a {
    background: none;
    color: #104674;
    font-size: 36px;
    font-weight: 500;
    line-height: 18px;
}
.closethis a:hover{
	text-decoration:none;
	}
.multiselect {
  width: 95%;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
    display: none;
    border: 2px #dadada solid;
    border-top: 0px;
    width: 100%;
	font-size: 16px;
    color: #969696;
	padding:5px;
}

#checkboxes input {
	height: 15px;
    width: 15px;
    font-size: 12px;
    vertical-align: middle;
	}

#checkboxes label {
  display: block;
  font-size:18px;
  font-weight:normal;
}

#checkboxes label:hover {
  
}	
#checkboxes input[type="checkbox"] {
  visibility: hidden;
  position:absolute;
}
#checkboxes label {
  cursor: pointer;
  margin-bottom:5px;
}
#checkboxes input[type="checkbox"] + label:before {
    border: 2px solid #0f4778;
    content: "\00a0";
    display: inline-block;
    font: 16px/1em sans-serif;
    height: 11px;
    margin: 0 .25em 0 0;
    padding: 0;
    vertical-align: middle;
    width: 11px;
}
#checkboxes input[type="checkbox"]:checked + label:before {
  background: #0f4778;
  color: #fff;
  content: "";
  text-align: center;
}
#checkboxes input[type="checkbox"]:checked + label:after {
  font-weight: bold;
}

#checkboxes input[type="checkbox"]:focus + label::before {
    outline: rgb(59, 153, 252) auto 5px;
}	
@media (max-width:640px) {
	.fm_bar, .wrap_results {
		width:100%;
		}
	.fm_bar select {
		margin-top:0px;
		border:none;
		border-top:2px solid #dcddde;
		height: 30px;
		}
	.bottompartform h2 {
		border-bottom:none;
		border-top:2px solid #dcddde;
		display: inline-block;
    	width: 95%;
		
		}
	.formtitlebor {
		border:none;
		}	
	.newdesignsub {
		border-bottom: 2px solid #e5e5e5;
    	margin-bottom: 0px;
		font-size:18px;
		}	
	#road_name {
		margin-top:5px;
		}			
	#sel_floor {
		border-bottom: 2px solid #e5e5e5;
		}	
	} 
  
</style>

<?php
  // Controller (logic, vars) part:

  $a_property = array('h' => 'House', 'f' => 'Flat');
  
  $a_borough = array();
  
  $sql = 'SELECT DISTINCT(d.borough), b.id, b.name FROM design_planning d LEFT JOIN `borough` b ON (d.borough=b.id)';
  $rs = getRs($sql);
  while ($row = mysqli_fetch_assoc($rs)) {
    $a_borough[ $row['id'] ] = $row['name'];
  }
  $a_ProT = array();
  $sqlProT = 'select project_type,id from dpproject_type';
  $rsProT = getRs($sqlProT);
  while ($rowProT = mysqli_fetch_assoc($rsProT)) {
    $a_ProT[ $rowProT['id'] ] = $rowProT['project_type'];
  }
  
  $a_roof = array('sv' => 'Slate & Velux', 'f' => 'Flat Roof', 'g' => 'All Glass');
  
  $a_floor = array(
    '-25' => '&lt; 25 SQM', // m<sup>2</sup>
    '25-29' => '25-29 SQM',
    '30-34' => '30-34 SQM',
    '35-39' => '35-39 SQM',
    '40-44' => '40-44 SQM',
    '45-' => '> 45 SQM'
  );
  
  if (!isset($_GET['road_name'])) $_GET['road_name'] = '';
  else $_GET['road_name'] = trim(substr(trim((string)$_GET['road_name']), 0, 30));
  
  if ($_GET['road_name']) {
    $_GET['property'] = '';
    $_GET['project_type'] = '';
    $_GET['borough'] = '';
    $_GET['roof'] = '';
    $_GET['floor'] = ''; 
  }
  
  if (!isset($_GET['property'])) $_GET['property'] = '';
  if (!isset($_GET['project_type'])) $_GET['project_type'] = '';
  if (!isset($_GET['borough'])) $_GET['borough'] = '';
  if (!isset($_GET['roof'])) $_GET['roof'] = '';
  if (!isset($_GET['floor'])) $_GET['floor'] = '';

  $where = '';
  // do not use page navigation for both views (map and matrix) per now
  
  //echo '!!!!!!!!!!!!!!!!<pre>'.print_r($_GET,1).'</pre>';#debug
  $out_of_territory = false;
  if ($_GET['road_name']) {
    //$where .= " AND title LIKE '%".mysql_real_escape_string($_GET['road_name'])."%'";
    $where .= " AND 1=1"; // show all on map, but centered close to search criteria point (by road name or postcode)
    
    // check if road name is possible postcode and if it's in territory
    
    $rs = getRs("SELECT map_postcodes FROM setting s WHERE s.setting_id = 1");
    
    $map_postcodes = array();
    $out_of_territory = false;
    $row = mysqli_fetch_assoc($rs);
    if (isset($row['map_postcodes'])) {
      $map_postcodes = explode(',', $row['map_postcodes']);
    }
    
    $postpart = $_GET['road_name'];
    $postpart = str_replace(' ', '', $postpart);
    
    if ($map_postcodes && strlen($postpart)<8) {
      
      if (strlen($postpart)<6 && strlen($postpart)>4) $postpart = substr($postpart, 0, 2);
      elseif (strlen($postpart)==6) $postpart = substr($postpart, 0, 3);
      else $postpart = substr($postpart, 0, 4);
      
      //echo print_r($map_postcodes, 1).' '.$postpart.':)';#debug
      
      $cover = false;
      foreach ($map_postcodes AS $v) {
        $v = trim($v);
        if (strtoupper($postpart) === strtoupper($v)) {
          $cover = true;
          break;
        }
      }
      
      if (!$cover) {
        $out_of_territory = true;
        //echo '<br/> out_of_territory!:)';#debug
      }
      
      
    }
    
  }
  
  $is_filter = false;
  
  // disable $where to get all results for map, no pagination navigation

  if ($_GET['property'] && in_array($_GET['property'], array_keys($a_property))) {
    //$where .= " AND property='".mysql_real_escape_string($_GET['property'])."'";
    $is_filter = true;
  }
  else $where .= " AND property<>''";
  
  if ($_GET['borough'] && in_array($_GET['borough'], array_keys($a_borough))) {
    //$where .= " AND borough='".(int)$_GET['borough']."'";
    $is_filter = true;
  }
  else $where .= " AND borough<>0";
  if($_GET['project_type'][0]){
	  foreach($_GET['project_type'] AS $keys=>$vals){
	  if ($vals && in_array($vals, array_keys($a_ProT))) {
		//$where .= " AND protypeID='".(int)$_GET['project_type']."'";
		$is_filter = true;
	  }
	  }
  }
  //else $where .= " AND protypeID<>0";
  
  if ($_GET['roof'] && in_array($_GET['roof'], array_keys($a_roof))) {
    //$where .= " AND roof='".mysql_real_escape_string($_GET['roof'])."'";
    $is_filter = true;
  }
  else $where .= " AND roof<>''";
  
  $floor = '';
  if ($_GET['floor'] && in_array($_GET['floor'], array_keys($a_floor))) {
    $floor = explode('-', $_GET['floor']);
    if (2==count($floor)) {
      $floor[0] = (float)$floor[0];
      $floor[1] = (float)$floor[1];
      if ($floor[0] && $floor[1]) {
        //$where .= " AND floor > ".(float)$floor[0]." AND floor <= ".(float)$floor[1];
      }
      elseif (!$floor[0] && $floor[1]) {
        //$where .= " AND floor <= ".(float)$floor[1];
      }
      elseif ($floor[0] && !$floor[1]) {
        //$where .= " AND floor > ".(float)$floor[0];
      }
      $is_filter = true;
    }
    else $floor = '';
  }
  //else $where .= " AND floor<>0";#last

  $sql = 'SELECT id, title, property, protypeID,borough, roof, floor, pdffile, postcode, date_created FROM design_planning WHERE 1=1'.$where.' ORDER BY id DESC';
  
  //echo $sql . "<br />";#debug
  
  mysqli_set_charset($dbconn,"utf8");
  $rs = mysqli_query($dbconn,$sql); // result set
  $total_recs = mysqli_num_rows($rs);
  $total_recs_for_first_time = $total_recs;
  $nav_links = '';

  $a_postcodes = array();
  $a_projects = array();
  $i = 0;

  if ($total_recs) {
    $total_recs = 0; // will be amount of search criteria matched (but all available on map) 
    while ($row = mysqli_fetch_assoc($rs)) {

      $row['postcode'] = strtoupper(str_replace(' ', '', $row['postcode']));

      $test_postcode = strtoupper(str_replace(' ', '', $_GET['road_name']));

      $a_projects[$i] = $row;
  
      if ($_GET['road_name'] || $is_filter) {
        if ($_GET['road_name']) {
          if (strpos(strtolower($row['title']), strtolower($_GET['road_name']))===0 || $row['postcode'] == $test_postcode) {
            $total_recs++;
            $a_projects[$i]['match'] = true;
          }
        }
        else { // $is_filter

          if (  (($_GET['property'] && $row['property']==$_GET['property']) || !$_GET['property'] )  

             && (array($row['protypeID'], $_GET['project_type'])) 
			 
             && ( ($_GET['borough'] && $row['borough']==$_GET['borough']) || !$_GET['borough'] )

             && ( ($_GET['roof'] && $row['roof']==$_GET['roof']) || !$_GET['roof'] )

             && (($floor && (
               ($floor[0] && $floor[1] && $row['floor'] > $floor[0] && $row['floor'] <= $floor[1])
               || 
               (!$floor[0] && $floor[1] && $row['floor'] <= $floor[1])
               || 
               ($floor[0] && !$floor[1] && $row['floor'] > $floor[0])
             )) || !$floor)

            ) {
            $total_recs++;
            $a_projects[$i]['match'] = true;

          }

        }
      }
      else {
        $total_recs++;
        $a_projects[$i]['match'] = true;
      }
      
      $a_postcodes[] = $row['postcode'];

      $i++;
    }

    // use the trick: when postcode not found, try this:
    if (!$total_recs && $_GET['road_name']) {
      $test_postcode = strtoupper(str_replace(' ', '', $_GET['road_name']));

      do {
        $test_postcode = substr($test_postcode, 0, -1);

        foreach ($a_projects AS $k => $row) {
          if ($row['postcode'] && $test_postcode && strpos($row['postcode'], $test_postcode)===0) {
            $total_recs++;
            $a_projects[$k]['match'] = true;
          }
        }

      } while (!$total_recs && $test_postcode);

    }

    //echo "Projects:" . count($a_projects)."<br />";


    //echo $test_postcode.'!!!';

    //mysql_data_seek($rs, 0);#not required

    // get latitude, longitude

    global $dbhost, $dbuser, $dbpass, $dbname, $dbname_uk_postcode, $dbconn;
    //echo $dbname_uk_postcode.'!!!';#debug
   // if (!mysql_select_db($dbname_uk_postcode, $dbconn)) die("Can't select the ".$dbname_uk_postcode.' database!');

   $dbconn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname_uk_postcode) or die ('Error connecting to database');
   mysqli_set_charset($dbconn,"utf8");
  $sql = "SELECT postcode, longitude, latitude FROM uk_postcode WHERE postcode IN('" . implode("','", $a_postcodes) . "')";
    $rs = mysqli_query($dbconn,$sql);
    while ($row = mysqli_fetch_assoc($rs)) {
      foreach ($a_projects AS $k => $v) {
        if ($v['postcode']==$row['postcode']) {
          $a_projects[$k]['latitude'] = $row['latitude'];
          $a_projects[$k]['longitude'] = $row['longitude'];
        }
      }
    }

    //echo '<pre>!!!'.print_r($a_projects, 1).'<pre>';#debug


    $center_lat = 0;
    $center_lng = 0;

    // fix $total_recs (to make sure all have $row['latitude'] to be visible on map)
    $total_recs = 0; // reset

    foreach ($a_projects AS $k => $row) {

      if (!isset($row['latitude'])) continue;

      if ($_GET['road_name'] || $is_filter) {

        if ($_GET['road_name']) {

          if ($row['title'] && $test_postcode && ((isset($row['match']) && $row['match']) || strpos($row['postcode'], $test_postcode)===0)) {
            $center_lat += $row['latitude'];
            $center_lng += $row['longitude'];
            $a_projects[$k]['match'] = true;
            $total_recs++;
          }

        }
        else {

          if (isset($row['match']) && $row['match']) {

            $center_lat += $row['latitude'];
            $center_lng += $row['longitude'];
            $total_recs++;

          }


        }

      }
      else { // show all


          $center_lat += $row['latitude'];
          $center_lng += $row['longitude'];
          $total_recs++;


      }

    } // end foreach

  } // endif $total_recs


  // get geo center of all points
  if ($total_recs) {
     $center_lat = $center_lat / $total_recs;
     $center_lng = $center_lng / $total_recs;
     //echo $center_lat . ' ::: ' .  $center_lng;#debug
  }
  
  // get more points near by
  if ($_GET['road_name'] && $total_recs) {
    $center_lat = $center_lat / $total_recs;
    $center_lng = $center_lng / $total_recs;
    
    $miles2km = 1.60934; // miles to kilomeeters
    $miles = 1; // 0.5, 1, 2 : default distance
    $kmRange = $miles * $miles2km;

    foreach ($a_projects AS $k => $row) {

      if (!isset($row['latitude'])) continue;
      if (!isset($row['longitude'])) continue;

      $lat = $row['latitude'];
      $lng = $row['longitude'];

      $distance = (
        3959 * acos (
          cos (  deg2rad ($center_lat) )
          * cos(  deg2rad ( $lat ) )
          * cos(  deg2rad ( $lng ) -  deg2rad ($center_lng) )
          + sin (  deg2rad ($center_lat) )
          * sin(  deg2rad ( $lat ) )
        )
      );

      if ($distance < $kmRange && !(isset($row['match']) && $row['match'])) {
        //echo $distance .'!!!<br/>';#debug
        $a_projects[$k]['match'] = true;
        $total_recs++;
      }

    }
  

  }
if ($total_recs) {

    // map view :

    // prepare records for map view

    $a_map = array();
    foreach ($a_projects AS $k => $v) {

      if (!isset($v['latitude'])) {
        echo '<!-- error postcode: '.print_r($v, 1).' (no lat, lng) -->';
        continue;
      }
      $key = $v['latitude'].' '.$v['longitude']; // unique key
      if (!isset($a_map[$key])) {
        $a_map[$key] = array(
          'postcode' => array(),
          'lat' => $v['latitude'],
          'lng' => $v['longitude'],
          'match' => isset($v['match'])?true:false,
          'title' => array(), 
          'property' => array(), 
          'project_type' => array(), // not used per now
          'roof' => array(), 
          'floor' => array(), 
          'pdffile' => array(),
          'date_created' => array(),
        );
      }
      $a_map[ $key ]['postcode'][] = $v['postcode'];
      $a_map[ $key ]['title'][] = $v['title'];
      $a_map[ $key ]['property'][] = $v['property'];
      $a_map[ $key ]['project_type'][] = $v['project_type'];
      $a_map[ $key ]['roof'][] = $v['roof'];
      $a_map[ $key ]['floor'][] = $v['floor'];
      $a_map[ $key ]['pdffile'][] = $v['pdffile'];
      $a_map[ $key ]['date_created'][] = $v['date_created'];

    }
    $a_map = array_values($a_map);
    /*
    echo "here at 769";
    echo "<pre>";
    //var_dump($a_map);
    
    echo "</pre>"; */
    $a_map = json_encode($a_map);
 /*   echo json_last_error_msg(); // Print out the error if any
    die(); // halt th e script*/
    // endif map view
    
    // matrix view:
    
////////////////////////////////////////////////////
    
    $i = 0;
    $pdf_exists = false;
    $matrix_view = '';
    foreach ($a_projects AS $row) {

      if (!isset($row['latitude'])) continue;
      if (!isset($row['longitude'])) continue;


      if ($_GET['road_name'] || $is_filter) {

        if ($_GET['road_name']) {

          if (isset($row['match']) && $row['match']) {

            $i++;

            //echo '<div class="item"><pre>'.print_r($row,1).'</pre></div>';#debug
            $basename = $row['pdffile'];
            $basename = explode('.', $basename);
            array_pop($basename);
            $basename = implode('.', $basename);

            if (file_exists(WISE_PATH.'pdf/design_planning/'.$row['pdffile'])) $pdf_exists = true;
            else $pdf_exists = false;

            $matrix_view .= '<div class="col_one_third'.(!($i%3)?' col_last':'').' item"><a href="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design_planning/'.$row['pdffile']:'http://buildteam.com/pdf/design_planning/'.$row['pdffile']).'" target="_blank"><img src="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design_planning/thumb/'.$basename.'.png?v='.$row['date_created']:'http://buildteam.com/pdf/design_planning/thumb/'.$basename.'.png?v='.$row['date_created']).'" alt="'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'" /><br/><div class="title">'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'</div></a></div>'.(!($i%3)?' <br clear="all" />':'');

          }

        }
        else {


          if (isset($row['match']) && $row['match']) {

            $i++;

            //echo $i.'!!!!!!!!!!!!!!!!!<br/>';#debug

            //echo '<div class="item"><pre>'.print_r($row,1).'</pre></div>';#debug
            $basename = $row['pdffile'];
            $basename = explode('.', $basename);
            array_pop($basename);
            $basename = implode('.', $basename);

            if (file_exists(WISE_PATH.'pdf/design_planning/'.$row['pdffile'])) $pdf_exists = true;
            else $pdf_exists = false;
            
            $matrix_view .= '<div class="col_one_third'.(!($i%3)?' col_last':'').' item"><a href="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design_planning/'.$row['pdffile']:'http://buildteam.com/pdf/design_planning/'.$row['pdffile']).'" target="_blank"><img src="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design_planning/thumb/'.$basename.'.png?v='.$row['date_created']:'http://buildteam.com/pdf/design_planning/thumb/'.$basename.'.png?v='.$row['date_created']).'" alt="'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'" /><br/><div class="title">'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'</div></a></div>'.(!($i%3)?' <br clear="all" />':'');
            

          }


        }

      }
      else {


        $i++;

        //echo '<div class="item"><pre>'.print_r($row,1).'</pre></div>';#debug
        $basename = $row['pdffile'];
        $basename = explode('.', $basename);
        array_pop($basename);
        $basename = implode('.', $basename);

        if (file_exists(WISE_PATH.'pdf/design_planning/'.$row['pdffile'])) $pdf_exists = true;
        else $pdf_exists = false;

        $matrix_view .= '<div class="col_one_third'.(!($i%3)?' col_last':'').' item"><a href="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design_planning/'.$row['pdffile']:'http://buildteam.com/pdf/design_planning/'.$row['pdffile']).'" target="_blank"><img src="'.($pdf_exists?'/'.WISE_RELURL.'pdf/design_planning/thumb/'.$basename.'.png?v='.$row['date_created']:'http://buildteam.com/pdf/design_planning/thumb/'.$basename.'.png?v='.$row['date_created']).'" alt="'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'" /><br/><div class="title">'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'</div></a></div>'.(!($i%3)?' <br clear="all" />':'');

      }

    }
    
    

    // endif matrix view (gotten by filter) //////////////////////////////

  } // endif $total_recs (means search criteria found)  

  
  // Template (view) part:
?>

<div class="full">
	<h1 class="newdesigntitle">Design database</h1>
  <h2 class="newdesignsub"><?php
  $curr_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  if ($curr_url == "http://www.buildteam.com/design-planning-database.html") {
    $total_recs = $total_recs_for_first_time;
  }
      echo ($total_recs?$total_recs:'No') . ' project'.(($total_recs>1 || !$total_recs)?'s':'').' found, ';
    if ($a_filter) echo implode(', ', $a_filter);
      else {
        if (!$_GET['road_name']) echo 'You can filter the results with controls below';
        else echo 'Road keyword <strong>&quot;'.htmlspecialchars($_GET['road_name'], ENT_QUOTES, 'UTF-8').'&quot;</strong>';
      }?></h2>
  
  <form class="fm_bar jClever" action="/design-planning-database.php" method="get" onsubmit="fixPlaceholder()">
  <fieldset>
  	 <div class="freeflow">
     <h2 class="formtitlebor">Search Address</h2>
      <input type="text" name="road_name" id="road_name" size="25" maxlength="30" style="font-style:<?php echo $_GET['road_name']?'normal':'italic' ?>" value="<?php echo $_GET['road_name']?htmlspecialchars($_GET['road_name'], ENT_QUOTES, 'UTF-8'):'road name or postcode'; ?>" /> <input type="submit" value="map search" />
    </div>
    
    <div class="bottompartform">
    <h2 class="formtitlebor">Refine your search</h2>
    <select name="property" id="sel_property">
      <option value="">property type</option>
      <?php
      foreach ($a_property AS $k => $v) {
        echo '<option value="'.$k.'"'.(($_GET['property'] && $k==$_GET['property'])?' selected="selected"':'').'>'.htmlspecialchars($v).'</option>';
      }
      ?>
    </select>
    <!--<select name="project_type" id="project_type">
      <option value="">Project type</option>
      <?php
      foreach ($a_ProT AS $k => $v) {
        echo '<option value="'.$k.'"'.(($_GET['project_type'] && $k==$_GET['project_type'])?' selected="selected"':'').'>'.htmlspecialchars($v).'</option>';
      }
      ?>
    </select>-->
    <div class="multiselect">
    <div class="selectBox" onclick="showCheckboxes()">
      <select>
        <option>Project Type</option>
      </select>
      <div class="overSelect"></div>
    </div>
    <div id="checkboxes">
    <?php
      foreach ($a_ProT AS $k => $v) {
		  $project_typeChecked = '';
		  if($_GET['project_type'][0]){
			  if((in_array($k, $_GET['project_type']))){
				  $project_typeChecked = 'checked="checked"';
			  }
		  }
        echo '<input type="checkbox" name="project_type[]" id="'.$k.'" value="'.$k.'" '.$project_typeChecked.'>'.'<label for="'.$k.'">'.htmlspecialchars($v).'</label>';
      }
      ?>
     </div> 
     </div>
    <select name="borough" id="sel_borough">
      <option value="">borough</option>
      <?php
      foreach ($a_borough AS $k => $v) {
        echo '<option value="'.$k.'"'.(($_GET['borough'] && $k==$_GET['borough'])?' selected="selected"':'').'>'.htmlspecialchars($v).'</option>';
      }
      ?>
    </select>
    
    <select name="roof" id="sel_roof">
      <option value="">roof</option>
      <?php
      foreach ($a_roof AS $k => $v) {
        echo '<option value="'.$k.'"'.(($_GET['roof'] && $k==$_GET['roof'])?' selected="selected"':'').'>'.htmlspecialchars($v).'</option>';
      }
      ?>
    </select>
    
    <select name="floor" id="sel_floor">
      <option value="">floor area</option>
      <?php
      $i = 0;
      foreach ($a_floor AS $k => $v) {
        echo '<option value="'.$k.'"';
        
        //$k = explode('-', $k);
        
        if ($_GET['floor'] && $k==$_GET['floor']) echo ' selected="selected"';
        
        echo '>'.$v.'</option>';
        $i++;
      }
      ?>
    </select>
    
    <input type="button" value="filter" onclick="this.form.road_name.value='';this.form.submit()" />
   <!-- <div style="color:#f45219;padding-top:14px;text-align:center">- OR -</div>-->
   </div>
  </fieldset>
  </form>
  
  <div class="wrap_results">
    
    
    <?php if(!empty($a_property)): ?>
    
    <div class="found_results"><div class="total"></div><?php
      $a_filter = array();
      
      if (!$_GET['road_name']) {
        
        foreach ($a_property AS $k => $v) {
          if ($_GET['property'] && $k==$_GET['property']) {
            $a_filter[] = htmlspecialchars($v);
            break;
          }
        }
        foreach ($a_ProT AS $k => $v) {
          if ($_GET['project_type'] && $k==$_GET['project_type']) {
            $a_filter[] = htmlspecialchars($v);
            break;
          }
        }
        foreach ($a_borough AS $k => $v) {
          if ($_GET['borough'] && $k==$_GET['borough']) {
            $a_filter[] = htmlspecialchars($v);
            break;
          }
        }
        foreach ($a_roof AS $k => $v) {
          if ($_GET['roof'] && $k==$_GET['roof']) {
            $a_filter[] = htmlspecialchars($v);
            break;
          }
        }
        foreach ($a_floor AS $k => $v) {
          if ($_GET['floor'] && $k==$_GET['floor']) {
            $a_filter[] = $v;
            break;
          }
        }
        
      }
      
      
        
      if ($total_recs) {
      ?>  
        
    <div class="clearfix"></div>
    
    <!-- map and matrix tabs here -->
    
    <?php
    ////////////////////////////////////////////////// tabs ///////////////////////////////////////////////
    
      // show if some filter vars exists, otherwise show map image
      if ((isset($_GET['road_name']) && $_GET['road_name']) || (isset($_GET['property']) && $_GET['property']) || (isset($_GET['project_type']) && $_GET['project_type']) || (isset($_GET['borough']) && $_GET['borough']) || (isset($_GET['roof']) && $_GET['roof']) || (isset($_GET['floor']) && $_GET['floor'])) { // show tabs
      ?>
      
      
      
      
      <!-- Nav tabs -->
      <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active"><a href="#map" aria-controls="map" role="tab" data-toggle="tab">Map view</a></li>
        <li role="presentation"><a href="#matrix" aria-controls="matrix" role="tab" data-toggle="tab">Matrix view</a></li>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="map">
          <div id="map-canvas"></div>
          <?php 
            if ($total_recs) {
              //echo '<pre>'.print_r($a_map, 1).'</pre>!!!!';#debug
            }
          ?>
        </div>
        <div role="tabpanel" class="tab-pane" id="matrix">
      
          <?php echo $matrix_view; ?>
          
        </div>
    
      </div>
        
    
      <?php  
      } // endif filter GET vars
      else { // show map image
      ?>
<!--        <div style="text-align:center"><img src="/images/london-map.png" style="width:100%;max-width:640px" /></div> -->
        <div style="text-align:center"><img src="/images/dbdhq.jpg" style="width:100%;max-width:640px" /></div> 
      <?php
      } // end else show map image


    /////////////////////////////////////////////////////////////////////////////////////////////////
    
    
        
        
      } // endif $total_recs (means search criteria found)  
      
    ?></div>
    
    <?php endif; // endif !$a_property ?>
    
    <?php if ($out_of_territory): ?>
    <div class="bookthanks" style="display: block;">
    			<div class="bookthanksinner">
              <div class="lightbox_content">
              <div class="closethis">
                      <a href="#">x</a>
                  </div>   
                 <h4>Oops...</h4> 
             <p class="found_results">
    Your search is outside our standard coverage area, however you may be interested in our<br/><a target="_blank" href="http://buildteam.com/getting-started/nationwide.html">Nationwide Design Service</a>
             
            </div>
            </div>
            </div>
    
    <?php endif; ?>
    
  </div>
</div>
<?php if ($total_recs) : // $_GET['road_name'] &&  ?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAJP0WdpprayWG6IpcvMC_4HTQBONhe8dE"></script>

<script src="/js/infobox.js"></script>

<script type="text/javascript">
    var map;
    var center_lat = <?php echo $center_lat ?>;
    var center_lng = <?php echo $center_lng ?>;
    
    var a_map = <?php echo $a_map; ?>;
    
    var map_state = {
      init: false,
      extended: false,
      zoom: 0
    };
    
    //alert('Hi! '+ center_lat + " " + center_lng);//debug
  
    function initialize() {
      var centerLatLng = new google.maps.LatLng( center_lat, center_lng );

      var bounds = new google.maps.LatLngBounds();

      var mapOptions = {
        //disableDefaultUI: false,
        scrollwheel: false,
        zoomControl: true,
        /*minZoom: 8,*/
        maxZoom: 14, // 12
        zoom: 9, /*10*/
        center: centerLatLng,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      };

      map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

      var blue_icon = { 
        url: '/images/design_planning/blue_pin1.png',
      };

      
      if (a_map) {
        var a_marker_map = [];
        var a_marker_pan = [];
        
        var position = {};

        //var infoWindow = new google.maps.InfoWindow(), i, position;

        var infoWindow = new InfoBox({
                content: "",
                map: map,
                disableAutoPan: false,
                maxWidth: 0, // 150
                alignBottom: true,
                pixelOffset: new google.maps.Size(-150, -65),
                zIndex: null,
                boxClass: "infoBox",
                closeBoxURL: '/images/design_planning/map_info_close.gif',
                closeBoxMargin: "3px", // "12px 4px 2px 2px",
                pane: "floatPane",
                enableEventPropagation: false,
                infoBoxClearance: "10px" // new google.maps.Size(1, 1) new google.maps.Size(1, 1)
        });


        for (i=0; i < a_map.length; i++) {
          position = new google.maps.LatLng( a_map[i].lat, a_map[i].lng );
          
          if (a_map[i].match) { // show matched at first and other by map drag or zoom event
            bounds.extend(position);

            a_marker_map[i] = new google.maps.Marker({
              position: position,
              map: map,
              icon: blue_icon,
              title: a_map[i].title.join("; ")
            });
            
            // Allow each marker to have an info window

            google.maps.event.addListener(a_marker_map[i], 'click', (function(marker, i) {
                return function() {
                    var buf = '';
                    var basename = "";
                    for (var j=0;j<a_map[i].title.length;j++) {
                      basename = a_map[i].pdffile[j];
                      basename = basename.split('.');
                      basename.pop();
                      basename = basename.join('.');

                      buf += '<div class="dt-info">';

                      buf += '<img src="/pdf/design_planning/thumb/' + basename + '.png?v=' + a_map[i].date_created[j] + '" />';
                      buf += '<h3>' + a_map[i].title[j] + '</h3>';
                      buf += (a_map[i].property[j]=='f'?'Flat':'House') + ', ' + a_map[i].postcode[j] + '<br/>';
                      buf += (a_map[i].roof[j]=='f'?'Flat Roof':(a_map[i].roof[j]=='sv'?'Slate &amp; Velux Roof':'All Glass Roof')) + '<br/>';
                      buf += 'Floor area: ' + a_map[i].floor[j] + ' m<sup>2</sup><br/>';
                      buf += '<a href="/pdf/design_planning/' + a_map[i].pdffile[j] + '" target="_blank">See PDF drawing</a>';
                      buf += '</div>';
                    }

                    infoWindow.setContent(buf);
                    infoWindow.open(map, marker);
                }
            })(a_marker_map[i], i));

            // Automatically center the map fitting all markers on the screen
            map.fitBounds(bounds);
          } // endif a_map[i].map
        } // end for
        
        /*google.maps.event.addListener(map, 'dragend', function() { 
          if (map_state.init && !map_state.extended) {
            extendMap();
          }
        });*/
        google.maps.event.addListener(map, 'zoom_changed', function() {
          if (map_state.init && !map_state.extended && map_state.zoom && map.getZoom() < map_state.zoom) {
            extendMap();
          }
        });

        
      } // endif a_map
      
  } // end initialize() function
  
  function extendMap() {
    
      var blue_icon = { 
        url: '/images/design_planning/blue_pin1.png',
      };
      
      if (a_map) {
        var a_marker_map = [];
        var a_marker_pan = [];
        
        var position = {};

        //var infoWindow = new google.maps.InfoWindow(), i, position;

        var infoWindow = new InfoBox({
                content: "",
                map: map,
                disableAutoPan: false,
                maxWidth: 0, // 150
                alignBottom: true,
                pixelOffset: new google.maps.Size(-150, -65),
                zIndex: null,
                boxClass: "infoBox",
                closeBoxURL: '/images/design_planning/map_info_close.gif',
                closeBoxMargin: "3px", // "12px 4px 2px 2px",
                pane: "floatPane",
                enableEventPropagation: false,
                infoBoxClearance: "10px" // new google.maps.Size(1, 1) new google.maps.Size(1, 1)
        });


        for (i=0; i < a_map.length; i++) {
          position = new google.maps.LatLng( a_map[i].lat, a_map[i].lng );
          
          if (!a_map[i].match) { // show other by map drag or zoom event
            //bounds.extend(position); // without extending

            a_marker_map[i] = new google.maps.Marker({
              position: position,
              map: map,
              icon: blue_icon,
              title: a_map[i].title.join("; ")
            });

            // Allow each marker to have an info window

            google.maps.event.addListener(a_marker_map[i], 'click', (function(marker, i) {
                return function() {
                    var buf = '';
                    var basename = "";
                    for (var j=0;j<a_map[i].title.length;j++) {
                      basename = a_map[i].pdffile[j];
                      basename = basename.split('.');
                      basename.pop();
                      basename = basename.join('.');

                      buf += '<div class="dt-info">';

                      buf += '<img src="/pdf/design_planning/thumb/' + basename + '.png?v=' + a_map[i].date_created[j] + '" />';
                      buf += '<h3>' + a_map[i].title[j] + '</h3>';
                      buf += (a_map[i].property[j]=='f'?'Flat':'House') + ', ' + a_map[i].postcode + '<br/>';
                      buf += (a_map[i].roof[j]=='f'?'Flat Roof':(a_map[i].roof[j]=='sv'?'Slate &amp; Velux Roof':'All Glass Roof')) + '<br/>';
                      buf += 'Floor area: ' + a_map[i].floor[j] + ' m<sup>2</sup><br/>';
                      buf += '<a href="/pdf/design_planning/' + a_map[i].pdffile[j] + '" target="_blank">See PDF drawing</a>';
                      buf += '</div>';
                    }

                    infoWindow.setContent(buf);
                    infoWindow.open(map, marker);
                }
            })(a_marker_map[i], i));

            // not center the map fitting all markers on the screen
            //map.fitBounds(bounds);
            
          } // endif a_map[i].map
        } // end for
    
      } // indif a_map
      
      map_state.extended = true; // one time extend
        
  } // end extendMap() function
  
  if (center_lat && center_lng) {
    google.maps.event.addDomListener(window, 'load', initialize);
    
    window.setTimeout(function() {
      map_state.init = true;
      map_state.zoom = map.getZoom();
    }, 1000);
    
  }
  
</script>
<?php endif; ?>


<script type="text/javascript">
  var road_name_placeholder = "road name or postcode";

  jQuery(document).ready(function(){
    //jQuery('.jClever').jClever(); 
  
    $("#road_name").blur(function(){
      if (''==this.value) {
        this.value=road_name_placeholder;
        this.style.fontStyle = "italic";
      }
    });
    
    $("#road_name").focus(function(){
      if (road_name_placeholder==this.value) {
        this.value='';
        this.style.fontStyle = "normal";
      }
    });
   
    $(".nav-tabs li a").click(function(e){
      var id = this.href + "";
      id = id.split("#");
      id = id[1];
      
      $(".nav-tabs li").removeClass("active");
      $(this).parent().addClass("active");
      
      $(".tab-pane").removeClass("active");
      $("#"+id).addClass("active");
      
      e.preventDefault();
    });
    
  });
  
  function fixPlaceholder() {
    if (road_name_placeholder==document.getElementById("road_name").value) {
      document.getElementById("road_name").value = "";
    }
  }
</script>
<script>
var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
</script>
<script>
$(".closethis").click(function(){
	$(".bookthanks").fadeOut(200);
	});
</script>
<?php
  require_once('./inc/footer.inc.php');
?>