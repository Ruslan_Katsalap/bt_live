<?php



require_once('./inc/header.inc.php');



?>

  	<div class="left">

    	<div id="bc"><a href="/" title="Home">Home</a> &rsaquo; <a href="/what-we-do.html" title="What We Do">What We Do</a> &rsaquo; <b>Pricing &amp; Tariff</b></div>

    	<h1>Pricing &amp; Tariff</h1>

      <p>

      <table class="t_pricing">

      	<tr>

        	<th></th>

        	<th>Mon - Fri<br />7am - 6pm</th>

          <th>Mon - Fri<br />6pm - 12 Midnight</th>

          <th>Saturday<br />7am - 12am</th>

          <th>Sunday<br />7am - 12am</th>

        </tr>

        <tr>

        	<th>General Trades</th>

          <td>&pound;50</td>

          <td>&pound;90</td>

          <td>&pound;90</td>

          <td>&pound;90</td>

        </tr>

        <tr>

        	<th>Roofing Repairs</th>

          <td>&pound;80</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

        </tr>

        <tr>

        	<th>General Plumbing Repairs</th>

          <td>&pound;80</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

        </tr>

        <tr>

        	<th>General Electrical Repairs</th>

          <td>&pound;80</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

          <td>&pound;120</td>

        </tr>

        <tr>

        	<th>Gas Safe Engineers</th>

          <td>&pound;90</td>

          <td>&pound;130</td>

          <td>&pound;130</td>

          <td>&pound;130</td>

        </tr>

        <tr>

        	<th>NICEIC Certified Electrician</th>

          <td>&pound;90</td>

          <td>&pound;130</td>

          <td>&pound;130</td>

          <td>&pound;130</td>

        </tr>

        <tr>

        	<th>Gas Safety Cert (Gas Safe)</th>

          <td>&pound;120</td>

          <td>&pound;150</td>

          <td>&pound;150</td>

          <td>&pound;150</td>

        </tr>

      </table>

      </p>



      <p>Above is our hourly rate. We charge a minimum of 1.5 hours per visit</p>
      <p>&nbsp;</p>

      

      <h3>No extras</h3>

      <p>The client will not be charged for our time travelling to or away from their job, there will also be no charge for parking and congestion charge.</p>
      <p>&nbsp;</p>

      

      <h3>Materials</h3>

      <p>All materials which are supplied by Build Team will be charged as cost plus 20% handling fee</p>

		</div>

    <div class="right">

    	<img src="/images/pricingabacus.jpg" width="326" height="436" alt="Build Team" title="Build Team" />

    </div>

<?php



require_once('./inc/footer.inc.php');



?>