<?php
	require_once('./inc/header.inc.php');
?>
<style type="text/css">
	.tour_dates {
		width: 25%;
		margin-bottom: 18px;
		float: left;
		border: 3px solid #183e70;
		padding: 38px 25px 25px 25px;
		margin-right: 30px;
}
.home-gallery-h2 h2, .int-land-h2 h2 {
    margin-left: 20px;
}
.date_box {
    width: 17%;
    background: #a0a0a0 !important;
    float: left;
    text-align: center;
    padding: 15px;
	color: #fff !important;
	margin-right:2%;
	min-height: 30px;
}
.form_row {
  float: left;
  margin: 10px 0;
  width: 100%;
}
.form_row > label {
  display: block;
    font-weight: regular;
    line-height: 1em;
    margin-bottom: 0;
    margin-top: 12px;
    font-size: 20px;
    float: left;
	color:#183e70 !important;
}
.form_input{
  width: 100%;
    margin-top: 5px;
    border: 1px solid #8b8f94;
    margin-bottom: 10px;
    background: none;
    max-height: 32px;
	height:32px;
}
.form_textarea {
  width: 100%;
    margin-top: 5px;
    border: 1px solid #8b8f94;
    margin-bottom: 10px;
    background: none;
}
 .register_details {
 background: #0f4778;
    padding: 15px 30px 15px 30px;
    color: #fff;
    cursor: pointer;
    width: 100px;
    border: none;
    font-size: 18px;
    margin-top: 20px;
}
.success_msg {
  border: 2px solid green;
  float: left;
  margin-bottom: 25px;
  padding: 6px 8% 6px 10px;
  width: auto;
  color: green;
}
.error_msg {
  background: #fca1a1 none repeat scroll 0 0;
  border: 2px solid #f00;
  color: #B71D1D;
  float: left;
  margin-bottom: 25px;
  padding: 5px 12px;
  width: 48%;
}
#housetourbg {
	position: relative;
max-width: 1920px;
margin: 0 auto;
background-color: #E7E7E8;
width: 100%;
	}
#housetourbg video {
	max-width:1140px;
	margin: 70px 20% 90px;
	}
.playpause {
    background-image:url(../housetour/play_arrow.png);
    background-repeat:no-repeat;
    width: 1140px;
	height: 607px;
	position: absolute;
	left: 0%;
	right: 3px;
	top: -5px;
    bottom:0%;
    margin:auto;
    background-size:cover;
	background-position: top;
	
}
.next_tour_arrow img {
    position: absolute;
    right: -100px;
    /* bottom: 0px; */
    margin-top: -20px;
	max-width: 90px;
}
.groud_flour_arrow img {
	 position: absolute;
    left: -145px;
    /* bottom: 0px; */
    margin-top: -40px;
    max-width: 140px;
	}
.needmoreinfo {
	display:none;
	}
.needmoreinfo img {
	position: absolute;
    right: -155px;
    /* bottom: 0px; */
    margin-top: 0px;
    max-width: 170px;
	}
.bookbutton {
    position: absolute;
    right: 20px;
}
.bookbutton a {
    padding: 5px 10px;
    border: 1px solid #a0a0a0;
}
.tour_dates {
	position:relative;
	}
.forbiggerfonts {
	font-size:15px;
	line-height: 17px;
	}
.date_box .bigfonts {
    font-size: 16px;
	padding-bottom: 5px;
    display: inline-block;
}
.details_box {
    position: relative;
    top: 15px;
	line-height: 18px;
	text-transform:capitalize;
}
.glyphicon-chevron-left:before, .glyphicon-chevron-right:before {
	content:"" !important;
	}
.carousel-control span img {
    max-width: 40px;
}
.col_full h1 {
	color: #f5641e !important;
	font-size:22px !important;
	}
#new_page_house_tours_new {
	font-size: 30px !important;
    color: #003c70;
    text-transform: capitalize !important;
    font-weight: bold;
    font-family: calibri !important;
	padding-bottom: 10px !important;
    margin-bottom: 20px !important;
	}
.forbiggerfonts p:first-child {
	padding-top:0px !important;
	}
.forbiggerfonts p {
    color: #75787f;
	font-weight: 400;
}
.forbiggerfonts p strong {
	    color: #0f4678;
	}
.form_row label {
	color: #104771;
	}
.broform100 input[type=checkbox] {
    width: auto;
    height: auto;
    vertical-align: middle;
    width: 20px;
    height: 20px;
    margin-right: 5px;
    padding: 5px;
}
.checkboxbro span {
    font-size: 16px;
    display: inline-block;
    width: 90%;
	vertical-align: top;
	line-height:1.5;
}
@media (max-width:640px) {
	.next_tour_arrow img, .groud_flour_arrow img, .needmoreinfo img{
		display:none !important;
		}
	.tour_dates {
		width:100%;
		}
	.bookbutton {
		position: relative;
		 right:0px !important;
		/* bottom: 4px; */
		margin-top: 25px;
}
#bookform h1 {
	font-size:18px;
	margin-top:20px
	}
#carousel-example-generic {
	display:none;
	}
.register_details {
	margin-bottom:20px;
	}
    .home-gallery-p {
    margin-left:0px;
  }
  .home-gallery-h2 h2, .int-land-h2 h2 {
    margin-left: 0px;
}
	}
@media (min-width:640px) {
	.details_box {
		line-height: 18px;
		}
		.bookbutton {
	bottom: 25px;
    }
    .open-hrs-p {
        width: 69%;
    }
    .main {
    max-width: 1920px !important;
  }
  .home-gallery-h2 {
    margin-left: 50px;
  }
  .home-gallery-p {
    width: 58%;
    
  }
  .home-gallery-p p {
    margin-top: 60px;
  }
	}
@media (min-width:641px) and (max-width:770px){
.col_last {
	float:right;
	}
form .col_half {
    width: 100%;
}
}
</style>
<section class="open-hrs-header">
	<div class="interior-header-bg"></div>
	<h1 class="page_house_tours_new">Interior & Landscape Design</h1>
	<p class="open-hrs-p">From Build to Design - we have our own Interior and Landscape
    Design Services, including full 3D renders of your chosen Design Plans.</p>
</section>
<div style="width:100%;max-width:1920px;background-color:#e6e6e7;">
<section id="intdesbg">
	<h3 class="int-design-text">
    Our interior and landscape designs are created in-house by Design Team. When the designs are complete 
    we liaise with our construction team to provide detailed cost estimates and advice on timeframe and execution.    
    </h3>
</section>
</div>

<div style="width:100%;max-width:1920px;background-color:#fff;">
<div class="home-gallery">
  <div class="home-gallery-text">
	<div class="home-gallery-h2">
  		<h2>Our<br>
		  Services</h2>
	</div>
<div class="home-gallery-p" style="float:left">
  <p>Click below to find out more about each service and the benefits of integrating
our interior and landscape design packages with your home refurbishment plans.</p>
</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mob-space" style="height:30px">
				
</div>
<div class="col-xs-12 col-sm-12 col-md-2 col-lg-2" style="width:2%">
				
</div>
<div class="col-xs-8 col-sm-8 col-md-4 col-lg-4" style="border:2px solid #183e70;padding:25px;margin-right:30px">
	<h3 class="our-site-title" style="margin-bottom: 25px;margin-top: 0px;">INTERIOR <br>DESIGN</h3>
	<div class="event-button">
			<a href="#interior">Find out more</a>
			<div class="event-button-arrow"></div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-1 col-lg-1 mob-space" style="padding:25px;margin-right:3px">
	
</div>
<div class="col-xs-8 col-sm-8 col-md-4 col-lg-4" style="border:2px solid #183e70;padding:25px">
	<h3 class="our-site-title" style="margin-bottom: 25px;margin-top: 0px;">LANDSCAPE <br>DESIGN</h3>
	<div class="event-button">
			<a href="#landscape">Find out more</a>
			<div class="event-button-arrow"></div>
	</div>
</div>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 mob-space" style="height:30px">
				
</div>
</div>
</div>
	
<section class="upcoming-dates-bg" style="padding: 140px 0 70px 0;" id="interior">
		<div class="upcoming-dates">
		    <div class="col_full">
			<h2>INTERIOR DESIGN</h2>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<p class="design-descr">Your interior design options often impact the envelope of your design, so we encourage all of our clients to think about the finishing touches at the very beginning. Our Interior Design Service goes a step further, and helps you to visualise the space you are going to create. Following a consultation with a member of our Design Team, we will produce a digital mood board depicting the finishes, electrical plans and 3D rendered images to illustrate how the finished extension will look from the inside.</p>
            <img src="/images_new_design/image_sample_interior.jpg">
			</div>
			
		</div>
</section>

<div style="width:100%;max-width:1920px;background-color:#fff;">
<div class="home-gallery">
  <div class="home-gallery-text">
	<div class="int-land-h2">
  		<h2>The<br>
		  Process</h2>
	</div>
    <div class="int-design-p">
        <p>Read through our easy to follow stage process, on how we approach each client’s proposal,
from initial drawings to final build. If you have further questions, please contact us <a href="http://buildteam.com/contact.html">here</a>.</p>
    </div>
    </div>
    <div class="title-border"></div>
    <div class="process-text">
        <h5>DESIGN BRIEF</h5>
        <span class="dot"></span>
        <p>We survey the site and develop a Design Brief in consultation with the homeowner.</p>

        <h5>INITIAL IDEAS</h5>
        <span class="dot"></span>
        <p>We prepare a shortlist of options - with Digital Mood Boards and sketched Design layout.</p>

        <h5>DETAILED DESIGN</h5>
        <span class="dot"></span>
        <p>Upon approval of the Lead Design, we develop the final drawing set - which includes the furniture layout, lighting and electrical
diagram, flooring and decorating schedule.</p>

        <h5>KITCHEN SPECIFICATION</h5>
        <span class="dot"></span>
        <p>We work with the homeowners to understand budget and requirements for the kitchen and respond with advice on sourcing
recommendations.</p>

        <h5>3D MODEL</h5>
        <span class="dot"></span>
        <p>Once the scheme has been finalised we produce a 3D rendered image of the new extension, so that you will be able to visualise the
finished room.</p>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:30px">
				
                </div>
                
                <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5">
                    <h3 class="our-site-title" style="margin-bottom: 25px;margin-top: 0px;">THE <br>OUTPUTS</h3>
                    <h5>Stage 1</h5>
                    <span class="dot"></span>
                    <p>Floorplan with furniture layout and specifications</p>
                    <h5>Stage 2</h5>
                    <span class="dot"></span>
                    <p>Lighting and Electrical plans</p>
                    <h5>Stage 3</h5>
                    <span class="dot"></span>
                    <p style="margin-bottom: 56px;">3D render of finished room</p>
                    
                </div>
                <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5 last">
                    <h3 class="our-site-title" style="margin-bottom: 25px;margin-top: 0px;">WHAT IS <br>INCLUDED?</h3>
                    <span class="dot"></span>
                    <p>Furniture Layout</p>
                    <span class="dot"></span>
                    <p>Lighting and Electical specifications</p>
                    <span class="dot"></span>
                    <p>Colour Scheme</p>
                    <span class="dot"></span>
                    <p>Flooring advice and specification</p>
                    <span class="dot"></span>
                    <p>Kitchen Layout and Budgeting Advice</p>
                    <span class="dot"></span>
                    <p>Introduction to suppliers and access to trade pricing where requested / applicable</p>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:30px"></div>
                <div class="col_one_third" style="border:none"></div>
<div class="col_one_third ild-button" style="border:none;margin-bottom: 1px;">
<div class="project-button" style="margin: 25px 35px;"><a href="https://www.buildteam.com/book-visit.html?type=standard">Enquire now
        <div class="project-button-arrow"></div></a>
        </div>
</div>
<div class="col_one_third" style="border:none"></div>
</div>          
                
    </div>
</div>




<section class="upcoming-dates-bg" style="padding: 140px 0 70px 0;" id="landscape">
		<div class="upcoming-dates">
		    <div class="col_full">
			<h2>LANDSCAPE DESIGN</h2>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<p class="design-descr">Our Landscape Design Service is perfectly suited to clients who wish to revamp their outdoor space in conjunction with the rear kitchen design. The service starts with an in-depth consultation with a member of our Design Team –we will then prepare design and layout options for the outdoor space –and advise on materials, lighting options and outdoor structures –such as a hardwood pergola.</p>
            <img src="/images_new_design/bt_landscape-service_desktop_replacement1.jpg">
			</div>
			
		</div>
</section>

<div style="width:100%;max-width:1920px;background-color:#fff;">
<div class="home-gallery">
  <div class="home-gallery-text">
	<div class="int-land-h2">
  		<h2>The<br>
		  Process</h2>
	</div>
    <div class="int-design-p">
        <p>Read through our easy to follow stage process, on how we approach each client’s proposal,
from initial drawings to final build. If you have further questions, please contact us <a href="http://buildteam.com/contact.html">here</a>.</p>
    </div>
    </div>
    <div class="title-border"></div>
    <div class="process-text">
        <h5>DESIGN BRIEF</h5>
        <span class="dot"></span>
        <p>We survey the site and develop a Design Brief in consultation with the homeowner.</p>

        <h5>INITIAL IDEAS</h5>
        <span class="dot"></span>
        <p>We prepare a shortlist of options - with Digital Mood Boards and sketched Design layout.</p>

        <h5>DETAILED DESIGN</h5>
        <span class="dot"></span>
        <p>Upon approval of the Lead Design, we develop the final drawing set - which includes the garden layout, lighting plan, proposed materials and planting schedule.</p>

        <h5>STYLING CONSTRUCTION</h5>
        <span class="dot"></span>
        <p>Our Build Department will provide you with a final quote for the work.</p>

        <h5>STYLING</h5>
        <span class="dot"></span>
        <p>We can help re-connect supplies for you to source, e.g; Plants, Furniture and other Styling Elements.</p>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:30px">
				
                </div>
                
                <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5">
                    <h3 class="our-site-title" style="margin-bottom: 25px;margin-top: 0px;">THE <br>OUTPUTS</h3>
                    <h5>Stage 1</h5>
                    <span class="dot"></span>
                    <p>Landscape Plan & Specifications</p>
                    <h5>Stage 2</h5>
                    <span class="dot"></span>
                    <p>Lighting Diagram</p>
                    <h5>Stage 3</h5>
                    <span class="dot"></span>
                    <p style="margin-bottom: -3px;">Planting Structure</p>
                    
                </div>
                <div class="col-xs-10 col-sm-10 col-md-5 col-lg-5 last">
                    <h3 class="our-site-title" style="margin-bottom: 25px;margin-top: 0px;">WHAT IS <br>INCLUDED?</h3>
                    <span class="dot"></span>
                    <p>Lighting</p>
                    <span class="dot"></span>
                    <p>Planting</p>
                    <span class="dot"></span>
                    <p>Fencing / Shielding</p>
                    <span class="dot"></span>
                    <p>Styling</p>
                    
                </div>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="height:30px"></div>
                <div class="col_one_third" style="border:none"></div>
<div class="col_one_third ild-button" style="border:none;margin-bottom: 1px;">
<div class="project-button" style="margin: 25px 35px;"><a href="https://www.buildteam.com/book-visit.html?type=standard">Enquire now
        <div class="project-button-arrow"></div></a>
        </div>
</div>
<div class="col_one_third" style="border:none"></div>
</div>          
                
    </div>
</div>

<div style="width:100%;max-width:1920px;background-color:#e6e6e7;height:140px;">
</div>

<?php
        require_once('explore-our-site.php');
    ?>

 <script>
 $('.video').parent().click(function () {
    if($(this).children(".video").get(0).paused){
        $(this).children(".video").get(0).play();
        $(this).children(".playpause").fadeOut();
    }else{
       $(this).children(".video").get(0).pause();
        $(this).children(".playpause").fadeIn();
    }
});
 </script>
 <script>
 $(".bookbutton a").click(function(){
	 var getarea = $(this).parents(".tour_dates").find(".details_box").children("span").html();
	 $("#bookform input[name=tour_sttending]").val(getarea);
	 $(".needmoreinfo").fadeIn(2000);
	 //alert(getarea);
	// $(window).scrollTop($('#bookform').offset().top);
	 $("html, body").animate({
        scrollTop: $('#bookform').offset().top - 100
    }, 1000);

	 });
 </script>
<script>
$('.carousel').carousel();
</script>
<?php require_once('./inc/footer.inc.php'); ?>