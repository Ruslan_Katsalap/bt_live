<?php

if (is_int(strpos($_SERVER['REQUEST_URI'], '/side-return-extensions-page.html')) && $_SERVER['REQUEST_URI']!='/what-we-do/side-return-extensions-page.html') {
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: /what-we-do/side-return-extensions-page.html');
  exit;
}

if (is_int(strpos($_SERVER['REQUEST_URI'], '/design-and-build-page.html')) && $_SERVER['REQUEST_URI']!='/what-we-do/design-and-build-page.html') {
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: /what-we-do/design-and-build-page.html');
  exit;
}

/*if (is_int(strpos($_SERVER['REQUEST_URI'], '/our-story-page.html')) && $_SERVER['REQUEST_URI']!='/about-us/our-story-page.html') {
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: /about-us/our-story-page.html');
  exit;
}*/

if ((is_int(strpos($_SERVER['REQUEST_URI'], '/design-service-page.html')) || is_int(strpos($_SERVER['REQUEST_URI'], '/architectural-design-phase-page.html'))) && $_SERVER['REQUEST_URI']!='/what-we-do/architectural-design-phase-page.html') {
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: /what-we-do/architectural-design-phase-page.html');
  exit;
}

if (is_int(strpos($_SERVER['REQUEST_URI'], '/guarantees-and-insurance-page.html')) && $_SERVER['REQUEST_URI']!='/about-us/guarantees-and-insurance-page.html') {
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: /about-us/guarantees-and-insurance-page.html');
  exit;
}

if (is_int(strpos($_SERVER['REQUEST_URI'], '/refurbishment-page.html')) && $_SERVER['REQUEST_URI']!='/what-we-do/refurbishment-page.html') {
  header('HTTP/1.1 301 Moved Permanently');
  header('Location: /what-we-do/refurbishment-page.html');
  exit;
}

require_once('./inc/header.inc.php');

?>
  	<div class="<?php echo (defined('hidePageMedia')?'full':'left'); ?>">
      
    	<?php /*if ($bc_trail) { echo $bc_trail; } else { ?>
        <div id="bc"><a href="/" title="Home">Home</a> &rsaquo; 
        <b><?php echo htmlspecialchars($title); ?></b>
        </div>
      <?php } */ ?>
      
    	<h1><?php echo htmlspecialchars($title); ?></h1>
			<?php 
      if (is_int(strpos($page_content, '<p')) || is_int(strpos($page_content, '<ul')) || is_int(strpos($page_content, '<ol'))) echo $page_content;
      else echo '<p>'.nl2br($page_content).'</p>';
      ?>
      
		</div>
<?php

require_once('./inc/footer.inc.php');

?>