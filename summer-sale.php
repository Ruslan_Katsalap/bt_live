<?php
require_once('./inc/util.inc.php');
require_once('./inc/header.inc.php');
?>
<link href="css/bootstrap.css" rel="stylesheet">
<style>
.maximgsize img {
	max-width:60% !important;
	}
.toptitleimg {
	padding:40px 0px;
	text-align:center;
	}
.topjanubanner {
	padding-bottom:20px;
	}
.janunopadding {
	padding:0px !important;
	}
.topjanubanner {
	margin-bottom:30px;
	
	}	
.forcentertable {
	display:table;
	width:100%;
	text-align:center;
	height:135px;
	}
.forheight {
	display:table-cell;
	vertical-align:middle;
	}
.forheight img {
	max-width:100%;
	}	
.janurighttitle, .janurightdesc {
    padding-left: 15px;
}	
.janurighttitle p{
	color:#104777;
	font-weight:bold;
	margin-left:0px;
	padding: 0px;
    margin-bottom: 0px;
	}
.forheight .januornage, .forheight .janublue {
	font-size:40px;
	}
.januornage {
	color:#f7921e;
	}
.janublue {
	color:#104777;
	}
.janurighttitle {
    padding-top: 25px;
}			
.janurighttitle p{
	font-size:18px;
	}
.janurightdesc p {
	font-size:18px;
	color:#74787f;
	font-weight: normal;
	line-height:20px;
	font-family:calibri !important;
	text-align:justify;
	}
p.januornage.janutermstitle {
    font-size: 18px;
    font-weight: bold;
}
.januterms p {
	
	}		
.janudotted {
	padding-top:20px;
	padding-bottom:20px;
	border-bottom: 1px solid #e0e0e0;
	}
.januterms p {
	font-size:16px;
	padding:0px;
	letter-spacing: 1px;
    line-height: 19px;
	}	
.janutermstitle {
	margin-top:20px;
	font-weight:bold;
	}
.forheight .januornage span {
	color:#ff0000;
	}	
.januterms .januornage {
	color:#104777;
	}
.januterms .janublue {
	color:#74787f;
	}		
@media (max-width:640px) {
	.toptitleimg {
	padding:20px 0px;
	}
	.topjanubanner {
    margin-bottom: 0px;
}
.janurightdesc p {
	text-align:justify;
	 }
	 .janurighttitle p {
    
    text-align: center;
}
.januterms {
	text-align:center;
	}
	.janurighttitle, .janurightdesc {
    padding-left: 0px;
}	
.maximgsize img {
	max-width:80% !important; 
	}
	}														
</style>
<!--<div class="title">
<h1 class="commonpagetitle">Summer Sale</h1>
</div>-->
<div class="">
	<div class="toptitleimg maximgsize">
    	<img src="images/summer_sale/sale_text.png" alt="">
    </div>
</div>

<div class="janudotted">
	<div class="col-sm-4 janunopadding">
    	<div class="forcentertable">
        	<div class="forheight">
            	<img src="images/summer_sale/landscape_sale.jpg" alt="">
            	<!--<div class="januornage">
                    <span>50%</span>
                </div>
                <div class="janublue">
                    <span>OFF</span>
                </div>-->
            </div>
        </div>
        
    </div>
    <div class="col-sm-8 janunopadding">
    	<div class="janurighttitle">
        	<p>LANDSCAPE DESIGN SERVICE</p>
        </div>
        <div class="janurightdesc">
        	<p>You shouldn’t underestimate the impact your garden will have on your new extension. Many of our clients choose to redesign their garden while they are redesigning their kitchen extension, mostly because they can ensure both designs will complement each other. Our Landscape Design Service includes an in-depth consultation with an experienced member of our Design Team. We will create various design and layout options for your garden. You are then free to amend the proposed options until you have your final design.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="janudotted">
	<div class="col-sm-4 janunopadding">
    	<div class="forcentertable">
        	<div class="forheight">
            	<img src="images/summer_sale/interior_2_sale.jpg" alt="">
            	<!--<div class="januornage">
                    <span>30%</span>
                </div>
                <div class="janublue">
                    <span>OFF</span>
                </div>-->
            </div>
        </div>
        
    </div>
    <div class="col-sm-8 janunopadding">
    	<div class="janurighttitle">
        	<p>INTERIOR DESIGN SERVICE</p>
        </div>
        <div class="janurightdesc">
         	<p>To ensure you make the most of your final design, we encourage all of our clients to think about the finishing touches at the very beginning, as we feel the interior of your extension is important to help shape your ‘shell’ design. Our Interior Design Service helps you visualise your space by offering guidance on the materials you want to use and how they will interact with features such as Velux windows and lighting.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="janudotted">
	<div class="col-sm-4 janunopadding">
    	<div class="forcentertable">
        	<div class="forheight">
            	<img src="images/summer_sale/fast_sale.jpg" alt="">
            	<!--<div class="januornage">
                    <span>25%</span>
                </div>
                <div class="janublue">
                    <span>OFF</span>
                </div>-->
            </div>
        </div>
        
    </div>
    <div class="col-sm-8 janunopadding">
    	<div class="janurighttitle">
        	<p>FAST TRACK SERVICE</p>
        </div>
        <div class="janurightdesc">
        	<p>Our Design Team offers a Fast Track Service, which is great for those of you who want to be on site as soon as planning has been granted. It enables us to start your structural drawings immediately. In the event your case officer requests amendments during the planning process, we make these free of charge and amend the structural calculations and drawings accordingly. We can then book in your Pre Contract Meeting and send over your Detailed Schedule of Works.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="janudotted">
	<div class="col-sm-4 janunopadding">
    	<div class="forcentertable">
        	<div class="forheight">
            	<img src="images/summer_sale/interior_sale.jpg" alt="">
            	<!--<div class="januornage">
                    <span>25%</span>
                </div>
                <div class="janublue">
                    <span>OFF</span>
                </div>-->
            </div>
        </div>
        
    </div>
    <div class="col-sm-8 janunopadding">
    	<div class="janurighttitle">
        	<p>PARTY WALL SERVICE</p>
        </div>
        <div class="janurightdesc">
        	<p>If you are keen to start building soon, we would recommend starting the Party Wall process as soon as we have submitted your planning application - this is mainly because it is a time consuming process and we find it beneficial to run it alongside the planning window. We can sort everything out for you and serve the relevant Notices and undertake the Schedule of Conditions on your behalf. Our Party Wall surveyor is a RICS Qualified Surveyor, and has years of experience dealing with all kinds of neighbours and varied agreements.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="januterms">
	
    <div class="col-sm-12 janunopadding">
    	<p class="januornage janutermstitle">Terms and Conditions</p>
        <p class="janublue janutermsdetails"><span class="januornage">1) </span>You can avail of multiple offers as detailed within this page, however these offers cannot be used in conjunction with any offer you may have received. <span class="januornage">2) </span>Ancillary services are only available as part of a Design Phase instruction. <span class="januornage">3) </span>The discount for the Party Wall Service applies to one Notice and Schedule of Condition only and can only be used if Design Team is instructed to serve all relevant Notices. <span class="januornage">4) </span>These offers are only valid for 30 days from the date of your site visit.</p>
    </div>
</div>

<?php
require_once('./inc/footer.inc.php');
?>