<?php

@set_time_limit(0);

$type = 'html';
if (isset($_GET['type'])) $type = (string)$_GET['type'];
if ('html'!=$type && 'xml'!=$type) $type = 'html'; 

require_once('./inc/util.inc.php');

$host = 'http://'.$_SERVER['HTTP_HOST'].'/';

$a_url = array(); // unique page URLs

if ('html'==$type) {
$meta_title = 'Sitemap | BuildTeam';

require_once('./inc/header.inc.php');

?>
  <div class="full">
    <h1>Sitemap</h1>
      
<?php

  // read cache file if exists
  if (file_exists('./xml/sitemap.html')) {
    include './xml/sitemap.html';
  }
  else {
    $fp = fopen('./xml/sitemap.html', 'w');
    
    $buf = getURLs(0, $host, 'html');
    
    fwrite($fp, $buf);
    fclose($fp);
    
    updateLastMod();
    
    echo $buf;
  }
?>
  </div>
<?php

require_once('./inc/footer.inc.php');
} // end if type=='html'
else { // if XML
  
  header('Content-Type: text/xml; charset=UTF-8');

  echo '<?xml version="1.0" encoding="utf-8"?>';
?>
<urlset
      xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
      xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
      xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
      xsi:schemaLocation="
            http://www.sitemaps.org/schemas/sitemap/0.9
            http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">

<?php
  // read cache file if exists
  if (file_exists('./xml/sitemap_main.xml')) {
    include './xml/sitemap_main.xml';
  }
  else {
    $fp = fopen('./xml/sitemap_main.xml', 'w');
    
    $buf = getURLs(0, $host, 'xml');

    fwrite($fp, $buf);
    fclose($fp);
    
    updateLastMod();

    echo $buf;
  }
?>
</urlset>
<?php
} // end if type=='xml'

// Update Blog MAP date id feeded
if (file_exists('./blog/sitemap.xml')) {
  $modtime = date('c', filectime('./blog/sitemap.xml'));
  //echo $modtime.'!!!';#debug
  
  $buf = file_get_contents('./sitemap.xml');
  
  if (preg_match('/<sitemap><loc>http\:\/\/buildteam.com\/blog\/sitemap.xml<\/loc><lastmod>(.+?)<\/lastmod><\/sitemap>/s', $buf, $out)) {
    //print_r($out);#debug
    if ($modtime!=$out[1]) {
      $buf = str_replace('<sitemap><loc>http://buildteam.com/blog/sitemap.xml</loc><lastmod>'.$out[1].'</lastmod></sitemap>', '<sitemap><loc>http://buildteam.com/blog/sitemap.xml</loc><lastmod>'.$modtime.'</lastmod></sitemap>', $buf);
      
      $fp = fopen('./sitemap.xml', 'w');
      fwrite($fp, $buf);
      fclose($fp);
    }
    
  }

  
}

// Functions area:

function getURLs($pid = 0, $url = '/', $type='xml') {
  global $a_url;
  
	$ret = '';

	$i = 0;

	$rs = getRs("SELECT page_id, page_name, nav_name, page_code, url, is_link, date_modified FROM page WHERE parent_page_id = {$pid} AND is_enabled = 1 AND is_active = 1 ORDER BY sort");

	while ($row = mysqli_fetch_assoc($rs)) {

		if ($i == 0 && $pid > 0 && 'html' == $type) {
			$ret .= '<ul style="line-height:30px;">';
		}
    
    $href = '';
    $priority = 1;

    if ($row['is_link'] != 0) {
      
      if ($row['page_code'] == 'index') {
        $href = $url;
      }   
      elseif ( strlen($row['page_code']) > 0 ) {
        $href = $url . $row['page_code'] . '.html';
      }
      elseif ( strlen($row['url']) > 0 ) {
        $href = $row['url'];
      }

    }
    
    if ('html' == $type) {
      $row['page_name'] = htmlspecialchars($row['page_name'], ENT_QUOTES, 'UTF-8');
      
      if ( 0 == $pid ) {
        $ret .= '<p>'; 
      }
      else {
        $ret .= '<li>';
      }
      
      if ($href) {
        $ret .= '<'.'a href="'.htmlspecialchars($href, ENT_QUOTES, 'UTF-8').'">';
      }
      else {  
        $ret .= '<font color="#336699">';
      }
      
      $ret .= $row['page_name'];
      
      if ($href) {
        $ret .= '</a>';
      }
      else {
        $ret .= '</font>';
      }
      
    }
    
    $level = str_replace('http://'.$_SERVER['HTTP_HOST'].'/', '', $href);
    $level = str_replace('http://buildteam.com/', '', $level);
    $level = explode('/', $level);
    //$ret .= '<pre>'.print_r($level,1).'</pre>';#debug
    if (count($level)==1 && $href!='http://'.$_SERVER['HTTP_HOST'].'/') $priority = 0.9;
    elseif (count($level)>1) $priority = 1 - count($level)*0.1;
    
    if ($href && 'xml'==$type && !in_array($href, $a_url)) {
      $ret .= '<url>'.
      '<loc>'.htmlspecialchars($href, ENT_QUOTES, 'UTF-8').'</loc>'.
      ($row['date_modified']?'<lastmod>'.date('c', $row['date_modified']).'</lastmod>':'').
      '<changefreq>weekly</changefreq>'.
      '<priority>'.sprintf('%1.1f', $priority).'</priority>'.
      '</url>';
    }
    
    // News:
    if ('news' == $row['page_code']) {
      $rs2 = getRs("SELECT c.news_category_code, c.news_category_name, n.news_id, n.news_code, n.title, n.imagefile, n.description, n.date_release FROM news n INNER JOIN news_category c ON c.news_category_id = n.news_category_id WHERE c.is_enabled = 1 AND c.is_active = 1 AND n.is_enabled = 1 AND n.is_active = 1 ORDER BY n.date_release DESC LIMIT 10000");
      
      if (mysqli_num_rows($rs2)) {
        
        if ('html'==$type) {
          $ret .= '<ul style="line-height:30px;">';
        }
        
        while ($row2 = mysqli_fetch_assoc($rs2)) {
          
          $href = 'http://'.$_SERVER['HTTP_HOST'].'/news/' . $row2['news_category_code'] . '/' . $row2['news_code'] . '.html';
          
          if ('html'==$type) {
            $ret .= '<li><a href="'.htmlspecialchars($href, ENT_QUOTES, 'UTF-8').'">' . $row2['title'] . ($row2['date_release'] > 0 ? ' ('.date('j F Y', $row2['date_release']).')' : ''). '</a></li>';
          }
          elseif ($href && 'xml'==$type && !in_array($href, $a_url)) {
            $ret .= '<url>'.
            '<loc>'.htmlspecialchars($href, ENT_QUOTES, 'UTF-8').'</loc>'.
            ($row2['date_release']?'<lastmod>'.date('c', $row2['date_release']).'</lastmod>':'').
            '<changefreq>weekly</changefreq>'.
            '<priority>0.7</priority>';
            
            if ($row2['imagefile']) {
              $ret .= '<image:image>'.
              '<image:loc>http://'.$_SERVER['HTTP_HOST'].'/media/'.htmlspecialchars($row2['imagefile'], ENT_QUOTES, 'UTF-8').'</image:loc>'.
              '<image:caption>'.htmlspecialchars($row2['title'], ENT_QUOTES, 'UTF-8').'</image:caption>'.
              '</image:image>';
            }
            
            $ret .= '</url>';
          }
        }
        
        if ('html'==$type) {
          $ret .= '</ul>';
        }
        
      }
      
    }
    
    // Press Releases:
    if ('press_releases' == $row['page_code']) {
    
      $rs2 = getRs("SELECT id, url_code, title, imagefile, date_release, time_release FROM press_release ORDER BY date_release DESC, time_release DESC LIMIT 10000");
      
      if (mysqli_num_rows($rs2)) {
        
        if ('html'==$type) {
          $ret .= '<ul style="line-height:30px;">';
        }
        
        while ($row2 = mysqli_fetch_assoc($rs2)) {
          
          $href = 'http://'.$_SERVER['HTTP_HOST'].'/press_releases/' . $row2['url_code'] . '.html';
          
          $row2['time_release'] = trim($row2['time_release']);
            
          $date_release = $row2['date_release'];
          
          $date_release = date('Y-m-d', $date_release);
          
          if ($row2['time_release']) $date_release .= ' '.$row2['time_release'];
          
          $date_release = strtotime($date_release);
          
          $date_release = gmdate('d M Y H:i', $date_release);
          
          if ($date_release) $date_release .= ' GMT';
          
          if ('html'==$type) {
            $ret .= '<li><a href="'.htmlspecialchars($href, ENT_QUOTES, 'UTF-8').'">' . $row2['title'] . ($date_release ? ' ('.$date_release.')' : ''). '</a></li>';
          }
          elseif ($href && 'xml'==$type && !in_array($href, $a_url)) {
            $ret .= '<url>'.
            '<loc>'.htmlspecialchars($href, ENT_QUOTES, 'UTF-8').'</loc>'.
            ($date_release?'<lastmod>'.date('c', strtotime($date_release)).'</lastmod>':'').
            '<changefreq>weekly</changefreq>'.
            '<priority>0.7</priority>';
            
            if ($row2['imagefile']) {
              $ret .= '<image:image>'.
              '<image:loc>http://'.$_SERVER['HTTP_HOST'].'/media/'.htmlspecialchars($row2['imagefile'], ENT_QUOTES, 'UTF-8').'</image:loc>'.
              '<image:caption>'.htmlspecialchars($row2['title'], ENT_QUOTES, 'UTF-8').'</image:caption>'.
              '</image:image>';
            }
            
            $ret .= '</url>';
          }
        }
        
        if ('html'==$type) {
          $ret .= '</ul>';
        }
        
      }
      
    }    
    
    // Projects:
    if (is_int(strpos($row['page_code'], 'projects-'))) {
      
      $project_category_code = str_replace('projects-', '', $row['page_code']);
      
      if ('html'==$type) {
        $ret .= '<ul style="line-height:30px;">';
      }
      
      $rs2 = getRs("SELECT c.project_category_code, c.project_category_name, i.filename, p.project_code, p.project_name, p.date_modified FROM project_category c INNER JOIN (project_image i INNER JOIN project p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 AND c.project_category_code = '" . formatSql($project_category_code) . "' ORDER BY p.sort, p.project_id, i.sort, i.project_image_id");      
      
      $project_code = '';
      
      while ($row2 = mysqli_fetch_assoc($rs2) ) {
        
        if ($project_code != $row2['project_code']) {
          
          if ('html'==$type && $project_code) $ret .= '</li>';
          if ('xml'==$type && $project_code) $ret .= '</url>';
          
          $project_code = $row2['project_code'];
          
          $href = 'http://'.$_SERVER['HTTP_HOST'].'/'.$row2['project_code'].'-'.str_replace('side-return-extensions', 'side-return-extension', $project_category_code).'.html';
          $priority = 0.7;
          
          // <img src="/phpthumb/phpThumb.php?src='.urlencode('/projects/' . $row['filename']) . '&amp;w=150&amp;h=150" width="150" alt="' . htmlentities($row['project_name']) . '" /><span style="display:block;margin-top:5px">'.htmlentities($row['project_name'])
          
          if ('html'==$type) {
            $ret .= '<li><a href="'.htmlentities($href, ENT_QUOTES, 'UTF-8').'">'.htmlentities($row2['project_name'], ENT_QUOTES, 'UTF-8').'</a>';
            
            // images
            //$ret .= '<img src="http://'.$_SERVER['HTTP_HOST'].'/projects/' . htmlentities($row2['filename'], ENT_QUOTES, 'UTF-8') . '" alt="' . htmlentities($row2['project_name'], ENT_QUOTES, 'UTF-8') . '" /><br/>';#debug
           
          }
          else { // 'xml'==$type 
            $ret .= '<url>'.
            '<loc>'.htmlspecialchars($href, ENT_QUOTES, 'UTF-8').'</loc>'.
            ($row2['date_modified']?'<lastmod>'.date('c', $row2['date_modified']).'</lastmod>':'').
            '<changefreq>weekly</changefreq>'.
            '<priority>0.7</priority>';
            
            $ret .= '<image:image>'.
              '<image:loc>http://'.$_SERVER['HTTP_HOST'].'/projects/'.htmlspecialchars($row2['filename'], ENT_QUOTES, 'UTF-8').'</image:loc>'.
              '<image:caption>'.htmlentities($row2['project_name'], ENT_QUOTES, 'UTF-8').'</image:caption>'.
              '</image:image>';
            
          }
          
        }
        else {
          //debug
          /*
          if ('html'==$type) {
            $ret .= "\n".'!!!!<img src="http://'.$_SERVER['HTTP_HOST'].'/projects/' . htmlentities($row2['filename'], ENT_QUOTES, 'UTF-8') . '" alt="' . htmlentities($row2['project_name'], ENT_QUOTES, 'UTF-8') . '" /><br/>'."\n";
          }
          */
          
          if ('xml'==$type) {
            $ret .= '<image:image>'.
              '<image:loc>http://'.$_SERVER['HTTP_HOST'].'/projects/'.htmlspecialchars($row2['filename'], ENT_QUOTES, 'UTF-8').'</image:loc>'.
              '<image:caption>'.htmlentities($row2['project_name'], ENT_QUOTES, 'UTF-8').'</image:caption>'.
              '</image:image>';
          }
          
        }
      
      }
      
      if ('html'==$type && $project_code) $ret .= '</li>';
      if ('xml'==$type && $project_code) $ret .= '</url>';
      
      
      if ('html'==$type) {
        $ret .= '</ul>';
      }
      
    }
    
		// check for kids

		$ret .= getURLs($row['page_id'], $url . $row['page_code'] . '/', $type);

		if ('html' == $type) {
      if ( 0 == $pid ) {
        $ret .= '</p>'; 
      }
      else {
        $ret .= '</li>';
      }
    }
    
		$i++;

    if ($href && !in_array($href, $a_url)) {
      $a_url[] = $href;
    }
    
	}
  
	if ( $i > 0 and $pid > 0 && 'html' == $type) {
		$ret .= '</ul>';
	}  
  
	return $ret;

}

function updateLastMod() {
  $buf = file_get_contents('./sitemap.xml');
  
  $buf = preg_replace('/<sitemap>.+?<\/sitemap>/s', '<sitemap><loc>http://buildteam.com/sitemap_main.xml</loc><lastmod>'.date('c').'</lastmod></sitemap>', $buf, 1);
  
  $fp = fopen('./sitemap.xml', 'w');
  fwrite($fp, $buf);
  fclose($fp);
}
?>