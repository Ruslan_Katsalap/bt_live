<?php
require_once('./inc/util.inc.php');
require_once('./inc/header.inc.php');
?>

<style>
body {
	background:url(../images/archi_design_phase/background_image.jpg) no-repeat;
	background-size: cover;
    background-attachment: fixed;
	}
.conveyor-belt-cover {
	max-width:100%;
	padding:0px 20px;
	margin-left:0px !important;
	}	
.c-paragraph, .c-title {
	padding-left:0px !important;
	}
.conveyor-belt {
	padding-left:0px !important;
	}	
.main {
	box-shadow: 0px 10px 10px rgba(0,0,0,0.3);
	}	
.top_part {
	background:rgba(255,255,255,1) !important;
	}	
#footer1 {
	margin:0px !important;
	}		
</style>

	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>


<div class="circle-arc">

<div class="click-circle-info">
<img src="/images/click-circle-info.png">
</div>

<div class="take-me-back">
<img src="/images/take-me-back.jpg">
</div>

<div class="architectural-title">
<img src="/images/architectural-title.jpg">
</div>


<div class="circle c1">
			<div class="circle-cover">
			<div class="circle-first"></div>
			<div class="circle-middle">1</div>
		<div class="circle-last"></div>
		</div>
	</div>	
	
	<div class="circle c2">
			<div class="circle-cover">
			<div class="circle-first"></div>
			<div class="circle-middle">2</div>
		<div class="circle-last"></div>
		</div>
	</div>	
	
	<div class="circle c3">
			<div class="circle-cover">
			<div class="circle-first"></div>
			<div class="circle-middle">3</div>
		<div class="circle-last"></div>
		</div>
	</div>
	
	
	<div class="circle c4">
			<div class="circle-cover">
			<div class="circle-first"></div>
			<div class="circle-middle">4</div>
		<div class="circle-last"></div>
		</div>
	</div>
	
	
	
	<div class="circle c5">
			<div class="circle-cover">
			<div class="circle-first"></div>
			<div class="circle-middle">5</div>
		<div class="circle-last"></div>
		</div>
	</div>	
	
	<div class="circle c6">
			<div class="circle-cover">
			<div class="circle-first"></div>
			<div class="circle-middle">6</div>
		<div class="circle-last"></div>
		</div>
	</div>	
	
	<div class="circle c7">
			<div class="circle-cover">
			<div class="circle-first"></div>
			<div class="circle-middle">7</div>
		<div class="circle-last"></div>
		</div>
	</div>
	
	
	<div class="circle c8">
			<div class="circle-cover">
			<div class="circle-first"></div>
			<div class="circle-middle">8</div>
		<div class="circle-last"></div>
		</div>
	</div>
	
	
</div>



<div class="conveyor-belt-cover">
<div class="conveyor-layer" style="">
	<div class="conveyor-belt">
	
		<div class="intro">
		<div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep"></div></td>
		<td>
		<div class="box">
			<div class="box-cover">
			<div class="box-first"></div>
			<div class="box-middle">Introduction</div>
			<div class="box-last"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep"></div></td>
		</tr>
		</table>
		</div>
		<div class="c-paragraph">
		Undertaking a home extension is a large project and you should be completely comfortable with the company you choose to instruct for both the Design & Build Phase. We offer a two-stage process so you are able to commit to the Design Phase and keep your options open when it comes to assigning a contractor for your Build. Having completed over 900 projects all over London, we work hard to exceed client expectations to produce designs that are practical and in line with the assigned budget. We have explained our Architectural Design Process below to offer an idea of how it all works, and what to expect if you choose to instruct us for the Design Phase.
		</div>
		
		</div>
		
		
		<div id="c1-section">
		
			<div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep"></div></td>
		<td>
		<div class="box">
			<div class="box-cover">
			<div class="box-first"></div>
			<div class="box-middle">1. DESIGN CONSULTATION</div>
			<div class="box-last"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep"></div></td>
		</tr>
		</table>
		</div>
		<div class="c-paragraph">
		The initial design consultation allows us to  understand what you want to accomplish and your approximate budget for the build. Every single project is different and it's important we fully understand what you want to gain from your home extension from the very beginning. The design consultation also provides us with the opportunity to tell you more about how we work. The consultation is undertaken by a member of our Architectural Team so you'll get a knowledgeable, creative team member, passionate about design and architecture. Following your Design Consultation, we will send over your quote based on the discussion and our understanding of your requirements. This outlines both our Fixed Fee Design Service, and also a budget estimate from our the construction arm of Build Team.
		<br>
		<a href="http://www.buildteam.com/getting-started/book-site-visit.html" class="book-a-site-visit-link">Book A Design Consultation</a>
		</div>
		
		</div>
		
		
		
		
		
		
		<div id="c2-section">
		
		<div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep sepup"></div></td>
		<td>
		<div class="box m-adj">
			<div class="box-cover">
			<div class="box-first p-adj"></div>
			<div class="box-middle p-adj" style="line-height:inherit;">2. MEASURED SURVEY</div>
			<div class="box-last p-adj"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep sepup"></div></td>
		</tr>
		</table>
		</div>
		
		<div class="c-paragraph">
		If you choose to instruct us to proceed with the Design Phase, the first thing we do is book in your Measured Survey. This is a meeting at your property which is typically 2 hours (this does vary on project size). We will measure up the property and also recap on your requirements for the design so that we leave the meeting with all that is necessary to produce the existing plans and proposed design options.
		</div>
		<div class="clear"></div>
			<img src="/images/architectural-team-third.jpg" width="100%">
		</div>
		
		
		
		<div id="c3-section">
		<div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep sepup"></div></td>
		<td>
		<div class="box m-adj">
			<div class="box-cover">
			<div class="box-first p-adj"></div>
			<div class="box-middle p-adj" style="line-height:inherit;">3. DESIGN DEVELOPMENT</div>
			<div class="box-last p-adj"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep sepup"></div></td>
		</tr>
		</table>
		</div>
		
		<div class="c-paragraph">
		Following the Measured Survey, we will send over existing & proposed floorplans within 5 to 7 days. You are free to amend your plans or request new plans until you are totally happy with your final layout - we don't rest until it's perfect. We actually encourage our clients to take this time to experiment and consider design alternatives before proceeding with the planning application. Lots of clients choose to speak with their designer face to face, over the phone or on email - it's completely up to you.
		</div>
		
		
		

		
<div class="container">
  <div id="myCarousel" class="carousel slide" data-ride="carousel">

    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
    </ol>

 
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="/images/archi_design_phase/archi_1.jpg" alt="rose">
      </div>

      <div class="item">
        <img src="/images/archi_design_phase/archi_2.jpg" alt="lily">
      </div>
    
      <div class="item">
        <img src="/images/archi_design_phase/archi_3.jpg" alt="lotus">
      </div>

      <div class="item">
        <img src="/images/archi_design_phase/archi_4.jpg" alt="jasmine" >
      </div>
    </div>

    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
     
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      
    </a>
  </div>
</div>

		
		
		
		
		<!--<img src="/images/slider1.jpg" width="100%">-->
		
		</div>
		
		
		
		
		
		<div id="c4-section">

		<div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep"></div></td>
		<td>
		<div class="box">
			<div class="box-cover">
			<div class="box-first"></div>
			<div class="box-middle">4. PLANNING SET</div>
			<div class="box-last"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep"></div></td>
		</tr>
		</table>
		</div>
		<div class="c-paragraph">
		Once you are completely satisfied with your design, we will prepare your planning application for you. This includes your planning application or permitted development application and anything else we feel would support your application such as a Heritage Report or a Design & Access Statement. This also includes your drawing set (site plan, existing and proposed ground floor plan, roof plans, sectional details, side elevations etc.). Our team our will be able to explain exactly what we feel is necessary to submit with the planning application.
		</div>
		
		</div>
		
		
		
		
		<div id="c5-section">
		  
		<div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep sepup"></div></td>
		<td>
		<div class="box m-adj">
			<div class="box-cover">
			<div class="box-first p-adj"></div>
			<div class="box-middle p-adj" style="line-height:inherit;">5. PLANNING PERIOD</div>
			<div class="box-last p-adj"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep sepup"></div></td>
		</tr>
		</table>
		</div>
		<div class="c-paragraph">
		Planning generally takes 8 weeks. Once we submit your application, we will monitor progress with the local authority to ensure it is validated in a timely manner. Validation means that the council are registering the application as valid and confirming that they have what they need to make a determination. Once the application is validated, a Case Officer (CO) will be assigned. We then make contact with the CO to ensure that they have our details. Sometimes the CO will want to carry out a brief site visit at around week 6 or 7 of the planning period and in this case we will be in touch with you to book it in. They generally just want to have a quick look around and take some photos.
		</div>
		</div>
		
		
		<div id="c6-section">
		 
		 
		 <div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep"></div></td>
		<td>
		<div class="box">
			<div class="box-cover">
			<div class="box-first"></div>
			<div class="box-middle">6. PARTY WALL</div>
			<div class="box-last"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep"></div></td>
		</tr>
		</table>
		</div>
		<div class="c-paragraph">
		If you want to build soon, we would recommend getting started on Party Wall (PW) once you have submitted
to planning, so you can run the process alongside the 8 week planning window. Party Wall can be lengthy, especially if your neighbours are not supportive of the works you intend to carry out. It’s important to remember that no neighbour can stop you from building, they can only delay it during the PW negotiations. We have an in-house Party Wall Expert who can serve Notice & draw up the Schedules of Condition. We take a very sympathetic approach when it comes to serving to Notice in the hope of gaining a consent to the works which ultimately saves you time and money. We do this by offering lots of information and support if your neighbours have questions or concerns.
		</div>
		</div>
		
		
		
		<div id="c7-section">
		
		
		<div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep sepup"></div></td>
		<td>
		<div class="box m-adj">
			<div class="box-cover">
			<div class="box-first p-adj"></div>
			<div class="box-middle p-adj" style="line-height:inherit;">7. BUILDING REG PACKS</div>
			<div class="box-last p-adj"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep sepup"></div></td>
		</tr>
		</table>
		</div>
		<div class="c-paragraph">
		If planning permission is accepted, we will notify you and send over all of the documentation for your information and records. Once the application is accepted, we begin your Building Reg Pack. These are the
detailed drawings that Building Control and your assigned contractor will require during your Build. They are the detailed drawings that show all of the steel placement and specifications etc. If your application is rejected, we will look carefully as the reasons for rejection and work with you to find the best solution forward, be that to Appeal the decision or amend the drawings and resubmit.
		</div>
		</div>
		
		
		<div id="c8-section">
		
		 <div class="c-title">
		<table width="100%">
		<tr>
		<td width="40%"><div class="sep"></div></td>
		<td>
		<div class="box">
			<div class="box-cover">
			<div class="box-first"></div>
			<div class="box-middle">8. PRE-SITE ACTIVITIES</div>
			<div class="box-last"></div>
			</div>
		</div>
		</td>
		
		<td width="40%"><div class="sep"></div></td>
		</tr>
		</table>
		</div>
		<div class="c-paragraph">
		Once we have your detailed drawings, our construction arm will prepare a fixed price tender for the works. On acceptance of this quotation, we will raise our deposit invoice and book a start date for the works to commence on site. We will also arrange a Pre-Contract Meeting to introduce you to Project Manager and Foreman for the works, and run through any pre-start formalities such as the provision of a temporary kitchen, waste permits and any neighbourly matters pertaining to the Party Wall Award.
		</div>
		
		</div>
		
		
		<!--<div id="cblank"></div>-->
		
		</div>
	</div>
</div>


<div class="take-me-top">
<img src="/images/take-me-top.jpg">
</div>



<style>



a.book-a-site-visit-link {
    color: #124575;
    padding: 3px 0px;
    font-size: 15px;
    margin-top: 9px;
    display: inline-block;
    font-weight: bold;
}


.main
{
	max-width:980px !important;
}



.circle-arc {
    width: 100%;
    height: 450px;
    float: left;
    position: relative;
    background: url(/images/dots.png);
    background-position: top;
}


.circle{
	position:absolute;
}


.circle-cover
{
width:104px;
height:104px;
position:relative;
cursor:pointer;
}

.circle-cover a {
    height: 100px;
    width: 100px;
    float: left;
    border: 1px solid #000;
	background:#d7d7d7;
	position:relative;
	z-index:100;
}

.circle-first
{
width:100px;
height:100px;
border-radius:100px;
position:absolute;
border:2px solid #104678;
top:0px;
left:2px;
z-index:2;
}

.circle-last
{
width:100px;
height:100px;
border-radius:100px;
border:2px solid #104678;
position:absolute;
top:0px;
left:-2px;
z-index:1;
}
.circle-middle
{
width:100px;
height:100px;
border-radius:100px;
position:absolute;
top:2px;
left:2px;
color:#104678;
background:#fff;
//background:#f5641e;
z-index:0;
text-align:center;
font-size:28px;
line-height:100px;
color:#000;
font-family: times new roman;
}



.circle-cover:hover > .circle-middle
{
color:#f5641e;
	//background:#f5641e !important;	
}

.circle-middle.active{
	color:#f5641e;
	//background:#f5641e !important;	
}


.c1{ left:7px; bottom:25px;}
.c8{ right:7px;bottom:25px;}



.c2{ left:75px; bottom:158px;}
.c7{ right:75px; bottom:158px;}


.c3{ left:185px; bottom:260px;}
.c6{ right:185px; bottom:260px;}


.c4{ left:350px; bottom:328px;}
.c5{ right:350px; bottom:328px;}




.conveyor-belt-cover
{
  width:110%;
  overflow:hidden;
  height:600px;
  position:relative;
  margin-left:-5%;
}
.conveyor-belt
{
  width:100%;
  padding-left: 10px;
  overflow-y:scroll;
  height:500px;
  padding-right:10px;
  overflow-x: hidden;
}

.conveyor-belt img {
    margin-bottom: 30px;
    width: 92%;
    margin: 20px auto;
    display: table;
}

#c1-section {
    width: 100%;
	padding-bottom:30px;
    //height: 500px;
    //background: #ccc;
	   float:left;
}

#c2-section {
    width: 100%;
	padding-bottom:30px;
    //height: 500px;
   // background: red;
    float:left;
}

#c3-section {
    width: 100%;
	padding-bottom:30px;
    //height: 500px;
    //background: blue;
	 float:left;
}

#c4-section {
    width: 100%;
	padding-bottom:30px;
    //height: 500px;
    float:left;
}

#c5-section {
    width: 100%;
	padding-bottom:30px;
    //height: 500px;
    //background: yellow;
	 float:left;
}

#c6-section {
    width: 100%;
	padding-bottom:30px;
    //height: 500px;
    //background: cyan;
	 float:left;
}
#c7-section {
    width: 100%;
	padding-bottom:30px;
    //height: 500px;
    //background: green;
	 float:left;
}
#c8-section {
    width: 100%;
	padding-bottom:30px;
	//height: 500px;
    //background: orange;
	 float:left;
}

#cblank
{
	height:1000px;
	width: 100%;
	float:left;
	border-top:2.5px solid rgb(148,176,200);
	margin-top:50px;
}

.zopim
{
	display:none !important;
}

.click-circle-info {
    position: absolute;
    left: -230px;
    bottom: 100px;
    cursor: pointer;
    width: 250px;
}
.click-circle-info img {
	width:100%;
	}
.take-me-back
{
	position:absolute;
    right: -140px;
    bottom: 280px;
	cursor:pointer;
	display:none;
	
}

.take-me-top
{
cursor:pointer;
margin:auto;
display: table;
}

.architectural-title {
    top: 50%;
    left: 50%;
    position: absolute;
    margin-left: -142px;
    margin-top: -50px;
}

.c-title {
    display: block;
    float: left;
    width: 100%;
    height: 90px;
	margin-bottom:00px;
	margin-top: 10px;
}

.c-paragraph {
    display: block;
    float: left;
    width: 100%;
    font-size: 17px;
    font-weight: normal;
    text-align: justify;
    line-height: 25px;
    color: rgb(116,120,127);
}


.box {
    position: relative;
    margin-left: -23px;
}
.box-cover
{
//width:104px;
//height:104px;
//position:relative;
//cursor:pointer;
}



.box-first {
    width: 240px;
    height: 55px;
    position: absolute;
    border: 2px solid rgb(148,176,200);
    top: 4px;
    left: 2px;
    z-index: 20;
}

.box-last {
    width: 240px;
    height: 55px;
    border: 2px solid rgb(148,176,200);
    position: absolute;
    top: 0px;
    left: -2px;
    z-index: 20;
}

.box-middle {
    width: 242px;
    height: 51px;
    position: absolute;
    top: 6px;
    left: -2px;
    z-index: 5;
    text-align: center;
    font-size: 18px;
    line-height: 47px;
    color: rgb(116,120,127);
    font-family: times new roman;
    background: #fff;
    text-transform: uppercase;
}

.sep {
    width: 99.5%;
    height: 2.5px;
    background: rgb(148,176,200);
    margin-top: 28px;
    position: relative;
    z-index: 0;
}

.intro{ float:left; display:block;padding-bottom:30px; }

.p-adj {
    padding: 5px 5px;
    line-height: 36px !important;
    font-size: 18px;
    margin-top: -2px !important;
    box-sizing: border-box;
}

.m-adj {
    margin-left: -22px;
}

.item img
{
	width:100% !important;
}

.carousel-control.left {
    background-image:url(/images/left-arrow.jpg) !important;
	background-color:transparent;
	background-repeat:no-repeat;
	background-position:center;
	left:-33px;
}

.carousel-control.right{
    background-image:url(/images/right-arrow.jpg) !important;
	background-color:transparent;
	background-repeat:no-repeat;
	background-position:center;
	right:-33px;
}

.carousel-control 
{
	width:34px !important
}



.c-paragraph,
.c-title {
    width: 96%;
    padding-left: 4%;
}

.container {
    width: 92%;
    max-width: 100%;
    padding: 0px;
	float:left;
    margin-left: 2% !important;
}


.carousel-indicators {
    bottom: -16px !important;
}
.carousel-indicators li {
    background-color: rgb(163, 184, 201) !important;
    border: 1px solid #114576 !important;

}

.carousel-indicators .active {
    background-color: #fff !important; 
}
</style>

<script>
jQuery(document).ready(function($){
$('.conveyor-belt').css("overflow-y","scroll");
var ch = $('.intro').outerHeight() + $('#c1-section').outerHeight() + $('#c2-section').outerHeight() + $('#c3-section').outerHeight() +$('#c4-section').outerHeight() + $('#c5-section').outerHeight() + $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() +  $('#cblank').outerHeight();

$('.conveyor-belt-cover').css("height", ch+"px");
$('.conveyor-belt').css("height", ch+"px");


$('.conveyor-belt').css("overflow-y","hidden");

var intro = $(".intro").offset().top;
var box1 = $("#c1-section").offset().top;
var box2 = $("#c2-section").offset().top;
var box3 = $("#c3-section").offset().top;
var box4 = $("#c4-section").offset().top;
var box5 = $("#c5-section").offset().top;
var box6 = $("#c6-section").offset().top;
var box7 = $("#c7-section").offset().top;
var box8 = $("#c8-section").offset().top;


$('.take-me-top').click(function(e) {

var ch = $('.intro').outerHeight() + $('#c1-section').outerHeight() + $('#c2-section').outerHeight() + $('#c3-section').outerHeight() +$('#c4-section').outerHeight() + $('#c5-section').outerHeight() + $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() + 40;

$('.conveyor-belt-cover').css("height", ch+"px");
$('.conveyor-belt').css("height", ch+"px");
			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			$(".circle-middle").removeClass("active");	  
			 
			$("html, body").animate({
            scrollTop: 0
			}, 600);
			
			//$('html,body').stop().animate({
                 // scrollTop: intro},
                  //'slow');
			$('.click-circle-info').fadeIn();  
			$('.take-me-back').fadeOut();  
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
			
});


$('.take-me-back').click(function(e) {

var ch = $('.intro').outerHeight() + $('#c1-section').outerHeight() + $('#c2-section').outerHeight() + $('#c3-section').outerHeight() +$('#c4-section').outerHeight() + $('#c5-section').outerHeight() + $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() + 40;

$('.conveyor-belt-cover').css("height", ch+"px");
$('.conveyor-belt').css("height", ch+"px");
			

			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			
            
			$('.conveyor-belt').stop().animate({
                  scrollTop: intro},
                  'slow');
			$('.click-circle-info').fadeIn();  
			$('.take-me-back').fadeOut();  
			
			
			
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
			
			
});


$('.c1').click(function(e) {
			
			
			
var coneh = $('#c1-section').outerHeight() + $('#c2-section').outerHeight() + $('#c3-section').outerHeight() +$('#c4-section').outerHeight() + $('#c5-section').outerHeight() + $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() + 10;

			$('.conveyor-belt-cover').css("height", coneh+"px");
			$('.conveyor-belt').css("height", coneh+"px");			
			
			
			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			$(".c1 .circle-middle").addClass("active");
            
			$('.conveyor-belt').stop().animate({
                  scrollTop: box1},
                  'slow');
			$('.click-circle-info').fadeOut();  
			$('.take-me-back').fadeIn();  
			
			
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
});

$('.c2').click(function(e) {
			
			

			var ctwoh = $('#c2-section').outerHeight() + $('#c3-section').outerHeight() +$('#c4-section').outerHeight() + $('#c5-section').outerHeight() + $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() + 10;

			$('.conveyor-belt-cover').css("height", ctwoh+"px");
			$('.conveyor-belt').css("height", ctwoh+"px");		


			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			$(".c2 .circle-middle").addClass("active");
              
			$('.conveyor-belt').stop().animate({
                  scrollTop: box2},
                  'slow');
			$('.click-circle-info').fadeOut();
			$('.take-me-back').fadeIn();
			
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
});

$('.c3').click(function(e) {
			
			
			var cthreeh = $('#c3-section').outerHeight() +$('#c4-section').outerHeight() + $('#c5-section').outerHeight() + $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() +10;

			$('.conveyor-belt-cover').css("height", cthreeh+"px");
			$('.conveyor-belt').css("height", cthreeh+"px");	
			
			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			$(".c3 .circle-middle").addClass("active");
            
			$('.conveyor-belt').stop().animate({
                  scrollTop: box3},
                  'slow');
			$('.click-circle-info').fadeOut();
			$('.take-me-back').fadeIn();
			
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
});

$('.c4').click(function(e) {
	
			
			var cfourh = $('#c4-section').outerHeight() + $('#c5-section').outerHeight() + $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() + 10;

			$('.conveyor-belt-cover').css("height", cfourh+"px");
			$('.conveyor-belt').css("height", cfourh+"px");
			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			$(".c4 .circle-middle").addClass("active");
			
              $('.conveyor-belt').stop().animate({
                  scrollTop: box4},
                  'slow');
			  $('.click-circle-info').fadeOut();
			  $('.take-me-back').fadeIn();
			  
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
});

$('.c5').click(function(e) {
		
			
			var cfiveh = $('#c5-section').outerHeight() + $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() + 10;

			$('.conveyor-belt-cover').css("height", cfiveh+"px");
			$('.conveyor-belt').css("height", cfiveh+"px");
			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			$(".c5 .circle-middle").addClass("active");
			
              $('.conveyor-belt').stop().animate({
                  scrollTop: box5},
                  'slow');
			  $('.click-circle-info').fadeOut();
			  $('.take-me-back').fadeIn();
			 
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
});

$('.c6').click(function(e) {

			
			var csixh =  $('#c6-section').outerHeight() + $('#c7-section').outerHeight() + $('#c8-section').outerHeight() + 10;

			$('.conveyor-belt-cover').css("height", csixh+"px");
			$('.conveyor-belt').css("height", csixh+"px");
			
			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			$(".c6 .circle-middle").addClass("active");
			
            $('.conveyor-belt').stop().animate({
                  scrollTop: box6},
                  'slow');
			$('.click-circle-info').fadeOut();
			$('.take-me-back').fadeIn();
			
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
});

$('.c7').click(function(e) {

			
			var cseventhh =  $('#c7-section').outerHeight() + $('#c8-section').outerHeight() + 10;

			$('.conveyor-belt-cover').css("height", cseventhh+"px");
			$('.conveyor-belt').css("height", cseventhh+"px");
			
			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			
			$(".c7 .circle-middle").addClass("active");
             
			$('.conveyor-belt').stop().animate({
                  scrollTop: box7},
                  'slow');
			
			$('.click-circle-info').fadeOut();
			$('.take-me-back').fadeIn();
			
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden");
});

$('.c8').click(function(e) {
	
			
			var ceighth =  $('#c8-section').outerHeight() + 10;

			$('.conveyor-belt-cover').css("height", ceighth+"px");
			$('.conveyor-belt').css("height", ceighth+"px");
			
			//belt start
			$('.conveyor-belt').css("overflow-y","scroll");
			
			$(".circle-middle").removeClass("active");
			$(".c8 .circle-middle").addClass("active");
     
			$('.conveyor-belt').stop().animate({
                  scrollTop: box8},
                  'slow');
			$('.click-circle-info').fadeOut();
			$('.take-me-back').fadeIn();
	
			//belt stop
			$('.conveyor-belt').css("overflow-y","hidden"); 
});
	/*
    $("a").on('click', function(event) {
		event.preventDefault();
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.substr(1) +']');
        if (target.length) {
			
			
			$('.conveyor-belt').animate({
                  scrollTop: (target.offset().top +
                              target.offsetParent().scrollTop())},
                  'slow');
			
			return false;
			
			$('.location-wrapper').stop().animate({
                  scrollTop: box6},
                  'slow');
				  
				
			
            $('html,body').stop().animate({
              scrollTop: target.offset().top - 587
            }, 1000);
            return false;
			
		}
    });
*/
});
</script>



<style>

@media only screen (max-width:1366px) and (min-width:1350px){
.click-circle-info {
    position: absolute;
    left: -194px;
    bottom: 100px;
    cursor: pointer;
    width: 210px;
}
.click-circle-info img
{
	width:100%;
}

}


@media (max-width:1350px) {
	.click-circle-info {
		position: absolute;
		left: -10px;
		bottom: 325px;
		cursor: pointer;
		width: 210px;
		}
	.take-me-back {
		right:-50px;
		}
		
	}

@media only screen and (max-width:480px){
	
	
	
.conveyor-belt-cover
{
top:-40px;	
}
	
	
	
.circle-arc {
    background-size: 100%;
	height: 200px;
	background-repeat:no-repeat;
}
	
.architectural-title {
    top: 50%;
    left: 50%;
    position: absolute;
    margin-left: -50px;
    margin-top: -30px;
    width: 100px;
}	
	
	
	.circle-first {
    width: 40px;
    height: 40px;
    border-radius: 100px;
    position: absolute;
    border: 1px solid #104678;
    top: 0px;
    left: 2px;
    z-index: 2;
}
	
	.circle-middle {
    width: 36px;
    height: 36px;
    border-radius: 100px;
    position: absolute;
    top: 2px;
    left: 2px;
    color: #104678;
    background: #fff;
    z-index: 0;
    text-align: center;
    font-size: 19px;
    line-height: 35px;
    color: #000;
    font-family: times new roman;
}
	
	.circle-last {
    width: 40px;
    height: 40px;
    border-radius: 100px;
    border: 1px solid #104678;
    position: absolute;
    top: 0px;
    left: -2px;
    z-index: 1;
}
	
	
	
	.circle-cover {
    width: 44px;
    height: 44px;
    position: relative;
    cursor: pointer;
}
	
	
.c1 { left: 3px; bottom: 40px; }
.c8 { right: 0px; bottom: 40px;}
	
	
.c2 { left: 23px;bottom: 88px; }
.c7 { right: 20px;bottom: 85px; }	
	
	
.c3 { left: 62px;bottom: 130px; }
.c6 { right: 58px;bottom: 130px; }	
	
	
.c4 { left: 122px;bottom: 156px; }
.c5 { right: 118px;bottom: 156px; }	
	
	
	
.click-circle-info {
    left: -7px;
    bottom: 173px;
    width: 136px;
}
.take-me-back {
    position: absolute;
    right: 10px;
    bottom: 190px;
    width: 70px;
}

.c-paragraph, .c-title {
    width: 97%;
    padding-left: 3%;
}

.box-first {
    width: 112px;
    height: 45px;
}

.box-middle {
    width: 115px;
    height: 46px;
    font-size: 11px;
    line-height: 39px;
}


.box-last {
    width: 112px;
    height: 45px;
}
	
.box {
    position: relative;
    margin-left: -20px;
}


.c-title
{
height:50px;
margin-bottom: 15px;	
}
	
.p-adj {
    padding: 5px 5px;
    line-height: 15px !important;
    font-size: 12px;
    margin-top: -4px !important;
    box-sizing: border-box;
}
	
	.container {
    width: 76%;
    max-width: 100%;
    padding: 0px;
    float: left;
    margin-left: 12% !important;
}

.take-me-top
{
display: none;
}


.c-paragraph {
    font-size: 10px;
    line-height: 15px;
}

.sep {
    height: 0px;
    border-top: 2px solid #94b0c8;
    margin-top: 24px;
}

.sepup
{
	margin-top: 20px;
}

.carousel-indicators
{
	
	display:none;
}


.topwhite {
    padding: 15px 0px !important;
}


}








@media only screen and (max-width:322px){
	
	
	
.circle-cover {
    width: 40px;
    height: 40px;
}

.circle-first {
    width: 35px;
    height: 35px;
    top: 0px;
    left: 2px;
}
	
.circle-middle {
    width: 31px;
    height: 31px;
    top: 2px;
    left: 2px;
    font-size: 18px;
    line-height: 32px;
}	
	
.circle-last {
    width: 35px;
    height: 35px;
    top: 0px;
    left: -1px;
}

.c1 {
    left: 2px;
    bottom: 57px;
}

.c2 {
    left: 18px;
    bottom: 100px;
}

.c3 {
    left: 54px;
    bottom: 137px;
}

.c4 {
    left: 108px;
    bottom: 160px;
}

.c5 {
    right: 105px;
    bottom: 160px;
}

.c6 {
    right: 51px;
    bottom: 135px;
}

.c7 {
    right: 16px;
    bottom: 97px;
}

.c8 {
    right: -2px;
    bottom: 54px;
}


.take-me-top
{
display: none;
}

.box {
    position: relative;
    margin-left: -26px;
}
	
}




</style>


<?php require_once('./inc/footer.inc.php'); ?>