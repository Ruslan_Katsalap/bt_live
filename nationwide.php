<?php 
require_once('./inc/header.inc.php');
?>

<style type="text/css">
	.darkblue-box, .lightblue-box {
    font-size:20px;
		padding: 5px;
    padding-top:7px;
		margin-right: 5px;
		width: 98px;
	}

	.check {
		margin-top: 20px;
	}

	.check li {
		padding-bottom: 30px !important;
	}
	.col_half {
		font-size:16px;
		}
	
	
	</style>



<div class="full">
	<h1>Nationwide design service for house extensions in UK</h1>

	<div class="col_half">
		<p style="text-align:justify">No matter where you live in the UK, we can now help you design and build a Side Return Extension with our packaged Nationwide Design Service. We have developed this service in response to a growing number of homeowners who are seeking a Fixed Fee design package for house building and side extensions. Our construction company in London can also help with the tender process, selection of local contractor and on-site project management.</p>

		<div style="font-weight: bold;">
			<p>Our Nationwide Design Service includes:</p>
			<ul class="check">
				<li>Measured Survey for architectural design</li>
				<li>Production and Submission of Architectural Design Options</li>
				<li>Preparation and submission of Full Planning Application (or Certificate of Lawful Development where applicable)</li>
				<li>Acting as Client Agent during the currency of Planning Application
Full Structural Engineering and Beam Calculations for permitted development extensions</li>
				<li>House Building Regulation drawings</li>
				<li>Detailed Schedule of side return extension works and Tender Pack for line pricing</li>
				<li>Shortlist of 3 local, vetted contractors to invite to tender for the house designs in UK</li>
			</ul>

			<p class="fee-orange">Fixed Fee of just £3,995 + VAT</p>

			<p class="links"><a href="/contact.html?type=nationwide">enquire now</a> <a href="/what-we-do/design-planning-database.html">view example plans</a><p>

			<p>Optional Services<br />(Fixed Fee Quotations Available on request)</p>

			<ul class="check">
				<li>3D visualisation of proposed scheme</li>
				<li>Party Wall Notice &amp; Full Schedule of Condition</li>
				<li>Bill of quantities by RICS approved surveyor</li>
			</ul>
		</div>
	</div>
	<div class="col_half col_last">
		<img src="/images/nationwide-design2.png" alt="Nationwide Design Drawing" style="width:100%;max-width:467px">
		<p style="font-weight: bold;">As we understand speed of the essence, we aim to undertake the measured
survey within three working days of instruction and send you initial designs with
one week of the survey.</p>
	<p>&nbsp;</p>

	

	<div class="boxes">
			<div class="darkblue-box">find out<br><span class="orange">more</span></div>
			<div class="lightblue-box"><a href="/what-we-do/guarantees-and-insurance-page.html">10 year<br><span class="orange">guarantee</span></a></div>
			<div class="lightblue-box"><a href="/about-us/guarantees-and-insurance-page.html"><span class="orange">completion</span><br>pack</a></div>
			<div class="lightblue-box" style="margin-right: 0;"><a href="/build-your-price-start.html">build your<br><span class="orange">price</span></a></div>
		</div>
	</div>

 </div>

 <?php
 require_once('./inc/footer.inc.php');
 ?>