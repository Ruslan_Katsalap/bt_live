<?php

global $a_settings;

require_once('./inc/settings.php');

//define('WISE_RELURL', '');
//$https = '';

require_once('./inc/header.inc.php');
?>

  <div class="full initial_visit-page">
		
    
      <div id="bc"><a href="/">Home</a> &rsaquo; <a href="/getting-started/book-site-visit.html">Book a Site Visit</a> &rsaquo; <b>Initial Visit</b>
      </div>
      
      <h1>Initial Visit</h1>
      
    <div class="row three-columns visits">
      <div class="col-sm-3 col-sm-offset-1">
        <div class="visit-frame">
          <h3>Standard<span><?php if ($a_settings['a_visit_types']['standard']['price']) : ?>&pound;<?php echo $a_settings['a_visit_types']['standard']['price'] ?><sup>*</sup><?php else: ?>FREE<?php endif; ?></span></h3>
          <div class="visit-wrap">
            <ul>
              <li>Laser-Measured survey of proposed floor area</li>
              <li>Detailed Quotation</li>
            </ul>
          </div>
          <a href="/book-visit.html?type=standard" class="book-now"><img src="/images/btn-book_now.png" alt="Book Now" /></a>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="visit-frame">
          <h3 class="premium">Premium<span><?php if ($a_settings['a_visit_types']['premium']['price']) : ?>&pound;<?php echo $a_settings['a_visit_types']['premium']['price'] ?><sup>*</sup><?php else: ?>FREE<?php endif; ?></span></h3>
          <div class="visit-wrap">
            <p>Standard Plus:</p>
            <ul>
              <li>Early morning and late evening appointments</li>
              <li>Cost advice and optimising your budget</li>
              <li>Specialist Planning and Party Wall advice</li>
              <li>£125 + VAT fully refundable within 30 days against subsequent design instruction</li>
            </ul>
          </div>
          <a href="<?php echo $https ?>/<?php echo WISE_RELURL ?>book-visit.html?type=premium" class="book-now"><img src="/images/btn-book_now.png" alt="Book Now" /></a>
        </div>
      </div>
      <div class="col-sm-3">
        <div class="visit-frame">
          <h3 class="d3premium">3D Premium<span><?php if ($a_settings['a_visit_types']['3dconcept']['price']) : ?>&pound;<?php echo $a_settings['a_visit_types']['3dconcept']['price'] ?><sup>*</sup><?php else: ?>FREE<?php endif; ?></span></h3>
          <div class="visit-wrap">
            <p>Premium Plus:</p>
            <ul>
              <li>Creative Designs and Layout Options</li>
              <li>3D Visuals of how the exterior will look</li>
            </ul>
          </div>
          <a href="<?php echo $https ?>/<?php echo WISE_RELURL ?>book-visit.html?type=3dconcept" class="book-now"><img src="/images/btn-book_now.png" alt="Book Now" /></a>
        </div>
      </div>
      <div class="col-sm-10 col-sm-offset-1">
        * excluding VAT at prevailing rate
      </div>
    </div>
      
  </div>

<?php
require_once('./inc/footer.inc.php');
?>
    