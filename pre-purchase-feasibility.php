<?php

require_once('./inc/header.inc.php');
?>
<style>
/* Css for gallery */
.prebutton img {
    max-width: 120px;
}
.prebutton {
    position: absolute;
    bottom: 10px;
    right: 10px;
}
.img-block {
    
    position: relative;
}
.col_one_third a {
    position: relative;
    width: 100%;
    display: block;
}
.col_one_third img {
    border: 0;
    /* max-width: 450px !important; */
    width: 100% !important;
	max-width: inherit !important;
	min-height: 200px;
}
.col_one_third {
	margin-right: 2.7%;
}
</style>
<style>
/* ---- button ---- */

.button-group  .button {
  display: inline-block;
  padding: 10px 18px;
  margin-bottom: 10px;
  background: #EEE;
  border: none;
  border-radius: 7px;
  background-image: linear-gradient( to bottom, hsla(0, 0%, 0%, 0), hsla(0, 0%, 0%, 0.2) );
  color: #222;
  font-family: sans-serif;
  font-size: 16px;
  text-shadow: 0 1px white;
  cursor: pointer;
}

.button-group  .button:hover {
  background-color: #8CF;
  text-shadow: 0 1px hsla(0, 0%, 100%, 0.5);
  color: #222;
}

.button-group  .button:active,
.button-group  .button.is-checked {
  background-color: #28F;
}

.button-group  .button.is-checked {
  color: white;
  text-shadow: 0 -1px hsla(0, 0%, 0%, 0.8);
}

.button-group  .button:active {
  box-shadow: inset 0 1px 10px hsla(0, 0%, 0%, 0.8);
}

/* ---- button-group ---- */

.button-group:after {
  content: '';
  display: block;
  clear: both;
}
.clear {
	clear: both;
}
.button-group .button {
  float: left;
  border-radius: 0;
  margin-left: 0;
  margin-right: 1px;
}

.button-group .button:first-child { border-radius: 0.5em 0 0 0.5em; }
.button-group .button:last-child { border-radius: 0 0.5em 0.5em 0; }
.grid-gallery {
	position: relative;
}
a.hover-box {
    display: none;
    outline: none;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.77);
    visibility: hidden;
    opacity: 0;
}
.inner-hover-box {
	display: block;
    position: absolute;
    text-align: center;
    top: 50%;
    left: 50%;
    width: 100%;
    transform: translateX(-50%) translateY(-50%);
}
.inner-hover-box i {
    display: inline-block;
    font-style: normal;
}
.inner-hover-box i {
font-size:24px;
font-weight: 500;
}
.element-item-gallery:hover .hover-box {
    visibility: visible;
    opacity: 1;
    display: block;
}
.filter-btn {
	border: none;
    color: #fff;
    padding: 14px 0px;
    background: #0f4778;
    font-size: 16px;
    /* box-shadow: none; */
	    border-radius: 5px;
		display: block;
    width: 100%;
}
.noresult {
	display:none;
	padding:50px 0px;
	text-align:center;
}
.noresult p {
	color:#f58630;
	font-size:22px;
	font-weight: 500;
}
.noresult p a{
	color:#f58630;
	text-decoration:underline;
}
@media only screen and (max-width: 768px){
	.col_one_fourth  {
		min-height: 20px !important;
	}
	.nice-select.wide {
		width: 100% !important;
	}
	.col_one_fourth:nth-child(2n) {
		margin: 0 auto;
	}
}

</style>
<style>
.main p, .main-bottom li {
	font-size: 18px;
	color: #9A9A9A;
	line-height: 20px;
}
.main-bottom li {
	line-height: 26px;
}
.main-bottom ul {
	list-style: none;
}
.main-bottom li::before{
	content:"•";
	color: rgb(15,71,120);
	display:inline-block; width:1em; margin-left:0px;
}
hr{
	
	height:1px;
	display: block;
	border:0px;
	border-top:2px solid rgba(154,154,154,0.5);
	margin: 1em 0;
	padding:0
}
.list{
	position:relative;
	display: inline-block;
	width: 485px;
	vertical-align: top;
	line-height:22px;
}

.list ul {
    margin-top: 0;
    padding-top: 0;
}
.list ul.pleft {
	padding-left: 25%;
}
.Testimonial{
	 margin-bottom: 0;
	padding-bottom: 0;
}
.Testimonialtitle{
	margin-top: 0;
	padding-top: 0;
	
}
.main-bottom {
	padding: 0 !important;
}
.main-bottom .col_two_fifth.col_last {
	display: none;
}
h1.p-heading {
	margin-bottom: 5px !important;
	padding-bottom: 0 !important;
}
h2 {
	font-size: 24px !important;
    color: #003c70;
    text-transform: capitalize !important;
    font-weight: bold;
    font-family: calibri !important;
}
h2.p-heading { 
	margin-bottom: 20px !important;
	padding-bottom: 0 !important;
}
.main-bottom h2 {
	font-size: 18px;
	color: #003c70;
	margin-top: 0;
	margin-bottom: 5px !important;
}
.img-block {
	margin: 20px 0;
}
.img-block img {
	width: 100%;
}
.list-space li {
	height: 0;
    overflow: hidden;
   	animation: slide 0.3s ease 0.3s forwards;
}
.list-space li {
    list-style: none;
    font-size: 17px;
    font-weight: normal !important;
    padding: 0px !important;
    margin: 0px !important;
    margin-bottom: 10px !important;
    color: #0f4778 !important;
    cursor: pointer;
    background-image: url(http://www.buildteam.com/images_new_design/faq-arrow-img.jpg);
    background-position: 0px -54px;
    background-repeat: no-repeat;
    padding-left: 30px !important;
	text-align:justify;
	    width: 93%;
	  }

.expand
{
	background-position: 0px 1px !important;
}
	  
.list-space span {
    font-size: 15px important;
    font-weight: normal;
    line-height: 24px;
    margin-bottom: 18px;
    color: #7b7b7b;
    margin-top: -10px;
    width: 97%;
    font-size: 12pt !important;
    font-family: roboto !important;
    text-indent: 0px !important;
    padding-left: 30px;
}

.list-space span p{
text-align:justify;
padding-top:5px;
font-weight: normal;
font-family: calibri;
}

.list-space span p span{
	margin-left:0px !important;
}
@keyframes slide {
  from { height: 0;}
  to {height: 20px;}
}	
@media (min-width:480px) and (max-width:1024px){
	body{
	  margin-left:100px;
	  margin-right:100px;
	  margin-top:120px;
	}

	.list{
		width: 100%;
	}
	.list ul.pleft {
		padding-left: 0;
	}
}

@media only screen and (max-width: 480px)
{
	p, li {
		font-size: 14px !important;
	}
	.list{
		width: 100%;
	}
	.list ul.pleft {
		padding-left: 0;
	}
.list-space li
{
margin-left:10px !important;
box-sizing: border-box;
line-height: 20px;
text-align: left;
}
.list-space span {
    margin-left: 50px;
	box-sizing: border-box;
	width: 82%;
	line-height: 20px;
}
.list-space span p {
    text-align: left;
    padding-top: 5px;
}
}
@media (max-width:640px) {
	.prebutton img {
    max-width: 50px;
}
	}
</style>
<link rel="stylesheet" type="text/css" href="/css/nice-select.css" />
<div class="full">
	<h1 class="p-heading">Pre Purchase feasibility</h1>
	<p align="justify">We have designed this service for prospective homeowners, who are looking to purchase a property but want to understand more about the potential to extend. A member of our Architectural Team will visit the property with you to get a feel for the existing property and understand more about what you want to achieve. During the site visit, we will offer advice on planning feasibility, design options and we can also address any questions you have on the Design &amp; Build process (eg. timelines). We will take some measurements and additional details which will enable us to offer you with quotes for both the Design &amp; Build Phase.</p>
	<div class="img-block"><img src="/img/pre-purchase.jpg" alt="A picture of a kitchen extension"/><div class="prebutton">
	<a href="https://www.buildteam.com/book-visit.html?type=prepurchase"><img src="../images/pre_button.png" /></a></div></div>
	<hr/>
	
	<div class="list main-bottom">
		<h2 class="p-heading">Our Pre-Purchase Feasibility Service includes:</h2>
		<ul id="accordion">
			<li>Site visit at the property</li>
			<li>Flexible appointments</li>
			<li>Survey of proposed floor area</li>
		</ul>
	</div>
	<div class="list  main-bottom">
		<ul class="pleft">
		  <li>Creative design ideas</li>
		  <li>Specialist planning advice</li>
		  <li>Cost advice on optimising your budget</li>
		  <li>Budget Estimate within 1 working day</li>
	   </ul>
	</div>
	<hr/>
	<br class="clear" />
	<div>
		<h2 class="p-heading">Frequently Asked Questions</h2>
		<ul class="check list-space">
			<li data-id="span-1">How do I get access for the visit?</li>
			<span id="span-1" style="display:none">
				<p>
					Depending on how far you are through the buying process, you will need to arrange access via the Estate Agent. 
					This is where our flexible appointments come in useful, as we’re arranging an appointment with you, the agent and us. 

				</p>
			</span>
			<li data-id="span-2">How long does the visit last?</li>
			<span id="span-2" style="display:none">
				<p>
					This completely depends on you – we would never leave you with unanswered questions. Depending on the size of the property and the scope of works, site visits can last anywhere between 20 minutes and an hour.  
				</p>
			</span>
			<li data-id="span-3">How long will it take to receive the quote for the Build?</li>
			<span id="span-3" style="display:none">
				<p>
					We understand that time is of the essence when it comes to buying a property, so we issue quotes within 1 working day of the site visit. 
				</p>
			</span>
			<li data-id="span-4">Can I start the Design Phase before I exchange or complete?</li>
			<span id="span-4" style="display:none">
				<p>
					Yes absolutely. Purchasing a property can be time consuming so it can be beneficial to get stuck into the Design Phase during this period. However, and for simplicity, we would advise that you are the legal owner before a planning application is submitted.
				</p>
			</span>
		</ul>
	</div>
</div>

<?php

require_once('./inc/footer.inc.php');

?>
<script src="/js/isotope.pkgd.min.js"></script>
<script src="/js/jquery.nice-select.js"></script>
<script>
// external js: isotope.pkgd.js

// init Isotope
var $grid = $('.grid-gallery').isotope({
	itemSelector: '.element-item-gallery',
	percentPosition: true
});
// filter functions
/*var filterFns = {
  // show if number is greater than 50
  numberGreaterThan50: function() {
    var number = $(this).find('.number').text();
    return parseInt( number, 10 ) > 50;
  },
  // show if name ends with -ium
  ium: function() {
    var name = $(this).find('.name').text();
    return name.match( /ium$/ );
  }
};
// bind filter button click
$('.filters-button-group').on( 'click', 'button', function() {
  var filterValue = $( this ).attr('data-filter');
  // use filterFn if matches value
  filterValue = filterFns[ filterValue ] || filterValue;
  $grid.isotope({ filter: filterValue });
});
// change is-checked class on buttons
$('.button-group').each( function( i, buttonGroup ) {
  var $buttonGroup = $( buttonGroup );
  $buttonGroup.on( 'click', 'button', function() {
    $buttonGroup.find('.is-checked').removeClass('is-checked');
    $( this ).addClass('is-checked');
  });
});*/

		

	$(document).ready(function() {
		$("#new-select-filter-new").click(function(){
			var gotvalue;
			var getfilter1 = $(".select-filter1").val() != '' ? '.'+$(".select-filter1").val() : '';
			var getfilter2 = $(".select-filter2").val() != '' ? '.'+$(".select-filter2").val() : '';
			var getfilter3 = $(".select-filter3").val() != '' ? '.'+$(".select-filter3").val() : '';
			var final = getfilter1 + getfilter2 + getfilter3;
			/*var filtergroup = $(".forfilterborder").attr("data-filter-group");
			filters[ filtergroup ] = final;	
			var filterValue = concatValues( filters );
			//alert(filterValue)
			//filters[ $filtervalue ] = $this.attr('data-filter');
			$container.isotope({ filter: filterValue, transitionDuration: "1s" });*/
						
				//var filterValue = $( this ).attr('data-filter');
			  // use filterFn if matches value
			  filterValue =  final;
			  $grid.isotope({ filter: filterValue });
			  if ( !$grid.data('isotope').filteredItems.length ) {
				  //$('#msg-box').show();
				 $(".noresult").show();
				}
			else {
				$(".noresult").hide();
			}
		});
		
		$('select.custom-filter').niceSelect();
    });
	$(document).on('click', "ul.list-space li", function () {
			var id = $(this).attr("data-id");
			jQuery("#"+id).slideToggle();
			jQuery(this).toggleClass("expand");
	});

</script>