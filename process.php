<?php 
require_once('./inc/header.inc.php');
?>



<div class="full">
	<h1>The 8 step process of building a side return extension</h1>

	<div class="col_three_fifth">

		<table border="0" class="process-table">
			<tr>
				<td class="process-first" id="td_quote"><span>quote</span></td>
				<td class="process-second" id="td_step_1"><span>1</span></td>
				<td class="process-third">
					<div>
						<h3>Free quotation</h3>
                        <p style="text-align:justify">You can obtain your own estimate using our online price calculator. Whether you are looking to build side extensions, do a mansard loft conversion or find out an estimated price for other side return ideas, you can easily plan your side return costs. We survey your property and offer you a free, no obligation estimate for the works. Alternatively, you may wish to book a <a href="/getting-started/planning-design-consultation.html" style="text-decoration:underline">Premium Site Visit</a>.</p>
					</div>
				</td>
			</tr>

			<tr>
				<td class="process-first" rowspan="3" id="td_process_design"><span style="margin-top: 240px;">design phase</span></td>
				<td class="process-second" id="td_step_2"><span>2</span></td>
				<td class="process-third">
					<div>
						<h3>PLANNING APPLICATION</h3>
                      <p style="text-align:justify">We can only work with permitted development extensions so we prepare and submit the planning application to the Local Authority. We act as your agent during the currency of the application and deal with any enquiries on your behalf so all modern house plans to be accepted.</p>
					</div>
				</td>
			</tr>

			<tr class="row_party_wall">
				<td class="process-first">&nbsp;</td>
				<td class="process-second" id="td_step_3"><span>3</span></td>
				<td class="process-third">
					<div>
						<h3>PARTY WALL SERVICE</h3>
                      <p style="text-align:justify">Should you appoint us to do so, we serve Notice to the adjoining owner and undertake a full Schedule of Condition. If necessary, we arrange for a full Party Wall Award to be drawn up between you and your adjoining owner. No side extensions are built without approval.</p>
					</div>
				</td>
			</tr>

			<tr class="row_struct_engineer">
				<td class="process-first">&nbsp;</td>
				<td class="process-second" id="td_step_4"><span>4</span></td>
				<td class="process-third">
					<div>
						<h3>STRUCTURAL ENGINEERING</h3>
                      <p style="text-align:justify">Our Structural Engineer designs your scheme and prepares the beam calculations for the side return extension. We produce the working drawings for submission to our Approved Inspector for the purposes of Building Control. Modern house plans require approval, but they are worth the wait.</p>
					</div>
				</td>
			</tr>

		</table>

		<table border="0" class="process-table" style="margin-top: 30px;">

			<tr>
				<td class="process-first" rowspan="3" id="td_process_build"><span style="margin-top: 240px;">build phase</span></td>
				<td class="process-second" id="td_step_5"><span>5</span></td>
				<td class="process-third">
					<div>
						<h3>DEMOLITION AND STEELWORK</h3>
                      <p style="text-align:justify">We strip out the existing kitchen and erect a temporary kitchen if you plan to live at the property during the works. We build the new party wall, pour the foundations and install the steelwork for the side return kitchen extension.</p>
					</div>
				</td>
			</tr>

			<tr class="row_flat_out">
				<td class="process-first">&nbsp;</td>
				<td class="process-second" id="td_step_6"><span>6</span></td>
				<td class="process-third">
					<div>
						<h3>FIT OUT, PLASTERING &amp; HEATING</h3>
						<ul>
							<li>We construct the roof and install the doors and roof lights with a modern interior design in our minds.</li>
							<li>We undertake 1st fix plumbing and electrics.</li>
							<li>We install the underfloor heating, screed and plastering.</li>
						</ul>
					</div>
				</td>
			</tr>

			<tr class="row_kitchen">
				<td class="process-first">&nbsp;</td>
				<td class="process-second" id="td_step_7"><span>7</span></td>
				<td class="process-third">
					<div>
						<h3>KITCHEN INSTALLATION &amp; DECORATION<br>(OPTIONAL)</h3>
						<ul>
							<li>We install the kitchen, or accommodate the kitchen company you have employed to undertake the works.</li>
							<li>We lay the floor tiles or engineered wooden floor for the side return kitchen extension.</li>
							<li>We decorate and undertake 2nd fix electrical and plumbing works according to your kitchen extension ideas.</li>
						</ul>
					</div>
				</td>
			</tr>

			<tr class="row_handover">
				<td class="process-first" id="td_handover"><span>handover</span></td>
				<td class="process-second" id="td_step_8"><span>8</span></td>
				<td class="process-third">
					<div>
						<h3>CLIENT HANDOVER</h3>
						<ul>
							<li>We handover the build including a folder containing the Building Control completion certificate, Gas Safe &amp; NICEIC certificates and 10 year specialist guarantee for your side extension.</li>
							<li>We arrange for our professional photographer to visit and provide you with the full high resolution images after the shoot of the permitted development extensions.</li>
						</ul>
					</div>
				</td>
			</tr>

		</table>

		<style type="text/css">
		.darkblue-box, .lightblue-box {
			width: 107px;
			margin-right: 10px;
		}
		</style>

		<div class="boxes">
			<div class="darkblue-box">find out<br><span class="orange">more</span></div>
			<div class="lightblue-box"><a href="/what-we-do/guarantees-and-insurance-page.html">10 year<br><span class="orange">guarantee</span></a></div>
			<div class="lightblue-box"><a href="/about-us/guarantees-and-insurance-page.html"><span class="orange">completion</span><br>pack</a></div>
			<div class="lightblue-box" style="margin-right: 0;"><a href="/build-your-price-start.html">build your<br><span class="orange">price</span></a></div>
		</div>

	</div>

	<div class="col_two_fifth col_last" style="text-align:center">
    <!--div class="col_full">
      <img src="/images/process-1.png" style="width:100%;max-width:350px" />
		</div-->
		<div class="col_full">
      <img src="/images/process-2.jpg" style="width:100%;max-width:476px" />
		</div>
	</div>


 </div>

 <?php
 require_once('./inc/footer.inc.php');
 ?>