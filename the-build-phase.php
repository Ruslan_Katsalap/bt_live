<?php require_once('./inc/header.inc.php'); ?>

<?php //require_once('save_how_can_help_form.php'); ?>
<link rel="stylesheet" href="css/slick.css" type="text/css">
<link rel="stylesheet" type="text/css" href="/Flipbook/font-awesome.css">
<script src="/Flipbook/flipbook.min.js"></script>

<style>
	.titlehowwecan h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	border-bottom: 1px solid #003c70;
}	
.howwedesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #003c70;
    line-height: 24px;
    font-weight: normal;
}
.middleinner {
    max-width: 980px;
    margin: 0 auto;
	padding:30px 0px;
}
.innerrow .insiderow {
    float: left;
    width: 33%;
    text-align: center;
	padding:15px 0px;
}

.innerrow:after {
    content: "";
    clear: both;
    display: table;
}
.upgradetitle h2 {
    color: #0f4778;
    font-size: 18px;
	margin-top:15px;
}
.upgradedesc p {
    color: #0f4777;
    font-size: 16px;
}

.insiderow img {
    max-width: 60px;
}
.bottomshortcut {
	max-width:980px;
	margin:0 auto;
	}
.shortcutinline {
    float: left;
    width: 24.2%;
}
.shortcutinline:nth-child(3) {
    margin-right: 1%;
}
.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
    margin-left: 1%;
}
.shortcutdesc {
    background: rgba(255,255,255,0.7);
    padding: 10px;
    text-align: center;
    color: #0f4679;
    position: absolute;
    bottom: 0px;
    width: 100%;
    font-weight: bold;
    font-size: 18px;
}
.shortcutwarp {
    position: relative;
}
.shortcutwarp img {
    max-width: 100%;
}
.tenderprocess {
    background: #ecebeb;
    margin: 50px 0px;
}
.tenderpprocesstitle h1 {
    font-weight: normal;
    padding: 0px;
}
.noteinfo {
    text-align: right;
    position: relative;
    top: 30px;
}
.noteinfo p {
    background: #bdbdbd;
    display: inline-block;
    padding: 0;
    color: #547088;
    padding-right: 10px;
    font-size: 16px;
}
.noteinfo span {
    background: #9f9f9f;
    display: inline-block;
    width: 20px;
    text-align: center;
    padding: 5px 5px;
    color: #FFF;
    font-size: 20px;
    vertical-align: middle;
    margin-right: 10px;
}
.noteinfo.noteinfo2 {
    top: -20px;
    right: 3px;
	margin-bottom:30px;
}
.moreinfo {
	max-width:980px;
	margin:0 auto;
	}
.moreinfo h1 {
    font-size: 20px !important;
    border-bottom: 2px solid #0f4778;
}
.completionpack {
    background: #e2e2e2;
    padding-bottom: 30px;
    /* margin-bottom: 20px; */
    margin-top: 30px;
}
.completioninner {
    max-width: 980px;
    margin: 0 auto;
}
.completioninner h1 {
    text-align: center;
    padding-top: 30px;
    font-weight: normal;
}
.completioninner p {
    font-size: 20px;
    text-align: justify;
    color: #0f4778;
    padding-top: 0px;
}
.complitioninline {
    width: 50%;
    float: left;
}
.complitionrow:after {
	content:"";
	display:table;
	clear:both;
	}
.complitioninline h2 {
    font-size: 26px;
    text-align: center;
    color: #0f4778;
	font-weight:normal !important;
	margin-bottom:20px;
}
.complitioninline li {
    font-size: 22px;
    color: #0f4778;
    margin-bottom: 10px;
}
.complitioninline ul {
    padding-left: 20px;
}
.buildwithbuildsec {
    padding: 50px 0px;
}
.innerbuildwith {
    max-width: 980px;
    margin: 0 auto;
}
.leftbuildwith {
    float: left;
	width:50%;
}
.rightbuildwith {
    float: right;
	width:50%;
}
.buildsecdetails img {
    width: 100%;
}
.buildsecdetails {
	text-align:center;
	}
.buildsecdetails p {
    font-size: 22px;
    color: #afafaf;
    line-height: 25px;
    padding-top: 0px;
}
.buildsecdetails h2 {
    color: #124b7b;
    font-size: 24px;
    font-weight: bold;
    padding-bottom: 10px;
}

.rightbuildwith .buildsecdetails {
    padding: 65px 0px;
}
.leftbuildwith .buildsecdetails {

}
#video {
    display: none;
    height: 275px !important;
}
.buildsecdetails a {
    display: inline-block;
    margin-top: 20px;
    font-size: 20px;
    /* font-weight: normal; */
    width: 200px;
    border-radius: 10px;
    color: #f5641e;
    font-style: italic;
}	
.numbertext {
    margin-bottom: 50px;
}
.inlinenumber:first-child {
    width: 10%;
}
.inlinenumber {
    display: inline-block;
    width: 89%;
    vertical-align: middle;
}
.numbertitle h2 {
    font-size: 20px;
    color: #0e4776;
    margin-bottom: 5px;
}	
@media (max-width:640px) {
	.buildwithbuildsec {
    padding: 50px 20px;
}
.leftbuildwith {
    width: 100%;
}
.rightbuildwith {
    width: 100%;
}
.rightbuildwith .buildsecdetails {
    padding: 65px 0px;
    padding-bottom: 10px;
}
	}
@media (max-width:768px) {
	button.slick-prev.slick-arrow {
		left:0px;
		}
	button.slick-next.slick-arrow{
		right:0px;
		}
	.tenderslider:before {
		display:none;
		}	
	.inlineflipbook {
			margin-left: 0px;
			width: 100% !important;
			max-width: 100%;
		}	
	.fliboocwrap {
		background: #f2f5f8;
		height: 825px;
		widows: 100%;
		margin-top: 50px;
	}
	.inlineflipbook.flipmargin {
		margin-bottom: 30px;
	}	
.inlinenumber:first-child {
    display:none;
}
.inlinenumber {
	width:100%;
	}
	.completionpack {
    padding: 15px 20px;
}

			
	}	
</style>
<div class="howwecanhelp">
    	<div class="titlehowwecan">
        	<h2>The Build Phase</h2>
        </div>
        <div class="howwedesc">
        	<p>Our unique two-stage process means you maintain complete flexibility when it comes to assigning a contractor for your Build. If you choose to use our Build Service, you’ll benefit from our full turn-key service. Your assigned Architectural Designer & Project Manager will ensure that the transition between Design & Build is completely seamless, and they’ll keep in close communication throughout the Build.</p>
        </div>
    </div>
</div>
<div class="completionpack">
	<div class="completioninner">
    	<h1>The <strong>Build Process</strong></h1>
        <div class="numbertext">
        	<div class="inlinenumber">
            	<div class="numberimg">
                	<img src="images/the_build_phase/1.jpg" alt="">
                </div>
            </div>
        	<div class="inlinenumber">
            	<div class="numbertitle">
                	<h2>Site Meeting</h2>
                    <p>Upon instruction of the Build Phase, we will arrange a meeting with you at the property with your Project Manager and Site Foreman. This enables us to confirm all of the details and answer any remaining questions.</p>
                </div>
            </div>
        </div>
        
        <div class="numbertext">
        	<div class="inlinenumber">
            	<div class="numberimg">
                	<img src="images/the_build_phase/2.jpg" alt="">
                </div>
            </div>
        	<div class="inlinenumber">
            	<div class="numbertitle">
                	<h2>Preparation Activities</h2>
                    <p>Before the build starts, we will have to complete a checklist of activities. This might include assigning an Approved Inspector and suspending a parking bay for your skip.</p>
                </div>
            </div>
        </div>
        
        <div class="numbertext">
        	<div class="inlinenumber">
            	<div class="numberimg">
                	<img src="images/the_build_phase/3.jpg" alt="">
                </div>
            </div>
        	<div class="inlinenumber">
            	<div class="numbertitle">
                	<h2>Start Date</h2>
                    <p>At the commencement of works you can meet your Project Manager and the Foreman at the property. The contractor will lay protective layers, strip off, dig foundations and carry out excavation works.</p>
                </div>
            </div>
        </div>
        
        <div class="numbertext">
        	<div class="inlinenumber">
            	<div class="numberimg">
                	<img src="images/the_build_phase/4.jpg" alt="">
                </div>
            </div>
        	<div class="inlinenumber">
            	<div class="numbertitle">
                	<h2>On-site Construction</h2>
                    <p>An extension usually takes between 12 to 16 weeks. During this time, your Project Manager will give you regular updates about the works and you can be as involved as you would like to be.</p>
                </div>
            </div>
        </div>
        
        <div class="numbertext">
        	<div class="inlinenumber">
            	<div class="numberimg">
                	<img src="images/the_build_phase/5.jpg" alt="">
                </div>
            </div>
        	<div class="inlinenumber">
            	<div class="numbertitle">
                	<h2>Payment Certificates</h2>
                    <p>Your Project Manager will inspect the build at regular intervals to check compliance with the specification, address any queries from the contractor and inspect the quality of the works. We will take the burden away from you with regards to certifying payments and addressing quality issues.</p>
                </div>
            </div>
        </div>
        
         <div class="numbertext">
        	<div class="inlinenumber">
            	<div class="numberimg">
                	<img src="images/the_build_phase/6.jpg" alt="">
                </div>
            </div>
        	<div class="inlinenumber">
            	<div class="numbertitle">
                	<h2>Snagging</h2>
                    <p>We will arrange for you to give your Foreman a list of snagging items that need to be carried out to complete the build and after those are completed the site will be cleared of waste and tools.</p>
                </div>
            </div>
        </div>
        
         <div class="numbertext">
        	<div class="inlinenumber">
            	<div class="numberimg">
                	<img src="images/the_build_phase/7.jpg" alt="">
                </div>
            </div>
        	<div class="inlinenumber">
            	<div class="numbertitle">
                	<h2>Completion Pack</h2>
                    <p>The project is handed over together with a folder containing our 10 year structural guarantee for the works that we’ve carried out and other important documentation regarding your project.</p>
                </div>
            </div>
        </div>
    </div>
</div>
  
<div class="buildwithbuildsec">
	<div class="innerbuildwith">
    	<div class="leftbuildwith">
        	<div class="buildsecdetails2">
            	<a href="javascript:void(0)"><img src="images/build_phase/video_layer.jpg" alt=""></a>
                <video id="video" width="320" height="240" controls>
                  <source src="/images/build_phase/Build_Team_Hardcoded.mp4" type="video/mp4">
                	Your browser does not support the video tag.
                </video>

                <!--<div id="video" style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/260411006" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>-->
               
            </div>
        </div>
        
        <div class="rightbuildwith">
        	<div class="buildsecdetails">
                <h2>See For Yourself</h2>
                <p>
                   Our Lead Designer, David, talks <br>
				   you through an ongoing build.
				</p>
                <a href="https://www.buildteam.com/watch_a_build.html">Click here to watch our 8 part series</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="videobottom">
    </div>
</div>
<script src="js/slick.min.js"></script>

<script>
$(".helpmechoose").click(function(){
	$(".quizwrap").animate({left:"0%"});
	});
$(".closebuttonquiz").click(function(){
	$(".quizwrap").animate({left:"-100%"});
	});	
</script>
<script>
$('.slick-slider').slick({
  slidesToShow: 4,
  slidesToScroll: 4,
  autoplay: false,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '10px',
        slidesToShow: 1
      }
    }
	]
});
$(".buildsecdetails2 a").click(function(){
		$(this).hide();
		$("#video").show();
		//$("#video").play();
		});
	
	$(document).on('click', '.buildsecdetails2 a', function (e) {
		var video = $("#video").get(0);
		if (video.paused === false) {
			video.pause();
		} else {
			video.play();
		}

    return false;
});	
</script>
<?php require_once('./inc/footer.inc.php'); ?>