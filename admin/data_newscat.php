<?php

$PageTitle = "News Categories";
$TableName = "news_category";
$PrimaryKey = "news_category_id";
$FieldNames = "news_category_id,news_category_name,news_category_code,is_enabled";
$DisplayNames = "ID,Name,Code,Enabled";
$ModFieldNames = "news_category_id,news_category_name,news_category_code,sort,is_enabled";
$ModDisplayNames = "ID,Name,Code,Sort,Enabled";
$ModFieldTypes = "-1,2,-1,1,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>