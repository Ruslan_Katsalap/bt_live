<?php

require_once ('./inc/util.inc.php');

$curr_url = $_SERVER["SCRIPT_NAME"];

$curr_url = strtolower($curr_url);
$arr_curr_url = explode("/", $curr_url);
$curr_url = $arr_curr_url[2]; //str_replace("/", "", $curr_url);
$curr_url = str_replace(".php", "", $curr_url);
$parent_url = explode("_", $curr_url);
$parent_url = $parent_url[0];

//echo $curr_url;
//echo $parent_url;

$arr_nav = array("Home","Enquiries","Data Management","Build Your Price","RFQs","Booked Visits","BYP Calculator","Reports","Admin Tools");
$arr_nav_url = array("index","enquiries","data","ref","rfq","book","bypcal","reports","admin");


$arr_subnav = array("Home","Contact,Requested Calls,Build Only Enquiries,House Tour Bookings, Post Brochure, Download Brochure, Read Brochure, Do it with our help","Pages,Projects,Project Categories,News,News Categories,Press Releases,TV,Q&A,Team,Promos,Testimonials,Design &amp; Planning,Events,Join Our Team, Find Scheme,Gallery,Borough,Project Type,Floor Area, GDPR, Design Bundle Formula,Design Bundle Records,House Tours
","Summary,Category,Subcategories,Components Old,Components,Room Size,CGI,Margin,Units","Summary Old,Quote Requests Old BYP, Summary, Quote Requests","DC Bookings,PP Visits,Other Visits","List of Quotes,Images, BYP Price List, BYP Additional Cost, BYP Final Video, New BYP","Reports, How Can We Help, Upgrade to Premium SV, Videos, Design Phase","Settings,Re-solicitation email,Admin Accounts");
$arr_subnav_url = array("index","enquiries_contact_form,enquiries_request_call,enquiries_architectural_plan,enquiries_housebook,enquiries_brochure,enquiries_downloadBrochure,enquiries_readbrochure,enquiries_doitwithourhelp","data,data_project,data_projectcat,data_news,data_newscat,data_press_release,data_tv,data_faq,data_team,data_promo,data_testimonial,data_design_planning,data_events,data_join_our_team,data_find_scheme,data_project_new,data_projectborough,data_projecttype,data_projectfloorarea,data_gdpr,data_bundle_project_formula,data_design_bundle,data_house_tours","ref,ref_cat,ref_sub,ref_comp_old,ref_comp,ref_rs,ref_cgi,ref_margin,ref_unit","rfq,rfq_quotes,rfq_newsummary,rfq_newquotes","dc_booking,pp_booking,book","bypcal,bypcal_images,bypcal_pricelist,bypcal_additionalcost,bypcal_video, bypcal_new","reports,reports_HowCanWeHelp,reports_upgrade_to_premium,reports_videos,reports_design_phase","admin,admin_resolicit,admin_accounts");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex">
<title>buildteam&trade; &bull; CMS 0.1</title>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<?php
if(isset($admin_page_name)){
    if($admin_page_name == "dc_booking"){
       echo' <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>';
        echo '<script src="/assets_b/js/datapicker_ui.js"></script>';
        echo "<style>#ui-datepicker-div {display: none;}</style>";
        echo '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">';
        echo '<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>';
        echo '<script src="/assets_b/js/moment.js"></script>';

    }
} else {
  echo '<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>';
  echo '<script src="js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>';
}
?>





<link rel="stylesheet" href="css/ui/ui.css" type="text/css" />
<script language="javascript" type="text/javascript" src="js/default.js"></script>
<?php if (is_int(strpos($_SERVER['REQUEST_URI'], 'data_news.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'data.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'data_press_release.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'data_faq.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'admin.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'admin_resolicit.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'book_settings.php'))) : ?>
<script src="js/ckeditor/ckeditor.js" type="text/javascript"></script>
<?php endif; ?>

<?php if (is_int(strpos($_SERVER['REQUEST_URI'], 'rfq.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'rfq_newsummary.php'))) : ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<style type="text/css">
.info_content {
  float:left;
  width:300px;
  min-height:50px;
}
.info_content a {
  color:#4169e1;
  text-decoration:underline;
}
.info_content a:hover {
  text-decoration:none;
}
h3.head_totals span {
  color:blue;
}
</style>
<?php endif; ?>

<style>
  /*dc admin form styles*/
.dc_book_admin {
  margin: 25px;
  max-width: 350px;
  text-align: center;
}

.dc_picker {
  position: relative;
  margin-bottom: 20px;
}

.dc_picker_prev {
  background: #20326f;
  position: absolute;
  top: 0;
  left: 0;
}

.dc_picker_next {
  background: #20326f;
  position: absolute;
  right: 0;
  top: 0;
}

.dc_box {
  margin-bottom: 20px;
}

.dc_slot_row {
  margin-bottom: 20px;
}

.dc_slot_row span {
  margin-right: 10px;
}

.update_slots_btn {
  background: #20326f;
  color: #fff;
  padding: 20px;
}

.slot_msg {
  display: none;
  text-align: center;
  margin: 10px 0;
  color: green;
}

.dc_box {
  margin-bottom: 45px;
}

.change_time, .change_date_range {
    background: #20326f;
    color: #fff;
}
</style>



</head>
<body>
<table cellspacing="0" width="100%" class="t_logo">
	<tr>
  	<td><img src="pics/logo.gif" width="208" height="45" alt="buildteam" /></td>
    <td style="text-align:right">Logged in as: <b><?php echo $_SESSION['admin_name']?></b> | <a href="login.php" class="rb">Logout</a>
  </tr>
</table>
<table cellspacing="0" width="100%">
	<tr>
  	<td class="td_nav_bg">
    	<table cellspacing="0">
      	<tr valign="bottom">
          <td><img src="pics/blank.gif" width="20" height="8" /></td>
          
          <?php
					
					$sel_i = 0;
					$i = 0;					

					foreach ($arr_nav_url as $value) {
						if ( $parent_url == $value) {
							$sel_i = $i;
							echo '
							<td>
								<table cellspacing="0" class="t_nav_sel">
									<tr>
										<td><img src="pics/nav_elem_left.gif" width="2" height="30" /></td>
										<td class="txt"><a href="' . $value . '.php" class="w">' . $arr_nav[$i] . '</a></td>
										<td><img src="pics/nav_elem_right.gif" width="2" height="30" /></td>
									</tr>
								</table>
							</td>						
							';
						}
						else {    
							//$sel_i = $i;						
							echo '
							<td>
								<table cellspacing="0" class="t_nav_elem">
									<tr>
										<td><img src="pics/nav_sel_left.gif" width="2" height="27" /></td>
										<td class="txt"><a href="' . $value . '.php" class="nav">' . $arr_nav[$i] . '</a></td>
										<td><img src="pics/nav_sel_right.gif" width="2" height="27" /></td>
									</tr>
								</table>
							</td>';
						}
						echo '<td><img src="pics/blank.gif" width="4" height="8" /></td>';
						
						$i ++;
					}

					?>
        </tr>
      </table>
    </td>
  </tr>
	<tr>
  	<td class="td_subnav">  
			<?php

      if(isset($admin_page_name)){
        if($admin_page_name == "dc_booking" || $admin_page_name == "pp_booking"){
          echo '<a href="dc_booking.php">DC Bookings</a><a href="pp_booking.php" class="sel">PP Visits</a><a href="book.php" class="sel">Other Visits</a>';
        }
      } else {
            
            $j = 0;
        
            foreach ($arr_subnav_url as $sub_value) {
            
              if ( $sel_i == $j ) {
                
                $subnav = explode(",", $arr_subnav[$j]);
                $subnav_url = explode(",", $sub_value);
      		  print_r($sel_i);
                $k = 0;
                
                foreach ($subnav_url as $value) {
              
                  if ($curr_url == $value) {
                    echo '<a href="' . $value . '.php" class="sel">' . $subnav[$k] . '</a>';
                  }
                  else {
                    echo '<a href="' . $value . '.php">' . $subnav[$k] . '</a>';
                  }
                  
                  $k ++;
                }
              }
              
              $j ++;
            }
      }
      ?>
    </td>
  </tr>
	<tr>
  	<td class="td_nav_shd"><img src="pics/blank.gif" width="5" height="8" /></td>
  </tr>
</table>
<table cellspacing="0" width="100%">
	<tr>
  	<td class="td_content">