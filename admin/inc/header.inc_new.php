<?php

require_once ('./inc/util.inc.php');

$curr_url = $_SERVER["SCRIPT_NAME"];

$curr_url = strtolower($curr_url);
$arr_curr_url = explode("/", $curr_url);
$curr_url = $arr_curr_url[2]; //str_replace("/", "", $curr_url);
$curr_url = str_replace(".php", "", $curr_url);
$parent_url = explode("_", $curr_url);
$parent_url = $parent_url[0];

//echo $curr_url;
//echo $parent_url;

$arr_nav = array("Home","Data Management","RFQs","Booked Visits","BYP Calculator","House Tour Bookings","Reports","Admin Tools", "GDPR");
$arr_nav_url = array("index","data","rfq","book","bypcal","housebook","reports","admin", "gdpr");

$arr_subnav = array("Home","Pages,Projects,Project Categories,News,News Categories,Press Releases,TV,Q&A,Team,Promos,Testimonials,Design &amp; Planning,Events","Summary,Quote Requests","Booked Visits,Coupons,Settings","List of Quotes,Images","Reports","Settings,Re-solicitation email,Admin Accounts");
$arr_subnav_url = array("index","data,data_project,data_projectcat,data_news,data_newscat,data_press_release,data_tv,data_faq,data_team,data_promo,data_testimonial,data_design_planning,data_events","rfq,rfq_quotes","book,book_coupon,book_settings","bypcal,bypcal_images","reports","admin,admin_resolicit,admin_accounts");

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="robots" content="noindex">
<title>buildteam&trade; &bull; CMS 0.1</title>
<link rel="stylesheet" type="text/css" href="css/default.css" />
<script src="js/jquery-1.3.2.min.js" type="text/javascript"></script>
<script src="js/jquery-ui-1.7.2.custom.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="css/ui/ui.css" type="text/css" />
<script language="javascript" type="text/javascript" src="js/default.js"></script>
<?php if (is_int(strpos($_SERVER['REQUEST_URI'], 'data_news.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'data.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'data_press_release.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'data_faq.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'admin.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'admin_resolicit.php')) || is_int(strpos($_SERVER['REQUEST_URI'], 'book_settings.php'))) : ?>
<script src="js/ckeditor/ckeditor.js" type="text/javascript"></script>
<?php endif; ?>

<?php if (is_int(strpos($_SERVER['REQUEST_URI'], 'rfq.php'))) : ?>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script type="text/javascript" src="https://www.google.com/jsapi"></script>
<style type="text/css">
.info_content {
  float:left;
  width:300px;
  min-height:50px;
}
.info_content a {
  color:#4169e1;
  text-decoration:underline;
}
.info_content a:hover {
  text-decoration:none;
}
h3.head_totals span {
  color:blue;
}
</style>
<?php endif; ?>

</head>
<body>
<table cellspacing="0" width="100%" class="t_logo">
	<tr>
  	<td><img src="pics/logo.gif" width="208" height="45" alt="buildteam" /></td>
    <td style="text-align:right">Logged in as: <b><?php echo $_SESSION['admin_name']?></b> | <a href="login.php" class="rb">Logout</a>
  </tr>
</table>
<table cellspacing="0" width="100%">
	<tr>
  	<td class="td_nav_bg">
    	<table cellspacing="0">
      	<tr valign="bottom">
          <td><img src="pics/blank.gif" width="20" height="8" /></td>
          
          <?php
					
					$sel_i = 0;
					$i = 0;					

					foreach ($arr_nav_url as $value) {
					
						if ( $parent_url == $value ) {
							$sel_i = $i;
							echo '
							<td>
								<table cellspacing="0" class="t_nav_sel">
									<tr>
										<td><img src="pics/nav_elem_left.gif" width="2" height="30" /></td>
										<td class="txt"><a href="' . $value . '.php" class="w">' . $arr_nav[$i] . '</a></td>
										<td><img src="pics/nav_elem_right.gif" width="2" height="30" /></td>
									</tr>
								</table>
							</td>						
							';
						}
						else {          
							echo '
							<td>
								<table cellspacing="0" class="t_nav_elem">
									<tr>
										<td><img src="pics/nav_sel_left.gif" width="2" height="27" /></td>
										<td class="txt"><a href="' . $value . '.php" class="nav">' . $arr_nav[$i] . '</a></td>
										<td><img src="pics/nav_sel_right.gif" width="2" height="27" /></td>
									</tr>
								</table>
							</td>';
						}
						echo '<td><img src="pics/blank.gif" width="4" height="8" /></td>';
						
						$i ++;
					}

					?>
        </tr>
      </table>
    </td>
  </tr>
	<tr>
  	<td class="td_subnav">  
			<?php
      
      $j = 0;
  
      foreach ($arr_subnav_url as $sub_value) {
      
        if ( $sel_i == $j ) {
          
          $subnav = explode(",", $arr_subnav[$j]);
          $subnav_url = explode(",", $sub_value);
          $k = 0;
          
          foreach ($subnav_url as $value) {
        
            if ($curr_url == $value) {
              echo '<a href="' . $value . '.php" class="sel">' . $subnav[$k] . '</a>';
            }
            else {
              echo '<a href="' . $value . '.php">' . $subnav[$k] . '</a>';
            }
            
            $k ++;
          }
        }
        
        $j ++;
      }
      
      ?>
    </td>
  </tr>
	<tr>
  	<td class="td_nav_shd"><img src="pics/blank.gif" width="5" height="8" /></td>
  </tr>
</table>
<table cellspacing="0" width="100%">
	<tr>
  	<td class="td_content">
    