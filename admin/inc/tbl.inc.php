<?php

require_once ('./inc/util.inc.php');


$_TableName = $TableName;
$_PrimaryKeyColumn = $PrimaryKey;
$_ColumnsToRetrieve = $FieldNames;
$_ColumnNamesToRetrieve = $DisplayNames;
$_ModColumnsToRetrieve = $ModFieldNames;
$_ModColumnNamesToRetrieve = $ModDisplayNames;
$_ModColumnTypesToRetrieve = $ModFieldTypes;


if ( isset($SearchFields) ) {
	$_SearchFields = $SearchFields;
}
else {
	$_SearchFields = '';
}

if ( isset($_REQUEST['kw']) ) {
	$_Kw = safe($_REQUEST['kw']);
}
else {
	$_Kw = '';
}


if (isset($PageTitle)) {
	$_PageTitle = $PageTitle;
}
else {
	$_PageTitle = 'Table Manager';
}

if (isset($AddNew)) {
	$_AddNew = $AddNew;
}
else {
	$_AddNew = true;
}
if (isset($AllowEdit)) {
	$_AllowEdit = $AllowEdit;
}
else {
	$_AllowEdit = true;
}

if (isset($AllowDelete)) {
	$_AllowDelete = $AllowDelete;
}
else {
	$_AllowDelete = true;
}

if (isset($SplitScreen)) {
	$_SplitScreen = $SplitScreen;
}
else {
	$_SplitScreen = true;
}

$_ShowTable = false;
$_ShowEdit = false;
$_hasRecords = false;
$_AllowHTML = false;
$viewClass = 'hideGrp';
$editClass = 'showGrp';
$_SortColumn = 0;
$_SortDir = 0;
$_Page = 0;
$ItemID = 0;
$_BtnText = "Add New";
$_pages_ = "1 of 1";
$sql = '';

/***************************************************************************************************/
// additional bottom content
/***************************************************************************************************/
$pd_navcontentarticles = '';

$_getCurrentUrl = $_SERVER['PHP_SELF']; //'news_articles.php';

$arr_ColumnsToRetrieve = explode(',', $_ColumnsToRetrieve);
$arr_ColumnNamesToRetrieve = explode(',', $_ColumnNamesToRetrieve);
$arr_ModColumnsToRetrieve = explode(',', $_ModColumnsToRetrieve);
$arr_ModColumnNamesToRetrieve = explode(',', $_ModColumnNamesToRetrieve);
$arr_ModColumnTypesToRetrieve = explode(',', $_ModColumnTypesToRetrieve);
$arr_SearchFields = explode(',', $_SearchFields);

if (isset($_REQUEST['id'])) {
	$ItemID = $_REQUEST['id'];
}

// table containing rows to display
$arr_Table; // = $arr_ColumnsToRetrieve;

// table containing details to display
$arr_ColumnValues = $arr_ModColumnTypesToRetrieve;
$arr_ModColumnValues = $arr_ModColumnTypesToRetrieve;
$arr_ModColumnDisplayValues = $arr_ModColumnTypesToRetrieve;

/***************************************************************************************************/
// remove image
/***************************************************************************************************/
if (isset($_GET['removeimage']) && isset($_GET['nonce']) && $_GET['nonce']==session_id()) {
	if ($_TableName == 'project' ||  $_TableName == "project_new" ) {
		
    $rs = getRs("SELECT filename FROM project_image WHERE project_image_id = '" . (int)$_GET['removeimage']. "'");
    
    $row = mysqli_fetch_assoc($rs);
    if ($row && isset($row['filename'])) {
      $path = explode('/', $abspath);
      array_pop($path);
      array_pop($path);
      array_pop($path);
      $path = implode('/', $path);
    
      @unlink($path.'/projects/'.$row['filename']);
    }
    
    setRs("DELETE FROM project_image WHERE project_image_id = '" . (int)$_GET['removeimage']. "'");
	}
	elseif ($_TableName == 'page') {
		//setRs("UPDATE page_media SET is_active = 0 WHERE page_media_id = '" . (int)$_GET['removeimage']. "'");
    // Custom: real remove image
    
    $rs = getRs("SELECT filename FROM page_media WHERE page_media_id = '" . (int)$_GET['removeimage']. "'");
		
		$row = mysqli_fetch_assoc($rs);
    if ($row && isset($row['filename'])) {
      $path = explode('/', $abspath);
      array_pop($path);
      array_pop($path);
      array_pop($path);
      $path = implode('/', $path);
      //die($path.'/media/'.$row['filename']);
      
      if ('pdf' == getExt($row['filename'])) {
        @unlink($path.'/pdf/'.$row['filename']);
      }
      else {
        @unlink($path.'/media/'.$row['filename']);
      }      
      
      setRs("DELETE FROM page_media WHERE page_media_id = '" . (int)$_GET['removeimage']. "'");
    }
    
    
	}
	elseif ($_TableName == 'team') {
		setRs("UPDATE team SET imagefile_" . $_GET['removeimage']. " = '' WHERE team_id = '" . $ItemID . "'");
	}
	elseif ($_TableName == 'bypcal_images') {
		setRs("UPDATE bypcal_images SET image = '' WHERE id = '" . $ItemID . "'");
	}
  elseif ($_TableName == 'component') {
    setRs("UPDATE {$_TableName} SET image = '' WHERE {$_PrimaryKeyColumn} = '" . $ItemID . "'");
  }
  elseif ($_TableName == 'press_release') {
    $rs = getRs("SELECT imagefile, pdffile FROM press_release WHERE id = '" . (int)$ItemID. "'");
		
		$row = mysqli_fetch_assoc($rs);
    
    $path = explode('/', $abspath);
    array_pop($path);
    array_pop($path);
    array_pop($path);
    $path = implode('/', $path);
    
    if ('pdf'==$_GET['removeimage']) {
      @unlink($path.'/pdf/'.$row['pdffile']);
      
      //exit;#debug
      setRs("UPDATE {$_TableName} SET pdffile = '' WHERE {$_PrimaryKeyColumn} = '" . $ItemID . "'");
    }
    else {
      //echo $path.'/media/'.$row['imagefile'];#debug
      @unlink($path.'/media/'.$row['imagefile']);
      
      //exit;#debug
      setRs("UPDATE {$_TableName} SET imagefile = '' WHERE {$_PrimaryKeyColumn} = '" . $ItemID . "'");
    }
  }
  elseif ('design_planning'==$_TableName) {
    
    $rs = getRs("SELECT pdffile FROM $_TableName WHERE id = '" . (int)$ItemID. "'");
		
		$row = mysqli_fetch_assoc($rs);

    $path = explode('/', $abspath);
    array_pop($path);
    array_pop($path);
    array_pop($path);
    $path = implode('/', $path);
    
    $subdir = $_TableName.'/';
    
    //die($path.'/pdf/'.$subdir.$row['pdffile']);#debug
    
    @unlink($path.'/pdf/'.$subdir.$row['pdffile']);
    
    $basename = explode('.', $row['pdffile']);
    array_pop($basename);
    $basename = implode('.', $basename);
    @unlink($path.'/pdf/'.$subdir.'thumb/'.$basename.'.png');
    
    setRs("UPDATE {$_TableName} SET pdffile = '' WHERE {$_PrimaryKeyColumn} = '" . $ItemID . "'");
    
  }
  elseif ('event_photos' == $_TableName) {
    
    $rs = getRs("SELECT imagefile FROM $_TableName WHERE {$_PrimaryKeyColumn} = '" . (int)$ItemID. "'");
		
    $row = mysqli_fetch_assoc($rs);
    
    @unlink($ROOT_FOLDER . 'images/events/' . $row['imagefile']);
    // remove thumbs:
    @unlink($ROOT_FOLDER . 'images/events/preview/' . $row['imagefile']);
    @unlink($ROOT_FOLDER . 'images/events/thumb/' . $row['imagefile']);
    
    setRs("UPDATE {$_TableName} SET imagefile = '' WHERE {$_PrimaryKeyColumn} = '" . $ItemID . "'");
    
  }
	else {
		setRs("UPDATE {$_TableName} SET imagefile = '' WHERE {$_PrimaryKeyColumn} = '" . $ItemID . "'");
	}
}

/***************************************************************************************************/
// delete
/***************************************************************************************************/
if (isset($_GET['del']) && $_AllowDelete) {
	if ($_GET['del'] > 0) {
        if (isset($_GET['nonce']) && $_GET['nonce']==session_id()) {
          setRs("DELETE FROM " . $_TableName . " WHERE " . $_PrimaryKeyColumn . " = '" . (int)$_GET['del'] . "'");
        }
        else {
		  setRs("UPDATE " . $_TableName . " SET is_active = 0 WHERE " . $_PrimaryKeyColumn . " = '" . (int)$_GET['del'] . "'");
        }
	}
}


/**************************************************************
************* Table Saving and Update functions ***************
***************************************************************/

if (isset($_REQUEST['btnSubmit'])) {
	$btnSubmit = strtolower(safe($_REQUEST['btnSubmit']));
}
else {
	$btnSubmit = '';
}

$sql = '';
 /* random string generator*/
function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}
/* random string generator*/
if ($btnSubmit == 'save changes') {
	$currVal = '';
	$sql = 'UPDATE ' . $_TableName . ' SET ';
	$j = 0;
	for ($i = 1; $i < sizeof($arr_ModColumnsToRetrieve); $i ++) {
		if ($arr_ModColumnTypesToRetrieve[$i] != -1 && $arr_ModColumnTypesToRetrieve[$i] != 99) {
			$currVal = stripslashes($_REQUEST[$arr_ModColumnsToRetrieve[$i]]);
			if ($arr_ModColumnsToRetrieve[$i] == 'password' && ($currVal == $passMask or $currVal == $passMask2)) {
			}
			else {
				if ($j > 0) {
					$sql .= ', ';
				}
				if ($arr_ModColumnTypesToRetrieve[$i] == 0) {
					if ($currVal == "1") {
						$currVal = 1;
					}
					else {
						$currVal = 0;
					}
				}
				elseif ($arr_ModColumnsToRetrieve[$i] == 'date_release' || $arr_ModColumnTypesToRetrieve[$i] == 14) {					               $currVal = strtotime($currVal);
				}
				elseif ($arr_ModColumnsToRetrieve[$i] == 'password') {
					$currVal = formatPassword($currVal);
				}
				$j += 1;
			 	$sql .= $arr_ModColumnsToRetrieve[$i] . ' = \'' . formatSql($currVal) . '\'';
			}
		}
	}
	if ($_TableName == 'news') {
		$sql .= ', news_code = \'' . toLink($_REQUEST['title']) . '\'';
	}
	elseif ($_TableName == 'press_release') {
		$sql .= ', url_code = \'' . toLink($_REQUEST['title']) . '\'';
	}  
	elseif ($_TableName == 'project') {
		$sql .= ', project_code = \'' . toLink($_REQUEST['project_name']) . '-'. generateRandomString() . '\'';
	}
	elseif ($_TableName == 'project_new') {
		$sql .= ', project_code = \'' . toLink($_REQUEST['project_name']) . '-' . generateRandomString() . '\'';
	}
	elseif ($_TableName == 'team') {
		$sql .= ', team_code = \'' . toLink($_REQUEST['name']) . '\'';
	}
	elseif ($_TableName == 'project_category') {
		$sql .= ', project_category_code = \'' . toLink($_REQUEST['project_category_name']) . '\'';
	}
	elseif ($_TableName == 'news_category') {
		$sql .= ', news_category_code = \'' . toLink($_REQUEST['news_category_name']) . '\'';
	}
  if (('page' == $_TableName) || ('project' == $_TableName) || ('project_new' == $_TableName)) { // custom: date_modified for page, project
    $sql .= ', date_modified=UNIX_TIMESTAMP()';
  }
	$sql .= ' WHERE ' . $_PrimaryKeyColumn . ' = ' . $_REQUEST[$_PrimaryKeyColumn];
	//echo ($sql);exit;
	setRs($sql);
	//header("Location: " . $_getCurrentUrl . "?id=" . $_REQUEST[$_PrimaryKeyColumn] . "&p=" . $_REQUEST["p"]);
  if ('page' == $_TableName || 'project' == $_TableName || ('project_new' == $_TableName) || 'news' == $_TableName || 'press_release' == $_TableName) {
    // clear sitemap cache
    $path = explode('/', $abspath);
    array_pop($path);
    array_pop($path);
    array_pop($path);
    $path = implode('/', $path);
    
    @unlink($path.'/xml/sitemap.html');
    @unlink($path.'/xml/sitemap_main.xml');
  }
}

elseif ($btnSubmit == 'add new') {
	$currVal  = '';
  $currVal = '';
	$j = 0;
  $sql = 'INSERT INTO ' . $_TableName . ' (';
	for ($i = 1; $i < sizeof($arr_ModColumnsToRetrieve); $i ++) {
		if ($arr_ModColumnTypesToRetrieve[$i] != -1 && $arr_ModColumnTypesToRetrieve[$i] != 99) {
			if ($j > 0) {
				$sql .= ', ';
			}
			$j += 1;
			$sql .= $arr_ModColumnsToRetrieve[$i];
		}
	}
	if ($_TableName == 'news') {
		$sql .= ', news_code';
	}
  elseif ($_TableName == 'press_release') {
		$sql .= ', url_code';
	}
	elseif ($_TableName == 'project') {
		$sql .= ', project_code';
	}elseif ($_TableName == 'project_new') {
		$sql .= ', project_code';
	}
	elseif ($_TableName == 'team') {
		$sql .= ', team_code';
	}
	elseif ($_TableName == 'project_category') {
		$sql .= ', project_category_code';
	}
	elseif ($_TableName == 'news_category') {
		$sql .= ', news_category_code';
	}
	$sql .= ', date_created'.(('project' == $_TableName || 'project_new' == $_TableName || 'page' == $_TableName)?', date_modified':'').') VALUES (';
	$j = 0;
	for ($i = 1; $i < sizeof($arr_ModColumnsToRetrieve); $i ++) {
		if ($arr_ModColumnTypesToRetrieve[$i] != -1 && $arr_ModColumnTypesToRetrieve[$i] != 99) {
			$currVal = stripslashes($_REQUEST[$arr_ModColumnsToRetrieve[$i]]);

			// mb

			$postcode_no_space = '';
			if ($_TableName == 'project_new' && $arr_ModColumnsToRetrieve[$i] == 'postcode_no_space') $postcode_no_space = $currVal;

			// mb

			if ($j > 0) {
				$sql .= ', ';
			}
			if ($arr_ModColumnTypesToRetrieve[$i] == 0) {
				if ($currVal == "1") {
					$currVal = 1;
				}
				else {
					$currVal = 0;
				}
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'date_release' || $arr_ModColumnTypesToRetrieve[$i] == 14) {
				$currVal = strtotime($currVal);
			}
      elseif ($arr_ModColumnsToRetrieve[$i]=='weight' && ''==$currVal) {
        $currVal = 0;
      }
			$j += 1;
			$sql .= '\'' . formatSql($currVal) . '\'';
		}
	}
	if ($_TableName == 'news') {
		$sql .= ', \'' . toLink($_REQUEST['title']) . '\'';
	}
  elseif ($_TableName == 'press_release') {
		$sql .= ', \'' . toLink($_REQUEST['title']) . '\'';
	}
	elseif ($_TableName == 'news_category') {
		$sql .= ', \'' . toLink($_REQUEST['news_category_name']) . '\'';
	}
	elseif ($_TableName == 'project' || $_TableName == 'project_new') {
		$sql .= ', \'' . toLink($_REQUEST['project_name']) . '\'';
	}
	elseif ($_TableName == 'project_category') {
		$sql .= ', \'' . toLink($_REQUEST['project_category_name']) . '\'';
	}
	elseif ($_TableName == 'team') {
		$sql .= ', \'' . toLink($_REQUEST['name']) . '\'';
	}
	$sql .= ', \'' . time() . '\''.((('project' == $_TableName) || ('project_new' == $_TableName) || ('page' == $_TableName))?', \'' . time() . '\'':'').')';
	//echo ($sql);
	if ($_AddNew) {
		setRs($sql);
		$ItemID = mysqli_insert_id($dbconn);

// mb
		if (($_TableName == 'project' || $_TableName == 'project_new') && $postcode_no_space != '') {
  		  $rs2 = getRs("SELECT northing,easting FROM open_postcode_geo WHERE postcode_no_space = '{$postcode_no_space}' limit 1");

		  $row2 = mysqli_fetch_assoc($rs2);
		  if ($row2) {
			  $easting = $row2['easting'];
			  $northing = $row2['northing'];
			  $sql2 = "UPDATE `{$_TableName}` SET easting='".$easting."',northing='".$northing."' WHERE project_id='".$ItemID."'";
			  //echo "{$sq2} <br />" ;
			  setRs($sql2);
          }
		}
// mb	

	}
  
  if ('page' == $_TableName || 'project' == $_TableName  || 'project_new' == $_TableName || 'news' == $_TableName || 'press_release' == $_TableName) {
    // clear sitemap cache
    $path = explode('/', $abspath);
    array_pop($path);
    array_pop($path);
    array_pop($path);
    $path = implode('/', $path);
    
    @unlink($path.'/xml/sitemap.html');
    @unlink($path.'/xml/sitemap_main.xml');
	
	
	
  }
  
	//header("Location: " . $_getCurrentUrl . "?id=" . $ItemID);
}

/**************************************************************
********************* Custom Table functions*******************
***************************************************************/

if (isset($_POST['navcontentarticleid'])) {
	if ($_POST['navcontentarticleid'] > 0) {
		setRs("INSERT INTO pd_navcontentarticles SET navcontentid = '" . $_POST['navcontentid'] . "', articleid = '" . $_POST['navcontentarticleid'] . "'");
	}
}
if (isset($_GET['del_navcontentarticleid'])) {
	setRs("UPDATE pd_navcontentarticles SET active = 0, enabled = 0 WHERE navcontentarticleid = '" . $_GET['del_navcontentarticleid'] . "'");
}

/**************************************************************
*********************** Upload functions **********************
***************************************************************/

// In PHP versions earlier than 4.1.0, $HTTP_POST_FILES should be used instead
// of $_FILES.
if (isset($_FILES['imagefile'])) {
	$fn = getUniqueFilename(basename($_FILES['imagefile']['name']));
	$uploadfilename = $fn;
	$uploadfiletype = $_FILES['imagefile']['type'];
	$uploadfilesize = $_FILES['imagefile']['size'];
	
    if ('event_photos'==$TableName) { // custom
      $uploadfile = $ROOT_FOLDER . 'images/events/' . $uploadfilename;
    }
    else {
      $uploadfile = $ROOT_FOLDER . 'media/' . $uploadfilename;
    }

	if (move_uploaded_file($_FILES['imagefile']['tmp_name'], $uploadfile)) {
      
      // remove previous image
      $rs = getRs("SELECT imagefile FROM $_TableName WHERE {$_PrimaryKeyColumn} = '" . (int)$ItemID. "'");
		
      $row = mysqli_fetch_assoc($rs);
      
      if ('event_photos'==$TableName) {
        $path = 'images/events/';
      }
      else {
        $path = 'media/';
      }
      
      @unlink($ROOT_FOLDER . $path . $row['imagefile']);
      if ('event_photos'==$TableName) {
        @unlink($ROOT_FOLDER . $path . 'preview/' . $row['imagefile']);
        @unlink($ROOT_FOLDER . $path . 'thumb/' . $row['imagefile']);
        generateThumb($uploadfilename);
      }
      
      setRs('UPDATE ' . $_TableName . ' SET imagefile = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
      
      
      
	}
}

if (isset($_FILES['pdffile'])) {
	$fn = str_replace(' ', '_', basename($_FILES['pdffile']['name'])); // getUniqueFilename
	$uploadfilename = $fn;
	$uploadfiletype = $_FILES['pdffile']['type'];
	$uploadfilesize = $_FILES['pdffile']['size'];
	
  $subdir = '';
  if ('design_planning' == $TableName) $subdir = $TableName.'/';
  
  $uploadfile = $ROOT_FOLDER . 'pdf/' . $subdir . $uploadfilename;
	//echo "<pre>"; 
	//print_r($_FILES);
	if (move_uploaded_file($_FILES['pdffile']['tmp_name'], $uploadfile)) {
	/*if (copy($_FILES['pdffile']['tmp_name'], $uploadfile)) {*/

    $rs = getRs("SELECT pdffile FROM $_TableName WHERE $_PrimaryKeyColumn = '" . (int)$ItemID. "'");
		
		$row = mysqli_fetch_assoc($rs);
    @unlink($ROOT_FOLDER . 'pdf/' . $subdir . $row['pdffile']);
    
    if ('design_planning' == $TableName) {
      $basename = explode('.', $row['pdffile']);
      array_pop($basename);
      $basename = implode('.', $basename);
      @unlink($path.'/pdf/'.$subdir.'thumb/'.$basename.'.png');
    }
    
    setRs('UPDATE ' . $_TableName . ' SET pdffile = \'' . $uploadfilename . '\', date_created=UNIX_TIMESTAMP() WHERE ' . $_PrimaryKeyColumn . ' = ' . (int)$ItemID);
    
    if ('design_planning' == $TableName) {
        $basename = explode('.', $uploadfilename);
        array_pop($basename);
        $basename = implode('.', $basename);
        
        ob_start();
        
        //$cmd = 'convert -density 150x150 '.$uploadfile.'[0] '.$ROOT_FOLDER . 'pdf/' . $subdir . 'thumb/' . $basename.'_150.png';
        
        $cmd = 'gs -q -dBATCH -dNOPAUSE -sDEVICE=pngalpha -dMAxBitmap=500000000 -dAlignToPixles=0 -dGridFitTT=0 -r36x36 -sOutputFile='.$ROOT_FOLDER . 'pdf/' . $subdir . 'thumb/' . $basename.'.png '.$uploadfile;
        
        $last_line = system($cmd, $retval);
        
        //$last_line = system('convert --help', $retval);#debug
        
        //$last_line = system('convert '.$dirname . '/' . $basename.'_150.png[435x600] '.$dirname . '/' . $basename.'.png', $retval);
        
        $buf = ob_get_contents();

        ob_end_clean();
    
        if ($last_line) $msg = '<pre>'.$buf.'</pre> Code: '.$retval;
        else $msg = 'Error Code: '.$retval;#debug
        
        if (!$retval) {
          getThumbFromPDF($ROOT_FOLDER . 'pdf/' . $subdir . 'thumb/' . $basename.'.png');
        }
        else die($cmd.'<br/>'.$msg);#debug
        
    }
    
	}
}

if (isset($_FILES['thumbnailImg'])) {
 
 $uploadfilename = getUniqueFilename(basename($_FILES['thumbnailImg']['name']));
  
  $uploadfiletype = $_FILES['thumbnailImg']['type'];
	$uploadfilesize = $_FILES['thumbnailImg']['size'];
	
	if ( $_TableName == "project_new" ) {	
		$uploadfile = $ROOT_FOLDER . 'projects_new/thumbnail/' . $uploadfilename;
	}
	if ( $_TableName == "testimonials" ) {	
		$uploadfile = $ROOT_FOLDER . 'testimonials/thumbnail/' . $uploadfilename;
	}
	
	if (move_uploaded_file($_FILES['thumbnailImg']['tmp_name'], $uploadfile)) {
		setRs('UPDATE ' . $_TableName . ' SET thumbnailImg = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
	}
}


if (isset($_FILES['filename'])) {
	if ('pdf' == getExt($_FILES['filename']['name'])) {
    $uploadfilename = str_replace(' ', '_', $_FILES['filename']['name']);
  }
  else {
    $uploadfilename = getUniqueFilename(basename($_FILES['filename']['name']));
  }
	$uploadfiletype = $_FILES['filename']['type'];
	$uploadfilesize = $_FILES['filename']['size'];
	
	if ( $_TableName == "project" || $_TableName == "project_new" ) {	
		$uploadfile = $ROOT_FOLDER . 'projects/' . $uploadfilename;
	}
	else {
		$uploadfile = $ROOT_FOLDER . 'media/' . $uploadfilename;
	}

  // PDF custom for pages
  if ('pdf' == getExt($uploadfilename)) {
    $uploadfile = $ROOT_FOLDER . 'pdf/' . $uploadfilename;
  }
  
	if (move_uploaded_file($_FILES['filename']['tmp_name'], $uploadfile)) {
		 if ( $_TableName == "project" ||  $_TableName == "project_new"  ) {
			 setRs("INSERT INTO project_image (project_id, project_image_name, filename, sort, date_created, date_modified) VALUES('{$ItemID}','" . formatSql($_POST['project_image_name']) . "','$uploadfilename','" . (int)$_POST['project_image_sort'] . "','" . time() . "','" . time() . "')");
		 }
		 else {
			 setRs("INSERT INTO page_media (page_id, page_media_name, filename, sort, date_created, date_modified) VALUES('{$ItemID}','" . formatSql($_POST['page_media_name']) . "','$uploadfilename','" . (int)$_POST['page_media_sort'] . "','" . time() . "','" . time() . "')");
		 }
	}
}

if (isset($_FILES['imagefile_sm'])) {
	$uploadfilename = getUniqueFilename(basename($_FILES['imagefile_sm']['name']));
	$uploadfiletype = $_FILES['imagefile_sm']['type'];
	$uploadfilesize = $_FILES['imagefile_sm']['size'];
	$uploadfile = $ROOT_FOLDER . 'team/' . $uploadfilename;

	if (move_uploaded_file($_FILES['imagefile_sm']['tmp_name'], $uploadfile)) {
		 setRs('UPDATE ' . $_TableName . ' SET imagefile_sm = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
	}
}

if (isset($_FILES['imagefile_lg'])) {
	$uploadfilename = getUniqueFilename(basename($_FILES['imagefile_lg']['name']));
	$uploadfiletype = $_FILES['imagefile_lg']['type'];
	$uploadfilesize = $_FILES['imagefile_lg']['size'];
	$uploadfile = $ROOT_FOLDER . 'team/' . $uploadfilename;

	if (move_uploaded_file($_FILES['imagefile_lg']['tmp_name'], $uploadfile)) {
		 setRs('UPDATE ' . $_TableName . ' SET imagefile_lg = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
	}
}

if (isset($_FILES['smallImage'])) {
	$uploadfilename = getUniqueFilename(basename($_FILES['smallImage']['name']));
	$uploadfiletype = $_FILES['smallImage']['type'];
	$uploadfilesize = $_FILES['smallImage']['size'];
	$uploadfile = $ROOT_FOLDER . 'media/' . $uploadfilename;

	if (move_uploaded_file($_FILES['smallImage']['tmp_name'], $uploadfile)) {
		 setRs('UPDATE ' . $_TableName . ' SET smallImage = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
	}
}

if (isset($_FILES['image'])) {
	$uploadfilename = getUniqueFilename(basename($_FILES['image']['name']));
	$uploadfiletype = $_FILES['image']['type'];
	$uploadfilesize = $_FILES['image']['size'];
	$uploadfile = $ROOT_FOLDER . 'byp/' . $uploadfilename;

	if (move_uploaded_file($_FILES['image']['tmp_name'], $uploadfile)) {
		 setRs('UPDATE ' . $_TableName . ' SET image = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
	}
}
if (isset($_FILES['pageImage1'])){
	$fn = getUniqueFilename(basename($_FILES['pageImage1']['name']));
	$uploadfilename = $fn;
	if ('page'==$_TableName) { // custom
	  $uploadfile = $ROOT_FOLDER . 'images/homeBanners/' . $uploadfilename;
	}
	if (move_uploaded_file($_FILES['pageImage1']['tmp_name'], $uploadfile)) {
		setRs('UPDATE ' . $_TableName . ' SET pageImage1 = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
	}
}
if (isset($_FILES['pageImage2'])){
	$fn = getUniqueFilename(basename($_FILES['pageImage2']['name']));
	$uploadfilename = $fn;
	if ('page'==$_TableName) { // custom
	  $uploadfile = $ROOT_FOLDER . 'images/homeBanners/' . $uploadfilename;
	}
	if (move_uploaded_file($_FILES['pageImage2']['tmp_name'], $uploadfile)) {
		setRs('UPDATE ' . $_TableName . ' SET pageImage2 = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
	}
}
if (isset($_FILES['pageImage3'])){
	$fn = getUniqueFilename(basename($_FILES['pageImage3']['name']));
	$uploadfilename = $fn;
	if ('page'==$_TableName) { // custom
	  $uploadfile = $ROOT_FOLDER . 'images/homeBanners/' . $uploadfilename;
	}
	if (move_uploaded_file($_FILES['pageImage3']['tmp_name'], $uploadfile)) {
		setRs('UPDATE ' . $_TableName . ' SET pageImage3 = \'' . $uploadfilename . '\' WHERE ' . $_PrimaryKeyColumn . ' = ' . $ItemID);
	}
}

/**************************************************************
********************** Display functions **********************
***************************************************************/

function saveCookieSorting($tbl, $val, $dir) {
  setcookie($tbl . '_SortColumn', $val, time() + 7200);
	setcookie($tbl . '_SortDir', $dir, time() + 7200);
}

function setSorting() {
	global $_PrimaryKeyColumn, $_TableName, $arr_ColumnsToRetrieve, $_SortDir, $_SortColumn;
	$defaultSort = $_PrimaryKeyColumn . ' DESC';
	$sortColumn = 0;
	$sortDir = 1;
	//echo $_COOKIE[$_TableName . '_SortColumm'];
	if (isset($_REQUEST['s']) and isset($_REQUEST['sd'])) {
		saveCookieSorting($_TableName, $_REQUEST['s'], $_REQUEST['sd']);
		$sortColumn = $_REQUEST['s'];
		$sortDir = $_REQUEST['sd'];
	}
	elseif (isset($_COOKIE[$_TableName . '_SortColumn']) and isset($_COOKIE[$_TableName . '_SortDir'])) {
		$sortColumn = $_COOKIE[$_TableName . '_SortColumn'];
		$sortDir = $_COOKIE[$_TableName . '_SortDir'];
	}
	if (is_numeric($sortColumn) and is_numeric($sortDir)) {
		if ($sortColumn < sizeof($arr_ColumnsToRetrieve)) {
			$_SortColumn = $sortColumn;
			$_SortDir = $sortDir;
			$defaultSort = $arr_ColumnsToRetrieve[$sortColumn];
			if ($sortDir == '1') {
				$defaultSort .= ' DESC';
			}
		}
	}
	return $defaultSort;
}


$temp_ColumnsToRetrieve = $_ColumnsToRetrieve;

$temp_ColumnsToRetrieve = str_replace("LastModified", "UNIX_TIMESTAMP(LastModified) AS LastModified", $temp_ColumnsToRetrieve);
$temp_ColumnsToRetrieve = str_replace("DateCreated", "UNIX_TIMESTAMP(DateCreated) AS DateCreated", $temp_ColumnsToRetrieve);
$sql = "SELECT {$temp_ColumnsToRetrieve} FROM {$_TableName} WHERE is_active = 1";
/*
print_r($arr_SearchFields);
echo $_Kw.'!!!!!!!!!!!!!!!';
echo sizeof($arr_SearchFields) .'> 1 '. strlen($_Kw);
*/
if ( sizeof($arr_SearchFields) >= 1 && strlen($_Kw) ) {
	$sql .= " AND (";
	$x = 0;
	foreach ($arr_SearchFields as $value) {
		$x ++;
		if ( $x > 1 ) {
			$sql .= " OR ";
		}
    if ('postcode'==$value) $_Kw = str_replace(' ', '', $_Kw);
		$sql .= "{$value} LIKE '{$_Kw}%'";
    if ('rfq'==$_TableName) break;
	}
  
  if ('rfq'==$_TableName) {
    if (isset($_REQUEST['name']) && $_REQUEST['name']) $sql .= " AND name LIKE '%".mysqli_real_escape_string($dbconn,trim($_REQUEST['name']))."%'";
    
    if (isset($_REQUEST['address']) && $_REQUEST['address']) $sql .= " AND address LIKE '%".mysqli_real_escape_string($dbconn,trim($_REQUEST['address']))."%'";
    
    if (isset($_REQUEST['rfq_code']) && $_REQUEST['rfq_code']) $sql .= " AND rfq_code LIKE '".mysqli_real_escape_string($dbconn,trim($_REQUEST['rfq_code']))."%'";
  }
  
	$sql .= ")";
}

if (!strlen($_Kw) && 'rfq'==$_TableName) {
  $sql .= " AND (1=1";
  
  if (isset($_POST['name']) && $_POST['name']) $sql .= " AND name LIKE '%".mysqli_real_escape_string(trim($_POST['name']))."%'";
  
  if (isset($_POST['address']) && $_POST['address']) $sql .= " AND address LIKE '%".mysqli_real_escape_string(trim($_POST['address']))."%'";
  
  if (isset($_POST['rfq_code']) && $_POST['rfq_code']) $sql .= " AND rfq_code LIKE '".mysqli_real_escape_string(trim($_POST['rfq_code']))."%'";
  
  $sql .= ")";
}

if ('byp_Quote'==$_TableName) { 
  $sql .= " AND (1=1";
  
  if (isset($_POST['kw']) && $_POST['kw']) $sql .= " AND addPostcode LIKE '%".mysqli_real_escape_string($dbconn,trim($_POST['kw']))."%'";
  if (isset($_POST['name']) && $_POST['name']) $sql .= " AND fname LIKE '%".mysqli_real_escape_string($dbconn,trim($_POST['name']))."%'";
  
  if (isset($_POST['address']) && $_POST['address']) $sql .= " AND address LIKE '%".mysqli_real_escape_string($dbconn,trim($_POST['address']))."%'";
  
  if (isset($_POST['rfq_code']) && $_POST['rfq_code']) $sql .= " AND refCode LIKE '".mysqli_real_escape_string(trim($_POST['rfq_code']))."%'";
  
  if (isset($_POST['homeowner']) && $_POST['homeowner']) $sql .= " AND currentstatus NOT LIKE 'pre-purchase' AND dates_created >= '" . date('Y') . "-01-01'"; // mb 20210228

  if (isset($_POST['prepurchase']) && $_POST['prepurchase']) $sql .= " AND currentstatus LIKE 'pre-purchase' AND dates_created >= '" . date('Y') . "-01-01'";  // mb 20210228

  $sql .= ")";
}


$sql .= " ORDER BY " . setSorting();
//echo ($sql);#debug

$rs = getRs($sql);
$num_items = mysqli_affected_rows($dbconn);


//paging
$max_num = 50;

$pages = ceil( $num_items / $max_num );

if( $pages < 1 )
	$pages = 1;

if (isset($_REQUEST['p']) and is_numeric($_REQUEST['p'])) {
	$_Page = (int)$_REQUEST['p'];
}
else {
	$_Page = 1;
}

if( $_Page < 1 )
	$_Page = 1;
if( $_Page > $pages )
	$_Page = $pages;

$sqlLimitFrom = ( $_Page - 1 ) * $max_num;
$sqlLimit = " LIMIT $sqlLimitFrom, $max_num";

$sql .= " {$sqlLimit}";
$rs = getRs($sql);

$_pages_ = "{$_Page} of {$pages}";

$j = 0;
while ($row = mysqli_fetch_assoc($rs)) {
	for ($i = 0; $i < sizeof($arr_ColumnsToRetrieve); $i += 1) {
		if ($arr_ColumnsToRetrieve[$i] == 'is_enabled' || $arr_ColumnsToRetrieve[$i] == 'is_active' || $arr_ColumnsToRetrieve[$i] == 'is_default') {
			$arr_ColumnValues[$i] = yesNoFormat($row[$arr_ColumnsToRetrieve[$i]]);
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'parent_page_id') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT page_name FROM page WHERE page_id = ' . $row[$arr_ColumnsToRetrieve[$i]], "page_name");
		}
		elseif (($arr_ColumnsToRetrieve[$i] == 'rfq_status_id' || $arr_ColumnsToRetrieve[$i] == 'statusQuo') && ($_TableName != 'rfq_status' || $_TableName != 'byp_Quote')) {
			$arr_ColumnValues[$i] = getSqlValue('SELECT rfq_status_name FROM rfq_status WHERE rfq_status_id = ' . $row[$arr_ColumnsToRetrieve[$i]], "rfq_status_name");
		}
    
		elseif ($arr_ColumnsToRetrieve[$i] == 'terrace_type_id' && $_TableName != 'terrace_type') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT terrace_type_name FROM terrace_type WHERE is_active = 1 AND terrace_type_id = ' . $row[$arr_ColumnsToRetrieve[$i]], "terrace_type_name");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'news_category_id' && $_TableName != 'news_category') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT news_category_name FROM news_category WHERE is_active = 1 AND news_category_id = ' . $row[$arr_ColumnsToRetrieve[$i]], "news_category_name");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'project_category_id' && $_TableName != 'project_category') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT project_category_name FROM project_category WHERE is_active = 1 AND project_category_id = ' . $row[$arr_ColumnsToRetrieve[$i]], "project_category_name");
		}
    elseif ($arr_ColumnsToRetrieve[$i] == 'property' && $_TableName == 'design_planning') {
      $arr_ColumnValues[$i] = $a_property[$row[$arr_ColumnsToRetrieve[$i]]];
    }
    elseif ($arr_ColumnsToRetrieve[$i] == 'roof' && $_TableName == 'design_planning') {
      $arr_ColumnValues[$i] = $a_roof[$row[$arr_ColumnsToRetrieve[$i]]];
    }
    elseif ($arr_ColumnsToRetrieve[$i] == 'borough' && $_TableName == 'design_planning') {
      $arr_ColumnValues[$i] = getSqlValue('SELECT name FROM borough WHERE id = ' . $row[$arr_ColumnsToRetrieve[$i]], "name");
    }elseif ($arr_ColumnsToRetrieve[$i] == 'protypeID' && $_TableName == 'design_planning') {
      $arr_ColumnValues[$i] = getSqlValue('SELECT project_type FROM dpproject_type WHERE id = ' . $row[$arr_ColumnsToRetrieve[$i]], "project_type");
    }
		elseif ($arr_ColumnsToRetrieve[$i] == 'articlesubcategoryid' && $_TableName != 'pd_articlesubcategories') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT articlesubcategory FROM pd_articlesubcategories WHERE active = 1 AND articlesubcategoryid = ' . $row[$arr_ColumnsToRetrieve[$i]], "articlesubcategory");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'newscategoryid' && $_TableName != 'pd_newscategories') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT newscategory FROM pd_newscategories WHERE active = 1 AND newscategoryid = ' . $row[$arr_ColumnsToRetrieve[$i]], "newscategory");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'hbbcategoryid' && $_TableName != 'pd_hbbcategories') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT hbbcategory FROM pd_hbbcategories WHERE active = 1 AND hbbcategoryid = ' . $row[$arr_ColumnsToRetrieve[$i]], "hbbcategory");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'navid' && $_TableName != 'pd_nav') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT caption FROM pd_nav WHERE navid = ' . $row[$arr_ColumnsToRetrieve[$i]], "caption");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'bannerlocationid' && $_TableName != 'pd_bannerlocations') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT bannerlocation FROM pd_bannerlocations WHERE active = 1 AND bannerlocationid = ' . $row[$arr_ColumnsToRetrieve[$i]], "bannerlocation");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'banneraccountid' && $_TableName != 'pd_banneraccounts') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT company FROM pd_banneraccounts WHERE active = 1 AND banneraccountid = ' . $row[$arr_ColumnsToRetrieve[$i]], "company");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'videocategoryid' && $_TableName != 'pd_videocategories') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT videocategory FROM pd_videocategories WHERE active = 1 AND videocategoryid = ' . $row[$arr_ColumnsToRetrieve[$i]], "videocategory");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'communityarticlecategoryid' && $_TableName != 'pd_communityarticlecategories') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT communityarticlecategory FROM pd_communityarticlecategories WHERE active = 1 AND communityarticlecategoryid = ' . $row[$arr_ColumnsToRetrieve[$i]], "communityarticlecategory");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'sponsoredlinktypeid' && $_TableName != 'pd_sponsoredlinktypes') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT sponsoredlinktype FROM pd_sponsoredlinktypes WHERE active = 1 AND sponsoredlinktypeid = ' . $row[$arr_ColumnsToRetrieve[$i]], "sponsoredlinktype");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'sponsoredlinkaccountid' && $_TableName != 'pd_sponsoredlinkaccounts') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT company FROM pd_sponsoredlinkaccounts WHERE active = 1 AND sponsoredlinkaccountid = ' . $row[$arr_ColumnsToRetrieve[$i]], "company");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'CategoryId' && $_TableName != 'raymoviecategories') {
			$arr_ColumnValues[$i] = getSqlValue('SELECT raymoviecategory FROM raymoviecategories WHERE active = 1 AND raymoviecategoryid = ' . $row[$arr_ColumnsToRetrieve[$i]], "raymoviecategory");
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'date_release') {
			$arr_ColumnValues[$i] = date($PHP_SHORT_DATE_FORMAT, $row[$arr_ColumnsToRetrieve[$i]]);
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'date_created' || $arr_ColumnsToRetrieve[$i] == 'date_modified') {
			$arr_ColumnValues[$i] = date($PHP_LONG_DATE_FORMAT, $row[$arr_ColumnsToRetrieve[$i]]);
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'message' && ($_TableName == 'architectural_plans' )) {$addDot = '';
			if(strlen($row[$arr_ColumnsToRetrieve[$i]])>30)
				$addDot = '...';
			$arr_ColumnValues[$i] =   substr($row[$arr_ColumnsToRetrieve[$i]],0,30).$addDot;
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'imageName' && ($_TableName == 'architectural_plans' )) {$addDot = '';
			if(strlen($row[$arr_ColumnsToRetrieve[$i]])>30)
				$addDot = '...';
			$arr_ColumnValues[$i] =   substr($row[$arr_ColumnsToRetrieve[$i]],0,30).$addDot;
		}
		elseif ($arr_ColumnsToRetrieve[$i] == 'planning_url') {
			$arr_ColumnValues[$i] =   substr($row[$arr_ColumnsToRetrieve[$i]],0,30);
		}
		else {
			$arr_ColumnValues[$i] = $row[$arr_ColumnsToRetrieve[$i]];
		}
		//$arr_Table[$j][$i] = $row[$arr_ColumnsToRetrieve[$i]]; //$arr_ModColumnValues;
		//echo '<li> ' . $row[$arr_ColumnsToRetrieve[$i]];
	}
	$arr_Table[$j] = $arr_ColumnValues;
	$j ++;
	$_hasRecords = true;
  
}

if ($ItemID > 0) {

	$temp_ColumnsToRetrieve = $_ModColumnsToRetrieve;
	$temp_ColumnsToRetrieve = str_replace("LastModified", "UNIX_TIMESTAMP(LastModified) AS LastModified", $temp_ColumnsToRetrieve);
	$temp_ColumnsToRetrieve = str_replace("DateCreated", "UNIX_TIMESTAMP(DateCreated) AS DateCreated", $temp_ColumnsToRetrieve);
  
  $temp_ColumnsToRetrieve .= ', date_created';
  
  // custom
  if ('rfq'==$TableName) {
    $photo_fields = 'photo,photo2,photo3,photo4,photo5';
    $temp_ColumnsToRetrieve .= ','.$photo_fields;
  }
  
	$sql = 'SELECT ' . $temp_ColumnsToRetrieve . ' FROM ' . $_TableName . ' WHERE ' . $_PrimaryKeyColumn . ' = \'' . $ItemID . '\'';
	$viewClass = 'showGrp';
	$editClass = 'hideGrp';

	$rs = getRs($sql);
	while ($row = mysqli_fetch_assoc($rs)) {
		for ($i = 0; $i < sizeof($arr_ModColumnsToRetrieve); $i += 1) {
			$arr_ModColumnValues[$i] = $row[$arr_ModColumnsToRetrieve[$i]];
			if ($arr_ModColumnTypesToRetrieve[$i] == '0') {
				$arr_ModColumnDisplayValues[$i] = yesNoFormat($row[$arr_ModColumnsToRetrieve[$i]]);
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'parent_page_id') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT page_name FROM page WHERE is_active = 1 AND page_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "page_name");
			}
			elseif (($arr_ModColumnsToRetrieve[$i] == 'date_created' or $arr_ModColumnsToRetrieve[$i] == 'date_modified') and strlen($row[$arr_ModColumnsToRetrieve[$i]]) > 0) {
				$arr_ModColumnDisplayValues[$i] = date($PHP_LONG_DATE_FORMAT, $row[$arr_ModColumnsToRetrieve[$i]]);
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'subcategory_id' && $_TableName != 'subcategory') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT subcategory_name FROM subcategory WHERE is_active = 1 AND subcategory_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "subcategory_name");
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'category_id' && $_TableName != 'category') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT category_name FROM category WHERE is_active = 1 AND category_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "category_name");
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'rfq_status_id' && $_TableName != 'rfq_status') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT rfq_status_name FROM rfq_status WHERE rfq_status_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "rfq_status_name");
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'news_category_id' && $_TableName != 'news_category') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT news_category_name FROM news_category WHERE is_active = 1 AND news_category_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "news_category_name");
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'project_category_id' && $_TableName != 'project_category') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT project_category_name FROM project_category WHERE is_active = 1 AND project_category_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "project_category_name");
			}
      elseif ($arr_ModColumnsToRetrieve[$i] == 'borough' && $_TableName == 'design_planning') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT name FROM borough WHERE id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "name");
			}
		elseif ($arr_ModColumnsToRetrieve[$i] == 'protypeID' && $_TableName == 'design_planning') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT project_type FROM dpproject_type WHERE id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "project_type");
			}
      elseif ($arr_ModColumnsToRetrieve[$i] == 'property' && $_TableName == 'design_planning') {
				$arr_ModColumnDisplayValues[$i] = $a_property[ $row[$arr_ModColumnsToRetrieve[$i]] ];
			}
      elseif ($arr_ModColumnsToRetrieve[$i] == 'roof' && $_TableName == 'design_planning') {
				$arr_ModColumnDisplayValues[$i] = $a_roof[ $row[$arr_ModColumnsToRetrieve[$i]] ];
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'terrace_type_id' && $_TableName != 'terrace_type') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT terrace_type_name FROM terrace_type WHERE terrace_type_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "terrace_type_name");
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'quote_type_id' && $_TableName != 'quote_type') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT quote_type_name FROM quote_type WHERE quote_type_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "quote_type_name");
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'contact_method_id' && $_TableName != 'contact_method') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT contact_method_name FROM contact_method WHERE contact_method_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "contact_method_name");
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'measure_unit_id' && $_TableName != 'measure_unit') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT name FROM measure_unit WHERE is_active = 1 AND measure_unit_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "name");			
			}
      elseif ($arr_ModColumnsToRetrieve[$i] == 'is_email_quote' && $_TableName == 'rfq') {
        if (-1==$row[$arr_ModColumnsToRetrieve[$i]]) $arr_ModColumnDisplayValues[$i] = 'Saved';
        elseif (1==$row[$arr_ModColumnsToRetrieve[$i]]) $arr_ModColumnDisplayValues[$i] = 'Emailed';
        else $arr_ModColumnDisplayValues[$i] = 'No';
      }
			elseif ($arr_ModColumnsToRetrieve[$i] == 'subcategory_1_component_id' or $arr_ModColumnsToRetrieve[$i] == 'subcategory_2_component_id' or $arr_ModColumnsToRetrieve[$i] == 'subcategory_2_component_id' or $arr_ModColumnsToRetrieve[$i] == 'subcategory_3_component_id' or $arr_ModColumnsToRetrieve[$i] == 'subcategory_4_component_id' or $arr_ModColumnsToRetrieve[$i] == 'subcategory_5_component_id' or $arr_ModColumnsToRetrieve[$i] == 'subcategory_6_component_id') {
				$arr_ModColumnDisplayValues[$i] = getSqlValue('SELECT component_name FROM component WHERE is_active = 1 AND component_id = ' . $row[$arr_ModColumnsToRetrieve[$i]], "component_name");			
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'imagefile' and strlen($row[$arr_ModColumnsToRetrieve[$i]]) > 0) { //($arr_ModColumnTypesToRetrieve[$i] == 99 && strlen($row[$arr_ModColumnsToRetrieve[$i]]) > 0) {
			  // custom
              if ('event_photos'==$TableName) {
                $arr_ModColumnDisplayValues[$i] = '<img src="/images/events/thumb/' . $row[$arr_ModColumnsToRetrieve[$i]] . '" alt="" /><br /><a href="' . $_getCurrentUrl . '?removeimage=1&id=' . $ItemID . '&nonce='.session_id().'">Remove Image</a>';
              }
              else {
                $arr_ModColumnDisplayValues[$i] = '<img src="/media/' . $row[$arr_ModColumnsToRetrieve[$i]] . '" width="100" alt="" /><br /><a href="' . $_getCurrentUrl . '?removeimage=1&id=' . $ItemID . '&nonce='.session_id().'">Remove Image</a>';
              }
			}
      elseif ($arr_ModColumnsToRetrieve[$i] == 'pdffile' and strlen($row[$arr_ModColumnsToRetrieve[$i]]) > 0) {
        
        $subdir = '';
        if ('design_planning' == $TableName) $subdir = $TableName.'/';
        
        $basename = explode('.', $row[$arr_ModColumnsToRetrieve[$i]]);
        array_pop($basename);
        $basename = implode('.', $basename);
        
        $arr_ModColumnDisplayValues[$i] = '<a href="/pdf/'.$subdir.$row[$arr_ModColumnsToRetrieve[$i]].'" target="_blank">' . $row[$arr_ModColumnsToRetrieve[$i]] . '<br/><img src="/pdf/'.$subdir.'thumb/'.$basename.'.png?v='.$row['date_created'].'" alt="PDF Thumbnail" /></a><br/><a href="' . $_getCurrentUrl . '?removeimage=pdf&id=' . $ItemID . '&nonce='.session_id().'" onclick="return confirm(\'Are you sure?\')">Remove File</a>';
      }
			elseif ( $arr_ModColumnsToRetrieve[$i] == 'imagefile_sm' and strlen($row[$arr_ModColumnsToRetrieve[$i]]) > 0) {
				$arr_ModColumnDisplayValues[$i] = '<img src="/team/' . $row[$arr_ModColumnsToRetrieve[$i]] . '" width="100" alt="" /><br /><a href="' . $_getCurrentUrl . '?removeimage=sm&id=' . $ItemID . '&nonce='.session_id().'">Remove Image</a>';
			}
			elseif ( $arr_ModColumnsToRetrieve[$i] == 'imagefile_lg' and strlen($row[$arr_ModColumnsToRetrieve[$i]]) > 0) {
				$arr_ModColumnDisplayValues[$i] = '<img src="/team/' . $row[$arr_ModColumnsToRetrieve[$i]] . '" width="100" alt="" /><br /><a href="' . $_getCurrentUrl . '?removeimage=lg&id=' . $ItemID . '&nonce='.session_id().'">Remove Image</a>';
			}
			elseif ( $arr_ModColumnsToRetrieve[$i] == 'image' and strlen($row[$arr_ModColumnsToRetrieve[$i]]) > 0) {			
				$arr_ModColumnDisplayValues[$i] = '<img src="/byp/' . $row[$arr_ModColumnsToRetrieve[$i]] . '" alt="" /><br /><a href="' . $_getCurrentUrl . '?removeimage=1&id=' . $ItemID . '&nonce='.session_id().'">Remove Image</a>';
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'date_created' || $arr_ModColumnsToRetrieve[$i] == 'date_modified') {
				$arr_ModColumnDisplayValues[$i] = date($PHP_SHORT_DATE_FORMAT, $row[$arr_ModColumnsToRetrieve[$i]]);
				$arr_ModColumnValues[$i] = date($PHP_SHORT_DATE_FORMAT, $row[$arr_ModColumnsToRetrieve[$i]]);
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'date_release') {
				$arr_ModColumnDisplayValues[$i] = date($PHP_SHORT_DATE_FORMAT, $row[$arr_ModColumnsToRetrieve[$i]]);
				$arr_ModColumnValues[$i] = date($PHP_SHORT_DATE_FORMAT, $row[$arr_ModColumnsToRetrieve[$i]]);
			}
			elseif ($arr_ModColumnsToRetrieve[$i] == 'password') {
				$arr_ModColumnDisplayValues[$i] = $passMask;
				$arr_ModColumnValues[$i] = $passMask;
			}
			else {
				if (!is_int(strpos($row[$arr_ModColumnsToRetrieve[$i]], '<p>'))) {
          $arr_ModColumnDisplayValues[$i] = nl2br($row[$arr_ModColumnsToRetrieve[$i]]);
        }
        else {
          $arr_ModColumnDisplayValues[$i] = $row[$arr_ModColumnsToRetrieve[$i]];
        }
			}
		}
    
 
    // custom
    if ('rfq'==$TableName) {
      $photo_fields = explode(',', $photo_fields);
      $a_photo = array();
      foreach ($photo_fields AS $v) $a_photo[$v] = $row[$v];
      //print_r($a_photo);#debug
    }
    
	}
	$_BtnText = 'save changes';
}
else {
	for ($i = 0; $i < sizeof($arr_ModColumnsToRetrieve); $i += 1) {
		if ($arr_ModColumnTypesToRetrieve[$i] == 1) {
			$arr_ModColumnDisplayValues[$i] = '';
			$arr_ModColumnValues[$i] = '0';		
		}
		else {
			$arr_ModColumnDisplayValues[$i] = '';
			$arr_ModColumnValues[$i] = '';
		}
	}
}


/**************************************************************
*********************** Bottom tables *************************
***************************************************************/

if (($_TableName == 'project' || $_TableName == 'project_new') and $arr_ModColumnValues[0] > 0) {
	$rs = getRs("SELECT * FROM project_image WHERE is_active = 1 AND project_id = '" . $arr_ModColumnValues[0] . "'");
	$pd_navcontentarticles = '
	<table width="100%">
		<tr>
			<td colspan="4" class="td_2_1">
				<table>
					<td style="padding:3px"><img src="pics/arr_1.gif" width="10" height="5" alt="" /></td>
					<td style="padding:3px">Project Images</td>
				</table>
			</td>
		</tr>
			<tr>
				<td>';
		
		while ( $row = mysqli_fetch_assoc($rs) ) {

			$pd_navcontentarticles .= '<div class="image-left"><img src="/projects/' . $row['filename'] . '" width="100" alt="" /><a href="javascript:warnAction(\'Really Delete\',\'' . $_getCurrentUrl . '?id=' . $arr_ModColumnValues[0] . '&removeimage=' . $row['project_image_id'] . '&nonce='. session_id() .'\')">Remove</a></div>';

		}
		
		$pd_navcontentarticles .= '<div class="clear"></div></td>
		</tr>
		<tr>
			<td colspan="4" class="td_2_1">
				<table>
					<td style="padding:3px"><img src="pics/arr_1.gif" width="10" height="5" alt="" /></td>
					<td style="padding:3px">Add New Image</td>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table class="t_form">
					<tr>
						<td>File</td>
						<td><input type="file" name="filename" class="input"></td>
					</tr>
					<tr>
						<td>Title</td>
						<td><input type="text" name="project_image_name" class="input"></td>
					</tr>
					<tr>
						<td>Sort</td>
						<td><input type="text" name="project_image_sort" value="0" class="input_sm"></td>
					</tr>
				</table>
		</tr>
	</table>
	';

}


if ($_TableName == 'page' and $arr_ModColumnValues[0] > 0) {
	$rs = getRs("SELECT * FROM page_media WHERE is_active = 1 AND page_id = '" . $arr_ModColumnValues[0] . "' ORDER by sort");
	$pd_navcontentarticles = '
	<table width="100%">
		<tr>
			<td colspan="4" class="td_2_1">
				<table>
					<td style="padding:3px"><img src="pics/arr_1.gif" width="10" height="5" alt="" /></td>
					<td style="padding:3px">Page Media</td>
				</table>
			</td>
		</tr>
			<tr>
				<td>';
		
		while ( $row = mysqli_fetch_assoc($rs) ) {

			$pd_navcontentarticles .= '<div class="image-left">';
      $ext = getExt($row['filename']);
      if ('gif'==$ext || 'jpg'==$ext || 'jpeg'==$ext || 'png'==$ext) $pd_navcontentarticles .= '<img src="/media/' . $row['filename'] . '" width="100" alt="" />';
      elseif ('pdf'==$ext) $pd_navcontentarticles .= '<a href="/pdf/' . $row['filename'] . '" target="_blank">'.$row['filename'].'</a>';
      else $pd_navcontentarticles .= '<a href="/media/' . $row['filename'] . '" target="_blank">'.$row['filename'].'</a>';
      
      $pd_navcontentarticles .= '<a href="javascript:warnAction(\'Really Delete\',\'' . $_getCurrentUrl . '?id=' . $arr_ModColumnValues[0] . '&removeimage=' . $row['page_media_id'] . '&nonce='.session_id().'\')">Remove</a></div>';
		}		
		
		$pd_navcontentarticles .= '<div class="clear"></div></td>
		</tr>
		<tr>
			<td colspan="4" class="td_2_1">
				<table>
					<td style="padding:3px"><img src="pics/arr_1.gif" width="10" height="5" alt="" /></td>
					<td style="padding:3px">Add New Image or PDF</td>
				</table>
			</td>
		</tr>
		<tr>
			<td>
				<table class="t_form">
					<tr>
						<td>File</td>
						<td><input type="file" name="filename" class="input"></td>
					</tr>
					<tr>
						<td>Title</td>
						<td><input type="text" name="page_media_name" class="input"></td>
					</tr>
					<tr>
						<td>Sort</td>
						<td><input type="text" name="page_media_sort" value="0" class="input"></td>
					</tr>
				</table>
		</tr>
	</table>
	';

}



require ('./inc/header.inc.php');

echo '
<form action="' . $_getCurrentUrl . '" method="get">
<table cellspacing="0" width="100%">
	<tr>
		<td>
			<table cellspacing="5">
				<tr>
					<td><h1>' . htmlspecialchars($_PageTitle) . '</h1></td>
					<!---<td><input type="text" name="kw" value="' . $_Kw . '" class="input_md" /></td>
					<td><input type="submit" value="search" class="button" /></td>
					--->
				</tr>
			</table>
		</td>
		<td style="text-align:right">

			<table border="0" cellspacing="5">
				<tr>
				';
				
					if ($_AddNew) {
					echo '			
					<td><a href="'. $_getCurrentUrl . '?id=0"><img src="pics/icon_open.gif" width="14" height="14" border="0" alt="" /></a></td>
					<td><a href="'. $_getCurrentUrl . '?id=0" class="nav">Add New</a></td>
					<td>|</td>
					';
					}
					
					echo '
					<td nowrap><b>Total Records:</b> ' . $num_items . '</td>
				</tr>
			</table>
			
		</td>
	</td>
</table>
</form>
<form action="' . $_getCurrentUrl . '" method="post" enctype="multipart/form-data">'.
('rfq'==$TableName?'':'<input type="hidden" name="kw" value="' . $_Kw . '" />').
'<input type="hidden" name="p" value="' . $_Page . '" />
<input type="hidden" name="new_p" value="1" />
<table cellspacing="0" width="100%">';
  
  if ('rfq'==$TableName) {
    echo '<tr><td colspan="3" style="padding:10px">
    Search by Postcode: <input type="text" name="kw" size="20" maxlength="10" value="'.$_Kw.'" /> 
    by Customer name: <input type="text" name="name" size="30" maxlength="70" value="'.(isset($_REQUEST['name'])?htmlspecialchars($_REQUEST['name'], ENT_QUOTES, 'UTF-8'):'').'" /> 
    by Street address: <input type="text" name="address" size="20" maxlength="40" value="'.(isset($_REQUEST['address'])?htmlspecialchars($_REQUEST['address'], ENT_QUOTES, 'UTF-8'):'').'" /> 
    BYP quote ref: <input type="text" name="rfq_code" size="20" maxlength="20" value="'.(isset($_REQUEST['rfq_code'])?htmlspecialchars($_REQUEST['rfq_code'], ENT_QUOTES, 'UTF-8'):'').'" />
    <br/><br/>
    <input type="submit" value="Search" style="" /></td></tr>';
    
  }

   // mb 20210228  
   if ('byp_Quote'==$TableName) {
    echo '<tr><td colspan="3" style="padding:10px">
    Search by Postcode: <input type="text" name="kw" size="20" maxlength="10" value="'.$_Kw.'" /> 
    by Customer name: <input type="text" name="name" size="30" maxlength="70" value="'.(isset($_REQUEST['name'])?htmlspecialchars($_REQUEST['name'], ENT_QUOTES, 'UTF-8'):'').'" /> 
    by Street address: <input type="text" name="address" size="20" maxlength="40" value="'.(isset($_REQUEST['address'])?htmlspecialchars($_REQUEST['address'], ENT_QUOTES, 'UTF-8'):'').'" /> 
    BYP quote ref: <input type="text" name="rfq_code" size="20" maxlength="20" value="'.(isset($_REQUEST['rfq_code'])?htmlspecialchars($_REQUEST['rfq_code'], ENT_QUOTES, 'UTF-8'):'').'" />
    <label for="homeowner">Homeowner: </label><input type="checkbox" name="homeowner" id="homeowner" />
    <label for="prepurchase">Pre-Purchase: </label><input type="checkbox" name="prepurchase" id="prepurchase"/>
    <br/><br/>
    <input type="submit" value="Search" style="" /></td></tr>';
    if (isset($_REQUEST['homeowner'])) { ?>
	<script>
		$('#homeowner').attr('checked', true);
		$(document).ready(function() {
		$('#bqhp').attr('href', 'byp_homeowner_csv_export.php');
		});
	</script>
    <?php }
    if (isset($_REQUEST['prepurchase'])) { ?>
	<script>
		$('#prepurchase').attr('checked', true);
		$(document).ready(function() {
			$('#bqhp').attr('href', 'byp_pre_purchase_csv_export.php');
		});	
	</script>
    <?php }
    //var_dump($_POST); #debug
    
  }

  if(isset($admin_page_name)){
        if($admin_page_name == "dc_booking"){
          $swith_link = '';
        }
      } else { 
      	$swith_link = '<td><a href="javascript:editmode(0)" class="nav"><img src="pics/icon_pen.gif" width="12" height="12" border="0" alt="" /></a></td><td><a href="javascript:editmode(0)" class="nav">Switch to View Mode</a>';
      }

  echo '<tr>
    <td width="'.('rfq'==$TableName?70:50).'%">
      <table border="0" cellspacing="5" cellpadding="0" id="frm_navLinks" runat="server">        
        <input type="hidden" name="new_p" value="1" />
        <tr>
          <td><a href="' . $_getCurrentUrl . '?p=1&kw=' . $_Kw . '&id=' . $ItemID . '">&laquo; First</a></td>
          <td><a href="' . $_getCurrentUrl . '?p=' . ($_Page - 1) . '&kw=' . $_Kw . '&id=' . $ItemID . '">&lsaquo; Previous</a></td>
          <td>|</td>
          <td align="right"><b>Page</b>:
					<select class="input_md" onchange="goTo(this)" style="background:#fff;width:75px">';
					for ($i = 1; $i <= $pages; $i ++) {
						echo '<option value="' . $_getCurrentUrl . '?p=' . $i . '&kw=' . $_Kw . '&id=' . $ItemID . '"';
						if ($_Page == $i) {
							echo ' selected';
						}
						echo '>' . $i . ' of ' . $pages . '</option>';
					}
					echo '</select>
					</td>
          <td>|</td>
          <td><a href="' . $_getCurrentUrl . '?p=' . ($_Page + 1) . '&kw=' . $_Kw . '&id=' . $ItemID . '">Next &rsaquo;</a></td>
          <td><a href="' . $_getCurrentUrl . '?p=' . $pages . '&kw=' . $_Kw . '&id=' . $ItemID . '">Last &raquo;</a></td>
        </tr>
      </table>
    </td>
		<td><img src="pics/blank.gif" width="10" height="10" alt="" /></td>
		<td width="'.('rfq'==$TableName?30:50).'%">
		
<div id="div_viewmode" class="' . $viewClass . '">
<table border="0"><tr><td><a href="javascript:editmode(1)" class="nav"><img src="pics/icon_pen.gif" width="12" height="12" border="0" alt="" /></a></td><td><a href="javascript:editmode(1)" class="nav">Switch to Edit Mode</a></td></tr></table></div><div id="div_editmode" class="' . $editClass . '"><table border="0"><tr>'.
	
$swith_link

.'</td></tr></table></div>

		
		</td>
  </tr>  
</table>

<table cellspacing="0" cellpadding="0" width="100%" border="0">
<tr valign="top">
';



if ($_SplitScreen) {
	echo '<td width="'.('rfq'==$TableName?70:50).'%">';
}
else {
	echo '<td width="100%">';
}




echo '
<table cellspacing="0" cellpadding="0" width="100%" border="0">
  <tr>';
  
	  for ($i = 0; $i < sizeof($arr_ColumnsToRetrieve); $i += 1) {
	    $cSortDir = 0;
			if ($_SortColumn == $i) {
	      if ($_SortDir == 0) {
	        $cSortDir = 1;
				}
				else {
	        $cSortDir = 0;
				}
			}
		echo '
	  <td class="td_2_2">
	    <table width="100%" border="0" cellpadding="2" cellspacing="0">
	      <tr bgcolor="#DCDCDC" onmouseover="this.className=\'td_2_showHand\'" onmouseout="this.className=\'td_2_hideHand\'" onClick="location.href=\'' . $_getCurrentUrl . '?p=' . $_Page . '&s=' . $i . '&sd=' . $cSortDir . '&kw=' . $_Kw . '&id=' . $ItemID . '\'">
	        <td nowrap style="padding:3px">' . $arr_ColumnNamesToRetrieve[$i] . '</td>
					';
	        if ($_SortColumn == $i) {
					echo '
	        <td width="5" style="padding:3px"><img src="pics/arr_' . $_SortDir . '.gif" width="10" height="5" alt="" /></td>
					';
					}
					echo '
	      </tr>
	    </table>
	  </td>
		';
		}
	if($DeletedTBLName==$TableName)	
	echo '<td class="td_2_2">Delete</td>
	</tr>
	';
	if ($_hasRecords == true) {
	
    //for ($j = 0; $j < sizeof($arr_Table[0]); $j += 1) {
    //for ($j = 0; $j < 1; $j += 1) {	
    foreach($arr_Table as $row) {
      
      if($TableName != "dc_bookings" && $TableName != "pp_booking"){
	      $onclick = 'onclick="location.href=\'' . $_getCurrentUrl . '?p=' . $_Page . '&s=' . $_SortColumn . '&sd=' . $_SortDir . '&kw=' . $_Kw . '&id=' . $row[0] . '\'"';
	  } else {
	  	$onclick = "";
	  }
      
      echo '<tr bgcolor="#F7F6F4" valign="top" onmouseover="this.className=\'td_3_showHand\'" onmouseout="this.className=\'td_3_hideHand\'" '.('team'!=$TableName?$onclick:'').'>';
        for ($i = 0; $i < sizeof($arr_ColumnsToRetrieve); $i ++) {
          echo '<td class="td_3';
          if ($row[0] == $ItemID) {
            echo '_2';
          }


          
          // custom
          $field_view = $row[$i];


          // if($TableName == "dc_booking" && $i == 15 && $field_view != ""){
          	
          // 	echo "<a href='/dc/'".$field_view.">";

          // 	//continue;
          // }

          //echo $TableName;

          // echo $TableName;
          // echo "here_";
          // echo $i;
          //echo $field_view;



          if ('rfq'==$TableName && 'amount_quote'==$arr_ColumnsToRetrieve[$i] && $field_view < 0) {
            $field_view = (-1*$field_view) . ' <span class=txt_rb>POA</span>';
          }

          if( $TableName == "dc_bookings" && $i == 15 && $field_view != ""){
          	$actual_link = "http://$_SERVER[HTTP_HOST]";
          	
          	echo '"><a target="_blank" href="'.$actual_link.'/book_uploaded_files/'.$field_view.'">'.$field_view.'</a></td>';
          	
          	//continue;
          } else {


	          if( $TableName == "dc_bookings" && $i == 16){
		      	if($field_view == '0'){
		      		$checked = "";
		      	} else {
		      		$checked = "checked";
		      	}
		      	echo '"><input data-id="'.$row[0].'" type="checkbox" name="book_confirm_check" class="book_team_confirm" value="Booked" '.$checked.'></td>';
		      } else {



		          echo '" '.(('team'==$TableName && 'ID'!=$arr_ColumnNamesToRetrieve[$i])?$onclick:'').'>' . (  ('team'==$TableName && 'ID'==$arr_ColumnNamesToRetrieve[$i])?'<input type="checkbox" name="chk_id[]" value="'.$field_view.'" /><span '.$onclick.'>':'' ) . $field_view . (('team'==$TableName && 'ID'==$arr_ColumnNamesToRetrieve[$i])?'</span>':'') . '</td>';
		       }

	      }

	      //booked checkbox
	      


        }
      if($DeletedTBLName==$TableName){	
		if(isset($_GET['p']))
			$pageNaP = '&p='.$_GET['p'];
		if(isset($_GET['id']))
			$pageNaID = '&id='.$_GET['id'];
		echo '<td class="td_3_2"><a href="' . $_getCurrentUrl . '?Deleteid=' . $row[0] . '&DeleteByID'.$pageNaP.$pageNaID.'" onclick="return confirm(&#34;Really DELETE this record?\nYou will not be able to undo this action!&#34;)">Delete</a></td>';
	  }
	echo '</tr>';
    }
	}
echo '</table>'.
// custom CSV
('rfq'==$TableName?'<br clear="all" /><a style="float:right" href="csv_export.php'.($_Kw?'?kw='.urlencode($_Kw):'').'">&laquo; Download CSV &raquo;</a>':'').

// custom CSV for new byp
// mb 20210228
('byp_Quote'==$TableName?'<br clear="all" /><a style="float:right" id="bqhp" href="byp_csv_export.php'.($_Kw?'?kw='.urlencode($_Kw):'').'">&laquo; Download CSV &raquo;</a>':'').

('design_planning'==$TableName?'<br clear="all" /><a style="float:right" href="pd_csv_export.php">&laquo; Download CSV &raquo;</a>':'').

//  custom delete
('team'==$TableName?'<br clear="all" /><input class="button" type="button" value="Delete" onclick="if (confirm(\'Are you sure you want to delete?\')) this.form.submit();" />':'').

'</td>
<td><img src="pics/blank.gif" width="10" height="10" alt="" /></td>
<td width="50%">

<table cellspacing="0" cellpadding="0" width="100%" border="0">
<tr valign="top">
<td>

<div id="div_view" class="' . $viewClass . '">
<table cellspacing="0" cellpadding="0" width="100%" border="0">
';
	for ($i = 0; $i < sizeof($arr_ModColumnsToRetrieve); $i = $i + 1) {
	
  // custom
  $field_view = $arr_ModColumnDisplayValues[$i];
  if ('rfq'==$TableName) {
    if ('property_type'==$arr_ModColumnsToRetrieve[$i]) $field_view = ucfirst($field_view);
    if ('amount_quote'==$arr_ModColumnsToRetrieve[$i]) {
      if ($field_view < 0) {
        $field_view = (-1*$field_view) . ' <span class=txt_rb>POA</span>';
      }
    }
    if ('postcode'==$arr_ModColumnsToRetrieve[$i]) {
      $field_view = '<a href="https://maps.google.com/maps?q='.urlencode($field_view).',+United+Kingdom" target="_blank">'.$field_view.'</a>';
    }
  }
  if ('statusQuo'==$arr_ModColumnsToRetrieve[$i]) {
	  
	  $getStatusQuoName= mysqli_fetch_assoc(mysqli_query($dbconn,"SELECT rfq_status_name FROM rfq_status WHERE rfq_status_id = '" . $arr_ModColumnValues[$i]."'"));
      $field_view = $getStatusQuoName['rfq_status_name'];
    }
  if('imageName'==$arr_ModColumnsToRetrieve[$i] && 'architectural_plans'==$TableName)
	 $field_view ='<a href="/images/architectural/upload/'.$arr_ModColumnValues[$i].'" target="_blank">'.$arr_ModColumnValues[$i].'</a><br/>'; 
  echo '
  <tr valign="top">
    <td class="td_2_1" width="120" style="padding:3px">' . $arr_ModColumnNamesToRetrieve[$i] . '</td>
    <td class="td_3">' . $field_view . '&nbsp;</td>
  </tr>';
	}
    if( 'byp_Quote'==$TableName){
		echo '<tr><td class="td_2_1" width="120" style="padding:3px">Images</td><td class="td_3">';
		$sqlBypImg = mysqli_query($dbconn,"SELECT imagename FROM byp_Quote_images WHERE byp_Quote_id = '" . $_REQUEST['id']."'");
	   while($bypImages = mysqli_fetch_assoc($sqlBypImg)){
		   echo '<a href="/bypAttaimage/'.$bypImages['imagename'].'">'.$bypImages['imagename'].'</a></br>';
	   }
	   echo '</td></tr>';
   }
  // custom
  if ('rfq'==$TableName && isset($a_photo) && $a_photo) {
    echo '<tr bgcolor="#F7F6F4"><td class="td_2_1" style="padding:3px">Photos</td><td class="td_3">';
    foreach ($a_photo AS $v) {
      if ($v) echo '<a href="/images/rfq/'.$arr_ModColumnDisplayValues[0].'/'.$v.'" target="_blank"><img src="/phpthumb/phpThumb.php?src='.urlencode('/images/rfq/'.$arr_ModColumnDisplayValues[0].'/'.$v).'&amp;w=220&amp;h=293" alt="/images/rfq/'.$arr_ModColumnDisplayValues[0].'/'.$v.'" /></a><br/>';
    }
    echo '</td></tr>';
  }
 /*  if ('architectural_plans'==$TableName ) {
    echo '<tr bgcolor="#F7F6F4"><td class="td_2_1" style="padding:3px">Photos</td><td class="td_3">';
    echo '<a href="/images/architectural/upload/'.$arr_ModColumnDisplayValues[8].'" target="_blank">'.$arr_ModColumnDisplayValues[8].'</a><br/>';
    echo '</td></tr>'; 
  } */
  if ($_AddNew && $ItemID > 0 && 'byp_Quote'==$TableName) {
	  $pdfNameNew = getSqlValue('SELECT pdfName FROM byp_Quote WHERE id = ' . $_REQUEST['id'], "pdfName");
	  if(strlen($pdfNameNew)>3){
		  echo '<tr><td colspan="2"><a target="_blank" href="https://www.buildteam.com/'.$pdfNameNew.'">PDF report</a></tr>';
		}
  }
  if ($_AddNew && $ItemID > 0 && 'rfq'==$TableName) {
    $pdfFile = getSqlValue('SELECT emailed_pdf FROM rfq WHERE rfq_id = ' . $_REQUEST['id'], "emailed_pdf");
    if(strlen($pdfFile)>3){
      echo '<tr><td colspan="2"><a target="_blank" href="'.$pdfFile.'">PDF report</a></tr>';
    }
	echo '
  <tr>
    <td>'.
      (('rfq'==$TableName || 'news'==$TableName || 'page'==$TableName || 'press_release'==$TableName || 'design_planning'==$TableName || 'event_photos'==$TableName)?'<input type="hidden" name="nonce" value="'.session_id().'" /><input type="submit" value="Delete" class="button" onclick="return confirm(\'Do you really want to delete this '.('rfq'==$TableName?'quote':('news'==$TableName?'news and related images':('page'==$TableName?'page and related images':'item and related files'))).' (it can not be restored)?\')" name="btn_delete" />':'').
    '</td>
    <td>
      <table border="0" cellpadding="5" cellspacing="0" width="100%">
        <tr>
          <td align="right">
					<input type="button" onclick="location.href=\'' . $_getCurrentUrl . '?id=0\'" value="Add New" class="button" /></td>
        </tr>
      </table>
    </td>
  </tr>
	';
	}

if(isset($admin_page_name)){
    if($admin_page_name == "dc_booking"){
    	


      echo '</table>
			</div>';
			echo 'Start Time:<input type="text" class="timepicker_start" />
	End Time: <input type="text" class="timepicker_end" />
	<button class="change_time">Update Time</button>';

	echo '<input style="margin-left:25px;" type="number" class="dc_date_range" value="" placeholder="Enter range" required/><button class="change_date_range">Update DateRange</button>';
		echo '<div class="dc_book_admin">
		<div class="dc_picker">
			<button class="dc_picker_prev" style="display:none;">
				<img src="/assets_b/img/datepick_left_arrow.png" alt="">
			</button>
			<div class="dc_picker_fld">
				<input type="text" id="dc_datepicker" name="_dc_date"  />
			</div>
			<button class="dc_picker_next" style="display:none;">
				<img src="/assets_b/img/datepick_right_arrow.png" alt="">
			</button>
		</div>
		<form class="dc_slots_form">
			<div class="dc_slots_container">
				<div class="dc_1 dc_box">
					
				</div>
				<div class="dc_2 dc_box">
					
				</div>
				<div class="dc_3 dc_box">
					
				</div>
			</div>
			<div class="slot_msg slot_succ">Slot updated</div>
			<div class="slot_msg slot_err" style="color: red;">Slot update error</div>
			<button type="submit" class="update_slots_btn">Update slots</button>
		</form>
	</div>';	
    }
  } else {


			echo '
		</table>
		</div>

		<div id="div_edit" class="' . $editClass . '">
		<!-- right col -->

		<table cellspacing="0" cellpadding="0" width="100%" border="0">'.
		  ($_PrimaryKeyColumn!='id'?'<input type="hidden" name="' . $_PrimaryKeyColumn . '" value="' . $ItemID . '" />':'').
			'<input type="hidden" name="id" value="' . $ItemID . '" />
			<input type="hidden" name="p" value="' . $_Page . '" />
			';
		  for ($i = 0; $i < sizeof($arr_ModColumnsToRetrieve); $i = $i + 1) {
			echo '
		  <tr valign="top">
		    <td class="td_2_1" width="120" style="padding:3px">' . $arr_ModColumnNamesToRetrieve[$i];
				if  ($arr_ModColumnTypesToRetrieve[$i] > 2 && $_AllowHTML) {
					echo '<br />&lt;HTML&gt;';
				}
				echo '
				</td>
		    <td class="td_3" nowrap>
				';
					if ($i == 0 || ($arr_ModColumnTypesToRetrieve[$i] == -1 && $ItemID == 0)) {
		        echo '[auto]';
					}
					elseif ($arr_ModColumnTypesToRetrieve[$i] == -2) {
						if ($ItemID == 0) {
							echo '<input type="hidden" name="adminid" value="' . $_SESSION['adminid'] . '" />';
						}
						else {
							echo '<input type="hidden" name="adminid" value="' . $arr_ModColumnValues[$i] . '" />';
						}
					}
					elseif ($arr_ModColumnTypesToRetrieve[$i] == -1) {
		        
		        // custom
		        $field_view = $arr_ModColumnDisplayValues[$i];
		        if ('rfq'==$TableName) {
		          if ('property_type'==$arr_ModColumnsToRetrieve[$i]) $field_view = ucfirst($field_view);
		          if ('amount_quote'==$arr_ModColumnsToRetrieve[$i]) {
		            if ($field_view < 0) {
		              $field_view = (-1*$field_view) . ' <span class=txt_rb>POA</span>';
		            }
		          }
		        }
		        
		        echo $field_view; //$arr_ModColumnDisplayValues[$i];
					}
					elseif ($arr_ModColumnTypesToRetrieve[$i] == '12R' && 'testimonials'== $TableName) {
							if($arr_ModColumnValues[$i]=='Build')
								$BuildSelec = 'checked';
							if($arr_ModColumnValues[$i]=='Design')
								$Designselec = 'checked';
						 echo '<input type="radio" name="' . $arr_ModColumnsToRetrieve[$i] . '" value="Build" '.$BuildSelec.'/>Build &nbsp;&nbsp;
						<input type="radio" name="' . $arr_ModColumnsToRetrieve[$i] . '" value="Design" '.$Designselec.' />Design';
					}
					elseif ($arr_ModColumnTypesToRetrieve[$i] == 0) {
		        echo '<input type="hidden" name="' . $arr_ModColumnsToRetrieve[$i] . '" value="" />
						<input type="checkbox" name="' . $arr_ModColumnsToRetrieve[$i] . '" value="1"';
		        if ($arr_ModColumnValues[$i] == '1') {
		          echo ' checked';
						}
						echo ' />';
					}
		      elseif ($arr_ModColumnTypesToRetrieve[$i] == 3) {
		        echo '<textarea name="' . $arr_ModColumnsToRetrieve[$i] . '" class="input" rows="3">'  . $arr_ModColumnValues[$i] . '</textarea>';
					}
		      elseif ($arr_ModColumnTypesToRetrieve[$i] == 4) {
		      	echo '<textarea name="' . $arr_ModColumnsToRetrieve[$i] . '" class="input" rows="8">'  . $arr_ModColumnValues[$i] . '</textarea>';
					}
		      elseif ($arr_ModColumnTypesToRetrieve[$i] == 5) {
		      	echo '<textarea name="' . $arr_ModColumnsToRetrieve[$i] . '" class="input" rows="40">'  . $arr_ModColumnValues[$i] . '</textarea>';
					}
		      elseif ($arr_ModColumnTypesToRetrieve[$i] == 7) {
		      	echo '<textarea name="' . $arr_ModColumnsToRetrieve[$i] . '" id="' . $arr_ModColumnsToRetrieve[$i] . '" class="input" rows="20">'  . $arr_ModColumnValues[$i] . '</textarea><script type="text/javascript">CKEDITOR.replace( "' . $arr_ModColumnsToRetrieve[$i] . '", {height:500} );</script>';
					}
					elseif ($arr_ModColumnTypesToRetrieve[$i] == 99) {
		         echo '<input type="file" name="' . $arr_ModColumnsToRetrieve[$i] . '" class="input"><br />' . $arr_ModColumnValues[$i];
					if('project_new'==$TableName && $arr_ModColumnValues[$i] !='')
					echo '<a href="javascript:warnAction(&#39;Really Delete&#39;,&#39;/admin/data_project_new.php?p='.$_GET["p"].'&amp;s='.$_GET["s"].'&amp;sd='.$_GET["sd"].'&amp;kw='.$_GET["kw"].'&amp;id='.$_GET["id"].'&amp;thmRemove=Yes&#39;)">&nbsp;&nbsp;Remove</a>';
					}
				elseif ($arr_ModColumnTypesToRetrieve[$i] == 11) {
		         echo '<input type="hidden" value="'.$arr_ModColumnValues[$i].'" name="' . $arr_ModColumnsToRetrieve[$i] . '" class="input"><br />' .'<a href="/images/architectural/upload/'.$arr_ModColumnValues[$i].'" target="_blank">'.$arr_ModColumnValues[$i].'</a><br/>';
					}
		      elseif ($arr_ModColumnsToRetrieve[$i] == 'date_release') {
		      	echo '<input type="text" id="' . $arr_ModColumnsToRetrieve[$i] . '" name="' . $arr_ModColumnsToRetrieve[$i] . '" value="' . $arr_ModColumnValues[$i] . '" class="input_sm" />';
		      }
		      elseif ($arr_ModColumnsToRetrieve[$i] == 'time_release') {
		      	echo '<input type="text" id="' . $arr_ModColumnsToRetrieve[$i] . '" name="' . $arr_ModColumnsToRetrieve[$i] . '" value="' . $arr_ModColumnValues[$i] . '" class="input" style="width:85px" size="14" maxlength="14" /> i.e. 14:00';
		      }
		      elseif ($arr_ModColumnsToRetrieve[$i] == 'floor' && 'design_planning'== $TableName) {
		      	echo '<input type="text" id="' . $arr_ModColumnsToRetrieve[$i] . '" name="' . $arr_ModColumnsToRetrieve[$i] . '" value="' . $arr_ModColumnValues[$i] . '" class="input" style="width:85px" size="14" maxlength="14" /> i.e. 47.1';
		      }
					else {
		        if ($arr_ModColumnsToRetrieve[$i] == 'borough' && $_TableName == 'design_planning') {
		          echo dboDropDown("borough", "id", "name", $arr_ModColumnValues[$i], "name", "borough", "", "", "");
		        }
				elseif ($arr_ModColumnsToRetrieve[$i] == 'protypeID' && $_TableName == 'design_planning') {
		          echo dboDropDown("dpproject_type", "id", "project_type", $arr_ModColumnValues[$i], "project_type", "protypeID", "", "", "");
		        }
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'property' && $_TableName == 'design_planning') {
		          echo arrDropDown($arr_ModColumnsToRetrieve[$i], $arr_ModColumnValues[$i], $a_property);
		        }
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'roof' && $_TableName == 'design_planning') {
		          echo arrDropDown($arr_ModColumnsToRetrieve[$i], $arr_ModColumnValues[$i], $a_roof);
		        }
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'subcategory_id' && $_TableName != 'subcategory') {
		          echo dboDropDown("subcategory", "subcategory_id", "subcategory_name", $arr_ModColumnValues[$i], "subcategory_id", "subcategory_id", "input", "ref_sub.php", "Subcategory");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'category_id') {
		          echo dboDropDown("category", "category_id", "category_name", $arr_ModColumnValues[$i], "category_id", "category_id", "input", "ref_cat.php", "Category");
				} elseif ($arr_ModColumnsToRetrieve[$i] == 'borough' && ($_TableName == 'project_new') ) {
		          echo dboDropDown("project_borough", "b_name", "b_name", $arr_ModColumnValues[$i], "b_id", "borough", "input");
				} elseif ($arr_ModColumnsToRetrieve[$i] == 'floor_area' && ($_TableName == 'project_new') ) {
					echo dboDropDown("project_floorarea", "floor_area", "floor_area", $arr_ModColumnValues[$i], "id", "floor_area", "input");
				}
				elseif ($arr_ModColumnsToRetrieve[$i] == 'project_type' && ($_TableName == 'project_new') ) {
					echo dboDropDown("project_type", "project_type", "project_type", $arr_ModColumnValues[$i], "id", "project_type", "input");
				}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'rfq_status_id') {
					echo dboDropDown("rfq_status", "rfq_status_id", "rfq_status_name", $arr_ModColumnValues[$i], "rfq_status_id", "rfq_status_id", "input_md");
				}elseif ($arr_ModColumnsToRetrieve[$i] == 'statusQuo') {
					echo dboDropDown("rfq_status", "rfq_status_id", "rfq_status_name", $arr_ModColumnValues[$i], "rfq_status_id", "statusQuo", "input_md");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'terrace_type_id') {
		          echo dboDropDown("terrace_type", "terrace_type_id", "terrace_type_name", $arr_ModColumnValues[$i], "terrace_type_name", "terrace_type_id", "input");
						}
		        elseif (substr($arr_ModColumnsToRetrieve[$i], strlen($arr_ModColumnsToRetrieve[$i]) - 13, strlen($arr_ModColumnsToRetrieve[$i])) == '_component_id') {
		          echo dboDropDown("component", "component_id", "component_name", $arr_ModColumnValues[$i], "component_id", $arr_ModColumnsToRetrieve[$i], "input");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'news_category_id' && $_TableName != 'news_category_') {
		          echo dboDropDown("news_category", "news_category_id", "news_category_name", $arr_ModColumnValues[$i], "news_category_name", "news_category_id", "input", "data_newscat.php", "News Category");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'project_category_id' && $_TableName != 'project_category') {
		          echo dboDropDown("project_category", "project_category_id", "project_category_name", $arr_ModColumnValues[$i], "project_category_name", "project_category_id", "input", "data_projectcat.php", "Project Category");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'parent_page_id') {
		          echo dboDropDown("page", "page_id", "page_name", $arr_ModColumnValues[$i], "page_name", "parent_page_id", "input", "", "", true);
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'faqcategoryid') {
		          echo dboDropDown("pd_faqcategories", "faqcategoryid", "faqcategory", $arr_ModColumnValues[$i], "faqcategory", "faqcategoryid", "input", "data_faqcategories.php", "Category");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'communityarticlecategoryid') {
		          echo dboDropDown("pd_communityarticlecategories", "communityarticlecategoryid", "communityarticlecategory", $arr_ModColumnValues[$i], "communityarticlecategory", "communityarticlecategoryid", "input", "data_communityarticlecategories.php", "Category");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'banneraccountid') {
		          echo dboDropDown("pd_banneraccounts", "banneraccountid", "company", $arr_ModColumnValues[$i], "company", "banneraccountid", "input", "data_banneraccount.php", "Account");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'bannerlocationid') {
		          echo dboDropDown("pd_bannerlocations", "bannerlocationid", "bannerlocation", $arr_ModColumnValues[$i], "bannerlocation", "bannerlocationid", "input");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'sponsoredlinkaccountid') {
		          echo dboDropDown("pd_sponsoredlinkaccounts", "sponsoredlinkaccountid", "company", $arr_ModColumnValues[$i], "company", "sponsoredlinkaccountid", "input");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'sponsoredlinktypeid') {
		          echo dboDropDown("pd_sponsoredlinktypes", "sponsoredlinktypeid", "sponsoredlinktype", $arr_ModColumnValues[$i], "sponsoredlinktype", "sponsoredlinktypeid", "input");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'CategoryId') {
		          echo dboDropDown("raymoviecategories", "raymoviecategoryid", "raymoviecategory", $arr_ModColumnValues[$i], "raymoviecategory", "CategoryId", "input", "data_communityvideocategories.php", "Category");
						}
		        elseif ($arr_ModColumnsToRetrieve[$i] == 'navid') {
		          echo dboNavDropDown($arr_ModColumnValues[$i]);
						}
						else {
		          echo '<input type="text" name="' . $arr_ModColumnsToRetrieve[$i] . '" value="' . $arr_ModColumnValues[$i] . '" class="input">';
						}
					}
				echo '
		    </td>
		  </tr>';
			}
		  if( 'byp_Quote'==$TableName){
				echo '<tr><td class="td_2_1" width="120" style="padding:3px">Images</td><td class="td_3">';
				$sqlBypImg = mysqli_query($dbconn,"SELECT imagename FROM byp_Quote_images WHERE byp_Quote_id = '" . $_REQUEST['id']."'");
			   while($bypImages = mysqli_fetch_assoc($sqlBypImg)){
				   echo '<a href="/bypAttaimage/'.$bypImages['imagename'].'">'.$bypImages['imagename'].'</a></br>';
			   }
			   echo '</td></tr>';
		   }
			echo '
		  <tr>
		    <td></td>
		    <td>
		      <table border="0" cellpadding="5" cellspacing="0" width="100%">
		        <tr>
		          <td nowrap>
							';
							if ($_AddNew || $ItemID > 0) {
							echo '
							
							<input type="submit" name="btnSubmit" value="' . $_BtnText . '" class="button" />&nbsp;<input type="reset" value="Reset" class="button" />&nbsp;
		          ';
							}
							
							if ($_AllowDelete && $ItemID > 0) {
							echo '
		          <input type="button" value="Delete" class="button_r" onclick="warnAction(\'Really DELETE this record?\nYou will not be able to undo this action!\',\'' . $_getCurrentUrl . '?del=' . $ItemID . '\')" />
							';
							}
							echo '</td>';
							if ($_AddNew && $ItemID > 0) {
		          echo '<td align="right">&nbsp;<input type="button" onclick="warnAction(\'Changes made to current record will not be save.\nContinue?\',\'' . $_getCurrentUrl . '?id=0\')" value="Add new" class="button" /></td>';
							}
						echo '
		        </tr>
		      </table>
		    </td>
		  </tr>
		</table>
		<br /><br />
		</div>
		';

}

echo $pd_navcontentarticles;

if ( strlen($pd_navcontentarticles) > 0) {
					if ($_AddNew || $ItemID > 0) {
					echo '
					
					<input type="submit" name="btnSubmit" value="' . $_BtnText . '" class="button" />&nbsp;<input type="reset" value="Reset" class="button" />&nbsp;
          ';
					}
					

}

echo '

</td>
</tr>
</table>

</form>
<div id="testdiv1" style="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></div>
<div id="testdiv2" style="position:absolute;visibility:hidden;background-color:white;layer-background-color:white;"></div>
';

require ('./inc/footer.inc.php');

// Functions area

function arrDropDown($name, $value, $a_data) {
  $buf = '<select name="'.$name.'">';
  
  foreach ($a_data AS $k => $v) {
    $buf .= '<option value="'.$k.'"'.($k==$value?' selected="selected"':'').'>'.htmlspecialchars($v).'</option>';
  }
  
  $buf .= '</select>';
  
  return $buf;
}

function getThumbFromPDF($filename) {

  $a_size = getimagesize($filename);
  $source = imagecreatefrompng($filename) or die('Error opening file '.$filename);
  
  if ($a_size[0] < $a_size[1]) { // rotate if width < height only
    $rotation = imagerotate($source, 90, imageColorAllocateAlpha($source, 255, 255, 255, 0));
  }
  else {
    $rotation = $source;
    $tmp = $a_size[0];
    $a_size[0] = $a_size[1];
    $a_size[1] = $tmp;
  }

  $basename = explode('.', $filename);
  array_pop($basename);
  $basename = implode('.', $basename);

  imagepng($rotation, $basename.'.png'); // '_r.png'
  $rotation = imagecreatefrompng($basename.'.png'); // '_r.png'
  // after rotation: $a_size[0] is height, $a_size[1] is width
  $aspect_ratio = $a_size[1] / $a_size[0];
  $new_width = 286;
  $new_height = round($new_width / $aspect_ratio);

  $thumb_img = imagecreatetruecolor($new_width, $new_height);
  imagecopyresampled($thumb_img, $rotation, 0, 0, 0, 0, $new_width, $new_height, $a_size[1], $a_size[0]);

  imagepng($thumb_img, $basename.'.png'); // '_r.png'

}

?>
