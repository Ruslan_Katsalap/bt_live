<?php

/* Config parameters */
$abspath = str_replace('\\','/',dirname(__FILE__));
$abspath .= '/';

require_once ($abspath.'config.inc.php');

$IP = $_SERVER['REMOTE_ADDR'];

$USER_AGENT = substr($_SERVER['HTTP_USER_AGENT'], 0, 244);



/* Constants */

$PHP_LONG_DATE_FORMAT = 'M d, Y g:i A';

$PHP_SHORT_DATE_FORMAT = 'n/d/Y';

$passMask = '&lt;encrypted&gt;';

$passMask2 = '<encrypted>';





/************************************************

*************** Database functions **************

*************************************************/



function dbConnect() {

	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;

	

	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass,$dbname) or die ('Error connecting to database');

	//mysql_select_db($dbname);
  
  $sql0 = "SET NAMES 'utf8'";
  @mysqli_query($dbconn,$sql0, $dbconn);

}



function dbClose() {

	global $dbconn;

	

	if (isset($dbconn)) {

		if ($dbconn != "") {

			mysql_close($dbconn);

		}

	}

}



function setRs($sql) {

	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;
	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die ('Error connecting to database');
	$statusQuery = mysqli_query($dbconn,$sql) or die ('Error running query ' .  $sql);

}





function getRs($sql) {

	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;
	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die ('Error connecting to database');
	$rs = mysqli_query($dbconn,$sql) or die ('Error running query ' .  $sql);

	return $rs;

}



function dboDropDown($_ddTable, $_ddValue, $_ddDisplay, $_ddSelected = '0', $_ddSort = '', $_ddField = '', $_ddClass = 'input_sm', $AddNewURL = '', $AltText = '', $AllowNull = false) {



	$arr_ddDisplay = explode(',', $_ddDisplay);

	$strDD = '';

	$sQuery = '';

	$i = 0;



	if (strlen($_ddField) == 0) {

		$_ddField = $_ddValue;

	}

	$sQuery =  'SELECT ' . $_ddValue . ', ' . $_ddDisplay . ' FROM ' . $_ddTable;

	if ($_ddTable != 'profiles') {

		//$sQuery .= ' WHERE is_active = 1 AND is_enabled = 1';

	}

	else {

		$sQuery .= ' WHERE FeaturedGuru = 0 AND PrimPhoto > 0';	

	}

	if (strlen($_ddSort) > 0) {

		$sQuery .= ' ORDER BY ' . $_ddSort;

	}

	

	$strDD = '<select id="id_' . $_ddField . '" name="' . $_ddField . '" class="' . $_ddClass . '">';

	if ($AllowNull == true) {

		$strDD .= '<option value="0">- none -</option>';

	}

	

	//echo $sQuery;

	$query = getRs($sQuery);// or die ('Error executing list query');

	while ($row = mysqli_fetch_assoc($query)) {

		$strDD .= '<option value="' . $row[$_ddValue] . '"';

		if ($_ddSelected == $row[$_ddValue]) {

			$strDD .= ' selected';

		}

		$strDD .= '>';

		for ($i = 0; $i < sizeof($arr_ddDisplay); $i += 1) {

			$strDD .= $row[$arr_ddDisplay[$i]];

			if ($i < sizeof($arr_ddDisplay) - 1) {

				$strDD .= ' ';

			}

		}

		$strDD .= '</option>';

	}

	$strDD .= '</select>';

	if (strlen($AddNewURL) > 0) {

		$strDD .= ' <a href="' . $AddNewURL . '"><img src="pics/icon_edit.gif" width="15" height="15" border="0" alt="Edit / Add New" /></a>';

	}



	return $strDD;

}



function dboNavDropDown($sel) {

	$ret = '<select name="navid" id="id_navid" class="input">';

	$ret .= showNav(0, 0, $sel);

	$ret .= '</select>';

	return $ret;

}



function showNav($id, $level, $sel) {

	$i = 0;

	$ret = '';

	$rs = getRs("SELECT * FROM pd_nav WHERE active = 1 AND enabled = 1 AND parentnavid = '" . $id . "' ORDER BY sort");

	while ($row = mysqli_fetch_assoc($rs)) {	

		$ret .= '<option';

		if ($sel == $row['navid']) {

			$ret .= ' selected';

		}

		$ret .= ' value="' . $row['navid'] . '">';

		if ($level == 3)

			$ret .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;&nbsp;&nbsp;';

		elseif ($level == 2)

			$ret .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;&nbsp;&nbsp;';

		elseif ($level == 1)

			$ret .= '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&rsaquo;&nbsp;&nbsp;';

		$ret .= $row['caption'] . '</option>';

		$ret .= showNav($row['navid'], $level + 1, $sel);

	}

	return $ret;

}





function getSqlValue($sql, $val) {

	$str = '';

	$arr_val = explode(',', $val);

	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;
	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die ('Error connecting to database');
	$query = mysqli_query($dbconn,$sql);// or die ('Error executing list query');

	if ($row = mysqli_fetch_assoc($query)) {

		for ($i = 0; $i < sizeof($arr_val); $i++) {

			$str .= $row[$arr_val[$i]] . ' ';

		}

	}

	return $str;

}



/************************************************

**************** Helper functions ***************

*************************************************/



function formatPassword($pass) {

	return sha1($pass);

}



function safe($posted) {

	if (!get_magic_quotes_gpc()) {

		return addslashes($posted);

	} else {

		return $posted;

	}

}
function yesNoFormat($str) {

	if ($str == 0 || $str == '') {

    return '<span class=txt_rb>No</span>';
    
	}

	else {
if($str=='16')
{
	return '<span class=txt_gb>PayPal</span>';
}else
if($str=='18')
{
	return '<span class=txt_gb>Bacs</span>';
}else
if($str=='20')
{
	return '<span class=txt_gb></span>';
}else
{
	return '<span class=txt_gb>Yes</span>';
}

	}

}



function toLink($str) {

	$v_str = strtolower(trim(substr($str, 0, 255)));

	$specialchars = array('~','!','@','#','$','%','^','&','*','(',')','_','+','=','|','}','{','"',':','?','>','<',',','.','/',';','\'','\\','`','–');

	$v_str = str_replace($specialchars, '', $v_str);

	$v_str = str_replace("  ","-",$v_str);

	$v_str = str_replace(" ","-",$v_str);
  
  $v_str = preg_replace("/\-{2,}/","-",$v_str);

	//$v_str = preg_replace('[\W]', '-', $v_str);

	//$v_str = preg_replace('[\W_]', '-', $v_str);

	/*

	$v_str = str_replace("'","",$v_str);

	$v_str = str_replace("<","",$v_str);

	$v_str = str_replace(">","",$v_str);

	$v_str = str_replace("&","",$v_str);

	$v_str = str_replace(" ","-",$v_str);

	*/

	return $v_str;

}

function formatSql($str) {
	$v_str = trim($str);
	
  //$v_str = str_replace("'","''",$v_str);
  //$v_str = str_replace("<","",$v_str);
	//$v_str = str_replace(">","",$v_str);
	//$v_str = str_replace("&","",$v_str);
	
	global $dbhost, $dbuser, $dbpass, $dbname, $dbconn;
	$dbconn = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname) or die ('Error connecting to database');
  $v_str = mysqli_real_escape_string($dbconn,$v_str);
  
  return $v_str;
}


function getUniqueFilename ($str) {

	$str_name = getFilename($str); //substr($str,0,strlen($str)-4);

	$str_ext = getExt($str);

	

	return strtolower(uniqid(toLink($str_name)) . '.' . $str_ext);

}



function getFilename($str) {

	return strtolower(preg_replace('/\.[^.]*$/', '', $str));

}



function getExt($str) {

	return strtolower(substr(strrchr($str, '.'), 1));

}



function isGif($str) {

	if (getExt($str) == 'gif')

		return true;

	else

		return false;

}



function isPng($str) {

	if (getExt($str) == 'png')

		return true;

	else

		return false;

}



function getUniqueID () {

	return uniqid();

}



function isValidEmail($email) {

	if(strlen($email) > 0 && eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) {

		return true;

	}

	else {

		return false;

	}

}



function mySqlDate($d = '') {

	if (strlen($d) > 0) {

		$arr_d = explode('/', $d);

		if (sizeof($arr_d) == 3) {

			return $arr_d[2] . "-" . $arr_d[0] . "-" . $arr_d[1];

		}

		else {

			return date("Y") . "-" . date("m") . "-" . date("d");

		}

	}

	else {

		return date("Y") . "-" . date("m") . "-" . date("d") . " " . date("H") . ":" . date("i") . ":" . date("s");

	}

}



function dateFormat($d) {

	$arr_d = explode('-', $d);

	if (sizeof($arr_d) == 3) {

		//return $arr_d[1] . '/' . $arr_d[2] . '/' . $arr_d[0];

	}

	else {

		//return '';

	}

	$php_date_format = 'M d, Y H:nn';

	$php_date_format = 'n/j/Y g:nn A';

	return date($php_date_format, $d);

}



function currency_format($amount) {

  $new_amount = sprintf("%.2f",$amount);

	return $new_amount;

}



session_start();



$goToLogin = false;



if (!defined('SkipAuth')) {

	if (isset($_SESSION['adminid'])) {

		if (strlen($_SESSION['adminid']) == 0) {

			$goToLogin = true;

		}

	}

	else {

		$goToLogin = true;

	}

}



if ($goToLogin == true) {

	header("Location: login.php");

	echo "<script>location.href='login.php';</script>";

}



// connect to db

dbConnect();

?>