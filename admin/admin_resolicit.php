<?php

require ('./inc/header.inc.php');

include '../crm/inc/init.php';

if (isset($_POST['nonce']) && session_id()==$_POST['nonce']) {
  //#20326F
  echo '<div style="color:green;background-color:white;font-weight:bold;padding:10px">Settings are saved successfully!</div>';
  
  if (isset($_POST['auto_email_on']) && $_POST['auto_email_on']) {
    $sql = "UPDATE ".DbTbl('settings')." SET value='1' WHERE id='auto_email_on'";
  }
  else {
    $sql = "UPDATE ".DbTbl('settings')." SET value='' WHERE id='auto_email_on'";
  }
  query($sql);
  
  if (isset($_POST['auto_email_msg'])) {
    $_POST['auto_email_msg'] = stripslashes($_POST['auto_email_msg']);
    $sql = "UPDATE ".DbTbl('settings')." SET value='".q($_POST['auto_email_msg'])."' WHERE id='auto_email_msg'";
    query($sql);
  }

  if (isset($_POST['auto_email_book'])) {
    $_POST['auto_email_book'] = stripslashes($_POST['auto_email_book']);
    $sql = "UPDATE ".DbTbl('settings')." SET value='".q($_POST['auto_email_book'])."' WHERE id='auto_email_book'";
    query($sql);
  }

  if (isset($_POST['byp_time_left'])) {
    $sql = "UPDATE ".DbTbl('settings')." SET value='".(int)$_POST['byp_time_left']."' WHERE id='byp_time_left'";
    query($sql);
  }
  
  if (isset($_POST['from_hour_hh']) && isset($_POST['from_hour_mm']) && isset($_POST['from_hour_ap'])) {
    $sql = "UPDATE ".DbTbl('settings')." SET value='".(int)$_POST['from_hour_hh'].':'.(int)$_POST['from_hour_mm'].':'.q($_POST['from_hour_ap'])."' WHERE id='from_hour'";
    query($sql);
  }
  
  if (isset($_POST['auto_email_subj'])) {
    $_POST['auto_email_subj'] = stripslashes($_POST['auto_email_subj']);
    $sql = "UPDATE ".DbTbl('settings')." SET value='".q($_POST['auto_email_subj'])."' WHERE id='auto_email_subj'";
    query($sql);
  }

  if (isset($_POST['auto_email_book_subj'])) {
    $_POST['auto_email_book_subj'] = stripslashes($_POST['auto_email_book_subj']);
    $sql = "UPDATE ".DbTbl('settings')." SET value='".q($_POST['auto_email_book_subj'])."' WHERE id='auto_email_book_subj'";
    query($sql);
  }

  if (isset($_POST['auto_email_subj_pp'])) {
    $_POST['auto_email_subj_pp'] = stripslashes($_POST['auto_email_subj_pp']);
    $sql = "REPLACE INTO ".DbTbl('settings')." SET id='auto_email_subj_pp', value='".q($_POST['auto_email_subj_pp'])."'";
    query($sql);
  }

  if (isset($_POST['auto_email_msg_pp'])) {
    $_POST['auto_email_msg_pp'] = stripslashes($_POST['auto_email_msg_pp']);
    $sql = "REPLACE INTO ".DbTbl('settings')." SET id='auto_email_msg_pp', value='".q($_POST['auto_email_msg_pp'])."'";
    query($sql);
  }
  
  if (isset($_POST['auto_email_from_name'])) {
    $_POST['auto_email_from_name'] = stripslashes($_POST['auto_email_from_name']);
    $sql = "REPLACE INTO ".DbTbl('settings')." SET id='auto_email_from_name', value='".q($_POST['auto_email_from_name'])."'";
    query($sql);
  }
  
  if (isset($_POST['auto_email_from_addr'])) {
    $_POST['auto_email_from_addr'] = stripslashes($_POST['auto_email_from_addr']);
    $sql = "REPLACE INTO ".DbTbl('settings')." SET id='auto_email_from_addr', value='".q($_POST['auto_email_from_addr'])."'";
    query($sql);
  }
  
  if (isset($_POST['auto_email_from_signature'])) {
    $_POST['auto_email_from_signature'] = stripslashes($_POST['auto_email_from_signature']);
    $sql = "REPLACE INTO ".DbTbl('settings')." SET id='auto_email_from_signature', value='".q($_POST['auto_email_from_signature'])."'";
    query($sql);
  }
  
  if (isset($_POST['auto_email_from_surveyor'])) {
    $_POST['auto_email_from_surveyor'] = stripslashes($_POST['auto_email_from_surveyor']);
    $sql = "REPLACE INTO ".DbTbl('settings')." SET id='auto_email_from_surveyor', value='".q($_POST['auto_email_from_surveyor'])."'";
    query($sql);
  }
  
  $path = dirname(__FILE__);
  $path = str_replace('\\', '/', $path);
  $path = explode('/', $path);
  array_pop($path);
  $path = implode('/', $path).'/pdf/';
  
  if (isset($_FILES['auto_email_attach_pp']) && $_FILES['auto_email_attach_pp']['tmp_name'] && !$_FILES['auto_email_attach_pp']['error']) {
    $filename = str_replace(' ', '_', $_FILES['auto_email_attach_pp']['name']);
    $ext = explode('.', $filename);
    $ext = strtolower(array_pop($ext));
    
    if ('php'!=$ext && move_uploaded_file($_FILES['auto_email_attach_pp']['tmp_name'], $path.$filename)) {
      $sql = "REPLACE INTO ".DbTbl('settings')." SET id='auto_email_attach_pp', value='".q($filename)."'";
      query($sql);
    }
  }
  
  if (isset($_POST['delete_attach_pp']) && $_POST['delete_attach_pp']) {
    SQL2Array("SELECT value FROM ".DbTbl('settings')." WHERE id='auto_email_attach_pp'", $v);
    if (isset($v[0]['value']) && $v[0]['value']) {
      $filename = $v[0]['value'];
      if (file_exists($path.$filename)) unlink($path.$filename); 
      $sql = "REPLACE INTO ".DbTbl('settings')." SET id='auto_email_attach_pp', value=''";
      query($sql);
    }
  }
  
}

$keys = "'auto_email_on', 'auto_email_book', 'auto_email_msg', 'byp_time_left', 'from_hour', 'auto_email_subj', 'auto_email_book_subj', 'auto_email_subj_pp', 'auto_email_msg_pp', 'auto_email_attach_pp', 'auto_email_from_name', 'auto_email_from_addr', 'auto_email_from_signature', 'auto_email_from_surveyor'";

$a_conf = array();

$a_keys = explode(',', $keys);
foreach ($a_keys AS $v) {
  $v = trim($v);
  $v = str_replace("'", "", $v);
  $a_conf[$v] = '';
}
$a_conf['from_hour_hh'] = 0;
$a_conf['from_hour_mm'] = 0;
$a_conf['from_hour_ap'] = 'am';

$sql = "SELECT id, value FROM ".DbTbl('settings')." WHERE id IN($keys)";

SQL2Array($sql, $row);
foreach ($row AS $v) {
  
  
  if ('from_hour' == $v['id']) {
    $v['value'] = explode(':', $v['value']);
    $a_conf['from_hour_hh'] = $v['value'][0];
    $a_conf['from_hour_mm'] = $v['value'][1];
    $a_conf['from_hour_ap'] = $v['value'][2];
  }
  
  $a_conf[ $v['id'] ] = $v['value'];
}

?>

<form action="admin_resolicit.php" method="post" enctype="multipart/form-data">
  <input type="hidden" name="nonce" value="<?php echo session_id() ?>" />

<h1>Re-solicitation email settings</h1>

<table width="100%">

<tr>
  <td class="td_2_1" style="width:140px;padding:3px">
    <label for="auto_email_on">Enable Re-solicitation emails<br/>for new quotes within the territory<br/>
    all emails (standard and pre-purchase) will come from the same source.
    </label>
  </td>
  <td class="td_3" style="width:500px;padding:10px">
    <input type="checkbox" name="auto_email_on" id="auto_email_on" value="1" <?php if ($a_conf['auto_email_on']) echo 'checked="checked"'; ?> />
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    <label for="byp_time_left">Dispatch time<br/>(will be sent on a business day: between Monday and Friday)</label>
  </td>
  <td class="td_3" style="padding:10px">
    <select name="byp_time_left">
        <option value="0"<?php if (0==$a_conf['byp_time_left']) echo ' selected="selected"'?>>next day (within 24 hrs)</option>
      	<option value="1"<?php if (1==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 24 hrs</option>
        <option value="2"<?php if (2==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 48 hrs</option>
        <option value="3"<?php if (3==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 72 hrs</option>
        <option value="4"<?php if (4==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 4 days</option>
        <option value="5"<?php if (5==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 5 days</option>
        <option value="6"<?php if (6==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 6 days</option>
        <option value="7"<?php if (7==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 1 week</option>
        <option value="8"<?php if (8==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 8 days</option>
        <option value="9"<?php if (9==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 9 days</option>
        <option value="10"<?php if (10==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 10 days</option>
        <option value="11"<?php if (11==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 11 days</option>
        <option value="12"<?php if (12==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 12 days</option>
        <option value="13"<?php if (13==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 13 days</option>
        <option value="14"<?php if (14==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 2 weeks</option>
        <option value="15"<?php if (15==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 15 days</option>
        <option value="16"<?php if (16==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 16 days</option>
        <option value="17"<?php if (17==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 17 days</option>
        <option value="18"<?php if (18==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 18 days</option>
        <option value="19"<?php if (19==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 19 days</option>
        <option value="20"<?php if (20==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 20 days</option>
        <option value="21"<?php if (21==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 3 weeks</option>
        <option value="22"<?php if (22==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 22 days</option>
        <option value="23"<?php if (23==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 23 days</option>
        <option value="24"<?php if (24==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 24 days</option>
        <option value="25"<?php if (25==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 25 days</option>
        <option value="26"<?php if (26==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 26 days</option>
        <option value="27"<?php if (27==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 27 days</option>
        <option value="28"<?php if (28==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 28 days</option>
        <option value="29"<?php if (29==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 29 days</option>
        <option value="30"<?php if (30==$a_conf['byp_time_left']) echo ' selected="selected"'?>>after 1 month</option>        
    </select>
  </td>
</tr>


<tr>
  <td class="td_2_1" style="padding:3px">
    Sent at
  </td>
  <td class="td_3" style="padding:10px">
    <select name="from_hour_hh">
      <option value="0"<?php if (0==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>00</option>
      <option value="1"<?php if (1==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>01</option>
      <option value="2"<?php if (2==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>02</option>
      <option value="3"<?php if (3==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>03</option>
      <option value="4"<?php if (4==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>04</option>
      <option value="5"<?php if (5==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>05</option>
      <option value="6"<?php if (6==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>06</option>
      <option value="7"<?php if (7==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>07</option>
      <option value="8"<?php if (8==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>08</option>
      <option value="9"<?php if (9==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>09</option>
      <option value="10"<?php if (10==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>10</option>
      <option value="11"<?php if (11==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>11</option>
      <option value="12"<?php if (12==$a_conf['from_hour_hh']) echo ' selected="selected"'?>>12</option>
    </select> : 
    <select name="from_hour_mm">
      <option value="0"<?php if (0==$a_conf['from_hour_mm']) echo ' selected="selected"'?>>00</option>
      <option value="30"<?php if (30==$a_conf['from_hour_mm']) echo ' selected="selected"'?>>30</option>
    </select> : 
    <select name="from_hour_ap">
      <option value="am"<?php if ('am'==$a_conf['from_hour_ap']) echo ' selected="selected"'?>>AM</option>
      <option value="pm"<?php if ('pm'==$a_conf['from_hour_ap']) echo ' selected="selected"'?>>PM</option>
    </select>
  </td>
</tr>

<tr>
  <td class="td_2_1" style="padding:3px">
    <label for="auto_email_subj">Email subject for <strong>Everyone</strong><br/>(except pre-purchase)</label>
  </td>
  <td class="td_3" style="padding:10px">
    <input type="text" name="auto_email_subj" id="auto_email_subj" class="input" value="<?php echo htmlentities($a_conf['auto_email_subj'], ENT_QUOTES, 'UTF-8', false) ?>" />
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    <label for="auto_email_msg">Email message for <strong>Everyone</strong><br/>(except pre-purchase)<br/>
    <br/>
    <span style="font-style:italic">Placeholders</span>:<br/><br/>
    [first name] - user first name<br/>
    <!--[rfq_id] - Ref ID<br/>
    [rfq_code] - Ref #<br/>-->
    [territory surveyor] - territory surveyor signature
    </label>
  </td>
  <td class="td_3" style="padding:10px">
    <textarea name="auto_email_msg" id="auto_email_msg" rows="11" cols="80" class="input"><?php echo htmlentities($a_conf['auto_email_msg'], ENT_QUOTES, 'UTF-8', $double_encode=false); ?></textarea>
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    <label for="auto_email_book_subj">Email subject for <strong>download brochure</strong></label>
  </td>
  <td class="td_3" style="padding:10px">
    <input type="text" name="auto_email_book_subj" id="auto_email_book_subj" class="input" value="<?php echo htmlentities($a_conf['auto_email_book_subj'], ENT_QUOTES, 'UTF-8', false) ?>" />
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    <label for="auto_email_book">Email message for <strong>download brochure</strong><br/>(except pre-purchase)<br/></label>
  </td>
  <td class="td_3" style="padding:10px">
    <textarea name="auto_email_book" id="auto_email_book" rows="11" cols="80" class="input"><?php echo htmlentities($a_conf['auto_email_book'], ENT_QUOTES, 'UTF-8', $double_encode=false); ?></textarea>
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    <label for="auto_email_subj_pp">Email subject for <strong>Pre-Purchase</strong> quotes</label>
  </td>
  <td class="td_3" style="padding:10px">
    <input type="text" name="auto_email_subj_pp" id="auto_email_subj_pp" class="input" value="<?php echo htmlentities($a_conf['auto_email_subj_pp'], ENT_QUOTES, 'UTF-8', false) ?>" />
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    <label for="auto_email_msg_pp">Email message for <strong>Pre-Purchase</strong> quotes<br/>
    <br/>
    <span style="font-style:italic">Placeholders</span>:<br/><br/>
    [first name] - user first name<br/>
    <!--[rfq_id] - Ref ID<br/>
    [rfq_code] - Ref #<br/>-->
    [territory surveyor] - territory surveyor signature
    </label>
  </td>
  <td class="td_3" style="padding:10px">
    <textarea name="auto_email_msg_pp" id="auto_email_msg_pp" rows="11" cols="80" class="input"><?php echo htmlentities($a_conf['auto_email_msg_pp'], ENT_QUOTES, 'UTF-8', $double_encode=false); ?></textarea>
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    Email Attachment for <strong>Pre-Purchase</strong> quotes
  </td>
  <td class="td_3" style="padding:10px">
    <input type="file" name="auto_email_attach_pp" />
    <?php 
    if ($a_conf['auto_email_attach_pp']) {
      echo '<a href="/pdf/'.$a_conf['auto_email_attach_pp'].'" target="_blank">'.$a_conf['auto_email_attach_pp'].'</a> <label><input type="checkbox" name="delete_attach_pp" value="1" /> Delete</label>';
    }
    ?>
  </td>
</tr>

<tr>
  <td class="td_2_1" style="padding:3px">
    Default From Name
  </td>
  <td class="td_3" style="padding:10px">
    <input type="text" name="auto_email_from_name" class="input" value="<?php echo htmlentities($a_conf['auto_email_from_name'], ENT_QUOTES, 'UTF-8', $double_encode=false); ?>" />
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    Default From Email Address
  </td>
  <td class="td_3" style="padding:10px">
    <input type="text" name="auto_email_from_addr" class="input" value="<?php echo htmlentities($a_conf['auto_email_from_addr'], ENT_QUOTES, 'UTF-8', $double_encode=false); ?>" />
  </td>
</tr>
<tr>
  <td class="td_2_1" style="padding:3px">
    <label for="auto_email_from_signature">Default Email Signature<br/>
    <br/>
    <span style="font-style:italic"></span>
    </label>
  </td>
  <td class="td_3" style="padding:10px">
    <textarea name="auto_email_from_signature" id="auto_email_from_signature" rows="11" cols="80" class="input"><?php echo htmlentities($a_conf['auto_email_from_signature'], ENT_QUOTES, 'UTF-8', $double_encode=false); ?></textarea>
  </td>
</tr>

<tr>
  <td class="td_2_1" style="padding:3px">
    Use Default From Name, Email &amp; Signature or a territory surveyor's info (assigned in <a href="/crm/adm/index.php?p=user" target="_blank">CRM</a>), so all emails (standard and pre-purchase) will come from the same source.
  </td>
  <td class="td_3" style="padding:10px">
    <input type="radio" name="auto_email_from_surveyor" id="auto_email_from_default" value="default" <?php if ('default'==$a_conf['auto_email_from_surveyor']) echo 'checked="checked"' ?> /> <label for="auto_email_from_default">Default</label>
    <input type="radio" name="auto_email_from_surveyor" id="auto_email_from_territory" value="territory" <?php if ('territory'==$a_conf['auto_email_from_surveyor']) echo 'checked="checked"' ?> /> <label for="auto_email_from_territory">Territory Surveyor</label> (adjust in <a href="/crm/adm/index.php?p=user" target="_blank">CRM</a> at first)
  </td>
</tr>


</table>

<br /><br /><input type="submit" value="Save Changes" class="button" />

</form>

<script type="text/javascript">
  CKEDITOR.replace( 'auto_email_msg', {height:300} );
  CKEDITOR.replace( 'auto_email_msg_pp', {height:300} );
  CKEDITOR.replace( 'auto_email_from_signature', {height:300} );
  CKEDITOR.replace( 'auto_email_book', {height:300} );
</script>

<?php

require ('./inc/footer.inc.php');

?>