<?php

$PageTitle = "News";
$TableName = "news";
$PrimaryKey = "news_id";
$FieldNames = "news_id,news_category_id,title,date_release,is_enabled";
$DisplayNames = "ID,Category,Title,Release Date,Enabled";
$ModFieldNames = "news_id,news_category_id,title,news_code,description,date_release,imagefile,smallImage,sort,is_enabled";
$ModDisplayNames = "ID,Category,Title,Code,Description,Release Date,Image,Small Image,Sort,Enabled";
$ModFieldTypes = "-1,1,2,-1,7,2,99,99,1,0";
$AllowDelete = true;

if ('POST'==$_SERVER['REQUEST_METHOD']) {
  require_once ('./inc/util.inc.php');
  if (isset($_POST['btn_delete']) && $_POST['btn_delete'] && isset($_POST['news_id']) && $_POST['news_id'] && isset($_POST['nonce']) && session_id()==$_POST['nonce']) {

    $rs = getRs("SELECT news_id, imagefile FROM news WHERE news_id = ".(int)$_POST['news_id']);
    
    $row = mysqli_fetch_assoc($rs);
    
    if ($row && isset($row['news_id'])) {
      
      $path = str_replace('\\', '/', dirname(__FILE__));
      
      $path = explode('/', $path);
      array_pop($path);
      $path = implode('/', $path);
      
      //echo $path.'<br/>';
      
      if ($row['imagefile']) unlink($path.'/media/'.$row['imagefile']);
      
      setRs('DELETE FROM news WHERE news_id='.$row['news_id']);
      
      //print_r($row);
      //exit;
      
    }
    header('Location: data_news.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;
  }
}

require ('./inc/tbl.inc.php');

?>