<?php

$PageTitle = "Subcategory";
$TableName = "subcategory";
$PrimaryKey = "subcategory_id";
$FieldNames = "subcategory_id,subcategory_name,is_enabled";
$DisplayNames = "ID,Subcategory,Enabled";
$ModFieldNames = "subcategory_id,category_id,subcategory_name,subcategory_code,description,image,cost_base,cost_extra,is_enabled";
$ModDisplayNames = "ID,Category,Subcategory,Code,Description,Image,Base Cost,Extra Cost,Enabled";
$ModFieldTypes = "-1,1,2,2,3,2,1,1,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>