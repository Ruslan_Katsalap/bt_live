<?php

$PageTitle = "CGI";
$TableName = "cgi";
$PrimaryKey = "cgi_id";
$FieldNames = "cgi_id,image,is_enabled";
$DisplayNames = "ID,Image,Enabled";
$ModFieldNames = "cgi_id,subcategory_1_component_id,subcategory_2_component_id,subcategory_3_component_id,subcategory_4_component_id,subcategory_5_component_id,subcategory_6_component_id,image,is_enabled";
$ModDisplayNames = "ID,Doors,Roof,Wall,Floor,Kitchen,Heating,Image,Enabled";
$ModFieldTypes = "-1,1,1,1,1,1,1,99,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>