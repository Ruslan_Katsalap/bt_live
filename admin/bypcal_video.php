<?php

$PageTitle = "BYP Calculator Final Result Video's";
$TableName = "bypcal_videos";
$PrimaryKey = "id";
$FieldNames = "id,property_id,house_type_id,extension_type_id,roof_type_id,roof_lights_id,door_type_id,remove_chimney,underfloor_heating,ad_wc,video";
$DisplayNames = "ID,About Property,House Type,Extension Type,Roof Type,Roof Lights,Door Type,Remove Chimney,Underfloor Heating,Ad W/C, Video";
$ModFieldNames = "id,property_id,house_type_id,extension_type_id,roof_type_id,roof_lights_id,door_type_id,remove_chimney,underfloor_heating,ad_wc,video";
$ModDisplayNames = "ID,About Property,House Type,Extension Type,Roof Type,Roof Lights,Door Type,Remove Chimney,Underfloor Heating,Ad W/C, Video";
$ModFieldTypes = "-1,2,1,1,1,1,1,0,0,0,99";
$AllowDelete = false;

require ('./inc/tbl.inc_bypcal_video.php');

?>