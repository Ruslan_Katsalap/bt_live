<?php

$PageTitle = "Project Categories";
$TableName = "project_borough";
$PrimaryKey = "b_id";
$FieldNames = "b_id,b_name";
$DisplayNames = "ID,Name";
$ModFieldNames = "b_id,b_name";
$ModDisplayNames = "ID,Name";
$ModFieldTypes = "-1,2";
$AllowDelete = true;

require ('./inc/tbl.inc.php');

?>