<?php

$PageTitle = "Admins";
$TableName = "admin";
$PrimaryKey = "admin_id";
$FieldNames = "admin_id,firstname,lastname,email,is_enabled";
$DisplayNames = "ID,First name,Last name,E-mail,Enabled";
$ModFieldNames = "admin_id,username,password,firstname,lastname,email,is_enabled";
$ModDisplayNames = "ID,Username,Password,Firstname,Lastname,E-mail,Enabled";
$ModFieldTypes = "-1,2,2,2,2,2,0";

require ('./inc/tbl.inc.php');

?>