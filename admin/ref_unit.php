<?php

$PageTitle = "Units";
$TableName = "measure_unit";
$PrimaryKey = "measure_unit_id";
$FieldNames = "measure_unit_id,name,abbreviation,is_default";
$DisplayNames = "ID,Name,Abbr,Default";
$ModFieldNames = "measure_unit_id,name,conversion_factor,abbreviation,min_dimension,max_dimension,dimension_increment,default_length,default_width,is_default,is_enabled";
$ModDisplayNames = "ID,Name,Conversion Factor,Abbr,Min Dimension,Max Dimension,Dimension Increment,Default Length,Default Width,Default Selection,Enabled";
$ModFieldTypes = "-1,2,1,2,1,1,1,1,1,0,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>