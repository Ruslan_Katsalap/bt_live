<?php

$PageTitle = "Testimonials";
$TableName = "testimonial";
$PrimaryKey = "testimonial_id";
$FieldNames = "testimonial_id,name,title,is_enabled";
$DisplayNames = "ID,Name,Title,Enabled";
$ModFieldNames = "testimonial_id,name,title,quote,sort,is_enabled";
$ModDisplayNames = "ID,Name,Title / Company,Quote,Sort,Enabled";
$ModFieldTypes = "-1,2,2,4,1,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>