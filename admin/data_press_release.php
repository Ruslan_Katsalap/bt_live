<?php

$PageTitle = "Press Releases";
$TableName = "press_release";
$PrimaryKey = "id";
$FieldNames = "id,title,date_release,time_release,is_enabled";
$DisplayNames = "ID,Title,Release Date, Time,Enabled";
$ModFieldNames = "id,title,url_code,date_release,time_release,body,imagefile,pdffile,is_enabled";
$ModDisplayNames = "ID,Title,Code,Release Date,Time,Body,Thumbnail image,PDF file,Enabled";
$ModFieldTypes = "-1,2,-1,2,2,7,99,99,0";
$AllowDelete = true; // delete manually
$delid = $_REQUEST['del'];

if($delid > 0)
{
	require_once ('./inc/util.inc.php');
	setRs('DELETE FROM press_release WHERE id='.(int)$delid);
	//echo $delid;
	 header('Location: data_press_release.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;
}





if ('POST'==$_SERVER['REQUEST_METHOD']) {
  //require_once ('./inc/util.inc.php');
  
  if (isset($_POST['btn_delete']) && $_POST['btn_delete'] && isset($_POST['id']) && $_POST['id'] && isset($_POST['nonce']) && session_id()==$_POST['nonce']) {
  
	/*
    $row = mysqli_fetch_assoc($rs);
    
    if ($row && isset($row['id'])) {
      
      $path = str_replace('\\', '/', dirname(__FILE__));
      
      $path = explode('/', $path);
      array_pop($path);
      $path = implode('/', $path);
      
      //echo $path.'<br/>';
      
      if ($row['imagefile']) unlink($path.'/media/'.$row['imagefile']);
      if ($row['pdffile']) unlink($path.'/pdf/'.$row['pdffile']);
      
      setRs('DELETE FROM press_release WHERE id='.$row['id']);
      
      //print_r($row);#debug
      //exit;#debug
      
    }
	*/
	
    header('Location: data_press_release.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;
  }
}

require ('./inc/tbl.inc.php');

?>