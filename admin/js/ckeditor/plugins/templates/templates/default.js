/*
Copyright (c) 2003-2012, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.addTemplates('default', {
        imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath('templates') + 'templates/images/'),
        templates: [{
                title: 'Job Post Template',
                image: 'template1.gif',
                description: 'One Description Content and one PDF File Link.',
                html: '<p><span>&pound;20,000 to &pound;25,000</span></p><p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore dicta sunt explicabo.</p><a class="out-team-learn-more-btn" target="_blank" href="#">LEARN MORE</a>'
        },{
                title: 'Image and Title',
                image: 'template1.gif',
                description: 'One main image with a title and text that surround the image.',
                html: '<h3><img style="margin-right: 10px" height="100" width="100" align="left"/>Type the title here</h3><p>Type the text here</p>'
        }, {
                title: 'Strange Template',
                image: 'template2.gif',
                description: 'A template that defines two colums, each one with a title, and some text.',
                html: '<table cellspacing="0" cellpadding="0" style="width:100%" border="0"><tr><td style="width:50%"><h3>Title 1</h3></td><td></td><td style="width:50%"><h3>Title 2</h3></td></tr><tr><td>Text 1</td><td></td><td>Text 2</td></tr></table><p>More text goes here.</p>'
        }]
});