<?php

$PageTitle = "Requested Calls";

$TableName = "request_calls";

$PrimaryKey = "id";

$FieldNames = "id,dates,full_name,number,enquiry,requestSubscribe,preffred_day,preffred_time";

$DisplayNames = "ID,Date,Full Name, Contact Number, Enquiry, Subscribe, Day, Time";

$ModFieldNames = "id,dates,full_name,number,enquiry,requestSubscribe,preffred_day,preffred_time";

$ModDisplayNames = "ID,Date,Full Name, Contact Number, Enquiry, Subscribe, Day, Time";

$ModFieldTypes = "-1,2,2,2,2,2,2,2";

$AllowDelete = false;

$AllowAdd = false;

//require_once ('./inc/util.inc.php');

$DeletedTBLName = "request_calls";
if(isset($_GET['DeleteByID']) && $_GET['Deleteid']!=''){
	require_once ('./inc/util.inc.php');
	setRs("DELETE FROM $TableName WHERE $PrimaryKey='{$_GET['Deleteid']}'");
}

require ('./inc/tbl.inc.php');

?>