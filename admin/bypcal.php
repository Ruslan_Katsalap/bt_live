<?php

$PageTitle = "List of BYP Quotes";
$TableName = "byp_calculator";
$PrimaryKey = "id";
$FieldNames = "id,house_type,extension_type,full_length,depth,skylight,door_type,fname,sname,contact_number,postcode,address2,comments,current_status,start_design";
$DisplayNames = "ID, House Type, Extension Type, Length, Depth, Skylight, Door Type, First Name, Surname, Contact Number,Postcode,Address,Comments,When you are looking to start the Design Phase , What is your current status";

require ('./inc/tbl.inc_house.php');

?>