<?php

$PageTitle = "Quote Requests";
$TableName = "rfq";
$PrimaryKey = "rfq_id";
$FieldNames = "rfq_id,rfq_code,postcode,rfq_status_id,name,email,amount_quote,date_created";
$DisplayNames = "ID,Quote Ref,Postcode,Status,Name,E-mail,Total,Created";
$ModFieldNames = "rfq_id,rfq_code,rfq_status_id,property_type,room_length,room_width,measure_unit_id,room_size,use_room_size_estimator,subcategory_1_component_id,subcategory_2_component_id,subcategory_3_component_id,subcategory_4_component_id,subcategory_5_component_id,subcategory_6_component_id,amount_quote,quote_type_id,name,address,postcode,phone,email,message,is_email_quote,is_mailing_list,contact_method_id,date_created,date_modified,ipaddress,ip_quotes,useragent,cover,start,status";

// subcategory_1_cost,subcategory_2_cost,subcategory_3_cost,subcategory_4_cost,subcategory_5_cost,subcategory_6_cost,subcategory_7_cost,subcategory_8_cost,subcategory_9_cost,subcategory_10_cost,subcategory_11_cost,

$ModDisplayNames = "ID,Quote Ref,Status,Property Type,Room Length,Room Width,Unit Measure,Room Size,Used Estimator,Doors,Roof,Wall,Ground Floor WC,Chimney Breast,Heating,Total,Quote Type,Name,Address,Postal Code,Phone,E-mail,Message,E-mail Quote,Mailing List,Contact Method,Created,Last Modified,Client IP,Quotes from IP,User Agent,Territory,When Start,Current Status";

// Doors Cost,Roof Cost,Wall Cost,Ground Floor WC Cost,Chimney Breast Cost,Heating,Labour,Materials,Plant,Waste,Misc,

$ModFieldTypes = "-1,-1,1,-1,-1,-1,-1,-1,0,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,-1,0,0,-1,-1,-1,-1,-1,-1";
$AllowDelete = false;

$SearchFields = "postcode";

if ('POST'==$_SERVER['REQUEST_METHOD']) {
  require_once ('./inc/util.inc.php');
  if (isset($_POST['btn_delete']) && $_POST['btn_delete'] && isset($_POST['rfq_id']) && $_POST['rfq_id'] && isset($_POST['nonce']) && session_id()==$_POST['nonce']) {
    
    $rs = getRs("SELECT rfq_id, photo, photo2, photo3, photo4, photo5 FROM rfq WHERE rfq_id = ".(int)$_POST['rfq_id']);

    $row = mysqli_fetch_assoc($rs);
    
    if ($row && isset($row['rfq_id'])) {
      
      $path = str_replace('\\', '/', dirname(__FILE__));
      
      $path = explode('/', $path);
      array_pop($path);
      $path = implode('/', $path);
      
      //echo $path.'<br/>';
      
      $path = $path.'/images/rfq/'.$row['rfq_id'];
      
      if ($row['photo']) @unlink($path.'/'.$row['photo']);
      if ($row['photo2']) @unlink($path.'/'.$row['photo2']);
      if ($row['photo3']) @unlink($path.'/'.$row['photo3']);
      if ($row['photo4']) @unlink($path.'/'.$row['photo4']);
      if ($row['photo5']) @unlink($path.'/'.$row['photo5']);
      
      if (file_exists($path)) rmdir($path);
      
      setRs('DELETE FROM rfq WHERE rfq_id='.$row['rfq_id']);
      
      //print_r($row);
      //exit;
      
    }
    header('Location: rfq_quotes.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;    
  }
}

require ('./inc/tbl.inc.php');

?>