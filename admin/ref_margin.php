<?php

require ('./inc/header.inc.php');

$rs = getRs("SELECT s.subcategory_id, s.subcategory_name, m.size_low, m.size_high, m.percent_margin FROM margin m INNER JOIN subcategory s ON s.subcategory_id = m.subcategory_id ORDER BY s.subcategory_id, m.size_low");

$ret = '<table class="t_margin"><tr><th>TOTAL SQM</th><th>&lt; 10</th><th>&lt; 15</th><th>&lt; 20</th><th>&lt; 25</th><th>&lt; 30</th><th>&lt; 35</th><th>&lt; 40</th><th>&gt 40</th></tr>';
$s_id = 0;
while ($row = mysqli_fetch_assoc($rs)) {
	if ($s_id != $row['subcategory_id']) {
		$s_id = $row['subcategory_id'];
		$ret .= '</tr><tr><th>' . $row['subcategory_name'] . '</th>';
	}
	$ret .= '<td>' . $row['percent_margin'] . '</td>';
}
$ret .= '</table>';

echo $ret;

require ('./inc/footer.inc.php');

?>