<?php

$PageTitle = "Q&A";
$TableName = "faq";
$PrimaryKey = "id";
$FieldNames = "id,cat,title,weight";
$DisplayNames = "ID,Cat,Question,Sort";
$ModFieldNames = "id,cat,title,body,weight";
$ModDisplayNames = "ID,Category,Question,Answer,Sort Order";
$ModFieldTypes = "-1,2,2,7,2";
$AllowDelete = true; // delete manually

if ('POST'==$_SERVER['REQUEST_METHOD']) {
  require_once ('./inc/util.inc.php');
  
  if (isset($_POST['btn_delete']) && $_POST['btn_delete'] && isset($_POST['id']) && $_POST['id'] && isset($_POST['nonce']) && session_id()==$_POST['nonce']) {      
    
    setRs('DELETE FROM faq WHERE id='.(int)$_POST['id']);
    
    header('Location: data_faq.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;
  }
}

require ('./inc/tbl.inc.php');

?>