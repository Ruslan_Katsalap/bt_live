<?php

$PageTitle = "Team Members";
$TableName = "team";
$PrimaryKey = "team_id";
$FieldNames = "team_id,name,title,sort,is_enabled";
$DisplayNames = "ID,Name,Title,Sort,Enabled";
$ModFieldNames = "team_id,name,team_code,team_design,team_build,title,description,linkedin,fav_bu,imagefile_sm,imagefile_lg,sort,is_enabled";
$ModDisplayNames = "ID,Name,Code,Design Team,Build Team,Title,Description,LinkedIn,Favourite Building,Thumbnail,Image,Sort,Enabled";
$ModFieldTypes = "-1,2,-1,0,0,1,4,4,99,99,1,0";
$AllowDelete = true;

if ('POST'==$_SERVER['REQUEST_METHOD']) {
  
  require_once ('./inc/util.inc.php');
  
  if (isset($_POST['chk_id']) && is_array($_POST['chk_id']) && $_POST['chk_id']) {
    
    //echo '<pre>'.print_r($_POST,1).'</pre>';#debug
    
    foreach ($_POST['chk_id'] AS $k => $v) {
      $_POST['chk_id'][$k] = (int)$v;
    }
    
    
    $rs = getRs("SELECT team_id, imagefile_sm, imagefile_lg FROM team WHERE team_id IN(".implode(',', $_POST['chk_id']).")");
    
    while ($row = mysqli_fetch_assoc($rs)) {
      
      if ($row && isset($row['team_id'])) {
        
        $path = str_replace('\\', '/', dirname(__FILE__));
        
        $path = explode('/', $path);
        array_pop($path);
        $path = implode('/', $path);
        
        //echo $path.'<br/>';#debug
        
        $path = $path.'/team/';
        
        if ($row['imagefile_sm']) @unlink($path.$row['imagefile_sm']);
        if ($row['imagefile_lg']) @unlink($path.$row['imagefile_lg']);
        
        setRs('DELETE FROM team WHERE team_id='.$row['team_id']);
        
        //print_r($row);#debug
        
      }
      
    }
    
    //exit;#debug
    
    header('Location: data_team.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;    
  }
}


require ('./inc/tbl.inc.php');

?>