<?php

$PageTitle = "Components";
$TableName = "component_new";
$PrimaryKey = "component_id";
$FieldNames = "component_id,component_name,cost_base,is_default";
$DisplayNames = "ID,Component,Base Cost,Default";
$ModFieldNames = "component_id,subcategory_id,component_name,description,image,cost_base,is_default,is_enabled";
$ModDisplayNames = "ID,Subcategory,Component,Description,Image,Base Cost,Default,Enabled";
$ModFieldTypes = "-1,1,2,3,99,1,0,0";
$AllowDelete = true;//false;

require ('./inc/tbl.inc.php');

global $ItemID;

if ('POST'==$_SERVER['REQUEST_METHOD']) {
  //echo '<pre>'.print_r($_POST,1).'</pre>';
  if (isset($_FILES['image'])) {
    
    $component_id = (isset($_POST['component_id']) && $_POST['component_id'])?(int)$_POST['component_id']:$ItemID;
    
    $rs = getRs("SELECT image FROM component WHERE component_id=".$component_id);
    $row = mysqli_fetch_assoc($rs);
    if ($row && isset($row['image'])) {
      
      $a_size = getimagesize( $ROOT_FOLDER . 'byp/' .$row['image'] );
      setRs('UPDATE ' . $TableName . ' SET image_w='.(int)$a_size[0].', image_h='.(int)$a_size[1].' WHERE component_id='.$component_id);
      
    }
    
  }
}

?>