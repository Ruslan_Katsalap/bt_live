<?php



require ('./inc/header.inc.php');

include ('./map.php');

$rs = getRs("SELECT * FROM setting WHERE setting_id = 1");
$row = mysqli_fetch_assoc($rs);

if (isset($_POST['base_pricing'])) {

	if (isset($_POST['is_notify_admin'])) {

    $is_notify_admin = "1";

	}

	else {

		$is_notify_admin = 0;

	}
  
  $_POST['flat_pricing']=(int)$_POST['flat_pricing'];
  if ($_POST['flat_pricing']<0) $_POST['flat_pricing'] = 100;
  
  $path = str_replace('\\', '/', dirname(__FILE__));
  $path = explode('/', $path);
  $admin = array_pop($path);
  define('ABSPATH', implode('/', $path).'/');
  
  $path = ABSPATH.'slideshow/';
  $a_dir = opendir($path);
  
  $carousel_images = array();
  if (isset($_POST['carousel_images']) && is_array($_POST['carousel_images'])) $carousel_images = $_POST['carousel_images'];
  
  foreach ($carousel_images AS $k => $v) {
    if (isset($_POST['delete_carousel_images'][$k])) {
      unset($carousel_images[$k]);
      @unlink(ABSPATH.'slideshow/'.$k);
    }
  }
  
  if (isset($_FILES['slide_image']) && isset($_POST['slide_image_sort']) && isset($_FILES['slide_image']['name']) && $_FILES['slide_image']['name']) {
    $uploadfile = ABSPATH.'slideshow/'.$_FILES['slide_image']['name'];
    if (move_uploaded_file($_FILES['slide_image']['tmp_name'], $uploadfile)) {
      $carousel_images[ $_FILES['slide_image']['name'] ] = (int)$_POST['slide_image_sort'];
    }
  }
  
  asort($carousel_images);
  
  //sort($carousel_images);
  reset($carousel_images);
  
  $carousel_images = array_flip($carousel_images);
  
  if (!isset($_POST['map_postcodes'])) $_POST['map_postcodes'] = array();
  
  $map_filename = 'london_postcodes_'.date('Y-m-d_H-i-s').'.png';
  $fp = fopen(ABSPATH.'images/'.$map_filename, 'wb');
  ob_start();
  drawMap($_POST['map_postcodes']);
  $out = ob_get_contents();
  ob_end_clean();
  fwrite($fp, $out);
  fclose($fp);
  @unlink(ABSPATH.'images/'.$row['map_filename']); // remove previous file
  
  //die(print_r($carousel_images,1));#debug
  
  if (is_array($_POST['map_postcodes'])) $_POST['map_postcodes'] = implode(',', $_POST['map_postcodes']);
  
  $territory = array();
  if ($row['territory_options']) $territory = unserialize($row['territory_options']);
  if (isset($_POST['territory_email'])) $territory[0]['email'] = $_POST['territory_email'];
  if (isset($_POST['territory_postcodes'])) $territory[0]['postcodes'] = $_POST['territory_postcodes'];
  if (isset($_POST['territory_pdf_signature'])) $territory[0]['pdf_signature'] = $_POST['territory_pdf_signature'];
  
  if (isset($_POST['territory_new_name'])) $_POST['territory_new_name'] = trim($_POST['territory_new_name']);
  else $_POST['territory_new_name'] = '';
  
  if ($_POST['territory_new_name']) {
    $territory[] = array(
      'name' => $_POST['territory_new_name'],
      'email' => $_POST['territory_new_email'],
      'postcodes' => $_POST['territory_new_postcodes'],
      'pdf_signature' => $_POST['territory_new_pdf_signature'],
      'notification_message' => $_POST['territory_new_notification_message'],
      'poa_message' => $_POST['territory_new_poa_message'],
    );
  }
  
  if (isset($_POST['territory']) && $_POST['territory']) {
    //echo '<pre>!!!'.print_r($territory, 1)."\n\n".print_r($_POST['territory'], 1).'</pre>';#debug
    //exit;
    
    $i = 1;
    foreach ($_POST['territory'] AS $k => $v) {
      if (trim($v['name'])) {
        $territory[$i] = $v;
        $i++;
      }
      else unset($territory[$k]);
    }
    
  }
  
  mysqli_query($dbconn,"SET NAMES 'utf8'");
  
	$sql = "UPDATE setting SET ".
  "base_pricing ='".mysql_real_escape_string($_POST['base_pricing'])."' , ".
  "base_areas ='".mysql_real_escape_string($_POST['base_areas'])."' , ".
  "design_cost = '".(float)$_POST['design_cost']."', ". // , discount_rate = '{$_POST['discount_rate']}' - commented
  "design_discount = '".(float)$_POST['design_discount']."', ".
  "offer_exp_days = '".mysql_real_escape_string($_POST['offer_exp_days'])."' , ".
  "duration_pricing ='".mysql_real_escape_string($_POST['duration_pricing'])."' , ".
  "duration_weeks ='".mysql_real_escape_string($_POST['duration_weeks'])."' , ".
  "postcode_pricing ='".mysql_real_escape_string($_POST['postcode_pricing'])."' , ".
  "flat_pricing ='".$_POST['flat_pricing']."' , ".
  "min_quote = '{$_POST['min_quote']}', max_quote = '{$_POST['max_quote']}', url_costing = '{$_POST['url_costing']}', url_details = '{$_POST['url_details']}', url_images = '{$_POST['url_images']}', notification_message = '".$_POST['notification_message']."', notification_message_homeowner = '".$_POST['notification_message_homeowner']."', notification_message_prepurchase = '".$_POST['notification_message_prepurchase']."', poa_message='".mysql_real_escape_string($_POST['poa_message'])."', admin_email = '{$_POST['admin_email']}', is_notify_admin = '{$is_notify_admin}', carousel_images='".mysql_real_escape_string(serialize($carousel_images))."', date_modified = '" . time() . "', map_postcodes='".mysql_real_escape_string($_POST['map_postcodes'])."', map_filename='".$map_filename."', territory_options='".mysql_real_escape_string(serialize($territory))."' WHERE setting_id = 1";
  
  //echo $sql;
  
  setRs($sql);
  
  $banner_fee = (float)$_POST['design_cost'] - (float)$_POST['design_discount'];
  $banner_date = $_POST['offer_exp_days'];
  $buf = 'var banner_a1={fee:"'.number_format($banner_fee).'",date:"'.$banner_date.'"}';
  file_put_contents(ABSPATH.'json/banner_a1.js', $buf);
  
  $rs = getRs("SELECT * FROM setting WHERE setting_id = 1");
  $row = mysqli_fetch_assoc($rs);
}

$territory = array();
if ($row['territory_options']) $territory = unserialize($row['territory_options']);




$ret = '<form action="admin.php?id=1" method="post" enctype="multipart/form-data">';

$ret .= "<h1>Settings</h1>";

$ret .= "<table width=\"100%\">";

if ($row) {

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:140px;padding:3px\">Base Areas (sqm ; separated)</td><td class=\"td_3\" style=\"width:500px;padding:10px\"><input type=\"text\" name=\"base_areas\" value=\"" . $row['base_areas'] . "\" class=\"input\" /></td></tr>";
  
	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Base Pricing (&pound;/m2 ; separated)</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"base_pricing\" value=\"" . $row['base_pricing'] . "\" class=\"input\" /></td></tr>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Min Quote Amount</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"min_quote\" value=\"" . $row['min_quote'] . "\" class=\"input_md\" /></td></tr>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Max Quote Amount</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"max_quote\" value=\"" . $row['max_quote'] . "\" class=\"input_md\" /></td></tr>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Costing Url</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"url_costing\" value=\"" . $row['url_costing'] . "\" class=\"input\" /></td></tr>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Details Url</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"url_details\" value=\"" . $row['url_details'] . "\" class=\"input\" /></td></tr>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Images Url</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"url_images\" value=\"" . $row['url_images'] . "\" class=\"input\" /></td></tr>";

	//$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Discount Rate (%)</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"discount_rate\" value=\"" . $row['discount_rate'] . "\" class=\"input_sm\" /></td></tr>";
  
  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Design Cost Standard</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"design_cost\" value=\"" . $row['design_cost'] . "\" class=\"input_sm\" /></td></tr>";

  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Design Cost Discount</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"design_discount\" value=\"" . $row['design_discount'] . "\" class=\"input_sm\" /></td></tr>";
  
  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Offer Expiry (date)</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"offer_exp_days\" value=\"" . $row['offer_exp_days'] . "\" class=\"input_sm\" /></td></tr>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Duration Pricing (; separated)</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"duration_pricing\" value=\"" . $row['duration_pricing'] . "\" class=\"input\" /></td></tr>";
  
	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Duration Weeks (; separated)</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"duration_weeks\" value=\"" . $row['duration_weeks'] . "\" class=\"input\" /></td></tr>";
  
  $ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">Postcode Pricing (%)<br/><br/>line format:<br/><strong>PostcodePart &nbsp; Digits</strong><br/><br/>example:<br/><strong>SE22 90</strong></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><textarea name=\"postcode_pricing\" rows=\"10\" class=\"input\">" . $row['postcode_pricing'] . "</textarea></td></tr>";
  
  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Flat Pricing (%)</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"flat_pricing\" value=\"" . $row['flat_pricing'] . "\" class=\"input_sm\" /></td></tr>";
  
	$ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">Cover text of Quote accompanying email<hr/>PDF end notes (i.e. FAQ)<br />General</td><td class=\"td_3\" style=\"padding:10px\"><textarea name=\"notification_message\" rows=\"10\" class=\"input\">" . $row['notification_message'] . "</textarea><script type=\"text/javascript\">CKEDITOR.replace( 'notification_message', {height:500} );</script></td></tr>";
  
  $ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">Cover text of Quote accompanying email<hr/>PDF end notes (i.e. FAQ)<br />Homeowners</td><td class=\"td_3\" style=\"padding:10px\"><textarea name=\"notification_message_homeowner\" rows=\"10\" class=\"input\">" . $row['notification_message_homeowner'] . "</textarea><script type=\"text/javascript\">CKEDITOR.replace( 'notification_message_homeowner', {height:500} );</script></td></tr>";
  
  $ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">Cover text of Quote accompanying email<hr/>PDF end notes (i.e. FAQ)<br />Pre-purchase</td><td class=\"td_3\" style=\"padding:10px\"><textarea name=\"notification_message_prepurchase\" rows=\"10\" class=\"input\">" . $row['notification_message_prepurchase'] . "</textarea><script type=\"text/javascript\">CKEDITOR.replace( 'notification_message_prepurchase', {height:500} );</script></td></tr>";
  
  
	$ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">POA Message</td><td class=\"td_3\" style=\"width:500px;padding:10px\"><textarea name=\"poa_message\" rows=\"4\" class=\"input\">" . $row['poa_message'] . "</textarea></td></tr>";

	$ret .= "</table>";



	$ret .= "<br /><br /><h1>Admin Notification</h1>";

	$ret .= "<table>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:200px;padding:3px\">Admin E-mail <br/><br/><i>receives quotes from all postcodes</i></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"admin_email\" value=\"" . $row['admin_email'] . "\" class=\"input\" /></td></tr>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Notify Admin (when new quotes arrive)</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"checkbox\" name=\"is_notify_admin\" value=\"1\"";

	if ($row['is_notify_admin']) {

		$ret .= " checked";

	}

	$ret .= " /></td></tr>";
  
  $ret .= "</table>";
  
  // Territory options
  $ret .= "<br /><br /><h1>Territory Options</h1>";
	$ret .= "<table width=\"100%\">";	
  
  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:120px;padding:3px\">Base Territory E-mail <br/><br/><i>receives quotes from adjusted postcodes only</i></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"territory_email\" value=\"" . (isset($territory[0]['email'])?$territory[0]['email']:'') . "\" class=\"input\" /></td></tr>";
  
  $ret .= "<tr><td class=\"td_2_1\" style=\"width:120px;padding:3px\">Base Territory Postcodes<br/><br/><i>first 3/4 symbols ; separated<br/>e.g. <strong>SE22; N1</strong></i></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><textarea name=\"territory_postcodes\" rows=\"5\" class=\"input\">" . (isset($territory[0]['postcodes'])?$territory[0]['postcodes']:'') . "</textarea></td></tr>";
  
	$ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">Base Territory PDF signature <br/><br/><i>Left side of computer generated image and before <strong>frequently asked questions</strong></i></td><td class=\"td_3\" style=\"padding:10px\"><textarea name=\"territory_pdf_signature\" rows=\"10\" class=\"input\">" . (isset($territory[0]['pdf_signature'])?$territory[0]['pdf_signature']:'') . "</textarea><script type=\"text/javascript\">CKEDITOR.replace( 'territory_pdf_signature', {height:300} );</script></td></tr>";
  
  $ret .= "</table>";
  
  if (count($territory) > 1) {
    foreach ($territory AS $k => $v) {
      if (!$k) continue;
      
      $ret .= '<br /><br /><h1>Territory '.$v['name'].'</h1>';
      $ret .= "<table width=\"100%\">";
      
      $ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:120px;padding:3px\">Territory Name<br/><br/>to delete this territory, just empty the name and save settings</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"territory[".$k."][name]\" value=\"" . htmlspecialchars($v['name'], ENT_QUOTES, 'UTF-8') .  "\" class=\"input\" /></td></tr>";
      
      $ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:120px;padding:3px\">Territory ".$v['name']." E-mail <br/><br/><i>receives quotes from adjusted postcodes only</i></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"territory[".$k."][email]\" value=\"" . htmlspecialchars($v['email'], ENT_QUOTES, 'UTF-8') . "\" class=\"input\" /></td></tr>";
      
      $ret .= "<tr><td class=\"td_2_1\" style=\"width:120px;padding:3px\">Territory ".$v['name']." Postcodes<br/><br/><i>first 3/4 symbols ; separated<br/>e.g. <strong>SE22; N1</strong></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><textarea name=\"territory[".$k."][postcodes]\" rows=\"5\" class=\"input\">" . htmlspecialchars($v['postcodes'], ENT_QUOTES, 'UTF-8') . "</textarea></td></tr>";
      
      $ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">Territory ".$v['name']." PDF signature <br/><br/><i>Left side of computer generated image and before <strong>frequently asked questions</strong></i></td><td class=\"td_3\" style=\"padding:10px\"><textarea name=\"territory[".$k."][pdf_signature]\" rows=\"10\" class=\"input\">" . htmlspecialchars($v['pdf_signature'], ENT_QUOTES, 'UTF-8') . "</textarea><script type=\"text/javascript\">CKEDITOR.replace( 'territory[".$k."][pdf_signature]', {height:300} );</script></td></tr>";
      
      $ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">Territory ".$v['name']." Cover text of Quote accompanying email</td><td class=\"td_3\" style=\"padding:10px\"><textarea name=\"territory[".$k."][notification_message]\" rows=\"5\" class=\"input\">" . htmlspecialchars($v['notification_message'], ENT_QUOTES, 'UTF-8') . "</textarea><script type=\"text/javascript\">CKEDITOR.replace( 'territory[".$k."][notification_message]', {height:300} );</script></td></tr>";
      
      $ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">Territory ".$v['name']." POA Message</td><td class=\"td_3\" style=\"width:500px;padding:10px\"><textarea name=\"territory[".$k."][poa_message]\" rows=\"4\" class=\"input\">" . htmlspecialchars($v['poa_message'], ENT_QUOTES, 'UTF-8') . "</textarea></td></tr>";
      
      $ret .= "</table>";
    }
  }
  
  $ret .= "<br /><br /><h1><a href=\"javascript:;\" onclick=\"if (document.getElementById('territory_new').style.display==='none') document.getElementById('territory_new').style.display = ''; else document.getElementById('territory_new').style.display = 'none';\">Add New Territory</a></h1>";
  $ret .= "<table id=\"territory_new\" width=\"100%\" style=\"display:none\">";
  
  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:120px;padding:3px\">New Territory Name</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"territory_new_name\" value=\"" . "\" class=\"input\" /></td></tr>";
  
  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:120px;padding:3px\">New Territory E-mail <br/><br/><i>receives quotes from adjusted postcodes only</i></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"territory_new_email\" value=\"" . "\" class=\"input\" /></td></tr>";
  
  $ret .= "<tr><td class=\"td_2_1\" style=\"width:120px;padding:3px\">New Territory Postcodes<br/><br/><i>first 3/4 symbols ; separated<br/>e.g. <strong>SE22; N1</strong></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><textarea name=\"territory_new_postcodes\" rows=\"5\" class=\"input\">" . "</textarea></td></tr>";
  
	$ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">New Territory PDF signature <br/><br/><i>Left side of computer generated image and before <strong>frequently asked questions</strong></i></td><td class=\"td_3\" style=\"padding:10px\"><textarea name=\"territory_new_pdf_signature\" rows=\"10\" class=\"input\">" . "</textarea><script type=\"text/javascript\">CKEDITOR.replace( 'territory_new_pdf_signature', {height:300} );</script></td></tr>";
  
	$ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">New Territory Cover text of Quote accompanying email</td><td class=\"td_3\" style=\"padding:10px\"><textarea name=\"territory_new_notification_message\" rows=\"5\" class=\"input\">" . "</textarea><script type=\"text/javascript\">CKEDITOR.replace( 'territory_new_notification_message', {height:300} );</script></td></tr>";
  
	$ret .= "<tr><tr valign=\"top\"><td class=\"td_2_1\" style=\"padding:3px\">New Territory POA Message</td><td class=\"td_3\" style=\"width:500px;padding:10px\"><textarea name=\"territory_new_poa_message\" rows=\"4\" class=\"input\">" .  "</textarea></td></tr>";
  
  
  $ret .= "</table>";
  // Custom Frontend settings
  // backend manage carousel images
  
  $carousel_images = unserialize($row['carousel_images']);
  if (!$carousel_images) $carousel_images = array();
  
  $ret .= "<br /><br /><h1>Frontend Settings</h1>";
  
	$ret .= "<table width=\"100%\">";
  
  $carousel_images = array_flip($carousel_images);

  
  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:200px;padding:3px\">Banner carousel on home page<br/><br/>Just upload <strong>930x275</strong> images</td><td class=\"td_3\" style=\"width:100px;padding:10px\">";
  
  foreach ($carousel_images AS $k => $v) {
    //$k = str_replace("'",'',$k);
    
    $tag_id = 'carousel_images_'.str_replace('.','_',$k);
    
    $ret .= '<div style="float:left;border:1px dotted silver;margin-right:5px;margin-bottom:3px;padding:2px;"><label for="delete_'.$tag_id.'">delete</label>&nbsp;<input type="checkbox" value="1" id="delete_'.$tag_id.'" name="delete_carousel_images['.$k.']" />&nbsp;<label for="'.$tag_id.'">'.$k . '</label>&nbsp;<input type="text" size="4" maxlength="4" name="carousel_images['.$k.']" value="'.(int)$v.'" id="'.$tag_id.'" /></div> ';
  }
  
  $ret .= '<br clear="all" />Upload new slide image: <input type="file" name="slide_image" /> Sort: <input type="text" size="4" maxlength="4" value="0" name="slide_image_sort" />';
  
  $ret .= "</td></tr>";
  
  
  $postcodes_checkboxes = '';
  
  if ($row['map_postcodes']) $row['map_postcodes'] = explode(',', $row['map_postcodes']);
  else $row['map_postcodes'] = array();
  
  //print_r($a_polygon);#debug
  
  $a_polygon_keys = array_keys($a_polygon);
  
  $areas = array_keys($a_polygon_color);
  
  sort($areas);
  
  $a_london_keys = array();
  
  foreach ($areas AS $k) {
    
    //$postcodes_checkboxes .= '<b>'.$k.':</b> ';
    
    foreach ($a_polygon_keys AS $k2) {
      $code = preg_replace('/\d+$/', '', $k2);
      if ($code == $k) {
        $postcodes_checkboxes .= '<label for="code_'.$k2.'">'.$k2.'&nbsp;<input type="checkbox" name="map_postcodes[]" value="'.$k2.'" id="code_'.$k2.'"'.(in_array($k2, $row['map_postcodes'])?' checked="checked"':'').' /></label> ';
        
        $a_london_keys[] = $k2;
      }
    }
    
    $postcodes_checkboxes .= '<br/>';
  }
  
  $prev_code = '';
  $postcodes_checkboxes .= '<br/><strong>Additional postcodes:</strong><br/>';
  foreach ($a_polygon_keys AS $k2) {
    if (!in_array($k2, $a_london_keys)) {
      $code = preg_replace('/\d+$/', '', $k2);
      if ($prev_code!=$code) $postcodes_checkboxes .= '<br/>';
      
      $postcodes_checkboxes .= '<label for="code_'.$k2.'">'.$k2.'&nbsp;<input type="checkbox" name="map_postcodes[]" value="'.$k2.'" id="code_'.$k2.'"'.(in_array($k2, $row['map_postcodes'])?' checked="checked"':'').' /></label> ';
      
      $prev_code = $code;
    }
  }
  
  
  $ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:15%;padding:3px\">Areas we cover for map</td><td class=\"td_3\" style=\"width:85%;padding:10px\">".$postcodes_checkboxes."</td></tr>";
}

$ret .= "</table>";



$ret .= "<br /><br /><input type=\"submit\" value=\"Save Changes\" class=\"button\" />";

$ret .= "</form>";



echo $ret;



require ('./inc/footer.inc.php');



?>