<?php

$PageTitle = "TV";
$TableName = "tv";
$PrimaryKey = "id";
$FieldNames = "id,title,weight";
$DisplayNames = "ID,Title,Sort";
$ModFieldNames = "id,title,url,weight";
$ModDisplayNames = "ID,Title,URL,Sort Order";
$ModFieldTypes = "-1,2,2,2";
$AllowDelete = false; // delete manually

if ('POST'==$_SERVER['REQUEST_METHOD']) {
  require_once ('./inc/util.inc.php');
  
  if (isset($_POST['btn_delete']) && $_POST['btn_delete'] && isset($_POST['id']) && $_POST['id'] && isset($_POST['nonce']) && session_id()==$_POST['nonce']) {      
    
    setRs('DELETE FROM tv WHERE id='.(int)$_POST['id']);
    
    header('Location: data_tv.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;
  }
}

require ('./inc/tbl.inc.php');

?>