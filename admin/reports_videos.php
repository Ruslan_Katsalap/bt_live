<?php

$PageTitle = "Videos";

$TableName = "video"; 

$PrimaryKey = "id";

$FieldNames = "id,project,link,duration,views";

$DisplayNames = "ID, Project, Link, Duration, Views";

$ModFieldNames = "id,project,link,duration,imagefile,is_active";
$ModDisplayNames = "ID, Project, Link, Duration, Thumbnail, Active";

$ModFieldTypes = "1,1,1,1,99,0";
require ('./inc/tbl.inc.php');

?> 