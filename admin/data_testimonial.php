<?php

$PageTitle = "Testimonials";
$TableName = "testimonials";
$PrimaryKey = "testimonial_id";
$FieldNames = "testimonial_id,name,title,is_enabled";
$DisplayNames = "ID,Name,Title,Enabled";
$ModFieldNames = "testimonial_id,name,title,quote,sort,category,is_enabled,video_url,project_url,thumbnailImg";
$ModDisplayNames = "ID,Name,Project,Testimonial,Sort,Category,Enabled,Video Url, Project Url, Thumbnail Image";
$ModFieldTypes = "-1,2,2,4,1,12R,0,2,2,99";
$AllowDelete = true;

require ('./inc/tbl.inc.php');

?>