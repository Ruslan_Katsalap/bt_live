<?php

$PageTitle = "Pages";
$TableName = "page";
$PrimaryKey = "page_id";
$FieldNames = "page_id,page_name,parent_page_id,sort,is_enabled";
$DisplayNames = "ID,Name,Parent,Sort,Enabled";
$ModFieldNames = "page_id,page_name,page_code,parent_page_id,meta_title,meta_description,meta_keywords,page_content,hide_page_media,url,sort,is_topnav,nav_name,is_link,is_enabled,alink1,pageImage1,alink2,pageImage2,alink3,pageImage3";
$ModDisplayNames = "ID,Name,Script Name (with <b>-page</b> suffix to use content),Parent,Meta Title,Meta Description,Meta Keywords,Page Content,Hide Page Media,External URL,Sort,Top Nav,Nav menu name,Hot Link,Enabled,Link 1,Image1,Link 2,Image2, Link 3,Image3";
$ModFieldTypes = "-1,2,2,1,2,3,3,7,0,2,1,0,2,0,0,1,99,1,99,1,99";
$AllowDelete = false;
$DeletedTBLName = "page";
if ('POST'==$_SERVER['REQUEST_METHOD']) {
  require_once ('./inc/util.inc.php');
  if (isset($_POST['btn_delete']) && $_POST['btn_delete'] && isset($_POST['page_id']) && ($_POST['page_id']=(int)$_POST['page_id']) && isset($_POST['nonce']) && session_id()==$_POST['nonce']) {

    $sql = "SELECT page_media_id, filename FROM page_media WHERE page_id = '" . $_POST['page_id']. "'";
    $rs = getRs($sql);
    
    $path = explode('/', $abspath);
    array_pop($path);
    array_pop($path);
    array_pop($path);
    $path = implode('/', $path);
    
    while ($row=mysqli_fetch_assoc($rs)) {
      //echo $path.'/media/'.$row['filename'].'<br>';#debug
      
      if ('pdf' == getExt($row['filename'])) {
        @unlink($path.'/pdf/'.$row['filename']);
      }
      else {
        @unlink($path.'/media/'.$row['filename']);
      }
      
      $sql = "DELETE FROM page_media WHERE page_media_id={$row['page_media_id']}";
      
      //echo $sql.'<br>';#debug
      setRs($sql);
      
    }
    
    $sql = "DELETE FROM page WHERE page_id={$_POST['page_id']}";
    setRs($sql);
    
    //echo $sql;
    //exit;#debug
    
    header('Location: data.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;
  }
  
}

if(isset($_GET['DeleteByID']) && $_GET['Deleteid']!=''){
	require_once ('./inc/util.inc.php');
	setRs("DELETE FROM $TableName WHERE $PrimaryKey='{$_GET['Deleteid']}'");
}


require ('./inc/tbl.inc.php');

?>