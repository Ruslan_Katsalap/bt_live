<?php

$PageTitle = "D & P Project Type";
$TableName = "dpproject_type";
$PrimaryKey = "id";
$FieldNames = "id,project_type";
$DisplayNames = "ID,Name";
$ModFieldNames = "id,project_type";
$ModDisplayNames = "ID,Name";
$ModFieldTypes = "-1,2";
$AllowDelete = true;

require ('./inc/tbl.inc.php');

?>