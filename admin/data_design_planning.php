<?php

$PageTitle = "Design & Planning Database";
$TableName = "design_planning";
$PrimaryKey = "id";
$FieldNames = "id,title,property,borough,postcode,roof,floor";
$DisplayNames = "ID,Project Name,Property Type,Borough,Postcode,Roof,Floor Area&#044; m<sup>2</sup>";
$ModFieldNames = "id,title,property,protypeID,borough,postcode,roof,floor,pdffile";
$ModDisplayNames = "ID,Project Name,Property Type,Project Type,Borough,Postcode,Roof,Floor Area&#044; m<sup>2</sup>,PDF file";
$ModFieldTypes = "-1,2,1,1,1,2,1,2,99";
$AllowDelete = false; // delete manually

global $a_property, $a_roof;
$a_property = array('h' => 'House', 'f' => 'Flat');
$a_roof = array('sv' => 'Slate & Velux', 'f' => 'Flat Roof', 'g' => 'All Glass');

if ('POST'==$_SERVER['REQUEST_METHOD']) {
  require_once ('./inc/util.inc.php');
  
  if (isset($_POST['btn_delete']) && $_POST['btn_delete'] && isset($_POST['id']) && $_POST['id'] && isset($_POST['nonce']) && session_id()==$_POST['nonce']) {

    $rs = getRs("SELECT id, pdffile FROM design_planning WHERE id = ".(int)$_POST['id']);
    
    $row = mysqli_fetch_assoc($rs);
    
    if ($row && isset($row['id'])) {
      
      $path = str_replace('\\', '/', dirname(__FILE__));
      
      $path = explode('/', $path);
      array_pop($path);
      $path = implode('/', $path);
      
      //echo $path.'<br/>';
      
      if ($row['pdffile']) {
        unlink($path.'/pdf/'.$TableName.'/'.$row['pdffile']);
        
        $basename = explode('.', $row['pdffile']);
        array_pop($basename);
        $basename = implode('.', $basename);
        @unlink($path.'/pdf/'.$TableName.'/thumb/'.$basename.'.png');
        
      }
      
      setRs('DELETE FROM design_planning WHERE id='.$row['id']);
      
      //print_r($row);#debug
      //exit;#debug
      
    }
    header('Location: data_design_planning.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;
  }
}

require ('./inc/tbl.inc.php');

?>