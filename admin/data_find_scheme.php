<?php

$PageTitle = "Find Scheme";

$TableName = "post_code";

$PrimaryKey = "id";

$FieldNames = "id,dates,postcode";

$DisplayNames = "ID,Date,Postcode";

$ModFieldNames = "id,dates,postcode";

$ModDisplayNames = "ID,Date,Postcode";

$ModFieldTypes = "-1,2,2";

$AllowDelete = false;

$AllowAdd = false;

//require_once ('./inc/util.inc.php');

require ('./inc/tbl.inc.php');

?>