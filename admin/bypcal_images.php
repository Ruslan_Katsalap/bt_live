<?php

$PageTitle = "Build Your Price Images";
$TableName = "bypcal_images";
$PrimaryKey = "id";
$FieldNames = "id,image,title,is_enabled";
$DisplayNames = "ID,Image,Title,Enabled";
$ModFieldNames = "id,image,title,is_enabled";
$ModDisplayNames = "ID,Image,Title,Enabled";
$ModFieldTypes = "-1,99,1,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>