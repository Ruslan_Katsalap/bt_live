<?php

$PageTitle = "Category";
$TableName = "category";
$PrimaryKey = "category_id";
$FieldNames = "category_id,category_name,is_enabled";
$DisplayNames = "ID,Category,Enabled";
$ModFieldNames = "category_id,category_name,is_displayed,is_enabled";
$ModDisplayNames = "category_id,category_name,Display,Enabled";
$ModFieldTypes = "-1,2,0,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>