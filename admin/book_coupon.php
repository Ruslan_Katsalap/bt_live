<?php

$PageTitle = "Coupons";
$TableName = "coupon";
$PrimaryKey = "coupon_id";
$FieldNames = "coupon_id,coupon_code,visit_type,valid_from,expiry_date,discount,used,is_enabled";
$DisplayNames = "ID,Coupon code,Product type,Valid from,Expiry date,Discount&#44; %,Used,Enabled?";
$ModFieldNames = "coupon_id,coupon_code,visit_type,valid_from,expiry_date,discount,is_enabled";
$ModDisplayNames = "ID,Coupon code,Product type,Valid from,Expiry date,Discount&#44; %,Enabled?";
$ModFieldTypes = "-1,2,1,14,14,1,0";
$AllowDelete = true;

require ('./inc/tbl.inc.php');

?>