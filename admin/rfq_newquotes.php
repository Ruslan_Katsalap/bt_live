<?php

$PageTitle = "Quote Requests";
$TableName = "byp_Quote";
$PrimaryKey = "id";
$FieldNames = "id,refCode,fname,surname,email,telphone,addPostcode,totalPrice,bypSubscribe,dates_created";
$DisplayNames = "ID,Quote Ref,First Name,Surname,Email,Telephone,Postcode,Cost,Subscribe,Created Date";
$ModFieldNames = "id,refCode,statusQuo,propertyType,depth,width,unitMmeasures,roomsize,usedCal,doors,roof,side,side2,chimneyHeating,chimneyBreast,additionnal,totalPrice,quoteType,fname,address,addPostcode,telphone,email,comment,emailQuote,mailingList,contactMethod,lastModified,clientIP,quotesFromIP,userAgent,territory,lookingStart,currentstatus";
$ModDisplayNames = "ID,
Quote Ref,
Status,
Property Type,
Room Length,
Room width,
Unit Measures,
Room Size,
Used Estimator,
Doors,
Roof,
Property,
Extention,
Ground Floor WC,
Chimney Breast,
Heating,
Total,
Quote Type,
First Name,
Address,
Postcode,
Telephone,
Email,
Comment,
Email Quote,
Mailing List,
Contact Method,
Last Modified,
ClientIP,
Quotes From IP,
User Agent,
Territory,
Looking Start,
Current Status";

// Doors Cost,Roof Cost,Wall Cost,Ground Floor WC Cost,Chimney Breast Cost,Heating,Labour,Materials,Plant,Waste,Misc,
$ModFieldTypes = "1,-1,1,-1,-1,1,1,-1,-1,-1,-1,1,1,1,1,1,1,1,-1,-1,-1,-1-1,1,1,1,1,1,1,1,1,1,1,1,1";

$AllowDelete = true;
$SearchFields = "postcode";
$DeletedTBLName = "byp_Quote";
if ('POST'==$_SERVER['REQUEST_METHOD']) {
 // require_once ('./inc/util.inc.php');
 
}
if(isset($_GET['DeleteByID']) && $_GET['Deleteid']!=''){
	require_once ('./inc/util.inc.php');
	setRs("DELETE FROM $TableName WHERE id='{$_GET['Deleteid']}'");
}
require ('./inc/tbl.inc.php');

?>