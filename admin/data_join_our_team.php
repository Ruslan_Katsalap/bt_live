<?php

$PageTitle = "Join Our Team";

$TableName = "join_our_team";

$PrimaryKey = "id";

$FieldNames = "id,title,description,pdffile";

$DisplayNames = "ID,Title,Description, Image";

$ModFieldNames = "id,title,description,pdffile,is_active";

$ModDisplayNames = "ID,Title,Description, Image, Active";

$ModFieldTypes = "-1,2,2,99,0";

$AllowDelete = false;

$AllowAdd = true;

//require_once ('./inc/util.inc.php');

require ('./inc/tbl.inc.php');

?>