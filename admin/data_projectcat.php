<?php

$PageTitle = "Project Categories";
$TableName = "project_category";
$PrimaryKey = "project_category_id";
$FieldNames = "project_category_id,project_category_name,project_category_code,is_enabled";
$DisplayNames = "ID,Name,Code,Enabled";
$ModFieldNames = "project_category_id,project_category_name,project_category_code,sort,is_enabled";
$ModDisplayNames = "ID,Name,Code,Sort,Enabled";
$ModFieldTypes = "-1,2,-1,1,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>