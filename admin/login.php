<?php

$skipAuth = true;

define('SkipAuth', 'true');

require ('./inc/util.inc.php');

$errMsg = '';

$_SESSION['adminid'] = '';

if (isset($_REQUEST['username']))
	$username = $_REQUEST['username'];
else
	$username = '';

if (isset($_REQUEST['password']))
	$password = $_REQUEST['password'];
else
	$password = '';

if (strlen($username) > 0 && strlen($password) > 0) {
	$rs = getRs("SELECT admin_id, username, firstname, lastname FROM admin WHERE is_enabled = 1 AND is_active = 1 AND username = '" . formatSql($username) . "' AND password = '" . formatPassword($password) . "'");
	if ($row = mysqli_fetch_assoc($rs)) {
		$errMsg = 'Login successful ... redirecting';
		$_SESSION['adminid'] = $row['admin_id'];
		$_SESSION['admin_name'] = $row['firstname'] . ' ' . $row['lastname'];
		//header("Location: /index.php?{$row['AdminID']}&{$_SESSION['adminid']}");
		header("Location: index.php");
	}
	else {
		$errMsg = 'Invalid username / password combination';
	}
}

//close
//dbClose();

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Build Team CMS v 1.0</title>
<link type="text/css" rel="stylesheet" href="css/login.css">
<script language="javascript" src="js/default.js" type="text/javascript"></script>
</head>

<body>
<table border="0" cellpadding="0" cellspacing="0" width="100%" height="600">
	<tr>
		<td align="center">
    	
      <table cellspacing="0" class="t_Login">
      	<tr>
        	<th>CMS Login</th>
          <th class="r">Your IP: <?php echo $IP ?></th>
        </tr>
        <tr>
        	<td colspan="3">
            
          	<table>
              <tr>
                <td colspan="2" class="red"><?php echo $errMsg ?></td>
              </tr>
             </table>
             <table>
				  	  <form action="login.php" method="post">
              <tr>
                <td>Username:</td>
                <td colspan="2"><input type="text" name="username" value="" class="input_login" required="yes" message="Username is required" /></td>
              </tr>
              <tr>
                <td>Password:</td>
                <td colspan="2"><input type="password" name="password" value="" class="input_login"required="yes" message="Password is required" /></td>
              </tr>
              <tr>
                <td></td>
                <!--<td><input type="checkbox" name="savepass" id="id_savepass" value="1" /> <label for="id_savepass">Remember me</label></td>--->
                <td><input type="submit" value="Login" class="button_login" /></td>
              </tr>
              </form>
            </table>
          </td>
        </tr>
      </table>
    </td>
  </tr>
</table>
        
</body>
</html>