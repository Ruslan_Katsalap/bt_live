<?php

$PageTitle = "Projects";
$TableName = "project";
$PrimaryKey = "project_id";
$FieldNames = "project_id,project_category_id,project_name,is_enabled";
$DisplayNames = "ID,Category,Name,Enabled";
$ModFieldNames = "project_id,project_category_id,project_name,project_code,sort,is_enabled";
$ModDisplayNames = "ID,Category,Name,Code,Sort,Enabled";
$ModFieldTypes = "-1,1,2,-1,1,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>