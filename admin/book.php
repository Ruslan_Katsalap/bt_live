<?php

$PageTitle = "Booked Visits";
$TableName = "visit";
$PrimaryKey = "visit_id";
$FieldNames = "visit_id,full_name,email,phone,postcode,addr,date_modified,availibility,visit_type,coupon_code,svSubscribe,is_enabled";
$DisplayNames = "ID,Full name,Email,Phone,Postcode,Addr,Book Date,Availibility,Type,Coupon Code,Subscribe,Paid";
$ModFieldNames = "visit_id,full_name,email,phone,postcode,addr,comments,availibility,visit_type,amount,coupon_code,is_enabled,log";
$ModDisplayNames = "ID,Full name,Email,Phone,Postcode,Address,Comments,Availibility,Visit type,Amount&#44; &pound;,Coupon code,Paid,Payment Log";
$ModFieldTypes = "-1,2,2,2,2,2,4,4,1,1,2,0,4";
$AllowDelete = false;


$DeletedTBLName = "visit";
if(isset($_GET['DeleteByID']) && $_GET['Deleteid']!=''){
	require_once ('./inc/util.inc.php');
	setRs("DELETE FROM $TableName WHERE $PrimaryKey='{$_GET['Deleteid']}'");
}
require ('./inc/tbl.inc.php');

?>