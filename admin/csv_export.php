<?php 

require_once ('./inc/util.inc.php');

$FieldNames = "r.rfq_id,r.rfq_code,r.postcode,r.rfq_status_id,r.name,r.email,r.amount_quote,r.date_created,r.phone,r.status,r.start";

$rs = getRs("SELECT $FieldNames, s.rfq_status_name FROM rfq r LEFT JOIN rfq_status s ON (r.rfq_status_id=s.rfq_status_id)".((isset($_GET['kw']) && $_GET['kw'])?" WHERE r.postcode LIKE '%".mysql_real_escape_string($_GET['kw'])."%'":'')." ORDER BY rfq_id DESC");

header('Content-Disposition: attachment; filename="rfq.csv"');
header('Content-Type: text/plain; charset=UTF-8');
$sep = ','; 
// separator symbol ; or ,
echo '"ID"'.$sep.'"Quote Ref"'.$sep.'"Postcode"'.$sep.'"Status"'.$sep.'"Name"'.$sep.'"E-mail"'.$sep.'"Phone"'.$sep.'"Total"'.$sep.'"POA"'.
$sep.'"Homeowner status"'.
$sep.'"When start"'.
$sep.'"Created"'."\r\n";
while ($row = mysqli_fetch_assoc($rs)) { 
  echo $row['rfq_id'].$sep.'"'.$row['rfq_code'].'"'.$sep.'"'.$row['postcode'].'"'.$sep.'"'.$row['rfq_status_name'].'"'.$sep.'"'.$row['name'].'"'.$sep.'"'.$row['email'].'"'.$sep.'"'.$row['phone'].'"'.$sep.abs($row['amount_quote']).$sep.($row['amount_quote']<0?'Yes':'').
  
  $sep.'"'.$row['status'].'"'.
  $sep.'"'.$row['start'].'"'.
  
  $sep.'"'.date('M d, Y g:i A', $row['date_created']).'"'."\r\n";
} 
?>