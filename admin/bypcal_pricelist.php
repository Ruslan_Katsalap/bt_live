<?php

$PageTitle = "BYP Calculator Price List by Square Meter";
$TableName = "bypcal_pricelist";
$PrimaryKey = "id";
$FieldNames = "id,square_meter,price";
$DisplayNames = "ID,Square Meter,Price";
$ModFieldNames = "id,square_meter,price";
$ModDisplayNames = "ID,Square Meter,Price";
$ModFieldTypes = "-1,2,2";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>