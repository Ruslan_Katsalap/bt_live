<?php

$admin_page_name = "dc_booking";

$PageTitle = "DC Bookings";
$TableName = "dc_bookings";
$PrimaryKey = "id";
$FieldNames = "id,f_name,surname,email,phone,address,postcode,link,contact,start,status,exten_type,events,slot_date,slot,filename,booked";

$DisplayNames = "ID,First Name,Surname,Email,Phone,Address,Postcode,Link,Contact Type,Type,Status Type,Exten Type, Subscribe,Date,Slot,Filename,Allocated";
// $ModFieldNames = "visit_id,full_name,email,phone,postcode,addr,comments,availibility,visit_type,amount,coupon_code,is_enabled,log";
// $ModDisplayNames = "ID,Full name,Email,Phone,Postcode,Address,Comments,Availibility,Visit type,Amount&#44; &pound;,Coupon code,Paid,Payment Log";
// $ModFieldTypes = "-1,2,2,2,2,2,4,4,1,1,2,0,4";
// $AllowDelete = false;


$DeletedTBLName = "dc_bookings";
if(isset($_GET['DeleteByID']) && $_GET['Deleteid']!=''){
	require_once ('./inc/util.inc.php');
	setRs("DELETE FROM $TableName WHERE $PrimaryKey='{$_GET['Deleteid']}'");
}
require ('./inc/tbl.inc.php');


?>




<script>
/***********************Book DC ADMIN ************************/
var book_holidays = ["2021-4-2", "2021-4-4", "2021-4-5", "2021-5-3", "2021-5-31", "2021-6-20", "2021-6-21", "2021-6-30", "2021-7-12", "2021-7-18", "2021-8-2", "2021-9-22","2021-11-30", "2021-12-21","2021-12-24","2021-12-25","2021-12-27","2021-12-26","2021-12-28","2021-12-31"]

var dc_slot_id = ""
var dc_slot_reset = [
	{"9:00": 1, "9:30": 1, "10:00": 1, "10:30": 1, "11:00": 1, "11:30": 1},
	{"12:00": 1, "12:30": 1, "13:00": 1, "13:30": 1, "14:00": 1, "14:30": 1},
	{"15:00": 1, "15:30": 1, "16:00": 1, "16:30": 1, "17:00": 1, "17:30": 1, "18:00":1, "18:30":1}
] //default values of slots
var dc_slots_json = dc_slot_reset

function upload_dc_slots() {
		var dc_slot_date = $('#dc_datepicker').val()
		$.ajax({
			type: "GET",
			url: "/api/dc_c_get_slots.php",
			data: {"slot_date": dc_slot_date},
			async: false,
			success: function(data){
				var data = JSON.parse(data)
				if(data.result){
					dc_slots_json = JSON.parse(data["data"])
					dc_slot_id = data["slot_id"]

					//console.log("data",slots_json)
					//console.log("slot id",slot_id)
				} else {
					dc_slots_json = dc_slot_reset
					dc_slot_id = ""
				}
				
			}
		});

		var slot_1 = create_dc_slot_elem(dc_slots_json[0])
		var slot_2 = create_dc_slot_elem(dc_slots_json[1])
		var slot_3 = create_dc_slot_elem(dc_slots_json[2])
		$('.dc_1').html(slot_1)
		$('.dc_2').html(slot_2)
		$('.dc_3').html(slot_3)
}

//update start and end time new fields
function update_slot_start_end_interval(data){
		var slot_1 = create_dc_slot_elem(data[0])
		var slot_2 = create_dc_slot_elem(data[1])
		var slot_3 = create_dc_slot_elem(data[2])
		$('.dc_1').html(slot_1)
		$('.dc_2').html(slot_2)
		$('.dc_3').html(slot_3)
}

function create_dc_slot_elem(slots){
	var slots_list = ""
	var slot = ""
	for(slot in slots){
		slot = '<div class="dc_slot_row" data-slot="'+slot+'"><span>'+slot+'</span><input type="number" name="slot_availability" value="'+slots[slot]+'" required/></div>'
		slots_list += slot
	}
	return slots_list
}

//update admin slots

function update_admin_losts(){
	var data = {}
	var slots = []
	

	var slot = {}
	$('.dc_1 .dc_slot_row').each(function(){
		var _that = $(this)
		slot[_that.data("slot")] = _that.find("input").val()
	})
	slots.push(slot)

	slot = {}
	$('.dc_2 .dc_slot_row').each(function(){
		var _that = $(this)
		slot[_that.data("slot")] = _that.find("input").val()
	})
	slots.push(slot)
	
	slot = {}
	$('.dc_3 .dc_slot_row').each(function(){
		var _that = $(this)
		slot[_that.data("slot")] = _that.find("input").val()
	})
	slots.push(slot)

	data["slots"] = JSON.stringify(slots)
	data["slot_date"] = $('#dc_datepicker').val()
	

	if(dc_slot_id == ""){ //update or add slots settings
		data["slot_status"] = "add"
		data["slot_id"] = dc_slot_id
	} else {
		data["slot_status"] = "update"
		data["slot_id"] = dc_slot_id
	}

	console.table(data)
	$.ajax({
		type: "GET",
		url: "/api/dc_admin_update.php",
		data: data,
		async: false,
		success: function(data){
			var data = JSON.parse(data)
			if(data.result){
				$(".slot_succ").show()
				setTimeout(function(){
					$(".slot_msg").hide()
				}, 3000)
				//location.reload()
				upload_dc_slots()
			} else {
				$(".slot_err").show()
				setTimeout(function(){
					$(".slot_msg").hide()
				}, 3000)
				//location.reload()
				upload_dc_slots()
			}
			
		}
	});
}

$(document).on("click", '.dc_picker_next', function (e) {
	e.preventDefault()
    var date = $('#dc_datepicker').datepicker('getDate');
    date.setTime(date.getTime() + (1000*60*60*24))

    //var disabled = $('#datepicker').datepicker('isDisabledDate', date);
    //console.log(disabled)

    $('#dc_datepicker').datepicker("setDate", date);
    var dayOfWeek = date.getUTCDay();
    var date_for_hol =  date.getFullYear()+"-"+(date.getMonth()+1) + "-"+date.getDate();
    upload_dc_slots()

    if(dayOfWeek == 5 || dayOfWeek == 6 || (book_holidays.indexOf(date_for_hol) != -1) ){
    	//date.setTime(date.getTime() + (1000*60*60*24))
    	//$('#datepicker').datepicker("setDate", date);
    	$('.dc_picker_next').trigger("click")
    }
});

//datepicker onchange
$(document).on("change", '#dc_datepicker', function(){
	upload_dc_slots()
})

$(document).on("click", '.dc_picker_prev', function (e) {
	e.preventDefault()
    var date = $('#dc_datepicker').datepicker('getDate');
    date.setTime(date.getTime() - (1000*60*60*24))
    $('#dc_datepicker').datepicker("setDate", date);
    var dayOfWeek = date.getUTCDay();
    var date_for_hol =  date.getFullYear()+"-"+(date.getMonth()+1) + "-"+date.getDate();
    upload_dc_slots()
    if(dayOfWeek == 5 || dayOfWeek == 6 || (book_holidays.indexOf(date_for_hol) != -1) ){
    	//date.setTime(date.getTime() + (1000*60*60*24))
    	//$('#datepicker').datepicker("setDate", date);
    	$('.prev_day_btn').trigger("click")
    }
});


$(document).on("click", '.update_slots_btn', function(e){
	e.preventDefault()
	update_admin_losts()
})
</script>

<script>
	//get range
	var d_max_range = 30
	$.ajax({
	type: "GET",
	url: "/api/dc_get_range.php",
	async: false,
	success: function(data){
		 var data = JSON.parse(data)
		// if(data.result){
			d_max_range = parseInt(data["range"]);
			console.log("max_range",d_max_range)
		// } else {
			
		 }
		
	})

</script>

<!-- book admin -->
<script>
 $( function() {

 	var array = ["2013-03-14","2013-03-15","2013-03-16"]

    $( "#dc_datepicker" ).datepicker({
    	dateFormat: "D, dd M yy",
    	numberOfMonths: 2,
    	minDate:0,
    	maxDate: d_max_range,
    	beforeShowDay: function(date){
    		//$.datepicker.noWeekends
    		var date_for_hol =  date.getFullYear()+"-"+(date.getMonth()+1) + "-"+date.getDate();
    		var dayOfWeek = date.getUTCDay();
    		return [ dayOfWeek != 5 && dayOfWeek != 6 && book_holidays.indexOf(date_for_hol) == -1 ]
    	}
    }).datepicker("setDate", new Date());
  } );

  $(function(){
  	upload_dc_slots()

  	var dd_date = $('#dc_datepicker').datepicker('getDate');
				    var dayOfWeek = dd_date.getUTCDay();
				    var date_for_hol =  dd_date.getFullYear()+"-"+(dd_date.getMonth()+1) + "-"+dd_date.getDate();

			if(dayOfWeek == 5 || dayOfWeek == 6 || (book_holidays.indexOf(date_for_hol) != -1) ){
		    	//date.setTime(date.getTime() + (1000*60*60*24))
		    	//$('#datepicker').datepicker("setDate", date);
		    	$('.dc_picker_next').trigger("click")
			}
  })
</script>
<!-- book end book admin -->



<!-- Timepicker  -->
<script>
	$(function(){
		$('.timepicker_start').timepicker({
		    timeFormat: 'H:mm',
		    interval: 30,
		    minTime: '8:00',
		    maxTime: '20:00',
		    defaultTime: '8:00',
		    startTime: '8:00',
		    dynamic: false,
		    dropdown: true,
		    scrollbar: true
		});

		$('.timepicker_end').timepicker({
		    timeFormat: 'H:mm',
		    interval: 30,
		    minTime: '8:00',
		    maxTime: '20:00',
		    defaultTime: '18:30',
		    startTime: '8:00',
		    dynamic: false,
		    dropdown: true,
		    scrollbar: true
		});
	})

function intervals(startString, endString) {
    var start = moment(startString, 'H:mm');
    var end = moment(endString, 'H:mm');

    // round starting minutes up to nearest 15 (12 --> 15, 17 --> 30)
    // note that 59 will round up to 60, and moment.js handles that correctly
    start.minutes(Math.ceil(start.minutes() / 15) * 15);

    var result = [];

    var current = moment(start);

    while (current <= end) {
        result.push(current.format('H:mm'));
        current.add(30, 'minutes');
    }

    return result;
}

//console.log(intervals('8:30', '17:30'));


$(document).on("click", ".change_time", function(e){
	e.preventDefault()
	var startTime = $('.timepicker_start').val()
	var endTime = $('.timepicker_end').val()
	var intervals_arr = intervals(startTime, endTime)
	var interval_list = [] //main obj for creating markup
	var interval_obj = {}
	//console.log(intervals_arr)
	intervals_arr.forEach(function(value, index, array){
		interval_obj[value] = 1
		if(value == '11:30' || value == '15:30' || value == endTime) {
			interval_list.push(interval_obj)
			interval_obj = {}
		}
	})

	console.log(interval_list);
	update_slot_start_end_interval(interval_list)
	$('.update_slots_btn').trigger("click")
})


//dc_bookings team booked
$(document).on("change", '.book_team_confirm', function(e){
	e.preventDefault()
	var booked_status = $(this).is(":checked") ? 1:0
	var id = $(this).data("id");
	var data = {status:booked_status, id:id}
	console.log("status",data)
	$.ajax({
		type: "GET",
		url: "/api/team_book.php",
		data: data,
		success: function(data){
			var data = JSON.parse(data)
			if(data.result){
				
			} else {
				
			}
			
		}
	});
})

//update daterange
$(document).on("click",'.change_date_range',function(e){
	e.preventDefault()
	var maxDate = $('.dc_date_range').val()
		$.ajax({
		type: "GET",
		url: "/api/dc_range.php",
		data: {maxDate:maxDate},
		success: function(data){
			// var data = JSON.parse(data)
			// if(data.result){
				location.reload()
			// } else {
				
			// }
			
		}
	});
})
	
</script>