<?php 
//mb 20210228
require_once ('./inc/util.inc.php');

//$FieldNames = "r.rfq_id,r.rfq_code,r.postcode,r.rfq_status_id,r.name,r.email,r.amount_quote,r.date_created,r.phone,r.status,r.start";
$FieldNames = "id,refCode,postcode,telphone,statusQuo,propertyType,depth,width,unitMmeasures,roomsize,usedCal,doors,roof,side,side2,chimneyHeating,chimneyBreast,additionnal,totalPrice,quoteType,fname,surname,address,addPostcode,telphone,email,comment,emailQuote,mailingList,contactMethod,lastModified,clientIP,quotesFromIP,userAgent,territory,lookingStart,currentstatus,dates_created";
$thisyear = date('Y');
//echo $thisyear; #DEBUG
//$q = "SELECT * FROM byp_Quote WHERE currentstatus NOT LIKE 'pre-purchase' AND dates_created >= '" . $thisyear . "-01-01' ORDER BY id DESC";
$q = "SELECT * FROM byp_Quote WHERE currentstatus NOT LIKE 'pre-purchase' and email != '' AND dates_created >= '" . $thisyear . "-01-01' GROUP by email ORDER BY `byp_Quote`.`email` DESC";

//echo $q; #debug
$rs = getRs($q);
$f = fopen('php://memory', 'w');
$filename = "homeowner-quotes.csv";
$delimiter = ','; 
$fields = ["ID", "Quote Ref", "Postcode", "Status", "First Name", "Last Name", "E-mail", "Phone", "Total", "POA", "When start", "Created"];
fputcsv($f, $fields, $delimiter);

while ($row = mysqli_fetch_assoc($rs)) { 
  $POA = ($row['totalPrice']<0?'Yes':'');
  //$formatdate = date('M d, Y g:i A', strtotime($row['dates_created']));

  $lineData = [ $row['id'], $row['refCode'], $row['postcode'], $row['currentstatus'], $row['fname'], $row['surname'], $row['email'], $row['telphone'], abs($row['totalPrice']), $POA,  $row['lookingStart'], $row['dates_created'] ];  
  fputcsv($f, $lineData, $delimiter);
} 

fseek($f, 0);

//set headers to download file rather than displayed
header('Content-Type: text/csv');
header('Content-Disposition: attachment; filename="' . $filename . '";');

//output all remaining data on a file pointer
fpassthru($f);
exit;
?>