<?php

$PageTitle = "Projects";
$TableName = "project_new";
$PrimaryKey = "project_id";
$FieldNames = "project_id,project_category_id,project_name,is_enabled,project_type,borough,floor_area";
$DisplayNames = "ID,Category,Name,Enabled,Project Type,Borough,Floor Area";
$ModFieldNames = "project_id,project_category_id,project_name,project_code,sort,is_enabled,project_type,borough,floor_area,postcode_no_space,thumbnailImg";
$ModDisplayNames = "ID,Category,Name,Code,Sort,Enabled,Project Type,Borough,Floor Area,Postcode No Space,Thumbnail Image";
$ModFieldTypes = "-1,1,2,-1,1,0,2,2,2,2,99";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

if($_GET['thmRemove'] == 'Yes'){
	//echo "UPDATE project_new SET thumbnailImg='' WHERE id='".$_GET['id']."'";
	mysqli_query($dbconn,"UPDATE project_new SET thumbnailImg='' WHERE project_id='".$_GET['id']."'");
}
?>