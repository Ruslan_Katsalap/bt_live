<?php
require ('./inc/header.inc.php');
if (!isset($_GET['start_date'])) $_GET['start_date'] = '';
if (!isset($_GET['end_date'])) $_GET['end_date'] = '';
$start_date = 0;
$end_date = 0;
$err_msg = '';
if ($_GET['start_date']) {
  $start_date = strtotime($_GET['start_date']);
  if (!$start_date) {
    $err_msg .= "Incorrect Start Date!<br/>";
  }
}
else {
  $start_date = strtotime('January 1');
  $_GET['start_date'] = date('m/d/Y', $start_date);
}
if ($_GET['end_date']) {
  $end_date = strtotime($_GET['end_date']);
  if (!$end_date) {
    $err_msg .= "Incorrect End Date!<br/>";
  }
}
if ($start_date > $end_date && $end_date) {
  $err_msg .= "End Date should be greater than Start Date!<br/>";
}
// get cover field
$sql_date = 'dates_created > 0';
if ($start_date) {
  $sql_date = 'dates_created >= '.$start_date;
}
if ($end_date) {
  $sql_date .= ' AND dates_created <= '.$end_date;
}
$sql = "SELECT COUNT(id) AS num, postcode FROM byp_Quote WHERE $sql_date  AND postcode IS NOT NULL AND postcode<>'' GROUP BY postcode";
$rs = getRs($sql);
$a_cover = array();
$total = 0;
while ($row = mysqli_fetch_assoc($rs)) {
  //if (''!=$row['postcode']) $a_cover['in'] = $row['num'];
  //else $a_cover['out'] = $row['num'];
  $total += $row['num'];
}

if (isset($_GET['start_date']))
	$sql_date1 = $sql_date;
else
	$sql_date1 = '';
$sqlTin = "SELECT id AS num, postcode FROM byp_Quote WHERE $sql_date1  AND territory='in' GROUP BY postcode";
$rsin = getRs($sqlTin);
$a_cover1 = array();
$totalin = 0;
while ($rowin = mysqli_fetch_assoc($rsin)) {
  $totalin++; //+= $rowin['num'];
}
$a_cover['in'] = $totalin;

$sqlout = "SELECT id AS num, postcode FROM byp_Quote WHERE $sql_date1  AND territory='out'  GROUP BY postcode ";
$rsout = getRs($sqlout);
$totalout = 0;
while ($rowout = mysqli_fetch_assoc($rsout)) {
//    $a_cover['out'] += $rowout['num'];
  $totalout++; //+= $rowout['num'];
}
$a_cover['out'] =  $totalout;

//print_r($a_cover);
// Get Averages
$rs = getRs("SELECT s.base_areas, s.base_pricing, s.min_quote, s.max_quote, s.design_cost, s.design_discount, s.offer_exp_days, s.duration_pricing, s.duration_weeks, s.postcode_pricing, s.flat_pricing, s.poa_message FROM setting s WHERE s.setting_id = 1");
$a_setting = mysqli_fetch_assoc($rs);
// IF(amount_quote>0,amount_quote,85000) ABS(amount_quote)
$sql = "SELECT AVG(IF(roomsize<100,IF(roomsize<9,9,roomsize),9.29)) AS avg_floor, AVG(IF(totalPrice>0,totalPrice,{$a_setting['max_quote']})) AS avg_amount, MIN(roomsize) AS min_floor, MAX(roomsize) AS max_floor, MIN(totalPrice) AS min_amount, MAX(totalPrice) AS max_amount FROM byp_Quote WHERE $sql_date AND territory = 'out' AND  postcode IS NOT NULL AND postcode<>''";
$rs = getRs($sql);
$a_avg = mysqli_fetch_assoc($rs);
// Get count by status and start
$a_status = array();
$sql = "SELECT COUNT(id) AS num, `currentstatus` FROM byp_Quote WHERE $sql_date  AND postcode IS NOT NULL AND postcode<>'' GROUP BY `currentstatus`";
$rs = getRs($sql);
while ($row = mysqli_fetch_assoc($rs)) {
  if (!$row['currentstatus']) {
    if (!isset($a_status['empty'])) $a_status['empty'] = $row['num'];
    else $a_status['empty'] += $row['num'];
  }
  else $a_status[ $row['currentstatus'] ] = $row['num'];
}
$a_start = array();
$sql = "SELECT COUNT(id) AS num, `lookingStart` FROM byp_Quote WHERE $sql_date  AND lookingStart IS NOT NULL AND lookingStart<>'' GROUP BY `lookingStart`";
$rs = getRs($sql);
while ($row = mysqli_fetch_assoc($rs)) {
  if (!$row['lookingStart']) {
    if (!isset($a_start['empty'])) $a_start['empty'] = $row['num'];
    else $a_start['empty'] += $row['num'];
  }
  else $a_start[ $row['lookingStart'] ] = $row['num'];
}
$sql = "SELECT IF(LENGTH(postcode)<6, LEFT(postcode, 2), IF(LENGTH(postcode)=6, LEFT(postcode, 3), LEFT(postcode, 4))) AS `prefix`, COUNT(id) AS num, AVG(IF(totalPrice>0,totalPrice,{$a_setting['max_quote']})) AS avg_amount FROM byp_Quote WHERE $sql_date  AND postcode IS NOT NULL AND postcode<>'' GROUP BY `prefix` ORDER BY num DESC";
//echo $sql.'<br/>';
$rs = getRs($sql);
$a_postcode = array();
$i = 0;
while ($row = mysqli_fetch_assoc($rs)) {
  $a_postcode[] = $row;
  $i++;
  if ($i==10) break;
}
// earliest date: 2013-04-14
/*$piwik_start_date = '2013-04-14';
$piwik_end_date = date('Y-m-d');
if ($end_date) $piwik_end_date = date('Y-m-d', $end_date);
//if ($start_date && $start_date > )
if ($start_date) $piwik_start_date = date('Y-m-d', $start_date);
if ($piwik_start_date >= '2013-04-14') {
  if ('2013-04-14' == $piwik_start_date) {
    $sql = "SELECT COUNT(id) AS num FROM byp_Quote WHERE dates_created >= ".strtotime('2013-04-14');
    if ($end_date) $sql .= ' AND dates_created <= '.$end_date;
    $sql .= "  AND postcode IS NOT NULL AND postcode<>''";
    $rs = getRs($sql);
    $row = mysqli_fetch_assoc($rs);
    $total_piwik_start = $row['num'];
  }*/
  /*
  define('PIWIK_INCLUDE_PATH', realpath('../inc/piwik'));
  define('PIWIK_USER_PATH', realpath('../inc/piwik'));
  define('PIWIK_ENABLE_DISPATCH', false);
  define('PIWIK_ENABLE_ERROR_HANDLER', false);
  define('PIWIK_ENABLE_SESSION_START', false);
  require_once PIWIK_INCLUDE_PATH . "/index.php";
  require_once PIWIK_INCLUDE_PATH . "/core/FrontController.php";
  require_once PIWIK_INCLUDE_PATH . "/core/API/Request.php";
  FrontController::getInstance()->init();
  $request = new Request("
      method=Actions.getPageUrl
      &idSite=1
      &date=$start_date,$end_date
      &period=range
      &format=XML
      &pageUrl=/build-your-price-app.html
      &token_auth=9a726be1212793f39cdb9f8eec2a0e13
  ");
  // Calls the API and fetch XML data back
  $result = $request->process();
  */
  /*$token_auth = '9a726be1212793f39cdb9f8eec2a0e13';
  // we call the REST API and request the 100 first keywords for the last month for the idsite=1
  $url = "http://buildteam.com/inc/piwik/";
  $url .= "?module=API&method=Actions.getPageUrl";
  $url .= "&idSite=1&period=range&date=$piwik_start_date,$piwik_end_date";
  $url .= "&format=PHP&pageUrl=/build-your-price-app.html";
  $url .= "&token_auth=$token_auth";
  $fetched = file_get_contents($url);
  $a_piwik = unserialize($fetched);
}*/
if ($err_msg) echo '<p style="color:red;font-weight:bold">'.$err_msg.'</p>';
?>
<form method="get" action="rfq_newsummary.php">
  Start Date:
  <input type="text" name="start_date" size="10" class="date_range" value="<?php echo $_GET['start_date'] ?>" />
  End Date:
  <input type="text" name="end_date" size="10" class="date_range" value="<?php echo $_GET['end_date'] ?>" />
  <input type="submit" value="Apply Date Range" />
</form>
<br/>
<h3 class="head_totals">No of Quotes generated: <span><?php echo $total ?></span>; Average quote amount: <span>&pound;<?php echo number_format(round($a_avg['avg_amount'])) ?></span>; Average floor area: <span><?php echo round($a_avg['avg_floor'], 2) ?>m<sup>2</sup></span></h3>
<div id="piechart" style="border:1px solid silver;width: 500px; height: 300px;float:left;margin-right:14px;margin-bottom:14px"></div>
<div id="chart_div" style="border:1px solid silver;width:500px;height:300px;float:left;margin-bottom:14px"></div>
<br clear="all" />
<div id="columnchart3" style="border:1px solid silver;width: 500px; height: 300px;float:left;margin-right:14px;margin-bottom:14px"></div>
<div id="columnchart4" style="border:1px solid silver;width: 500px; height: 500px;float:left;margin-bottom:14px"></div>
<br clear="all" />
<?php
//echo '<pre>';
//echo print_r($a_cover,1)."\n\n";
//echo print_r($a_avg,1)."\n\n";
//echo print_r($a_status,1)."\n\n";
//echo print_r($a_start,1)."\n\n";
//echo print_r($a_postcode,1);
/*$piwik_notes = 'To get &quot;Conversion Rate&quot; PIWIK data is used: uniquie pageviews of /build-your-price-app.html <br/>PIWIK was installed on server at 04/14/2013.<br/>';
if ($piwik_start_date >= '2013-04-14') {
  //echo "\n\n".print_r($a_piwik,1).' '.$a_piwik[0]['nb_visits'];#debug
  $piwik_notes .= '('.$a_piwik[0]['nb_visits'].' uniquie pageviews from '.$_GET['start_date'].($_GET['end_date']?' till '.$_GET['end_date']:'').')<br/>';
  if (isset($total_piwik_start)) {
    $piwik_notes .= 'Total quotes after PIWIK install: '.$total_piwik_start.'<br/>';
    $conversion_rate = round($total_piwik_start/$a_piwik[0]['nb_visits']*100, 2);
  }
  else $conversion_rate = round($total/$a_piwik[0]['nb_visits']*100, 2);
  //echo 'Conversion Rate: '.$conversion_rate.'%';
}
else {
  $piwik_notes = 'To see &quot;Conversion Rate&quot; please select Start Date later or equal 04/14/2013.<br/>';
}*/
if ('date_created > 0' == $sql_date) $sql_date = "date_created >= ".strtotime("January 1");
$sql = "SELECT id, refcode, postcode, ABS(totalPrice) AS amount, address, `lookingStart`, `currentstatus` FROM byp_Quote WHERE $sql_date  AND postcode IS NOT NULL AND postcode<>''";
$rs = getRs($sql);
$limit_markers = 1000; // 200
$total_makers = mysqli_num_rows($rs);
$a_marker = array();
$i = 0;
while ($row = mysqli_fetch_assoc($rs)) {
  if (!in_array($row['postcode'], array_keys($a_marker))) {
    $a_marker[$row['postcode']] = $row;
    $a_marker[$row['postcode']]['rfq_ids'] = array();
    $a_marker[$row['postcode']]['rfq_codes'] = array();
    $i++;
  }
  else {
    $a_marker[$row['postcode']]['rfq_ids'][] = $row['rfq_id'];
    $a_marker[$row['postcode']]['rfq_codes'][] = $row['rfq_code'];
  }
  if ($i>=$limit_markers) break;
}
if ($a_marker) {
  $postcodes = "";
  foreach ($a_marker AS $k => $v) $postcodes .= "'".mysql_real_escape_string($k)."',";
  $postcodes = substr($postcodes, 0, -1);
  // get latitude, longitude, select another DB
  global $dbconn, $dbname_uk_postcode;
  if (!mysql_select_db($dbname_uk_postcode, $dbconn)) die("Can't select the ".$dbname_uk_postcode.' database!');
  $sql = "SELECT postcode, latitude, longitude FROM uk_postcode WHERE postcode IN(".$postcodes.")";
  $rs = getRs($sql);
  $a_latlng = array();
  while ($row = mysqli_fetch_assoc($rs)) {
    $a_latlng[ $row['postcode'] ] = array('lat' => $row['latitude'], 'lng' => $row['longitude']);
  }
  // select the main database back
  if (!mysql_select_db($dbname, $dbconn)) die("Can't select the ".$dbname.' database!');
}
//echo '<pre>'.print_r($a_marker,1).'</pre>';#debug
//echo '</pre>';#debug
////////////////////////////////////////////////////////////////////////////////
?>
<!--h3>
  <?php if (isset($conversion_rate)) echo 'Conversion Rate: <span>'.$conversion_rate.'%</span>'; ?>
</h3>
<p><?php echo $piwik_notes; ?></p-->
<br clear="all" />
<div id="map-canvas" style="width:640px;height:540px;float:left;margin-right:14px"></div>
To prevent map overloading the maximum postcode quotes displayed is <?php echo $limit_markers ?> (total: <?php echo $total_makers ?>). <br/>
<br/>
You can filter map results by specifying Start and End Dates 
<!--If no Start Date is selected, YTD (Year-to-date: January 1 is used for map results)--> 
<script type="text/javascript">
  google.load("visualization", "1", {packages:["corechart"]});
  google.setOnLoadCallback(drawChart);
  function drawChart() {
    var data = google.visualization.arrayToDataTable([
      ['In Territory', 'Out of Territory'],
      <?php
      $i = 0;
      //foreach ($a_cover AS $k => $v) {
      foreach ($a_cover AS $k => $v) {
        $i++;
        //echo "['".('in'==$k?'In Territory':('out'==$k?'Out of Territory':$k))."', $v]";
		if('out'==$k)
			$trName = 'Out of Territory';
		else
			$trName = 'In Territory';
        echo "['".$trName."', $v]";
        if ($i<=count($a_cover)) echo ",\n";
      }
      ?>
    ]);
    var options = {
      title: 'Territory Cover'
    };
    var chart = new google.visualization.PieChart(document.getElementById('piechart'));
    chart.draw(data, options);
    options = {
      title: 'When Start',
      legend: { position: "none" },
      orientation: 'vertical'
    };
    data = google.visualization.arrayToDataTable([
      ['When Start', 'Number', { role: 'style' }],
      <?php
      $i = 0;
      foreach ($a_start AS $k => $v) {
        $i++;
        echo "['".('empty'==$k?'Not Specified':ucfirst($k))."', $v, '".('empty'==$k?'':'#5b3ab6')."']";
        if ($i<count($a_start)) echo ",\n";
      }
      ?>
      ]);
    var view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
     { calc: "stringify",
       sourceColumn: 1,
       type: "string",
       role: "annotation" },
     2]);
    chart = new google.visualization.ColumnChart(document.getElementById('chart_div'));
    chart.draw(view, options);
    options = {
      title: 'Current Status',
      legend: { position: "none" },
      orientation: 'vertical'
    };
    data = google.visualization.arrayToDataTable([
      ['Current Status', 'Number', { role: 'style' }],
      <?php
      $i = 0;
      foreach ($a_status AS $k => $v) {
        $i++;
        echo "['".('empty'==$k?'Not Specified':ucfirst($k))."', $v, '".('empty'==$k?'':'#5b3ab6')."']";
        if ($i<count($a_status)) echo ",\n";
      }
      ?>
      ]);
    view = new google.visualization.DataView(data);
    view.setColumns([0, 1,
     { calc: "stringify",
       sourceColumn: 1,
       type: "string",
       role: "annotation" },
     2]);
    chart = new google.visualization.ColumnChart(document.getElementById('columnchart3'));
    chart.draw(view, options);
    options = {
      title: '10 most popular postcodes (with average quote amounts)',
      legend: { position: "none" }/*,
      orientation: 'vertical'*/
    };
    data = google.visualization.arrayToDataTable([
      ['Postcode', 'Number', { role: 'style' }, { role: 'annotation' }],
      <?php
      $i = 0;
      foreach ($a_postcode AS $k => $v) {
        $i++;
        echo "['".$v['prefix']." £".number_format(round($v['avg_amount']))."', {$v['num']}, '#a300aa', '{$v['num']}']";
        if ($i<count($a_postcode)) echo ",\n";
      }
      ?>
    ]);
    chart = new google.visualization.ColumnChart(document.getElementById('columnchart4'));
    chart.draw(data, options);
  }
  function initialize() {
    var map;
    var bounds = new google.maps.LatLngBounds();
    var mapOptions = {
      mapTypeId: 'roadmap'
      //center: new google.maps.LatLng(51.465362,-0.134233),
      //zoom: 9
    };
    map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
    var markers = [
    <?php 
      /*['London, SW4 6DH', 51.465362, -0.134233, 'London, SW4 6DH Content'],
      ['London Eye, London', 51.503454,-0.119562, 'London Eye, London Content'],
      ['Palace of Westminster, London', 51.499633,-0.124755, 'Palace of Westminster, London Content']*/
      foreach ($a_marker AS $k => $v) {
        if (!isset($a_latlng[ $k ])) unset($a_marker[$k]);
      }
      $i = 0;
      foreach ($a_marker AS $k => $v) {
        echo "[";
        $title = $k; // $v['postcode']
        if ($v['address']) $title .= ' '. $v['address'];
        if ($v['rfq_ids']) $title .= ' x'.(count($v['rfq_ids'])+1).' quotes';
        $title = str_replace("\'", "{esc_slash}", $title);
        $title = str_replace("'", "\'", $title);
        $title = str_replace("{esc_slash}", "\'", $title);
        $title = str_replace("\r", " ", $title);
        $title = str_replace("\n", " ", $title);
        echo "'$title', {$a_latlng[ $k ]['lat']}, {$a_latlng[ $k ]['lng']}, '";
        if ($v['rfq_ids']) {
          $buf = '<a href="rfq_quotes.php?id='.$v['rfq_id'].'" target="_blank">'.$v['rfq_code'].'</a>, ';
          foreach ($v['rfq_ids'] AS $k2 => $v2) {
            $buf .= '<a href="rfq_quotes.php?id='.$v2.'" target="_blank">'.$v['rfq_codes'][$k2].'</a>, ';
          }
          $buf = substr($buf, 0, -2);
          echo $buf;
        }
        else {
          echo "&pound;".number_format($v['amount'])." ".$v['start'].', '.$v['status']."<br/>";
          echo '<a href="rfq_quotes.php?id='.$v['rfq_id'].'" target="_blank">'.$v['rfq_code'].'</a>';
        }
        echo "'";
        echo "]";
        $i++;
        if ($i<count($a_marker)) echo ",\n";
      }
    ?>
    ];
    var infoWindow = new google.maps.InfoWindow(), marker, i, position;
    for( i = 0; i < markers.length; i++ ) {
        position = new google.maps.LatLng(markers[i][1], markers[i][2]);
        bounds.extend(position);
        marker = new google.maps.Marker({
            position: position,
            map: map,
            title: markers[i][0]
        });
        // Allow each marker to have an info window    
        google.maps.event.addListener(marker, 'click', (function(marker, i) {
            return function() {
                infoWindow.setContent('<div class="info_content"><strong>'+ markers[i][0] +'</strong><br/>'+markers[i][3]+'</div>');
                infoWindow.open(map, marker);
            }
        })(marker, i));
        // Automatically center the map fitting all markers on the screen
        map.fitBounds(bounds);
    }
  }
  google.maps.event.addDomListener(window, 'load', initialize);
  $(document).ready(function() {
    // Datepicker
    $('.date_range').datepicker({
      showOn: 'focus',
      constrainInput: true
    });
  });
</script>
<?php
require ('./inc/footer.inc.php');
?>
