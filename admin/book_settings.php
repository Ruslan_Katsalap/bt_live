<?php

$key_field = 'id';
$value_field = 'value';

require ('./inc/header.inc.php');

include ('./map.php');

$rs = getRs("SELECT $key_field AS name, $value_field AS value FROM btcrm_settings");
$a_setting = array();

while ($row = mysqli_fetch_assoc($rs)) {
  $a_setting[$row['name']] = $row['value'];
}
//print_r($a_setting);
if (isset($_POST['nonce']) && $_POST['nonce']==session_id()) {
    $_POST['selected_date'] = serialize(array_unique($_POST['selected_date']));
    foreach ($_POST AS $k => $v) {
      if (in_array($k, array_keys($a_setting))) {
        
        if (get_magic_quotes_gpc()) {
          $v = stripslashes($v);
        }
        
        $sql = "UPDATE btcrm_settings SET $value_field = '".mysql_real_escape_string($v)."' WHERE $key_field = '".mysql_real_escape_string($k)."'";
        //echo $sql;
        setRs($sql);
        $a_setting[$k] = $v;
      }
    }

}

$a_setting['selected_date'] = unserialize($a_setting['selected_date']);

$ret = '<form action="book_settings.php" method="post" enctype="multipart/form-data">';
$ret .= '<input type="hidden" name="nonce" value="'.htmlspecialchars(session_id(), ENT_QUOTES, 'UTF-8').'" />';

$ret .= "<h1>Settings</h1>";

if (isset($_POST['nonce']) && $_POST['nonce']==session_id()) {
  $ret .= '<h2>Settiings are saved successfully!</h2>';
}

$ret .= "<table width=\"100%\">
<script>
  $( function() {
    $( '.datepicker' ).datepicker({
		onSelect: function(dateText, inst) { 
        var date = $(this).datepicker('getDate'),
            day  = date.getDate(),  
			day2  = date.getDate() + 1,  
            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],            year =  date.getFullYear();
				function getMonthShortName(month) {
    			return months[month];
				}
        		//alert(day + '-' + months);
				$('.date1').html(day);
				$('.date2').html(day2);
				$('.month').html(getMonthShortName(date.getMonth()));
			},
		
		});
		
  });
  $( function() {
    $( '.datepicker1' ).datepicker({
		onSelect: function(dateText, inst) { 
        var date = $(this).datepicker('getDate'),
            day  = date.getDate(),  
			day2  = date.getDate() + 1,  
            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],            year =  date.getFullYear();
				function getMonthShortName(month) {
    			return months[month];
				}
        		//alert(day + '-' + months);
				$('.date1').html(day);
				$('.date2').html(day2);
				$('.month').html(getMonthShortName(date.getMonth()));
			},
		
		});
		
  });
  $( function() {
    $( '.datepicker2' ).datepicker({
		onSelect: function(dateText, inst) { 
        var date = $(this).datepicker('getDate'),
            day  = date.getDate(),  
			day2  = date.getDate() + 1,  
            months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],            year =  date.getFullYear();
				function getMonthShortName(month) {
    			return months[month];
				}
        		//alert(day + '-' + months);
				$('.date1').html(day);
				$('.date2').html(day2);
				$('.month').html(getMonthShortName(date.getMonth()));
			},
		
		});
		
  });
  function addDate(){
	  var dateVal = $('.datepicker').val();
	  if(dateVal!=''){
		  var iddateVal = dateVal.replace('/','-');
		  iddateVal = iddateVal.replace('/','-');
		$('#addDate').append('<span id='+iddateVal+' style=\'margin:0 10px 0 0;\'>'+dateVal+'<input type=hidden name=selected_date[] value='+dateVal+'> <a onclick=deleteDate(&#39;'+iddateVal+'&#39;);>X</a></span>');//&nbsp;&nbsp;&nbsp;
		$('.datepicker').val('');
	  }else{
		  alert('Please select date.');
	  }
  }
  function deleteDate(id){
	//$('#'+id).html('');
	$('#'+id).remove();	
  }
  </script>
";


	$ret .= "<tr><td class=\"td_2_1\" style=\"width:140px;padding:3px\">Coupon functionality</td><td class=\"td_3\" style=\"width:500px;padding:10px\"><input type=\"radio\" name=\"coupon_enabled\" id=\"coupon_enabled_on\" value=\"1\"".($a_setting['coupon_enabled']?' checked="checked"':'')." /> <label for=\"coupon_enabled_on\">Enabled</label> <input type=\"radio\" name=\"coupon_enabled\" id=\"coupon_enabled_off\" value=\"0\"".(!$a_setting['coupon_enabled']?' checked="checked"':'')." /> <label for=\"coupon_enabled_off\">Disabled</label></td></tr>";
  
    $ret .= '<tr><td class="td_2_1" style="padding:3px">Admin email to receive booked visits</td><td class="td_3" style="width:500px;padding:10px"><input type="text" name="admin_email" value="' . htmlspecialchars($a_setting['admin_email'], ENT_QUOTES, 'UTF-8') . '" class="input" /></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">Confirm email Subject</td><td class="td_3" style="width:500px;padding:10px"><input type="text" name="confirm_subj" value="' . htmlspecialchars($a_setting['confirm_subj'], ENT_QUOTES, 'UTF-8') . '" class="input" /></td></tr>';
    
	$ret .= '<tr><td class="td_2_1" valign="top" style="padding:3px">Confirm email Message<br><br><em>Placeholders:</em><pre>'."\n\n".'[full name]'."\n".'[ref number]'."\n".'[availability]'."\n".'[postcode]'."\n".'[price]'."\n".'[visit type]'."\n".'[comments]'."\n\n".'</pre></td><td class="td_3" style="width:500px;padding:10px"><textarea name="confirm_email" class="input" cols="80" rows="11">' . htmlspecialchars($a_setting['confirm_email'], ENT_QUOTES, 'UTF-8') . '</textarea></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">Standard visit price, &pound;</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="standard_price" value="' . (int)$a_setting['standard_price'] . '" class="input" /></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">Standard visit price + VAT, &pound;</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="standard_price_vat" value="' . (int)$a_setting['standard_price_vat'] . '" class="input" /></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">Premium visit price, &pound;</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="premium_price" value="' . (int)$a_setting['premium_price'] . '" class="input" /></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">Premium visit price + VAT, &pound;</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="premium_price_vat" value="' . (int)$a_setting['premium_price_vat'] . '" class="input" /></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">3D Premium visit price, &pound;</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="3dpremium_price" value="' . (int)$a_setting['3dpremium_price'] . '" class="input" /></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">3D Premium visit price + VAT, &pound;</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="3dpremium_price_vat" value="' . (int)$a_setting['3dpremium_price_vat'] . '" class="input" /></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">Pre-Purchase visit price, &pound;</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="prepurchase_price" value="' . (int)$a_setting['prepurchase_price'] . '" class="input" /></td></tr>';

	$ret .= '<tr><td class="td_2_1" style="padding:3px">Pre-Purchase visit price + VAT, &pound;</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="prepurchase_price_vat" value="' . (int)$a_setting['prepurchase_price_vat'] . '" class="input" /></td></tr>';
	
	$ret .= '<tr><td class="td_2_1" style="padding:3px">From Date</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="dateFrom" value="' . $a_setting['dateFrom'] . '" class="input datepicker1" /></td></tr>';
	
	$ret .= '<tr><td class="td_2_1" style="padding:3px">To Date</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px" name="dateTo" value="' . $a_setting['dateTo'] . '" class="input datepicker2" /></td></tr>';
	
	$ret .= '<tr><td class="td_2_1" style="padding:3px">Selected Date:</td><td class="td_3" style="padding:10px"><input type="text" style="width:100px"  value="" class="input datepicker" /><a onclick="addDate()">Add</a>
		<div id="addDate">';
		foreach($a_setting['selected_date'] AS $selected_date){
			$iddateVal = date('m-d-Y', strtotime($selected_date));
			$dateVal = date('m/d/Y', strtotime($selected_date));
			$ret .=	 '<span id='.$iddateVal.' style="margin:0 10px 0 0;">'.$dateVal.'<input type=hidden name=selected_date[] value='.$dateVal.'> <a onclick=deleteDate(&#39;'.$iddateVal.'&#39;);>X</a></span>';//&nbsp;&nbsp;&nbsp;
		}
	$ret .=	'</div></td></tr>';

	$ret .= '</table>';


    /*
	$ret .= "<br /><br /><h1>Admin Notification</h1>";

	$ret .= "<table>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"width:200px;padding:3px\">Admin E-mail <br/><br/><i>receives quotes from all postcodes</i></td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"text\" name=\"admin_email\" value=\"" . $row['admin_email'] . "\" class=\"input\" /></td></tr>";

	$ret .= "<tr><tr><td class=\"td_2_1\" style=\"padding:3px\">Notify Admin (when new quotes arrive)</td><td class=\"td_3\" style=\"width:100px;padding:10px\"><input type=\"checkbox\" name=\"is_notify_admin\" value=\"1\"";

	if ($row['is_notify_admin']) {

		$ret .= " checked";

	}

	$ret .= " /></td></tr>";
  
  $ret .= "</table>";
  */

$ret .= "<br /><br /><input type=\"submit\" value=\"Save Changes\" class=\"button\" />";

$ret .= "</form>";

$ret .= '<script type="text/javascript">CKEDITOR.replace( "confirm_email", {height:300} );</script>';

echo $ret;

require ('./inc/footer.inc.php');



?>