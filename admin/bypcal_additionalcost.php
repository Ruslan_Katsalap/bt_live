<?php

$PageTitle = "BYP Calculator Additional Cost";
$TableName = "bypcal_additionalcost";
$PrimaryKey = "id";
$FieldNames = "id,removing_chimney,ad_wc,underfloor_heating,all_glass";
$DisplayNames = "ID,Removing Chimney,Ad W/C, Underfloor Heating, All Glass";
$ModFieldNames = "id,removing_chimney,ad_wc,underfloor_heating,all_glass";
$ModDisplayNames = "ID,Removing Chimney,Ad W/C, Underfloor Heating, All Glass";
/*$ModFieldTypes = "-1,1,1";
$AllowDelete = false;
*/
require ('./inc/tbl.inc_house.php');

?>