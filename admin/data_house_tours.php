<?php

$PageTitle = "House Tours";
$TableName = "events";
$PrimaryKey = "event_id";
$FieldNames = "event_id,event_project,event_area,event_date,is_enabled";
$DisplayNames = "ID,Project,Area,Date,Enabled";
$ModFieldNames = "event_id,event_project,event_area,event_date,is_displayed,is_enabled";
$ModDisplayNames = "ID,Project,Area,Date,Display,Enabled";
$ModFieldTypes = "-1,2,2,2,0,0";
$AllowDelete = true;

require ('./inc/tbl.inc.php');

?>