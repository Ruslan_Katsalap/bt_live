<?php

$PageTitle = "Build Only Enquires";

$TableName = "architectural_plans";

$PrimaryKey = "archi_id";

$FieldNames = "archi_id,dates,full_name,address,email,telephone,planning_url,looking_to_start,message,imageName";

$DisplayNames = "ID,Date,Full Name, Address, Email, Telephone, Planning URL, When to Start, Message,File Name";

$ModFieldNames = "archi_id,dates,full_name,address,email,telephone,planning_url,looking_to_start,message,imageName";

$ModDisplayNames = "ID,Date,Full Name, Address, Email, Telephone, Planning URL, When to Start, Message,File Name";

$ModFieldTypes = "-1,2,2,2,2,2,2,4,4,11";

$AllowDelete = false;

$AllowAdd = false;

//require_once ('./inc/util.inc.php');



require ('./inc/tbl.inc.php');

?>