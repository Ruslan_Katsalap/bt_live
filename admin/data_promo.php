<?php

$PageTitle = "Promo";
$TableName = "promo";
$PrimaryKey = "promo_id";
$FieldNames = "promo_id,promo_name,is_enabled";
$DisplayNames = "ID,Name,Enabled";
$ModFieldNames = "promo_id,promo_name,imagefile,bgcolor,url,sort,is_enabled";
$ModDisplayNames = "ID,Name,Image,Bg Color,Url,Sort,Enabled";
$ModFieldTypes = "-1,2,2,2,2,1,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>