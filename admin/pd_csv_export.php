<?php 

require_once ('./inc/util.inc.php');

$FieldNames = "id,title,property,borough,roof,floor,pdffile";

$rs = getRs("SELECT pd.$FieldNames, b.name AS borough_name, pd.date_created FROM design_planning pd LEFT JOIN borough b ON (pd.borough=b.id) ORDER BY id ASC");

header('Content-Disposition: attachment; filename="design_planning_'.date('Y-m-d_H-i').'.csv"');
header('Content-Type: text/plain; charset=UTF-8');

$a_property = array('h' => 'House', 'f' => 'Flat');
$a_roof = array('sv' => 'Slate & Velux', 'f' => 'Flat Roof', 'g' => 'All Glass');

$sep = ','; // ,
if (isset($_GET['semicolon'])) $sep = ';';
// separator symbol ; or ,

$DisplayNames = 'ID;Project Name;Property Type;Borough;Roof;Floor Area, m2;PDF file;Preview Image';

$a_head = explode(';', $DisplayNames);

echo '"' . implode('"'.$sep.'"', $a_head) . '"' . "\r\n";


while ($row = mysqli_fetch_assoc($rs)) {
  
  $basename = explode('.', $row['pdffile']);
  array_pop($basename);
  $basename = implode('.', $basename);
  
  echo $row['id'].$sep.'"'.$row['title'].'"'.$sep.'"'.$a_property[$row['property']].'"'.$sep.
  
  '"'.$row['borough_name'].'"'.$sep.'"'.$a_roof[$row['roof']].'"'.$sep.'"'.$row['floor'].'"'.$sep.

  '"http://www.buildteam.com/pdf/design-planning/'.$row['pdffile'].'"'.$sep.
  
  '"http://www.buildteam.com/pdf/design-planning/thumb/'.$basename.'.png?v='.$row['date_created'].'"'.
  
  "\r\n";
  
}

?>