<?php

$PageTitle = "Project Floor Area";
$TableName = "bundle_project_formula";
$PrimaryKey = "id";
$FieldNames = "id,product,sub_product,price,vat";
$DisplayNames = "ID,Product,Sub Product,Price,Vat";
$ModFieldNames = "id,price,vat";
$ModDisplayNames = "ID,Price,Vat";
$ModFieldTypes = "-1,2,2";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>