<?php

$PageTitle = "Project Floor Area";
$TableName = "project_floorarea";
$PrimaryKey = "id";
$FieldNames = "id,floor_area";
$DisplayNames = "ID,Floor Area";
$ModFieldNames = "id,floor_area";
$ModDisplayNames = "ID,Floor Area";
$ModFieldTypes = "-1,2";
$AllowDelete = true;

require ('./inc/tbl.inc.php');

?>