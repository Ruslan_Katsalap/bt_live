<?php

$PageTitle = "Event Photos";
$TableName = "event_photos";
$PrimaryKey = "photo_id";
$FieldNames = "photo_id,photo_title,date_created,sort,is_enabled";
$DisplayNames = "ID,Title,Upload Date,Sort,Enabled";
$ModFieldNames = "photo_id,photo_title,imagefile,sort,is_enabled";
$ModDisplayNames = "ID,Title,Image,Sort,Enabled";
$ModFieldTypes = "-1,2,99,1,0";
$AllowDelete = false;

if ('POST'== $_SERVER['REQUEST_METHOD']) {
  require_once ('./inc/util.inc.php');
  if (isset($_POST['btn_delete']) && $_POST['btn_delete'] && isset($_POST['photo_id']) && $_POST['photo_id'] && isset($_POST['nonce']) && session_id()==$_POST['nonce']) {

    $rs = getRs("SELECT photo_id, imagefile FROM event_photos WHERE photo_id = ".(int)$_POST['photo_id']);
    
    $row = mysqli_fetch_assoc($rs);
    
    if ($row && isset($row['photo_id'])) {
      
      $path = str_replace('\\', '/', dirname(__FILE__));
      
      $path = explode('/', $path);
      array_pop($path);
      $path = implode('/', $path);
      
      //echo $path.'<br/>';
      
      if ($row['imagefile']) {
        unlink($path.'/images/events/'.$row['imagefile']);
        unlink($path.'/images/events/preview/'.$row['imagefile']);
        unlink($path.'/images/events/thumb/'.$row['imagefile']);
      }
      
      setRs('DELETE FROM event_photos WHERE photo_id='.$row['photo_id']);
      
      //print_r($row);
      //exit;
      
    }
    header('Location: data_events.php?p='.(isset($_POST['p'])?$_POST['p']:'').'&kw='.(isset($_POST['kw'])?$_POST['kw']:''));
    exit;
  }
}


require_once ('./inc/util.inc.php');


function generateThumb($imagefile) {
  global $ROOT_FOLDER;
  
  require_once($ROOT_FOLDER.'phpthumb/phpthumb.class.php');
  
  $phpThumb = new phpThumb();
  
  $phpThumb->setSourceFilename( $ROOT_FOLDER . 'images/events/' . $imagefile );
  
  $phpThumb->setParameter('h', 800);
  $phpThumb->setParameter('config_output_format', 'jpeg');
  $phpThumb->setParameter('q', 92);
  //$phpThumb->setParameter('zc', 1);
  $phpThumb->setParameter('w', 1280);
  
  $ok = false;
  
  if ($phpThumb->GenerateThumbnail()) {
    $ok = $phpThumb->RenderToFile($ROOT_FOLDER . 'images/events/preview/' . $imagefile);
  }
    
  $phpThumb->resetObject();
  
  $phpThumb->setSourceFilename( $ROOT_FOLDER . 'images/events/' . $imagefile );
  $phpThumb->setParameter('config_output_format', 'jpeg');
  $phpThumb->setParameter('q', 92);
  $phpThumb->setParameter('h', 210);
  $phpThumb->setParameter('w', 210);
  $phpThumb->setParameter('far', 'C');
  
  if ($phpThumb->GenerateThumbnail()) {
    $ok = $phpThumb->RenderToFile($ROOT_FOLDER . 'images/events/thumb/' . $imagefile);
  }
  
  //echo $imagefile.':)';#debug
  
  return $ok;
}


// auto import
$rs = getRs("SELECT photo_id, imagefile, sort FROM event_photos");
$a_files = array();
$sort = 0;
while ($row = mysqli_fetch_assoc($rs)) {
  $a_files[] = $row['imagefile'];
  $row['sort'] = (int)$row['sort'];
  if ($row['sort'] > $sort) $sort = $row['sort'];
}

$path = $ROOT_FOLDER . 'images/events/';

if (is_dir($path)) {
    if ($dh = opendir($path)) {
        set_time_limit(0);
        while (($file = readdir($dh)) !== false) {
          if (filetype($path . $file) === 'file' && !in_array($file, $a_files)) {
            
            $a_ext = explode('.', $file);
            $ext = strtolower(array_pop($a_ext));
            
            if ('jpg' == $ext || 'jpeg' == $ext || 'gif' == $ext || 'png' == $ext) {
              
              $imagefile = getUniqueFilename($file);
              
              //echo "filename: $file $sort $ext : $imagefile" . "<br/>";#debug
              
              $sort += 10;
              
              $sql = "INSERT INTO event_photos SET photo_title='".mysql_real_escape_string(implode('.', $a_ext))."', imagefile='".mysql_real_escape_string($imagefile)."', sort=$sort, is_enabled=1, is_active=1, date_created=UNIX_TIMESTAMP(), date_modified=UNIX_TIMESTAMP()";
              
              //echo $sql.'<br/>';#debug
              
              setRs($sql);
              
              rename($path . $file, $path . $imagefile);
              
              generateThumb($imagefile);
              
            }
          }
        }
        closedir($dh);
    }
}


require ('./inc/tbl.inc.php');

?>