<?php

$PageTitle = "Room Size Estimate";
$TableName = "room_size_estimate";
$PrimaryKey = "room_size_estimate_id";
$FieldNames = "room_size_estimate_id,num_bedrooms,terrace_type_id,room_size";
$DisplayNames = "ID,# Bedrooms,Terrace Type,Room Size";
$ModFieldNames = "room_size_estimate_id,num_bedrooms,terrace_type_id,room_size,is_enabled";
$ModDisplayNames = "ID,# Bedrooms,Terrace Type,Room Size,Enabled";
$ModFieldTypes = "-1,1,1,1,0";
$AllowDelete = false;

require ('./inc/tbl.inc.php');

?>