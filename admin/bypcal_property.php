<?php

$PageTitle = "About Property";
$TableName = "about_property";
$PrimaryKey = "property_id";
$FieldNames = "property_id,property_name";
$DisplayNames = "ID,Property Name";
$ModFieldNames = "property_id,property_name";
$ModDisplayNames = "ID,Property Name";
/*$ModFieldTypes = "-1,2";
$AllowDelete = false;
*/
require ('./inc/tbl.inc_house.php');

?>