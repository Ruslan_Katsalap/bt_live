<?php
require_once('./inc/header.inc.php');
?>
<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0,  maximum-scale=1.0, user-scalable=no">
	<title>Build Your Price | Build Team</title>
	<link href="assets/css/style.css?v=5" rel="stylesheet" type="text/css" />
	<link href="assets/css/media.css" rel="stylesheet" type="text/css" />

	<link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
	<link href="assets/css/owl.theme.css" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lora" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Questrial' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script type="text/javascript" src="assets/js/html5shiv.min.js"></script>
	<script type="text/javascript" src="assets/js/jquery-1.9.1.min.js"></script> 
	<script type="text/javascript" src="assets/js/jquery-ui-1.8.24.custom.min.js"></script>
	<script type="text/javascript" src="assets/js/customslideshow.js"></script>
	<link href="https://fonts.googleapis.com/css?family=Droid+Serif" rel="stylesheet"> 
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet"> 
	<link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet">
	<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
	<script>
		$(document).ready(function() {

			$(".owl-carousel").owlCarousel({

      navigation : true, // Show next and prev buttons
      slideSpeed : 1000,
      paginationSpeed : 1000,
      singleItem:true

      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false

  });

		});
	</script>
	<script>
		$(document).ready(function() {
 		// tips 
 		$(".cal_help").hover(function(){ $(this).find(".cal_tip_areas").fadeIn(200); });

 		$(".cal_help").mouseleave(function(){ $(this).find(".cal_tip_areas").hide(); });


 		$(".close").click(function(){

 			$(".cal_tip_area").fadeOut();	






 		});


 		$(".cal_help").hover(function(){ $(this).find(".cal_tip_area1").toggle(); });

 		//$(".cal_help").mouseleave(function(){ $(this).find(".cal_tip_area1").hide(); });


 		$(".close").click(function(){

 			$(".cal_tip_area1").fadeOut();	






 		});
 		/*** faq ***/
		$(".next_btn").click(function(){
			$(".field_set wrapper .main_list_col .list_extend:first-child").slideDown("fast");
			});
		
 		$(".main_list_col .mail_list").click(function(){
			$(".main_list_col .mail_list").removeClass("active");
			$(this).addClass("active");
			
			
			
 			//$(this).find(".fa-plus").switchClass( "fa-plus", "fa-minus", 200 );
 			$(".main_list_col .mail_list").find(".fa-minus").switchClass( "fa-minus", "fa-plus", 200 );
			$(this).parent(".main_list_col").children(".list_extend").slideUp("slow");
 			$(this).next(".list_extend").stop('true','true').slideToggle("slow", function(){
				$(this).find(".fa-plus").switchClass( "fa-plus", "fa-minus", 200 );
				});
			$(".main_list_col li.active").find(".fa-plus").switchClass( "fa-plus", "fa-minus", 200 );	

 		});


 		});



 </script> 
 
 
<style>
.form_input.error_msg[name="postcode_start"] {
  border: 2px solid #f00;
}
.post_code_blue {
  background: rgba(35, 74, 125, 0.7) none repeat scroll 0 0;
  color: #fff;
  padding: 8px 0 20px;
  text-align: center;
  width: 60%;
  float:left;
}
.post_code_blue > label {
  float: left;
  margin-bottom: 10px;
  text-align: center;
  width: 100%;
}
.post_code_blue > .form_input {
  height: 32px;
  padding-left: 10px;
  width: 85%;
}
#startKitchenQuote > input[type="submit"] {
  background: #f4641e none repeat scroll 0 0;
  border: medium none;
  color: #fff;
  cursor: pointer;
  float: left;
  font-size: 20px;
  padding: 31px 35px;
}
.clearfix {
	clear:both;
	}
.lightblue-bg .darkblue-txt {
	
	}
#latest_quotes_container {
	padding:0px !important;
	}	
ul.byp_landing_list li {
   	color:#FFF;
    text-align: left;
    font-size: 12px !important;
}
ul.byp_landing_list {
	list-style:none !important;
	margin-left:0px !important;
	}
ul.byp_landing_list li:before {
	content: "•";
   font-size: 170%; /* or whatever */
   padding-right: 5px;
   vertical-align:middle;
	}	
.byp_top_wrap {
	margin-left:0px !important;
	background: #6687a6;
	padding:20px 20px 20px 30px !important;
	width:100% !important; 
	}
.byp-btn {
	margin-top: 20px !important;
    background: #6687a6 !important;
    color: #fff;
}	
ul#latest-quotes-slide a {
    color: #6687a6 !important;
}
.listtrem {
	color:#FFF;
	text-align:left;
	font-size:10px;
	}	
.byp-title, .byp_top_wrap, .byp-btn, .bottomnavbyp li a  {
	font-family:"calibri";
	}
.byp-btn, #byp_start_title {
	text-transform:uppercase;
	letter-spacing:5px;
	}
#byp_start_title {
	color: #6687a6;		
	}
.byp-btn a {
	font-size:18px;
	display:block;
	padding:10px 0px;
	}
.bottomnavbyp {
	text-align:right;
	}		
.bottomnavbyp ul {
	list-style:none;
	margin:0px;
	padding:0px;
	}
.bottomnavbyp ul li {
	display:inline-block;
	padding:5px 10px;
	}		
.bottomnavbyp ul li a {
	border-bottom: 1px solid #6687a6;
    color: #6687a6;
    font-size: 14px;
    text-transform: uppercase;
    font-weight: 600;
    margin-top: 15px;
    display: block;
	}	
@media (max-width:640px) {
	#byp_landing_middle_right {
		width:100% !important;
		margin-left:0px !important;
		}
	#latest-quotes-wrap {
    	width: 100% !important;
    	padding: 0px !important;
	}	
	#byp_landing_middle_right .lightblue-bg, ul.byp_landing_list li {
		width:100% !important;
		}
	ul#latest-quotes-slide a {
		font-size:14px !important;
		}
	#byp_landing_middle_left img {
		width: 100% !important;
		height: auto !important;
		margin: 0px !important;
	}
	#byp_landing_middle_left {
		width:100% !important;
		margin:0px !important;
		}
	.bottomnavbyp {
		text-align:left !important;
		}
	.bottomnavbyp ul li {
		display:block !important;
		width:auto !important;
		padding:0px 10px !important;
		}
	.bottomnavbyp ul li a {
		display:inline-block;
		
		}
	#byp_start_title {
		letter-spacing: 2px !important;
		}
	input#byp_postcode {
		border: 4px solid #6687a6 !important;
		}	
	.byp-title-mob {
		display:block !important;
		}
	.byp-title-desk {
		display:none !important;
		}
	.soflow {
		width:70px !important;
		}	
	.dontknowrap {
		margin-top:80px !important; 
		}							
	}	
@media (min-width:640px) {
	.byp-title-mob {
		display:none !important;
		}
	.byp-title-desk {
		display:block !important;
		}	
	}	
</style>
<link href="css/newbypmobile.css" rel="stylesheet">
<script type="text/javascript" src="/js/jquery.totemticker.min.js"></script>
<script language="javascript" type="text/javascript">
$(document).ready(function() {
	$("#latest-quotes-slide").totemticker({ row_height  :   '30px'  });
	});
</script>

</head>

<body>
<form method="post" action="thank-you-for-submit.html" name="fm_byp1" id="fm_byp1" onsubmit="return false" class="">
	<div class="wrapper" id="step_1">
    <div class="full">
	<div class="col_full" style="margin-top: 20px;">
    	<div id="byp_landing_middle_right">
      
      <div class="">
				
				 <div class="byp-title byp-title-mob" id="byp_start_title">ONLINE QUOTE CALCULATOR</div>
				<div class="search_col">
					<input type="text" placeholder="Enter your postcode to begin" class="srch_input" name="postcode" id="byp_postcode" size="10" maxlength="10" value="">

					<p onclick="bypStep(1);" id="byp_btn_next1" class="serch_btn" data-pin-nopin="true">&nbsp;</p>

				</div>
				
				<p class="loader"><span id="byp_img_loading"><img src="images/byp/loading.gif" alt="Loading"></span></p>
			</div>
      <div class="byp_top_wrap">
        
        <ul class="check byp_landing_list orange">
          <li>Quote in under 60 seconds</li>
          <li>Detailed quote by email</li>
          <li>Computer generated image of your extension</li>
          <li>Booked appointment within 48hrs*</li>
        </ul>
        <div class="listtrem" style="font-size: 10px;">*Subject to availability</div>
      </div>
      <div class="" id="latest-quotes-wrap">
        <div class="byp-btn" style="margin: 0 auto;"><a href="/build-your-price-app.html" class="darkblue-txt">Latest quotes</a></div>
        <!--<div style="margin-top: 10px; font-size: 16px; color: #fff"><span class="darkblue-txt">Latest quotes:</span>-->
        <div id="latest_quotes_container" class="lightblue-bg">   
          <ul id="latest-quotes-slide">
          <?php
            $rs = getRs("SELECT postcode, amount_quote, room_size FROM rfq WHERE is_active = 1 AND is_enabled = 1 ORDER BY date_modified DESC LIMIT 0, 10");
            $buf = '';
            while ( $row = mysqli_fetch_assoc($rs) ) {
              $buf .= '<li style="padding-top: 10px;"><a href="/build-your-price-app.html" style="color: #fff; text-decoration: none;">'.$row['postcode'].' '.$row['room_size'].'m2 &pound;'.number_format($row['amount_quote'],0).'</a></li>';
            }
            echo $buf;
          ?>
          </ul>
        </div>
		<!--
        <div id="latest_quotes_src" style="display:none">
			<?php echo $buf; ?>
        </div>
		-->
        </div>
      </div>
    <div id="byp_landing_middle_left">
    <div class="byp-title byp-title-desk" id="byp_start_title">ONLINE QUOTE CALCULATOR</div>
    <img src="/assets/images/BYP-kitchen.jpg" /></div>
    
      <div class="clearfix"></div>
      <div class="bottomnavbyp">
      	<ul>
        	<li><a href="#">View Example Quote</a></li>
        	<li><a href="">FAQ's</a></li>
        	<li><a href="">Retrieve a saved quote</a></li>
        </ul>
      </div>
    </div>
  </div>
  
  
  
 
  
<div id="login_dialog" style="display:none">
  <div class="lightbox_popup">
    <div class="lightbox_inner">
      <div class="lightbox_content">
        <h3 class="login_title">Log In</h3>
        <p id="user_email_msg" class="user_email_msg">Please log in if you have already registered, if not - please <a href="/build-your-price-app.html">get your first online quote</a>!</p>
        <p><label>Email:</label> <input type="text" class="login_email" value="" /></p>
        <p class="login_pwd_p"><label>Password:</label> <input class="login_pwd" type="password" onkeypress="if(event.keyCode==13){loginUserRequest('saved_quote');}" />
        </p>
        <p><input type="button" value="Log In" class="login_btn" onclick="loginUserRequest('saved_quote')" /> <img src="/images/byp/loading2.gif" alt="Loading" class="login_loading" style="display:none" /></p>
        <p style="clear:both;text-align:center"><a href="javascript:;" onclick="forgotPassword()" class="login_forgot_link">Forgot password?</a></p>
      </div>
    </div>
  </div>
</div>

</div>
	

		
		<!-- wrapper -->
		<div class="wrapper" id="step_2" style="display:none;">
			
			<div class="main_col">
				<!-- col -->
				<div class="field_set"id="first-set">
<ul class="form_nav">
	<li><a href="#"> Location</a></li>

	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#" onclick="step_2()" class="active"> Room Size </a></li>
	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#" onclick="step_3()">Exterior </a></li>
	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#" onclick="step_4()"> Interior </a></li>
	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#" onclick="step_5()"> Confirm details
	</a></li>
</ul>    
					<ul class="main_list_col">
						<li class="mail_list dimensn">Dimensions</li>
						
						<div class="list_extend" style="display:block;">
							<div class="list_question">
								<p>Please give us the dimensions of the finished space you wish to create. 
									
								</p>
								<!-- help -->
								<!--<div class="cal_help">-->
                            <!--<div class="cal_tip_area">
            				<p>Thanks for the idea of using a 
'speech bubble' for the addition
-
al information. I would like it 
to have this style instead. when 
the user hovers over the box bor
-
der will also highlight orange </p>
</div>-->
<!--</div>-->
<!-- /help -->
</div>

<div class="field_dimensinos">
	<div class="die_depth">
		<p>Depth</p>
		<select name="width" id="width" class="soflow" onchange="bypCalcRoomArea()"><option value="3">3</option><option value="3.5">3.5</option><option value="4">4</option><option value="4.5">4.5</option><option value="5">5</option><option value="5.5">5.5</option><option value="6">6</option><option value="6.5">6.5</option><option value="7">7</option><option value="7.5">7.5</option><option value="8">8</option><option value="8.5">8.5</option><option value="9">9</option><option value="9.5">9.5</option><option value="10">10</option></select>
	</div>

	<div class="into_col">X</div>
	<div class="die_depth">
		<p>Width</p>
		<select name="depth" id="depth" class="soflow" onchange="bypCalcRoomArea()"><option value="3">3</option><option value="3.5">3.5</option><option value="4">4</option><option value="4.5">4.5</option><option value="5">5</option><option value="5.5">5.5</option><option value="6">6</option><option value="6.5">6.5</option><option value="7">7</option><option value="7.5">7.5</option><option value="8">8</option><option value="8.5">8.5</option><option value="9">9</option><option value="9.5">9.5</option><option value="10">10</option></select>
	</div>
	<div id="byp_depth" style="display:none">3</div>
	<div id="byp_width" style="display:none">3</div>
	<div class="into_col color_gray"> = </div>

	<!-- help -->
	<div class="dontknowrap">
    	<span class="dontknow">(If you don’t know the dimensions, click here)</span>
		<div class="cal_tip_areas">
        	
			<span class="room_close"> X </span>
			<h3 class="room_head">Room Size Estimator </h3>
			<p class="bed_text">How many bedrooms do you have 
				<select name="room_size" id="bedrooms_size" onchange="select_bedroom()" id="room_size" class="">
					<option value="0">Select</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>
			</p>

			<div class="chck_box full_width">
				<input type="radio" name="terrace_type" value="1" id="rad_1" class="regular-radio" checked>
				<label for="rad_1"></label>
				<p>Terraced</p>
			</div>
			<div class="chck_box full_width">
				<input type="radio" name="terrace_type" value="2" id="rad_2" class="regular-radio">
				<label for="rad_2"></label>
				<p>Detached </p>
			</div>
			<p class="note_info">Based on this information we estimate your extension should be:</p>
			<input type="text" id="estimate_information" class="info_extension pull-right">
		</div>




		<div class="cal_tip_areas_mob">
			<span class="room_close"> X </span>
			<h3 class="room_head">Room Size Estimator </h3>
			<p class="bed_text">How many bedrooms do you have 
				<select name="room_size" id="bedrooms_size" onchange="select_bedroom()" id="room_size" class="">
					<option value="0">Select</option>
					<option value="2">2</option>
					<option value="3">3</option>
					<option value="4">4</option>
					<option value="5">5</option>
					<option value="6">6</option>
				</select>
			</p>

			<div class="chck_box full_width">
				<input type="radio" name="terrace_type" value="1" id="rad_1" class="regular-radio" checked>
				<label for="rad_1"></label>
				<p>Terraced</p>
			</div>
			<div class="chck_box full_width">
				<input type="radio" name="terrace_type" value="2" id="rad_2" class="regular-radio">
				<label for="rad_2"></label>
				<p>Detached </p>
			</div>
			<p class="note_info">Based on this information we estimate your extension should be:</p>
			<input type="text" id="estimate_information" class="info_extension pull-right">
		</div>

	</div>
	<!-- /help -->

</div>
<!-- / end field_dimensinos-->

<div class="dimension_show mr_b">
	<table border="0">
		<tr>
			<th></th>
			<th> <span class="depth_size">Depth (3m)</span><br>
				<div class="depth_aro"></div>
			</th>
			<th></th>

		</tr>
		<tr>
			<td style="border:none;"> <div class="die_width_show"><span class="width_size">Width (3m)</span></div>
				<div class="width_aro"></div>
			</td>
			<td class="size_box" style="height:90px; width:113px;"><span class="total"> 9m<label class="sup">2</label> </span></td>
			<td style="border-left:0"></td>
		</tr>
	</table>
</div>

</div>							 
<!-- list_extend end -->
</ul>					 
<button class="pre_btn" name="previous" type="button" onclick="show_step_1()"  value="Back">Back</button>
<button class="next_btn fr" id="next_btn1" onclick="show_step_3()" name="next" type="button" value="Next">Next</button>

<div class="myProgress">
  <div class="myBar" style="width: 25%;"></div>
</div>


</div>
<!-- /col -->
<!-- col -->

<!-- /col -->





</div>
<!--/ main col end -->

</div>

<div class="field_set wrapper" id="step_3" style="display:none">
	<ul class="form_nav">
	<li><a href="#"> Location</a></li>

	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#"  onclick="step_2()" style="color: #827f76;"> Room Size </a></li>
	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#" class="active"  onclick="step_3()">Exterior </a></li>
	<li><i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#"  onclick="step_3()"> Interior </a></li>
	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#"  onclick="step_4()"> Confirm details
	</a></li>
</ul>    		

	<ul class="main_list_col">
		<li class="mail_list dimensn">Extension <span class="pm"><i class="fa fa-minus fa-sm" aria-hidden="true"></i></span><span class="tick_done extension_tick pull-right" style="display:none"></span></li>

		<div class="list_extend extension_image" style="display:block;">
			<div class="list_question">
				<p>What kind of extension would you like to complete? </p>

			</div>

			<div class="row">
				<div class="col-6">
					<div class="choose_die full_width">
						<input type="radio" name="cb_wall" value="5" id="cb_wall_5" onchange="bypSelectComponent(this.id)" class="regular-radio"/><label for="cb_wall_5"></label>
						
						<p>Side Extension </p>
						<div class="cal_help">
							<div class="cal_tip_area1">
								<p>This involves extending into the side alley and knocking through the existing wall that runs parallel to your property. </p>
							</div>
						</div>
					</div>
					<div class="choose_die full_width">
						<input type="radio" name="cb_wall" value="6" id="cb_wall_6" onchange="bypSelectComponent(this.id)" class="regular-radio"/><label for="cb_wall_6"></label>

						<p>Rear Extension  </p>
						<div class="cal_help">
							<div class="cal_tip_area1">
								<p>Extending to the rear is common if you don't have a side alley to fill.</p>
							</div>
						</div>
					</div>
					<div class="choose_die full_width">
						<input type="radio" name="cb_wall" value="7" id="cb_wall_7" onchange="bypSelectComponent(this.id)" class="regular-radio"/><label for="cb_wall_7"></label>
						
						<p>Wraparound Extension </p>
						<!--<div class="cal_help">
							<div class="cal_tip_area1">
								<p>Velux windows come in all shapes and sizes and are a cost efficient method.</p>
							</div>
						</div>-->
					</div>

							<!-- <div class="addition_info">							
								<div class="addition_msg" onclick="show_real_images(1)"></div>
							-->
							<div class="row">

							</div>

							<div id="myModal" class="modal">
								<span class="close cursor" onclick="closeModal()">&times;</span>
								<div class="modal-content">

									<div class="mySlides newmyslide">
                                    	<div class="nametext">Side Extension</div>
										<div class="sl_icon_cam">   
											<ul><li><img src="assets/images/cam2.png" class="image" onclick="openModal1();currentSlide1(1)"></li><li><img src="assets/images/resize_new.png" class="image" onclick="openModal1();currentSlide1(1)"></li></ul>
										</div>	
										<div class="numbertext">1 / 3</div>
										<div class="inner_myslide">
											<img src="assets/images/Side_infill_02.jpg" class="sl_img">
										</div>
									</div>

									<div class="mySlides newmyslide">
                                    	<div class="nametext">Rear Extension</div>
										<div class="sl_icon_cam">   
											<ul><li><img src="assets/images/cam2.png" class="image" onclick="openModal1();currentSlide1(1)"></li><li><img src="assets/images/resize_new.png" class="image" onclick="openModal1();currentSlide1(1)"></li></ul>
										</div>	
										<div class="numbertext">2 / 3</div>
										<div class="inner_myslide">
											<img src="assets/images/Rear_Extension.jpg" class="sl_img">
										</div>
									</div>

									<div class="mySlides newmyslide">
                                    <div class="nametext">Wraparound Extension</div>
										<div class="sl_icon_cam">   
											<ul><li><img src="assets/images/cam2.png" class="image" onclick="openModal1();currentSlide1(1)"></li><li><img src="assets/images/resize_new.png" class="image" onclick="openModal1();currentSlide1(1)"></li></ul>
										</div>
										<div class="numbertext">3 / 3</div>
										<div class="inner_myslide">
											<img src="assets/images/Wraparound.jpg" class="sl_img" >
										</div>
									</div>



									<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
									<a class="next" onclick="plusSlides(1)">&#10095;</a>



								</div>
							</div>

							<?php /* this is for second slideshow  */ ?>


							<div id="myModal1" class="modal">
								<span class="close cursor" onclick="closeModal1()">&times;</span>
								<div class="modal-content">

									<div class="mySlides1 newmyslide">
                                    	<div class="nametext">Side Extension</div>
										<div class="sl_icon_cam">   
											<ul><li><img src="assets/images/cam2.png" class="image1" onclick="openModal();currentSlide(1)"></li><li><img src="assets/images/resize_new.png" class="image1" onclick="openModal();currentSlide(1)"></li></ul>
										</div>
										<div class="numbertext">1 / 3</div>
										<div class="inner_myslide">
											<img src="assets/images/Hinton_Rd_17.jpg" class="sl_img">
										</div>
									</div>

									<div class="mySlides1 newmyslide">
                                    	<div class="nametext">Rear Extension</div>
										<div class="sl_icon_cam">   
											<ul><li><img src="assets/images/cam2.png" class="image1" onclick="openModal();currentSlide(1)"></li><li><img src="assets/images/resize_new.png" class="image1" onclick="openModal();currentSlide(1)"></li></ul>
										</div>
										<div class="numbertext">2 / 3</div>
										<div class="inner_myslide">
											<img src="assets/images/Gellatly_Road_18.jpg" class="sl_img">
										</div>
									</div>

									<div class="mySlides1 newmyslide">
                                    	<div class="nametext">Wraparound Extension</div>
										<div class="sl_icon_cam">   
											<ul><li><img src="assets/images/cam2.png" class="image1" onclick="openModal();currentSlide(1)"></li><li><img src="assets/images/resize_new.png" class="image1" onclick="openModal();currentSlide(1)"></li></ul>
										</div>
										<div class="numbertext">3 / 3</div>
										<div class="inner_myslide">
											<img src="assets/images/Walford_Road31.jpg" class="sl_img">
										</div>
									</div>



									<a class="prev" onclick="plusSlides1(-1)">&#10094;</a>
									<a class="next" onclick="plusSlides1(1)">&#10095;</a>




								</div>
							</div>



							<?php /* this is for second slideshow  */ ?>



	<!--
                        	<div class="cal_help mob_bottom" onclick="show_content(1)">
                            
                            </div>
                          <!-- /help 
                      </div>-->

                  </div>



                  <div class="col-6 extension_gif">
                  	<div id="owl-demo owl_step_1" class="owl-carousel owl-theme">

                  		<div class="item"><img src="assets/images/webproject.gif" alt=""></div>


                  	</div>
			  <!-- <a href="#" class="photo_exampl" ><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>
				<i class="icon ion-android-expand" aria-hidden="true"></i>
			-->
			
		</div>




		<div class="col-6 image_scrap_extension" style="display:none">
			<div id="owl-demo owl_step_1" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/Side_infill_02.jpg" alt=""></div>
				<div class="item"><img src="assets/images/Rear_Extension.jpg" alt=" "></div>
				<div class="item"><img src="assets/images/Wraparound.jpg" alt=""></div>

			</div>
			  <!-- <a href="#" class="photo_exampl" ><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>
				<i class="icon ion-android-expand" aria-hidden="true"></i>
			-->
			<div class="gif_icon">

				<ul><li><img src="assets/images/cam.png" onclick="show_real_images(1)"></li><li><img src="assets/images/zoom.png" onclick="openModal();currentSlide(1)"></li></ul>
			</div>
		</div>
		<div class="col-6 image_real_extension" style="display:none">
			<div id="owl-demo owl_step_1" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/Hinton_Rd_17.jpg" alt=""></div>
				<div class="item"><img src="assets/images/Gellatly_Road_18.jpg" alt=" "></div>
				<div class="item"><img src="assets/images/Walford_Road31.jpg" alt=""></div>

			</div>
			<!-- <a href="#" class="photo_exampl" onclick="show_real_images(1)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true" onclick="openModal1();currentSlide1(2)"></i>-->
			<div class="gif_icon">

				<ul><li><img src="assets/images/cam.png" onclick="show_real_images1(1)"></li><li><img src="assets/images/zoom.png" onclick="openModal1();currentSlide1(1)"></li></ul>
			</div>					

		</div>
									<!--<div class="col-6 image_content_extension" style="display:none">
										<div id="owl-demo owl_step_1" class="owl-carousel owl-theme">

                <div class="item">This involves extending into the side alley and knocking through the existing wall that runs parallel to your property. </div>
                <div class="item">Extending to the rear is common if you don�t have a side alley to fill. This is also a common option if your local council restricts wraparounds. </div>
                <div class="item">Velux windows come in all shapes and sizes and are a cost efficient method. </div>

              </div>
			  <a href="#" class="photo_exampl" onclick="show_real_images(1)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true"></i>
			</div>-->

		</div>


	</div>							 
	<!-- list_extend end -->

	<li class="mail_list dimensn">Rooflights <span class="pm"><i class="fa fa-plus" aria-hidden="true"></i></span><span class="tick_done rooflights_tick pull-right" style="display:none"></span></li>

	<div class="list_extend rooflights">
		<div class="list_question">
			<p>Which type of roof would you like?  </p>

		</div>

		<div class="row">
			<div class="col-6">
				<div class="choose_die full_width">

					<input id="cb_roof_3" name="cb_roof" value="3"  onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>
					<label for="cb_roof_3"></label>
					<p>Slate Roof with Velux</p>
					<!--<div class="cal_help">
						<div class="cal_tip_area1">
							<p>Velux windows come in all shapes and sizes and are a cost efficient method. </p>
						</div>
					</div>-->
				</div>
				<div class="choose_die full_width">
					<input id="cb_roof_4" name="cb_roof" value="4" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>
					<label for="cb_roof_4"></label>
					<p>All Glass Roof </p>
					<!--<div class="cal_help">
						<div class="cal_tip_area1">
							<p>An all glass roof is made bespoke and so can be adapted to side infills, rear and wraparound extensions.</p>
						</div>
					</div>-->
				</div>
				<div class="choose_die full_width">
					<input id="cb_roof_18" name="cb_roof" value="18" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>

					<label for="cb_roof_18"></label>
					<p>Flat Roof with Rooflights </p>
					<!--<div class="cal_help">
						<div class="cal_tip_area1">
							<p>These rooflights look flat but are built on a slight angle to allow rainwater to drain effectively.</p>
						</div>
					</div>-->
				</div>

							<!--<div class="addition_info">							
								<div class="addition_msg" onclick="show_real_images(2)"></div>
							<div class="cal_help mob_bottom" onclick="show_content(2)">
                           
                            </div>
								
                        </div>-->
                    </div>
                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal2" class="modal">
                    	<span class="close cursor" onclick="closeModal2()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides2 newmyslide">
                            	<div class="nametext">Slate Roof with Velux</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image2" onclick="openModal3();currentSlide3(1)"></li><li><img src="assets/images/resize_new.png"  class="image2" onclick="openModal3();currentSlide3(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 3</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Velux_02.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides2 newmyslide">
                            	<div class="nametext">All Glass Roof </div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image2" onclick="openModal3();currentSlide3(1)"></li><li><img src="assets/images/resize_new.png"  class="image2" onclick="openModal3();currentSlide3(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 3</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/All_glass_02.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides2 newmyslide">
                            	<div class="nametext">Flat Roof with Rooflights </div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image2" onclick="openModal3();currentSlide3(1)"></li><li><img src="assets/images/resize_new.png"  class="image2" onclick="openModal3();currentSlide3(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">3 / 3</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Roof_lights_02.jpg" class="sl_img">
                    			</div>
                    		</div>



                    		<a class="prev" onclick="plusSlides2(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides2(1)">&#10095;</a>





                    	</div>
                    </div>



                    <?php /* this is for second slideshow  */ ?>


                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal3" class="modal">
                    	<span class="close cursor"  onclick="closeModal3()">&times;</span>
                    	<div class="modal-content">
							
                    		<div class="mySlides3 newmyslide">
                            	<div class="nametext">Slate Roof with Velux</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image3" onclick="openModal2();currentSlide2(1)"></li><li><img src="assets/images/resize_new.png"  class="image3" onclick="openModal2();currentSlide2(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 3</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/VictoriaRd.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides3 newmyslide">
                            	<div class="nametext">All Glass Roof </div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image3" onclick="openModal2();currentSlide2(1)"></li><li><img src="assets/images/resize_new.png"  class="image3" onclick="openModal2();currentSlide2(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 3</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/10_Walford Road20.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides3 newmyslide">
                            	<div class="nametext">Flat Roof with Rooflights </div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image3" onclick="openModal2();currentSlide2(1)"></li><li><img src="assets/images/resize_new.png"  class="image3" onclick="openModal2();currentSlide2(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">3 / 3</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Stokenchurch_14.jpg" class="sl_img">
                    			</div>
                    		</div>



                    		<a class="prev" onclick="plusSlides3(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides3(1)">&#10095;</a>




                    	</div>
                    </div>

                    <div class="col-6 rooflight_gif">
                    	<div id="owl-demo owl_step_1" class="owl-carousel owl-theme">

                    		<div class="item"><img src="assets/images/Rooflights.gif" alt=""></div>


                    	</div>
			  <!-- <a href="#" class="photo_exampl" ><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>
				<i class="icon ion-android-expand" aria-hidden="true"></i>
			-->
			
		</div>

		<?php /* this is for second slideshow  */ ?>		
		<div class="col-6 image_scrap_roof" style="display:none">
			<div id="owl-demo owl_step_2" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/Velux_02.jpg" alt=""></div>
				<div class="item"><img src="assets/images/All_glass_02.jpg" alt=" "></div>
				<div class="item"><img src="assets/images/Roof_lights_02.jpg" alt=""></div>

			</div>
			<!-- <a href="#" onclick="show_real_images(2)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true" onclick="openModal2();currentSlide2(2)"></i>-->
			<div class="gif_icon">
			   <!-- <a href="#" class="photo_exampl" ><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>
				<i class="icon ion-android-expand" aria-hidden="true"></i>
			-->
			<ul><li><img src="assets/images/cam.png" onclick="show_real_images(2)"></li><li><img src="assets/images/zoom.png" onclick="openModal2();currentSlide2(1)"></li></ul>
		</div>					

	</div>
	<div class="col-6 image_real_roof" style="display:none">
		<div id="owl-demo owl_step_2" class="owl-carousel owl-theme">

			<div class="item"><img src="assets/images/VictoriaRd.jpg" alt=""></div>
			<div class="item"><img src="assets/images/10_Walford Road20.jpg" alt=" "></div>
			<div class="item"><img src="assets/images/Stokenchurch_14.jpg" alt=""></div>

		</div>


		<div class="gif_icon">

			<ul><li><img src="assets/images/cam.png" onclick="show_real_images1(2)"></li><li><img src="assets/images/zoom.png" onclick="openModal3();currentSlide3(1)"></li></ul>
		</div>

	</div>
	<div class="col-6 image_content_roof" style="display:none">
		<div id="owl-demo owl_step_2" class="owl-carousel owl-theme">

			<div class="item">Velux windows come in all shapes and sizes and are a cost efficient method. </div>
			<div class="item">An all glass roof is made bespoke and so can be adapted to side infills, rear and wraparound extensions. </div>
			<div class="item">These rooflights look flat but are built on a slight angle to allow rainwater to drain effectively. </div>

		</div>
			  <!-- <a href="#" onclick="show_real_images(2)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>
			  <i class="icon ion-android-expand" aria-hidden="true"></i>-->

			  <div class="gif_icon">

			  	<ul><li><img src="assets/images/cam.png" onclick="show_real_images(2)"></li>
			  		<li><img src="assets/images/zoom.png"></li></ul>
			  	</div>

			  </div>
			</div>


		</div>							 
		<!-- list_extend end -->

		<li class="mail_list dimensn">Doors <span class="pm"><i class="fa fa-plus" aria-hidden="true"></i></span><span class="tick_done door_tick pull-right" style="display:none"></span></li>
		<div class="list_extend doors">
			<div class="list_question">
				<p>What kind of doors would you like? </p>

			</div>

			<div class="row">
				<div class="col-6">
					<div class="choose_die full_width">
						<input id="cb_door_1" name="cb_door" value="1" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>

						<label for="cb_door_1"></label>
						<p>Standard Patio Doors   </p>
						<!--<div class="cal_help">
							<div class="cal_tip_area1">
								<p>These doors some in useful when planning is hesitant to permit full openings to the rear of the property.</p>
							</div>
						</div>-->
					</div>
					<div class="choose_die full_width">
						<input id="cb_door_2" name="cb_door" value="2" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>

						<label for="cb_door_2"></label>
						<p>Bifold / Sliding Doors  </p>
						<!--<div class="cal_help">
							<div class="cal_tip_area1">
								<p>Bi-fold and Sliding Doors are our most popular and can be adapted both in height and length; one panel is usually fixed for conveniance.</p>
							</div>
						</div>-->
					</div>


						<!--	<div class="addition_info">							
								<div class="addition_msg" onclick="show_real_images(3)"></div>
							<div class="cal_help mob_bottom" onclick="show_content(3)">
                            
                            </div>	
								
                        </div>-->
                    </div>

                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal4" class="modal">
                    	<span class="close cursor" onclick="closeModal4()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides4 newmyslide">
                            	<div class="nametext">Standard Patio Doors</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png"  class="image4" onclick="openModal5();currentSlide5(1)"></li><li><img src="assets/images/resize_new.png" class="image4" onclick="openModal5();currentSlide5(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Patio_02.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides4 newmyslide">
                            	<div class="nametext">Bifold / Sliding Doors</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png"  class="image4" onclick="openModal5();currentSlide5(1)"></li><li><img src="assets/images/resize_new.png" class="image4" onclick="openModal5();currentSlide5(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Folding_02.jpg" class="sl_img">
                    			</div>
                    		</div>





                    		<a class="prev" onclick="plusSlides4(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides4(1)">&#10095;</a>



                    	</div>
                    </div>



                    <?php /* this is for second slideshow  */ ?>


                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal5" class="modal">
                    	<span class="close cursor" onclick="closeModal5()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides5 newmyslide">
                            	<div class="nametext">Standard Patio Doors</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image5" onclick="openModal4();currentSlide4(1)"></li><li><img src="assets/images/resize_new.png"  class="image5" onclick="openModal4();currentSlide4(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Haldon_Road26.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides5 newmyslide">
                            	<div class="nametext">Bifold / Sliding Doors</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image5" onclick="openModal4();currentSlide4(1)"></li><li><img src="assets/images/resize_new.png"  class="image5" onclick="openModal4();currentSlide4(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Halliwick_Road29.jpg" class="sl_img">
                    			</div>
                    		</div>





                    		<a class="prev" onclick="plusSlides5(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides5(1)">&#10095;</a>



                    	</div>
                    </div>

                    <div class="col-6 door_gif">
                    	<div id="owl-demo owl_step_1" class="owl-carousel owl-theme">

                    		<div class="item"><img src="assets/images/Doors.gif" alt=""></div>


                    	</div>
			  <!-- <a href="#" class="photo_exampl" ><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>
				<i class="icon ion-android-expand" aria-hidden="true"></i>
			-->
			
		</div>

		<?php /* this is for second slideshow  */ ?>		
		<div class="col-6 image_scrap_door" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/Patio_02.jpg" alt=""></div>
				<div class="item"><img src="assets/images/Folding_02.jpg" alt=" "></div>
				


			</div>
			<!--<a href="#" onclick="show_real_images(3)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true" onclick="openModal4();currentSlide4(2)"></i>-->
			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images(3)"></li><li><img src="assets/images/zoom.png" onclick="openModal4();currentSlide4(2)"></li></ul>
			</div>					


		</div>
		<div class="col-6 image_real_door" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/Haldon_Road26.jpg" alt=""></div>
				<div class="item"><img src="assets/images/Halliwick_Road29.jpg" alt=" "></div>


			</div>


			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images1(3)"></li><li><img src="assets/images/zoom.png" onclick="openModal5();currentSlide5(2)"></li></ul>
			</div>	

		</div>
		<div class="col-6 image_content_door" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item">These doors some in useful when planning is hesitant to permit full openings to the rear of the property. </div>
				<div class="item">Bi-fold and Sliding Doors are our most popular and can be adapted both in height and length; one panel is usually fixed for conveniance.</div>


			</div>
			<!--   <a href="#" onclick="show_real_images(3)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true"></i>-->
			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images(3)"></li><li><img src="assets/images/zoom.png"></li></ul>
			</div>					

		</div>

	</div>


</div>							 
<!-- list_extend end -->
</ul>

<input type="hidden" id="wallvalue">
<input type="hidden" id="roofvalue1">
<input type="hidden" id="doorvalue1">


<button class="next_btn fr" id="next_btn1" onclick="show_step_4()" name="next" type="button" value="Next">Next</button> 

<button class="pre_btn" name="previous" type="button" onclick="show_next_step()" value="Previous">Previous</button>
<div class="myProgress">
  <div class="myBar" style="width: 50%;"></div>
</div>

<!-- /form col -->
</div>
<!--------new div to add      -->

<div class="field_set wrapper" id="step_4" style="display:none">
	<ul class="form_nav">
	<li><a href="#"> Location</a></li>

	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#"  onclick="step_2()" style="color: #827f76;"> Room Size </a></li>
	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#"  onclick="step_3()" style="color: #827f76;">Exterior </a></li>
	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#" class="active"  onclick="step_4()"> Interior </a></li>
	<li> <i class="fa fa-angle-right" aria-hidden="true"></i> </li>
	<li><a href="#"  onclick="step_5()"> Confirm details
	</a></li>
</ul>    
	<ul class="main_list_col">
		<li class="mail_list dimensn">Heating <span class="pm"><i class="fa fa-minus fa-sm" aria-hidden="true"></i></span><span class="tick_done heating_tick pull-right" style="display:none"></span></li>

		<div class="list_extend heating_image" style="display:block;">
			<div class="list_question">
				<p>How do you want to heat your extension?</p>

			</div>

			<div class="row">
				<div class="col-6">
					<div class="choose_die full_width">
						<input id="cb_heating_16" name="cb_heating" value="16" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/><label for="cb_heating_16"></label>
						
						<p>Radiators  </p>
						<!--<div class="cal_help">
							<div class="cal_tip_area1">
								<p>Radiators are traditional and effective, we can even recess them into the wall to avoid disrupting the flat wall.</p>
							</div>
						</div>-->
					</div>
					<div class="choose_die full_width">
						<input id="cb_heating_17" name="cb_heating" value="17" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/><label for="cb_heating_17"></label>
						
						<p>Underfloor Heating </p>
						<!--<div class="cal_help">
							<div class="cal_tip_area1">
								<p>Not a radiator insight! Underfloor heating keeps a clutter free, sleek finish.</p>
							</div>
						</div>-->
					</div>

							<!--<div class="addition_info">							
								<div class="addition_msg" onclick="show_real_images(4)"></div>
							
								
                        	<div class="cal_help mob_bottom" onclick="show_content(4)">
                            
                            </div>
                         
                        </div>-->

                    </div>
                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal6" class="modal">
                    	<span class="close cursor" onclick="closeModal6()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides6 newmyslide">
                            	<div class="nametext">Radiators</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image6" onclick="openModal7();currentSlide7(1)"></li><li><img src="assets/images/resize_new.png"  class="image6" onclick="openModal7();currentSlide7(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Radiator_02.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides6 newmyslide">
                            	<div class="nametext">Underfloor Heating</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image6" onclick="openModal7();currentSlide7(1)"></li><li><img src="assets/images/resize_new.png"  class="image6" onclick="openModal7();currentSlide7(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Underfloor_02.jpg" class="sl_img">
                    			</div>
                    		</div>





                    		<a class="prev" onclick="plusSlides6(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides6(1)">&#10095;</a>




                    	</div>
                    </div>



                    <?php /* this is for second slideshow  */ ?>


                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal7" class="modal">
                    	<span class="close cursor" onclick="closeModal7()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides7 newmyslide">
                            	<div class="nametext">Radiators</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image7" onclick="openModal6();currentSlide6(1)"></li><li><img src="assets/images/resize_new.png"  class="image7" onclick="openModal6();currentSlide6(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Victoria_2.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides7 newmyslide">
                            	<div class="nametext">Underfloor Heating</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image7" onclick="openModal6();currentSlide6(1)"></li><li><img src="assets/images/resize_new.png"  class="image7" onclick="openModal6();currentSlide6(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Mayfield_Road.jpg" class="sl_img">
                    			</div>
                    		</div>





                    		<a class="prev" onclick="plusSlides7(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides7(1)">&#10095;</a>




                    	</div>
                    </div>



                    <?php /* this is for second slideshow  */ ?>				


                    <div class="col-6 image_scrap_heating">
                    	<div id="owl-demo" class="owl-carousel owl-theme">

                    		<div class="item"><img src="assets/images/Radiator_02.jpg" alt=""></div>
                    		<div class="item"><img src="assets/images/Underfloor_02.jpg" alt=" "></div>


                    	</div>
                    	<!-- <a href="#" onclick="show_real_images(4)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true" onclick="openModal6();currentSlide6(2)"></i>-->
                    	<div class="gif_icon">
                    		<ul><li><img src="assets/images/cam.png" onclick="show_real_images(4)"></li><li><img src="assets/images/zoom.png" onclick="openModal6();currentSlide6(1)"></li></ul>
                    	</div>					



                    </div>
                    <div class="col-6 image_real_heating" style="display:none">
                    	<div id="owl-demo" class="owl-carousel owl-theme">

                    		<div class="item"><img src="assets/images/Victoria_2.jpg" alt=""></div>
                    		<div class="item"><img src="assets/images/Mayfield_Road.jpg" alt=" "></div>


                    	</div>
                    	<!-- <a href="#" onclick="show_real_images(4)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true" onclick="openModal7();currentSlide7(2)"></i>-->

                    	<div class="gif_icon">
                    		<ul><li><img src="assets/images/cam.png" onclick="show_real_images1(4)"></li><li><img src="assets/images/zoom.png" onclick="openModal7();currentSlide7(2)"></li></ul>
                    	</div>					

                    </div>
                    <div class="col-6 image_content_heating" style="display:none">
                    	<div id="owl-demo" class="owl-carousel owl-theme">

                    		<div class="item">Radiators are traditional and effective, we can even recess them into the wall to avoid disrupting the flat wall.</div>
                    		<div class="item">Not a radiator insight! Underfloor heating keeps a clutter free, sleek finish.</div>


                    	</div>
                    	<!--  <a href="#" onclick="show_real_images(4)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>-->
                    	<div class="gif_icon">
                    		<ul><li><img src="assets/images/cam.png" onclick="show_real_images(4)"></li></ul>
                    	</div>					</div>

                    </div>


                </div>							 
                <!-- list_extend end -->

                <li class="mail_list dimensn">Chimney Breast <span class="pm"><i class="fa fa-plus" aria-hidden="true"></i></span><span class="tick_done chimney_tick pull-right" style="display:none"></span></li>

                <div class="list_extend chimney_image">
                	<div class="list_question">
                		<p>Would you like to remove a Chimney?</p>

                	</div>

                	<div class="row">
                		<div class="col-6">
                			<div class="choose_die full_width">
                				<input id="cb_kitchen_12" name="cb_kitchen" value="12" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>

                				<label for="cb_kitchen_12"></label>
                				<p>Remove Chimney	</p>
                				<!--<div class="cal_help">
                					<div class="cal_tip_area1">
                						<p>Chimneys are a common feature of Victorian Terraces and are rarely used.</p>
                					</div>
                				</div>-->
                			</div>
                			<div class="choose_die full_width">
                				<input id="cb_kitchen_14" name="cb_kitchen" value="14" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>

                				<label for="cb_kitchen_14"></label>
                				<p>Do not remove</p>
                				<!--<div class="cal_help">
                					<div class="cal_tip_area1">
                						<p>If you want to avoid the cost of removal, make feature out of your chimney by adapting your kitchen.</p>
                					</div>
                				</div>-->
                			</div>


						<!--	<div class="addition_info">							
								<div class="addition_msg" onclick="show_real_images(5)"></div>
							<div class="cal_help mob_bottom" onclick="show_content(5)">
                           
                            </div>
								
                        </div>-->
                    </div>
                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal8" class="modal">
                    	<span class="close cursor" onclick="closeModal8()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides8 newmyslide">
                            	<div class="nametext">Remove Chimney</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image8" onclick="openModal9();currentSlide9(1)"></li><li><img src="assets/images/resize_new.png"  class="image8" onclick="openModal9();currentSlide9(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Chimney_02.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides8 newmyslide">
                            	<div class="nametext">Do not remove</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image8" onclick="openModal9();currentSlide9(1)"></li><li><img src="assets/images/resize_new.png" class="image8" onclick="openModal9();currentSlide9(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/no_chimney.jpg" class="sl_img">
                    			</div>
                    		</div>





                    		<a class="prev" onclick="plusSlides8(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides8(1)">&#10095;</a>




                    	</div>
                    </div>



                    <?php /* this is for second slideshow  */ ?>


                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal9" class="modal">
                    	<span class="close cursor" onclick="closeModal9()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides9 newmyslide">
                            	<div class="nametext">Remove Chimney</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image9" onclick="openModal8();currentSlide8(1)"></li><li><img src="assets/images/resize_new.png" class="image9" onclick="openModal8();currentSlide8(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Victoria_Road.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides9 newmyslide">
                            	<div class="nametext">Do not remove</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image9" onclick="openModal8();currentSlide8(1)"></li><li><img src="assets/images/resize_new.png" class="image9" onclick="openModal8();currentSlide8(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Oswyth_Road10.jpg" class="sl_img">
                    			</div>
                    		</div>





                    		<a class="prev" onclick="plusSlides9(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides9(1)">&#10095;</a>




                    	</div>
                    </div>

                    <div class="col-6 chimney_gif">
                    	<div id="owl-demo owl_step_1" class="owl-carousel owl-theme">

                    		<div class="item"><img src="assets/images/Chimney-Breast.gif" alt=""></div>


                    	</div>
			  <!-- <a href="#" class="photo_exampl" ><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>
				<i class="icon ion-android-expand" aria-hidden="true"></i>
			-->
			
		</div>

		<?php /* this is for second slideshow  */ ?>			
		<div class="col-6 image_scrap_chimeny" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/Chimney_02.jpg" alt=""></div>
				<div class="item"><img src="assets/images/no_chimney.jpg" alt=""></div>


			</div>
			<!--  <a href="#" onclick="show_real_images(5)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true" onclick="openModal8();currentSlide8(1)"></i>-->
			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images(5)"></li><li><img src="assets/images/zoom.png" onclick="openModal8();currentSlide8(1)"></li></ul>
			</div>			

		</div>
		<div class="col-6 image_real_chimeny" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/Victoria_Road.jpg" alt=""></div>
				<div class="item"><img src="assets/images/Oswyth_Road10.jpg" alt=" "></div>


			</div>
			<!--<a href="#" onclick="show_real_images(5)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true" onclick="openModal9();currentSlide9(1)"></i>-->
			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images1(5)"></li><li><img src="assets/images/zoom.png" onclick="openModal9();currentSlide9(1)"></li></ul>
			</div>

		</div>
		<div class="col-6 image_content_chimeny" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item">Chimneys are a common feature of Victorian Terraces and are rarely used. </div>
				<div class="item">If you want to avoid the cost of removal, make feature out of your chimney by adapting your kitchen.</div>


			</div>
			<!--  <a href="#" onclick="show_real_images(4)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>-->
			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images(4)"></li></ul>
			</div>
		</div>

	</div>


</div>							 
<!-- list_extend end -->

<li class="mail_list dimensn">Ground floor WC <span class="pm"> <i class="fa fa-plus" aria-hidden="true"></i></span><span class="tick_done floor_tick pull-right" style="display:none"></span></li>
<div class="list_extend floor_image">
	<div class="list_question">
		<p>Would you like a WC? </p>

	</div>

	<div class="row">
		<div class="col-6">
			<div class="choose_die full_width">
				<input id="cb_floor_9" name="cb_floor" value="9" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>

				<label for="cb_floor_9"></label>
				<p>Install a WC</p>
				<!--<div class="cal_help">
					<div class="cal_tip_area1">
						<p>It's conveniant to have a WC on your ground floor and a very common place to put them in under the stairs.</p>
					</div>
				</div>-->
			</div>
			<div class="choose_die full_width">
				<input id="cb_floor_10" name="cb_floor" value="10" onchange="bypSelectComponent(this.id)" type="radio" class="regular-radio"/>

				<label for="cb_floor_10"></label>
				<p>Do Not Add WC </p>
				<!--<div class="cal_help">
					<div class="cal_tip_area1">
						<p>Sometimes a house has enough bathrooms! Click this option if you don�t need to add a WC.</p>
					</div>
				</div>-->
			</div>


						<!--	<div class="addition_info">							
								<div class="addition_msg" onclick="show_real_images(6)"></div>
							<div class="cal_help mob_bottom" onclick="show_content(6)">
                            
                            </div>
								
                        </div>-->
                    </div>
                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal10" class="modal">
                    	<span class="close cursor" onclick="closeModal10()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides10 newmyslide">
								<div class="nametext">Install a WC</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image10" onclick="openModal11();currentSlide11(1)"></li><li><img src="assets/images/resize_new.png" class="image10" onclick="openModal11();currentSlide11(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/WC_02.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides10 newmyslide">
								<div class="nametext">Do Not Add WC </div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image10" onclick="openModal11();currentSlide11(1)"></li><li><img src="assets/images/resize_new.png" class="image10" onclick="openModal11();currentSlide11(1)"></li></ul>
                    			</div>

                    			<div class="numbertext">2 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/no_WC.jpg" class="sl_img">
                    			</div>
                    		</div>





                    		<a class="prev" onclick="plusSlides10(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides10(1)">&#10095;</a>




                    	</div>
                    </div>



                    <?php /* this is for second slideshow  */ ?>


                    <?php /* this is for second slideshow  */ ?>


                    <div id="myModal11" class="modal">
                    	<span class="close cursor" onclick="closeModal11()">&times;</span>
                    	<div class="modal-content">

                    		<div class="mySlides11 newmyslide">
								<div class="nametext">Install a WC</div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image11" onclick="openModal10();currentSlide10(1)"></li><li><img src="assets/images/resize_new.png" class="image11" onclick="openModal10();currentSlide10(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">1 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/Mayfield_1.jpg" class="sl_img">
                    			</div>
                    		</div>

                    		<div class="mySlides11 newmyslide">
								<div class="nametext">Do Not Add WC </div>
                    			<div class="sl_icon_cam">   
                    				<ul><li><img src="assets/images/cam2.png" class="image11" onclick="openModal10();currentSlide10(1)"></li><li><img src="assets/images/resize_new.png" class="image11" onclick="openModal10();currentSlide10(1)"></li></ul>
                    			</div>
                    			<div class="numbertext">2 / 2</div>
                    			<div class="inner_myslide">
                    				<img src="assets/images/23_Mayfield_Rd14.jpg" class="sl_img">
                    			</div>
                    		</div>






                    		<a class="prev" onclick="plusSlides11(-1)">&#10094;</a>
                    		<a class="next" onclick="plusSlides11(1)">&#10095;</a>




                    	</div>
                    </div>
                    <div class="col-6 wc_gif">
                    	<div id="owl-demo owl_step_1" class="owl-carousel owl-theme">

                    		<div class="item"><img src="assets/images/WC.gif" alt=""></div>


                    	</div>
			  <!-- <a href="#" class="photo_exampl" ><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a>
				<i class="icon ion-android-expand" aria-hidden="true"></i>
			-->
			
		</div>


		<?php /* this is for second slideshow  */ ?>	
		<div class="col-6 image_scrap_floor" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/WC_02.jpg" alt=""></div>
				<div class="item"><img src="assets/images/no_WC.jpg" alt=""></div>


			</div>
			<!--	  <a href="#" onclick="show_real_images(6)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a><i class="icon ion-android-expand" aria-hidden="true" onclick="openModal10();currentSlide10(1)"></i>-->
			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images(6)"></li><li><img src="assets/images/zoom.png" onclick="openModal10();currentSlide10(1)"></li></ul>
			</div>						


		</div>
		<div class="col-6 image_real_floor" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item"><img src="assets/images/Mayfield_1.jpg" alt=""></div>
				<div class="item"><img src="assets/images/23_Mayfield_Rd14.jpg" alt=" "></div>


			</div>


			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images1(6)"></li><li><img src="assets/images/zoom.png" onclick="openModal11();currentSlide11(1)"></li></ul>
			</div>	
		</div>
		<div class="col-6 image_content_floor" style="display:none">
			<div id="owl-demo" class="owl-carousel owl-theme">

				<div class="item">It's conveniant to have a WC on your ground floor and a very common place to put them in under the stairs.</div>
				<div class="item">Sometimes a house has enough bathrooms! Click this option if you don�t need to add a WC. </div>


			</div>
			<!--  <a href="#" onclick="show_real_images(4)"><i class="fa fa-camera fa-sm" aria-hidden="true"></i></a> <i class="icon ion-android-expand" aria-hidden="true"></i>-->
			<div class="gif_icon">
				<ul><li><img src="assets/images/cam.png" onclick="show_real_images(4)"></li><li><img src="assets/images/zoom.png"></li></ul>
			</div>
		</div>
	</div>


</div>							 
<!-- list_extend end -->
</ul>



<button class="next_btn fr" id="next_btn1" name="next" onclick="show_step_5()" type="button" value="Next">Next</button> 

<button class="pre_btn" name="previous" type="button" value="Previous">Previous</button>
<div class="myProgress">
  <div class="myBar" style="width: 75%;"></div>
</div>

<!-- /form col -->
</div>
<span class="summary_btn mob_summry_btn" style="display:none">Summary</span> 

<div class="pop_wrap">
	<span class="close close_sum close-mob"> X </span>
	<span class="summary_btn_in">Summary </span>

	<div class="pop_row">
		<p class="postcode_user">SW9 9AJ</p> 
		<a href="byp-landing.php">Edit</a>
	</div>

	<div class="pop_row">
		<div class="dimension_show pop_die">
			<table border="0">

				<tr>

					<td class="size_box" style="height:82px; width:113px;"><span class="total">9m<sup>2</sup></span></td>
					<td style="border-left:0"></td>
				</tr>
			</table>
		</div>
	</div>

	<div class="pop_row">
		<p>Room size </p> 
		<a href="javascript:void(0)" onclick="show_next_step()">Edit</a>
	</div>

	<div class="pop_row">
		<p>Side wall to be removed  </p> 
		<a href="javascript:void(0)"  onclick="show_step_3()">Edit</a>
	</div>
	<div class="pop_row">
		<p>All glass roof  </p> 
		<a href="javascript:void(0)"  onclick="show_step_4()">Edit</a>
	</div>
	<div class="pop_row">
		<p>Standard patio doors </p> 
		<a href="javascript:void(0)">Edit</a>
	</div>
</div>
<!--------new div to add      -->



</form>

<form method="post" action="#" name="fm_byp4" id="fm_byp4" class="carousel slide">
	<div class="wrapper" id="step_5" style="display:none;">
		<ul class="form_nav">
	<li><a href="#"> Location</a></li>

	<li> > </li>
	<li><a href="#"  onclick="step_2()" style="color: #827f76;"> Room Size </a></li>
	<li> > </li>
	<li><a href="#"  onclick="step_3()" style="color: #827f76;">Exterior </a></li>
	<li> > </li>
	<li><a href="#"  onclick="step_4()" style="color: #827f76;"> Interior </a></li>
	<li> > </li>
	<li><a href="#" class="active"  onclick="step_5()"> Confirm details
	</a></li>
</ul>    	 
		<div class="confirmspace">
			<h2 class="detail_heading confrm dimensn">Confirm Details <h2>

				<div class="row">

					<div class="col-6 form_sec">
						<div class="confirm_col">
							<div class="confirm_field">
								<p>First Name*</p>	
								<input type="text" name="name" id="byp_name" maxlength="50">
								<span class="tick_done tick_done_name tick" style="display:none"></span>
							</div>
							<div class="confirm_field">
								<p>Surname</p>	
								<input type="text" name="surname" id="byp_surname" maxlength="100" >
								<span class="tick_done tick_done_surname tick" style="display:none"></span>
							</div>
							<div class="confirm_field">
								<p>Email</p>	
								<input type="email" name="email" id="byp_email" maxlength="100" >
								<span class="tick_done tick_done_email tick" style="display:none"></span>
							</div>
							<div class="confirm_field">
								<p>Password*</p>	
								<input type="password" name="password" id="byp_password" maxlength="100" >
								<span class="tick_done tick_done_password tick" style="display:none"></span>
							</div>
							<div class="confirm_field">
								<p>Retype Password*</p>	
								<input type="password" name="repassword" id="byp_repassword" maxlength="100" >
								<span class="tick_done tick_done_repassword tick" style="display:none"></span>
							</div>
							<input type="hidden" name="repassword" value="1" id="byp_repassword" maxlength="100" >
							<div class="confirm_field" style="margin-bottom:0px;">
								<p>Contact Number*</p>	
								<input type="text" name="phone" id="byp_phone" maxlength="20" >
								<span class="tick_done tick_done_phone tick" style="display:none"></span>
							</div>
						</div>
					</div>

					<div class="col-6 form_sec">
						<div class="confirm_col no_border_right confirm_second">
							<div class="confirm_field">
								<p>House or flat number*</p>	
								<input type="text" name="house" id="byp_house"> 
								<span class="tick_done tick_done_house tick" style="display:none"></span>
								
							</div>
							<div class="confirm_field">
							<select name="addr" id="byp_addr" style="display:none">
									<option value="">- or select address -</option>
								</select>
								</div>
							<div class="confirm_field">
								<p>Postcode*</p>	

								<input type="text" name="postcode_f" value="SE22 8PY" id="byp_postcode_f" readonly />
								<span class="tick_done tick_post"></span>
							</div>
							<input type="button" value=" Next " class="btn btn-orange mob_btn_next commonnextbtn" onclick="bypMobSwipeNextScreen()" style="display:none"/>
							<div class="confirm_field">
								<p>Comments</p>	
								<input type="text" name="message" cols="31" rows="3" id="byp_msg"></textarea>
								<span class="tick_done tick_done_msg tick" style="display:none"></span>
							</div>
							<div class="confirm_field">
								<p>Design Phase start date</p>	
								<select id="when_to_start">
									<option value="">Please select</option>
									<option value="immediately">immediately</option>
									<option value="1-3 months">1-3 months</option>
									<option value="3-6 months">3-6 months</option>
									<option value="1 year">1 year</option>
								</select>
								<span class="tick_done tick_done_start tick" style="display:none"></span>
							</div>
							<div class="confirm_field">
								<p>Your current status</p>	
								<select id="current_status">
									<option value="">Please select</option>
									<option value="pre-purchase">pre-purchase</option>
									<option value="just moved in">just moved in</option>
									<option value="resident for less than 1 year">resident for less than 1 year</option>
									<option value="resident for more than 1 year">resident for more than 1 year</option>
								</select>
								<span class="tick_done tick_done_status tick" style="display:none"></span>
							</div>

							<a href="/images/byp/example_quote.pdf" class="view_exmpl" target="_blank">View example quote</a>

						</div>
					</div>


				</div>

			</div>
			<script type="text/javascript">
				/* <![CDATA[ */
				goog_snippet_vars = function() {
					var w = window;
					w.google_conversion_id = 938671590;
					w.google_conversion_label = "LNARCNiS1WEQ5vvLvwM";
					w.google_remarketing_only = false;
				}
  // DO NOT CHANGE THE CODE BELOW.
  goog_report_conversion = function(url) {
  	goog_snippet_vars();
  	window.google_conversion_format = "3";
  	window.google_is_call = true;
  	var opt = new Object();
  	opt.onload_callback = function() {
  		if (typeof(url) != 'undefined') {
  			window.location = url;
  		}
  	}
  	var conv_handler = window['google_trackConversion'];
  	if (typeof(conv_handler) == 'function') {
  		conv_handler(opt);
  	}
  }
  /* ]]> */
</script>
<script type="text/javascript"
src="//www.googleadservices.com/pagead/conversion_async.js">
</script>
<!-- /resize -->
<div class="show_quote_col">
	<!--<a class="byp-btn-lg" href="javascript:;" onclick="validate_user()" id="byp_showmyquote_link">Show my quote</a>-->
</div>

<button class="next_btn fr" id="byp_showmyquote_link" onclick="validate_user(); submitUserInfo();" name="next" type="button" value="Next">Finish</button>
<button class="pre_btn quote_back" name="previous" type="button" onclick="show_step_4()" value="Back">Back</button>
<div class="myProgress">
  <div class="myBar" style="width: 100%;"></div>
</div>

<p class="note_quot">By clicking on the  show my quote button you confirm that you agree to our terms and conditions statement and you have read and understand the privacy policy</p>	 
</div>
</form>
<!-- wrapper -->
<div class="wrapper" id="result_1" style="display:none;">

	<div class="">
		<div class="onlie_quote_heading">
			<div class="row">
				<div class="col-6">

					<h2 class="detail_heading online_quot">Online Quote <h2>
					</div>
					<div class="col-6">						
						
							
							
							
							<div class="get_response" style="display:none;">


							</div>
							
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-6 mr40 house-img fr">
						<img src="assets/images/house-img.jpg">
					</div>
					<div class="col-6">
                    	<h2 class="detail_heading orange_text"> Quote Ref: <span id="detail_heading">PP/350/SW9</span> <h2>
                    </div>
					<div class="col-6 mr40">
						<div class="design_phase">
							<p>Design Phase </p>
							<ul class="d_phase_list">
								<li>Architectural plans </li>
								<li>Planning permission  </li>
								<li>Structural engineering <br>&pound;<b id="structural">3,995</b>   </li>
							</ul>
						</div>
						<div class="design_phase">
							<p>Construction Phase  </p>
							<ul class="d_phase_list">
								<li>Full project management  </li>
								<li>Electrical work & plumbing  </li>
								<li>Roof lights & doors  <br>&pound;<b id="roof">36,750</b>   </li>
							</ul>
						</div>
					</div>


					<div class="col-12 mr40">
						<div class="design_phase">
							<p>Duration of work will be approximately 12 weeks  </p>
							<p>Quotation subject to site survey and VAT at prevailing rate
							</p>
						</div>
					</div>


				</div>

				<div class="bottom_buttons">
					<div class="row">
						<div class="col-4 save_quot">
							<a href="#">Save quote </a>
						</div>
						<div class="col-4 text-center">
							<a href="#">Attach Images  </a>
						</div>
						<div class="col-4 ">
							<a href="#" class="link_right">Book A Site Visit </a>
						</div>
					</div>
				</div>

			</div>
			<!-- /resize -->



		</div>

<script>
$(document).ready(function(e) {
    $(".gif_icon li:last-child").click(function(){
		//e.preventDefault();
		var getindexslide = $(this).parents(".gif_icon").siblings(".owl-carousel").children(".owl-controls").children(".owl-pagination").children(".owl-page.active").index();
		$(".modal-content .newmyslide").css("display", "none");	
		var displayslide = $(".modal-content .newmyslide").each(function(index, element) {
		
          if ($(this).index() == getindexslide) {
			  $(this).css("display", "block");
			  }
        });
		
		});
});
</script>
<script>
	$(document).on('click', ".owl-buttons div" , function() {
		var getindex = $(this).parents(".owl-controls").children(".owl-pagination").children(".owl-page.active").index();
    	
		
		$(this).parents(".list_extend").children(".row").children(".col-6").children(".choose_die").each(function(index, element) {
          if ($(this).index() == getindex) {
			  $(this).children(".regular-radio").prop("checked", true);
			  }
        });  
		 
       
});
</script>
<script>
$(document).ready(function(e) {
    $(document).on('click', '.owl-pagination div' , function() {
		var getindex = $(this).index();
    	//alert(getindex);
		
		$(this).parents(".list_extend").children(".row").children(".col-6").children(".choose_die").each(function(index, element) {
          if ($(this).index() == getindex) {
			  $(this).children(".regular-radio").prop("checked", true);
			  }
        });  
		 
       
});
});
	
</script>

		<script>
			$(document).ready(function() {
	//$('.tick_done').hide();
	
	
var count = 0; // To Count Blank Fields
/*------------ Validation Function-----------------*/
$(".submit_btn").click(function(event) {
var radio_check = $('.rad'); // Fetching Radio Button By Class Name
var input_field = $('.text_field'); // Fetching All Inputs With Same Class Name text_field & An HTML Tag textarea
var text_area = $('textarea');
// Validating Radio Button
if (radio_check[0].checked == false && radio_check[1].checked == false) {
	var y = 0;
} else {
	var y = 1;
}
// For Loop To Count Blank Inputs
for (var i = input_field.length; i > count; i--) {
	if (input_field[i - 1].value == '' || text_area.value == '') {
		count = count + 1;
	} else {
		count = 0;
	}
}
// Notifying Validation
if (count != 0 || y == 0) {
	alert("*All Fields are mandatory*");
	event.preventDefault();
} else {
	return true;
}
});
/*---------------------------------------------------------*/
/* $(".next_btn").click(function() { // Function Runs On NEXT Button Click

var depth=$('#depth').val();
if()


$(this).parent().next().fadeIn('slow');
$(this).parent().css({
'display': 'none'
}); */
// Adding Class Active To Show Steps Forward;
$('.active').next().addClass('active');
});
$(".pre_btn").click(function() { // Function Runs On PREVIOUS Button Click
	$(this).parent().prev().fadeIn('slow');
	$(this).parent().css({
		'display': 'none'
	});
// Removing Class Active To Show Steps Backward;
$('.active:last').removeClass('active');
});
// Validating All Input And Textarea Fields
$(".submit_btn").click(function(e) {
	if ($('input').val() == "" || $('textarea').val() == "") {
		alert("*All Fields are mandatory*");
		return false;
	} else {
		return true;
	}
});

/* $("#next_btn1").click(function() {
alert('b');	// Function Runs On NEXT Button Click
var free_time = $('#free_time').val();

}); */





</script>


<script>
	$("#last").click(function() { 
		$(".h_hide").hide();
	});

	$(".mob_summry_btn").click(function() { 
		$(".pop_wrap").fadeIn();
		$(".summary_btn").hide();
		(".summary_btn_in").attr("style","color:#6687a6; background:#fff;");


	});

// for left navi animate 
$(".summary_btn_in ").click(function(){ 
	$(".pop_wrap").animate({'marginLeft':'0px'}, 400);

	$(".summary_btn_in").attr("style","color:#6687a6; background:#fff;");
})



$(".close_sum").click(function(){ $(".pop_wrap").animate({'marginLeft':'-441px'}, 400);})
$(".close_sum ").click(function(){	
	$(".summary_btn_in").attr("style","");
});








</script>

<script>


	$(document).ready(function(){
		var $j = jQuery;
		var windowwidth = $j(window).width();
		if (windowwidth <= '767') {
			$(".close-mob ").click(function() { 
				$(".pop_wrap").fadeOut();

			});
			$(".summary_btn_in").attr("style","");
		}

	});
</script>    
<script>
	$(document).ready(function() {
		$('.property_type').click(function () {
			var alt = $(this).attr("alt")

			$('#property_type').val(alt);

		});
	});

	$("input[name='terrace_type']:radio").change(function () {

		select_bedroom();

	});

	function change_box(id)
	{
		if(id==0)
		{
			$('#cb_wall_5').prop("checked",true);
			$('#cb_roof_3').prop("checked",true);
		}
		if(id==1)
		{
			$('#cb_wall_6').prop("checked",true);
			$('#cb_roof_4').prop("checked",true);
		}
		if(id==2)
		{
			$('#cb_wall_7').prop("checked",true);
			$('#cb_roof_18').prop("checked",true);
		}

	}
	function select_bedroom()
	{
		var bedroom=$('#bedrooms_size').val();
		var wall = $("input[name='terrace_type']:checked").val();

		if(bedroom==2 && wall==1)
		{
			$('#estimate_information').val(25);

		}
		else if(bedroom==3 && wall==1)
		{
			$('#estimate_information').val(27);

		}
		else if(bedroom==4 && wall==1)
		{
			$('#estimate_information').val(30);

		}
		else if(bedroom==5 && wall==1)
		{
			$('#estimate_information').val(42);

		}
		else if(bedroom==6 && wall==1)
		{
			$('#estimate_information').val(50);

		}

		else if(bedroom==2 && wall==2)
		{
			$('#estimate_information').val(26);

		}
		else if(bedroom==3 && wall==2)
		{
			$('#estimate_information').val(27.5);

		}
		else if(bedroom==4 && wall==2)
		{
			$('#estimate_information').val(40);

		}
		else if(bedroom==5 && wall==2)
		{
			$('#estimate_information').val(45);

		}
		else if(bedroom==6 && wall==2)
		{
			$('#estimate_information').val(50);

		}
		else 
		{
			alert('please select the bedroom size');
		}

	}
	function show_next_step()
	{


		$('#step_2').show();
		$('#step_1').hide();
		$('#step_3').hide();

	//$('.summary_btn').show();
	//$('.pop_wrap').show();
	var postcode=$('#byp_postcode_f').val();
	$('.postcode_user').html(postcode);
	
}
function show_step_1()
{
	$('#step_2').hide();
	$('#step_1').show();
	//$('.summary_btn').hide();
	location.reload(); 
}
function show_step_3()
{

	$('#step_2').hide();
	$('#step_1').hide();
	$('#step_3').show();	
	$('#step_4').hide();	
	//$('.summary_btn').show();
}
function show_step_4()
{
	var wallvalue = $("input[name='cb_wall']:checked").val();
	var roofvalue = $("input[name='cb_roof']:checked").val();
	var doorvalue = $("input[name='cb_door']:checked").val();

	$('#wallvalue').val(wallvalue);
	$('#roofvalue1').val(roofvalue);
	$('#doorvalue1').val(doorvalue);
	if(wallvalue && roofvalue && doorvalue)
	{
		$('#step_2').hide();
		$('#step_1').hide();
		$('#step_3').hide();	
		$('#step_4').show();
	}
	else 
	{
		alert("please select choice");
		return false;
	}

}
function show_step_5()
{
	var heatingvalue = $("input[name='cb_heating']:checked").val();
	var roofvalue = $("input[name='cb_kitchen']:checked").val();
	var doorvalue = $("input[name='cb_floor']:checked").val();
	if(heatingvalue && roofvalue && doorvalue)
	{
		$('#step_2').hide();
		$('#step_1').hide();
		$('#step_3').hide();	
		$('#step_4').hide();
		$('#step_5').show();
	//$('.summary_btn').hide();
}
else 
{
	alert("please select choice");
	return false;
}

}

function show_image(id)
{

	if(id==5 || id==6 || id==7)
	{
		$('.extension_gif').hide();
		$('.image_scrap_extension').show(); 

	}
	if(id==3 || id==4 || id==18)
	{
		$('.rooflight_gif').hide();
		$('.image_scrap_roof').show(); 
	}
	if(id==1 || id==2)
	{
		$('.door_gif').hide();
		$('.image_scrap_door').show(); 
	}
	if(id==12 || id==14)
	{
		$('.chimney_gif').hide();
		$('.image_scrap_chimeny').show(); 
	}	
	if(id==9 || id==10)
	{
		$('.wc_gif').hide();
		$('.image_scrap_floor').show(); 
	}


	$('.extension_image').find('.owl-pagination').addClass('extension_image1');
	$('.extension_image').find('.owl-wrapper').addClass('extension_image2');
	$('.rooflights').find('.owl-pagination').addClass('rooflights1');
	$('.rooflights').find('.owl-wrapper').addClass('rooflights2');
	$('.doors').find('.owl-pagination').addClass('doors1');
	$('.doors').find('.owl-wrapper').addClass('doors2');
	$('.heating_image').find('.owl-pagination').addClass('heating1');
	$('.heating_image').find('.owl-wrapper').addClass('heating2');

	$('.chimney_image').find('.owl-pagination').addClass('chimney1');
	$('.chimney_image').find('.owl-wrapper').addClass('chimney2');

	$('.floor_image').find('.owl-pagination').addClass('floor1');
	$('.floor_image').find('.owl-wrapper').addClass('floor2');



	if(id==5){

		$('.extension_tick').show();
		$('.extension_image1 div:nth-child(1)').addClass('active');
		
		$('.extension_image2').css('transform', 'translate3d(0px, 0px, 0px)');
		
		$('.extension_image1 div:nth-child(2)').removeClass('active');
		$('.owl-pagination div:nth-child(3)').removeClass('active');
	}
	else if(id==6)
	{
		$('.extension_tick').show();
		$('.extension_image1 div:nth-child(2)').addClass('active');
		$('.extension_image2').css('transform', 'translate3d(-451px, 0px, 0px)');
		
		$('.extension_image1 div:nth-child(1)').removeClass('active');
		$('.extension_image1 div:nth-child(3)').removeClass('active');
	}
	else if(id==7)
	{
		$('.extension_tick').show();
		$('.extension_image1 div:nth-child(3)').addClass('active');
		$('.extension_image2').css('transform', 'translate3d(-902px, 0px, 0px)');
		
		$('.extension_image1 div:nth-child(1)').removeClass('active');
		$('.extension_image1 div:nth-child(2)').removeClass('active');
	}
	else if(id==3)
	{
		$('.rooflights_tick').show();
		$('.rooflights1 div:nth-child(1)').addClass('active');
		$('.rooflights2').css('transform', 'translate3d(0px, 0px, 0px)');
		
		$('.rooflights1 div:nth-child(2)').removeClass('active');
		$('.rooflights1 div:nth-child(3)').removeClass('active');
	}
	else if(id==4)
	{
		$('.rooflights_tick').show();
		$('.rooflights1 div:nth-child(2)').addClass('active');
		$('.rooflights2').css('transform', 'translate3d(-451px, 0px, 0px)');
		
		$('.rooflights1 div:nth-child(1)').removeClass('active');
		$('.rooflights1 div:nth-child(3)').removeClass('active');
	}
	else if(id==18)
	{
		$('.rooflights_tick').show();
		$('.rooflights1 div:nth-child(3)').addClass('active');
		$('.rooflights2').css('transform', 'translate3d(-902px, 0px, 0px)');
		
		$('.rooflights1 div:nth-child(1)').removeClass('active');
		$('.rooflights1 div:nth-child(2)').removeClass('active');
	}
	else if(id==1)
	{
		$('.door_tick').show();
		$('.doors1 div:nth-child(1)').addClass('active');
		$('.doors2').css('transform', 'translate3d(0px, 0px, 0px)');
		
		$('.doors1 div:nth-child(3)').removeClass('active');
		$('.doors1 div:nth-child(2)').removeClass('active');
	}
	else if(id==2)
	{
		$('.door_tick').show();
		$('.doors1 div:nth-child(2)').addClass('active');
		$('.doors2').css('transform', 'translate3d(-455px, 0px, 0px)');
		
		$('.doors1 div:nth-child(1)').removeClass('active');
		$('.doors1 div:nth-child(3)').removeClass('active');
	}

	else if(id==16)
	{
		$('.heating_tick').show();
		$('.heating1 div:nth-child(1)').addClass('active');
		$('.heating2').css('transform', 'translate3d(0px, 0px, 0px)');
		
		$('.heating1 div:nth-child(2)').removeClass('active');
		$('.heating1 div:nth-child(3)').removeClass('active');
	}
	else if(id==17)
	{
		$('.heating_tick').show();
		$('.heating1 div:nth-child(2)').addClass('active');
		$('.heating2').css('transform', 'translate3d(-455px, 0px, 0px)');
		
		$('.heating1 div:nth-child(1)').removeClass('active');
		$('.heating1 div:nth-child(3)').removeClass('active');
	}
	else if(id==12)
	{
		$('.chimney_tick').show();
		$('.chimney1 div:nth-child(1)').addClass('active');
		$('.chimney2').css('transform', 'translate3d(0px, 0px, 0px)');
		
		$('.chimney1 div:nth-child(2)').removeClass('active');
		$('.chimney1 div:nth-child(3)').removeClass('active');
	}
	else if(id==14)
	{
		$('.chimney_tick').show();
		$('.chimney1 div:nth-child(2)').addClass('active');
		$('.chimney2').css('transform', 'translate3d(-455px, 0px, 0px)');
		
		$('.chimney1 div:nth-child(1)').removeClass('active');
		$('.chimney1 div:nth-child(3)').removeClass('active');
	}
	else if(id==9)
	{
		$('.floor_tick').show();
		$('.floor1 div:nth-child(1)').addClass('active');
		$('.floor2').css('transform', 'translate3d(0px, 0px, 0px)');
		
		$('.floor1 div:nth-child(2)').removeClass('active');
		$('.floor1 div:nth-child(3)').removeClass('active');
	}
	else 
	{
		$('.floor_tick').show();
		$('.floor1 div:nth-child(2)').addClass('active');
		$('.floor2').css('transform', 'translate3d(-455px, 0px, 0px)');
		
		$('.floor1 div:nth-child(1)').removeClass('active');
		$('.floor1 div:nth-child(3)').removeClass('active');
	}

}
function validate_user()
{
	$('.summary_btn').hide();
	var name=$('#byp_name').val();
	var surname=$('#byp_surname').val();
	var email=$('#byp_email').val();
	var passwd=$('#byp_password').val();
	var retype_passwd=$('#byp_repassword').val();
	var phone=$('#byp_phone').val();
	var house=$('#byp_house').val();
	var postcode=$('#byp_postcode_f').val();
	var msg=$('#byp_msg').val();
	var when_to_start=$('#when_to_start').val();
	var tick_done=$('#current_status').val();

	if(name && surname && email && passwd && retype_passwd && phone && house && postcode && msg && when_to_start && tick_done)
	{
		$('.tick_done').show();

		$.ajax({
			type:'POST',
			url:'register.php',
			data:{	name:name , surname:surname, email:email,register_email:email,phone:phone,postcode_f:postcode,house:house,register_password:passwd},
			success: function(data){


			}
		}); 


		var property_type=$('#property_type').val();
		var bedrooms=2;
		var cb_door=$('input[name=cb_door]:checked').val();
		var cb_floor=$('input[name=cb_floor]:checked').val();
		var cb_heating=$('input[name=cb_heating]:checked').val();
		var cb_kitchen=$('input[name=cb_kitchen]:checked').val();
		var cb_roof=$('input[name=cb_roof]:checked').val();
		var cb_wall=$('input[name=cb_wall]:checked').val();
		var measure_unit_id= "m";
		var room_length=$('#depth').val();

		var room_width=$('#width').val();
		var estimate_information=$('#estimate_information').val();
		if(estimate_information==0)
		{
			var room_size=room_length*room_width;
			var bedrooms=2;
			var terrace_type=1;
		}
		else 
		{
			var room_size=estimate_information;
			var bedrooms=$('#bedrooms_size').val();
			var terrace_type = $("input[name='terrace_type']:checked").val();
		}

		var start=$('#when_to_start').val();
		var status=$('#current_status').val();

		var addr="";
		var use_size_estimator=true;
		$.ajax({
			type:'POST',
			url:'getCosting.php',
			data:{	addr:addr , bedrooms:bedrooms, cb_door:cb_door,cb_floor:cb_floor,cb_heating:cb_heating,cb_kitchen:cb_kitchen,cb_roof:cb_roof,cb_wall:cb_wall,email:email,house:house,measure_unit_id:measure_unit_id,message:msg,name:name,password:passwd,phone:phone,postcode:postcode,
				property_type:property_type,room_length:room_length,room_size:room_size,room_width:room_width,start:start,status:status,surname:surname,terrace_type:terrace_type,use_size_estimator:use_size_estimator 

			},
			success: function(data){

				$(".get_response").html(data);

				var first= $('#first').text();

				$('#detail_heading').html(first);

				var second= $('#second').text();

				$('#roof').html(second);

				var third= $('#third').text();

				$('#structural').html(third);


			}
		});

		setTimeout(function() {
			$('#step_2').hide();
			$('#step_1').hide();
			$('#step_3').hide();	
			$('#step_4').hide();	
			$('#step_5').hide();
			$('#result_1').show();

		}, 5000)
	}
	else 
	{
		alert("Please enter all the values");	
	}

}

function step_2()
{
	$('#step_2').show();
	$('#step_1').hide();
	$('#step_3').hide();	
	$('#step_4').hide();	
	$('#step_5').hide();	
	
}
function step_3()
{
	$('#step_2').hide();
	$('#step_1').hide();
	$('#step_3').show();	
	$('#step_4').hide();	
	$('#step_5').hide();	
}
function step_4()
{
	$('#step_2').hide();
	$('#step_1').hide();
	$('#step_3').hide();	
	$('#step_4').show();	
	$('#step_5').hide();	
}
function step_5()
{
	$('#step_2').hide();
	$('#step_1').hide();
	$('#step_3').hide();	
	$('#step_4').hide();	
	$('#step_5').show();
	$('.tick_done').hide();
}
function show_real_images(id)
{
	if(id==1)
	{
		$('.image_scrap_extension').hide();
		$('.image_real_extension').show();
		$('.image_content_extension').hide();
	}
	else if(id==2)
	{
		$('.image_scrap_roof').hide();
		$('.image_content_roof').hide();
		$('.image_real_roof').show();
	}
	else if(id==3)
	{
		$('.image_scrap_door').hide();
		$('.image_content_door').hide();
		$('.image_real_door').show();
	}
	else if(id==4)
	{

		$('.image_scrap_heating').hide();
		$('.image_content_heating').hide();
		$('.image_real_heating').show();
	}
	else if(id==5)
	{
		$('.image_scrap_chimeny').hide();
		$('.image_content_chimeny').hide();
		$('.image_real_chimeny').show();
	}
	else
	{
		$('.image_scrap_floor').hide();
		$('.image_content_floor').hide();
		$('.image_real_floor').show();
	}
	
	
}
function show_real_images1(id)
{
	if(id==1)
	{
		$('.image_scrap_extension').show();
		$('.image_real_extension').hide();
		$('.image_content_extension').hide();
	}
	else if(id==2)
	{
		$('.image_scrap_roof').show();
		$('.image_content_roof').hide();
		$('.image_real_roof').hide();
	}
	else if(id==3)
	{
		$('.image_scrap_door').show();
		$('.image_content_door').hide();
		$('.image_real_door').hide();
	}
	else if(id==4)
	{
		$('.image_scrap_heating').show();
		$('.image_content_heating').hide();
		$('.image_real_heating').hide();
	}
	else if(id==5)
	{
		$('.image_scrap_chimeny').show();
		$('.image_content_chimeny').hide();
		$('.image_real_chimeny').hide();
	}
	else
	{
		$('.image_scrap_floor').show();
		$('.image_content_floor').hide();
		$('.image_real_floor').hide();
	}
	
	
}

function show_content(id)
{
	if(id==1)
	{
		$('.image_scrap_extension').hide();
		$('.image_real_extension').hide();
		$('.image_content_extension').show();
		
	}
	else if(id==2)
	{
		$('.image_scrap_roof').hide();
		$('.image_real_roof').hide();
		$('.image_content_roof').show();
	}
	else if(id==3)
	{
		$('.image_scrap_door').hide();
		$('.image_real_door').hide();
		$('.image_content_door').show();
	}
	else if(id==4)
	{
		$('.image_scrap_heating').hide();
		$('.image_real_heating').hide();
		$('.image_content_heating').show();
	}
	else if(id==5)
	{
		$('.image_scrap_chimeny').hide();
		$('.image_real_chimeny').hide();
		$('.image_content_chimeny').show();
	}
	else
	{
		$('.image_scrap_floor').hide();
		$('.image_real_floor').hide();
		$('.image_content_floor').show();
	}
	
	
}
function validateEmail(sEmail) {
	alert(sEmail);
	var filter = /^[\w\-\.\+]+\@[a-zA-Z0-9\.\-]+\.[a-zA-z0-9]{2,4}$/;
	if (filter.test(sEmail)) {
		return true;
	} else {
		return false;
	}
}
	/* function show_roof_image(id)
	{
		alert(id); 
	if(id==1)
	{
		$('.owl-pagination div:nth-child(1)').addClass('active');
		$('.owl-wrapper').css('transform', 'translate3d(0px, 0px, 0px)');
		
		$('.owl-pagination div:nth-child(2)').removeClass('active');
		$('.owl-pagination div:nth-child(3)').removeClass('active');
		
	}
	else if(id==2)
	{
			$('.owl-pagination div:nth-child(2)').addClass('active');
		$('.owl-wrapper').css('transform', 'translate3d(-45px, 0px, 0px)');
		
		$('.owl-pagination div:nth-child(1)').removeClass('active');
		$('.owl-pagination div:nth-child(3)').removeClass('active');
	}

		else 
		{
			$('.owl-pagination div:nth-child(3)').addClass('active');
		$('.owl-wrapper').css('transform', 'translate3d(-950px, 0px, 0px)');
		
		$('.owl-pagination div:nth-child(2)').removeClass('active');
		$('.owl-pagination div:nth-child(1)').removeClass('active');
		}
		
		
		
	} */
	
</script>
<script>
	$(document).ready(function() {

	});



</script>
<style>
	#footer1{}
	p.loader {
		width: 100%;
		float: left;
	}
	#byp_img_loading {
		display:none;
		float:none;
		width:132px;
		height:64px;
		padding-top:10px;
		text-align:center;
		margin:0 auto;

	} 

	.search_col {
		display: inline-block !important;
		position: relative !important;
		width: 100% !important;
	}
	.search_col .srch_input {
		border: 4px solid #6687a6 !important;
		box-sizing: border-box !important;
		display: inline-block !important;
		font-size: 15px !important;
		outline: medium none !important;
		padding: 10px 35px 10px 10px !important;
		width: 100% !important;
	}
	input#byp_postcode {

		height: 44px !important;
	}
	.field_set input, .field_set select, .field_set textarea {
		color: #666;
		font: 12px Arial,Helvetica,sans-serif;
		vertical-align: bottom !important;
	}
	
	.cal_help {
		vertical-align: bottom !important;
	}
	.soflow {
		width: 48px;
	}

	.item {
		font-size: 20px;
	}
	input[type="radio"] {
		-webkit-appearance: checkbox;
		-moz-appearance: checkbox;
		-ms-appearance: checkbox;     /* not currently supported */
		-o-appearance: checkbox;      /* not currently supported */
	}
	.into_col {
		vertical-align: bottom !important;
	}
	.width_aro:before {
		content: "";
		position: absolute;
		left: -5px;
		top: -4px;
		width: 0;
		height: 0;
		border-bottom: 5px solid #827f76;
		border-right: 5px solid transparent;
		border-left: 5px solid transparent;
	}
	.width_aro:after {
		content: "";
		position: absolute;
		right: -4px;
		bottom: -4px;
		width: 0;
		height: 0;
		border-top: 5px solid #827f76;
		border-right: 5px solid transparent;
		border-left: 5px solid transparent;
	}
	.detail_heading.confrm {
		text-align: left;
		width: 90%;
		margin: 0 auto;
	}

	.close
	{
		color:#f5641e;
		font-size:12px;
		font-weight:bold;
		line-height:1.1em;
		background:transparent;
		height: 11px;
		position: absolute;
		right: 12px;
		top: 14px;
		padding: 2px 22px 4px 5px;
		/*width: 40px;*/
	}
	.dimensn	{font-family: 'Droid Serif', serif !important;}
	.main_list_col .choose_die p{margin-right:15px !important;padding-top:12px;}
	body{font-family: 'Open Sans', sans-serif;}
	.fa.fa-sm.fa-minus {
		font-size: 23px;
	}
	.fa.fa-sm.fa-plus {
		font-size: 23px;
	}
	.fa.fa-plus {
		font-size: 23px;
	}
	.fa.fa-minus {
		font-size: 23px;
	}
	.fa.fa-angle-right {
		font-size: 19px;
	}
	.sup{ vertical-align: super; }
	.fa.fa-camera.fa-sm {
		font-size: 23px;
	}
	.icon.ion-android-expand{font-size:23px;}









	* {
		box-sizing: border-box;
	}

	.row > .column {
		padding: 0 8px;
	}

	.row:after {
		content: "";
		display: table;
		clear: both;
	}

	.column {
		float: left;
		width: 25%;
	}

	/* The Modal (background) */
	.modal {
		display: none;
		position: fixed;
		z-index: 1;
		padding-top: 100px;
		left: 0;
		top: 0;
		width: 100%;
		height: 100%;
		overflow: auto;
		background-color: #4d7499;

	}

	/* Modal Content */
	.modal-content {
		position: relative;
		margin: auto;
		padding: 0;
		width: 90%;
		max-width: 1200px;
	}

	/* The Close Button */
	.close {
		color: #f5641e;
		position: absolute;
		top: 10px;
		right: 25px;
		font-size: 35px;
		font-weight: bold;
	}

	.close:hover,
	.close:focus {
		color: #999;
		text-decoration: none;
		cursor: pointer;
	}

	.mySlides {
		display: none;
	}

	.close.cursor {
		cursor: pointer;
		color: #fff !important;
		left: 19%;
		top: 96px !important;
		max-width: 1000px;
		margin: 0 auto;
		left: 0px;
		right: 0px !important;
		width: 100%;
		text-align:right;
	}

	/* Next & previous buttons */
	.prev,
	.next {
		cursor: pointer;
		position: absolute;
		top: 50%;
		width: auto;
		padding: 16px;
		margin-top: -50px;
		color: #f5641e;
		font-weight: bold;
		font-size: 20px;
		transition: 0.6s ease;
		border-radius: 0 3px 3px 0;
		user-select: none;
		-webkit-user-select: none;
	}

	/* Position the "next button" to the right */
	.next {
		right: 0;
		border-radius: 3px 0 0 3px;
	}

	/* On hover, add a black background color with a little bit see-through */
	.prev:hover,
	.next:hover {
		background-color: rgba(0, 0, 0, 0.8);
	}

	/* Number text (1/3 etc) */
	.numbertext {
		color: #f2f2f2;
		font-size: 12px;
		padding: 8px 12px;
		position: absolute;
		bottom: -30px;
		left:105px;
	}

	img {
		margin-bottom: -2px;
	}

	.caption-container {
		text-align: center;
		background-color: black;
		padding: 2px 16px;
		color: white;
	}

	.demo {
		opacity: 0.6;
	}

	.active,
	.demo:hover {
		opacity: 1;
	}

	img.hover-shadow {
		transition: 0.3s
	}

	.hover-shadow:hover {
		box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
	}
	.close {
		color: #f5641e;
		font-size: 35px;
		font-weight: bold;
		position: absolute;
		right: 75px;
		top: 110px;
		z-index: 99999;
	}
	.close1 {
		color: #f5641e;
		font-size: 35px;
		font-weight: bold;
		position: absolute;
		right: 75px;
		top: 110px;
		z-index: 99999;
	}
	.sl_img {
		height: 560px;
		max-width: 100%;
		width: auto;

	}
	.summary_btn {
		width: 89px;
		padding: 14px 6px 9px 11px !important;
		background: #6687a6;
		font-size: 16px;
		color: #fff;
		cursor: pointer;
		position: fixed;
		left: -27px;
		top: 54%;
		-ms-transform: rotate(90deg);
		-webkit-transform: rotate(90deg);
		transform: rotate(90deg);
		border-radius: 14px 14px 0px 0px;
	}

	span.depth_size {

		font-weight: 100;
		font-size: 12px;
	}

	.cal_tip_area p{border:2px solid #6687a6;}
	.cal_tip_area::after{border-right: 9px solid #6687a6;}

	.cal_tip_area:before {
		content: "";
		position: absolute;
		top: 10px;
		left: -6px;
		border-top: 10px solid transparent;
		border-bottom: 10px solid transparent;
		border-right: 10px solid #fff !important;
		z-index: 999;
	}
	.cal_tip_area1 p{border:2px solid #6687a6;}
	.cal_tip_area1::after{border-right: 9px solid #6687a6;}

	.cal_tip_area1:before {
		content: "";
		position: absolute;
		top: 10px;
		left: -3px;
		border-top: 10px solid transparent;
		border-bottom: 10px solid transparent;
		border-right: 10px solid #fff !important;
		z-index: 999;
	}
	.fa.fa-sm.fa-minus {
		font-size: 13px;
		color: #f5641e;
		vertical-align: middle;
		padding-left: 20px;
	}
	.fa.fa-sm.fa-plus {
		font-size: 13px;
		color: #f5641e;
		vertical-align: middle;
		padding-left: 20px;
	} 
	.fa.fa-plus {
		font-size: 13px;
		color: #f5641e;
		vertical-align: middle;
		padding-left: 20px;
	}
	.fa.fa-minus {
		font-size: 13px;
		color: #f5641e;
		vertical-align: middle;
		padding-left: 20px;
	}
	.mySlides,.mySlides2,.mySlides4,.mySlides6,.mySlides8,.mySlides10,.mySlides1,.mySlides3,.mySlides5,.mySlides7,.mySlides9,.mySlides11 {text-align:center;}

	.icon.ion-android-expand {
		font-size: 23px;
		margin-left: 7px;
	}
	#byp_addr{display:none !important;}
</style>

<script type="text/javascript">
	$( document ).ready(function() {

    <?php // auto login 
    if (isset($_SESSION['userid']) && $_SESSION['userid']) {
    	echo 'global_byp_config.userid = parseInt("'.$_SESSION['userid'].'");';

    	if (isset($_SESSION['user_info']['firstname'])) {
    		echo 'document.getElementById("byp_name").value = "'.htmlspecialchars($_SESSION['user_info']['firstname'], ENT_QUOTES, 'UTF-8').'";';
    	}

    	if (isset($_SESSION['user_info']['surname'])) {
    		echo 'document.getElementById("byp_surname").value = "'.htmlspecialchars($_SESSION['user_info']['surname'], ENT_QUOTES, 'UTF-8').'";';
    	}

    	if (isset($_SESSION['user_info']['phone'])) {
    		echo 'document.getElementById("byp_phone").value = "'.htmlspecialchars($_SESSION['user_info']['phone'], ENT_QUOTES, 'UTF-8').'";';
    	}

    	if (isset($_SESSION['user_info']['address'])) {
    		echo 'document.getElementById("byp_house").value = "'.htmlspecialchars($_SESSION['user_info']['address'], ENT_QUOTES, 'UTF-8').'";';
    	}

    	if (isset($_SESSION['quote_info'])) {

    		if (isset($_SESSION['quote_info']['postcode'])) {
    			echo 'document.getElementById("byp_postcode").value = "'.$_SESSION['quote_info']['postcode'].'";';
    		}

    		if (isset($_SESSION['quote_info']['property_type']) && 'flat'==$_SESSION['quote_info']['property_type']) {
    			echo 'document.fm_byp1.property_type[1].checked=true;';
    		}

    		if (isset($_SESSION['quote_info']['measure_unit_id']) && 2 == $_SESSION['quote_info']['measure_unit_id']) {
    			echo 'document.fm_byp1.dim[0].checked=true;';
    		}          

    	}
    }
    ?>
    
  //debug
  /*
  document.getElementById("byp_slide_box").style.display = "none";
  document.getElementById("byp_get_costing").style.display = "block";
  //document.getElementById("byp_get_costing").style.textAlign = "left";
  
  document.getElementById("fm_byp5").quote_type_id.value = "email";
  
  if ("email"==document.getElementById("fm_byp5").quote_type_id.value) document.getElementById("thank_you_msg").innerHTML = "Your quote has been emailed and should arrive very shortly. We will be in touch shortly to arrange an appointment for a site visit.";
  else document.getElementById("thank_you_msg").innerHTML = "Your quote has been saved. You can retrieve it at any time in <a href=\"#\">your account page</a>.";
  
  document.getElementById("byp_get_costing").innerHTML = document.getElementById("byp_thank_you").innerHTML;
  
  
  global_byp_config.lightbox = $.lightbox({
    content: document.getElementById("login_dialog").innerHTML
  });
  //debug
  */

});

	function preloadAfterInit() {
		<?php if (isset($_SESSION['quote_info'])) {

			if (isset($_SESSION['quote_info']['room_length']) && (int)$_SESSION['quote_info']['room_length'] && isset($_SESSION['quote_info']['room_width']) && (int)$_SESSION['quote_info']['room_width']) {
				echo 'var depth=parseInt("'.$_SESSION['quote_info']['room_length'].'");';
				echo 'var width=parseInt("'.$_SESSION['quote_info']['room_width'].'");';
				?>
				for(var i=0;i<document.fm_byp1.depth.options.length;i++) {
					if (document.fm_byp1.depth.options[i].value==depth) {
						document.fm_byp1.depth.options.selectedIndex = i;
						break;
					}
				}
				for(i=0;i<document.fm_byp1.width.options.length;i++) {
					if (document.fm_byp1.width.options[i].value==width) {
						document.fm_byp1.width.options.selectedIndex = i;
						break;
					}
				}
				bypCalcRoomArea();
				<?php
      } // endif set length and width 
      
      if (isset($_SESSION['quote_info']['use_room_size_estimator']) && $_SESSION['quote_info']['use_room_size_estimator']) {

      	echo 'bypSwitchEstimator(1);';

      	$a_est = explode('|', $_SESSION['quote_info']['use_room_size_estimator']);
      	echo 'var est_br = parseInt("'.$a_est[0].'");';
      	echo 'var est_tt = parseInt("'.$a_est[1].'");';
      	?>
      	for (i=0;i<document.fm_byp1.bedrooms.options.length;i++) {
      		if (document.fm_byp1.bedrooms.options[i].value==est_br) {
      			document.fm_byp1.bedrooms.options.selectedIndex = i;
      			break;
      		}
      	}
      	if (1==est_tt) document.fm_byp1.terrace_type[0].checked=true;
      	if (2==est_tt) document.fm_byp1.terrace_type[1].checked=true;
      	bypEstimateRoomArea();
      	<?php
      }
      
      if (isset($_SESSION['quote_info']['subcategory_1_component_id']) && isset($_SESSION['quote_info']['subcategory_2_component_id']) && isset($_SESSION['quote_info']['subcategory_3_component_id']) && isset($_SESSION['quote_info']['subcategory_4_component_id']) && isset($_SESSION['quote_info']['subcategory_5_component_id']) && isset($_SESSION['quote_info']['subcategory_6_component_id'])) {

      	$a_component = array(
          'cb_door' => 1,     // subcategory_1_component_id
          'cb_roof' => 2,     // subcategory_2_component_id
          'cb_wall' => 3,     // subcategory_3_component_id
          'cb_floor' => 4,    // subcategory_4_component_id
          'cb_kitchen' => 5,  // subcategory_5_component_id
          'cb_heating' => 6   // subcategory_6_component_id
          );

      	echo 'var a_comp=[];';

      	foreach ($a_component AS $k => $v) {
      		echo 'a_comp["'.$k.'"]=parseInt("'.$_SESSION['quote_info']['subcategory_'.$v.'_component_id'].'");';
      	}

      	?>
      	var j = 0;
      	for (var i in global_byp_config.panel) {

      		key = global_byp_config.panel[i];

        var a_p = i.split("_"); // byp_step3_col4
        
        a_p = a_p[1].replace("step", "");
        
        eval("var fm_el = document.fm_byp" + a_p + "."+key+";");
        
        for(var y=0;y<fm_el.length;y++) {

        	if (fm_el[y].value == a_comp[key]) {
        		fm_el[y].checked = true;
        		a_p = key.replace("cb_", "byp_");
        		a_p += "_"+a_comp[key];
        		bypSelectComponent(a_p);
        		break;
        	}
        }

        j++;
    }



    <?php
      } // end if subcategories components
      
      if ( isset($_SESSION['quote_info']['name']) && isset($_SESSION['user_info']['firstname']) && isset($_SESSION['user_info']['surname']) && $_SESSION['quote_info']['name'] != $_SESSION['user_info']['firstname'].' '.$_SESSION['user_info']['surname'] ) {

      	$a_name = explode(' ', $_SESSION['quote_info']['name']);
      	$firstname = array_shift($a_name);
      	$surname = implode(' ', $a_name);

      	echo 'document.getElementById("byp_name").value = "'.htmlspecialchars($firstname, ENT_QUOTES, 'UTF-8').'";';

      	echo 'document.getElementById("byp_surname").value = "'.htmlspecialchars($surname, ENT_QUOTES, 'UTF-8').'";';

      }
      
      if (isset($_SESSION['quote_info']['phone'])) {
      	echo 'document.getElementById("byp_phone").value = "'.htmlspecialchars($_SESSION['quote_info']['phone'], ENT_QUOTES, 'UTF-8').'";';
      }

      if (isset($_SESSION['quote_info']['address'])) {
      	echo 'document.getElementById("byp_house").value = "'.htmlspecialchars($_SESSION['quote_info']['address'], ENT_QUOTES, 'UTF-8').'";';
      }
      
      if (isset($_SESSION['quote_info']['message'])) {
      	echo 'document.getElementById("byp_msg").value = "'.htmlspecialchars($_SESSION['quote_info']['message'], ENT_QUOTES, 'UTF-8').'";';
      }
      
      if (isset($_SESSION['quote_info']['start'])) {
      	echo 'var a_p="'.$_SESSION['quote_info']['start'].'";';
      	?>
      	document.getElementById("when_to_start").value = a_p;
      	<?php
      } // endif start
      
      if (isset($_SESSION['quote_info']['status'])) {
      	echo 'var a_p="'.$_SESSION['quote_info']['status'].'";';
      	?>
      	document.getElementById("current_status").value = a_p;
      	<?php
      }
      
      // set qid!
      if (isset($_SESSION['quote_info']['qid'])) echo 'global_byp_config.qid=parseInt("'.$_SESSION['quote_info']['qid'].'");';

      echo "<script type='text/javascript'> ga('send', 'event', 'quote', 'submit'); </script>";
      
      unset($_SESSION['quote_info']);
      
  } // endif set quote_info  ?>
}  
</script>

<script>
	$( document ).ready(function() {

		$('#byp_name').change(function()
		{

			$('.tick_done_name').show();

		});
		$('#byp_surname').change(function()
		{

			$('.tick_done_surname').show();

		});
		$('#byp_email').change(function()
		{
			var emil=$('#byp_email').val();
			var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if( !emailReg.test( emil ) ) {
				alert('Please enter valid email');
			} else {
				$('.tick_done_email').show();
			}



		});

		$('#byp_password').change(function()
		{

	//	$('.tick_done_password').show();

});
		$('#byp_repassword').change(function()
		{
			var password=$('#byp_password').val();
			var retype_passwd=$('#byp_repassword').val();

			if(password==retype_passwd){
				$('.tick_done_repassword').show();
				$('.tick_done_password').show();
			}
			else 
			{
				alert('please enter the same password ');
				$('#byp_repassword').val('');
				$('#byp_password').val('');

			}
		});
		$('#byp_phone').change(function()
		{

			$('.tick_done_phone').show();

		});
		$('#byp_house').change(function()
		{

			$('.tick_done_house').show();

		});
		$('#byp_postcode_f').change(function()
		{

			$('.tick_done_postcode').show();

		});
		$('#byp_msg').change(function()
		{

			$('.tick_done_msg').show();

		});
		$('#when_to_start').change(function()
		{

			$('.tick_done_start').show();

		});
		$('#current_status').change(function()
		{

			$('.tick_done_status').show();

		});
	});


</script>
<script>
	$( document ).ready(function() {
		$('.image').click(function() { 

			$('#myModal').hide();
		});
		$('.image1').click(function() { 

			$('#myModal1').hide();
			$('#myModal').show();
		});

		$('.image2').click(function() { 

			$('#myModal2').hide();

		});
		$('.image3').click(function() { 

			$('#myModal3').hide();
			$('#myModal2').show();
		});
		$('.image4').click(function() { 

			$('#myModal4').hide();

		});
		$('.image5').click(function() { 

			$('#myModal5').hide();
			$('#myModal4').show();
		});
		$('.image6').click(function() { 

			$('#myModal6').hide();

		});
		$('.image7').click(function() { 

			$('#myModal7').hide();
			$('#myModal6').show();
		});
		$('.image8').click(function() { 

			$('#myModal8').hide();

		});
		$('.image9').click(function() { 

			$('#myModal9').hide();
			$('#myModal8').show();
		});
		$('.image10').click(function() { 

			$('#myModal10').hide();

		});
		$('.image11').click(function() { 

			$('#myModal11').hide();
			$('#myModal10').show();
		});
		$('.page0').click(function() { 

			alert("hi");
		});
	});


	if (screen.width > 768) {
		$(".mob_help").click(function(){
			$(".cal_tip_areas").show();	});
		
		
		$(".room_close").click(function(){
			$(".cal_tip_areas").fadeOut();	});
	}
	else if (screen.width < 768) {

		$(".mob_help").click(function(){
			$(".cal_tip_areas_mob").show();	});
		
		
		$(".room_close").click(function(){
			$(".cal_tip_areas_mob").fadeOut();	});
	}
	/* $(".mob_help").click(function(){
		$(".cal_tip_areas").show();	});
		
		
		$(".room_close").click(function(){
			$(".cal_tip_areas").fadeOut();	}); */


/* 
$(".mob_help").click(function(){
		$(".cal_tip_areas_mob").show();	});
		
		
		$(".room_close").click(function(){
			$(".cal_tip_areas_mob").fadeOut();	}); */
		</script>
<script>
	$(document).ready(function(e) {
        $(".dontknow").click(function(){
			$(".cal_tip_areas").show();	});
		
		
		$(".room_close").click(function(){
			$(".cal_tip_areas").fadeOut();	});
    });
</script>
<script>
$(document).ready(function(e) {
    $(".pre_btn").click(function(){
			$(window).scrollTop(0);
		});
	$(".next_btn").click(function(){
			$(window).scrollTop(0);
		});	
});
</script>	
	<?php

	require_once('./inc/footer.inc.php');

	?>