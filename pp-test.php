<?php $page_name = "pp_booking"; ?>
<?php require_once('./inc/header.inc.php'); ?>
<!-- PP booking page -->
<div class="prepurch_wrapper">
	<div class="purch_header">
		<div class="center">
			<h4>Confirm Build Cost & Feasibility with a Pre-Purchase Visit</h4>
			<p>We have designed this service for prospective homeowners who are looking to purchase a property but want to understand more about the potential to extend. A member of our Architectural Team will visit the property with you and advise on how to extend and/or reconfigure the existing layout, the planning considerations and the estimated build costs.
			</p>
		</div>
	</div>
	<section class="service">
		<div class="center">
			<div class="sevice_row">
				<div class="services_list">
					<h5 class="title">Service Includes:</h5>
					<ul class="ser_list">
						<li>
							<img src="assets_b/img/ser_icon.png" class="ser_icon sr_list_item" />
							<span class="ser_ttl sr_list_item">Site visit at the property</span>
						</li>
						<li>
							<img src="assets_b/img/ser_icon.png" class="ser_icon sr_list_item" />
							<span class="ser_ttl sr_list_item">Survey of proposed floor area</span>
						</li>
						<li>
							<img src="assets_b/img/ser_icon.png" class="ser_icon sr_list_item" />
							<span class="ser_ttl sr_list_item">Creative design ideas</span>
						</li>
						<li>
							<img src="assets_b/img/ser_icon.png" class="ser_icon sr_list_item" />
							<span class="ser_ttl sr_list_item">Specialist planning advice</span>
						</li>
						<li>
							<img src="assets_b/img/ser_icon.png" class="ser_icon sr_list_item" />
							<span class="ser_ttl sr_list_item">Cost advice on optimising your budget</span>
						</li>
						<li>
							<img src="assets_b/img/ser_icon.png" class="ser_icon sr_list_item" />
							<span class="ser_ttl sr_list_item">Follow-up proposal within 1 working day</span>
						</li>
					</ul>
				</div>
				<div class="checkout_form">
					<div class="checkout_header">
						<div class="checkout_header_ttl">
							Fill the form below to book the Pre-Purchase:
						</div>
						<div class="checkout_header_amount">
							£150 + VAT
						</div>
					</div>
					<form class="check_form">
						<div class="check_form_row">
							<fieldset>
								<label for="ch_f_name">First Name<span>*</span></label>
								<input id="ch_f_name" type="text" name="f_name" required="" />
							</fieldset>
							<fieldset>
								<label for="ch_s_name">Surname<span>*</span></label>
								<input id="ch_s_name" type="text" name="s_name" required="" />
							</fieldset>
						</div>
						<div class="check_form_row">
							<fieldset>
								<label for="ch_email">Email Address<span>*</span></label>
								<input id="ch_email" type="email" name="email" required="" />
							</fieldset>
							<fieldset>
								<label for="ch_phone">Phone Number<span>*</span></label>
								<input id="ch_phone" type="text" name="phone" required="" />
							</fieldset>
						</div>
						<div class="check_form_row">
							<fieldset>
								<label for="ch_address">Property Address<span>*</span></label>
								<input id="ch_address" type="text" name="address" required="" />
							</fieldset>
							<fieldset>
								<label for="ch_postcode">Postcode<span>*</span></label>
								<input id="ch_postcode" type="text" name="postcode" required="" />
							</fieldset>
						</div>
						<div class="check_form_row">
							<fieldset>
								<label for="ch_link">Rightmove Link<span>*</span></label>
								<input id="ch_link" type="text" name="link" required="" />
							</fieldset>
							<fieldset>
								<label for="ch_discount">Discount Code</label>
								<input id="ch_discount" type="text" name="discount" />
							</fieldset>
						</div>
						<div class="checkout_footer">
							<div class="payment_methods_list">
								<img src="assets_b/img/pay_method_icon/visa.png" alt="">
								<img src="assets_b/img/pay_method_icon/mastercard.png" alt="">
								<img src="assets_b/img/pay_method_icon/express.png" alt="">
								<img src="assets_b/img/pay_method_icon/maestro.png" alt="">
								<img src="assets_b/img/pay_method_icon/applepay.png" alt="">
								<img src="assets_b/img/pay_method_icon/googlepay.png" alt="">
							</div>
							<button type=submit>
								<span>Checkout</span>
								<img src="assets_b/img/checkout_icon.png" alt="">
							</button>
							<button id="checkout-button" style="display: none;"></button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<section class="faq">
		<div class="center">
			<h5 class="title">Frequently Asked Questions</h5>
			<div class="faq_list">
				<div class="faq__item">
					<div class="faq_q">
						<h6>How do I get access for the visit?</h6>
						<img src="assets_b/img/faq_arrow.png" alt="" />
					</div>
					<div class="faq_answer">
						Depending on how far you are through the buying process, you will need to arrange access via the Estate Agent. This is where our flexible appointments come in useful, as we’re arranging an appointment with you, the agent and us.
					</div>
				</div>
				<div class="faq__item">
					<div class="faq_q">
						<h6>How long does the visit last?</h6>
						<img src="assets_b/img/faq_arrow.png" alt="" />
					</div>
					<div class="faq_answer">
						This completely depends on you – we would never leave you with unanswered questions. Depending on the size of the property and the scope of works, site visits can last anywhere between 20 minutes and an hour.
					</div>
				</div>
				<div class="faq__item">
					<div class="faq_q">
						<h6>How long will it take to receive the quote for the Build?</h6>
						<img src="assets_b/img/faq_arrow.png" alt="" />
					</div>
					<div class="faq_answer">
						We understand that time is of the essence when it comes to buying a property, so we issue quotes within 1 working day of the site visit.
					</div>
				</div>
				<div class="faq__item">
					<div class="faq_q">
						<h6>Can I start the Design Phase before I exchange or complete?</h6>
						<img src="assets_b/img/faq_arrow.png" alt="" />
					</div>
					<div class="faq_answer">
						Yes absolutely. Purchasing a property can be time consuming so it can be beneficial to get stuck into the Design Phase during this period. However, and for simplicity, we would advise that you are the legal owner before a planning application is submitted.
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Payment Status -->
	<div class="payment_status">
		<img src="assets_b/img/payment_successful_icon.png" alt="" />
	</div>
</div>
<script type="text/javascript">
    // Create an instance of the Stripe object with your publishable API key
    var stripe = Stripe("pk_live_51IVhwPBZ22hIs90im4mmg5npsrDQOMt8PsgccYtXN6TUszzY29X7ClyNAARn4MnaWrtC0GWAs2ILHjZEDjrsSIlF0064ljz1St");
    var checkoutButton = document.getElementById("checkout-button");

    checkoutButton.addEventListener("click", function () {

      	searchParams = new URLSearchParams()
		searchParams.set('user_email', document.getElementById("ch_email").value)
		searchParams.set('discount', document.getElementById("ch_discount").value)

      fetch("/api/pay.php", {
        method: "POST",
        body: searchParams
      })
        .then(function (response) {
          return response.json();
        })
        .then(function (session) {
          prepurchase_checkout(session.id) //
          return stripe.redirectToCheckout({ sessionId: session.id });
        })
        .then(function (result) {
          // If redirectToCheckout fails due to a browser or network
          // error, you should display the localized error message to your
          // customer using error.message.
          if (result.error) {
            alert(result.error.message);
          }
        })
        .catch(function (error) {
          console.error("Error:", error);
        });
    });
  </script>
<?php require_once('./inc/footer.inc.php'); ?>