<?php

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    	<h1>What we do</h1>
      <p>Build Team&rsquo;s roots trace back to Camberwell in South East London.  Dan moved to the area over 10 years ago, and employed Clive to help  modernise his flat. They got on well and quickly realised they had a  number of things in common: teamwork, attention to detail and the  importance of having fun!&nbsp; The flat led to a bigger flat, then a house,  another house&hellip;and so on. Each project involved bringing in new people,  and gradually a skilled and efficient team was formed. And critically,  on each project, people became increasingly aware of their role in the  team &ndash; Dan was Project Manager and Clive led the team day to day.&nbsp;  Build Team&rsquo;s values emerged &ndash; and these remain to the present day -  trust, teamwork and talent.</p>
       <p>For Dan, construction was still a  hobby rather than a living &ndash; yet the bigger the projects, the more  involved he became. Then in 2007, Dan decided to switch his career in  corporate banking for running Build Team full time. He had a clear  vision: to combine expert project management with talented tradesmen.</p>
      <p>Today,  while the team has grown, our values firmly remain. We focus on  maintenance and refurbishment contracts in Central London &ndash; and place a  strong emphasis on combining expert project management with talented  trades people.</p>
      <p>We want to protect the environment in which we  work, and that&rsquo;s why we have a clear sustainability policy covering how  we travel, source materials and dispose of waste.&nbsp; </p>
      <p>We care  about our customers and our team-members &ndash; and if you are one of  either, and like what we&rsquo;re about, please get in touch.</p>
		</div>
<?php

require_once('./inc/footer.inc.php');

?>