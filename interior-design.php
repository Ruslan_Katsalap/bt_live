<?php
require_once('./inc/header.inc.php');
?>
<link href="/css/horizontal.css" rel="stylesheet">
<link href="/css/examples.css" rel="stylesheet">
<link href="/css/ospb.css" rel="stylesheet">
<style>
.salesadded {
    width: 546px !important;
}
.main {
	padding:0px !important;
	}
.newmain {
	padding:170px 0px 0px 0px;
	
	}	
.newmaxwidth {
	max-width:980px;
	margin:0 auto;
	position:relative;
	}	
.newmaxwidth h3 {
	font-size: 30px;
    font-weight: bold;
    color: #FFF;
    padding-bottom: 50px;
    font-family: calibri !important;
	}	
.divadditionalbg {
	background:url(../images/additional_services/interior/interior_design.jpg) center;
	background-size:cover;
	}
.stepsul {
    list-style: none;
    display: table;
    width: 100%;
    text-align: center;
    margin: 0px;
    padding: 0px 0px;
	margin-top: 25px;
}
.stepsul li {
    color: #003c70;
    font-size: 18px;
    font-weight: normal;
    margin-left: 4px;
    margin-right: 4px;
    display: table-cell;
}
span.iconimage {
    display: block;
}	
span.forequality {
	display: block;
    min-width: 150px;
    margin: 0 auto;
    position: relative;
    bottom: -50px;
    font-weight: bold;
    color: #2f507a;
	}
	.pages li:after {
    content: "";
    position: absolute;
    height: 80px;
    width: 40px;
    background: transparent;
	opacity:0;
    left: -10px;
    bottom: 0;
}
.pages li {
    display: inline-block;
    width: 14px;
    height: 14px;
    margin: 0px 90px;
    text-indent:0px !important;
	overflow:inherit;
    border-radius: 10px;
    cursor: pointer;
	color:#FFF;
	position:relative;
    overflow: inherit !important;
    background: #fff;
    box-shadow: inset 0 0 0 1px rgba(0,0,0,.2);
}
.pages li.active {
    background: #75787f;
    color: #75787f;
}	
ul.pages:before {
    height: 3px;
    width: 100%;
    content: "";
    background: #c3d1dd;
    position: absolute;
    margin: 0 auto;
    margin-top: 6px;
    left: 0;
    /* margin: 0 auto; */
    right: 0;
	z-index:-1;
}
ul.pages {
    margin-top: 0px;
	margin-bottom: 50px;
}
span.iconimage img {
    max-height: 50px;
}
.oneperframe {
    height: auto;
    max-height: 550px;
    line-height: inherit;
}
.oneperframe ul li{
    background: none !important; 
}
.controls {
    position: absolute;
    width: 120%;
    top: 400px;
    left: -10%;
}
.controls .btn {
    background: none;
    border: none;
    box-shadow: none !important;
}
.controls img {
    max-width: 30px;
}
.controls .prev {
    left: 0px;
    position: absolute;
}
.controls .next {
    right: 0px;
    position: absolute;
}
.slidewidth_50 {
	float:left;
	width:49%;
	}
.slidewidth_50:first-child {
	margin-right:1%;
	}		
.bottomhalf:after {
	content:"";
	clear:both;
	}
.toptextslide p {
	text-align: justify;
    font-size: 20px;
    color: #959595;
    font-weight: normal;
    line-height: 22px;
	margin-bottom:50px;
	}
.sliderhalftext h4 {
	color:#2f507a;
	font-size:22px;
	}
.sliderhalftext {
	text-align:left;
	}	
.sliderhalftext p {
	color:#959595;
	font-size:18px;
	}	
.sliderhalfimg img {
	max-width:100%;
	}	
.oneperframe ul li {
	max-width:980px;
	}
.boltimgdesk {
position:absolute;
right:0px;
}	
.boltimgdesk img {
    max-width: 70px;
}
.boltimgdesk {
    position: absolute;
    right: 0px;
    top: -25px;
}
@media (min-width:640px) {
	.boltimgmob {
display:none;
}	
	}

@media (max-width:640px) {
	.newmain {
		padding:100px 0px 0px 0px;
	}
	span.iconimage img {
		max-height: 50px;
		max-width: 40px;
	}
	.divadditionalbg {
		background:none;
		}
	.newmaxwidth h3 {
		color:#2f507a;
		font-size:20px;
		padding-bottom: 20px;
		}
	.newmaxwidth {
		padding:0px 15px;
		}
	span.forequality {
		 min-width: 60px;
    	 font-size: 12px;
		}
	ul.pages {
		margin-bottom:50px;
		}
	.stepsul {
		margin-top:50px;
		}		
	.pages li {
		margin:0px 22px !important;
		}	
	.oneperframe ul li {
		max-width:300px;
		}
	.toptextslide p {
		padding: 0px 0px;
		font-size: 16px;
		color: #959595;
		font-weight: normal;
		line-height: 20px;
		margin-bottom:0px;
		}
	.sliderhalftext h4 {
		font-size:14px;
		}
	.sliderhalftext p {
		font-size:12px;
		padding-top: 10px;
		}
	.bottomhalf {
		max-width: 250px;
		margin: 0 auto;
	}	
	.controls {
		position: absolute;
		width: 100%;
		margin-top: -60px;
		left: 0%;
		top: inherit !important;
	}	
	.controls img {
    max-width: 10px;
}	
.formainswap {
	display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-orient: vertical;
    -moz-box-orient: vertical;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
    /* optional */
    -webkit-box-align: start;
    -moz-box-align: start;
    -ms-flex-align: start;
    -webkit-align-items: flex-start;
    align-items: flex-start;
	}
	.formainswap .formobileswap {
    -webkit-box-ordinal-group: 2;
    -moz-box-ordinal-group: 2;
    -ms-flex-order: 2;
    -webkit-order: 2;
    order: 2;
  }

  .formainswap .toptextslide {
    -webkit-box-ordinal-group: 1;
    -moz-box-ordinal-group: 1;
    -ms-flex-order: 1;
    -webkit-order: 1;
    order: 1;
  }	
  .formobileswap {
    margin-bottom: 30px;
}
.boltimgdesk {
display:none;
}		

.boltimgmob img {
    max-width: 70px;
}
.boltimgmob {
    position: relative;
    right: 0px;
    bottom: -30px;
    text-align: right;
}			
	}					
</style>
</div>

<div class="divadditionalbg">
	<div class="newmain">
    	<div class="newmaxwidth">
    		<h3>Interior Design</h3>
            <div class="boltimgdesk">	
            	<img src="/images/additional_services/interior/bolt_interior.png" alt="">
            </div>
        </div>
       </div> 
       </div>
       
       <div class="newmaxwidth">
       <div class="formainswap">
        <div class="formobileswap">
        <ul class="stepsul">
          <li><span class="iconimage"><img src="/images/additional_services/interior/completed.jpg" alt=""></span><span class="forequality">Site Visit</span></li>
          <li><span class="iconimage"><img src="/images/additional_services/interior/concept.jpg" alt=""></span><span class="forequality">Concept Design</span></li>
          <li><span class="iconimage"><img src="/images/additional_services/interior/detailed_concept.jpg" alt=""></span><span class="forequality">Detailed Design</span></li>
          <li><span class="iconimage"><img src="/images/additional_services/interior/decoration.jpg" alt=""></span><span class="forequality">Decoration</span></li>
          <li><span class="iconimage"><img src="/images/additional_services/interior/site_visit.jpg" alt=""></span><span class="forequality">Completed Works</span></li>
          
          <!--<li>2018</li>-->
        </ul>
        <ul class="pages">
              <li class="active"><span></span></li>
              <li class=""></li>
              <li class=""></li>
              <li class=""></li>
              <li class=""></li>
         </ul>
         </div>
         <div class="toptextslide">
              	<p>To ensure you make the most out of your final design, we encourage all of our clients to think about the finishing touches at the very beginning. We feel the interior of your extension is important to help shape your ‘shell’ design. Our Interior Design Service offers guidance on materials, features such as internal windows, kitchen spacing, lighting and more. We help you visualise your extension with 3D Visuals of your proposed design, which encourages you to think about the space and how it will interact with your furnishings.</p>
              </div>
              </div>
         <div class="frame oneperframe design-process" id="oneperframe">
         
          <ul class="clearfix">
            <li class="active">
              
              <div class="bottomhalf">
              	<div class="slidewidth_50">
                	<div class="sliderhalfimg">
                    	<img src="/images/additional_services/interior/site_sider.jpg" alt="">
                    </div>
                </div>
              	<div class="slidewidth_50">
                	<div class="sliderhalftext">
                    	<h4>Site Survey</h4>
                        <p>Site surveys give us an opportunity to survey your outside space...</p>
                    </div>
                </div>
              </div>
            </li>
            <li>
            	
              <div class="bottomhalf">
              	<div class="slidewidth_50">
                	<div class="sliderhalfimg">
                    	<img src="/images/additional_services/interior/site_sider.jpg" alt="">
                    </div>
                </div>
              	<div class="slidewidth_50">
                	<div class="sliderhalftext">
                    	<h4>Concept Design</h4>
                        <p>We work with you to find a style for your space with moodboards and sketches</p>
                    </div>
                </div>
              </div>
            </li>
            <li>
            	
              <div class="bottomhalf">
              	<div class="slidewidth_50">
                	<div class="sliderhalfimg">
                    	<img src="/images/additional_services/interior/site_sider.jpg" alt="">
                    </div>
                </div>
              	<div class="slidewidth_50">
                	<div class="sliderhalftext">
                    	<h4>Detailed Design</h4>
                        <p>Happy with your loobook? We’ll work on detailed plans and marked up renders to bring the design to life.</p>
                    </div>
                </div>
              </div>
            </li>
            <li>
            	
              <div class="bottomhalf">
              	<div class="slidewidth_50">
                	<div class="sliderhalfimg">
                    	<img src="/images/additional_services/interior/site_sider.jpg" alt="">
                    </div>
                </div>
              	<div class="slidewidth_50">
                	<div class="sliderhalftext">
                    	<h4>Site Survey</h4>
                        <p>Site surveys give us an opportunity to survey your outside space...</p>
                    </div>
                </div>
              </div>
            </li>
            <li>
            	
              <div class="bottomhalf">
              	<div class="slidewidth_50">
                	<div class="sliderhalfimg">
                    	<img src="/images/additional_services/interior/site_sider.jpg" alt="">
                    </div>
                </div>
              	<div class="slidewidth_50">
                	<div class="sliderhalftext">
                    	<h4>Completed Packet</h4>
                        <p>Once you are completely satisfied, we will prepare your final 3D renders and drawings for Suppliers.</p>
                    </div>
                </div>
              </div>
            </li>
            
            <!--<li>
            	<div class="lineyear"><span>2018</span></div>
            	<h4></h4>
            	<p></p>
            </li>-->
          </ul>
        </div>
        <div class="controls center">
          <button class="btn prev" ><img src="/images/story/prev_btn.jpg"></button>
          <button class="btn next"><img src="/images/story/next_btn.jpg"></button>
        </div>
        <div class="boltimgmob">	
            	<img src="/images/additional_services/interior/interior_bolt_mob.png" alt="">
            </div>
        <!--<div class="scrollbar">
          <div class="handle">
            <div class="mousearea"><img src="/images/design-pocess/house-drag.png"></div>
          </div>
        </div>-->
        </div>
        
      </div>
      
    </div>
  </div>
 
</div>

</div>
</div>
    </div>
</div>
<script type="text/javascript" src="/js/plugins.js"></script>
    <script type="text/javascript" src="/js/horizontal.js"></script>
    <script type="text/javascript" src="/js/sly.min.js"></script> 
<?php require_once('./inc/footer.inc.php'); ?>