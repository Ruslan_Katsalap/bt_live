<?php 
require_once('./inc/header.inc.php');
?>

<style type="text/css">
	.check {
		margin-top: 20px;
	}

	.check li {
		margin-bottom: 15px;
		font-weight: bold;
	}

	.padding-right {
		padding-right: 25px;
	}

	.darkblue-box, .lightblue-box {
		margin-right: 6px;
		width: 100px;
	}
	ul.question li {
    background: url(/images/question.png) no-repeat !important;
    /* text-indent: 25px; */
    padding-left: 25px;
}
ul.question {
	list-style:none;
	}
</style>

<div class="full">
	<h1>Pre-purchase feasibility</h1>

	<div class="col_one_third">
		<div class="padding-right">
		<p style="text-align:justify">We have designed this service specifically for clients looking to purchase a property which has the potential to build a side return extension. We will visit the property with you and advise on the feasibility of obtaining Planning Permission, advise on design and internal layout configurations for permitted development extensions and provide you with an assessment of the side return costs. If the property also has potential for a London loft conversion (mansard loft conversion, basement conversion, garage conversion), we can also advise on that too.</p>

		<p style="font-weight: bold;">Typical questions of clients who come to us at this stage are:</p>
		<ul class="check question">
			<li>How much will the extension / loft conversion cost?</li>
			<li>Will I get planning permission?</li>
			<li>Can the works be executed under Permitted Development Rights?</li>
			<li>What is the best internal configuration to maximise the liveable space of the house building?</li>
		</ul>
		</div>
	</div>
	<div class="col_one_third" style="font-weight:bold">
    <p>Our Pre-Purchase Feasibility Service includes:</p>
    <ul class="check">
      <li>Fasttrack service – we realise time is of the essence when you are considering making an offer or are about to exchange and Build Team will do its best to schedule an appointment within 24 hours of your enquiry</li>
      <li>A visit to the property to advise on the cost and feasibility of the side extension works</li>
      <li>Detailed Planning Advice and Guidance from us, a professional construction company in London</li>
      <li>Creative advice on design and internal layout options (kitchen extension ideas, loft conversion ideas, loft room ideas)</li>
      <li>An on-site overview of extension costs</li>
      <li>A budget estimate for the works on any permitted development extensions sent within 24 hours of the meeting</li>
    </ul>

    <p class="fee-orange">Fee of just £150 + VAT</p>

    <p class="links" ><a class="book-link" href="/book-visit.html?type=prepurchase">book now</a></p>
    
  </div>
  <div class="col_one_third col_last">
    <p><img src="/images/pre-purchase.jpg" style="width:100%;max-width:404px" /></p>
  </div>

  <div class="col_full">

		<div class="boxes" style="float:right">
			<div class="darkblue-box">find out<br><span class="orange">more</span></div>
			<div class="lightblue-box"><a href="/what-we-do/guarantees-and-insurance-page.html">10 year<br><span class="orange">guarantee</span></a></div>
			<div class="lightblue-box"><a href="/about-us/guarantees-and-insurance-page.html"><span class="orange">completion</span><br>pack</a></div>
			<div class="lightblue-box"><a href="/build-your-price-start.html">build your<br><span class="orange">price</span></a></div>
			<div class="lightblue-box" style="margin-right: 0;"><a href="/what-we-do/process.html">the <span class="orange">8 step</span><br>process</a></div>
		</div>
	</div>

</div>

<?php /* message pop custom, disabled per now:
<div class="messagepop">
  <h3>Please wait...</h3>
  <p>you are now being redirected to our sister<br /> company Design Team to complete your<br /> booking</p>
  
  <p class="logos">
    <img class="logo-1" src="/images/logo_newer.png" />
    <img class="loading" src="/images/loading.gif" />
    <img class="logo-2" src="/images/designteam_logo1.png" />
  </p>
</div>

<div class="overlay"></div>

<style>
.overlay {
    position:fixed;
    display:none; 

    /* color with alpha channel *
    background-color: rgba(0, 0, 0, 0.7); /* 0.7 = 70% opacity *

    /* stretch to screen edges *
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
}

.messagepop {
  background-color:#FFFFFF;
  border:15px solid #263c76;
  cursor:default;
  display:none;
  margin-top: 15px;
  position:absolute;
  text-align:left;
  width:594px;
  z-index:50;
  padding: 25px 25px 20px;
}
p.logos img{
  display: inline-block;
}
.logo-1{
  width:200px;
}
.logo-2{
  width:250px;
}
.loading{
  margin:0 50px 50px 50px;
}

.messagepop label {
  display: block;
  margin-bottom: 3px;
  padding-left: 15px;
  text-indent: -15px;
}
.messagepop h3{
  font-size:19px;
  font-weight: normal;
  text-align: center;
}
.messagepop p{
  font-size:20px;
  font-weight: normal;
  text-align: center;
  border:none;
}

.messagepop p, .messagepop.div {
  border-bottom: 1px solid #EFEFEF;
  margin: 8px 0;
  padding-bottom: 8px;
}
</style>

<script>
  var redirect_link = "";
  $(document).ready(function(){
    $("a.book-link").click(function(){
      $(".messagepop").center();
      redirect_link = $(this).attr("href");
      $(".messagepop").show();
      $(".overlay").toggle();
      setTimeout(function() {
        window.location = redirect_link;
      }, 5000);
      return false;
    });
    $(".messagepop").center();
  });
  
  jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", ( jQuery(window).height() - this.height() ) / 2+jQuery(window).scrollTop() + 0 + "px");
    this.css("left", ( jQuery(window).width() - this.width() ) / 2+jQuery(window).scrollLeft() + "px");
    return this;
  }
</script>
*/
?>

<?php
  require_once('./inc/footer.inc.php');
?>