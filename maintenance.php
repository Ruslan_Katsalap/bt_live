<?php

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    	
			<?php echo $bc_trail; ?>
      
    	<h1>Maintenance</h1>
      <p>Build Team’s maintenance division services the needs of residential and commercial clients on both a large and small scale. This side of the business has grown rapidly since its launch, based on servicing the repeat needs of our small but expanding client base.</p>
      <p>Our contracts now include, a luxury high-rise development in Westminster consisting of 84 apartments, high street chain stores such as Petit Bateau and Pretty Pregnant, the head office of Streetcar, and maintaining luxury homes in some of London’s most prestigious areas.</p>
      <p>Our maintenance team are experts in their field and fully trained in first aid and customer service.</p>
      <p>Our maintenance engineers are trained to work in high traffic office and retail environments and they understand that the needs of customers always come first.</p>
      <p>If you would like to find out how our maintenance division could assist you or your business, please get in touch to book an appointment for a Maintenance Manager to visit, or email us on <a href="/contact.html">contact page</a>.</p>
      
      <div class="logo-sm" style="width:180px;padding-top:18px;"><img src="/images/logo-streetcar.png" width="147" height="53" alt="streetcar" /></div>
      <div class="logo-sm" style="width:280px;padding-top:30px;"><img src="/images/logo-pretty.png" width="227" height="28" alt="petty pregnant" /></div>
      <div class="logo-sm" style="width:130px;"><img src="/images/logo-petit.png" width="92" height="88" alt="petit bateau" /></div>
      <div class="clear"></div>

		</div>
    <div class="right">
    	<img src="/images/mainten.gif" width="326" height="436" alt="BuildTeam" />
    </div>
<?php

require_once('./inc/footer.inc.php');

?>