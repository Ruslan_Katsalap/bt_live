<?php

require_once('./inc/header.inc.php');

$rs = getRs("SELECT map_postcodes, map_filename FROM setting WHERE setting_id = 1");
$row = mysqli_fetch_assoc($rs);

$row['map_postcodes'] = str_replace(',', ', ', $row['map_postcodes']);

?>
<style>
.coverinfo {
	border-bottom:5px solid #c3d1dd;
	padding-bottom:20px;
	}

.coverinfo p {
	font-size:18px;
	color:#969696;
	padding-top: 0px;
	text-align:justify;
	}
.commingmap {
	height:300px;
	width:100%;
	border-bottom:5px solid #c3d1dd;
	}
.checkifwecover {
	background:#f3f4f8;
	max-width:300px;
	padding:20px;
	margin-top:20px;
	}	
.checkifwecover p {
    padding-top: 0px;
    font-size: 18px;
    margin-bottom: 10px;
	color:#0e4777;
	font-weight:normal;
}
.wecoverinner {
    padding: 0px 20px;
}
.wecoverinner input[type=text] {
    padding: 0px 0px;
    margin-bottom: 10px;
    text-indent: 5px;
    height: 30px;
	border:none;
}
.wecoverinner input[type=button] {
    background: #0e4777;
    border: none;
    max-width: 70px;
    color: #FFF;
    height: 25px;
	
}
.wecoverinner input {
    width: 100%;
    padding: 0px;
    height: 20px;
}	
</style>
  	<div class="col_full">
    	<?php
			
			//echo $bc_trail;
			
			?>
    	
        <h1>Areas We Cover</h1>
      <div class="col_full">
      		<div class="coverinfo">
      		<p>Looking to build a side return extension in one of the following London areas? We are the right construction company in London to provide you with a full design and build solution at any of the following postcodes across London. Check out this page to find out if we service your area and feel free to also browse our website for more useful information. You can find out an approximate cost for your construction plans using our online calculator, read more about what we can offer in terms of side extensions, kitchen design ideas, basement conversion plans, loft conversion plans, mansard loft conversion ideas and check out our database for examples of previous projects.</p>
            
            </div>
      </div>
      <div class="col_full">
      	<div class="commingmap"></div>
            <div class="checkifwecover">
                <div class="wecoverinner">
                	<p>Check if we cover your area</p>
                    <input type="text" placeholder="Enter your postcode and hit the find button">
                    <input type="button" value="Find">
                </div>
            </div>
        <!--<img src="/images/<?php echo $row['map_filename']; ?>" alt="London areas Build Team cover: <?php echo $row['map_postcodes']; ?>" title="London areas Build Team cover: <?php echo $row['map_postcodes']; ?>" style="width:100%;max-width:690px" />-->
      </div>
      
      <!--<div class="col_one_fourth col_last">
		  
        <p style="text-align:justify">Looking to build a side return extension in one of the following London areas? We are the right construction company in London to provide you with a full design and build solution at any of the following postcodes across London.</p>
        <p style="text-align:justify">Check out this page to find out if we service your area and feel free to also browse our website for more useful information. You can find out an approximate cost for your construction plans using our online calculator, read more about what we can offer in terms of side extensions, kitchen design ideas, basement conversion plans, loft conversion plans, mansard loft conversion ideas and check out our database for examples of previous projects. </p>
        <p style="text-align:justify">We believe we are the right Build Team to design and build your side return extension.</p>
		  
        <p>Legend:</p>
		  
        <table>
        <tr><td style="padding: 5px 0;">
        <div style="width: 25px; height: 15px; background: #808385; border: 1px solid #808385; margin-right: 10px;">&nbsp;</div>
        </td><td>Full design & build service</td></tr>
        <tr><td style="padding: 5px 0;"><div style="width: 25px; height: 15px; background: #fff; border: 1px solid #808385;margin-right: 10px;">&nbsp;</div></td>
        <td>Please contact us for availablility</td>
        </tr>
        <tr><td style="padding-left:7px;padding-top:5px"><img src="/images/map_point2.gif" width="15" height="15" alt="Build Team HQ"></td><td>Build Team HQ <!--(SW4 6DH)-->
        <!--</td></tr></table>-->
      
     <!--   <p>If your area is not covered, please <a href="/contact.html" target="_blank">click here</a>
	to send us your details as we may still be
	able to offer you our integrated Design &amp;
	Structural package for Side Returns</p>
      
      </div>-->
      
      <!--<br clear="all" /><br/><br/><br/>
      We offer the full Design and Build service for Side Return Extensions in the following postcodes across London: <?php echo $row['map_postcodes']; ?>  
      <br/><br/>
      If you area is not covered, please <a href="/contact.html">click here</a> to send us your details as we may still be able to offer you our integrated Design & Structural package for Side Returns-->
		</div>
		
		<!--<div class="clear">
        <div style="margin-top: 30px; float: left;">
	<div class="darkblue-box" style="margin-right: 10px;">
		find out<br>
		<span class="orange">more</span></div>
	<div class="lightblue-box" style="margin-right: 10px;">
		<a href="/what-we-do/guarantees-and-insurance-page.html">10 year<br>
		<span class="orange">guarantee</span></a></div>		
		<div class="lightblue-box" style="margin-right: 10px;"><a href="/about-us/guarantees-and-insurance-page.html">
		completion<br><span class="orange">pack</span></a></div>
		
		
	<div class="lightblue-box" style="margin-right: 10px;"><a href="/build-your-price-start.html">
		build your<br>
		<span class="orange">price</span></a></div>
			<div class="lightblue-box"><a href="/what-we-do/process.html">
		the <span class="orange">8 step</span><br>
		process</a></div>
	</div>
</div>-->
<?php

require_once('./inc/footer.inc.php');

?>