<?php


header('HTTP/1.1 301 Moved Permanently');
header('Location: /about-us/guarantees-and-insurance-page.html');
exit;




require_once('./inc/header.inc.php');







?>



  	<div class="full">



    	<?php



			



			echo $bc_trail;



			



			?>



    	<h1>Guarantees &amp; Insurance</h1>



      <p>For standard projects, Build Team will guarantee the works for a period of twelve months from the date of practical completion. Where a JCT or other contractual arrangements exist, Build Team will adhere to the defects liability period as set out in the contract. You can view a copy of our standard <a href="/business-terms-and-conditions.html" title="Terms &amp; Conditions">Terms &amp; Conditions</a>.</p>



      <p>Build Team are fully insured with £5m Public Liability and £10m Employers Liability cover.</p>



      <p>On completion of Design and Build projects, Build Team will provide an operating and maintenance manual for the works containing drawings, copies of the Local Authority building regulations consent, instruction booklets and relevant guarantees.</p>



      <p>We carry the very highest levels of Insurance, please <a href="/pdf/2012_0201_BTH_insurance_schedule.pdf" target="_blank" title="click here to view our insurance policy">click here to view our insurance policy</a>.</p>



		</div>



<?php







require_once('./inc/footer.inc.php');







?>