<?php $page_name = "dc_booking"; ?>
<?php require_once('./inc/header.inc.php'); ?>

<?php 
//HolidayAPI

//fbc8dba5-a831-4284-a9b7-015c96085e33



?>
<!-- DC booking page -->
<div class="book_wrapper">
		<section class="consult">
			<div class="center">
				<h4 class="ttl">Book a FREE Design Consultation</h4>
				<p class="desc">Get started today by booking a free Design Consultation with one of our 
				Architectural Designers to discuss your project and find out more.</p>
				<div class="book_box">
					<div class="book_tabs">
						<div class="book_tab book_tab_active book_tab_1">
							<div class="tab_num">
								1
							</div>
							<div class="tab_ttl">
								Choose a Date
							</div>
						</div>
						<div class="book_tab book_tab_2">
							<div class="tab_num">
								2
							</div>
							<div class="tab_ttl">
								Your Details
							</div>
						</div>
						<div class="book_tab book_tab_3">
							<div class="tab_num">
								3
							</div>
							<div class="tab_ttl">
								Confirmation
							</div>
						</div>
					</div>
					<div class="book_1_content" style="display: block;">
						<div class="book_1_datapick">
							<button class="datepick_btn prev_day_btn">
								<img src="assets_b/img/datepick_left_arrow.png" alt="">
							</button>
							<div class="d_picker_wrap">
								<input type="text" id="datepicker" name="_date" disabled />
							</div>
							<button class="datepick_btn nex_day_btn">
								<img src="assets_b/img/datepick_right_arrow.png" alt="">
							</button>
						</div>
						<div class="book_1_time">
							<div class="book_1_el">
								<div class="book_1_el_content">
									<div class="book_time_l">
										Morning
									</div>
									<div class="book_time_r">										
										<div class="book_time_row__item book_slot_1">
											<!-- slot 1 -->
										</div>
									</div>
								</div>
							</div>
							<div class="book_1_el">
								<div class="book_1_el_content">
									<div class="book_time_l">
										Afternoon
									</div>
									<div class="book_time_r">										
										<div class="book_time_row__item book_slot_2">
											<!-- slot 2 -->
										</div>
									</div>
								</div>
							</div>
							<div class="book_1_el">
								<div class="book_1_el_content">
									<div class="book_time_l">
										Evening
									</div>
									<div class="book_time_r">										
										<div class="book_time_row__item book_slot_3">
											<!-- slot 3 -->
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="book_err book_slot_err">
							Please select a time slot
						</div>
						<div class="book_control_btns">
							<button class="control_btn control_back_btn control_back_inactive">Back</button>
							<button type="button" class="control_btn control_orange control_next_btn">Next</button>
						</div>
					</div>
					<div class="book_2_content" style="display: none;">
						<form class="book_form">
							<div class="book_form_contact_wrap">
								<div class="book_form_contact book_form_sec">
									<h4 class="book_form_ttl">CONTACT DETAILS</h4>
									<div class="book_form_row">
										<fieldset>
											<label for="book_f_name">First Name<span>*</span></label>
											<input id="book_f_name" type="text" name="f_name" required="" />
										</fieldset>
										<fieldset>
											<label for="book_surname">Surname<span>*</span></label>
											<input id="book_surname" type="text" name="surname" required="" />
										</fieldset>
									</div>
									<div class="book_form_row">
										<fieldset>
											<label for="book_email">Email Address<span>*</span></label>
											<input id="book_email" type="email" name="email" required="" />
										</fieldset>
										<fieldset>
											<label for="book_phone">Telephone<span>*</span></label>
											<input id="book_phone" type="text" name="phone" required="" />
										</fieldset>
									</div>
									<div class="book_form_row book_form_row_address">
										<fieldset>
											<label for="book_address">Address<span>*</span></label>
											<input id="book_address" type="text" name="address" required=""/>
										</fieldset>
										<fieldset class="postcode_sec">
											<label for="book_postcode">Postcode<span>*</span></label>
											<input id="book_postcode" type="text" name="postcode" required="" />
										</fieldset>
									</div>
									<div class="how_contact">
										<div class="form_fld_ttl">How would you like to be contacted? </div>
										<div class="how_contact_type">
											<div class="how_contact_el">
												<label class="rad_container">Video Call
												  <input type="radio" name="contact_type" checked="checked" value="video call">
												  <span class="r_checkmark"></span>
												</label>
											</div>
											<div class="how_contact_el">
												<label class="rad_container">Phone Call
												  <input type="radio" name="contact_type" value="phone call">
												  <span class="r_checkmark"></span>
												</label>
											</div>
										</div>
									</div>
									
								</div>
							</div>
							<div class="book_form_project book_form_sec">
								<h4 class="book_form_ttl">PROJECT DETAILS</h4>
								<div class="book_form_row">
									<select name="start" class="start_select" required="">
										<option selected disabled>When are you looking to start?*</option>
										<option value="Immediately">Immediately</option>
										<option value="1-3 months">1-3 Months</option>
										<option value="3-6 Months">3-6 Months</option>
										<option value="1 Year">1 Year</option>
									</select>
									<select name="status" required="" class="status_select">
										<option selected disabled>Please describe your current status*</option>
										<option value="Pre-Purchase">Pre-Purchase</option>
										<option value="Just Moved In">Just Moved In</option>
										<option value="Resident For Less Than 1 Year">Resident For Less Than 1 Year</option>
										<option value="Resident For More Than 1 Year">Resident For More Than 1 Year</option>
									</select>
								</div>
								<div class="exten_type">
									<div class="form_fld_ttl">Which type of extension are looking to do?</div>
									<div class="exten_list">
										<span class="exten_item" data-val="Ground Floor">
											Ground Floor
										</span>
										<span class="exten_item" data-val="Loft">
											Loft
										</span>
										<span class="exten_item" data-val="Ground Floor + Loft">
											Ground Floor + Loft
										</span>
										<span class="exten_item" data-val="Other">
											Other
										</span>
									</div>
								</div>
								<div class="book_form_row">
									<fieldset>
										<label for="book_link">Rightmove Link</label>
										<input id="book_link" type="text" name="link" />
									</fieldset>
									<fieldset class="postcode_sec">
										<div class="book_file">
											<label for="book_file">Upload Floor Plans (if available)</label>
											<div class="book_file_fld">
												<input id="book_file" type="file" name="file"  />
												<span class="book_filename"></span>
											</div>
										</div>
									</fieldset>
								</div>
								<div class="book_check">
									<div class="book_check_terms">
										<label class="check_container">
										  <input type="checkbox" name="terms" checked>
										  <span class="checkmark"></span>
										  I am accept the <a href="#" target="_blank">terms and conditions</a>
										</label>
									</div>
									<div class="book_check_events">
										<label class="check_container">
										  <input type="checkbox" name="events">
										  <span class="checkmark"></span>
										  Keep me updated on Build Team news, events and offers
										</label>
									</div>
								</div>
							</div>
							<div class="book_err book_exten_err" style="display: none;">
								Please select a type of extension
							</div>
							<div class="book_control_btns">
								<button class="control_btn ctrl_back_btn control_back_btn">Back</button>
								<button type="submit" class="control_btn control_orange book_confirm_btn">Confirm Booking</button>
							</div>
						</form>
					</div>
					<div class="book_3_content" style="display: none;">
						<div class="book_3_mg">
							<h4>THANK YOU FOR YOUR BOOKING!</h4>
							<p class="book_3_text">
								You will shortly receive an email, 
								outlining all the details of your booking.

							</p>
							<div class="book_line"></div>
							<p class="book_3_text">We look forward to discussing your plans 
							in more depth!</p>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="expect">
			<div class="center">
				<h4 class="ttl">What To Expect</h4>
				<p class="desc">With each consultation, you will get to talk with one member of our Design Team for 30mins to discuss your project and any other queries you may have about future plans.</p>
				<div class="expect_list_box">
					<ul class="expect_list">
						<li>
							<div class="expect_icob_wrap">
								<img src="assets_b/img/expect_icon/budget.png" alt="">
							</div>
							<span class="expect_name">Estimate Budget Prediction</span>
						</li>
						<li>
							<div class="expect_icob_wrap">
								<img class="timeline_icon" src="assets_b/img/expect_icon/timeline.png" alt="">
							</div>
							<span class="expect_name">Project Timeline Overview</span>
						</li>
						<li>
							<div class="expect_icob_wrap">
								<img src="assets_b/img/expect_icon/design.png" alt="">
							</div>
							<span class="expect_name">Discuss Design Ideas</span>
						</li>
						<li>
							<div class="expect_icob_wrap">
								<img src="assets_b/img/expect_icon/service.png" alt="">
							</div>
							<span class="expect_name">Additional Services: Party Wall Services</span>
						</li>
					</ul>
				</div>
			</div>
		</section>
	</div>

<!-- footer -->
<?php require_once('explore-our-site.php'); ?>
<?php require_once('./inc/footer.inc.php'); ?>


<script>
	//get range
	var d_max_range = 30
	$.ajax({
	type: "GET",
	url: "/api/dc_get_range.php",
	async: false,
	success: function(data){
		 var data = JSON.parse(data)
		// if(data.result){
			d_max_range = parseInt(data["range"]);
			console.log("max_range",d_max_range)
		// } else {
			
		 }
		
	})

</script>


	<!-- jquery UI js-->
<script src="/assets_b/js/datapicker_ui.js"></script>
<script>



	$( function() {

	  	

		    $( "#datepicker" ).datepicker({
		    	dateFormat: "D, dd M yy",
		    	beforeShowDay: $.datepicker.noWeekends,
		    	minDate:0,
		    	maxDate: d_max_range,
		    	// beforeShow: function(){
		    	// 	console.log("here")
			    // 		var date = $('#datepicker').datepicker('getDate');
					  //   var dayOfWeek = date.getUTCDay();
					  //   var date_for_hol =  date.getFullYear()+"-"+(date.getMonth()+1) + "-"+date.getDate();

					  //   if(dayOfWeek == 5 || dayOfWeek == 6 || (book_holidays.indexOf(date_for_hol) != -1) ){
					  //   	//date.setTime(date.getTime() + (1000*60*60*24))
					  //   	//$('#datepicker').datepicker("setDate", date);
					  //   	$('.nex_day_btn').trigger("click")
					  //   }
		    	// }
		    }).datepicker("setDate", new Date());

		    // alert("test");
		    // $("body").html("")


	   


	  } );

	  $(function(){

	  	
		  	upload_slots()

		  	var dd_date = $('#datepicker').datepicker('getDate');
					    var dayOfWeek = dd_date.getUTCDay();
					    var date_for_hol =  dd_date.getFullYear()+"-"+(dd_date.getMonth()+1) + "-"+dd_date.getDate();

				if(dayOfWeek == 5 || dayOfWeek == 6 || (book_holidays.indexOf(date_for_hol) != -1) ){
			    	//date.setTime(date.getTime() + (1000*60*60*24))
			    	//$('#datepicker').datepicker("setDate", date);
			    	$('.nex_day_btn').trigger("click")
				}
		 		
	  })
</script>	

