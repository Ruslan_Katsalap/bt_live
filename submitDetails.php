<?php

// sleep(3); //debug

require_once ('./inc/util.inc.php');

$xhr = (isset($_SERVER['HTTP_X_REQUESTED_WITH'])) && ($_SERVER['HTTP_X_REQUESTED_WITH'] == 'XMLHttpRequest');
if (!$xhr) echo '<textarea>';
 
//debug
$debug = false;
if ($debug) {
  $fp = fopen('report.txt', 'a');
  fwrite($fp, print_r($_POST,1)."\n\n".print_r($_SESSION,1));
}

// main content of response here 

$qid = isset($_POST['qid'])?(int)$_POST['qid']:0;

$userid = 0;
$email = false;

if (isset($_POST['userid'])) $userid=(int)$_POST['userid'];

if ($userid && isset($_SESSION['userid']) && $userid==$_SESSION['userid']) {
  // new BYP client
  // logged in user submits request to save or email his quote
  
  if (isset($_POST['quote_type_id']) && 'email'==$_POST['quote_type_id']) {
    $email = true;
    $user_status = 1; // emailed
  }
  else $user_status = -1; // just save quote
  
  if ($qid) {
    $sql = "UPDATE rfq SET is_email_quote = $user_status, date_modified = '" . time() . "'";
    if (1==$user_status) $sql .= ", rfq_status_id = 2";
    $sql .= " WHERE rfq_id = '{$qid}' AND userid = '".$userid."'";
    
    setRs($sql);
  }
  
}
else {
  // $quote_type_id = 2 means DESIGN & BUILD SERVICE (ID from `quote_type` table)
  $quote_type_id = isset($_POST['quote_type_id'])?(int)$_POST['quote_type_id']:0;
  $name = isset($_POST['name'])?trim($_POST['name']):'';
  $address = isset($_POST['address'])?trim($_POST['address']):'';
  $phone = isset($_POST['phone'])?trim($_POST['phone']):'';
  $email = isset($_POST['email'])?trim($_POST['email']):'';
  $message = isset($_POST['message'])?trim($_POST['message']):'';
  
  if ($qid) setRs("UPDATE rfq SET name = '" . formatSql($name) . "', address = '" . formatSql($address) . "', phone = '" . formatSql($phone) . "', email = '" . formatSql($email) . "', message = '" . formatSql($message) . "', quote_type_id = '{$quote_type_id}', rfq_status_id = 2, date_modified = '" . time() . "' WHERE rfq_id = '{$qid}' AND ipaddress = '".formatSql($IP)."'");
  
}

if ($qid) {
  
  // email quote if needed
  if (!$userid || $email)  {
    //require_once ('sendmail.php');//old method HTML quote
    
    $_POST['id'] = $qid;
    $sql = "SELECT rfq_code FROM rfq WHERE rfq_id=$qid";
    $rs = getRs($sql);
    $row = mysqli_fetch_assoc($rs);
    $_POST['code'] = $row['rfq_code'];
    // new PDF quote
    ob_start();
    include_once 'quote-email.php';
    ob_end_clean();
  }
  
  // process uploaded image
  $error = '';
  
  if ($_FILES) {
    $i = 0;
    
    $a_mime = array("image/jpeg", "image/jpg", "image/jpe_", "image/pjpeg", "image/vnd.swiftview-jpeg", "image/x-citrix-pjpeg", "image/jp_", "application/jpg", "application/x-jpg", "image/pjpeg", "image/pipeg", "image/vnd.swiftview-jpeg", "image/x-xbitmap", "image/x-citrix-pjpeg",
    
    "image/png", "application/png", "application/x-png",
    
    "image/gif", "image/x-xbitmap", "image/gi_"
    );
    
    foreach ($_FILES AS $v) {
      // If empty box in IE
      if (4==$v['error'] && !$v['name']) continue;
      
      if ($v['error']) {
        $error .= 'File '.$v['name'].' is not uploaded ';
        if (1==$v['error'] || 2==$v['error']) $error .= '(size too big) ';
        else $error .= '('.$v['error'].') ';
      }
      else {
        if (!in_array($v['type'], $a_mime)) $error .= 'File '.$v['name'].' is not uploaded (wrong type) ';
        else {
          $ext = explode('.', $v['name']);
          $ext = strtolower(array_pop($ext));
          if ('jpg'!=$ext && 'jpeg'!=$ext && 'png'!=$ext && 'gif'!=$ext) {
            $error .= 'File '.$v['name'].' is not uploaded (wrong type) ';
          }
        }
      }
    }
    
    if (!$error) {
      $path = str_replace('\\', '/', dirname(__FILE__));
      @mkdir($path.'/images/rfq/'.$qid, 0777); 
      $sql = '';
      foreach ($_FILES AS $v) {
        if (!$v['name']) continue; // IE hot fix
        $filename = explode('.', $v['name']);
        $ext = strtolower(array_pop($filename));
        $filename = md5(uniqid(rand(),true));
        $j = 1;
        do {
          if ($j>1) $savename = $filename . '_' . $j . '.' .$ext;
          else $savename = $filename . '.' .$ext;
          $j++;
        } while (file_exists($path.'/images/rfq/'.$qid.'/'.$savename));
        if (!move_uploaded_file($v['tmp_name'], $path.'/images/rfq/'.$qid.'/'.$savename)) {
          $error = 'File '.$v['name'].' is not uploaded (server error) ';
          break;
        }
        $k = $i+1;
        $sql .= 'photo'.($k>1?$k:'')."='".$savename."',";
        $i++;
        if (5==$i) break;
      }
      
      if (!$error) {
        if ($sql)    
          setRs("UPDATE rfq SET " . substr($sql, 0, -1) . " WHERE rfq_id = '{$qid}' AND ipaddress = '".formatSql($IP)."'");
      }
    }
    
  }
  if (!$_POST && !$_FILES) $error = 'Uploaded files are too big';
  
  if (!$error) echo "status=success";
  else echo $error;
}

if (!$xhr) echo '</textarea>';

?>