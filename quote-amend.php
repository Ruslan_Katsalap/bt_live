<?php

require_once ('./inc/util.inc.php');

if (isset($_SESSION['userid']) && $_SESSION['userid']) {
  if (isset($_GET['id']) && ($_GET['id']=(int)$_GET['id'])) {
    
    $sql = "SELECT * FROM rfq WHERE rfq_id={$_GET['id']} AND userid=".(int)$_SESSION['userid'];
    $rs = getRs($sql);
    if (mysqli_num_rows($rs)) {
      
      $a_rfq = mysqli_fetch_assoc($rs);
      
      $_SESSION['quote_info'] = array(
        'qid' => $a_rfq['rfq_id'],
        'postcode' => $a_rfq['postcode'],
        'room_length' => $a_rfq['room_length'],
        'room_width' => $a_rfq['room_width'],
        'room_size' => $a_rfq['room_size'],
        'property_type' => $a_rfq['property_type'],
        'measure_unit_id' => $a_rfq['measure_unit_id'],
        'use_room_size_estimator' => $a_rfq['use_room_size_estimator'],
        'subcategory_1_component_id' => $a_rfq['subcategory_1_component_id'],
        'subcategory_2_component_id' => $a_rfq['subcategory_2_component_id'],
        'subcategory_3_component_id' => $a_rfq['subcategory_3_component_id'],
        'subcategory_4_component_id' => $a_rfq['subcategory_4_component_id'],
        'subcategory_5_component_id' => $a_rfq['subcategory_5_component_id'],
        'subcategory_6_component_id' => $a_rfq['subcategory_6_component_id'],
        'name' => $a_rfq['name'],
        'phone' => $a_rfq['phone'],
        'address' => $a_rfq['address'],
        'message' => $a_rfq['message'],
        'start' => $a_rfq['start'],
        'status' => $a_rfq['status']
      );
      
      header('Location: /build-your-price-app.html');
      //echo 'Found!)';#debug
      
      exit;
    }
  }
}

header('Location: /build-your-price-app.html');
exit;

?>