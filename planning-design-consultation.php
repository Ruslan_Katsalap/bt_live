<?php 
require_once('./inc/header.inc.php');
?>

<style type="text/css">
	.check {
		margin-top: 20px;
		padding-left: 17px;
	}

	.check li {
		margin-bottom: 15px;
		font-weight: bold;
	}

	.padding-right {
		padding-right: 25px;
	}

	.darkblue-box, .lightblue-box {
		margin-right: 6px;
		width: 96px;
	}
	.col_one_third, .col_two_third {
		font-size:16px;
		}
</style>

<div class="full">
	<h1>Premium site visit for planning and design consultation</h1>

	<div class="col_one_third">

		<p style="text-align:justify">A number of homeowners contact Build Team, a professional construction company in London for advice and guidance before embarking on their journey to improve their homes with a side return extension, a side return kitchen extension or a conversion of any type. In response to this need, we launched our Premium Site visit service to help you design your own house with affordable, modern house plans and permitted development extensions. During this visit, we advise clients on Planning regulations and Party Wall matters, along with creative house designs and side return ideas and how to make the most of the space you have available.</p>

		<p style="font-weight: bold;">Typical questions of clients who come to us at this stage are:</p>
		<ul class="check question">
			<li>I can’t visualise how the house extensions will look. Are you able to help?</li>
			<li>What are the local borough planning guidelines and will I obtain Planning Permission?</li>
			<li>Am I best to apply for Full Planning Permission, or could the works be executed under my Permitted Development Rights?</li>
			<li>What are the potential minefields of the Party Wall Process? How and when should I engage my neighbour and what happens if they dislike my proposals?</li>
			<li>I have a limited budget, but wish to spend it wisely. What are my options and what are the side return costs and implications?</li>
		</ul>

	</div>
	<div class="col_two_third col_last">
		<div class="col_half" style="font-weight: bold;">

			<p>Our Planning &amp; Design Consultation for Side Extensions includes:</p>
			<ul class="check">
				<li>A fast-tracked appointment with the option of early morning and late evening appointments</li>
				<li>Experienced and specialist advice on Planning Permission and Party Wall matters</li>
				<li>Advice on detailed extension costs and how to add the most value</li>
				<li>Creative advice on potential home design and internal layout options and side return ideas</li>
				<li>A detailed budget estimate fast-tracked and sent within 24 hours of the meeting</li>
			</ul>

			<p class="fee-orange">Fee of just £125 + VAT</p>
			<p style="padding-top: 0; font-weight: normal;">(fully refundable within 30 days against subsequent Design Phase instruction)</p>

			<p class="links" ><a class="book-link" href="/book-visit.html?type=premium">Book now</a></p>

		</div>
		<div class="col_half col_last">
			<p><img src="/images/planning.jpg" style="width: 100%;"></p>
			<p style="font-size: 14px;">We also offer early morning and late evening appointments as part of this service.</p>
			<p style="font-size: 14px;">A number of our clients who book a Planning & Design Consultation subsequently contract Build Team to undertake the Design Phase. In this event, we will refund the £125 + VAT fee against the cost of the Design Phase if instructed within 30 days.</p>
		</div>

		<div class="col_full">

      <div class="boxes">
        <div class="darkblue-box">find out<br><span class="orange">more</span></div>
        <div class="lightblue-box"><a href="/what-we-do/guarantees-and-insurance-page.html">10 year<br><span class="orange">guarantee</span></a></div>
        <div class="lightblue-box"><a href="/about-us/guarantees-and-insurance-page.html"><span class="orange">completion</span><br>pack</a></div>
        <div class="lightblue-box"><a href="/build-your-price-start.html">build your<br><span class="orange">price</span></a></div>
        <div class="lightblue-box" style="margin-right: 0;"><a href="/what-we-do/process.html">the <span class="orange">8 step</span><br>process</a></div>
      </div>
      
    </div>
	</div>

</div>

<?php /* message popup custom, disabled per now:
<div class="messagepop">
  <h3>Please wait...</h3>
  <p>you are now being redirected to our sister<br /> company Design Team to complete your<br /> booking</p>
  
  <p class="logos">
    <img class="logo-1" src="/images/logo_newer.png" />
    <img class="loading" src="/images/loading.gif" />
    <img class="logo-2" src="/images/designteam_logo1.png" />
  </p>
</div>

<div class="overlay"></div>

<style>
.overlay {
    position:fixed;
    display:none; 

    /* color with alpha channel *
    background-color: rgba(0, 0, 0, 0.7); /* 0.7 = 70% opacity *

    /* stretch to screen edges *
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;
}

.messagepop {
  background-color:#FFFFFF;
  border:15px solid #263c76;
  cursor:default;
  display:none;
  margin-top: 15px;
  position:absolute;
  text-align:left;
  width:594px;
  z-index:50;
  padding: 25px 25px 20px;
}
p.logos img{
  display: inline-block;
}
.logo-1{
  width:200px;
}
.logo-2{
  width:250px;
}
.loading{
  margin:0 50px 50px 50px;
}

.messagepop label {
  display: block;
  margin-bottom: 3px;
  padding-left: 15px;
  text-indent: -15px;
}
.messagepop h3{
  font-size:19px;
  font-weight: normal;
  text-align: center;
}
.messagepop p{
  font-size:20px;
  font-weight: normal;
  text-align: center;
  border:none;
}

.messagepop p, .messagepop.div {
  border-bottom: 1px solid #EFEFEF;
  margin: 8px 0;
  padding-bottom: 8px;
}
</style>

<script>
  var redirect_link = "";
  $(document).ready(function(){
    $("a.book-link").click(function(){
      $(".messagepop").center();
      redirect_link = $(this).attr("href");
      $(".messagepop").show();
      $(".overlay").toggle();
      setTimeout(function() {
        window.location = redirect_link;
      }, 5000);
      return false;
    });
    $(".messagepop").center();
  });
  
  jQuery.fn.center = function () {
    this.css("position","absolute");
    this.css("top", ( jQuery(window).height() - this.height() ) / 2+jQuery(window).scrollTop() + 0 + "px");
    this.css("left", ( jQuery(window).width() - this.width() ) / 2+jQuery(window).scrollLeft() + "px");
    return this;
  }
</script>
*/
?>

<?php
  require_once('./inc/footer.inc.php');
?>