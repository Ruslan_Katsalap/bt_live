<?php

@session_start();
$path = dirname(__FILE__);
$path = str_replace('\\', '/', $path);
global $a_settings;
include_once $path . '/inc/init.php';

//echo WISE_DB_NAME.'<pre>'.print_r($_SESSION, 1).'</pre>!!!';#debug

if ( ! isset($_SESSION[WISE_DB_NAME]['book_visit']['product_code'])) {
    header('Location: initial_visit');
    exit();
} else {
    //echo '!!!<pre>'.print_r($_SESSION[WISE_DB_NAME]['book_visit'], 1).'</pre>';#debug
}

if ('designteamldn.com.local' != $_SERVER['HTTP_HOST'] && 'localhost' != $_SERVER['HTTP_HOST'] && ! is_int(strpos($_SERVER['HTTP_HOST'],
        '192.168.')) && ! (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && 'https' == $_SERVER['HTTP_X_FORWARDED_PROTO'])
) {
/*    header('Location: https://' . (strpos($_SERVER['HTTP_HOST'],
            'www.') === 0 ? '' : 'www.') . $_SERVER['HTTP_HOST'] . '/' . WISE_RELURL . 'pay_now');
    exit(); 


*/
}

$title = 'Booking Summary';

// $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['name']

$meta_description = 'Booking Summary DesignTeam London';

$name = htmlentities(substr($_SESSION[WISE_DB_NAME]['book_visit']['fname'] . ' ' . $_SESSION[WISE_DB_NAME]['book_visit']['lname'],
    0, 150), ENT_QUOTES, 'UTF-8');

$postcode = htmlentities($_SESSION[WISE_DB_NAME]['book_visit']['postcode'], ENT_QUOTES, 'UTF-8');

$availibility = $_SESSION[WISE_DB_NAME]['book_visit']['availibility'];
$o_avail      = json_decode($availibility);
$a_pref_dates = [];
$availibility = '';
if ($o_avail) {
    foreach ($o_avail AS $k => $v) {
        $k    = explode('/', $k);
        $k[2] = substr($k[2], 2, 3);
        $k    = $k[1] . '.' . $k[0] . '.' . $k[2];
        $availibility .= $k . ' - ' . strtoupper($v) . "\n";
        $a_pref_dates[$k] = $v;
    }
}
$availibility = trim($availibility);


$sql = "SELECT * FROM visit WHERE visit_id=" . (int)$_SESSION[WISE_DB_NAME]['book_visit']['visit_id'];
SQL2Array($sql, $visit);

if (isset($visit[0]['visit_id'])) {
    $visit = $visit[0];
} else {
    header('Location: initial_visit');
    exit();
}


$error = "";
if ('POST' == $_SERVER['REQUEST_METHOD']) {

    $CardName  = '';
    $k         = 'chname';
    $_POST[$k] = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ( ! $_POST[$k]) {
        $error .= "enter Card Holder Name\n";
    } else {
        $CardName = $_POST[$k];
    }

    $CardNumber = '';
    $k          = 'ccnumber';
    $_POST[$k]  = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ( ! $_POST[$k]) {
        $error .= "enter Card Number\n";
    } else {
        $CardNumber = $_POST[$k];
    }

    $ExpiryDateMonth = '';
    $k               = 'expmonth';
    $_POST[$k]       = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ( ! $_POST[$k]) {
        $error .= "select Month of Expiry Date\n";
    } else {
        $ExpiryDateMonth = $_POST[$k];
    }

    $ExpiryDateYear = '';
    $k              = 'expyear';
    $_POST[$k]      = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ( ! $_POST[$k]) {
        $error .= "select Year of Expiry Date\n";
    } else {
        $ExpiryDateYear = $_POST[$k];
    }

    $k         = 'terms_agree';
    $_POST[$k] = isset($_POST[$k]) ? (int)$_POST[$k] : '';
    if ( ! $_POST[$k]) {
        $error .= "please read and accept Terms & Conditions to proceed\n";
    }


    $StartDateMonth = '';
    $k              = 'startmonth';
    $_POST[$k]      = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ($_POST[$k]) {
        $StartDateMonth = $_POST[$k];
    }

    $StartDateYear = '';
    $k             = 'startyear';
    $_POST[$k]     = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ($_POST[$k]) {
        $StartDateYear = $_POST[$k];
    }

    $CV2       = '';
    $k         = 'cv2';
    $_POST[$k] = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ( ! $_POST[$k]) {
        $error .= "enter CV2 Number\n";
    } else {
        $CV2 = $_POST[$k];
    }

    $IssueNumber = '';
    $k           = 'cinumber';
    $_POST[$k]   = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ($_POST[$k]) {
        $IssueNumber = $_POST[$k];
    }

    $PostCode  = '';
    $k         = 'billing_postcode';
    $_POST[$k] = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ($_POST[$k]) {
        $PostCode = $_POST[$k];
    }

    $Address1  = '';
    $k         = 'addr1';
    $_POST[$k] = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    $k         = 'addr2';
    $_POST[$k] = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    $addr2     = $_POST['addr2'];
    if ($addr2) {
        $addr2 = str_replace('_comma_', ',', $addr2);
        $addr2 = str_replace('_', ' ', $addr2);
        $addr2 = ucwords($addr2);
    }
    if ( ! $_POST['addr1'] || strlen($_POST['addr1']) < 4) {
        $Address1 = $addr2;
    } else {
        $Address1 = $_POST['addr1'];
    }

    $City      = ''; // 'London';
    $k         = 'city';
    $_POST[$k] = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ($_POST[$k]) {
        $City = $_POST[$k];
    }

    $State     = ''; //'London'; // county
    $k         = 'county';
    $_POST[$k] = isset($_POST[$k]) ? trim(get_magic_quotes_gpc() ? stripslashes($_POST[$k]) : $_POST[$k]) : '';
    if ($_POST[$k]) {
        $State = $_POST[$k];
    }

}


// PaymentSense integration ////////////////////////////////////////////

include_once("paymentsense/Config.php");

// set vars for PaymentSense processor
$ShoppingCartAmount           = $visit['amount'] * 100; // normalize total amount
$ShoppingCartCurrencyShort    = "GBP";
$ShoppingCartOrderID          = $visit['visit_id'];
$ShoppingCartOrderDescription = "DesignTeam " . $a_settings['a_visit_types'][$_SESSION[WISE_DB_NAME]['book_visit']['product_code']]['name'] . " Visit";

$CountryShort = 'GBR'; // United Kingdom

$Amount           = $ShoppingCartAmount;
$CurrencyShort    = $ShoppingCartCurrencyShort;
$OrderID          = $ShoppingCartOrderID;
$OrderDescription = $ShoppingCartOrderDescription;

$Address2 = '';
$Address3 = '';
$Address4 = '';


require_once("paymentsense/PaymentFormHelper.php");

$ShoppingCartHashDigest = PaymentFormHelper::calculateHashDigest(PaymentFormHelper::generateStringToHash($ShoppingCartAmount,
    $ShoppingCartCurrencyShort,
    $ShoppingCartOrderID,
    $ShoppingCartOrderDescription,
    $SecretKey));

include("paymentsense/PreProcessPaymentForm.php");

//include ("paymentsense/ISOCurrencies.php"); // include once

if (isset($DuplicateTransaction) != true) {
    $DuplicateTransaction = false;
}

// Allow zero amount for 100% discount coupons:
if (($visit['amount'] == 0 || 1) && 'POST' == $_SERVER['REQUEST_METHOD']) {
    $TransactionSuccessful = true;
    $Message               = "Zero amount (100% discount coupon)";
}

if (isset($TransactionSuccessful) && $TransactionSuccessful && isset($Message) && $Message) {

    placeVisit($visit, $Message); // update log, used coupon, thank you session, send email(s)

}


include 'header.php';

//echo '!!!<pre>'.print_r($a_settings, 1).'</pre>';#debug

?>

    <div class="row book-visit">
        <div class="col-sm-7 col-sm-offset-1">
            <h1><?php echo $title ?></h1>
            <div class="book-wrap">
                <form method="POST" id="fm_pay_now"
                      name="Form" <?php if (isset($FormAction) && $FormAction != "PaymentForm.php") {
                    echo 'action="' . $FormAction . '"';
                }
                if (isset($FormAttributes)) {
                    echo ' ' . $FormAttributes;
                } ?>>

                    <!-- important -->
                    <input name="FormMode" type="hidden" value="<?php

                    if (isset($NextFormMode) && 'RESULTS' != $NextFormMode) {
                        echo $NextFormMode;
                    } else {
                        echo 'PAYMENT_FORM';
                    }

                    ?>"/>
                    <input name="HashDigest" type="hidden" value="<?php if (isset($ShoppingCartHashDigest))
                        echo $ShoppingCartHashDigest ?>"/>
                    <span id="init_HashDigest" style="display:none"><?php if (isset($ShoppingCartHashDigest))
                            echo $ShoppingCartHashDigest ?></span>


                    <?php if (isset($TransactionSuccessful) && $TransactionSuccessful): ?>
                        You are redirecting now, to proceed immediately please <a href="thank_you">click here</a>
                        <script type="text/javascript">
                            document.location = "thank_you";
                        </script>
                    <?php endif; // $TransactionSuccessful ?>

                    <?php if (($error || (isset($Message) == true && $Message != "") || (isset($DuplicateTransaction) && $DuplicateTransaction)) && ! (isset($TransactionSuccessful) && $TransactionSuccessful)) {

                        echo '<div class="col-sm-12"><div class="error">';

                        if ($error) {
                            echo 'Please fill out the following information:<br/><br/>' . nl2br(htmlspecialchars($error,
                                    ENT_QUOTES, 'UTF-8'));
                        }

                        // paymentsense:
                        //if (isset($Message) == true && $Message != "")	{
                        if ( ! $error) {
                            if (isset($Message) && $Message) {
                                //echo $Message; #debug
                                // show message without specific details, write details into DB log
                                $a_pay_msg = explode('<br />', $Message);
                                // show general message:
                                echo 'Please check Payment information:<br/><br/>' . trim($a_pay_msg[0]); // basic message
                                // clean details:

                                if (count($a_pay_msg) > 1) {
                                    $a_pay_msg[1] = str_replace('</li>', "\n", $a_pay_msg[1]);
                                    $a_pay_msg[1] = strip_tags($a_pay_msg[1]);
                                    //echo $a_pay_msg[1];#debug
                                    if ( ! $visit['log']) {
                                        $sql = "UPDATE visit SET log='" . q($a_pay_msg[1]) . "', date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=0 WHERE visit_id=" . (int)$visit['visit_id'];
                                    } else {
                                        $sql = "UPDATE visit SET log=CONCAT(log, '" . q($a_pay_msg[1]) . "'), date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=0 WHERE visit_id=" . (int)$visit['visit_id'];
                                    }
                                    query($sql);

                                    if (is_int(strpos($a_pay_msg[1], 'PaymentMessage.CardDetails.IssueNumber'))) {
                                        echo '<br/>please enter correct Card issue number or leave it blank';
                                    }
                                    if (is_int(strpos($a_pay_msg[1], 'PaymentMessage.CardDetails.CV2'))) {
                                        echo '<br/>please enter correct CV2 Number';
                                    }

                                }
                            } else {

                                if (isset($DuplicateTransaction) && $DuplicateTransaction) {
                                    echo 'We detected a duplicate transaction. It means that a transaction with these details has already been processed by the payment provider.';
                                    if (isset($PreviousTransactionMessage) && $PreviousTransactionMessage) {
                                        echo ' The details of the original transaction:<br/><br/>' . $PreviousTransactionMessage;
                                    }
                                }

                            }
                        }

                        echo '</div></div>';

                    }
                    ?>

                    <div class="book-summary">
                        <div class="col-sm-6">
                            <p><em>Name:</em> <span><?php /*Lucas Docherty*/
                                    echo $name ?></span></p>
                            <p><em>Postcode:</em> <span id="visit_postcode"><?php /*SW4 6DH*/
                                    echo $postcode ?></span></p>
                            <p><em>Dates selected:</em> <span><?php echo nl2br($availibility);
                                    /*29.01.15 - AM<br/>
                                    28.01.15 - PM<br/>
                                    23.01.15 - AM*/
                                    ?></span></p>
                            <p id="visit_addr" style="display:none"><?php echo htmlentities($visit['addr'], ENT_QUOTES,
                                    'UTF-8') ?></p>
                            <p id="visit_addr2" style="display:none"><?php echo htmlentities($visit['addr2'],
                                    ENT_QUOTES, 'UTF-8') ?></p>
                        </div>
                        <div class="col-sm-6">

                            <!--<p><em>Booking <?php echo $a_settings['a_visit_types'][$_SESSION[WISE_DB_NAME]['book_visit']['product_code']]['name'] ?> Visit</em></p>-->

                            <p><em>Price:</em> <span
                                    id="price"><?php if ( ! isset($visit['amount']) || ! isset($visit['amount'])): ?>&pound;<?php echo $a_settings['a_visit_types'][$_SESSION[WISE_DB_NAME]['book_visit']['product_code']]['price_vat'] ?>
                                    (&pound;<?php echo $a_settings['a_visit_types'][$_SESSION[WISE_DB_NAME]['book_visit']['product_code']]['price'] ?>
                                    + VAT)<?php else:

                                        if ($visit['amount']) {
                                            echo '&pound;' . $visit['amount'] . ' (&pound;' . round($visit['amount'] / 1.2) . ' + VAT)';
                                        } else {
                                            echo 'FREE';
                                        }
                                        /* if exists take price from visit record */

                                        ?>

                                    <?php endif; ?></span>
                                <span
                                    id="init_price">&pound;<?php echo $a_settings['a_visit_types'][$_SESSION[WISE_DB_NAME]['book_visit']['product_code']]['price_vat'] ?>
                                    (&pound;<?php echo $a_settings['a_visit_types'][$_SESSION[WISE_DB_NAME]['book_visit']['product_code']]['price'] ?>
                                    + VAT)</span>
                            </p>

                            <?php if (isset($a_settings['coupon_enabled']) && $a_settings['coupon_enabled']) : ?>
                                <p class="input"><em>Offer Code:</em> <span><input type="text" name="coupon_code"
                                                                                   id="coupon_code"
                                                                                   value="<?php if (isset($visit['coupon_code'])) {
                                                                                       echo htmlspecialchars($visit['coupon_code'],
                                                                                           ENT_QUOTES, 'UTF-8');
                                                                                   } ?>"/></span></p>
                            <?php endif; ?>

                            <p id="coupon_msg" class="error"></p>

                            <p>
                                <em><a href="/<?php echo WISE_RELURL ?>book_visit?type=<?php echo $_SESSION[WISE_DB_NAME]['book_visit']['product_code']; ?>">Edit <?php echo $a_settings['a_visit_types'][$_SESSION[WISE_DB_NAME]['book_visit']['product_code']]['name'] ?>
                                        Visit</a></em></p>
                        </div>
                    </div>
                    <div class="card-title">
                        <div class="col-sm-6">
                            <h2>Payment - Card Details</h2>
                        </div>
                        <div class="col-sm-6">
                            <img src="/<?php echo WISE_RELURL ?>images/credit_cards.png"
                                 alt="VISA, MasterCard, Maestro, VISA Debit, VISA Electron"/>
                        </div>
                    </div>
                    <div id="pay_cc_details">

                        <?php //echo '<div class="error">$NextFormMode='.$NextFormMode.' $FormAttributes='.$FormAttributes.' $DuplicateTransaction: '.(isset($DuplicateTransaction) && $DuplicateTransaction?'Yes':'No').' $TransactionSuccessful: '.(isset($TransactionSuccessful) && $TransactionSuccessful?'Yes':'No').'</div>';#debug ?>

                        <?php if ( ! $error && 'THREE_D_SECURE' == $NextFormMode) : ?>
                            <div class="col-sm-12">

                                <?php

                                $SiteSecureBaseURL = PaymentFormHelper::getSiteSecureBaseURL();
                                include("paymentsense/Templates/3DSIFrame.tpl");

                                //echo $ShoppingCartHashDigest . ' :: '. $szHashDigest;#debug

                                ?>
                            </div>
                        <?php endif; // if THREE_D_SECURE ?>

                        <?php
                        // !(isset($SuppressFormDisplay) && $SuppressFormDisplay)
                        if ($error || ('THREE_D_SECURE' != $NextFormMode && ! (isset($TransactionSuccessful) && $TransactionSuccessful))) : ?>

                        <div class="col-sm-6">
                            <label>Card Holder Name</label>
                            <input type="text" name="chname" value="<?php if (isset($_POST['chname'])) {
                                echo htmlentities($_POST['chname'], ENT_QUOTES, 'UTF-8');
                            } ?>"/>

                            <label>Card Number</label>
                            <input type="text" name="ccnumber" id="ccnumber"
                                   value="<?php if (isset($_POST['ccnumber'])) {
                                       echo htmlentities($_POST['ccnumber'], ENT_QUOTES, 'UTF-8');
                                   } ?>" autocomplete="off"/>


                            <div class="row">
                                <div class="col-sm-12"><label>CV2 Number (for more info <a href="#" class="dt-ajax"
                                                                                           data-toggle="modal"
                                                                                           data-target="#modalCV2">click
                                            here</a>)</label></div>
                                <div class="col-xs-6"><input type="text" name="cv2"
                                                             value="<?php if (isset($_POST['cv2'])) {
                                                                 echo htmlentities($_POST['cv2'], ENT_QUOTES, 'UTF-8');
                                                             } ?>" autocomplete="off"/></div>
                            </div>


                        </div>
                        <div class="col-sm-6">
                            <label>Expiry Date</label>

                            <div class="row">
                                <div class="col-xs-5">

                                    <div class="dt-selectbox">
                                        <input type="hidden" name="expmonth" value="<?php if (isset($_POST['expmonth']))
                                            echo htmlentities($_POST['expmonth'], ENT_QUOTES, 'UTF-8') ?>"/>
                                        <div class="dt-select" id="dt-dropdown-expmonth">
                                            <span><?php echo (isset($_POST['expmonth']) && $_POST['expmonth']) ? htmlentities($_POST['expmonth'],
                                                    ENT_QUOTES, 'UTF-8') : '&nbsp;' ?></span>
                                            <img src="/<?php echo WISE_RELURL ?>images/bg_selectbox.png"
                                                 class="dt-select-dropdown" alt="select"/>
                                        </div>
                                        <div class="dt-select-wrap" id="dt-expmonth-body">
                                            <div class="dt-button dt-component" id="dt-expmonth-">&nbsp;</div>
                                            <?php for ($i = 1; $i <= 12; $i++) {
                                                if ($i < 10) {
                                                    $k = '0' . $i;
                                                } else {
                                                    $k = $i;
                                                }
                                                ?>
                                                <div
                                                    class="dt-button dt-component<?php if (isset($_POST['expmonth']) && $k == $_POST['expmonth'])
                                                        echo ' dt-active' ?>"
                                                    id="dt-expmonth-<?php echo $k ?>"><?php echo $k ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-6">

                                    <div class="dt-selectbox">
                                        <input type="hidden" name="expyear" value="<?php if (isset($_POST['expyear']))
                                            echo htmlentities($_POST['expyear'], ENT_QUOTES, 'UTF-8') ?>"/>
                                        <div class="dt-select" id="dt-dropdown-expyear">
                                            <span><?php echo (isset($_POST['expyear']) && $_POST['expyear']) ? substr(date('Y'),
                                                        0, 2) . htmlentities($_POST['expyear'], ENT_QUOTES,
                                                        'UTF-8') : '&nbsp;' ?></span>
                                            <img src="/<?php echo WISE_RELURL ?>images/bg_selectbox.png"
                                                 class="dt-select-dropdown" alt="select"/>
                                        </div>
                                        <div class="dt-select-wrap" id="dt-expyear-body">
                                            <div class="dt-button dt-component" id="dt-expyear-">&nbsp;</div>
                                            <?php
                                            $k = (int)date('Y');
                                            for ($i = $k; $i < ($k + 11); $i++) {
                                                $v = substr($i, 2, 2);
                                                ?>
                                                <div
                                                    class="dt-button dt-component<?php if (isset($_POST['expyear']) && $v == $_POST['expyear'])
                                                        echo ' dt-active' ?>"
                                                    id="dt-expyear-<?php echo $v ?>"><?php echo $i ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <label>Card Start Date (if on card)</label>
                            <div class="row">
                                <div class="col-xs-5">

                                    <div class="dt-selectbox">
                                        <input type="hidden" name="startmonth"
                                               value="<?php if (isset($_POST['startmonth']))
                                                   echo htmlentities($_POST['startmonth'], ENT_QUOTES, 'UTF-8') ?>"/>
                                        <div class="dt-select" id="dt-dropdown-startmonth">
                                            <span><?php echo (isset($_POST['startmonth']) && $_POST['startmonth']) ? htmlentities($_POST['startmonth'],
                                                    ENT_QUOTES, 'UTF-8') : '&nbsp;' ?></span>
                                            <img src="/<?php echo WISE_RELURL ?>images/bg_selectbox.png"
                                                 class="dt-select-dropdown" alt="select"/>
                                        </div>
                                        <div class="dt-select-wrap" id="dt-startmonth-body">
                                            <div class="dt-button dt-component" id="dt-startmonth-">&nbsp;</div>
                                            <?php for ($i = 1; $i <= 12; $i++) {
                                                if ($i < 10) {
                                                    $k = '0' . $i;
                                                } else {
                                                    $k = $i;
                                                }
                                                ?>
                                                <div
                                                    class="dt-button dt-component<?php if (isset($_POST['startmonth']) && $k == $_POST['startmonth'])
                                                        echo ' dt-active' ?>"
                                                    id="dt-startmonth-<?php echo $k ?>"><?php echo $k ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>
                                <div class="col-xs-6">

                                    <div class="dt-selectbox">
                                        <input type="hidden" name="startyear"
                                               value="<?php if (isset($_POST['startyear']))
                                                   echo htmlentities($_POST['startyear'], ENT_QUOTES, 'UTF-8') ?>"/>
                                        <div class="dt-select" id="dt-dropdown-startyear">
                                            <span><?php echo (isset($_POST['startyear']) && $_POST['startyear']) ? substr(date('Y'),
                                                        0, 2) . htmlentities($_POST['startyear'], ENT_QUOTES,
                                                        'UTF-8') : '&nbsp;' ?></span>
                                            <img src="/<?php echo WISE_RELURL ?>images/bg_selectbox.png"
                                                 class="dt-select-dropdown" alt="select"/>
                                        </div>
                                        <div class="dt-select-wrap" id="dt-startyear-body">
                                            <div class="dt-button dt-component" id="dt-startyear-">&nbsp;</div>
                                            <?php
                                            $k = (int)date('Y') - 15;
                                            for ($i = $k; $i < ($k + 16); $i++) {
                                                $v = substr($i, 2, 2);
                                                ?>
                                                <div
                                                    class="dt-button dt-component<?php if (isset($_POST['startyear']) && $v == $_POST['startyear'])
                                                        echo ' dt-active' ?>"
                                                    id="dt-startyear-<?php echo $v ?>"><?php echo $i ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                </div>

                            </div>

                            <div class="row">
                                <div class="col-sm-12"><label>Card issue number (if applicable)</label></div>
                                <div class="col-xs-6"><input type="text" name="cinumber"
                                                             value="<?php if (isset($_POST['cinumber'])) {
                                                                 echo htmlentities($_POST['cinumber'], ENT_QUOTES,
                                                                     'UTF-8');
                                                             } ?>" autocomplete="off"/></div>
                            </div>

                        </div>

                    </div><!-- /#pay_cc_details -->

                    <div class="pay_bottom">

                        <div class="col-sm-12 checkbox">
                            <input type="checkbox" name="same_addr" id="same_addr"
                                   value="1" <?php if (isset($_POST['same_addr']) && $_POST['same_addr'])
                                echo ' checked="checked"' ?> /> <label for="same_addr">Tick this box if billing address
                                and site visit address are the same</label>
                        </div>

                        <div class="col-sm-6">
                            <label>Postcode</label>
                            <input type="text" name="billing_postcode"
                                   value="<?php if (isset($_POST['billing_postcode'])) {
                                       echo htmlentities($_POST['billing_postcode'], ENT_QUOTES, 'UTF-8');
                                   } ?>"/>
                        </div>
                        <div class="col-sm-6">

                            <label>House / Flat No, Street</label>
                            <input type="text" name="addr1" value="<?php if (isset($_POST['addr1'])) {
                                echo htmlentities($_POST['addr1'], ENT_QUOTES, 'UTF-8');
                            } ?>"/>

                            <div class="dt-selectbox" style="display:none">
                                <input type="hidden" name="addr2" value="<?php if (isset($_POST['addr2']))
                                    echo htmlentities($_POST['addr2'], ENT_QUOTES, 'UTF-8') ?>"/>
                                <div class="dt-select" id="dt-dropdown-addr2">
                  <span><?php if (isset($_POST['addr2']) && $_POST['addr2']) {
                          $addr2 = $_POST['addr2'];
                          $addr2 = str_replace('_comma_', ',', $addr2);
                          $addr2 = str_replace('_', ' ', $addr2);
                          echo htmlentities($addr2, ENT_QUOTES, 'UTF-8');
                      } else {
                          echo 'Or select address';
                      } ?></span>
                                    <img src="/<?php echo WISE_RELURL ?>images/bg_selectbox.png"
                                         class="dt-select-dropdown" alt="select"/>
                                </div>
                                <div class="dt-select-wrap" id="dt-addr2-body">
                                    <div class="dt-button dt-component" id="dt-addr2-">Or select address</div>
                                    <?php if (isset($_SESSION[WISE_DB_NAME]['a_addr']) && $_SESSION[WISE_DB_NAME]['a_addr']) :
                                        foreach ($_SESSION[WISE_DB_NAME]['a_addr'] AS $v) {
                                            $k = strtolower($v);
                                            $k = str_replace(' ', '_', $k);
                                            $k = str_replace(',', '_comma_', $k);
                                            echo '<div class="dt-button dt-component' . ((isset($_POST['addr2']) && $k == $_POST['addr2']) ? ' dt-active' : '') . '" id="dt-addr2-' . $k . '">' . $v . '</div>' . "\n";
                                        }
                                    else: ?>
                                        <span>-- No data --</span>
                                    <?php endif; ?>
                                </div>
                            </div>


                        </div>

                        <div class="billing_city_county" style="display:none">
                            <div class="col-sm-6">
                                <label>City</label>
                                <input type="text" name="city" value="<?php if (isset($_POST['city'])) {
                                    echo htmlentities($_POST['city'], ENT_QUOTES, 'UTF-8');
                                } ?>"/>
                            </div>
                            <div class="col-sm-6">
                                <label>County</label>
                                <input type="text" name="county" value="<?php if (isset($_POST['county'])) {
                                    echo htmlentities($_POST['county'], ENT_QUOTES, 'UTF-8');
                                } ?>"/>
                            </div>
                        </div>


                        <div class="col-sm-12 checkbox">
                            <input type="checkbox" name="terms_agree" id="terms_agree"
                                   value="1" <?php if (isset($_POST['terms_agree']) && $_POST['terms_agree'])
                                echo ' checked="checked"' ?> /> <label for="terms_agree">I have read and accept the <a
                                    href="/<?php echo WISE_RELURL ?>terms_conditions" target="_blank">Terms&nbsp;&amp;&nbsp;Conditions</a>
                                of sale</label>
                        </div>


                        <div class="col-sm-12">
                            <a href="#" class="book-now"><span>Book Now</span></a>
                        </div>

                    </div>

                <?php else: // close pay_cc_details /div
                    echo '</div><!-- /#pay_cc_details -->';
                    ?>
                <?php endif; // endif !$SuppressFormDisplay && 'THREE_D_SECURE' != $NextFormMode ?>


                </form>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="v_progress">
                <h3>how it works</h3>
                <div class="step">
                    <h4>choose</h4>
                    <p>you select dates that are convenient for you</p>
                </div>
                <div class="step">
                    <h4>confirm</h4>
                    <p>we will contact you within 1 working day to confirm your site visit</p>
                </div>
                <h4>site visit</h4>
                <p>meeting with a member of Design Team</p>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalCV2" tabindex="-1" role="dialog" aria-labelledby="modalCV2Label"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="modalCV2Label">CV2 Number</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <img src="images/CV2_sample.png" alt="CV2 number"/>
                        </div>
                        <div class="col-sm-6">
                            The <strong>CV2</strong> Number (&quot;<strong>C</strong>ard <strong>V</strong>erification
                            <strong>V</strong>alue&quot;, also called <strong>CVV</strong> or <strong>CVV2</strong>) is
                            your card security code. To find it, please turn your card over and look at the signature
                            strip. You will see the last 3 digits printed in the box
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Ok</button><!--btn-default-->
                </div>
            </div>
        </div>
    </div>

<?php include 'footer.php'; ?>