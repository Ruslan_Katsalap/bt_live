<?php $page_name = "pp_booking"; ?>
<?php require_once('./inc/header.inc.php'); ?>
<div class="center" style="
    margin-top: 100px;
    margin-bottom: 100px;
    font-size: 25px;
    line-height: 35px;

">
	<div class="succ_pay_msg" style="text-align: center;">
		<p style="margin-left: 0;margin-right: 0;">Thank you for your booking.</p>
		<p style="margin-left: 0;margin-right: 0;">A member of our team will be in touch<br> shortly to schedule the Pre-Purchase visit.</p>
	</div>
</div>
<?php require_once('./inc/footer.inc.php'); ?>