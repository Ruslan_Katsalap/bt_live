<?php
	require_once('./inc/header.inc.php');
/**************This is CRM integration code*****************/
         require_once($_SERVER["DOCUMENT_ROOT"]."/crm_config.php");
/**************This is CRM integration code*****************/
	$curr_url = getCurrentUrl();
	$rs = getRs("SELECT * FROM page WHERE page_code = '{$curr_url}'");
	$res = mysqli_fetch_assoc($rs);
	$pid = $res['page_id'];
	$media = getRs("SELECT * FROM page_media WHERE page_id =".$pid);
	$media_res = mysqli_fetch_assoc($media);

if($_REQUEST['user_name'] != '' && $_REQUEST['contact_number'] != '' && $_REQUEST['uemail'] != '') {
/**************This is CRM integration code*****************/
 list($name,$last_name) =  explode(" ",$_POST['user_name']);
 $full_name =  $name.' '.$last_name;
 $now = strftime("%Y-%m-%dT%H:%M:%S");
 $comment = "Do you require our Design or Build Service or Both ?: ";
 $comment.= "\r\n".$_POST['design_build_service'];
 $comment.="\r\n Which tour are you interested in attending ? ".$_POST['tour_sttending'];
 $comment.="\r\n Address:\r\n ".$_POST['address'];

 $data = array(
    "NAME" =>  $name,
    "LAST_NAME" => $last_name,
    "UF_CRM_1490175334" => $now,
    "UF_CRM_1500629522" => 146,
    "EMAIL_HOME" => $_POST['uemail'],
    "PHONE_HOME" =>$_POST['contact_number'],
    "TITLE" => $full_name,
    "SOURCE_DESCRIPTION" => "BT",
    "COMMENTS" => $comment
  );
  CrmClient::$SOURCE_ID = 4;

  if(!CrmClient::sendToCRM($data)) {
     echo 'error send to crm = ',CrmClient::$LAST_ERROR;
  }
/**************This is CRM integration code*****************/
}
 ?>
<!--<div style="width:50%; margin:auto;"><img src="../images/co.jpg" alt="House Tours" width="100%" /></div> -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<style type="text/css">
	.tour_dates {
		width: 25%;
		margin-bottom: 18px;
		float: left;
		border: 3px solid #183e70;
		padding: 38px 25px 25px 25px;
		margin-right: 30px;
}

.date_box {
    width: 17%;
    background: #a0a0a0 !important;
    float: left;
    text-align: center;
    padding: 15px;
	color: #fff !important;
	margin-right:2%;
	min-height: 30px;
}
.form_row {
  float: left;
  margin: 10px 0;
  width: 100%;
}
.form_row > label {
  display: block;
    font-weight: regular;
    line-height: 1em;
    margin-bottom: 0;
    margin-top: 12px;
    font-size: 20px;
    float: left;
	color:#183e70 !important;
}
.form_input{
  width: 100%;
    margin-top: 5px;
    border: 1px solid #8b8f94;
    margin-bottom: 10px;
    background: none;
    max-height: 32px;
	height:32px;
}
.form_textarea {
  width: 100%;
    margin-top: 5px;
    border: 1px solid #8b8f94;
    margin-bottom: 10px;
    background: none;
}
 .register_details {
 background: #0f4778;
    padding: 15px 30px 15px 30px;
    color: #fff;
    cursor: pointer;
    width: 100px;
    border: none;
    font-size: 18px;
    margin-top: 20px;
}
.success_msg {
  border: 2px solid green;
  float: left;
  margin-bottom: 25px;
  padding: 6px 8% 6px 10px;
  width: auto;
  color: green;
}
.error_msg {
  background: #fca1a1 none repeat scroll 0 0;
  border: 2px solid #f00;
  color: #B71D1D;
  float: left;
  margin-bottom: 25px;
  padding: 5px 12px;
  width: 48%;
}
#housetourbg {
	position: relative;
max-width: 1920px;
margin: 0 auto;
background-color: #E7E7E8;
width: 100%;
	}
#housetourbg video {
	max-width:1000px;
	margin: 60px 24% 60px;
	}
.playpause {
    background-image:url(../housetour/play_arrow.png);
    background-repeat:no-repeat;
    width: 1000px;
height: 535px;
position: absolute;
left: 0%;
right: -8px;
top: 29px;
    bottom:0%;
    margin:auto;
    background-size:cover;
	background-position: top;
	
}
.next_tour_arrow img {
    position: absolute;
    right: -100px;
    /* bottom: 0px; */
    margin-top: -20px;
	max-width: 90px;
}
.groud_flour_arrow img {
	 position: absolute;
    left: -145px;
    /* bottom: 0px; */
    margin-top: -40px;
    max-width: 140px;
	}
.needmoreinfo {
	display:none;
	}
.needmoreinfo img {
	position: absolute;
    right: -155px;
    /* bottom: 0px; */
    margin-top: 0px;
    max-width: 170px;
	}
.bookbutton {
    position: absolute;
    right: 20px;
}
.bookbutton a {
    padding: 5px 10px;
    border: 1px solid #a0a0a0;
}
.tour_dates {
	position:relative;
	}
.forbiggerfonts {
	font-size:15px;
	line-height: 17px;
	}
.date_box .bigfonts {
    font-size: 16px;
	padding-bottom: 5px;
    display: inline-block;
}
.details_box {
    position: relative;
    top: 15px;
	line-height: 18px;
	text-transform:capitalize;
}
.video2 {
	height: 372px;
	margin-top: -48px;
}
.glyphicon-chevron-left:before, .glyphicon-chevron-right:before {
	content:"" !important;
	}
.carousel-control span img {
    max-width: 40px;
}
.col_full h1 {
	color: #f5641e !important;
	font-size:22px !important;
	}
#new_page_house_tours_new {
	font-size: 30px !important;
    color: #003c70;
    text-transform: capitalize !important;
    font-weight: bold;
    font-family: calibri !important;
	padding-bottom: 10px !important;
    margin-bottom: 20px !important;
	}
.forbiggerfonts p:first-child {
	padding-top:0px !important;
	}
.forbiggerfonts p {
    color: #75787f;
	font-weight: 400;
}
.forbiggerfonts p strong {
	    color: #0f4678;
	}
.form_row label {
	color: #104771;
	}
.broform100 input[type=checkbox] {
    width: auto;
    height: auto;
    vertical-align: middle;
    width: 20px;
    height: 20px;
    margin-right: 5px;
    padding: 5px;
}
.checkboxbro span {
    font-size: 16px;
    display: inline-block;
    width: 90%;
	vertical-align: top;
	line-height:1.5;
}
.col-md-6, .col-lg-6 {
	padding-right: 10px !important;
	padding-left: 10px  !important;
}

@media (max-width:640px) {
	.next_tour_arrow img, .groud_flour_arrow img, .needmoreinfo img{
		display:none !important;
		}
		.video {
    margin-left: -15px;
}
.col-xs-12 {
    width: 100% !important;
}
	.tour_dates {
		width:100%;
		}
	.bookbutton {
		position: relative;
		 right:0px !important;
		/* bottom: 4px; */
		margin-top: 25px;
}
#bookform h1 {
	font-size:18px;
	margin-top:20px
	}
#carousel-example-generic {
	display:none;
	}
.register_details {
	margin-bottom:20px;
	}
	.home-booking-p p {
	font-size:15px !important;
	width: 315px !important;
margin: 0 auto;
}

	}
@media (min-width:640px) {
	.details_box {
		line-height: 18px;
		}
		.bookbutton {
	bottom: 25px;
	}
	}
@media (min-width:641px) and (max-width:770px){
.col_last {
	float:right;
	}
form .col_half {
    width: 100%;
}
}
.home-booking-p p {
	font-size:16px;
	width: 61%;
margin: 0 auto;
font-style: normal;
}
.home-booking-h3 {
	margin-top:0px !important;
}
</style>
</div>
<section class="open-hrs-header">
	<div class="open-hrs-header-bg"></div>
	<h1 class="page_house_tours_new">Open House</h1>
	<p class="open-hrs-p">Our open houses are a free, no-obligation opportunity for Design and Build clients
	to tour one of our completed Builds.</p>
</section>
<section id="housetourbg">
	<video class="video" controls poster="../housetour/video_poster.png"><source src="../housetour/BuildTeam_House_Tour No_Strap.mp4" type="video/mp4"> </video>
    <div class="playpause"></div>
</section>
<section id="new_page_house_tours">

		<div class="col_full">
			<div class="col_half" style="width:46%; margin-right:5%">
				<!--<img src="../media/<?php echo $media_res['filename']; ?>" alt="<?php echo $media_res['page_media_name']; ?>" width="100%" />-->
                <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">
        <div class="item active">
          <img src="/images/house_tour/House_Tour20.jpg" width="100%" />

        </div>
        <div class="item">
         <img src="/images/house_tour/House_Tour14.jpg" width="100%" />

        </div>
        <div class="item">
         <img src="/images/house_tour/House_Tour06.jpg" width="100%" />

        </div>
        <div class="item">
         <img src="/images/house_tour/House_Tour35.jpg" width="100%" />

        </div>

        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"><img src="/images/house_tour/prev_slide.png" alt=""></span>

        </a>
        <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"><img src="/images/house_tour/next_slide.png" alt=""></span>

        </a>
        </div>
			</div>
			<div class="col_half col_last forbiggerfonts" style="width:48%">
				<?php echo $res['page_content']; ?>
			</div>
		</div>
		</section>
		<section class="upcoming-dates-bg">
		<div class="upcoming-dates">
		    <div class="col_full">
			<h2>Upcoming Dates</h2>
			<?php $today = date('Y-m-d');
			$query = "SELECT * FROM `events` WHERE `event_date`>='".$today."' AND `is_enabled`='1' AND `is_active`='1' ORDER BY `event_date` ASC";
			//$query = "SELECT * FROM `events`  ORDER BY `event_date` ASC";
			$events = mysqli_query($dbconn,$query); 
			?>
			  	<div class="tour_dates_con">
				<?php $count = 1;
				while ($event = mysqli_fetch_assoc($events)) {
					$dayMon = date('F jS', strtotime($event['event_date']));
					$year = date('Y', strtotime($event['event_date'])); ?>
					<div class="tour_dates">
						<img src="/images_new_design/icon-map.png" height="30" width="20">
						<h3 class="event-title"><?php echo $event['event_area']; ?></h3>
						<h5 class="event-date"><?php echo $dayMon; ?>, <?php echo $year; ?></h5>
						
							<div class="event-button">
								<a href="#book">Book Now</a>
								<div class="event-button-arrow"></div>
		                    </div>
						
					</div>
					<?php if( $count%2==0 ){
						echo "</div><div class='tour_dates_con'>";
					}
					$count++;
				} ?>
				<?php
				if (mysqli_num_rows($events)<=1) {
				?>
					<div class="tour_dates" style="border:none; background-color:#fff;opacity:0.5;padding: 25px 25px 25px 25px;height: 278px;">
						
						<h3 class="event-title">To Be Announced</h3>
						<h5 class="event-date">&nbsp;</h5>
							<div class="event-button">
								<a href="#book">Book Slot</a>
								<div class="event-button-arrow"></div>
		                    </div>
						
					</div>
				<?php
				}
				?>
				</div>
		 	</div>
		</div>

		<!--<div style="width:100%;max-width:1920px;background-color:#183e70;">
<div class="home-booking">
  
<h3 class="home-booking-h3">Virtual House Tours</h3>

  <div class="home-booking-p">
	  <p>
	  Due to the ongoing Coronavirus Pandemic our popular Open House Events have moved online!
You can now join us from the comfort of your own home, take a seat as we guide you through finished extensions project
designed and built by us. Members of the team who have been involved and the homeowner
join us to explain the process and answer your questions.
	  </p>
  </div>
  <div class="home-booking-button">
<div class="book-button">
			<a href="#">Find out more</a>
			<div class="book-button-arrow"></div>
  </div>
  </div>
</div>
</div>-->
		</section>
		<section class="events-book" id="book">	
		<div class="col_full" id="bookform">
			<h2>To register your interest, please fill in your details below</h2>
			<div class="desktop_version_form">
				<form action="" method="POST" class="validate-form">
					<div class="col_half">
						<div class="form_row">
							<label>Name <span style="color:#f00;">*</span>:</label>
							<input type="text" name="user_name" value="" class="form_input" required="true"/>
						</div>
						<div class="form_row">
							<label>Address:</label>
							<textarea name="address" class="form_textarea" style="height: 144px;"></textarea>
						</div>
						<div class="form_row" style="margin: 23px 0 0 0;">
							<div class="g-recaptcha" data-sitekey="6LdPWR4TAAAAAMChs-OJSL7QUr61otgZ1zAhBOx_"></div>
							<input type="hidden" name="current_date" value="<?php echo date('H:i:s M d, Y'); ?>" readonly>
						</div>


						<div class="form_row" style="margin: 0px;">
							<label>&nbsp;</label>
							<input onclick="ga('send', 'event', 'housetours', 'booking');" type="submit" name="register_details" value="Submit" class="register_details" />
						</div>
					</div>
					<div class="col_half col_last">
						<div class="form_row">
							<label>Contact Number<span style="color:#f00;">*</span>:</label>
							<input type="text" name="contact_number" value="" class="form_input" required="true"/>
						</div>
						<div class="form_row">
							<label>Email Address<span style="color:#f00;">*</span>:</label>
							<input type="email" name="uemail" class="form_input" value="" required="true" />
						</div>
						<div class="form_row">
							<label>Do you require our Design or Build Service ? or Both ?:</label>
							<textarea name="design_build_service" class="form_textarea"></textarea>
						</div>
						<div class="form_row" style="margin-bottom: 30px;">
                        	<div class="needmoreinfo">
                        	<img src="/images/house_tour/more_info.png" alt="">
                            </div>
							<label>Which tour are you interested in attending?</label>
							<input type="text" name="tour_sttending" class="form_input" value="" />
						</div>
                        <div class="broform100 checkboxbro broleft">
                                <input type="checkbox" name="houseSubscribe" value="Yes"><span>Sign up to our newsletter and receive updates on our latest blogs, news items and special offers – see our <a href="https://www.buildteam.com/privacy-policy.html">privacy policy</a> for more information on how we use your personal data.</span>
                            </div>
					</div>

				</form>
				<?php

					/*if(isset($_REQUEST['register_details']))
					{
						  $q = http_build_query(array(
								'secret'    => '6LdPWR4TAAAAAMChs-OJSL7QUr61otgZ1zAhBOx_',
								'response'  => $_POST['g-recaptcha-response'],
								'remoteip'  => $_SERVER['REMOTE_ADDR'],
							));
							$result = json_decode(file_get_contents('https://www.google.com/recaptcha/api/siteverify?'.$q));



							$ch = curl_init();
							curl_setopt($ch, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
							curl_setopt($ch, CURLOPT_HEADER, 0);
							curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
							curl_setopt($ch, CURLOPT_POST, 1);
							curl_setopt($ch, CURLOPT_POSTFIELDS, array(
								'secret'    => '6LdPWR4TAAAAAMChs-OJSL7QUr61otgZ1zAhBOx_',
								'response'  => $_POST['g-recaptcha-response'],
								'remoteip'  => $_SERVER['REMOTE_ADDR'],
							));

							$resp = json_decode(curl_exec($ch));
							curl_close($ch);


							// print_r($q);
							// echo "https://www.google.com/recaptcha/api/siteverify?" . $q;
							if ($resp->success) {
								// Continue processing form data
						*/

						if($_REQUEST['user_name'] == '' || $_REQUEST['contact_number'] == '' || $_REQUEST['uemail'] == '')
						{
							echo '<div class="error_msg">Please Fill Required (*) Fields.</div>';
						}
						else
						{
							$ins = mysqli_query($dbconn,"INSERT INTO house_tour_enquires (uname, uemail, contact, address, design_build_service, tour_sttending, houseSubscribe) VALUES ('".$_REQUEST['user_name']."', '".$_REQUEST['uemail']."', '".$_REQUEST['contact_number']."', '".$_REQUEST['address']."', '".$_REQUEST['design_build_service']."', '".$_REQUEST['tour_sttending']."', '".$_REQUEST['houseSubscribe']."')");
							$to = 'hello@buildteam.com';
							$cto = $_REQUEST['uemail'];
							$subject = 'Booking Form';
							$message = '<div style="background:#fff;width:650%; margin:0 auto; padding:10px;">
									'.$_REQUEST['user_name'].',<br/><br/>
									Would like to attend an upcoming House Tour.<br/><br/>
									<b>Details of the request are as follows:</b><br/></br>
									Name : '.$_REQUEST['user_name'].' <br/>
									Email Address : '.$_REQUEST['uemail'].' <br/>
									Contact Number : '.$_REQUEST['contact_number'].' <br/>
									Design, Build or both : '.$_REQUEST['design_build_service'].' <br/>
									House Tour: '.$_REQUEST['tour_sttending'].' <br/><br/>

									Warm Regards,<br/>
									Build Team
								</div>';
							$message1 = '<div style="background:#fff;width:650%; margin:0 auto; padding:10px;">
									Hi,<br/><br/>
									Your booking request has been recieved and a member of our team will be in touch within the next working day.  <br/><br/>

									Warm Regards<br/> Build Team<br/>
								</div>';

							// Always set content-type when sending HTML email
							$headers = "MIME-Version: 1.0" . "\r\n";
							$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
							$headers .= 'From: Build Team <info@buildteam.com>' . "\r\n";

							$sent = mail($to,$subject,$message,$headers);
							mail($cto,$subject,$message1,$headers);

							if($sent)
							{
								echo '<div class="success_msg">Thank you for submitting your interest, Build Team will contact you shortly.</div>';
							}
						}

					/*}
						else{
							echo '<div class="error_msg">Please prove you are not a robot .</div>';
						}
					}*/
				?>
			</div>
			<div class="mobile_version_form">
				<form action="" method="POST">
					<div class="col_half">
						<div class="form_row">
							<label>Name<span style="color:#f00;">*</span>:</label>
							<input type="text" name="user_name" value="" class="form_input"  required="true" />
						</div>
						<div class="form_row">
							<label>Contact Number<span style="color:#f00;">*</span>:</label>
							<input type="text" name="contact_number" value="" class="form_input" required="true" />
						</div>
						<div class="form_row">
							<label>Address<span style="color:#f00;">*</span>:</label>
							<textarea name="address" class="form_textarea" style="height: 116px;"></textarea>
						</div>
						
						<div class="form_row">
							<label>Email Address<span style="color:#f00;">*</span>:</label>
							<input type="email" name="uemail" class="form_input" value="" required="true" />
						</div>
						<div class="form_row">
							<label>Do you require our Design or Build Service? or Both?:</label>
							<textarea name="design_build_service" class="form_textarea"></textarea>
						</div>
						<div class="form_row">
							<label>Which tour are you interested in attending?</label>
							<input type="text" name="tour_sttending" class="form_input" value="" />
						</div>
						<div class="broform100 checkboxbro broleft">
                                <input type="checkbox" name="houseSubscribe" value="Yes"><span>Sign up to our newsletter and receive updates on our latest blogs, news items and special offers – see our <a href="https://www.buildteam.com/privacy-policy.html">privacy policy</a> for more information on how we use your personal data.</span>
						</div>
						<div class="form_row" style="margin: 23px 0 0 0;">
							<div class="g-recaptcha" data-sitekey="6LdPWR4TAAAAAMChs-OJSL7QUr61otgZ1zAhBOx_"></div>
							<input type="hidden" name="current_date" value="<?php echo date('H:i:s M d, Y'); ?>" readonly>
						</div>
						<div class="form_row" style="margin: 0px;">
							
							<input onclick="ga('send', 'event', 'housetours', 'booking');" type="submit" name="register_details_mob" value="Submit" class="register_details" />
						</div>
					</div>
				</form>
				<?php
					if(isset($_REQUEST['register_details_mob']))
					{
						if($_REQUEST['user_name'] == '' || $_REQUEST['contact_number'] == '' || $_REQUEST['uemail'] == '')
						{
							echo '<div class="error_msg">Please Fill Required (*) Feilds.</div>';
						}
						else
						{
							$ins = mysqli_query($dbconn,"INSERT INTO house_tour_enquires (uname, uemail, contact, address, design_build_service, tour_sttending, houseSubscribe) VALUES ('".$_REQUEST['user_name']."', '".$_REQUEST['uemail']."', '".$_REQUEST['contact_number']."', '".$_REQUEST['address']."', '".$_REQUEST['design_build_service']."', '".$_REQUEST['tour_sttending']."', '".$_REQUEST['houseSubscribe']."')");
							$to = 'hello@buildteam.com,declan@designteam.co.uk';
							$cto = $_REQUEST['uemail'];
							$subject = 'Booking Form';
							$message = '<div style="background:#fff;width:650%; margin:0 auto; padding:10px;">
									'.$_REQUEST['user_name'].',<br/><br/>
									Would like to attend an upcoming House Tour.<br/><br/>
									<b>Details of the request are as follows:</b><br/></br>
									Name : '.$_REQUEST['user_name'].' <br/>
									Email Address : '.$_REQUEST['uemail'].' <br/>
									Contact Number : '.$_REQUEST['contact_number'].' <br/>
									Design, Build or both : '.$_REQUEST['design_build_service'].' <br/>
									House Tour: '.$_REQUEST['tour_sttending'].' <br/><br/>

									Warm Regards,<br/>
									Build Team
								</div>';
							$message1 = '<div style="background:#fff;width:650%; margin:0 auto; padding:10px;">
									Hi,<br/><br/>
									Your booking request has been recieved and a member of our team will be in touch within the next working day.  <br/><br/>

									Warm Regards<br/> Build Team<br/>
								</div>';

							// Always set content-type when sending HTML email
							$headers = "MIME-Version: 1.0" . "\r\n";
							$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
							$headers .= 'From: Build Team <info@buildteam.com>' . "\r\n";

							$sent = mail($to,$subject,$message,$headers);
							mail($cto,$subject,$message1,$headers);

							if($sent)
							{
								echo '<div class="success_msg">Thank you for submitting your interest, Build Team will contact you shortly.</div>';
							}
						}
					}
				?>
			</div>
		</div>
	</section>
	
	<section class="upcoming-dates-bg" style="padding:70px 0" id="previous">
		<div class="upcoming-dates">
		    <div class="col_full">
			<h2 style="border-bottom: 1px solid #183e70;padding-bottom: 20px;">Our Previous Tours</h2>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<iframe width="550" height="325" src="https://www.youtube.com/embed/2tfbH34nHzM" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			<h5>Gellatly Road</h5>
			<p>Our clients Bob & Anna transformed their existing kitchen into a light, bright family room with separate WC/Shower, utility space and study area of around 40m². The rear terrace was paved and features a lean to glazed canopy so that the outdoors can be enjoyed all year round. <br><a href="https://www.buildteam.com/se14-mSQ6xvGDcL-side-return-extension.html">View Gallery HERE.</a></p>
			<!--<div class="playpause"></div>-->
			<div style="height:100px"></div>
			</div>
			<!--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="event-button" style="max-width: 280px;margin: 45px 0 70px 0;">
					<a href="https://build-team.vr-360-tour.com/e/KdQBcm9mnVs/e?hidelive=true">View 3D Tour
					<div class="event-button-arrow"></div></a>
			</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				
			</div>
			
			
			
			<br>-->
			
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<iframe width="550" height="325" src="https://www.youtube.com/embed/efrlQofQgFo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			<h5>Thorpedale Road</h5>
			<p>Our clients Caspar & Kelly decided to extend their 2 bedroom flat in East
Dulwich SE22 to accommodate their growing family.<br>
The property was extended to the rear and side, with a third bedroom
incorporated within the side return. <br><a href="https://www.buildteam.com/n4-KVvkDHLmns-side-return-extension.html">View Gallery HERE.</a></p>
			<!--<video class="video" controls poster="../housetour/video_poster.png"><source src="../housetour/BuildTeam_House_Tour No_Strap.mp4" type="video/mp4"> </video>
			<div class="playpause"></div>-->
			<div style="height:100px"></div>
			</div>
			<!--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="event-button" style="max-width: 280px;margin: 45px 0 70px 0;">
					<a href="https://build-team.vr-360-tour.com/e/Mx8Bc7jqfBk/e">View 3D Tour
					<div class="event-button-arrow"></div></a>
			</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				
			</div>
			<br>-->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<iframe width="550" height="325" src="https://www.youtube.com/embed/xjq1yJ6HZwk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			<h5>East Dulwich Road</h5>
			<p>Our clients Caspar & Kelly decided to extend their 2 bedroom flat in East
Dulwich SE22 to accommodate their growing family.<br>
The property was extended to the rear and side, with a third bedroom
incorporated within the side return. <br><a href="#book">Book Today</a></p>
			<!--<div class="playpause"></div>-->
			<div style="height:100px"></div>
			</div>
			<!--<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="event-button" style="max-width: 280px;margin: 45px 0 70px 0;">
					<a href="https://build-team.vr-360-tour.com/e/KdQBdgCZBZ8/e">View 3D Tour
					<div class="event-button-arrow"></div></a>
			</div>
			</div>
			<br>-->
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-6">
			<img src="/images_new_design/video_blank.jpg">
			<h5>Coming Soon</h5>
			<p></p>
			<!--<video class="video" controls poster="../housetour/video_poster.png"><source src="../housetour/BuildTeam_House_Tour No_Strap.mp4" type="video/mp4"> </video>
			<div class="playpause"></div>-->
			<div style="height:100px"></div>
			</div>
		 	</div>
		</div>
	</section>
	<?php
        require_once('explore-our-site.php');
    ?>

 <script>
 $('.video').parent().click(function () {
    if($(this).children(".video").get(0).paused){
        $(this).children(".video").get(0).play();
        $(this).children(".playpause").fadeOut();
    }else{
       $(this).children(".video").get(0).pause();
        $(this).children(".playpause").fadeIn();
    }
});
 </script>
 <script>
 $(".bookbutton a").click(function(){
	 var getarea = $(this).parents(".tour_dates").find(".details_box").children("span").html();
	 $("#bookform input[name=tour_sttending]").val(getarea);
	 $(".needmoreinfo").fadeIn(2000);
	 //alert(getarea);
	// $(window).scrollTop($('#bookform').offset().top);
	 $("html, body").animate({
        scrollTop: $('#bookform').offset().top - 100
    }, 1000);

	 });
 </script>
<script>
$('.carousel').carousel();
</script>
<?php require_once('./inc/footer.inc.php'); ?>