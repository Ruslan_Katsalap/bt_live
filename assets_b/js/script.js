$(document).on("click", ".faq__item", function(e){
	var _that = $(this)
	_that.toggleClass("faq_open")
	_that.find(".faq_answer").slideToggle(function(){
		if(_that.hasClass("faq_open")){
			_that.find("img").attr("src", "/assets_b/img/faq_open_arrow.png")
		} else {
			_that.find("img").attr("src", "/assets_b/img/faq_arrow.png")
		}
	})
})

$(document).on("submit", '.check_form', function(e){
	e.preventDefault()
	$("#checkout-button").trigger("click")
	
})

function prepurchase_checkout(session_id){
	var data = {}
	$('.check_form input[type=text], .check_form input[type=email]').each(function(){
		data[$(this).attr("name")] = $(this).val()
	})
	data["session_id"] = session_id

	console.table(data)

	$.ajax({
		type: "GET",
		url: "/api/pp-save.php",
		data: data,
		success: function(data){
			// var data = JSON.parse(data)
			// if(data.result){
				
			// }
			
		}
	});
}


/***********
* BOOKING
************/

var book_holidays = ["2021-4-2", "2021-4-4", "2021-4-5", "2021-5-3", "2021-5-31", "2021-6-20", "2021-6-21", "2021-6-30", "2021-7-12", "2021-7-18", "2021-8-2", "2021-9-22","2021-11-30", "2021-12-21","2021-12-24","2021-12-25","2021-12-27","2021-12-26","2021-12-28","2021-12-31"]

var book_date = "";
var book_slot = "";


//slot click 
$(document).on("click", ".time_light", function(e){
	e.preventDefault()
	var _that = $(this)
	$('.time_light').removeClass('time_selected')
	_that.addClass("time_selected")
})

//exten 2 sreen click
$(document).on("click", '.exten_item', function(e){
	e.preventDefault()
	$('.exten_item').removeClass("exten_selected")
	var _that = $(this)
	_that.addClass("exten_selected")
})

//confirm booking 
$(document).on("submit", '.book_form', function(e){
	e.preventDefault()

	if(!$('.exten_selected').length){
		$('.book_exten_err').show()
		setTimeout(function(){
			$('.book_exten_err').hide()
		},3000)

		return
	}

	if($('.start_select').val() == null || $('.status_select').val() == null) return



	var data = {}
	$("input[type=text], input[type='email']").not("input[name=_date]").each(function(){
		var _that = $(this)
		data[_that.attr("name")] = _that.val()
	})

	var contact = $("input[name=contact_type]:checked").val()
	data["contact"] = contact

	data["start"] = $('.start_select').val()
	data["status"] = $('select[name=status]').val()
	data["exten_type"] = $('.exten_selected').data("val")

	if($("#book_file").val()){ //upload file
		//upload file here
	}

	if($('input[name=events]').is(":checked")){
		data["events"] = "1"
	} else {
		data["events"] = "0"
	}

	//from 1 screen data
	data["date"] = book_date
	data["slot"] = book_slot

	//update slots global var
	//slots_json[1]["12:30"] = --slots_json[1]["12:30"]
		data["slot_setting"] = update_slot_availability(book_slot, slots_json)
	if(slot_id == ""){ //update or add slots settings
		data["slot_status"] = "add"
		data["slot_id"] = slot_id
	} else {
		data["slot_status"] = "update"
		data["slot_id"] = slot_id
	}

	//ics file
	var ics_filename = "";
	var ics_admin_filename = ""

	//create ics file
	$.ajax({
		type: "POST",
		url: "/api/download-ics.php",
		data: {"date_start": book_date+" "+book_slot, 
				"req_type":data["contact"],
				"name":data["f_name"],
				"address":data["address"],
				"postcode":data["postcode"],
				"link":data["link"],
		},
		async: false,
		success: function(data){
			var data = JSON.parse(data)
			ics_filename = data["ics_filename"]
			ics_admin_filename = data["ics_admin_filename"]
			
		}
	});

	data["ics_filename"] = ics_filename;
	data["ics_admin_filename"] = ics_admin_filename

	console.table(data)
	
	

	//file uploading
	//upload file
	if($('#book_file').val() !== ""){
		var file_data = $('#book_file').prop('files')[0];   
		var form_data = new FormData()  
		var uniqid =  Math.random().toString(16).slice(2)
		var fileformat = $('#book_file').val().split('.').pop().toLowerCase()
		var filename = uniqid +"."+ fileformat

		form_data.append('file', file_data)
		form_data.append('filename', uniqid)

		
		data["file"] = filename

		var preloader_before = $('.book_2_content .book_control_btns').html()
		var prel = '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>'

		$.ajax({
		    url: '/api/book_upload_file.php',
		    cache: false,
		    contentType: false,
		    processData: false,
		    data: form_data,                         
		    type: 'POST',
			async: false,
			beforeSend: function(){
				$('.book_2_content .book_control_btns').html(prel)
			},
		    success: function(res){
		    	save_book(data)
		    }
		});		
	} else {
		save_book(data)
	}

})

function save_book(data){
	var preloader_before = $('.book_2_content .book_control_btns').html()
	var prel = '<div class="lds-ring"><div></div><div></div><div></div><div></div></div>'
	//send data
	$.ajax({
		type: "GET",
		url: "/api/save_book.php",
		data: data,
		beforeSend: function(){
				$('.book_2_content .book_control_btns').html(prel)
		},
		success: function(data){
			var data = JSON.parse(data)
			if(data.result){
				//success screen
				$('.book_2_content').hide()
				$('.book_3_content').show()
				$('.book_tab').removeClass("book_tab_active")
				$('.book_tab_3').addClass("book_tab_active")
				$('.book_tab_2').addClass("book_tab_completed")
			}
			
		}
	});
}

function update_slot_availability(book_slot, data){
	//slots_json[1]["12:30"] = --slots_json[1]["12:30"]
	var json = ""
	data.forEach(function(el, index, arr){
		
		for (slot in el) {
			if(slot == book_slot){
				data[index][slot] = --data[index][slot]
				json = JSON.stringify(data)
				break;
			}
		}
	})
	//console.log(json)
	return json
}

//next btn click
$(document).on("click", ".control_next_btn", function(e){
	e.preventDefault()
	if(!$('.time_selected').length){
		$('.book_slot_err').show()
		setTimeout(function(){
			$('.book_err').hide()
		}, 3000)

		return
	}

	$('.book_1_content').hide()
	$('.book_2_content').show()
	$('.book_tab').removeClass("book_tab_active")
	$('.book_tab_2').addClass("book_tab_active")
	$('.book_tab_1').addClass("book_tab_completed")
	book_date = $("#datepicker").val()
	book_slot = $('.time_selected').text()
	// console.table({
	// 	date : book_date,
	// 	slot : book_slot
	// })
})

//back btn click
$(document).on("click", '.ctrl_back_btn', function(e){
	e.preventDefault()
	$('.book_err').hide()
	$('.book_2_content').hide()
	$('.book_1_content').show()
	$('.book_tab').removeClass("book_tab_active book_tab_completed")
	$('.book_tab_1').addClass("book_tab_active")
})

//booking upload file onchange
$(document).on("change", '#book_file', function(e){
	e.preventDefault()
	var filename = $(this).val().split('\\').pop();
	$('.book_filename').html(truncate(filename))
})

function truncate(input) {
   if (input.length > 20)
      return input.substring(0,20) + '...';
   else
      return input;
};



var slot_id = ""
var slot_reset = [
	{"9:00": 1, "9:30": 1, "10:00": 1, "10:30": 1, "11:00": 1, "11:30": 1},
	{"12:00": 1, "12:30": 1, "13:00": 1, "13:30": 1, "14:00": 1, "14:30": 1},
	{"15:00": 1, "15:30": 1, "16:00": 1, "16:30": 1, "17:00": 1, "17:30": 1,"18:00":1, "18:30":1}
] //default values of slots
var slots_json = slot_reset







//upload slots
function upload_slots(){
	// console.log("12:30",slots_json[1]["12:30"])
	// slots_json[1]["12:30"] = --slots_json[1]["12:30"]
	// console.log(slots_json)
	var slot_date = $('#datepicker').val()
	$.ajax({
		type: "GET",
		url: "/api/dc_c_get_slots.php",
		data: {"slot_date": slot_date},
		async: false,
		success: function(data){
			var data = JSON.parse(data)
			if(data.result){
				slots_json = JSON.parse(data["data"])
				slot_id = data["slot_id"]

				//console.log("data",slots_json)
				//console.log("slot id",slot_id)
			} else {
				slots_json = slot_reset
				slot_id = ""
			}
			
		}
	});


	var slots = ""
	var slot_1 = create_slot_elem(slots_json[0])
	var slot_2 = create_slot_elem(slots_json[1])
	var slot_3 = create_slot_elem(slots_json[2])
	$('.book_slot_1').html(slot_1)
	$('.book_slot_2').html(slot_2)
	$('.book_slot_3').html(slot_3)
}

function create_slot_elem(slots){
	var slots_list = ""
	var slot_elem = ""
	var slot_clss = ""
	for(slot_el in slots){
		if(slots[slot_el] != 0) slot_clss = "time_light"
			else slot_clss = ""
		slot_elem = '<span class="book_time_elem '+slot_clss+'">'+slot_el+'</span>'
		slots_list += slot_elem
	}
	return slots_list
}

//datapicker

$(document).on("click", '.nex_day_btn', function () {
    var date = $('#datepicker').datepicker('getDate');

  
    date.setTime(date.getTime() + (1000*60*60*24))
    $('#datepicker').datepicker("setDate", date);
    var dayOfWeek = date.getUTCDay();
    var date_for_hol =  date.getFullYear()+"-"+(date.getMonth()+1) + "-"+date.getDate();
    //console.log('cal_date',date_for_hol)
    //console.log('cal_date',dayOfWeek)

    upload_slots()

    if(dayOfWeek == 5 || dayOfWeek == 6 || (book_holidays.indexOf(date_for_hol) != -1) ){
    	//date.setTime(date.getTime() + (1000*60*60*24))
    	//$('#datepicker').datepicker("setDate", date);
    	$('.nex_day_btn').trigger("click")
    }
    

    //var disabled = $('#datepicker').datepicker('isDisabledDate', date);
    //console.log(disabled)

    
});

$(document).on("click", '.prev_day_btn', function () {
    var date = $('#datepicker').datepicker('getDate');
    date.setTime(date.getTime() - (1000*60*60*24))
    $('#datepicker').datepicker("setDate", date);
    var dayOfWeek = date.getUTCDay();
    var date_for_hol =  date.getFullYear()+"-"+(date.getMonth()+1) + "-"+date.getDate();
    upload_slots()

    if(dayOfWeek == 5 || dayOfWeek == 6 || (book_holidays.indexOf(date_for_hol) != -1) ){
    	//date.setTime(date.getTime() + (1000*60*60*24))
    	//$('#datepicker').datepicker("setDate", date);
    	$('.prev_day_btn').trigger("click")
    }
});
