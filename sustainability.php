<?php

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    	<?php
			
			echo $bc_trail;
			
			?>
    	<h1>Sustainable</h1>
      <p>Build Team are passionate about sustainability for three reasons:</p>
      <ol>
        <li>We love London, and want to do our bit to conserve the environment in which we operate</li>
        <li>Being  green costs us less &ndash; in fuel, waste disposal and material costs -  which in turn means we&rsquo;re able to pass on the benefit to our customers</li>
        <li>Operating  in a sustainable manner makes us more efficient &ndash; we get to travel  around London quicker by bike and public transport, materials arrive  direct to site, and waste is collected as it is removed by our network  of recycle agents.</li>
      </ol>
      <p>Build Team have a clear policy to minimise our impact on the environment:</p>
      <p><b>How we Travel:</b> We  are attempting to get rid of as much as possible the gas guzzling vans  which are so common amongst trades people. The project managers cycle  as much as possible and we have a small fleet of Build Team branded  bikes. If distance is too far we have a scooter which is shared.  Obviously in the building trade there will be a need for vans and we  have on order two LPG vans which will decrease our fuel consumption and  carbon emissions. We are also incentivising our workforce to use public  transport with oyster card top ups.</p>
      <p><b>Our use of Materials:</b> We try to use only water based paint as opposed to oil as the disposal  of the oil paint is very messy and a virulent polluter. Fashionable  solid wood flooring can be purchased from sustainable sources in most  cases and we always favour these. And finally, to cut down on vehicle  movements, we get all of materials (including the screws!) delivered  direct to site. This also makes us more productive, and means our  trades people get more done in a day.</p>
      <p><b>How we Dispose of Waste:</b> We see this as potentially the most important of the three. We try to  reduce landfill as much as possible, recycling our scrap metal and  reusing as much building material as possible which with a little work  is easily salvageable. We have also found with our association with a  London charity worker that we are able to find homes for kitchen  cabinets and assorted appliances which may have found their way to the  tip.</p>
		</div>
<?php

require_once('./inc/footer.inc.php');

?>