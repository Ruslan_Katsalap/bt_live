<?php

require_once('./inc/header.inc.php');
?>
<style type="text/css">
.star-wrapper{
    font-size: 15px;
    display:inline-block;
	padding-top:0;
}
.compare-table tr td{
    position: relative;
}
.text-center{
	text-align:center;
}
</style>


	<h1>service level agreement</h1>
	<p>Client satisfaction is our top priority, and to achieve this we feel it's important to keep an honest path of communication between our Teams and 
	our Clients. For this reason, we are proud to offer you our Service Level Agreement, which shows what our targets are and where we are currently running 
	in comparison to those targets. We will always do what we can to beat our targets, and we hope our current run rates will show you how dedicated we are 
	to ensuring complete client satisfaction throughout both the Design and the Build process. </p>
	
	<br/><br/>
	<div class="col_two_third">
		 <table class="compare-table">
			<thead>
				<tr>
					<th class="compare-first text-center">Service</th>
					<th class="compare-second text-center">Target</th>
					<th class="compare-third text-center">Current</th>
				</tr>
			</thead>
				<tbody>
						<tr class="odd">
							<td>Percentage of planning applications that are granted
								<p class="star-wrapper">*
								</p>
							</td>
							<td class="text-center">80%</td>
							<td class="text-center">86%</td>
						</tr>  
					   <tr class="even">
							<td>Number of days between initial meeting and submitting design scheme to the council</td>
							<td class="text-center">35</td>
							<td class="text-center">27.5</td>
						</tr>  
						<tr class="odd">
							<td>
								Number of days in the Design Phase   
								<p class="star-wrapper">**
								</p>
							</td>
							<td class="text-center">84</td>
							<td class="text-center">76</td>
						</tr>  
						<tr class="even">
							<td>
								Percentage difference between initial quote and
								final agreed contract sum 
								<p class="star-wrapper">***
									
								</p>
							</td>
							<td class="text-center">+ 15%</td>
							<td class="text-center">+ 7%</td>
						</tr>  
						<tr class="odd">
							<td>Response time to client queries </td>
							<td class="text-center">24 hrs</td>
							<td class="text-center">3.5 hrs</td>
						</tr>  
						<tr class="even">
							<td>
								Number of hours between initial site visit and
								receiving the quote 
							</td>
							<td class="text-center">24 hrs</td>
							<td class="text-center">9 hrs</td>
						</tr>  
						<tr class="odd">
							<td>Turnaround time for drawing amendments </td>
							<td class="text-center">72 hrs</td>
							<td class="text-center">48 hrs</td>
						</tr>  
				</tbody>
         </table>
<br/>
		 <span><sup>*</sup>based on clients who took our advice during the application process.</span><br/>
		 <span><sup>**</sup>this includes 8 week planning determination period.</span><br/>
		 <span><sup>***</sup>based on clients who did not make significant changes to their scheme.</span><br/>
	</div>
	<div class="col_one_third col_last">
		<span>Last Updated:</span><br/>
		<span>9:00, <?php echo date("M d Y", strtotime( "previous monday" ));?>
		</span>
	</div>
 


<?php

require_once('./inc/footer.inc.php');

?>