<?php
header("");
require_once('./inc/header.inc.php');
?>

<style type="text/css">
    body {
        /*background:url(//www.buildteam.com/images/contact/new_sv_bg.jpg) no-repeat center;*/
        background-size: cover;
    }

    .darkblue-box,
    .lightblue-box {
        width: 115px;
        margin-right: 10px;
    }

    .top_part {
        background: #fff;
    }



    .main {
        background: rgba(255, 255, 255, 1);
        padding-left: 50px !important;
        padding-right: 50px !important;
        box-sizing: border-box !important;
        padding-bottom: 50px !important;
        padding: 100px 50px 100px 50px !important;
        max-width: 1080px !important;
    }


    body {
        background-attachment: fixed;
        background-size: cover;
        text-align: left !important;
    }

    footer.round-2 {
        margin-top: 0px;
    }

    .page-title {
        font-size: 30px;
        text-align: left;
        font-weight: bold;
        color: #0f4778;
        border-bottom: 1px solid #d7d7d7;
        padding-bottom: 11px;
        margin-bottom: 0px;
    }

    p {
        text-align: left;
        font-size: 16px;
        font-weight: 400;
        text-align: justify;
        color: #7b7b7b;
        //font-size: 20.5px;
        line-height: 24px;
        font-family: roboto !important;
    }

    h3 {
        color: #4b759a !important;
        text-align: left;
        font-weight: 400 !important;
    }

    input#postcode {
        padding: 7px;
        margin-top: 10px;
        width: 210px;
        border: 2px solid #d5d5d5;
    }

    .book-a-site-table {
        margin-top: 30px;
    }

    .options-content {
        visibility: visible;
        max-width: 100% !important;
        margin-top: 20px;
        margin-bottom: 0px;
    }

    .book-a-site-table th {
        padding: 8px 0px;
        font-size: 21px;
        border-top: 2px solid #d7d7d7;
        //border-bottom: 1px solid #d7d7d7;
        color: #4b759a;
        font-weight: normal;
        text-align: inherit;
    }

    th {
        text-align: inherit;
    }


    .book-a-site-table td {
        padding: 8px 0px;
        font-size: 19px;
        color: #7d7d7d;
        font-weight: normal;
    }

    .tick {
        background-image: url(../images/tick.png);
        background-position: center;
        background-repeat: no-repeat;

    }

    .tick-star {
        background-image: url(../images/tick-star.png);
        background-position: center;
        background-repeat: no-repeat;

    }

    tr {
        border-top: 1px solid #e0e0e0;
    }

    .postcode {
        display: inline;
        padding-left: 7px;
        text-transform: uppercase;
    }

    .up_down {
        display: none;
    }

    .kaction {
        opacity: 1;
    }

    .post-continue {
        cursor: pointer;
        font-size: 20px;
        color: #ff8f4f;
        padding-top: 9px;
        font-weight: normal;
    }


    .foot-note {
        width: 200px;
        height: 80px;
        position: absolute;
        background: url(../images/foot-note.png);
        right: -218px;
        bottom: 15px;
        display: none;
    }

    #termstext {
        font-size: 14px;
    }

    @media only screen and (max-width: 480px) {
        .main {
            padding-left: 15px !important;
            padding-right: 15px !important;
            max-width: 1080px;
            width: 100%;
        }

        .full p {
            margin: 0 0 10px;
            font-size: 12px;
            line-height: inherit !important;
        }

        .full h3 {
            font-size: 17px;
        }

        .book-a-site-table th {
            font-size: 11px;
        }

        .book-a-site-table td {
            font-size: 10px;
            max-width: 140px;
        }

        #termstext {
            font-size: 10px;
        }
    }

</style>

<div class="full">
    <h2 class="page-title">Meet With Us</h2>
    <?php
    $ns = getRs("SELECT * FROM page WHERE page_id=52 AND is_enabled=1 limit 1");
    if ($nw = mysqli_fetch_assoc($ns)) {
        $page_content = $nw['page_content'];
        echo $page_content;
    } ?>
    <!--<h3>Enter your postcode to find out what site visits are available in your area:</h3>
<input type="text" id="postcode" placeholder="E.g. CR0 9JJ..." minlength="6" maxlength="8">
<br>
<div class="post-continue">Continue</div>-->
    <div class="options-content"></div>

    <div class="site-table-box" style="position:relative">
        <table class="book-a-site-table">
            <tr>
                <th>
                    <font color="#0f4778"><b>What's included?</b>
                        <div class="postcode"></div>
                    </font>
                </th>
                <th class="kaction">
                    <center>Consultation</center>
                </th>
                <th class="kaction premium">
                    <center>Premium</center>
                </th>
                <th class="kaction">
                    <center>Concept</center>
                </th>
                <th class="kaction">
                    <center>3D Concept</center>
                </th>
            </tr>

            <tr>
                <td>Discussion about the project brief</td>
                <td class="tick kaction"></td>
                <td class="tick kaction premium"></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>
            <tr>
                <td>Design fee proposal</td>
                <td class="tick kaction"></td>
                <td class="tick kaction premium"></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>

            <tr>
                <td>Build Quotation</td>
                <td class="tick kaction"></td>
                <td class="tick kaction premium"></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>

            <tr>
                <td>On site survey of proposed floor area</td>
                <td></td>
                <td class="tick kaction premium"></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>

            <tr>
                <td>Cost advice and optimising your budget</td>
                <td></td>
                <td class="tick kaction premium"></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>

            <tr>
                <td>Specialist planning advice</td>
                <td></td>
                <td class="tick kaction premium"></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>

            <tr>
                <td>Party Wall advice and guidance</td>
                <td></td>
                <td class="tick kaction premium"></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>

            <tr>
                <td>Creative design ideas</td>
                <td></td>
                <td></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>

            <tr>
                <td>Follow up meeting & plans</td>
                <td></td>
                <td></td>
                <td class="tick kaction"></td>
                <td class="tick kaction"></td>
            </tr>

            <tr>
                <td>3D Visuals</td>
                <td></td>
                <td></td>
                <td></td>
                <td class="tick kaction"></td>
            </tr>
            <tr>
                <td>
                    <font color="#4b759a">Cost</font>
                </td>
                <td class="kaction">
                    <center>
                        <font color="#4b759a">FREE</font>
                    </center>
                </td>
                <td class="kaction premium">
                    <center>
                        <font color="#4b759a">£150+VAT<span style="color:#f5641e;">*</span></font>
                    </center>
                </td>
                <td class="kaction">
                    <center>
                        <font color="#4b759a">£200+VAT</font>
                    </center>
                </td>
                <td class="kaction">
                    <center>
                        <font color="#4b759a">£250+VAT</font>
                    </center>
                </td>
            </tr>

            <tr style="border-bottom: 1px solid #e0e0e0;">
                <td>
                    <font id="termstext"><span style="color:#f5641e;">*</span>£150 + VAT fully refundable within 30days
                        on Design instruction</font>
                </td>

                <td class="kaction">
                    <center>
                        <font color="#ff5d20">
                            <form action="/book-design-consultation.html" id="standard" method="post"><input
                                    type="hidden" name="dateFrom"
                                    value="<?= $_POST['dateFrom'] ?>" /><input
                                    type="hidden" name="dateTo"
                                    value="<?= $_POST['dateTo'] ?>" /><a
                                    href="#" style="color:#ff8641; display:block !important;"
                                    onclick="document.getElementById('standard').submit();" class="compare-link">Book
                                    now</a></form>
                        </font>
                    </center>
                </td>

                <td class="kaction premium">
                    <center>
                        <font color="#ff5d20">
                            <form action="/book-design-consultation.html" id="premium" method="post"><input type="hidden"
                                    name="dateFrom"
                                    value="<?= $_POST['dateFrom'] ?>" /><input
                                    type="hidden" name="dateTo"
                                    value="<?= $_POST['dateTo'] ?>" /><a
                                    href="#" style="color:#ff8641; display:block !important;" class="compare-link"
                                    onclick="document.getElementById('premium').submit();">Book now</a></form>
                        </font>
                    </center>
                </td>

                <td class="kaction">
                    <center>
                        <font color="#ff5d20">
                            <form action="/book-design-consultation.html" id="concept" method="post"><input type="hidden"
                                    name="dateFrom"
                                    value="<?= $_POST['dateFrom'] ?>" /><input
                                    type="hidden" name="dateTo"
                                    value="<?= $_POST['dateTo'] ?>" /><a
                                    href="#" style="color:#ff8641; display:block !important;" class="compare-link"
                                    onclick="document.getElementById('concept').submit();">Book now</a></form>
                        </font>
                    </center>
                </td>

                <td class="kaction">
                    <center>
                        <font color="#ff5d20">
                            <form action="/book-design-consultation.html" id="3dconcept" method="post"><input
                                    type="hidden" name="dateFrom"
                                    value="<?= $_POST['dateFrom'] ?>" /><input
                                    type="hidden" name="dateTo"
                                    value="<?= $_POST['dateTo'] ?>" /><a
                                    href="#" style="color:#ff8641; display:block !important;" class="compare-link"
                                    onclick="document.getElementById('3dconcept').submit();">Book now</a></form>
                        </font>
                    </center>
                </td>
            </tr>


        </table>
        <div class="foot-note"></div>
    </div>










    <script>
        $(document).ready(function() {

            var twoChars, threeChars, fourChars, london2;

            /* All Pincodes */
            var all = ["WD6", "EN5", "EN4", "EN2", "EN1", "EN3", "E4", "N9", "N21", "N14", "N13", "N18", "E17",
                "E18", "E10", "E11", "E15", "E7", "E12", "E13", "E6",
                "E16", "SE28", "SE18", "SE2", "SE9", "DA16", "DA15", "BR7", "BR1", "BR2", "BR4", "BR3",
                "CR0", "SE25", "CR7", "CR4", "SM6", "SM5", "SM1",
                "SM4", "SM3", "KT4", "KT4", "KT5", "KT1", "KT2", "TW11", "TW12", "TW2", "TW10", "TW1",
                "TW9", "TW8", "TW7", "TW3", "TW4", "TW5", "UB2",
                "UB1", "UB5", "UB6", "HA0", "HA9", "HA2", "HA1", "HA3", "NW9", "NW4", "N3", "NW7", "N12",
                "N11", "N20", "HA8", "HA7"
            ];

            var inM25 = ['SW20', 'SW19', 'SW17', 'SW16', 'SE19', 'SE20', 'SE26', 'SE6', 'SE12', 'SE3', 'SE7',
                'SE13', 'SE23', 'SE27', 'SE21', 'SE22',
                'SE4', 'SE14', 'SE8', 'SE10', 'E14', 'E3', 'E9', 'SE16', 'SE15', 'SE24', 'SW2', 'SW12',
                'SW4', 'SW9', 'SE5', 'SW18', 'SW15', 'SW14', 'SW13',
                'SW6', 'SW11', 'SW8', 'SW1', 'SE11', 'SE17', 'SE1', 'EC3', 'EC2', 'E1', 'E2', 'E8', 'E5',
                'N16', 'N15', 'N17', 'N22', 'N8', 'N4', 'N5', 'N1', 'EC1',
                'EC4', 'WC2', 'WC1', 'NW1', 'W1', 'W2', 'SW7', 'SW3', 'W14', 'W6', 'W4', 'W3', 'W5', 'W13',
                'W7', 'NW10', 'NW2', 'NW11', 'N2', 'N10', 'N6',
                'N19', 'N7', 'NW5', 'NW3', 'NW6', 'NW8', 'W9', 'W10', 'W12', 'W11', 'W8', 'SW10', 'SW5'
            ];

            var outLondon1 = ["AB", "AL", "BA", "BB", "BD", "BH", "BL", "BN", "BS", "CA", "CF", "CH", "CM",
                "CO", "CT", "CV", "CW", "DE", "DG",
                "DH", "DL", "DN", "DT", "DY", "EH", "EX", "FK", "FY", "GL", "GU", "HD", "HG", "HP", "HR",
                "HU", "HX", "IV", "KA", "KW", "KY",
                "LA", "LD", "LE", "LL", "LN", "LS", "LU", "ME", "MK", "ML", "NE", "NG", "NN", "NP", "NR",
                "OL", "OX", "PA", "PE", "PH", "PL", "PO",
                "PR", "RG", "RH", "SA", "SG", "SK", "SL", "SN", "SO", "SP", "SR", "SS", "ST", "SY", "TA",
                "TD", "TF", "TN", "TQ", "TR", "TS",
                "WA", "WF", "WN", "WR", "WS", "WV", "YO"
            ];

            var outLondon2 = ["G1", "G2", "G3", "G4", "G5", "G9", "G11", "G12", "G13", "G14", "G15", "G20",
                "G21", "G22", "G23",
                "G31", "G32", "G33", "G34", "G40", "G41", "G42", "G43", "G44", "G45", "G46", "G51", "G52",
                "G53", "G58", "G60", "G61", "G62", "G63", "G64",
                "G65", "G66", "G67", "G68", "G69", "G70", "G71", "G72", "G73", "G74", "G75", "G76", "G77",
                "G78", "G79", "G81", "G82", "G83", "G84", "S1", "S2",
                "S3", "S4", "S5", "S6", "S7", "S8", "S9", "S10", "S11", "S12", "S13", "S14", "S17", "S18",
                "Stw2 6hg19", "S20", "S21", "S25", "S26", "S30", "S31", "S32", "S33",
                "S35", "S36", "S40", "S41", "S42", "S43", "S44", "S45", "S49", "S60", "S61", "S62", "S63",
                "S64", "S65", "S66", "S70", "S71", "S72", "S73", "S74",
                "S75", "S80", "S81", "M1", "M2", "M3", "M3", "M4", "M5", "M6", "M7", "M8", "M9", "M11",
                "M12", "M13", "M14", "M15", "M16", "M17", "M18", "M19", "M20",
                "M21", "M22", "M23", "M24", "M25", "M26", "M27", "M28", "M29", "M30", "M31", "M32", "M33",
                "M34", "M35", "M38", "M40", "M41", "M43", "M44", "M45",
                "M46", "M50", "L1", "L2", "L3", "L4", "L5", "L6", "L7", "L8", "L9", "L10", "L11", "L12",
                "L13", "L14", "L15", "L16", "L17", "L18", "L19", "L20", "L20", "L21", "L22",
                "L23", "L24", "L25", "L26", "L27", "L28", "L29", "L30", "L31", "L32", "L33", "L34", "L35",
                "L36", "L37", "L38", "L39", "L40", "B1", "B2", "B3", "B4", "B5",
                "B6", "B7", "B8", "B9", "B10", "B11", "B12", "B13", "B14", "B15", "B16", "B17", "B18",
                "B19", "B20", "B21", "B23", "B24", "B25", "B26", "B27", "B28", "B29",
                "B30", "B31", "B32", "B33", "B34", "B35", "B36", "B37", "B38", "B40", "B42", "B43", "B44",
                "B45", "B46", "B47", "B48", "B49", "B50", "B60", "B61",
                "B62", "B63", "B64", "B65", "B66", "B67", "B68", "B69", "B70", "B71", "B72", "B73", "B74",
                "B75", "B76", "B77", "B78", "B79", "B80", "B90", "B91",
                "B92", "B93", "B94", "B95", "B96", "B97", "B98"
            ];

            /* Disabling the submit button if length is less than 5 */


            $('.post-continue').click(function(e) {


                var fieldValue = $('#postcode').val();

                if (fieldValue.length < 1) {
                    $('.kaction').css("opacity", "0.5");
                    $('.premium').css("opacity", "0.5");
                    $('.options-content').html('');
                }

                $('.postcode').html(fieldValue);

                clickMe();

            });



            function clickMe() {

                var pincode = $('#postcode').val().toUpperCase();

                switch (pincode.length) {
                    case 6:
                        twoChars = pincode.substr(0, 2);
                        if ($.inArray(twoChars, all) >= 0) {
                            checkForAll();
                        } else if ($.inArray(twoChars, inM25) >= 0) {
                            sameText();
                        } else if ($.inArray(twoChars, outLondon2) >= 0) {
                            outsideLondon();
                        } else if (($.inArray(twoChars, inM25) == -1) && ($.inArray(twoChars, all) == -1) && ($
                                .inArray(twoChars, outLondon2) == -1)) {
                            outsideEngland();
                        }
                        break;
                    case 7:
                        threeChars = pincode.substr(0, 3);
                        london2 = pincode.substr(0, 2);
                        if ($.inArray(threeChars, all) >= 0) {
                            checkForAll();
                        } else if ($.inArray(threeChars, inM25) >= 0) {
                            sameText();
                        } else if ($.inArray(london2, outLondon1) >= 0) {
                            outsideLondon();
                        } else if ($.inArray(threeChars, outLondon2) >= 0) {
                            outsideLondon();
                        } else if (($.inArray(threeChars, inM25) == -1) && ($.inArray(threeChars, all) == -1) &&
                            ($.inArray(threeChars, outLondon1) == -1) && ($.inArray(threeChars, outLondon2) == -
                                1)) {
                            outsideEngland();
                        }
                        break;
                    case 8:
                        fourChars = pincode.substr(0, 4);
                        london2 = pincode.substr(0, 2);
                        if ($.inArray(fourChars, all) >= 0) {
                            checkForAll();
                        } else if ($.inArray(fourChars, inM25) >= 0) {
                            sameText();
                        } else if ($.inArray(london2, outLondon1) >= 0) {
                            outsideLondon();
                        } else if ($.inArray(fourChars, outLondon2) >= 0) {
                            outsideLondon();
                        } else if (($.inArray(fourChars, inM25) == -1) && ($.inArray(fourChars, all) == -1) && (
                                $.inArray(fourChars, outLondon1) == -1) && ($.inArray(fourChars, outLondon2) ==
                                -1)) {
                            outsideEngland();
                        }
                        break;
                }

            }


            //$('.kaction form a').hide();
            //$('.premium form a').hide();


            function outsideLondon() {

                //$('.options-content').html('As you are outside of our coverage area, we wouldn\'t be able to assist at this stage. '+' <a href="//www.buildteam.com/contact.html">Contact Us</a> if anything is unclear or if you have any questions. ').css({'visibility':'visible', 'max-width':'90%'});
                //$('.kaction').css("opacity","0.5");
                ///$('.premium').css("opacity","0.5");
                //$('.kaction form a').hide();
                //$('.premium form a').hide();
                $('.flag').unbind('click');

            }

            function outsideEngland() {


                //$('.options-content').html('As you are outside of our coverage area, we wouldn\'t be able to assist at this stage. '+' <a href="//www.buildteam.com/contact.html">Contact Us</a> if anything is unclear or if you have any questions. ').css({'visibility':'visible', 'max-width':'90%'});

                //$('.kaction').css("opacity","0.5");
                //$('.premium').css("opacity","0.5");

                $('.kaction form a').hide();
                $('.premium form a').hide();

                $('.flag').unbind('click');
            }

            function sameText() {

                //$('.options-content').text('We can offer you all of our services which are detailed below.').css({'visibility':'visible', 'max-width':'90%'});
                $('.kaction').css("opacity", "1");
                $('.premium').css("opacity", "1");
                $('.kaction form a').show();
                $('.premium form a').show();
            }

            function checkForAll() {


                //$('.options-content').text('We can offer you all of our services, however we would only be able to offer you a Premium Site Visit, '+'details of which can be found below.').css({'visibility':'visible', 'max-width':'70%'});
                //$('.kaction').css("opacity","0.5");
                $('.premium').css("opacity", "1");

                //$('.kaction form a').hide();
                $('.premium form a').show();
            }






        });

    </script>

    <!-- <div style="width:100%;text-align:center">
	<img src="/images/book-site-visit.png" id="img_book_visit" usemap="#linkmap" />
	<map name="linkmap" id="linkmap">
		<area shape="rect" coords="432,222,525,240" href="/getting-started/planning-design-consultation.html">
		<area shape="rect" coords="781,128,871,147" href="/what-we-do/architectural-design-phase-page.html">
	</map>
  <img src="/images/book-site-visit480.png" id="img_book_visit480" usemap="#linkmap480" />
  <map name="linkmap480" id="linkmap480">
    <area shape="rect" coords="227,338,362,418" href="/getting-started/planning-design-consultation.html" />
    <area shape="rect" coords="131,560,268,622" href="/what-we-do/architectural-design-phase-page.html" />
  </map>
  <img src="/images/book-site-visit320.png" id="img_book_visit320" usemap="#linkmap320" />
  <map name="linkmap320" id="linkmap320">
    <area shape="rect" coords="177,287,308,365" href="/getting-started/planning-design-consultation.html" />
    <area shape="rect" coords="100,487,223,546" href="/what-we-do/architectural-design-phase-page.html" />
  </map>
  </div>
  
	<h1 style="margin-top: 20px;">what will be covered during the initial site visit?</h1>

	<div class="col_two_third">
		
    <table class="compare-table">
			<tr>
				<th class="compare-first">Service</th>
				<th class="compare-second">Standard<br>site visit</th>
				<th class="compare-third">Premium<br>site visit</th>
        <th class="compare-third">3D concept visit</th>
			</tr>
			<tr class="odd">
				<td>Laser-measured survey of proposed floor area</td>
				<td class="check-sign">&nbsp;</td>
				<td class="check-sign">&nbsp;</td>
        <td class="check-sign">&nbsp;</td>
			</tr>
			<tr class="even">
				<td>Budget Estimate</td>
				<td class="check-sign">&nbsp;</td>
				<td class="check-sign">&nbsp;<span style="display: inline-block; color: #f6651e; float: right; margin-right: 22px;">*</td>
        <td class="check-sign">&nbsp;</td>
			</tr>
			<tr class="odd">
				<td>Early morning and late evening appointments</td>
				<td class="cross-sign">&nbsp;</td>
				<td class="check-sign">&nbsp;</td>
        <td class="check-sign">&nbsp;</td>
			</tr>
			<tr class="even">
				<td>Cost advice and optimising your budget</td>
				<td class="cross-sign">&nbsp;</td>
				<td class="check-sign">&nbsp;</td>
        <td class="check-sign">&nbsp;</td>
			</tr>
			<tr class="odd">
				<td>Specialist Planning Advice</td>
				<td class="cross-sign">&nbsp;</td>
				<td class="check-sign">&nbsp;</td>
        <td class="check-sign">&nbsp;</td>
			</tr>
			<tr class="even">
				<td>Party Wall Advice and Guidance</td>
				<td class="cross-sign">&nbsp;</td>
				<td class="check-sign">&nbsp;</td>
        <td class="check-sign">&nbsp;</td>
			</tr>
			<tr class="odd">
				<td>Creative design ideas and layout options</td>
				<td class="cross-sign">&nbsp;</td>
				<td class="check-sign">&nbsp;</td>
        <td class="check-sign">&nbsp;</td>
			</tr>
			<tr class="even">
				<td>3D visual of how the exterior will look</td>
				<td class="cross-sign">&nbsp;</td>
				<td class="cross-sign">&nbsp;</td>
        <td class="check-sign">&nbsp;</td>
			</tr>
			<tr class="bsv-totals">
				<td>Cost</td>
				<td class="orange compare-second">Free</td>
				<td class="orange compare-third">£125+VAT **</td>
        <td class="orange compare-third">£250+VAT</td>
			</tr>
			<tr style="background: #fff;">
				<td style="padding: 0; font-size: 12px;">
        * Fastrack within 24 hours<br/>
        ** £125 + VAT fully refundable within 30 days against subsequent design instruction
        </td>
				<td class="compare-second" style="padding: 0;"><span class="links"><form action="/book-visit.html?type=standard" id="standard" method="post"><input type="hidden" name="dateFrom" value="<?= $_POST['dateFrom'] ?>"
    /><input type="hidden" name="dateTo"
        value="<?= $_POST['dateTo'] ?>" /><a
        href="#" onclick="document.getElementById('standard').submit();" class="compare-link">Book now</a></form></span>
    </td>
    <td class="compare-third" style="padding: 0;"><span class="links">
            <form action="/book-visit.html?type=premium" id="premium" method="post"><input type="hidden" name="dateFrom"
                    value="<?= $_POST['dateFrom'] ?>" /><input
                    type="hidden" name="dateTo"
                    value="<?= $_POST['dateTo'] ?>" /><a
                    href="#" class="compare-link" onclick="document.getElementById('premium').submit();">Book now</a>
            </form>
        </span></td>
    <td class="compare-third" style="padding: 0;"><span class="links">
            <form action="/book-visit.html?type=3dconcept" id="3dconcept" method="post"><input type="hidden"
                    name="dateFrom"
                    value="<?= $_POST['dateFrom'] ?>" /><input
                    type="hidden" name="dateTo"
                    value="<?= $_POST['dateTo'] ?>" /><a
                    href="#" class="compare-link" onclick="document.getElementById('3dconcept').submit();">Book now</a>
            </form>
        </span></td>
    </tr>
    </table>

</div>
<div class="col_one_third col_last">
    <img src="/images/booksite.jpg" style="width: 100%;">
</div>
<br clear="all" />
<div class="col_full">

    <p style="text-align:justify">
        This is how we help clients navigate the process of getting started with their Side Return. The graph above
        explains how to approach our side return extension website and your own house building project in an easy way.
        Home extensions in London can dig a hole in your budget if you do not plan ahead, so we came up with a solution
        to help you find out the price of any loft conversion, side extension, basement conversion or mansard loft
        conversion you have in mind by using our online calculator. Having a rough idea about the extension costs, you
        can then decide to have our house builders visit the site and give you more specific details. You can opt for a
        standard visit or a premium site survey. While the first one implies no obligations and no fees and you can
        design your own house within legal boundaries, the second one is a consultation about planning and designing the
        side extensions as best as possible, but for a fee. You can find out which plans are within permitted
        development extensions and which not and the budget estimate will help you decide whether to commence the design
        phase or not. Our website is meant to help you architect your home as best as possible, according to your budget
        availability and interest in our company.
    </p>

    <div class="boxes" style="margin-top: 30px;">
        <div class="darkblue-box">find out<br><span class="orange">more</span></div>
        <div class="lightblue-box"><a href="/what-we-do/process.html">the <span class="orange">8
                    step</span><br>process</a></div>
        <div class="lightblue-box"><a href="/build-your-price-start.html">build your<br><span
                    class="orange">price</span></a></div>
        <div class="lightblue-box" style="margin-right: 0;"><a href="/blog">read our<br><span
                    class="orange">blog</span></a></div>
    </div>
</div>

-->

</div>




<?php
require_once('./inc/footer.inc.php');
