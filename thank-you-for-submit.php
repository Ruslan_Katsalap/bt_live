<?php
require_once('./inc/header.inc.php');
?>
<link href="/css/byp.css?v=8" type="text/css" rel="stylesheet">
<link href="css/newbypmobile.css" rel="stylesheet">
<!-- Google Code for Quote Conversion Page -->
<script type="text/javascript">
  /* <![CDATA[ */
  var google_conversion_id = 980275045;
  var google_conversion_language = "en";
  var google_conversion_format = "1";
  var google_conversion_color = "ffffff";
  var google_conversion_label = "tgaaCKPOrgYQ5Z630wM";
  var google_conversion_value = 0;
  var google_remarketing_only = false;
  /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js"></script>
<noscript>
  <div style="display:inline;">
    <img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/980275045/?value=0&amp;label=tgaaCKPOrgYQ5Z630wM&amp;guid=ON&amp;script=0"/>
  </div>
</noscript>
<style>
	#newwidth #mob_screen_title {
		margin:0 auto !important;
		}
	#newwidth {
		margin-top:20px;
		}	
	#newwidth #byp_thank_you{
		margin:0 auto !important;
		width:60%;
		}
	#newwidth #byp_thank_you p {
		width:100% !important;
		margin-top:10px;
		text-align:left;
		font-size:14px;
		}
	#byp_thank_you p span {
		color: #345986 !important;
		font-weight:600;
		}
				 
</style>
<!-- /Google Code for Quote Conversion Page -->

<div id="byp_container" class="newhidden-xs">
  <div id="byp_head">
    <img src="/images/byp/byp_logo_tm_big.png" alt="Build Your Price" />
  </div>
  <div id="byp_navigation" style="background-image: url('/images/byp/bg_navigation4.png');">
    <div class="byp_item" id="byp_step1">Step 1: Room Size</div>
    <div class="byp_item" id="byp_step2">Step 2: Exterior Finish</div>
    <div class="byp_item" id="byp_step3">Step 3: Interior Finish</div>
    <div class="byp_item" id="byp_step4">Quote</div>
  </div>
  <div id="byp_slide_box">
    <div id="byp_thank_you">
      <p>Thank you for visiting</p>
      <h3>BuildYourPrice<sup>&trade;</sup></h3>
      <p>We will contact<br/>you shortly</p>
    </div>
  </div>
</div>

<div id="newwidth" class="newvisible-xs">
	<div id="mob_screen_title" class="titlefontfamily">Thank You</div>
  	<div id="byp_thank_you">
      <p>Thank you for using BuildYourPrice™ Your Quote has been saved. You can retrieve it at any time in<br><span>your account page</span></p>
      <div class="mobilelogo"><img src="/images/byp-image.jpg" class="byp-logo"></div>
    </div>
  </div>

<?php
require_once('./inc/footer.inc.php');
?>