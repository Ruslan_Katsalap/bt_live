<?php



require_once('./inc/header_new.php');



?>
<!-- Hello world -->
  <div class="main">
  	<div class="left">

    	<h1>Why chose Build Team?</h1>
      
      <ul class="check">
      	<li><b>Full design and build service</b><br />
        At Build Team, we offer a turn-key solution for clients who require both an architect and a builder. To deliver this service Build Team works closely with their sister company, bdAr Architects and designers (www.bdar.co.uk).</li>
        <li><b>Professional Project Managers</b><br />
        With a dedicated and experienced Project Manager assigned to every build, refurbishing properties in London is carried out quickly, efficiently and always to a high quality standard.</li>
        <li><b>Experienced and specialist Tradesmen</b><br />
        Build Team employ only the best and highly qualified tradesmen and the competency of all operatives is checked before they are deployed to site.</li>
        <li><b>We offer Value for Money</b><br />
        We are committed to offering great value for money through clear and transparent pricing and offering a detailed breakdown of the costs on every quotation.</li>
    	</ul>
		</div>
</div>
<!-- test -->
<?php



require_once('./inc/footer_new.php');



?>