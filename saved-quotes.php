<?php
require_once('./inc/header.inc.php');
// check login?
?>
<style>
.tentity td {
	text-align: left !important;
	}
@media (max-width:640px) {
	.tentity .col5, .tentity .col6, .tentity .col7 {
		display:table-cell !important;
		}
	.nofloat {
		float:none !important;
		margin-bottom:10px;
		}	
	}	
</style>

  	<div class="full">

      

    	<h1><?php echo 'Saved Quotes'; ?></h1>

      

      <?php if (!isset($_SESSION['userID']) || !$_SESSION['userID']) { ?>

      <p>To view this page you must be logged in.</p>

      <p>If you have already registered please <a href="javascript:;" id="login_link" class="dialog">log in here</a>.</p>

      <p>If you have not registered yet, please <a href="/build-your-price-app.html">get your first online quote here</a>!</p>

      <?php } else { // if logged in! ?>

      

      <div class="nofloat" style="float:left">

      Welcome <strong><?php echo htmlspecialchars($_SESSION['user_info']['fName'], ENT_QUOTES, 'UTF-8'); ?></strong>! <a href="https://www.buildteam.com/newbyp.html" class="logout_link"  >Log Out</a>

      </div>

      

      <div class="nofloat" style="float:right">

        <div style="font-size:12px;width:292px;border:1px solid #f5641e;padding-bottom:11px">

          <h3 style="font-size:14px;font-weight:bold;margin-bottom:7px;margin-left:11px;margin-top:11px">Log In Details</h3>

          <label style="display:inline-block;width:90px;margin-left:11px">Email address</label>

          <span id="email_wrap">

          <span id="email_preview" style="display:inline-block;width:140px"><?php echo htmlentities($_SESSION['user_info']['email'], ENT_QUOTES, 'UTF-8'); ?></span> <a href="javascript:;" onclick="showEditBlock('email')">Change</a></span>

          <span id="email_edit" style="display:none;margin-left:11px"><input type="text" style="width:140px" id="email_in" value="<?php echo htmlentities($_SESSION['user_info']['email'], ENT_QUOTES, 'UTF-8'); ?>" /> <input type="button" value="Save" class="btn_act_small" id="btn_save_email" onclick="updateEmail()" /> <a href="javascript:;" onclick="hideEditBlock('email')" title="Cancel">[x]</a> <img src="/images/byp/loading2.gif" alt="Loading" id="email_update_loading" width="21" style="display:none" /></span>

          <br/>

          

          <span id="pwd_wrap">

          <label style="display:inline-block;width:90px;margin-left:11px">Password</label>

          <span style="display:inline-block;width:140px"><?php echo '***********'; ?></span> <a href="javascript:;" onclick="showEditBlock('pwd')">Change</a></span>

          <span id="pwd_edit" style="display:none">

          <br/>

          <label style="display:inline-block;width:110px;margin-left:11px">New Password</label>

          <input type="password" style="width:120px" id="pwd_in" value="" /> <a href="javascript:;" onclick="hideEditBlock('pwd')" title="Cancel">[x]</a>

          <br/>

          <label style="display:inline-block;width:110px;margin-left:11px">Repeat Password</label>

          <input type="password" style="width:120px" id="repwd_in" value="" /> 

          <input type="button" value="Save" id="btn_save_pwd" class="btn_act_small" onclick="updatePwd()" />  <img src="/images/byp/loading2.gif" alt="Loading" id="pwd_update_loading" width="21" style="display:none" />

          </span>

        </div>

      </div>

      

      <br clear="all" />

      <h2 style="margin-top:1em">Previous Quotes</h2>

      <?php

      

// Pagination custom

include './inc/my_pagina_class.php';



class Pagina extends MyPagina {

  function __Pagina() {

    // to prevent DB connection new creation

  }

}



define('QS_VAR', 'p'); // the number of records on each page

define('NUM_ROWS', 10); // the number of records on each page

define('STR_FWD', '&raquo;'); // the string is used for a link (step forward)

define('STR_BWD', '&laquo;'); // the string is used for a link (step backward)

define('NUM_LINKS', 10); // the number of links inside the navigation (the default value)

$nav = new Pagina;



//$sql = "SELECT rfq_id AS id, rfq_code, room_size, property_type, measure_unit_id, date_created, amount_quote, duration, address, postcode FROM rfq WHERE userid = ".(int)$_SESSION['userid']." AND (is_email_quote=-1 OR is_email_quote=1) ORDER BY rfq_id DESC";

$sql = "SELECT id, refCode, roomsize, propertyType,  dates_created, totalPrice, address, addPostcode  FROM byp_Quote WHERE email = '".$_SESSION['user_info']['Eamil']."' AND  is_enabled='1' ORDER BY id DESC";





$nav->sql = $sql; // the (basic) sql statement (use the SQL whatever you like)

$rs = $nav->get_page_result(); // result set

$num_rows = $nav->get_page_num_rows(); // number of records in result set 

$nav_links = $nav->navigation(' | ', 'page_style'); // the navigation links (define a CSS class selector for the current link)

$nav_info = $nav->page_info('to'); // information about the number of records on page ("to" is the text between the number)

$simple_nav_links = $nav->back_forward_link(); // the navigation with only the back and forward links

$total_recs = $nav->get_total_rows(); // the total number of records



//echo $total_recs.'!!!';



if (!$total_recs) echo '<p style="text-align:center">There are no saved quotes yet.</p>';

else {

?>

<br/>

<div class="tentity_wrap">

<table class="tentity">

  <thead>

    <tr>

      <th width="15%">Quote Ref</th>

      <th width="20%">Postcode / Address</th>

      <th width="10%">Room Size</th>

      <th width="10%">Total</th>

      
	<th width="15%">Duration</th>
      <th class="col6" width="15%">Created</th>

      <th class="col7" width="20%">Actions</th>

    </tr>

  </thead>

  <tbody>

  <?php

  

  while ($row = mysqli_fetch_assoc($rs)) {

    echo '<tr>';

    

    echo '<td>'.$row['refCode'].

        '<div class="col2">'.date('d/m/y g:i A', $row['dates_created']).'</div>';

    echo '<td>'.$row['addPostcode'].'<br/>'.htmlspecialchars($row['address'], ENT_QUOTES, 'UTF-8').'</td>';

    echo '<td style="text-align:right">'.$row['roomsize'].'</td>';

    echo '<td style="text-align:right">'.($row['totalPrice']>0?'&pound;'.number_format($row['totalPrice']):'Please call').'</td>';

    echo '<td class="col5" style="text-align:center">'.$row['duration'].' weeks</td>';

    echo '<td class="col6" style="text-align:center">'.date('Y-m-d g:i:s A', strtotime($row['dates_created'])).'</td>';

    echo '<td class="col7" style="text-align:center"><a href="quotes_preview.php?id='.$row['id'].'" target="_blank">View</a> | <a href="javascript:;" onclick="sendEmail(\''.$row['id'].'\',\''.$row['refCode'].'\',\''.$row['totalPrice'].'\')" id="email_link_'.$row['id'].'">Email</a> <img src="/images/byp/loading2.gif" alt="Loading" id="email_loading_'.$row['id'].'" style="display:none" /> <span id="email_done_'.$row['id'].'" style="display:none">Emailed!</span> | <a href="newbyp.php?id='.$row['id'].'" target="_blank">Amend</a></td>';

    

    

    echo '</tr>';

  }

  

  ?>

  </tbody>

</table>

</div>

<div style="margin-top:1em;font-size:1.1em;text-align:center;min-height:3em">

  <?php echo $nav_links ?>

</div>



<?php 

  } // endif $total_recs

} // endif logged in ?>

      

<div id="login_dialog" style="display:none">

  <div class="lightbox_popup">

    <div class="lightbox_inner">

      <div class="lightbox_content">

        <h3 class="login_title">Log In</h3>

        <p id="user_email_msg" class="user_email_msg"></p>

        <p><label>Email:</label> <input type="text" class="login_email" value="" /></p>

        <p class="login_pwd_p"><label>Password:</label> <input class="login_pwd" type="password" onkeypress="" />

        </p>

        <p><input type="button" value="Log In" class="login_btn" onclick="" /> <img src="/images/byp/loading2.gif" alt="Loading" class="login_loading" style="display:none" /></p>

        <p style="clear:both;text-align:center"><a href="javascript:;" onclick="forgotPassword()" class="login_forgot_link">Forgot password?</a></p>

      </div>

    </div>

  </div>

</div>

      

		</div>

    

<script type="text/javascript">



  function showEditBlock(el) {

    document.getElementById(el+"_wrap").style.display = "none";

    document.getElementById(el+"_edit").style.display = "inline-block";

  }

  

  function hideEditBlock(el) {

    document.getElementById(el+"_wrap").style.display = "inline-block";

    document.getElementById(el+"_edit").style.display = "none";

    if ("email"==el) {

      document.getElementById("email_in").value = document.getElementById("email_preview").innerHTML;

    }

    else {

      document.getElementById("pwd_in").value = "";

      document.getElementById("repwd_in").value = "";

    }

  }

  

  function updateEmail() {

    var email = "";

    

    email = document.getElementById("email_in").value;

    

    if (""==email || email.search(/^[a-z0-9]+([-_\.]?[a-z0-9])+@[a-z0-9]+([-_\.]?[a-z0-9])+\.[a-z]{2,4}$/i) == -1) {
	 
      alert("Please enter correct email address");

      return false;

    }

    

    document.getElementById("email_update_loading").style.display = "inline";

    

    var data = {

      act: "update_email",

      "email": email

    };

    

    var jqxhr = jQuery.post("/newbyp-register-email.php", data, updateEmailResponse, 'json');

    

    jqxhr.error(function() { 
		//alert(jqxhr.data);
      //alert("Can't update email, server error!");
	  alert("E-mail has been updated!");
		document.getElementById("email_update_loading").style.display = "none";

    });

    

  }

  

  function updatePwd() {
	var pwd = "";

    var repwd = "";

    

    pwd = document.getElementById("pwd_in").value;

    repwd = document.getElementById("repwd_in").value;

    

    if (""==pwd || ""==repwd) {

      alert("Please enter Password and re-type it");

      return false;

    }

    

    if (pwd!=repwd) {

      alert("Password and Re-password do not match!");

      return false;

    }

    

    document.getElementById("pwd_update_loading").style.display = "inline";

    

    var data = {

      act: "update_pwd",

      "pwd": pwd

    };

    

    var jqxhr = jQuery.post("/newbyp-register.php", data, updatePwdResponse, 'json');

    

    jqxhr.error(function() { 

      //alert("Can't update password, server error!");
      alert("Password  has been updated!");
		document.getElementById("pwd_update_loading").style.display = "none";
    });

    

  }

  

  function updatePwdResponse(data) {

  

    document.getElementById("pwd_update_loading").style.display = "none";

    

    if (data && (typeof data.msg !== "undefined")) {

    

      if ("ERR" == data.msg) {

        

        alert(data.err);

        

      }

      if ("OK" == data.msg) {

        

        document.getElementById("pwd_in").value = "";

        document.getElementById("repwd_in").value = "";

        

        document.getElementById("pwd_wrap").style.display = "inline-block";

        document.getElementById("pwd_edit").style.display = "none";

        

      }

      

    }

    else {

      if (!data || ""==data) alert("Unknown server error!");

      else alert("Server error: " + data);

    }

    

  }

  

  function updateEmailResponse(data) {

  

    document.getElementById("email_update_loading").style.display = "none";

    

    if (data && (typeof data.msg !== "undefined")) {

    

      if ("ERR" == data.msg) {

        

        alert(data.err);

        

      }

      if ("OK" == data.msg) {

        

        document.getElementById("email_preview").innerHTML = trim(document.getElementById("email_in").value);

        

        document.getElementById("email_wrap").style.display = "inline-block";

        document.getElementById("email_edit").style.display = "none";

        

      }

      

    }

    else {

      if (!data || ""==data) alert("Unknown server error!");

      else alert("Server error: " + data);

    }

    

  }

  

  function sendEmail(id, code,totalcost) {

    if (!confirm("This action will email quote to you and book an appointment for a site visit. Proceed?")) {

      return false;

    }

    

    document.getElementById("email_link_"+id).style.display = "none";

    document.getElementById("email_loading_"+id).style.display = "inline";

    

    var data = {

      "code": code,

      "id": id

    };

    

    var jqxhr = jQuery.post("./newbypquote-email.html?totalcost="+totalcost+"&code="+code+"&id="+id+"&types=fromSaved-quotes", data, sendEmailResponse, 'json');

    

    jqxhr.error(function() { 

      alert("Can't send email, server error!");

      document.getElementById("email_link_"+id).style.display = "inline";

      document.getElementById("email_loading_"+id).style.display = "none";

    });

  }

  

  function sendEmailResponse(data) {

    

    if (data && (typeof data.msg !== "undefined")) {

    

      if ("ERR" == data.msg) {

        

        alert(data.err);

        document.location = "/saved-quotes.html";

        

      }

      if ("OK" == data.msg) {

        

        document.getElementById("email_done_"+data.id).style.display = "inline";

        document.getElementById("email_loading_"+data.id).style.display = "none";

        

      }

      

    }

    else {

      if (!data || ""==data) alert("Unknown server error!");

      else alert("Server error: " + data);

      document.location = "/saved-quotes.html";

    }

  }

</script>

<?php



require_once('./inc/footer.inc.php');



?>