<?php

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    	
			<?php echo $bc_trail; ?>
      
    	<h1>Reliable</h1>
      <p>Years of experience has taught us the benefits of reliability. This aspect of our business has been key to keeping happy customers that come back to us year on year, whether we are working on residential or commercial projects.</p>
      <p>Each morning our team sets a time frame for the day, which if alters unexpectedly, all parties are informed immediately, especially the client, and necessary action is taken to bring our schedule back on course. We work a transparent policy across the board which gives us the ability to build trustworthy relationships with our clients.</p>
      <p>As the reality of any build and maintenance work is often only adjacent to the concept, our efforts are best spend in keeping this parallel as tight as possible in order to benefit the budgets and expectations.</p>
		</div>
<?php

require_once('./inc/footer.inc.php');

?>