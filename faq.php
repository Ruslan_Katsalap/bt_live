<?php

require_once('./inc/util.inc.php');


require_once('./inc/header.inc.php');

?>
    <style type="text/css">
      
      .lightblue-box, .darkblue-box {
      	margin-right: 10px;
      	width: 108px;
      }  
	  .faq-title {
		  font-weight:bold !important;
		  }
	  
	  .list-space li {
    list-style: none;
    font-size: 17px;
    font-weight: normal !important;
    padding: 0px !important;
    margin: 0px !important;
    margin-bottom: 10px !important;
    color: #0f4778 !important;
    cursor: pointer;
    background-image: url(http://www.buildteam.com/images_new_design/faq-arrow-img.jpg);
    background-position: 0px -54px;
    background-repeat: no-repeat;
    padding-left: 30px !important;
	text-align:justify;
	    width: 93%;
	  }

.expand
{
	background-position: 0px 1px !important;
}
	  
.list-space span {
    font-size: 15px important;
    font-weight: normal;
    line-height: 24px;
    margin-bottom: 35px;
    color: #7b7b7b;
    margin-top: -10px;
    width: 97%;
    font-size: 12pt !important;
    font-family: roboto !important;
    text-indent: 0px !important;
    padding-left: 30px;
}

.list-space span p{
text-align:justify;
padding-top:5px;
}

.list-space span p span{
	margin-left:0px !important;
}

.top-note {
    
    padding: 20px 0px;
    font-size: 15px;
    color: #444;
    margin-bottom: 20px;
    font-weight: normal;
    color: #7b7b7b;
    width: 100%;
	    padding-top: 10px;
}

.top-note p {
    padding-top: 0px;
    line-height: 24px;
	font-size:18px;
	    text-align: justify;
}

.cat-filter {
    font-size: 15px;
    float: left;
    padding: 10px 0px;
    display: inline-block;
    margin-right: 35px;
    color: #757575;
	margin-bottom: 10px;
}
.cat {
    width: 100%;
    clear: both;
    margin-bottom: 40px;
    display: block;
    float: left;
	border-top: 2px solid #c3d1dd;
    border-bottom: 2px solid #c3d1dd;
}

.sep {

    height: 37px;
    background: #104771;
    margin-bottom: -12px;
    display: inline-block;
    margin-left: 5px;
    border-right: 1px solid #0f4778;
    margin-right: 5px;
}

.ac{
	color:#f1622a !important;
}

o:p
{
	display:none;
}

.col_full {
    clear: both;
    float: none;
    margin-right: 0;
}

.list-space li {
	height: 0;
    overflow: hidden;
   	animation: slide 0.3s ease 0.3s forwards;
	}
	
@keyframes slide {
  from { height: 0;}
  to {height: 20px;}
}	
@media (max-width:640px) {
	.cat-filter b {
	display:inline-block;
	min-width:85px;
	}	
	}
@media (max-width:340px) {
	.list-space span p {
		font-size:14px;
		}
	.top-note p {
    font-size:14px;
		}	
	}	
    </style>

	
<script>
jQuery(document).ready(function($){
		jQuery("#faq1").slideToggle();
		jQuery(".faq1").addClass("expand");
});
</script> 



<script>
function loadDoc(i) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("faq-list").innerHTML =
      this.responseText;
    }
  };

  xhttp.open("GET", "https://www.buildteam.com/faq-query.php?cat="+i, true);
  xhttp.send();
}
</script>





    <a name="top"></a>
  	<div class="full">

    	
			<!--div id="bc"><a href="faq.html" title="Questions and Answers">Q&amp;A</a></div-->


<div class="col_full" style="position:relative;z-index:999;margin-bottom:100px;">
<h1 class="faq-title" style="font-weight:bold;color:#114578 !important;text-transform: capitalize;font-weight: normal;">Frequently Asked Questions</h1>
	  
      

<div class="top-note">  
<?php 	  
$ns = getRs("SELECT * FROM page WHERE page_id=44 AND is_enabled=1 limit 1");
if ($nw = mysqli_fetch_assoc($ns)) {
    $page_content = $nw['page_content'];
    echo $page_content;
}  
?>
</div>
	 

	  

<div class="cat"> 
<?php

$dropdownsql = "SELECT distinct cat FROM faq WHERE is_active = 1 AND cat != '' ORDER BY weight, id";


//run sql queries separately.  Ideally they would be combined into one right?
$rs = mysqli_query($dbconn,$dropdownsql);

$keywords = array();

while($raw = mysqli_fetch_array($rs)) {
    $keywords = array_merge($keywords, explode(', ', $raw[0]));
}

$values = array_unique($keywords, SORT_STRING);

//and finally echo the keywords into a dropdown 
foreach($values as $value){ 

  ?>
    <script>
function loadDocc(i) {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200) {
      document.getElementById("faq-list").innerHTML =
      this.responseText;
    }
  };

  xhttp.open("GET", "https://www.buildteam.com/faq-query.php?cat="+i, true);
  xhttp.send();
  
 
 
  //document.getElementById(i).style.color = "red";
}
</script>
<div class="cat-filter"><b><?php echo $value; ?> FAQ’s</b> <span class="sep"></span> <a class="all-a" id="<?php echo $value; ?>" onClick="loadDocc('<?php echo $value; ?>')">Filter</a> </div>
<?php
}

?> 	 
<div class="cat-filter"><b>All FAQ’s</b> <span class="sep"></span> <a class="all-a ac"  onClick="loadDoc('all')">Filter</a> </div>
	
	

</div> 


<div id="faq-list">
	  
<?php
  $rs = getRs("SELECT id, title, body FROM faq WHERE is_active = 1 ORDER BY weight, id");
  echo '<ul class="check list-space" style="float:left">';
	while ( $row = mysqli_fetch_assoc($rs) ) {
    /*
?>  

<script>
jQuery(document).ready(function($){
	jQuery(".faq<?php echo $row['id']; ?>").click(function(){
		jQuery("#faq<?php echo $row['id']; ?>").slideToggle();
		jQuery(this).toggleClass("expand");
	});
});
</script>  
 <?php
*/
$str = $row['body'];
	$pattern = '#<o:p>(\s|&nbsp;)*</o:p>#';
    $str = str_replace('<o:p></o:p>', '', $str);
	
	
    echo '<li class="faq'.$row['id'].'">'.$row['title'].'</li>';
	echo '<span id="faq'.$row['id'].'" style="display:none">'.$str.'</span>';
  }
  echo '</ul>';
  mysqli_data_seek($rs, 0);

?>
     </div> <!--faq-list-->
    </div>
      
   

      <div class="col_full" style="display:none;">
      <div class="newspaper">
      
      <?php
      //echo '<div class="col_one_third">';
      $i = 0;
      $col = 1;
      $max_cols = 3;
      $item_per_col = ceil(mysqli_num_rows($rs) / $max_cols);
      while ( $row = mysqli_fetch_assoc($rs) ) {
        $i++;
        echo '<div style="padding-bottom: 15px;padding-right:10px"><a name="faq'.$row['id'].'"></a><strong style="font-weight: bold;">'.$row['title'].'</strong><p>'.$row['body'].'</p></div>';
        
        if ($i>=$item_per_col && $col < $max_cols) {
          $i = 0;
          $col++;
          //echo '</div><div class="col_one_third'.($col==$max_cols?' col_last':'').'">';
        }
        
      }
      //echo '</div>';
      ?></div>
      
    </div>


</div>

<style>
@media only screen and (max-width: 480px)
{
.list-space li
{
margin-left:10px !important;
box-sizing: border-box;
line-height: 20px;
text-align: left;
}

.list-space span {
    margin-left: 50px;
	box-sizing: border-box;
	width: 82%;
	line-height: 20px;
}
.faq-title
{
	margin-left:40px !important;
	font-size:22px !important
}

.top-note {
    margin-left: 39px;
	    width: 80%;
}

.top-note p
{
	    text-align: left;
}

.list-space span p {
    text-align: left;
    padding-top: 5px;
}

.cat {
    width: 85%;
    height: inherit;
    clear: both;
    margin-left: 40px;
    display: block;
    float: left;
	    margin-bottom: 40px;
}
.cat-filter
{
	font-size:14px;
	float:left;
	display:inline-block;
	margin-right:80px;
	color:#757575;
}
.col_full {

    margin-left: 0px;
}



}
</style>


<?php
  $rss = getRs("SELECT id, title, body FROM faq WHERE is_active = 1 ORDER BY weight, id");
	while ( $roww = mysqli_fetch_assoc($rss) ) {
    
?>  
<script>

$(document).on('click', ".faq<?php echo $roww['id']; ?>", function () {
		jQuery("#faq<?php echo $roww['id']; ?>").slideToggle();
		jQuery(this).toggleClass("expand");
});


$(document).ready(function(){
	$(".all-a").click(function(){
		$("a").removeClass("ac");
		$(this).addClass("ac");
	});
});
</script> 

<?php
	}
?>



<div class="clear" style="clear:both;width:100%;"></div>



<div class="clear" style="clear:both;width:100%;"></div>







<?php

require_once('./inc/footer.inc.php');

?>