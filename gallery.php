<?php require_once('./inc/header.inc.php'); ?>
<script src="https://www.buildteam.com/js/jquery.sumoselect.js"></script>
<link href="https://www.buildteam.com/css/sumoselect.css" rel="stylesheet" />

<script type="text/javascript">
        $(document).ready(function () {
          
            window.test = $('.testsel').SumoSelect({okCancelInMulti:true, captionFormatAllSelected: "Yeah, OK, so everything." });
			 window.test = $('.testsel2').SumoSelect({okCancelInMulti:true, captionFormatAllSelected: "Yeah, OK, so everything." });
			  window.test = $('.testsel3').SumoSelect({okCancelInMulti:true, captionFormatAllSelected: "Yeah, OK, so everything." });

  
        });	
    </script>
<style>
/* Css for gallery */
p.CaptionCont.SelectBox:before {
    border-bottom: 2px solid #9A9A9A;
    border-right: 2px solid #9A9A9A;
    content: '';
    display: block;
    height: 10px;
    margin-top: -4px;
    pointer-events: none;
    position: absolute;
    left: 12px;
    top: 36%;
    -webkit-transform-origin: 66% 66%;
    -ms-transform-origin: 66% 66%;
    transform-origin: 66% 66%;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
    -webkit-transition: all 0.15s ease-in-out;
    transition: all 0.15s ease-in-out;
    width: 10px;
}
p.CaptionCont.SelectBox:after {
    position: absolute;
    right: 0;
    top: 0;
    content: "";
    width: 40px;
    height: 42px;
    background-image: url(/img/filter-icon.png?1);
    background-repeat: no-repeat;
    background-position: 4px 7px;
    background-size: 28px;
}
.SumoSelect > .CaptionCont {
    position: relative;
    border: 1px solid #A4A4A4;
    min-height: 14px;
    background-color: #fff;
    border-radius: 2px;
    margin: 0;
    -webkit-tap-highlight-color: transparent;
    background-color: #fff;
    border-radius: 5px;
    border: solid 1px #0f4778;
    box-sizing: border-box;
    clear: both;
    cursor: pointer;
    display: block;
    float: left;
	font-family: inherit;
    font-size: 14px;
    font-weight: normal;
    height: 32px;
    line-height: 30px;
    outline: none;
    padding-left: 40px;
    padding-right: 8px;
    position: relative;
    text-align: left !important;
    -webkit-transition: all 0.2s ease-in-out;
    transition: all 0.2s ease-in-out;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    white-space: nowrap;
    width: auto;
    width: calc(100% - 52px);
}
.col_last {
	float:left;
}
.col_one_third a {
    position: relative;
    width: 100%;
    display: block;
}
.col_one_third img {
    border: 0;
    /* max-width: 450px !important; */
    width: 100% !important;
	max-width: inherit !important;
	min-height: 200px;
	height: 308px;
}
.col_one_third {
	margin-right: 2.7%;
}
.MultiControls p:last-child {
	display:none !important;
	}
.SumoSelect > .optWrapper.multiple > .MultiControls > p {
	
	}	
.MultiControls p:first-child {
    background: url(/img/filter-icon-orange.png) no-repeat center;
    color: #FFF;
    background-size: 28px;
    text-indent: 1000px;
    overflow: hidden;
    padding: 11px 0px !important;
    position: absolute;
    top: -46px;
    width: 42px !important;
    right: 1px;
}	
.MultiControls p:hover {
    background-color: #fff !important; 
}
.titlewithreset {
	max-width:1048px;
	}
.titlewithreset h1, .titlewithreset .resetright {
    display: inline-block;
    width: 48%;
    vertical-align: bottom;
	
}
.titlewithreset .resetright {
    display: inline-block;
    float: right;
    text-align: right;
    vertical-align: middle;
	position: relative;
    top: 10px;
}
.resetright p {
    font-size: 16px;
    color: #0e4776;
    font-weight: normal;
	display: inline-block;
	cursor:pointer;
}
.resetright img {
    display: inline-block;
    max-width: 25px;
    vertical-align: middle;
}
.resetpertifilter {
    position: absolute;
    top: 0px;
    right: 13px;


}
.resetpertifilter p {
    margin: 0px;
    padding: 0px;
    padding-top: 9px;
	cursor: pointer;
    font-size: 20px;
}
.resetpertifilter img {
    display: inline-block;
    max-width: 25px !important;
    vertical-align: middle;
    max-height: 25px !important;
    min-height: inherit;
}
.resetpertifilter {
	display:none;
	}
</style>
<style>
/* ---- button ---- */

.button-group  .button {
  display: inline-block;
  padding: 10px 18px;
  margin-bottom: 10px;
  background: #EEE;
  border: none;
  border-radius: 7px;
  background-image: linear-gradient( to bottom, hsla(0, 0%, 0%, 0), hsla(0, 0%, 0%, 0.2) );
  color: #222;
  font-family: sans-serif;
  font-size: 16px;
  text-shadow: 0 1px white;
  cursor: pointer;
}

.button-group  .button:hover {
  background-color: #8CF;
  text-shadow: 0 1px hsla(0, 0%, 100%, 0.5);
  color: #222;
}

.button-group  .button:active,
.button-group  .button.is-checked {
  background-color: #28F;
}

.button-group  .button.is-checked {
  color: white;
  text-shadow: 0 -1px hsla(0, 0%, 0%, 0.8);
}

.button-group  .button:active {
  box-shadow: inset 0 1px 10px hsla(0, 0%, 0%, 0.8);
}

/* ---- button-group ---- */

.button-group:after {
  content: '';
  display: block;
  clear: both;
}

.button-group .button {
  float: left;
  border-radius: 0;
  margin-left: 0;
  margin-right: 1px;
}

.button-group .button:first-child { border-radius: 0.5em 0 0 0.5em; }
.button-group .button:last-child { border-radius: 0 0.5em 0.5em 0; }
.grid-gallery {
	position: relative;
}
a.hover-box {
    display: none;
    outline: none;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.77);
    visibility: hidden;
    opacity: 0;
}
.inner-hover-box {
	display: block;
    position: absolute;
    text-align: center;
    top: 50%;
    left: 50%;
    width: 100%;
    transform: translateX(-50%) translateY(-50%);
}
.inner-hover-box i {
    display: inline-block;
    font-style: normal;
}
.inner-hover-box i {
font-size:24px;
font-weight: 500;
}
.element-item-gallery:hover .hover-box {
    visibility: visible;
    opacity: 1;
    display: block;
}
.filter-btn {
	border: none;
    color: #fff;
    padding: 14px 0px;
    background: #0f4778;
    font-size: 16px;
    /* box-shadow: none; */
	    border-radius: 5px;
		display: block;
    width: 100%;
}
select[multiple], select[size] {
    width:100%;
}
.noresult {
	display:none;
	padding:50px 0px;
	text-align:center;
}
.noresult p {
	color:#f58630;
	font-size:22px;
	font-weight: 500;
}
.noresult p a{
	color:#f58630;
	text-decoration:underline;
}
.outer-filter{
	min-height: 93px;

}
.outer-filter .first-hr{
    max-width: 1078px;
    margin-top: 20px;
    margin-bottom: 26px;
	border-top: 1px solid #0f4778;
	width: 97%;
}
.outer-filter .second-hr{
	margin-top: 98px;
    max-width: 1078px;
    border-top: 1px solid #0f4778;
	width:97%;
	display:flex;
}
.clear {
	clear: both;
}
@media only screen and (min-width: 1010px){
.full {
	width: 100%;
max-width: 1078px;
margin: 20px auto;
display: flow-root;
padding-left: 30px;
}
}
@media only screen and (max-width: 768px){
	.col_one_fourth  {
		min-height: 20px !important;
	}
	.nice-select.wide {
		width: 100% !important;
	}
	.col_one_fourth:nth-child(2n) {
		margin: 0 auto;
	}
	.nice-select {
		margin-bottom: 10px;
	}
	.outer-filter {
		min-height: 230px;
	}
	.outer-filter .second-hr{
	}
	.filters.col_last {
		margin-bottom: 0 !important;
	}
	.SumoSelect > .CaptionCont {
		width:100% !important;
		height:40px !important;
		}
	.col_one_third {
		margin-bottom:15px !important; 
		}	
}

</style>
<link rel="stylesheet" type="text/css" href="/css/nice-select.css" />
<div class="full">
<div class="titlewithreset">
<h1>Gallery</h1>
<div class="resetright">
	<p>Reset Filter <img src="img/illus/restart_img.jpg" alt=""></p>
</div>
<div class="clearfix"></div>
</div>
<div class="outer-filter">
<hr class="first-hr">
<div class="col_one_third getfilter1 forfilterborder" data-filter-group="value">
<select  multiple="multiple" data-group="borough" placeholder="Borough" class="wide custom-filter select-filter1 testsel option-set">
 	<!--<option data-display="" class="all">All</option>-->
	<?php
		$b_query = mysqli_query($dbconn,"select b_name from project_borough where is_enabled=1 and is_active=1");
		while($b_row = mysqli_fetch_array($b_query)) {
			$b_val = str_replace(" ", "_", $b_row['b_name']);
			echo '<option id="'.$b_val.'" value="'.$b_val.'" data-name="'.$b_val.'">'.$b_row['b_name'].'</option>';
		}
	?>
</select>
<div class="resetpertifilter resetborough">
	<p><img src="/img/cross.png" alt=""></p>
</div>
</div>

<div class="col_one_third getfilter2 forfilterborder" data-filter-group="value">
<select class="wide custom-filter select-filter2 testsel2 option-set" data-group="floor" placeholder="Floor Area" multiple="multiple">
	<option data-display="" class="all">All</option>
	<!--<option data-display="FLOOR AREA"></option>-->
	<?php
		if($_SERVER['REMOTE_ADDR'] == '122.173.126.246') {
			echo "test";
			$q = mysqli_query($dbconn,"select * from project_floorarea");
		while($t_row = mysqli_fetch_array($q)) {
			print_r($t_row);
		}
			
		}
		
		
		$t_query = mysqli_query($dbconn,"select floor_area from project_floorarea where is_enabled=1 and is_active=1");
		while($t_row = mysqli_fetch_array($t_query)) {
			$t_val = str_replace(" ", "_", $t_row['floor_area']);
			$t_val = str_replace("&gt;", "a_", $t_val);
			$t_val = str_replace("&lt;", "a_", $t_val);
			echo '<option id="'.$t_val.'" value="'.$t_val.'" data-name="'.$t_val.'">'.$t_row['floor_area'].'</option>';
		}
	?>
</select>

<div class="resetpertifilter resetfloor">
	<p><img src="/img/cross.png" alt=""></p>
</div>
</div>
<div class="col_one_third forfilterborder getfilter3  filters col_last" data-filter-group="value">
<select class="wide custom-filter select-filter3 testsel3 option-set" data-group="project-type" placeholder="Project Type" multiple="multiple">
	<!--<option data-display="" class="all">All</option>-->
	<!--<option data-display="PROJECT TYPE"></option>-->
	<?php
		$t_query = mysqli_query($dbconn,"select project_type from project_type where is_enabled=1 and is_active=1");
		while($t_row = mysqli_fetch_array($t_query)) {
			$p_val = str_replace(" ", "_", $t_row['project_type']);
			echo '<option id="'.$p_val.'" value="'.$p_val.'" data-name="'.$p_val.'">'.$t_row['project_type'].'</option>';
		}
	?>
</select>
<div class="resetpertifilter resetprotype">
	<p><img src="/img/cross.png" alt=""></p>
</div>
</div>
<hr class="second-hr"> 
</div>
<!--<div class="col_one_third col_last">
	<input type="button" class="filter-btn" value="Filter" id="new-select-filter-new">
</div>-->
<br clear="all" />
<div class="grid-gallery">
<?php

$i = 1;
$ret = '';
$project_code = '';

if ( isset($_GET['p']) ) {
	//$project_code = $_GET['p'];
}


$rs = getRs("SELECT p.thumbnailImg, c.project_category_code, c.project_category_name, i.filename, p.project_code, p.project_name,p.project_type,p.borough,p.floor_area FROM project_category c INNER JOIN (project_image i INNER JOIN project_new p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 ORDER BY p.sort, p.project_id, i.sort, i.project_image_id");

$cols = 3;
$project_count = 0;
$prev_count = 0;

while ($row = mysqli_fetch_assoc($rs) ) {
	//print_r($row);
	if ( $i == 1 ) {
		// echo '<div id="bc"><a href="/index.html">Home</a> &rsaquo; <a href="/our-experience.html">Our Experience</a> &rsaquo; <b>' . $row['project_category_name'] . '</b></div>';
	}
		$project_type = '';
		$borough = '';
		$floor_area = '';
		if($row["project_type"] != null && $row["project_type"] != "") {
			$project_type = str_replace(" ", "_", $row["project_type"]);
		}
		if($row["borough"] != null && $row["borough"] != "") {
			$borough = str_replace(" ", "_", $row["borough"]);
		}
		if($row["floor_area"] != null && $row["floor_area"] != "") {
			
			$floor_area = str_replace(" ", "_", $row["floor_area"]);
			$floor_area = str_replace("&gt;", "a_", $floor_area);
			$floor_area = str_replace("&lt;", "a_", $floor_area);
			$floor_area = str_replace(">", "a_", $floor_area);
			$floor_area = str_replace("<", "a_", $floor_area);
		}
		if ($project_code != $row['project_code']) {
			$project_code = $row['project_code'];
      $project_count++;
		}
		else {
			continue;
      //$ret .= 'display:none;';
		}
	$ret .= '<'.'div class="col_one_third element-item-gallery '. $project_type .' '.$borough .' '.$floor_area.' '. ($i%3 == 0 ?  ' ' : '') .'" data-category="'.$project_category_code.'" style="text-align:center';
		$project_category_code = $row['project_category_code'];
		if($row['project_category_code'] == 'side-return-extensions') {
			$project_category_code = 'side-return-extension';
		}
		 
		if($row['thumbnailImg']==''){
			$thumbnail = '/projects/' . $row['filename'];
		}else{
			$thumbnail = '/projects_new/thumbnail/' . $row['thumbnailImg'];
		}
		
		$ret .= '"><div><a  href="/'.$row['project_code'].'-'.$project_category_code.'.html?slider=1" title="' . htmlentities($row['project_name']) . '" ><img src="'.$thumbnail . '" style="width:100%;max-width:450px" alt="' . htmlentities($row['project_name']) . '" /></a>
		<a class="hover-box" href="/'.$row['project_code'].'-'.$project_category_code.'.html">
			   <span  class="inner-hover-box"><i>'.htmlentities($row['project_name']).'</i></span>
		</a>
		</div></div>'; // debug: ' '.$project_count
    
    if (0==($project_count % $cols) && $project_count!=$prev_count) {
      //$ret .= '<br clear="all" />'; // debug: ($project_count % $cols).'!!!
      $prev_count = $project_count;
    }    
	$i += 1;
}

echo $ret;

?>
</div>
<br clear="all"/>


<div class="noresult"><p>Unfortunately we haven't completed any projects that match your search criteria - <a href="#" onclick="window.location.reload();return false;">go back</a></p></div>

<p style="text-align:justify">
London homes are constantly in need of refurbishment services because everyone wants to keep their houses up to date, spacious and welcoming and London does encourage a tasteful modern life style. Our construction company in London specialises in London property management and we are ready to answer your call for building refurbishment in London. Our gallery displays some of the stylish house decorating ideas, elegant loft room ideas and novel architectural design used in the refurbishment of various spaces. Our interior designers and house builders work with your existing home design but improve it based on your own requirements. They come up with living room decorating ideas, kitchen extensions, loft conversions plans and always come up with amazing house designs in UK. We want to provide you with house changes, exactly as you envisaged them and aim to have that garage conversion, basement conversion or mansard loft conversion exactly as you dreamed of having it. Building refurbishment in London is what we do best, but we are also open to working on office refurbishment London or hotel refurbishment London. Don’t hesitate to contact us!
</p>

</div>

<?php

require_once('./inc/footer.inc.php');

?>
<script src="/js/isotope.pkgd.min.js"></script>
<!--<script src="/js/jquery.nice-select.js"></script>-->
<script>
// external js: isotope.pkgd.js
$(window).load(function(){
	$(".select-filter1 option").each(function () {
			console.log($(this).val().replace('&', '\\&'));
			$(this).val($(this).val().replace('&', '\\&'));
		})
	
	$(".select-filter2 option").each(function () {
			console.log($(this).val().replace('<', '\\<'));
			$(this).val($(this).val().replace('<', '\\<'));
		})
	})
// init Isotope
$(window).load(function(){

var $grid = $('.grid-gallery').isotope({
	itemSelector: '.element-item-gallery',
	percentPosition: true
});
 

	$(".options li").each(function(index, element) {
        $(this).removeClass("selected");
    });
	$(".getfilter1 .CaptionCont span").text("Borough");
	$(".getfilter2 .CaptionCont span").text("Floor Area");
	$(".getfilter3 .CaptionCont span").text("Project Type");
	
   $(".resetright p").click(function(){
    $(".grid-gallery").isotope({
        filter: '*'
    });
	$(".options li").each(function(index, element) {
        $(this).removeClass("selected");
    });
	$(".getfilter1 .CaptionCont span").text("Borough");
	$(".getfilter2 .CaptionCont span").text("Floor Area");
	$(".getfilter3 .CaptionCont span").text("Project Type");
	
	/*$('.testsel').sumo.unSelectItem();
	$('.testsel2').sumo.unSelectItem();
	$('.testsel3').sumo.unSelectItem();*/

$(".testsel").change(function(){
	$(".resetborough").fadeIn(300);
	});

$(".testsel2").change(function(){
	$(".resetfloor").fadeIn(300);
	});

$(".testsel3").change(function(){
	$(".resetprotype").fadeIn(300);
	});
});

	
		var $container;
		var filters = {};
		
		var data = {
			  borough: 'Lambeth,Southwark,Lewisham,Wandsworth,Islington,Haringey,Brent,Camden,Greenwich,Hackney,Enfield,Barnet,Kensington_&_Chelsea,Redbridge,Westminster,Hounslow,Hammersmith_&_Fulham'.split(','),
			  area: '<25 25_-_30 30_-_40 40>'.split(' '),
			  catego: 'Ground_Floor_Extension Loft_Conversion yellow green'.split(' '),
			  //sizes: 'uk-size8 uk-size9 uk-size10 uk-size11'.split(' ')
			};
	$(function(){
		
			
		createContent();
		// $grid.isotope();
		
			$(".forfilterborder").on("change", function(jQEvent){
			var gotvalue;
			var getfilter1;
			var getfilter2;
			var getfilter3;
			var values = [];  
			var flooring = [];
			var project = [];
			var combofilternew = [];
			$(".select-filter1 option:selected").each(function () {
			   var $this = $(this);
			   if ($this.length) {
				getfilter1 = String('.'+$this.val());
				//console.info(getfilter1);
				values.push(getfilter1);
			   }
			});
			$(".select-filter2 option:selected").each(function () {
			   var $this = $(this);
			   if ($this.length) {
				getfilter2 = String('.'+$this.val());
				//alert(getfilter2)
				//console.info(getfilter2);
				flooring.push(getfilter2);
			   }
			});
			$(".select-filter3 option:selected").each(function () {
			   var $this = $(this);
			   if ($this.length) {
				getfilter3 = String('.'+$this.val());
				//console.info(getfilter3);
				project.push(getfilter3);
			   }
			});
			var $combine = values; //not sure how to combine all value like this (ok,good), or array(ok,good) acceptable also
			var $floor = flooring;
			var $project = project;
			
			
			
			var final = $combine + $floor + $project;
			var d = String([].concat.apply([], [$combine, $floor, $project]));
			combofilternew.push(final);
			var $getfull = combofilternew;
			//var flatArray = [].concat.apply([], $getfull);
			$getfull = $getfull.join(',');
			//combofilternew.push(final);
			//final = '.Lambeth,.Southwark';
			// var $checkbox = $( jQEvent.target );
			//manageCheckbox( $checkbox );
			//alert(final);
			//alert(d);
			//final = getComboFilter( filters );
			$grid.isotope({ filter: d });
		
			
			  if ( !$grid.data('isotope').filteredItems.length ) {
				  //$('#msg-box').show();
				 $(".noresult").show();
				}
			else {
				$(".noresult").hide();
			}
			$(".resetborough p").click(function(){
				//alert($combine);
				var newd = String([].concat.apply([], [$floor, $project]));
				//alert(newd);
				$grid.isotope({ filter : newd });
				$(".getfilter1 .options li").each(function(index, element) {
					$(this).removeClass("selected");
				});
				$(".getfilter1 .CaptionCont span").text("Borough");
				$(this).fadeOut(300);
				});
			$(".resetfloor p").click(function(){
				//alert($combine);
				var newd2 = String([].concat.apply([], [$combine, $project]));
				//alert(newd);
				$grid.isotope({ filter : newd2 });
				$(".getfilter2 .options li").each(function(index, element) {
						$(this).removeClass("selected");
					});
					
					$(".getfilter2 .CaptionCont span").text("Floor Area");
					$(this).fadeOut(300);
				});
			$(".resetprotype p").click(function(){
				//alert($combine);
				var newd3 = String([].concat.apply([], [$floor, $combine]));
				//alert(newd);
				$grid.isotope({ filter : newd3 });
				$(".getfilter3 .options li").each(function(index, element) {
					$(this).removeClass("selected");
				});
				
				$(".getfilter3 .CaptionCont span").text("Project Type");
				$(this).fadeOut(300);
				});		
		});
			
			function createContent() {
				
				  var borough = $(data.borough);
				  var area;
				  var catego;
				  var items = '';
				  //alert(data.borough.length);
				  // dynamically create content
				  for (var i=0, len1 = $(borough).length; i < len1; i++) {
					borough = data.borough[i];
					for (var j=0, len2 = data.area.length; j < len2; j++) {
					  area = data.area[j];
						for (var l=0, len3 = data.catego.length; l < len3; l++) {
						catego = data.catego[l];
						
						 
						
					  }
					}
				  }
				
				 // $grid.append( items );
				}
				
			function getComboFilter( filters ) {
			  var i = 0;
			  var comboFilters = [];
			  var message = [];
			
			  for ( var prop in filters ) {
				message.push( filters[ prop ].join(' ') );
				var filterGroup = filters[ prop ];
				// skip to next filter group if it doesn't have any values
				if ( !filterGroup.length ) {
				  continue;
				}
				if ( i === 0 ) {
				  // copy to new array
				  comboFilters = filterGroup.slice(0);
				} else {
				  var filterSelectors = [];
				  // copy to fresh array
				  var groupCombo = comboFilters.slice(0); // [ A, B ]
				  // merge filter Groups
				 
				  for (var k=0, len3 = filterGroup.length; k < len3; k++) {
					for (var j=0, len2 = groupCombo.length; j < len2; j++) {
					  filterSelectors.push( groupCombo[j] + filterGroup[k] ); // [ 1, 2 ]
					   alert(groupCombo);
					}
			
				  }
				  // apply filter selectors to combo filters for next group
				  comboFilters = filterSelectors;
				}
				i++;
			  }
			
			  var finalnew = comboFilters.join(', ');
			 
			  //return final;
			 //  alert(final);
			}	
		
		function manageCheckbox( $checkbox ) {
			  var checkbox = $checkbox[0];
			
			  var group = $checkbox.parents('.option-set').attr('data-group');
			  // create array for filter group, if not there yet
			  var filterGroup = filters[ group ];
			  if ( !filterGroup ) {
				filterGroup = filters[ group ] = [];
			  }
			
			  var isAll = $checkbox.hasClass('all');
			  // reset filter group if the all box was checked
			  if ( isAll ) {
				delete filters[ group ];
				if ( !checkbox.checked ) {
				  checkbox.checked = 'checked';
				}
			  }
			  // index of
			  var index = $.inArray( checkbox.value, filterGroup );
			
			  if ( checkbox.checked ) {
				var selector = isAll ? 'input' : 'input.all';
				$checkbox.siblings( selector ).removeAttr('checked');
			
			
				if ( !isAll && index === -1 ) {
				  // add filter to group
				  filters[ group ].push( checkbox.value );
				}
			
			  } else if ( !isAll ) {
				// remove filter from group
				filters[ group ].splice( index, 1 );
				// if unchecked the last box, check the all
				if ( !$checkbox.siblings('[checked]').length ) {
				  $checkbox.siblings('input.all').attr('checked', 'checked');
				}
			  }
			  
			
			}
		//$('select.custom-filter').niceSelect();
    });
	});
</script>