<?php

require_once('./inc/header.inc.php');
?>

<div class="full">
<h1>build team tv – see our work on side extensions</h1><br/>

<?php
$i = 1;
$ret = '';

$rs = getRs("SELECT title, url FROM tv WHERE is_active = 1 ORDER BY weight DESC");

$cols = 2;
$project_count = 0;
$prev_count = 0;

while ($row = mysqli_fetch_assoc($rs) ) {
	if ( $i == 1 ) {
		//echo '<div id="bc"><a href="/index.html">Home</a> &rsaquo; <a href="/our-experience.html">Our Experience</a> &rsaquo; <b>' . $row['project_category_name'] . '</b></div>';
	}
	
		
		$ret .= '<'.'div class="tv_video col_half'. ($i%2==1 ? '' : ' col_last') .'" style="text-align:center';
    
    $url = $row['url'];
    
    if (strpos($url, 'vimeo.com')) {
      $url = explode('/', $url);
      $url = array_pop($url);
      if (!$url) $url = array_pop($url);
      $url = '//player.vimeo.com/video/'.$url.'?title=0&amp;byline=0&amp;portrait=0';
    }
    
    // href="/projects/' . $row['filename'] . '" rel="shadowbox[project_' . $row['project_code'] . ']"
		$ret .= '">'.
    '<iframe src="'.$url.'" width="447" height="251" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>'.
    '<span style="display:block;margin-top:5px">'.htmlentities($row['title']).'</span></div>'; // debug: ' '.$project_count
    
    /*
    if (0==($project_count % $cols) && $project_count!=$prev_count) {
      $ret .= '<br clear="all" />'; // debug: ($project_count % $cols).'!!!
      $prev_count = $project_count;
    }
    */
    
	$i += 1;
}

echo $ret;

?>

<br clear="all" />
<!--<p style="text-align:justify">
Our online galleries display our work for different loft conversions, side extensions and building refurbishments in London, but getting to know us will increase your confidence in hiring us. We ensure you we are a professional construction company in London, dedicated to our work, passionate about how we architect your home, how the house building takes place and the house decorating ideas we propose. Building side return kitchen extensions is our specialty and building modern kitchens is something we do best, but we do not shy away from other type of house building or refurbishment project. Please meet the team and their work, in a more cursive, fluent way, by watching our videos. Get to know our engineers, architects, interior designers and house builders better by inviting them to survey your home and propose modern house plans. Obtain a free instant online quote for the type of work you have in mind. Extension costs can be estimated without an on-site evaluation. 
</p>-->

</div>

<?php

require_once('./inc/footer.inc.php');

?>