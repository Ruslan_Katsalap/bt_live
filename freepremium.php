<?php
header("");
require_once('./inc/header.inc.php');
?>
<link href="/css/book-visit.css?v=1" rel="stylesheet">

<?php
$flagenew = false;
global $a_settings;
$newflaging = false;
require_once('./inc/settings.php');


/*if (!isset($_GET['type']) || !in_array($_GET['type'], array_keys($a_settings['a_visit_types']))) {
  header('Location: /getting-started/book-site-visit-new.html');
  exit();
}*/

$error = "";

@session_start();

if (isset($_SESSION[WISE_DB_NAME]['book_visit']['fname']) && !isset($_POST['nonce'])) {

  //echo '!!!<pre>'.print_r($_SESSION[WISE_DB_NAME]['book_visit'], 1).'</pre>';#debug

  $_POST['fname'] = $_SESSION[WISE_DB_NAME]['book_visit']['fname'];
  $_POST['email'] = $_SESSION[WISE_DB_NAME]['book_visit']['email'];
  $_POST['phone'] = $_SESSION[WISE_DB_NAME]['book_visit']['phone'];
  $_POST['coupon_code'] = $_SESSION[WISE_DB_NAME]['book_visit']['coupon_code'];
  $_POST['postcode'] = $_SESSION[WISE_DB_NAME]['book_visit']['postcode'];
  $_POST['addr'] = $_SESSION[WISE_DB_NAME]['book_visit']['addr'];
  $_POST['comments'] = $_SESSION[WISE_DB_NAME]['book_visit']['comments'];
  $_POST['availibility'] = isset($_SESSION[WISE_DB_NAME]['book_visit']['availibility'])?$_SESSION[WISE_DB_NAME]['book_visit']['availibility']:'';
  //$mystring = $_GET['coupon_code'];
  //echo $mystring;

  if ($_POST['availibility']) {
    $o_avail = json_decode($_POST['availibility']);
    $a_pref_dates = array();
    $availibility = '';
    if ($o_avail) {
      foreach ($o_avail AS $k => $v) {

        $k = explode('/', $k);
        $k = $k[1] . '.' . $k[0] . '.' . $k[2];
        $availibility .= $k.' - '.strtoupper($v)."\n";

        $a_pref_dates[$k] = $v;
      }

      $availibility = trim($availibility);

      //echo '<pre>'.$availibility.'!!!'.print_r($o_avail, 1).'</pre>';#debug
    }
  }

}

if (isset($_POST['nonce']) && $_POST['nonce']==session_id()) {

  // validation, set values:

  $k = 'fname';
  $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
  if (!$_POST[$k]) $error .= "\n"; //enter Full name

  $k = 'coupon_code';
  $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
  if (!$_POST[$k]) $error .= "\n"; //enter code

  $k = 'email';
  $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
  if (!$_POST[$k]) $error .= "\n"; //enter Your email\n
  else {
    if (!preg_match("/^[a-z0-9_\.-]+@([a-z0-9]+([\-]+[a-z0-9]+)*\.)+[a-z]{2,7}$/i", $_POST[$k])) {
      $error .= "&nbsp;"; //enter correct Email address
    }
  }

  $k = 'phone';
  $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
  if (!$_POST[$k]) $error .= "\n"; //enter Contact number
  else {
    if (!preg_match("/^[0-9\.\-\s\(\)]{10,}$/i", $_POST[$k])) {
      $error .= "\n"; //enter correct Contact number (at least 10 digits)
    }
  }


  $k = 'postcode';
  $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
  if (!$_POST[$k]) $error .= "\n"; //enter Postcode
  else {

    $_POST[$k] = strtoupper($_POST[$k]);

    $postcode = $_POST[$k];

    include BT_PATH.'xml/postcode.inc.php';

    $valid = bypValidatePostcode($postcode, 'session');

    if (!$valid) {
      $error .= "\n"; //enter correct UK Postcode
    }
    else { // if valid, check territory:

      $rs = getRs("SELECT map_postcodes FROM setting s WHERE s.setting_id = 1");

      $map_postcodes = array();
      $out_of_territory = false;
      $row = mysqli_fetch_assoc($rs);
      if (isset($row['map_postcodes'])) {
        $map_postcodes = explode(',', $row['map_postcodes']);
      }

      //echo '<pre>'.print_r($map_postcodes, 1).'</pre>';#debug
      //exit;//debug

      $postpart = $postcode;
      $postpart = str_replace(' ', '', $postpart);

      if ($map_postcodes) {

        if (strlen($postpart)<6 && strlen($postpart)>4) $postpart = substr($postpart, 0, 2);
        elseif (strlen($postpart)==6) $postpart = substr($postpart, 0, 3);
        else $postpart = substr($postpart, 0, 4);

        //echo print_r($map_postcodes, 1).' '.$postpart.':)';#debug

        $cover = false;
        foreach ($map_postcodes AS $v) {
          $v = trim($v);
          if (strtoupper($postpart) === strtoupper($v)) {
            $cover = true;
            break;
          }
        }

        if (!$cover) $out_of_territory = true;

      }

      if ($out_of_territory) {
	  //$flagenew = true;
	  $error .= "\n";
	  $flagenew = true;
	  }

    }
  }

  $k = 'addr';
  $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';

  if (!$_POST['addr']) {
    $error .= "\n"; //enter Your House / Flat No & Street
  }

  $k = 'availibility';
  $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
  if (!$_POST['availibility']) {
    $error .= "select at least one Your Availibility Date\n";
  }
  else {
    $o_avail = json_decode($_POST['availibility']);
    $a_pref_dates = array();
    $availibility = '';
    if (!$o_avail) $error .= "select at least one Your Availibility Date\n";
    else {
      foreach ($o_avail AS $k => $v) {
        $k = explode('/', $k);
        $k[2] = substr($k[2], 2, 3);
        $k = $k[1] . '.' . $k[0] . '.' . $k[2];
        $availibility .= $k.' - '.strtoupper($v)."\n";
        $a_pref_dates[$k] = $v;
      }
      $availibility = trim($availibility);
      //echo '<pre>'.print_r($o_avail, 1).'</pre>';#debug
      if (!$a_pref_dates) $error .= "select at least one Your Availibility Date\n";
    }
  }

  $k = 'comments';
  $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
  $_POST[$k] = substr($_POST[$k], 0, 500); // limit comment length, short comment

  if (!$error) {
    $visit_id = 0; // order ID, 0 means add new order

    if (isset($_SESSION[WISE_DB_NAME]['book_visit']['visit_id'])) $visit_id = $_SESSION[WISE_DB_NAME]['book_visit']['visit_id'];

    $_SESSION[WISE_DB_NAME]['book_visit'] = array(
      'fname' => $_POST['fname'],
      'email' => $_POST['email'],
      'phone' => $_POST['phone'],
	  'coupon_code' => $_POST['coupon_code'],
      'postcode' => $_POST['postcode'],
      'addr' => $_POST['addr'],
      'comments' => $_POST['comments'],
      'availibility' => $_POST['availibility'],
      'product_code' => $_GET['type'],
	  'visit_type' => 'Free Premium'
    );


/**************This is CRM integration code*****************/
/* require_once($_SERVER["DOCUMENT_ROOT"]."/crm_config.php");
 list($name,$last_name) =  explode(" ",$_POST['fname']);
 $full_name =  $name.' '.$last_name;
 $now = strftime("%Y-%m-%dT%H:%M:%S");

 $comment = "\r\n Availability: ".$_POST['availibility'];
 $comment.= "\r\n Product Code: ".$_GET['type'];
 $comment.= "\r\n Comment: ".$_POST['comments'];

 $data = array(
    "NAME" =>  $name,
    "LAST_NAME" => $last_name,
    "UF_CRM_1490175334" => $now,
    "EMAIL_HOME" => $_POST['email'],
    "PHONE_HOME" =>$_POST['phone'],
    "UF_CRM_1500629522" => 146,
    "ADDRESS" => $_POST['addr']."\r\n ".$_POST['postcode'],
    "SOURCE_DESCRIPTION" => "BT",
    "TITLE" => $full_name,
    "COMMENTS" => $comment
  );
  $visit_type = "Premium";
  if(!empty($_POST['coupon_code'])) {
		$cp = getRs("SELECT * FROM coupon WHERE coupon_code='".$_POST['coupon_code']."'");
		if ($cw = mysqli_fetch_assoc($cp)) {
			if($cw["is_enabled"]) {
				$visit_type = "Free Premium";
			}
		}
  }
   CrmClient::$SOURCE_ID = 'WEB_FORM';

  file_put_contents($_SERVER['DOCUMENT_ROOT'].'/log.txt',print_r($data,1));

  if(!CrmClient::sendToCRM($data)) {
     echo 'error send to crm';
  }*/
 /**************This is CRM integration code**************** */
   $visit_id = 0;
    if ($visit_id) {
      $sql = "UPDATE visit SET full_name='".q($_POST['fname'])."', email='".q($_POST['email'])."', phone='".q($_POST['phone'])."', coupon_code='".q($_POST['coupon_code'])."', postcode='".q($_POST['postcode'])."', addr='".q($_POST['addr'])."', comments='".q($_POST['comments'])."', availibility='".q($availibility)."', visit_type='".$visit_type."', amount=".(int)$a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat'].", date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=1 WHERE visit_id=".(int)$visit_id;
      query($sql);
	  $newflaging = true;
    }
    else {
      $sql = "INSERT INTO visit SET full_name='".q($_POST['fname'])."', email='".q($_POST['email'])."', phone='".q($_POST['phone'])."', coupon_code='".q($_POST['coupon_code'])."', postcode='".q($_POST['postcode'])."', addr='".q($_POST['addr'])."', comments='".q($_POST['comments'])."', availibility='".q($availibility)."', visit_type='".$visit_type."', amount=".(int)$a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat'].", date_created=UNIX_TIMESTAMP(), date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=1";
      query($sql);
	  $visit_id = mysql_insert_id();
	  $newflaging = true;
    }
    $_SESSION[WISE_DB_NAME]['book_visit']['visit_id'] = $visit_id;
    ob_start();
 }

}
ob_start();
require_once('./inc/header.inc.php');
?>


<style type="text/css">
body {
	/*background:url(http://www.buildteam.com/images/contact/new_sv_bg.jpg) no-repeat center;*/
	background-size:cover;
	}
	.darkblue-box, .lightblue-box {
		width: 115px;
		margin-right: 10px;
	}

	.top_part {
    background: #fff;
}



	.main {
   background:rgba(255,255,255,1);
	padding-left: 50px !important;
    padding-right: 50px !important;
    box-sizing: border-box !important;
	padding-bottom:50px !important;
	padding: 100px 50px 100px 50px !important;
	max-width:1080px !important;
	}


	body
	{
		background-attachment:fixed;
		background-size:cover;
		text-align: left !important;
	}

	footer.round-2 {
    margin-top: 0px;
	}

	.page-title
	{
    font-size: 30px;
    text-align: left;
    font-weight: bold;
    color: #0f4778;
    border-bottom: 2px solid #d7d7d7;
    padding-bottom: 11px;
	margin-bottom: 20px;
	}

	p
	{
    text-align: left;
    font-size: 16px;
    font-weight: 400;
	text-align:justify;
	color:#7b7b7b;
	//font-size: 20.5px;
	line-height: 24px;
	font-family: roboto !important;
	}

	h3 {
    color: #4b759a !important;
    text-align: left;
    font-weight: 400 !important;
	}

	input#postcode {
    padding: 7px;
    margin-top: 10px;
    width: 210px;
    border: 2px solid #d5d5d5;
}

.book-a-site-table {
    margin-top: 30px;
}
.options-content
{
    visibility: visible;
    max-width: 100% !important;
    margin-top: 20px;
    margin-bottom: 0px;
}

.book-a-site-table th {
    padding: 8px 0px;
    font-size: 21px;
    border-top: 2px solid #d7d7d7;
    //border-bottom: 1px solid #d7d7d7;
    color: #4b759a;
    font-weight: normal;
	text-align:inherit;
}
th
{
		text-align:inherit;
}


.book-a-site-table td {
    padding: 8px 0px;
    font-size: 19px;
	color:#7d7d7d;
	font-weight:normal;
}

.tick
{
	background-image:url(../images/tick.png);
		background-position:center;
		background-repeat:no-repeat;

}

.tick-star
{
	background-image:url(../images/tick-star.png);
		background-position:center;
		background-repeat:no-repeat;

}

tr
{
	border-top:1px solid #e0e0e0;
}

.postcode{
    display: inline;
    padding-left: 7px;
    text-transform: uppercase;
}

.up_down
{
	display:none;
}
.kaction
{
	opacity:1;
}
.post-continue {
    cursor: pointer;
    font-size: 20px;
    color: #ff8f4f;
    padding-top: 9px;
    font-weight: normal;
}


.foot-note {
    width: 200px;
    height: 80px;
    position: absolute;
    background: url(../images/foot-note.png);
    right: -218px;
    bottom: 15px;
	display:none;
}
#termstext {
	font-size:14px;
	}

@media only screen and (max-width: 480px)
{
	.main {
    padding-left: 15px !important;
    padding-right: 15px !important;
    max-width: 1080px;
    width: 100%;
}

.full p {
    margin: 0 0 10px;
    font-size: 12px;
    line-height: inherit !important;
}
.full h3
 {
    font-size: 17px;
}

.book-a-site-table th
{
	font-size:11px;
}
.book-a-site-table td {
    font-size: 10px;
	max-width:140px;
}
#termstext {
	font-size:10px;
	}
}
.main {
	padding-bottom: 0 !important;
}
.main.tborder{
	padding-top:0 !important;
	padding-bottom: 100px !important;
}


</style>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<style type="text/css">

.options-content
{
    visibility: visible;
    max-width: 100% !important;
    margin-top: 20px;
    margin-bottom: 0px;
}

.book-a-site-table th {
    padding: 8px 0px;
    font-size: 21px;
    border-top: 2px solid #d7d7d7;
    //border-bottom: 1px solid #d7d7d7;
    color: #4b759a;
    font-weight: normal;
	text-align:inherit;
}
th
{
		text-align:inherit;
}


.book-a-site-table td {
    padding: 8px 0px;
    font-size: 19px;
	color:#7d7d7d;
	font-weight:normal;
}

.tick
{
	background-image:url(../images/tick.png);
		background-position:center;
		background-repeat:no-repeat;
}

.tick-star
{
	background-image:url(../images/tick-star.png);
		background-position:center;
		background-repeat:no-repeat;
}

tr
{
	border-top:1px solid #e0e0e0;
}

.postcode{
    display: inline;
    padding-left: 7px;
    text-transform: uppercase;
}

.up_down
{
	display:none;
}
.kaction
{
	opacity:0.5;
}
.book-wrap
{
border:none !important;
padding-top: 15px;
}

.book-wrap input[type=text],
.book-wrap textarea
{
	width:100%;
	margin-top:5px;
	border:1px solid #8b8f94;
	margin-bottom:10px;
	background:none;
	max-height:32px;
}
.book-wrap .ui-widget.ui-widget-content {
	background:none !important;
	}

label,
span.red
{
	font-size: 18px;
}

.ui-datepicker-prev
{
	display:none;
}

.ui-datepicker .ui-datepicker-title {
    margin: 0 2.3em;
    line-height: 1.8em;
    text-align: left;
    margin-left: 0px;
    font-size: 21px;
    font-weight: normal;
    color: #4b759a;
    margin-top: 6px;
    border-bottom: 1px solid #d7d7d7;
    padding-bottom: 5px;
    width: 100%;
}
.ui-datepicker-year
{
	display:none;
}

.ui-datepicker th {
    color: #8b8f94;
    padding: 5px .3em;
    text-align: left;
    font-weight: normal;
    border: 0;
    font-size: 19px;
    border-bottom: 1px solid #d7d7d7;
    padding-bottom: 8px;
}


.ui-datepicker td span,
.ui-datepicker td a
 {
    font-size: 20px;
	line-height:0px;
	 padding-bottom: 12px;
    height: 39px;
    padding-top: 17px;
	box-sizing:border-box !important;
	border: 2px solid transparent !important;
	}

.ui-datepicker .ui-datepicker-next {
    right: 0px;
    top: 10px;
}

a.ui-state-default {
    color: #8b8f9a !important;
}


.ui-datepicker .ui-datepicker-calendar .dt_pick1 a {
    border: 2px solid #ed7401 !important;
    line-height: 0px;
    padding-bottom: 12px;
    height: 39px;
    padding-top: 17px;
    background: none !important;
    border-radius: 8px;
	box-sizing:border-box !important;
}

.ui-datepicker .ui-datepicker-calendar .dt_pick2 a
{
	border: 2px solid #7c7c7c !important;
    line-height: 0px;
    padding-bottom: 12px;
    height: 39px;
    padding-top: 17px;
    background: none !important;
    border-radius: 8px;
	box-sizing:border-box !important;
}

.ui-datepicker .ui-datepicker-calendar .dt_pick3 a
{
	border: 2px solid #324875 !important;
    line-height: 0px;
    padding-bottom: 12px;
    height: 39px;
    padding-top: 17px;
    background: none !important;
    border-radius: 8px;
	box-sizing:border-box !important;
}
.ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active, a.ui-button:active, .ui-button:active, .ui-button.ui-state-active:hover {
    background: none;
border-none;
}

.ui-state-default, .ui-widget-content .ui-state-default
{
	border: 2px solid transparent !important;
}

span.ui-icon.ui-icon-circle-triangle-e {
    background: url(../images/date-right-arrow.png) !important;
    font-size: 0px !important;
    height: 30px !important;
    width: 30px !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
}

span.date-right-arrow {
    background: url(../images/date-right-arrow.png) !important;
    font-size: 0px !important;
    height: 16px !important;
    width: 30px !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
    display: inline-block;
    margin-top: 5px;
}

.choice-table tr, .choice-table td {
    vertical-align: top;
    padding: 10px 0px;
	font-weight: normal;
}
table.choice-table {
    margin-top: 10px;
    padding-top: 10px;
    display: block;
	    height: 290px;
    border-bottom: 1px solid #D7D7D0;
}

.ui-datepicker td.ui-state-disabled span {
    background-color: #fff;
}


a.book-now, a.book-now-btn {
  -webkit-border-radius: 6;
  -moz-border-radius: 6;
  border-radius: 6px;
  font-family: Arial;
  color: #ffffff;
  font-size: 13px;
  background:#0f4778;
  padding: 10px 30px 10px 30px;
  text-decoration: none;
  height:auto !important;
  float: left !important;
}

a.book-now:hover, a.book-now-btn:hover {
  background: #5d7c98;
  text-decoration: none;
}
#pref_dates > li {
	min-height:35.5px;
	}

table.ui-datepicker-calendar td {
    padding: 7px 0px;
}
.book-wrap .ui-widget.ui-widget-content {
    position: relative !important;
    z-index: 1 !important;
	border:none !important;
}

.ui-state-highlight, .ui-widget-content .ui-state-highlight
{
	background:none !important;
	border:none !important;
}

.ui-datepicker .ui-datepicker-calendar .ui-state-highlight a {
	background:none !important;
	}

#pref_dates > li {
    width: 100%;
    padding-left: 30px;
}

#pref_dates li i
{
	display:none !important;
}

#pref_dates li span
{
	border:none !important;
}

#pref_dates li span em {
    display: block;
    float: left;
    font-style: normal;
    padding: 5px;
    padding-left: 0;
    width: 71px;
}

#pref_dates li span ul
{
	border-left:0px;
}

#pref_dates li span ul li.date_am {
    border-top-left-radius: 0px !important;
    border-bottom-left-radius: 0px !important;
}

#pref_dates {
    padding-left: 0px;
    position: absolute;
    top: 10px;
}

#pref_date_1 {

}

li#pref_date_2 {




}
#pref_dates span.removedates {
    position: absolute;
    right: 0px;
    font-size: 22px !important;
    color: rgb(237, 116, 1);
	cursor:pointer;
}
#pref_dates #pref_date_1, #pref_dates #pref_date_2 {
	border-bottom: 1px solid #dedede;
	}
#pref_dates .fordynamic:nth-child(1) {
	margin-top: 0px;
    margin-bottom: 0px !important;

	    padding-bottom: 7px;
	}
#pref_dates .fordynamic:nth-child(2){
	 margin-top: 10px;
	 margin-bottom: 0px !important;
	  padding-bottom: 6px;
	  height:36px;

	}
 #pref_dates .fordynamic:nth-child(3) {
	 margin-top: 10px;
	 margin-bottom: 0px !important;
	}
li#pref_date_3 {


}

.choice-table tr, .choice-table td
{
	border:none !important;
}



.book-wrap a.book-now {
    margin-top: -58px;
    margin-left:0px;
}


.book-wrap .ui-widget.ui-widget-content {

    border-bottom: 1px solid #d7d7d7 !important;

}
	.error p {
	padding-top:0px !important;
	display:none;
	}
.error {
	margin-top:0px !important;
	}
.enquirywrap label p {
	padding-top:10px;
	}
#popupform .carousel-control span {
	text-shadow:none !important;
	}
button.ui-dialog-titlebar-close {
	background: url(/images/close.jpg) no-repeat center !important;
    height: 20px !important;
    top: 16px !important;
    background-size: contain !important;
    border: none;
    width: 20px !important;
	z-index:2;
	}
.promptimage {
    position: absolute;
    top: 22%;
    right: -1502px;
	display:none;

}
.comefromleft {
	right:-152px !important;
	display:block !important;

	animation:newani 0.3s;
	}

.closethis {
    position: absolute;
    right: 0px;
    top: 10px;
}
.closethis a {
    background: none;
    color: #104674;
    font-size: 24px;
    font-weight: 600;
    line-height: 18px;
}
.closethis a:hover{
	text-decoration:none;
	}
@-webkit-keyframes newani {
	0% {
		right:-165px;
		}
	100% {
		right:-152px;
		}
	}
.promptimage img {
}
#loadingif img {
	max-width:80px;

	}
#loadingif {
	position: absolute;
    bottom: 25px;
    left: 150px;
	display:none;
	}
@media only screen and (min-width: 480px)	{
.timecont .pertime, .daycont .pertiday {
	width:46px !important;
	}
.popformfields:nth-child(3) {
	margin-top:10px !important;
	margin-bottom:10px !important;

	}
.popformfields {
	margin-bottom:5px !important;
	}
#tabelwidth {
	width:80%;
	}
}

@media only screen and (max-width: 480px)
{

	.main {
    padding-left: 15px !important;
    padding-right: 15px !important;
    max-width: 1080px;
    width: 100%;
}

.full p {
    margin: 0 0 10px;
    font-size: 12px;
    line-height: inherit !important;
}
.full h3
 {
    font-size: 17px;
}

.page-title {
    font-size: 21px;
    text-align: left;
    font-weight: bold;
    color: #0f4778;
    border-bottom: 1px solid #d7d7d7;
    padding-bottom: 15px;
    margin-bottom: 0px;
    margin-top: 0px;
}

.book-wrap .ui-widget.ui-widget-content {
    position: relative !important;
    z-index: 1 !important;
    border: none !important;
    padding-left: 0px !important;
    margin-left: 0px;

}



.ui-datepicker-calendar td
{
	padding:0px !important;
}
.ui-datepicker-calendar td span,
.ui-datepicker-calendar td a,
.ui-datepicker-calendar th span,
.ui-datepicker-calendar th a
{
    font-size: 15px;
    padding: 2px 0px !important;
    height: 28px !important;
	    line-height: 20px !important;
}

.ui-widget.ui-widget-content
{
	max-width:100% !important;
}


.ui-datepicker th {
    font-size: 10px !important;
}
.new_width {
	width:150px;
	}
#pref_dates > li {
	padding-left:0px !important;
	}
form#fm_book_visit {
    display: -webkit-box;
    display: -moz-box;
    display: -ms-flexbox;
    display: -webkit-flex;
    display: flex;
    -webkit-box-orient: vertical;
    -moz-box-orient: vertical;
    -webkit-flex-direction: column;
    -ms-flex-direction: column;
    flex-direction: column;
    -webkit-box-align: start;
    -moz-box-align: start;
    -ms-flex-align: start;
    -webkit-align-items: flex-start;
    align-items: flex-start;
}
.col-sm-6.your-dates {
    -webkit-box-ordinal-group: 2;
    -moz-box-ordinal-group: 2;
    -ms-flex-order: 2;
    -webkit-order: 2;
    order: 2;
}

.col-sm-6.clalendargetup {
	 -webkit-box-ordinal-group: 1;
    -moz-box-ordinal-group: 1;
    -ms-flex-order: 1;
    -webkit-order: 1;
    order: 1;
	}
#tabelwidth {
	width:60%;
	}
.bookthanksinner {
	height:200px !important;
	padding: 20px 10px !important;
    width: 96% !important;
	}
.bookthanksinner h4 {
	font-size:20px !important;
	padding-top: 5px;
	margin-top: -10px;
	}

}
.bookthanks .bookthanksinner {
	height:auto !important;
}
.coupon-header {
	padding: 30px 0;
	background: #0f4778;
	font-family:roboto;
}
.coupon-header .full {
	max-width: 1080px !important;
	margin: 0 auto;
}
.coupon-header .full .content-area {
	padding: 0 50px;
}
.coupon-header .full .content-area .box-c {
	padding: 6px 12px;
	border: 1px solid #fff;
	float: left;
	margin-right: 10px;
}
.coupon-header .full .content-area .box-c input {
	float: left;
    //max-width: 16px;
	width: 300px;
    font-size: 20px;
    padding: 0 0px;
    text-align: left;
    background: transparent;
    color: #fff;
    border: none;
    border-bottom: 1px solid #fff;

}
.coupon-header .full .content-area .box-c input:focus{
	outline:none;
}
.coupon-header .full .content-area .bcontent{
	font-size:24px;
	color:#fff;
	padding-bottom: 15px;
	font-weight: 300;
	text-align:left;
}
.coupon-header .full .content-area a.book-now-btn{
	background: transparent;
    font-size: 23px;
    border: 1px solid #fff;
    border-radius: 12px;
    padding: 5px 15px 2px 15px !important;
	width:auto !important;
	font-family:roboto;
	font-weight:500;
}
.coupon-header .full .content-area .tleft{
	float:left;
}
.coupon-header .full .content-area .tleft p{
	font-size:24px;
	color:#fff;
}
.coupon-header .full .content-area .tright{
	float:right;
}
.coupon-header .full .content-area .tright p{
	font-size:30px;
	color:#fff;
	font-weight:500;
	padding: 0px 15px 0px 15px !important;
    margin: 0px;
}

.coupon-header .full .content-area .apply{
	margin-top: 30px;
}
.coupon-header .full .content-area .tleft p.danger-msg , .coupon-header .full .content-area .tleft p.success-msg {
	color: #fff;
	padding: 5px 0 0 20px;
	float: left;
	display: none;
}
.coupon-header .full .content-area .tleft p.success-msg {
	color: #fff;
}
@media only screen and (max-width: 480px){
	.coupon-header .full .content-area {
		padding: 0 14px !important;
	}
}
@media only screen and (max-width: 671px){
	.coupon-header .full .content-area a.book-now-btn, .coupon-header .full .content-area .tright p {
		font-size: 16px;
	}
	.coupon-header .full .content-area .box-c input {
		max-width: 130px;
		font-size: 12px;
	}
	.coupon-header .full .content-area {
		padding: 0 80px;
	}
	.coupon-header .full .content-area .box-c
	{
	    margin-top: 10px;
		padding: 4px 9px;
	}
	.coupon-header .full .content-area .bcontent {
		font-size: 16px;
	}
}
@media only screen and (min-width: 769px){
.book-wrap .col-sm-4{
	padding-left: 0;
	padding-right: 0;
}
.book-wrap form .col-sm-4.first-child{
	padding-right: 15px;
}
.book-wrap form .col-sm-4.last-child{
	padding-left: 15px;
}
.book-wrap .col-sm-6.your-dates{
	padding-left:0px;
}
.book-wrap .col-sm-6.clalendargetup{
	padding-right:0px;
}
}
@media only screen and (max-width: 768px){
.book-wrap .col-sm-4, .book-wrap .col-sm-6{
	padding-left: 0;
	padding-right: 0;
}
.coupon-header .full .content-area .tleft p.danger-msg, .coupon-header .full .content-area .tleft p.success-msg {
	padding: 0px 0 0 20px;
	font-size: 16px;
}
.coupon-header {
	padding: 5px 0 20px 0;
}
}

</style>

<div class="full">
<h2 class="page-title">Book your Premium Site Visit</h2>
</div>
</div>

<div class="coupon-header">
	<div class="full">
		<div class="content-area">
		<form id="coupon-form">
		<p class="bcontent">Please enter the code on the back of your flyer to get your Free Premium Site Visit</p>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="40" id="cp-code" name="cp-code1" data-key="1">
				</div>
			</div>
		<!--	<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code2" data-key="2">
				</div>
			</div>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code3" data-key="3">
				</div>
			</div>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code4" data-key="4">
				</div>
			</div>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code5" data-key="5">
				</div>
			</div>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code6" data-key="6">
				</div>
			</div>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code7" data-key="7">
				</div>
			</div><div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code8" data-key="8">
				</div>
			</div>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code9" data-key="9">
				</div>
			</div>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1"  name="cp-code10"data-key="10">
				</div>
			</div>
			<div class="box-row">
				<div class="box-c">
					<input type="text" maxlength="1" name="cp-code11" data-key="11">
				</div>
			</div>
			-->
			<div style="clear:both"></div>
			<div class="apply">
				<div class="tleft">
					<a href="javascript:void(0)" class="book-now-btn">APPLY </a><p class="c-msg"></p>
				</div>
				<div class="tright">
				<p id="coupon-price-t">£150</p>
				</div>
			</div>
			<div style="clear:both"></div>
			</form>
		</div>

	</div>

</div>



<div class="main tborder">
<div class="full">
<?php
$ns = getRs("SELECT * FROM page WHERE page_id=52 AND is_enabled=1 limit 1");
if ($nw = mysqli_fetch_assoc($ns)) {
    $page_content = $nw['page_content'];
    echo $page_content;}?><!--<h3>Enter your postcode to find out what site visits are available in your area:</h3>
<input type="text" id="postcode" placeholder="E.g. CR0 9JJ..." minlength="6" maxlength="8">
<br>
<div class="post-continue">Continue</div>-->
<div class="options-content"></div>

<div class="book-wrap">
          <form method="POST" id="fm_book_visit" name="fm_book_visit" onsubmit="$('#loadingif').show();">
			<input type="hidden" name="coupon_code" value="" id="coupon_code"/>
            <input type="hidden" name="nonce" value="<?php echo session_id() ?>" />
            <div class="col-sm-12">

              <?php if ($error) echo '<div class="error"><p>Please fill out the following information:<br/><br/>'.nl2br(htmlspecialchars($error, ENT_QUOTES, 'UTF-8')).'</div>'; ?>

            </div>
            <div class="col-sm-4 first-child">
              <label>Full name<span class="red">*</span></label>
              <input type="text" name="fname" value="<?php if (isset($_POST['fname'])) echo htmlspecialchars($_POST['fname'], ENT_QUOTES, 'UTF-8') ?>" />

              <label>Email<span class="red">*</span></label>
              <input type="text" name="email" value="<?php if (isset($_POST['email'])) echo htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8') ?>" />

			 </div>

			 <div class="col-sm-4">
              <label>Contact number<span class="red">*</span></label>
              <input type="text" name="phone" value="<?php if (isset($_POST['phone'])) echo htmlspecialchars($_POST['phone'], ENT_QUOTES, 'UTF-8') ?>" />

              <label>House / Flat No &amp; Street<span class="red">*</span></label>
              <input type="text" name="addr" value="<?php if (isset($_POST['addr'])) echo htmlspecialchars($_POST['addr'], ENT_QUOTES, 'UTF-8') ?>" />
              </div>
			  <div class="col-sm-4 last-child">
              <label>Postcode<span class="red">*</span></label>
              <input id="book-postcode" type="text" name="postcode" value="<?php if (isset($_POST['postcode'])) echo htmlspecialchars($_POST['postcode'], ENT_QUOTES, 'UTF-8') ?>" />

              <label>Message</label>
              <textarea name="comments" style="height:30px !important;"><?php if (isset($_POST['comments'])) echo htmlspecialchars($_POST['comments'], ENT_QUOTES, 'UTF-8') ?></textarea>
            </div>

			<div class="col-sm-6 your-dates">
              <h3>Your Dates</h3>
			  <p>(In order of preference)<br>To set or remove a date, just click on a calender day</p>
              <input type="hidden" name="availibility" value="<?php if (isset($_POST['availibility'])) echo htmlspecialchars($_POST['availibility'], ENT_QUOTES, 'UTF-8') ?>" />
			  <table class="choice-table">
			  <tr><td class="new_width" style="border-bottom: 1px solid #dedede !important;"><font color="#ed7401" size="4">1st Choice</font></td><td id="tabelwidth" rowspan="3" style="position:relative"><ol id="pref_dates">
			  <li id="pref_date_1" style="height:36px;"></li>
			  <li id="pref_date_2" style="height:37px;"></li>
			  <li id="pref_date_3" style="height:36px;"></li>
			  </ol>
			  </td></tr>
			  <tr style="border-bottom: 1px solid #dedede !important;"><td><font color="#7c7c7c" size="4">2nd Choice</font></td></tr>
			  <tr style="border-bottom: 1px solid #dedede !important;"><td ><font color="#324875" size="4">3rd Choice</font></td></tr>
			  <!--tr><td colspan="3"><font color="#4b759a" size="4" class="request-call">Request a call back</font></td></tr-->
			  </table>



			<a href="#" class="book-now" id="book-now-request-form">Book now </a>
             <span id="loadingif"><img src="https://www.buildteam.com/images/newhome/loading_bt.gif" alt=""></span>
            </div>

			<div class="col-sm-6 clalendargetup">
				<div id="avail_cal"></div>
                <div class="promptimage">
                	<img src="http://www.buildteam.com/images/new_sv/roundtext.png" alt="">
                </div>
			</div>






          </form>






        </div>
	</div>

 <div class="clear"></div>
  <div class="outsidepoup">
    			<div class="outsideinner">
              <div class="lightbox_content">
             <h4>Thank you for considering Build Team</h4>
             <p>You are outside of our standard coverage area, please contact us on 0207 495 6561 or e-mail hello@buildteam.com to arrange your site visit.</p>
             <a href="javascript:void(0);">Ok</a>
            </div>
            </div>
            </div>

	<div class="bookthanks">
		<div class="bookthanksinner">
			  <div class="lightbox_content">
			  <div class="closethis">
					  <a href="/getting-started/freepremium.html">x</a>
				  </div>
			 <h4>Thank you for your booking</h4>
			 <p>You should receive a confirmation email within 24 hours. While you wait why not check out our gallery.</p>
			 <a href="https://www.buildteam.com/project-gallery/gallery.html">Ok</a>
			</div>
		</div>
	</div>

	<div class="bookthanksinvalid">
		<div class="bookthanksinner">
			  <div class="lightbox_content">
			  <div class="closethis">
					  <a href="javascript:void(0)" class="close-x-btn">x</a>
				  </div>
			 <h4>Code Invalid</h4>
			 <p>Sorry but the code you entered is invalid - please try again. If the problem persists feel free to contact a member of the to Our who will be more than happy to assist.</p>
			 <a href="javascript:void(0)" class="close-x-btn">Ok</a>
			</div>
		</div>
	</div>




<script>
$(document).ready(function () {

	var twoChars, threeChars, fourChars, london2;

/* All Pincodes */
    var all = ["WD6", "EN5", "EN4", "EN2", "EN1", "EN3", "E4", "N9", "N21", "N14", "N13", "N18", "E17", "E18", "E10", "E11", "E15", "E7", "E12", "E13", "E6",
        "E16", "SE28", "SE18", "SE2", "SE9", "DA16", "DA15", "BR7", "BR1", "BR2", "BR4", "BR3", "CR0", "SE25", "CR7", "CR4", "SM6", "SM5", "SM1",
        "SM4", "SM3", "KT4", "KT4", "KT5", "KT1", "KT2", "TW11", "TW12", "TW2", "TW10", "TW1", "TW9", "TW8", "TW7", "TW3", "TW4", "TW5", "UB2",
        "UB1", "UB5", "UB6", "HA0", "HA9", "HA2", "HA1", "HA3", "NW9", "NW4", "N3", "NW7", "N12", "N11", "N20", "HA8", "HA7"];

    var inM25 = ['SW20', 'SW19', 'SW17', 'SW16', 'SE19', 'SE20', 'SE26', 'SE6', 'SE12', 'SE3', 'SE7', 'SE13', 'SE23', 'SE27', 'SE21', 'SE22',
        'SE4', 'SE14','SE8', 'SE10', 'E14', 'E3', 'E9', 'SE16', 'SE15', 'SE24', 'SW2', 'SW12', 'SW4', 'SW9', 'SE5', 'SW18', 'SW15', 'SW14', 'SW13',
        'SW6', 'SW11', 'SW8','SW1', 'SE11', 'SE17', 'SE1', 'EC3', 'EC2', 'E1', 'E2', 'E8', 'E5', 'N16', 'N15', 'N17', 'N22', 'N8', 'N4', 'N5', 'N1', 'EC1',
        'EC4', 'WC2', 'WC1', 'NW1', 'W1', 'W2','SW7', 'SW3', 'W14', 'W6', 'W4', 'W3', 'W5', 'W13', 'W7', 'NW10', 'NW2', 'NW11', 'N2', 'N10', 'N6',
        'N19', 'N7', 'NW5', 'NW3', 'NW6', 'NW8', 'W9', 'W10','W12', 'W11', 'W8', 'SW10', 'SW5'];

    var outLondon1 = ["AB", "AL", "BA", "BB", "BD", "BH", "BL", "BN", "BS", "CA", "CF", "CH", "CM", "CO", "CT", "CV", "CW", "DE", "DG",
        "DH", "DL","DN", "DT", "DY", "EH", "EX", "FK", "FY", "GL", "GU", "HD", "HG", "HP", "HR", "HU", "HX", "IV", "KA", "KW", "KY",
        "LA", "LD", "LE", "LL","LN", "LS", "LU", "ME", "MK", "ML", "NE", "NG", "NN", "NP", "NR", "OL", "OX", "PA", "PE", "PH", "PL", "PO",
        "PR", "RG", "RH", "SA", "SG","SK", "SL", "SN", "SO", "SP", "SR", "SS", "ST", "SY", "TA", "TD", "TF", "TN", "TQ", "TR", "TS",
        "WA", "WF", "WN", "WR", "WS", "WV", "YO"];

    var outLondon2 = ["G1","G2","G3","G4","G5","G9","G11","G12","G13","G14","G15","G20","G21","G22","G23",
        "G31","G32","G33","G34","G40","G41","G42","G43","G44","G45","G46","G51","G52","G53","G58","G60","G61","G62","G63","G64",
        "G65","G66","G67","G68","G69","G70","G71","G72","G73","G74","G75","G76","G77","G78","G79","G81","G82","G83","G84","S1","S2",
        "S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14","S17","S18","Stw2 6hg19","S20","S21","S25","S26","S30","S31","S32","S33",
        "S35","S36","S40","S41","S42","S43","S44","S45","S49","S60","S61","S62","S63","S64","S65","S66","S70","S71","S72","S73","S74",
        "S75","S80","S81","M1","M2","M3","M3","M4","M5","M6","M7","M8","M9","M11","M12","M13","M14","M15","M16","M17","M18","M19","M20",
        "M21","M22","M23","M24","M25","M26","M27","M28","M29","M30","M31","M32","M33","M34","M35","M38","M40","M41","M43","M44","M45",
        "M46","M50","L1","L2","L3","L4","L5","L6","L7","L8","L9","L10","L11","L12","L13","L14","L15","L16","L17","L18","L19","L20","L20","L21","L22",
        "L23","L24","L25","L26","L27","L28","L29","L30","L31","L32","L33","L34","L35","L36","L37","L38","L39","L40","B1","B2","B3","B4","B5",
        "B6","B7","B8","B9","B10","B11","B12","B13","B14","B15","B16","B17","B18","B19","B20","B21","B23","B24","B25","B26","B27","B28","B29",
        "B30","B31","B32","B33","B34","B35","B36","B37","B38","B40","B42","B43","B44","B45","B46","B47","B48","B49","B50","B60","B61",
        "B62","B63","B64","B65","B66","B67","B68","B69","B70","B71","B72","B73","B74","B75","B76","B77","B78","B79","B80","B90","B91",
        "B92","B93","B94","B95","B96","B97","B98"];

    /* Disabling the submit button if length is less than 5 */


   $('.post-continue').click(function(e) {


        var fieldValue =  $('#postcode').val();

		if (fieldValue.length < 1) {
          $('.kaction').css("opacity","0.5");
		  $('.premium').css("opacity","0.5");
		  $('.options-content').html('');
       }

		$('.postcode').html(fieldValue);

		clickMe();

    });



	  function clickMe(){

	var pincode = $('#postcode').val().toUpperCase();

        switch (pincode.length) {
            case 6:
                twoChars = pincode.substr(0, 2);
                if ($.inArray(twoChars, all) >= 0) {
                    checkForAll();
                } else if ($.inArray(twoChars, inM25) >= 0) {
                    sameText();
                } else if($.inArray(twoChars, outLondon2) >= 0){
                    outsideLondon();
                } else if (($.inArray(twoChars, inM25) == -1) && ($.inArray(twoChars, all) == -1) && ($.inArray(twoChars, outLondon2) == -1)) {
                    outsideEngland();
                }
                break;
            case 7:
                threeChars = pincode.substr(0, 3);
                london2 = pincode.substr(0,2);
                if ($.inArray(threeChars, all) >= 0) {
                    checkForAll();
                } else if ($.inArray(threeChars, inM25) >= 0) {
                    sameText();
                } else if ($.inArray(london2, outLondon1) >= 0) {
                    outsideLondon();
                }  else if ($.inArray(threeChars, outLondon2) >= 0) {
                   outsideLondon();
                } else if (($.inArray(threeChars, inM25) == -1) && ($.inArray(threeChars, all) == -1) && ($.inArray(threeChars, outLondon1) == -1) && ($.inArray(threeChars, outLondon2) == -1)) {
                    outsideEngland();
                }
                break;
            case 8:
                fourChars = pincode.substr(0, 4);
                london2 = pincode.substr(0,2);
                if ($.inArray(fourChars, all) >= 0) {
                    checkForAll();
                } else if ($.inArray(fourChars, inM25) >= 0) {
                    sameText();
                } else if ($.inArray(london2, outLondon1) >= 0) {
                    outsideLondon();
                }  else if ($.inArray(fourChars, outLondon2) >= 0) {
                   outsideLondon();
                } else if (($.inArray(fourChars, inM25) == -1) && ($.inArray(fourChars, all) == -1) && ($.inArray(fourChars, outLondon1) == -1) && ($.inArray(fourChars, outLondon2) == -1)) {
                    outsideEngland();
                }
                break;
        }

	  }


		//$('.kaction form a').hide();
		//$('.premium form a').hide();


	function outsideLondon() {

        //$('.options-content').html('As you are outside of our coverage area, we wouldn\'t be able to assist at this stage. '+' <a href="http://www.buildteam.com/contact.html">Contact Us</a> if anything is unclear or if you have any questions. ').css({'visibility':'visible', 'max-width':'90%'});
		//$('.kaction').css("opacity","0.5");
		///$('.premium').css("opacity","0.5");
		//$('.kaction form a').hide();
		//$('.premium form a').hide();
        $('.flag').unbind('click');

    }

    function outsideEngland() {


        //$('.options-content').html('As you are outside of our coverage area, we wouldn\'t be able to assist at this stage. '+' <a href="http://www.buildteam.com/contact.html">Contact Us</a> if anything is unclear or if you have any questions. ').css({'visibility':'visible', 'max-width':'90%'});

        //$('.kaction').css("opacity","0.5");
		//$('.premium').css("opacity","0.5");

		$('.kaction form a').hide();
		$('.premium form a').hide();

        $('.flag').unbind('click');
    }

    function sameText() {

        //$('.options-content').text('We can offer you all of our services which are detailed below.').css({'visibility':'visible', 'max-width':'90%'});
		$('.kaction').css("opacity","1");
		$('.premium').css("opacity","1");
		$('.kaction form a').show();
		$('.premium form a').show();
    }

    function checkForAll() {


        //$('.options-content').text('We can offer you all of our services, however we would only be able to offer you a Premium Site Visit, '+'details of which can be found below.').css({'visibility':'visible', 'max-width':'70%'});
		//$('.kaction').css("opacity","0.5");
        $('.premium').css("opacity","1");

		//$('.kaction form a').hide();
		$('.premium form a').show();
    }






});
</script>

</div>

<script src="/js/jquery-ui-book.min.js"></script>
<script src="/js/jquery-ui.multidatespicker.js?v=1"></script>
<script src="/js/book-visit.js?v=1"></script>
<script type="text/javascript">
    jQuery(document).ready(DesignTeam.initBookCal);



</script>
<script>
$(document).ready(function(e) {
    $(".choice-table").hover(function(e) {
        $(".promptimage").addClass("comefromleft");
    });
	$(".book-now-btn").on("click", function() {
		$("#coupon-form").submit();
	});
	$('.bookthanksinvalid .close-x-btn').on("click", function() {
		 $('.bookthanksinvalid').show();
	});
	$("#coupon-form").on("submit",function(e) {
		e.preventDefault();
		console.log("---------------");
		var arr = [];
		$(this).find("input[type='text']").each(function (index, node) {
			arr.push(node.value);
		});
		var couponCode = arr.toString().replace(/,/g, '');
		$.ajax({
			url: "/book-site-visit-ajax.php",
			type: 'POST',
			data: { coupon: couponCode },
			success: function(result){
				$("#coupon_code").val("");
				if(result == "applied") {
					$("#coupon_code").val(couponCode);
					$(".coupon-header p.c-msg").removeClass('danger-msg');
					$(".coupon-header p.c-msg").addClass('success-msg').html('Code applied').fadeIn("slow");
					/*$("#coupon-price-t").fadeOut("slow", function() {
						$("#coupon-price-t").html("£0.00").fadeIn("slow");
					});*/
					if($('#coupon-price-t').html().split("£")[1] == 150) {
						$price = 150;
							setInterval(function(){
								$price -= 1;
								if($price > 1) {
									$("#coupon-price-t").html("£"+$price);
								} else if($price == 0) {
									$("#coupon-price-t").html("£"+$price+".00");
								}

							},
							10);
					}
				} else if(result == "invalid") {
					$("#coupon_code").val("invalid");
					$(".coupon-header p.c-msg").removeClass('success-msg');
					$(".coupon-header p.c-msg").addClass('danger-msg').html('Code invalid').fadeIn("slow");
					if($("#coupon-price-t").html() == "£0.00") {
						$("#coupon-price-t").fadeOut("slow", function() {
							$("#coupon-price-t").html("£150.00").fadeIn("slow");
						});
					}
				} else if(result == "nla") {
					$("#coupon_code").val("invalid");
					$(".coupon-header p.c-msg").removeClass('success-msg');
					$(".coupon-header p.c-msg").addClass('danger-msg').html('Code invalid').fadeIn("slow");
					if($("#coupon-price-t").html() == "£0.00") {
						$("#coupon-price-t").fadeOut("slow", function() {
							$("#coupon-price-t").html("£150.00").fadeIn("slow");
						});
					}
				} else {
					$(".coupon-header p.c-msg").hide();
					if($("#coupon-price-t").html() == "£0.00") {
						$("#coupon-price-t").fadeOut("slow", function() {
							$("#coupon-price-t").html("£150.00").fadeIn("slow");
						});
					}
				}
			}
		});
		console.log();
	});
	$(".coupon-header input[type='text']").keyup(function () {

		/*var key = parseInt($(this).attr("data-key"));
		if(event.keyCode == 8) {
			if(key > 1) {
				key -= 1;
			}
		} else {
			if(key < 11) {
				key += 1;
			}
		}
		$(".coupon-header input[name='cp-code"+key+"']").focus();
		if (this.value.length == 1) {
		  $(this).next(".coupon-header input[type='text']").focus();
		}*/

	});
});
</script>

<script>

$(".selected").click(function(){

	});
</script>


<?php
 require_once('./inc/footer.inc.php');
?>
