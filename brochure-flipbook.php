<?php  require_once('./inc/header.inc.php'); ?>

<link rel="stylesheet" type="text/css" href="https://www.buildteam.com/Flipbook/flipbook.style.css">
<link rel="stylesheet" type="text/css" href="https://www.buildteam.com/Flipbook/font-awesome.css">
<script src="https://www.buildteam.com/Flipbook/flipbook.min.js"></script>
<script>
jQuery(document).ready(function () {
      var options = {
        pdfUrl:"https://www.buildteam.com/Flipbook/Build_Team_Brochure.pdf",
          pages:[

            	{
                    title:"Cover",
                },

            	{
                },

            	{
                    title:"Page 3",
                },

            	{},

            	{},

            	{
              htmlContent:'<iframe style="position:absolute; bottom:30px; left:30px;" width="690" height="465" src="https://www.youtube.com/embed/a4u_kKYXyRc" frameborder="0" allowfullscreen></iframe></div>'
              },

            	{},

            	{
                    title:"End"
                },
            ]
      };
      jQuery("#container").flipBook(options);
    })    
</script>  

<div id="container">
    <p>Real 3D Flipbook has lightbox feature - book can be displayed in the same page with lightbox effect.</p>
    <p>Click on a book cover to start reading.</p>
    <img src="images/1.jpg" />
</div>


<!--<script src="Flipbook/embed.js"></script>-->
<?php require_once('./inc/footer.inc.php'); ?>