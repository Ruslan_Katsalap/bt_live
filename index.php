<?php
require_once('./inc/header.inc.php');
?>
<link href="https://fonts.googleapis.com/css?family=Libre+Baskerville:400i" rel="stylesheet">
<style>
  @font-face {
    font-family: 'libre_baskervillebold';
    src: url('/fonts/librebaskerville-regular-webfont.woff2') format('woff2'),
      url('/fonts/librebaskerville-regular-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

  }

  @font-face {
    font-family: 'libre_baskervilleitalic';
    src: url('/fonts/librebaskerville-italic-webfont.woff2') format('woff2'),
      url('/fonts/librebaskerville-italic-webfont.woff') format('woff');
    font-weight: normal;
    font-style: normal;

  }

  .hometextabimation h1 span {}

  .hometextabimation {
    background: url(images/home_redesign/textanimationback.jpg);
    background-size: cover;
    padding: 100px 0px;
    text-align: center;
    margin-top: 0px;
  }

  .hometextabimation h1 {
    font-size: 50px !important;
    font-weight: bold;
    color: #FFF;

  }

  .hometextabimation h1 span {

    font-weight: normal !important;
  }

  .hometextabimation h2 {
    font-size: 50px !important;
    font-weight: bold;
    color: #FFF;
  }

  .hometextabimation .mainpadding {
    height: 152px;
  }

  .ityped-cursor {
    display: none;
  }

 

  

  .mainpressouter:before {
    content: "";
    clear: both;
    display: block;
  }

  @media only screen and  (min-width:641px) {
	  .aad_slider_leaf_1
	  {
		width: 40px;
		margin: -40px 0 0 -40px;
		position: absolute;
	  }
	  .aad_slider_leaf_2
	  {
		  width: 40px;
    position: absolute;
	  }
    section.lazy.slider.slick-initialized.slick-slider {
      display: none;
    }
    .logos-mob {
      display:none;
    }
  }
  @media only screen and  (max-width:640px) {
    .home-gallery-p .vt {
left: 0px !important;
    }
    .home-gallery-p {

    height: auto !important;
}
  .logos-mob {
    position: relative;
width: 100%;
z-index: 1;
max-width: 640px;
height: 60px !important;
margin:2% 0%;
vertical-align: middle;
  overflow-x: scroll; 
  overflow-y: hidden;
    display: flex;
}
  
  .logos-mob img {
    width:100%;
    float:left;
    max-width:100px !important;
    margin-right:30px !important;
    display: inline-block;
  vertical-align: middle;
  max-height: 40px;
  }
}

  .buildwithbuildsec {
    background: #fff;
    padding: 50px 0px;
  }

  .innerbuildwith {
    max-width: 980px;
    margin: 0 auto;
  }

  .leftbuildwith {
    float: left;
    width: 50%;
  }

  .rightbuildwith {
    float: right;
    width: 50%;
  }

  .buildsecdetails img {
    width: 100%;
  }

  .buildsecdetails {
    text-align: center;
  }

  .buildsecdetails p {
    font-size: 22px;
    color: #afafaf;
    line-height: 25px;
    padding-top: 0px;
  }

  .buildsecdetails h2 {
    color: #124b7b;
    font-size: 24px;
    font-weight: bold;
    padding-bottom: 10px;
  }

  .rightbuildwith .buildsecdetails {
    padding: 65px 0px;
  }

  .leftbuildwith .buildsecdetails {}

  #video {
    display: none;
    height: 275px !important;
  }

  .buildsecdetails a {
    display: inline-block;
    margin-top: 20px;
    font-size: 20px;
    /* font-weight: normal; */
    width: 200px;
    border-radius: 10px;
    color: #f5641e;
    font-style: italic;
  }


  .aad_fixed_right_phone_icon {
    display: none;
  }

  #aad_fixed_right_phone_icon_number {
    color: #fff !important;
  }
  
  
 
 
    .aad_fixed_right_house_tour {
    /*  display: none !important;  */
      /*height: 50px !important;*/
      width: 100%;
      position: absolute;
      /*bottom: 0px;*/
      top:30px;
	  opacity:1;
      z-index: 1 !important;
      transition: 0.5s ease-in-out;
      text-align: left;
      font-size: 1.7em !important;
      right: 0px;
    }

    .aad_fixed_right_house_tour  h1 {
	    margin: 0 0px 20px -36px;
        float: left;
        padding: 1px 38px;
        color:#fff;
        font-size: 85px !important;
        text-transform:uppercase !important;
        background: #1a3f6e;
        width: 100%;
        position: absolute;
        top: 562px;
        text-align: center;
    }
    .aad_fixed_right_house_tour  p {    
	    margin: 0;
        float: left;
        padding: 5px 20px;
        color:#fff !important;
        text-align:center;
        background: #1a3f6e;
        width: 100%;
        top: 532px;
        position: absolute;
        font-size: 20px;
        font-family: Calibri;
        font-weight: bold;
    }
    .aad_fixed_right_house_tour  p.covid {    
	    margin: 0;
        float: left;
        padding: 12px 20px;
        color:#fff !important;
        text-align:center;
        background: #2d6d16;
        width: 100%;
        height: 45px;
        position: absolute;
        top:90px;
        font-size: 40px;
        font-weight: 500;
        
    }
    .aad_fixed_right_house_tour_a {
        margin: 0px 0px 0 0;
        background: #fff;
        width: 100%;
        height: 36px !important;
        padding-top: 13px;
        position: absolute;
        top: 692px;
        font-family: Calibri;
        font-weight: bold;
        color: #153E6F;
        font-size: 20px;
        text-align: center;
    }
    .aad_fixed_right_house_tour  a.gallery {
    margin: 0px 0px 0 0;
    text-transform: capitalize !important;
        text-decoration: underline;
    background: #fff;
    width: 42%;
    padding: 14px 0px;
    font-weight: bold;
    float: left;
    text-align: right;
    }
    .aad_fixed_right_house_tour  a.tour {
    margin: 0px 0px 0 0;
    text-transform: capitalize !important;
        text-decoration: underline;
    background: #fff;
    width: 45%;
    padding: 14px 0px;
    font-weight: bold;
    float: right;
    text-align: left;
    }
    
    @media screen and (min-width:769px) and (max-width:1600px) {
        .aad_fixed_right_house_tour {
      width: 100%;
      position: absolute;
      top:30px;
	  
      z-index: 1 !important;
      transition: 0.5s ease-in-out;
      text-align: left;
      font-size: 1.7em !important;
      right: 0px;
    }

    .aad_fixed_right_house_tour  h1 {
	    margin: 0 0px 20px -36px;
        float: left;
        padding: 12px 38px;
        color:#fff;
        font-size: 85px !important;
        text-transform:uppercase !important;
        background: #1a3f6e;
        width: 100%;
        position: absolute;
        top: 465px;
        text-align: center;
    }
    .aad_fixed_right_house_tour  p {    
	    margin: 0;
        float: left;
        padding: 5px 20px;
        color:#fff !important;
        text-align:center;
        background: #1a3f6e;
        width: 100%;
        top: 430px;
        position: absolute;
        font-size: 25px;
    }
    .aad_fixed_right_house_tour  p.covid {    
	    margin: 0;
        float: left;
        padding: 12px 20px;
        color:#fff !important;
        text-align:center;
        background: #2d6d16;
        width: 100%;
        height: 45px;
        position: absolute;
        top:90px;
        font-size: 40px;
        font-weight: 500;
        
    }
    .aad_fixed_right_house_tour_a {
        margin: 0px 0px 0 0;
        background: #fff;
        width: 100%;
        height: 50px !important;
        padding: 0px;
        position: absolute;
        top: 590px;
    }
    .aad_fixed_right_house_tour  a.gallery {
    margin: 0px 0px 0 0;
    text-transform: capitalize !important;
        text-decoration: underline;
    background: #fff;
    width: 42%;
    padding: 14px 0px;
    font-weight: bold;
    float: left;
    text-align: right;
    }
    .aad_fixed_right_house_tour  a.tour {
    margin: 0px 0px 0 0;
    text-transform: capitalize !important;
        text-decoration: underline;
    background: #fff;
    width: 45%;
    padding: 14px 0px;
    font-weight: bold;
    float: right;
    text-align: left;
    }
        
    }
    
    
 
 
.aad_stroke_text {
  color: #0F4778;
  text-shadow: -1px -1px 0 #fff, 1px -1px 0 #fff, -1px 1px 0 #fff, 1px 1px 0 #fff;
}

    .aad_fixed_right_house_block {
        border-top: 1px solid rgb(255, 255, 255);
        margin-top: 25px;
        padding-top: 10px;
    }
    .aad_fixed_right_house_user {
        width: 50%;
        float: left;
    }
    .aad_fixed_right_house_stars {
        width: 50%;
        float: right;
    }
    .aad_fixed_right_house_tour_3
	{
	    height: 952px;
		position: absolute;
		right: 0;
		width: 410px;
		padding: 250px 39px 0 39px;
		opacity: 0.78;
		background: #0F4778;		
	}
    .aad_fixed_right_house_tour_3 h2
	{
		/*text-decoration:underline;*/
		font-size:22px;
		color:#fff;
		font-weight:bold;
	}
    .aad_fixed_right_house_tour_3 p
	{
		font-size: 25px;
		color: #fff;    
		line-height: 30px;
		margin: 50px 0 0 0;
	}
    .aad_fixed_right_house_tour_3 a
	{
		font-size: 25px;
		background: #fff;
		color: #0F4778;
		margin: 221px 0 0 -39px;
		padding: 11px 29px;
		float: left;
		font-weight: bold;
        position: absolute;
        left: 39px;
	}
    .aad_fixed_right_house_tour_2 {
      display: block !important;
      height: 50px !important;
      width: 300px;
      position: absolute;
      top: 130px;
	  opacity:0.78;
      z-index: 1 !important;
      transition: 0.5s ease-in-out;
      text-align: left;
      font-size: 1.7em !important;
      right: 0px;
    }

    .aad_fixed_right_house_tour_2  h1 {
	    margin: 0 0px 20px 0;
    float: left;
    padding: 12px 20px;
	color:#fff;
	font-size: 45px !important;
	text-transform:uppercase !important;
    background: #0F4778;
    width: 100%;
    }
    .aad_fixed_right_house_tour_2  h2 {
	    margin: 0 0px 20px 0;
    float: left;
    padding: 12px 20px;
	color:#fff;
	font-size: 25px !important;
	text-transform:uppercase !important;
    background: #0F4778;
    width: 100%;
    }
    .aad_fixed_right_house_tour_2  a {
    margin: 0px 0px 0 0;
    text-transform: uppercase !important;
    background: #fff;
    width: 160px;
	font-size:20px;
    padding: 10px 0px;
    font-weight: bold;
    float: right;
    text-align: center;
    }
	
	@media only screen and (min-width:1439px) and (max-width:1441px)
	{
		.home-banner-h3 {
      margin-top: 110px;
    }
  }
  .logos {
    position: absolute;
width: 100%;
z-index: 1;
max-width: 1140px;
height: 50px !important;
top: 80%;
margin:2% 25%;
  }
  .logos img {
    width:100%;
    float:left;
    max-width:150px !important;
    margin-right:30px !important;
  }
  .home-gallery-p {
    width: 60%;
    float: left;
    height: 160px;
}
.home-gallery-p p {
  margin-top: 62px;
}
.home-gallery-p .vt {
margin-top: 38px;
left: 43px;
position: relative;
}
</style>
<script>
  window.addEventListener("load", function(e) {
  var container = document.querySelector(".home-banner-btns");
  var middle = container.children[Math.floor((container.children.length - 1) / 2)];
  container.scrollLeft = middle.offsetLeft +
    middle.offsetWidth / 2 - container.offsetWidth / 2;
});
</script>
</div>
<div class="slider">

  <ul class="bxslider">

    
<li>
  <div class="aad_fixed_right_house_tour">
         <h2 class="home-banner-h2">The Extension Specialists <br>Trusted By 1,000+ London <br>Home Owners</h2>         
      <h4 class="home-banner-p"></h4>
    <h3 class="home-banner-h3">In-house Design & Build Management</h3>
      <h5 class="home-banner-h5">Select your home improvement below</h5>
      <div class="home-banner-btns">
        <div class="home-banner-button"><a href="https://www.buildteam.com/extension-services.php">Ground Floor Extension
        <div class="home-banner-button-arrow"></div></a>
        </div>
        <div class="home-banner-button"><a href="https://www.buildteam.com/extension-services.php">Ground + Loft Conversion
        <div class="home-banner-button-arrow"></div></a>
      </div>
        <div class="home-banner-button"><a href="https://www.buildteam.com/extension-services.php">Loft Conversion
        <div class="home-banner-button-arrow"></div></a>
      </div>
      </div>
  </div>
  
  <div class="newdesktop parallaxnew aad_desktop_banner_bg" data-parallax="scroll"
    style="background-image:url('../images_new_design/23.21.20_bt_homepage_desktop.jpg'); background-size:cover; width:100%;"></div>
</li>


</ul>

<div class="newlesttabs book-viisit-tab openedtab">
  <div class="newtabvisi">
    <div class="sidetabtitle"><span id="opened"> </span>Meet with us</div>
    <div class="openicon"><img src="images_new_design/desktop_white_arrow.png" alt=""></div>
  </div>
  <div class="tabmaxwidth">
    <div class="dateholder">
      <div class="fromdate dateinline">
        <p class="date date1_aad">
          <? //=date('d',strtotime($dateFrom))?>
          <?php
		  
      $CURRENTTIME = new DateTime($data['current_time']);
      $OFFICETIME  = new DateTime('04:00:00');
      $aad_date = date("m-d-Y");
      $aad_day=date("D"); 
      $aad_cur_day=date("d");
      $date1 = str_replace('-', '/', $aad_date);
      $aad_cur_day_1=date('d',strtotime($date1 . "+0 days"));
      if($aad_day=="Sat")
      {
        
        if ($CURRENTTIME  > $OFFICETIME) 
        {
          $aad_show_day = date('d',strtotime($date1 . "+3 days"));
        }
        else
        {
          $aad_show_day = date('d',strtotime($date1 . "+2 days"));
        }
      }else
      if($aad_day=="Sun")
      {
        if ($CURRENTTIME  > $OFFICETIME) 
        {
          $aad_show_day = date('d',strtotime($date1 . "+2 days"));
        }
        else
        {
          $aad_show_day = date('d',strtotime($date1 . "+1 days"));
        }
      }else
      {
        if ($CURRENTTIME  > $OFFICETIME) 
        {
          $aad_show_day=$aad_cur_day_1;
        }
        else
        {
          $aad_show_day=$aad_cur_day;
        }
      }
        echo $aad_show_day;
    ?>
      </p>
      <p class="month month1">

        <?=date('M',strtotime($date1))?>
      </p>
        <input type='button' name="dateFrom" class="datepicker datepicker1" />

      </div>
      <div class="ordiv dateinline"> <span>OR</span> </div>
      <div class="todate dateinline">
        <p class="date date2_aad">
          <?//=date('d', strtotime($dateTo))?>
          <?php 

if($aad_day=="Fri")
{					
  if ($CURRENTTIME  > $OFFICETIME) 
  {
    $aad_show_day = date('d',strtotime($date1 . "+4 days"));
  }  else 
  {
    $aad_show_day = date('d',strtotime($date1 . "+3 days"));
  }
}else
if($aad_day=="Sat")
{
  if ($CURRENTTIME  > $OFFICETIME) 
  {
    $aad_show_day = date('d',strtotime($date1 . "+4 days"));
  }  else 
  {
    $aad_show_day = date('d',strtotime($date1 . "+3 days"));
  }
}else
if($aad_day=="Sun")
{
  if ($CURRENTTIME  > $OFFICETIME) 
  {
    $aad_show_day = date('d',strtotime($date1 . "+3 days"));
  }  else 
  {
    $aad_show_day = date('d',strtotime($date1 . "+2 days"));
  }
}else
{
  if ($CURRENTTIME  > $OFFICETIME) 
  {
    $aad_show_day = date('d',strtotime($date1 . "+1 days"));
  }  else 
  {
    $aad_show_day = date('d',strtotime($date1 . "+0 days"));
  }
}
  echo $aad_show_day;
?>
</p>
<p class="month month2">

  <?=date('M',strtotime($date1))?>
</p>
        <input type='button' name="dateTo" class="datepicker datepicker2" />
      </div>
    </div>
    <!--<div class="mobilewrap"> <a href="tel:020 7495 6561">02074956561</a> </div>-->
    <div class="linktab"> <a href="https://www.buildteam.com/book-design-consultation.html" onclick="savedates();">Book Now</a></span>
    </div>
  </div>
</div>
<div class="newlesttabs brochure-tab">
  <div class="newtabvisi">
    <div class="sidehidetab">
      <div class="sidetabtitle">Brochure</div>
      <div class="sideexpandtab"> <a href="https://www.buildteam.com/download-our-brochure.html">Download a Brochure</a>
      </div>
    </div>
    <a href="https://www.buildteam.com/download-our-brochure.html">
      <div class="openicon"><img src="images_new_design/desktop_blue_arrow.png" alt=""></div>
      <div class="closeicon"><img src="images_new_design/desktop_white_arrow.png" alt=""></div>
    </a>
  </div>
</div>
<div class="newlesttabs online-quote-tab">
  <div class="newtabvisi">
    <div class="sidehidetab">
      <div class="sidetabtitle">Online Quote </div>
      <div class="sideexpandtab"> <a href="https://www.buildteam.com/online-quote-calculator.html">Instant Online
          Quote</a> </div>
    </div>
    <a href="https://www.buildteam.com/online-quote-calculator.html">
    <div class="openicon"><img src="images_new_design/desktop_blue_arrow.png" alt=""></div>
      <div class="closeicon"><img src="images_new_design/desktop_white_arrow.png" alt=""></div>
    </a>
  </div>
</div>
<div class="newlesttabs design-database-tab">
  <div class="newtabvisi">
    <form action="design-planning-database.php">
      <div class="sidehidetab">
        <div class="sidetabtitle">Database</div>
        <div class="sideexpandtab"> <span class="inputitle">Find a Scheme Near You</span>
          <input id="field" placeholder="Enter your postcode" name="road_name" type="text" />
        </div>
      </div>
      <div class="openicon"><img src="images_new_design/desktop_blue_arrow.png" alt=""></div>
      <div class="closeicon">
        <input type="image" src="images_new_design/desktop_white_arrow.png">
        <!--<img src="img/hometabs/search_icon.png" alt="">-->
      </div>
    </form>
  </div>
</div>
<div class="onlinecalc" style="display:none;">
  <div>
    <div id="toggleswitch"><span class="collapsedcalc"></span></div>
    <p>Find a Scheme Near You</p>
    <span>Get an idea of cost feasibility by using our online quote calculator. <a
        href="https://www.buildteam.com/online-quote-calculator.html">Start Now</a></span>
  </div>
</div>
</div>
<div class="scrolldown">
  <div class="scrlbtn">
    <img src="images/howcanwe/images/scrlbtn.png" alt="">
    <span>Scroll Down</span>
  </div>
</div>
</div>
<div id="aad_custom_banner" class="small parallaxnew aad_custom_banner">
  <div class="aad_fixed_right_house_tour">
  <div class="aad_fixed_right_house_tour">
         <h2 class="home-banner-h2">The Extension Specialists<br> Trusted By 1,000+ London<br> Home Owners</h2>         
      <h4 class="home-banner-p"></h4>
      
      
    <h3 class="home-banner-h3">In-house Design & Build Management</h3>
      <h5 class="home-banner-h5">Select your home improvement below</h5>
      <div class="home-banner-btns">
        <div class="home-banner-button"><a href="https://www.buildteam.com/extension-services.php">Ground Floor Extension
          <div class="home-banner-button-arrow"></div></a>
        </div>
        <div class="home-banner-button"><a href="https://www.buildteam.com/extension-services.php">Ground + Loft Conversion
          <div class="home-banner-button-arrow"></div></a>
        </div>
        <div class="home-banner-button"><a href="https://www.buildteam.com/extension-services.php">Loft Conversion
          <div class="home-banner-button-arrow"></div></a>
        </div>
      </div>
      

  </div>
  <div class="newdesktop parallaxnew aad_desktop_banner_bg" data-parallax="scroll"
    style="background-image:url('../images_new_design/bt_homepage_banner-mobile_replacement.jpg'); background-size:cover; width:100%;"></div>


  </div>
  
</div> 
</div>
</div>
<div class="logos-mob">
  <img src="/images_new_design/logos/homapage_features-01-01.png">
  <img src="/images_new_design/logos/homapage_features-02-01.png">
  <img src="/images_new_design/logos/homapage_features-03-01.png">
  <img src="/images_new_design/logos/homapage_features-04-01.png" style="width:40px;height:50px">
  <img src="/images_new_design/logos/homapage_features-05-01.png">
  <img src="/images_new_design/logos/homapage_features-06-01.png">
  </div>


<div class="mainouterwrapper">
  <div class="mainpadding">
    <div class="howcanwehelp">
      <div class="howcaninner">
        <div class="sectiontitle">
          <h1 class="mainnewtitle">How Can We Help?</h1>
        </div>
        <div class="sectiondesc">
          <p>We offer a variety of solutions for you to choose from because every project is unique and we understand that
our clients would like different levels of involvement from our team. Whether you are looking for design, build, design & build
or anything else in-between, we are here to help.</p>
        </div>
        <div class="howcangrid">
          <div class="inlinegrid">
            <div class="innerinlinegrid">
              
              <div class="gridimage"> <img src="images_new_design/icon_map.png" alt=""> </div>
              <div class="gridtitle">
                <h2><a href="https://buildteam.com/architectural-design-phase.html">Design Phase</a></h2>
              </div>
              </div>
              <div class="verticletext">
                <p><span>Design and Planning</span></p>
                <p><span>Building Reg. Drawings</span></p>
                <p><span>Detailed Schedule of Works </span></p>
                <p><span>Interior & Landscape Design (optional bolt-on)</span></p>
                <div class="clearfix"></div>
              </div>
            
          </div>
          <div class="inlinegrid"> 
            <div class="innerinlinegrid">
              
              <div class="gridimage"> <img src="images_new_design/icon_hat.png" alt=""> </div>
              <div class="gridtitle">
                <h2><a href="https://www.buildteam.com/the-build-phase.html">Build Phase</a></h2>
              </div>
              </div>
              <div class="verticletext">
                <p><span>Full Build Service</span></p>
                <p><span>Project Management</span></p>
                <p><span>Payment Stage Certification</span></p>
                <p><span>10 Year Guarantee In-house Design and Project Management</span></p>
                <div class="clearfix"></div>
              </div>
            
          </div>
          <div class="inlinegrid" style="border-bottom:none;">
            <div class="innerinlinegrid" style="border-right:none;">
              
              <div class="gridimage"> <img src="images_new_design/icon_house.png" alt=""> </div>
              <div class="gridtitle">
                <h2><a href="https://www.buildteam.com/online-quote-calculator.html">Additional Services</a></h2>
              </div>
              </div>
              <div class="verticletext">
                <p><span>Party Wall Service</span></p>
                <p><span>Approved Inspector</span></p>
                <p><span>Thames Water</span></p>
                <p><span>Fast Track Service</span></p>
                <div class="clearfix"></div>
              </div>
            
          </div>
          <div id="expandit"><img src="images_new_design/desktop_blue_arrow_down.png"></div>
        </div>
        <div class="howcangrid nobgdrid">
          <div class="inlinegrid">
            <div class="moreinforarrow"> <img src="images/newhome/more_arrow.png" alt=""> </div>
          </div>
          <div class="inlinegrid">
            <div class="moreinforarrow"> <img src="images/newhome/more_arrow.png" alt=""> </div>
          </div>
          <div class="inlinegrid">
            <div class="moreinforarrow"> <img src="images/newhome/more_arrow.png" alt=""> </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>

<div class="home-gallery">
  <div class="home-gallery-text">
<div class="home-gallery-h2">
  <h2>Explore<br>
Our Gallery.</h2>
</div>
<div class="home-gallery-p">
  <p>Browse through our most recently completed extensions, our gallery offers
an insight into our work, as well as case studies of projects.</p>
</div>
</div>
<div class="home-gallery-pics">
<?php

$i = 1;
$ret = '';
$project_code = '';

if ( isset($_GET['p']) ) {
	//$project_code = $_GET['p'];
}


$rs = getRs("SELECT p.thumbnailImg, c.project_category_code, c.project_category_name, i.filename, p.project_code, p.project_name,p.project_type,p.borough,p.floor_area FROM project_category c INNER JOIN (project_image i INNER JOIN project_new p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 ORDER BY p.sort, p.project_id, i.sort, i.project_image_id");

$cols = 3;
$project_count = 3;
$prev_count = 0;

while (($row = mysqli_fetch_assoc($rs)) && ($i < 4) ) {
	
		$project_type = '';
		$borough = '';
		$floor_area = '';
		if($row["project_type"] != null && $row["project_type"] != "") {
			$project_type = str_replace(" ", "_", $row["project_type"]);
		}
		if($row["borough"] != null && $row["borough"] != "") {
			$borough = str_replace(" ", "_", $row["borough"]);
		}
		if($row["floor_area"] != null && $row["floor_area"] != "") {
			
			$floor_area = str_replace(" ", "_", $row["floor_area"]);
			$floor_area = str_replace("&gt;", "a_", $floor_area);
			$floor_area = str_replace("&lt;", "a_", $floor_area);
			$floor_area = str_replace(">", "a_", $floor_area);
			$floor_area = str_replace("<", "a_", $floor_area);
		}
		if ($project_code != $row['project_code']) {
			$project_code = $row['project_code'];
      $project_count++;
		}
		else {
			continue;
      //$ret .= 'display:none;';
		}
	$ret .= '<'.'div class="col_one_third element-item-gallery '. $project_type .' '.$borough .' '.$floor_area.' '. ($i%3 == 0 ?  ' ' : '') .'" data-category="'.$project_category_code.'" style="text-align:center';
		$project_category_code = $row['project_category_code'];
		if($row['project_category_code'] == 'side-return-extensions') {
			$project_category_code = 'side-return-extension';
		}
		
		if($row['thumbnailImg']==''){
			$thumbnail = '/projects/' . str_replace('.jpg', '-450.jpg', $row['filename']);
		}else{
			$thumbnail = '/projects/' . str_replace('.jpg', '-450.jpg', $row['filename']);
		}
		
		$ret .= '"><div><a  href="/'.$row['project_code'].'-'.$project_category_code.'.html?slider=1" title="' . htmlentities($row['project_name']) . '" ><img src="'.$thumbnail . '" style="width:100%;max-width:450px;height:200px" alt="' . htmlentities($row['project_name']) . '" /></a>
    <div class="project-text">
    <a href="/'.$row['project_code'].'-'.$project_category_code.'.html">
			   <h4 class="gallery-box-title">'.htmlentities($row['project_name']).'</h4>
    </a>
    <p>'.htmlentities($row['project_category_name']).'</p>
    <div class="project-button"><a href="/'.$row['project_code'].'-'.$project_category_code.'.html?slider=1">View Project
        <div class="project-button-arrow"></div></a>
        </div>
        </div>   
		</div></div>'; // debug: ' '.$project_count
    
    if (0==($project_count % $cols) && $project_count!=$prev_count) {
      //$ret .= '<br clear="all" />'; // debug: ($project_count % $cols).'!!!
      $prev_count = $project_count;
    }    
	$i += 1;
}

echo $ret;

?>
</div>
<div class="home-gallery-pics mob-button">
<div class="col_one_third" style="border:none"></div>
<div class="col_one_third" style="border:none">
<div class="project-button" style="margin: 25px 20px;"><a href="/project-gallery/gallery.html">Go to Gallery
        <div class="project-button-arrow"></div></a>
        </div>
</div>
<div class="col_one_third" style="border:none"></div>
</div>
</div>

<div class="testimonialscontainer">
  <div class="mainpadding">
    <div class="testimonialswrap">
      <div class="sectiontitle">
        <h2 class="mainnewtitle-border">Client Testimonials</h2>

      </div>
      
    </div>
    
</div>
<section class="lazy slider" data-sizes="50vw">
  
    <?php
    $i = 0;
    $rs = getRs("SELECT testimonial_id, quote, name, title, project_url, video_url, thumbnailImg,category FROM testimonials WHERE is_active = 1 AND is_enabled = 1 ORDER BY testimonial_id DESC");
    $total = mysqli_num_rows($rs);
    $half = false;
    
    while ( ($row = mysqli_fetch_assoc($rs)) && ($i <= 2) ) {
      $i++;
      
    ?>
    <div>
    <div class="threetestimonials testbottom">
    <div class="nameclient"><?php echo $row['name']; ?><br>
      <span style="font-weight:300"><?php echo $row['title']; ?></span></div>
      <p>
        <?php echo $row['quote']; ?>
      </p>
      
    </div>
  </div>
  
    <?php 
      
    } ?>  
   
</section>
</div>


<div style="width:100%;max-width:1920px;background-color:#183e70;">
<div class="home-booking">
  <div class="home-booking-p">Not sure where to start?</div>
  <div class="home-booking-title">
<h3 class="home-booking-h3">Book a Design Consultation with an</h3>
<h3 class="home-booking-h3">Architectural Designer today!</h3>

  </div>
  <div class="home-booking-button">
<div class="book-button">
			<a href="https://www.buildteam.com/book-design-consultation.html">Book now</a>
			<div class="book-button-arrow"></div>
  </div>
  </div>
</div>
</div>




<section class="upcoming-dates-bg">
		<div class="upcoming-dates">
		    <div class="col_full">
			<h2 style="border-bottom: 1px solid #183e70;padding-bottom: 20px;">Open House</h2>
			<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="padding-left:0;padding-right:0">
			<p class="home-house-tour-p">Build Team’s Open House have moved online! You can now join us from the comfort of your own home, take a seat
as we guide you through finished extensions project designed and built by us. Members of the team who have
been involved and the homeowner join us to explain the process and answer your questions.</p>
			<!--<video class="video" controls poster="../housetour/video_poster.png"><source src="../housetour/BuildTeam_House_Tour No_Strap.mp4" type="video/mp4"> </video>
      <div class="playpause"></div>-->
      <iframe width="1140" height="620" src="https://www.youtube.com/embed/efrlQofQgFo" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				
			</div>
			<div class="col-xs-12 col-sm-12 col-md-3 col-lg-3">
			<div class="event-button" style="max-width: 280px;margin: 45px 0 70px 0;">
					<a href="https://buildteam.com/about-us/house-tours.html">Find out more</a>
					<div class="event-button-arrow"></div>
			</div>
			</div>
			<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4">
				
			</div>
		 	</div>
		</div>
	</section>

  <div class="home-gallery">
  <div class="home-gallery-text">
<div class="home-gallery-h2">
  <h2>3D Virtual<br>
  Tours.</h2>
</div>
<div class="home-gallery-p" style="width: 55%;">
  <p class="vt">Virtually walk through some of our previous projects in 3D. These tours offers 
you an insight of what some of our completed projects looks like; helping you to visualise the 
potential of your new space, all from the comfort of your own home.</p>
</div>
</div>
<div class="home-gallery-pics">
<div class="col_one_third element-item-gallery" style="text-align:center"><div>
  <a href="https://build-team.vr-360-tour.com/e/KdQBcm9mnVs/e?hidelive=true" target="_blank">
  <img src="/images_new_design/3d-tour-gellatly-front-image-450.jpg" style="width:100%;max-width:450px;height:200px" alt="SE15" loading="lazy"></a>
    <div class="project-text">
    <a href="https://build-team.vr-360-tour.com/e/KdQBcm9mnVs/e?hidelive=true" target="_blank">
			   <h4 class="gallery-box-title">Gellatly Road</h4>
    </a>
    <p>A Wraparound extension project in SE14 with a dedicated area for a home office, giving this growing family of four all the space they need to live, work and entertain.
</p>
    <div class="project-button">
      <a href="https://build-team.vr-360-tour.com/e/KdQBcm9mnVs/e?hidelive=true" target="_blank">View Tour
        <div class="project-button-arrow"></div></a>
        </div>
        </div>   
		</div></div><div class="col_one_third element-item-gallery" style="text-align:center"><div>
      <a href="https://Build-Team.vr-360-tour.com/e/KdQBdgCZBZ8/e" target="_blank">
      <img src="/images_new_design/3d-tour-dulwich-front-image-450.jpg" style="width:100%;max-width:450px;height:200px" alt="SW4" loading="lazy"></a>
    <div class="project-text">
    <a href="https://Build-Team.vr-360-tour.com/e/KdQBdgCZBZ8/e" target="_blank">
			   <h4 class="gallery-box-title">East Dulwich Grove</h4>
    </a>
    <p>A Ground Floor Extension where we added 28SQM of additional space to create an open plan living, kitchen and dining room with additional study and play space. </p>
    <div class="project-button">
      <a href="https://Build-Team.vr-360-tour.com/e/KdQBdgCZBZ8/e" target="_blank">View Tour
        <div class="project-button-arrow"></div></a>
        </div>
        </div>   
		</div></div><div class="col_one_third element-item-gallery" style="text-align:center"><div>
      <a href="https://build-team.vr-360-tour.com/e/Mx8Bc7jqfBk/e"  target="_blank">
      <img src="/images_new_design/3d-tour-thorpedale-front-image-450.jpg" style="width:100%;max-width:450px;height:200px" alt="N4" loading="lazy"></a>
    <div class="project-text">
    <a href="https://build-team.vr-360-tour.com/e/Mx8Bc7jqfBk/e" target="_blank">
			   <h4 class="gallery-box-title">Thorpedale Road</h4>
    </a>
    <p>Renovation of a family home in Finsbury Park flooded with natural light by the four large Architectural Skylights and the full-width Crittall doors to the garden.</p>
    <div class="project-button"><a href="https://build-team.vr-360-tour.com/e/Mx8Bc7jqfBk/e" target="_blank">View Tour
        <div class="project-button-arrow"></div></a>
        </div>
        </div>   
    </div></div>
    <!--<div class="col_one_third" style="border:none"></div>-->

</div>


</section>




<?php

require_once('./inc/footer.inc.php');