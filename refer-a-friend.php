<?php
require_once('./inc/header.inc.php');
?>
<style>
li span {
	color:#f5641e;
	}
</style>
<div class="full">

    	 
		<h1 style="font-size:30px;"> Terms & Conditions: Refer a Friend </h1>

	 <div style="line-height: 2; letter-spacing:2;">
     	<p class="texttitle"><strong>Site Visit:</strong></p>
		<ol style="padding-left:20px;">
        	<li>This offer cannot be used in conjunction with any other offer.</li>
            <li>The ‘referrer’ can only claim if they have had a site visit prior to the referral.</li>
            <li>The referred site visit must be within our initial catchment area <a href="http://www.buildteam.com/about-us/cover_areas.html"><span><</span> http://www.buildteam.com/about-us/cover_areas.html <span>></span></a>.</li>
            <li>Site visits are subject to availability and must take place within office opening hours, Monday to Friday.</li>
            <li>Design Team must be notified about the referral prior to the site visit being carried out.</li>
            <li>The voucher will be a £20 John Lewis Voucher which is for online use only.</li>
        </ol>
        
		</div>
       <div style="line-height: 2; letter-spacing:2;">
     	<p class="texttitle"><strong>Design Client:</strong></p>
		<ol style="padding-left:20px;">
        	<li>This offer cannot be used in conjunction with any other offer.</li>
            <li>This offer can only be claimed when the ‘friend’ instructs Design Team for the Design Phase.</li>
            <li>Design Team must be made aware on instruction of the Design Phase. The offer can only be applied to a commencement invoice.</li>
            <li>The ‘Referrer’ will receive £250.00 in the form of John Lewis Vouchers which are for online use only.</li>
            <li>The ‘Friend’ will receive £250.00 off their design commencement invoice.</li>
        </ol>
        
		</div> 
</div>


<?php



require_once('./inc/footer.inc.php');



?>