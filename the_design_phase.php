<?php require_once('./inc/header.inc.php'); ?>

<?php //require_once('save_how_can_help_form.php'); ?>
<link rel="stylesheet" href="css/slick.css" type="text/css">
<link rel="stylesheet" type="text/css" href="/Flipbook/font-awesome.css">
<script src="/Flipbook/flipbook.min.js"></script>

<style>
	.titlehowwecan h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	border-bottom: 1px solid #003c70;
}	
.howwedesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #003c70;
    line-height: 24px;
    font-weight: normal;
}
.fliboocwrap {
	background:#f2f5f8;
	height:425px;
	widows:100%;
	margin-top:50px;
	}
.tenderprocessinner {
    max-width: 980px;
    margin: 0 auto;
}
.tenderpprocesstitle {
    text-align: center;
}
.tenderpprocesstitle h1 {
    font-weight: normal;
}
.tenderpprocesstitle h1 span {
    font-weight: bold;
}
.tenderprocess {
    padding:50px 0px;
}
.tenderslider {
    text-align: center;
	position:relative;
}
.processnumber p {
    color: #74787f;
    font-size: 16px;
}
.processnumber p span {
    font-weight: bold;
    color: #0e4776;
}
.procestitle h3 {
    font-size: 20px;
    font-weight: bold;
    color: #0f4778;
}
.processdesc span {
    font-size: 16px;
    color: #0f4776;
    margin-top: 5px;
    display: inline-block;
	line-height:20px;
}	
.processpic {
    margin: 10px 0px;
	text-align:center;
}
.processpic img {
    width: 130px;
    border-radius: 100px;
    margin: 0 auto;
}
.helpmechoose a {
    background: #f2f5f8;
    padding: 15px 31px;
    font-size: 20px;
    color: #0a4f7c;
    letter-spacing: 1px;
}
button.slick-prev.slick-arrow {
    position: absolute;
    top: 37%;
    background: none;
    color: #FFF;
    border: none;
    height: 30px;
    /* width: 30px; */
    left: -30px;

}
button.slick-next.slick-arrow {
    position: absolute;
    top: 37%;
    background: none;
    color: #FFF;
    border: none;
    height: 30px;
    /* width: 30px; */
    right: -30px;
   
}
.helpmechoose a span {
    font-weight: bold;
}
.helpmechoose {
    position: absolute;
    left: -83px;
	top:200px;
    transform: rotate(-90deg);
}
.up_down {
	display:none;
	}	
.slick-prev, .slick-next {
    font-size: 0;
    line-height: 0;
    position: absolute;
    top: 50%;
    display: block;
    width: 20px;
    height: 20px;
    padding: 0;
    -webkit-transform: translate(0, -50%);
    -ms-transform: translate(0, -50%);
    transform: translate(0, -50%);
    cursor: pointer;
    color: transparent;
    border: none;
    outline: none;
    background: transparent;
}	
.slick-prev:before, .slick-next:before {
    content: "";
    background: url(images/howcanwe/images/arrow.png) no-repeat;
    font-size: 20px;
    line-height: 1;
    height: 30px;
    /* opacity: .75; */
    /* color: white; */
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    width: 30px;
    display: block;
    background-size: contain;
}
.slick-prev:before {
	transform:rotate(180deg);
	}
.tenderslider:before {
    content: "";
    display: inline-block;
    height: 213px;
    width: 223px;
    background: url(images/howcanwe/images/curved-line.png) no-repeat center;
    position: absolute;
    background-size: contain;
    left: -120px;
}
.flipbookwrapinner {
    text-align: center;
    max-width: 980px;
    margin: 0 auto;
}
.inlineflipbook {
    width: 450px;
    height: 370px;
    float: left;
    position: relative;
}
.flipbook-main-wrapper {
	background:none !important;
	}
.flipbook-bg-light {
    background: none;
    box-shadow: none !important;
}	
.inlineflipbook.flipmargin {
    margin-right: 80px;
}
.slick-slide:last-child .tenderslider:before {
	display:none;
	}
div#flipbookone, div#flipbooktwo {
    position: relative;
    width: 100%;
    height: 100%;
}	
.flipbookwrapinner h2 {
    font-weight: bold;
    color: #0e4677;
    font-size: 16px;
    padding: 10px 0px;
}
.drop-down-box.btn-bundle:first-child {
    /* background: aliceblue; */
    border-left: 1px solid #bdc5cb;
}
.drop-down-box {
    border: 1px solid #bdc5cb;
    color: #003c70;
    padding: 14px 10px;
    font-size: 19px;
    font-weight: 600;
    margin: 20px 0;
    box-shadow: 6px 6px 0 #bdc5cb20;
    width: 17.6%;
    float: left;
    min-height: 192px;
    text-align: center;
    line-height: 20px;
}

.drop-down-box.btn-bundle {
    width: 24.8%;
    border-left: none;
    padding: 6px 0 0 0;
    box-shadow: none;
    min-height: 585px;
} 
.drop-down-box .desc {
    font-size: 16px;
    color: #74787f;
    padding-top: 10px;
    font-weight: normal !important;
}
.drop-down-box.btn-bundle .inner-box-bundle {
    padding: 0 14px;
}
.drop-down-box.btn-bundle ul li:before {
    content: ".";
    font-size: 40px;
    line-height: 0;
    left: -14px;
    top: 2px;
    position: absolute;
    color: #003c70;
}
img.o-image, img.invert-image {
    margin-bottom: 12px;
}
.drop-down-box.btn-bundle ul {
    border-top: 1px dashed #bdc5cb;
    margin-top: 16px;
    list-style: none;
    padding-top: 14px;
}
.drop-down-box.btn-bundle ul li {
    text-align: left;
    font-size: 18px;
    font-weight: 500;
    color: #74787f;
    position: relative;
    margin-left: 26px;
    line-height: 20px;
}
.bundle .drop-down-box .desc {
    font-size: 16px;
    color: #74787f;
    padding-top: 10px;
    font-weight: normal !important;
}
.bottomshortcut {
	max-width:980px;
	margin:0 auto;
	}
.shortcutinline {
    float: left;
    width: 24.2%;
}
.shortcutinline:nth-child(3) {
    margin-right: 1%;
}
.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
    margin-left: 1%;
}
.shortcutdesc {
    background: rgba(255,255,255,0.7);
    padding: 10px;
    text-align: center;
    color: #0f4679;
    position: absolute;
    bottom: 0px;
    width: 100%;
    font-weight: bold;
    font-size: 18px;
}
.shortcutwarp {
    position: relative;
}
.shortcutwarp img {
    max-width: 100%;
}
.tenderprocess {
    background: #ecebeb;
    margin: 50px 0px;
}
.tenderpprocesstitle h1 {
    font-weight: normal;
    padding: 0px;
}
.noteinfo {
    text-align: right;
    position: absolute;
    bottom: -50px;
    right: 0px;
}
.tenderprocessslider {
    position: relative;
}
.noteinfo p {
    background: #bdbdbd;
    display: inline-block;
    padding: 0;
    color: #547088;
    padding-right: 10px;
    font-size: 16px;
}
.noteinfo span {
    background: #9f9f9f;
    display: inline-block;
    width: 20px;
    text-align: center;
    padding: 5px 5px;
    color: #FFF;
    font-size: 20px;
    vertical-align: middle;
    margin-right: 10px;
}
.noteinfo.noteinfo2 {
    top: -20px;
    right: 3px;
	margin-bottom:30px;
}
.noteinfo3 {
	display:none;
	}
.slick-current .tenderslider:before {
	display:none !important;
	}
.moreinfo {
	max-width:980px;
	margin:0 auto;
	}
.moreinfo h1 {
    font-size: 20px !important;
    border-bottom: 2px solid #0f4778;
}		
@media (max-width:768px) {
	button.slick-prev.slick-arrow {
		left:0px;
		}
	button.slick-next.slick-arrow{
		right:0px;
		}
	.tenderslider:before {
		display:none;
		}	
	.inlineflipbook {
			margin-left: 0px;
			width: 100% !important;
			max-width: 100%;
		}	
	.fliboocwrap {
		background: #f2f5f8;
		height: 825px;
		widows: 100%;
		margin-top: 50px;
	}
	.inlineflipbook.flipmargin {
		margin-bottom: 30px;
	}
	.moreinfo, .bottomshortcut {
	padding:0px 20px;
	}	
	.noteinfo3 {
		display:block;
		}	
		.tenderprocess2 {
    padding: 0px 20px;
}
.drop-down-box.btn-bundle {
	width:100%;
	border:1px solid #bdc5cb !important;
	height: auto !important;
	min-height:inherit !important;
    padding-bottom: 20px;
	}	
	img.o-image, img.invert-image {
		max-width:100px;
		}
		.shortcutinline {
    float: none;
    width: 100%;
	margin-bottom:5px;
}		
.shortcutinline img {
	width:100%;
	}
	.shortcutinline:nth-child(2), .shortcutinline:nth-child(3) {
	margin-left:0px !important;
	}	
	}	
</style>
<div class="howwecanhelp">
    	<div class="titlehowwecan">
        	<h2>Architectural Design Phase</h2>
        </div>
        <div class="howwedesc">
        	<p>Over the past decade, we have perfected a flexible design process which enables you to simply select exactly what you need. Whether you require some simple planning feasibility advice, or a comprehensive tender package, our Architectural Team would be delighted to assist with your project.</p>
        </div>
    </div>
</div>

<div class="tenderprocess">
	<div class="tenderprocessinner">
    	<div class="tenderpprocesstitle">
        	<h1>Design <span>Process</span></h1>
        </div>
        
        <div class="tenderprocessslider">
        	<div class="innerporcessslider">
            	<div class="slick-slider">
                	<div>
                    	<div class="tenderslider">
                            <div class="processnumber">
                                <p>1<span>/8</span></p>
                            </div>
                            <div class="processpic">
                                <div class="picinner">
                                    <img src="images/archiimage/1.jpg" alt="">
                                </div>
                            </div>
                            <div class="procestitle">
                                <h3>Measured Survey</h3>
                            </div>
                            <div class="processdesc">
                                    <span>Enables your Designer<br>
                                    to measure up, and to<br>
                                    sit down with you to<br>
                                    discuss your design</span>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                    	<div class="tenderslider">
                            <div class="processnumber">
                                <p>2<span>/8</span></p>
                            </div>
                            <div class="processpic">
                                <div class="picinner">
                                    <img src="images/archiimage/2.jpg" alt="">
                                </div>
                            </div>
                            <div class="procestitle">
                                <h3>Design Development</h3>
                            </div>
                            <div class="processdesc">
                                    <span>Develop your design<br>
                                    with your Architectural<br>
                                    Designer. We offer<br>
                                    unlimited amendments</span>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                    	<div class="tenderslider">
                            <div class="processnumber">
                                <p>3<span>/8</span></p>
                            </div>
                            <div class="processpic">
                                <div class="picinner">
                                    <img src="images/archiimage/3.jpg" alt="">
                                </div>
                            </div>
                            <div class="procestitle">
                                <h3>Planning Application</h3>
                            </div>
                            <div class="processdesc">
                                    <span>Your Designer will<br>
                                    prepare and submit your<br>
                                    planning application on<br>
									your behalf
                                    </span>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                    	<div class="tenderslider">
                            <div class="processnumber">
                                <p>4<span>/8</span></p>
                            </div>
                            <div class="processpic">
                                <div class="picinner">
                                    <img src="images/archiimage/4.jpg" alt="">
                                </div>
                            </div>
                            <div class="procestitle">
                                <h3>Party Wall Service</h3>
                            </div>
                            <div class="processdesc">
                                    <span>If you need to serve<br>
                                    Notice, our Party Wall<br>
                                    department can assist<br>
                                    with Party Wall matters</span>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                    	<div class="tenderslider">
                            <div class="processnumber">
                                <p>5<span>/8</span></p>
                            </div>
                            <div class="processpic">
                                <div class="picinner">
                                    <img src="images/archiimage/5.jpg" alt="">
                                </div>
                            </div>
                            <div class="procestitle">
                                <h3>Planning Decision</h3>
                            </div>
                            <div class="processdesc">
                                    <span>We contact your Case<br>
                                    Officer at various stages<br>
                                    to ensure everything is<br>
                                    on track</span>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                    	<div class="tenderslider">
                            <div class="processnumber">
                                <p>6<span>/8</span></p>
                            </div>
                            <div class="processpic">
                                <div class="picinner">
                                    <img src="images/archiimage/6.jpg" alt="">
                                </div>
                            </div>
                            <div class="procestitle">
                                <h3>Building Reg Pack</h3>
                            </div>
                            <div class="processdesc">
                                    <span>We create your Building<br>
                                    Regulation Pack which<br>
                                    includes structural<br>
                                    calculations</span>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                    	<div class="tenderslider">
                            <div class="processnumber">
                                <p>7<span>/8</span></p>
                            </div>
                            <div class="processpic">
                                <div class="picinner">
                                    <img src="images/archiimage/7.jpg" alt="">
                                </div>
                            </div>
                            <div class="procestitle">
                                <h3>Tender Pack</h3>
                            </div>
                            <div class="processdesc">
                                    <span>We issue your Design<br>
                                    Completion Pack which<br>
                                    includes your Detailed<br>
                                    Schedule of Works</span>
                            </div>
                        </div>
                    </div>
                    
                    <div>
                    	<div class="tenderslider">
                            <div class="processnumber">
                                <p>8<span>/8</span></p>
                            </div>
                            <div class="processpic">
                                <div class="picinner">
                                    <img src="images/archiimage/8.jpg" alt="">
                                </div>
                            </div>
                            <div class="procestitle">
                                <h3>Build Tender</h3>
                            </div>
                            <div class="processdesc">
                                    <span>If you would like our<br>
                                    Build arm to offer their<br>
                                    tender they would be<br>
                                    delighted to do so</span>
                            </div>
                        </div>
                    </div>
                   
                </div>
            	
            </div>
            <div class="noteinfo noteinfo3">
            	<a href="https://www.buildteam.com/architectural-design-phase.html"><p><span>i</span>Check out our detailed timeline</p></a>
            </div>
        </div>
    </div>
</div>   

<div class="tenderprocess2">
	<div class="tenderprocessinner">
    	<div class="tenderpprocesstitle">
        	<h1>Choose Your <span>Design Bundle</span></h1>
        </div>
        <div class="howwedesc">
        	<p>As every project is unique, homeowners frequently approach us with different requirements. For this reason, we have created some simple Design Bundles which include different levels of involvement from our team. Whether you’re starting from scratch, have planning permission or simply need structural calculations, we can adjust our scope to fit your requirements, so you only pay for what you need.</p>
        </div>
        
        <div class="inner-box" data-id="bundle" style="">
					<div class="drop-down-box btn-bundle" data-id="preplaning">
						<div class="inner-box-bundle">
							<div class="">
								<img src="/images/get-quote/new_icons.png" class="o-image">
								
							</div>
							<div class="">
								<div class="">Pre-Planning Advice</div>
								<div class="desc">Explore feasibility options via your local council
</div>
							</div>
						</div>
						<div class="clear"></div>
						<ul>
							<li>Expert advice on planning </li>
							<li>Drawings and design development </li>
							<li>Pre-Planning application</li>
						</ul>
					</div>
					<div class="drop-down-box btn-bundle" data-id="designplan">
						<div class="inner-box-bundle">
						<div class="">
							<img src="/images/get-quote/new_icons2.png" class="o-image">
							
						</div>
						<div class="">
							<div class="">Design &amp; Planning </div>
							<div class="desc">Finalise your design and obtain planning consent
</div>
						</div>
						</div>
						<div class="clear"></div>
						<ul>
							<li>Full measured survey </li>
							<li>Unlimited drawing amendments </li>
							<li>Planning advice and submission of application</li>
						</ul>
					</div>
					<div class="drop-down-box btn-bundle" data-id="ggtb">
						<div class="inner-box-bundle">
						<div class="">
							<img src="/images/get-quote/new_icons3.png" class="o-image">
							
						</div>
						<div class="">
							<div class="">Building Reg. Pack</div>
							<div class="desc">Get ready to build with full Building Control drawings
</div>
						</div>
						</div>
						<div class="clear"></div>
						<ul>
							<li>Full measured survey </li>
							<li>Unlimited drawing amendments </li>
							<li>Planning advice and submission of application </li>
							<li>Full Building Reg. Pack</li>
							<li>Structural calculations</li>
                            <li>Detailed schedule of works</li>
						</ul>
					</div>
					<div class="drop-down-box btn-bundle" data-id="fftb">
						<div class="inner-box-bundle">
						<div class="">
							<img src="/images/get-quote/new_icons4.png" class="o-image">
							
						</div>
						<div class="">
							<div class="">Fast Track To Build </div>
							<div class="desc">Fast Track your Tender Pack and begin collecting contracts
</div>
						</div>
						</div>
						<div class="clear"></div>
						<ul>
							<li>Full measured survey</li>
							<li>Unlimited drawing amendments </li>
							<li>Planning advice and submission of application </li>
							<li>Fast Track full Building Reg. Pack (save 8 weeks) </li>
							<li>Fast Track Structural calculations (save 8 weeks) </li>
							<li>Structural amendments guarantee - should the Case Officer request amendments to the design during the planning window, we will undertake any re-engineering required free of charge</li>
							<li>Party Wall Notice to one adjoining owner</li>
						</ul>
					</div>
                    <div class="clearfix"></div>
				</div>
                <div class="noteinfo noteinfo2">
            	<a href="https://www.buildteam.com/design_bundles.html"><p><span>i</span>Use our Bundle Calculator to get an idea of cost</p></a>
            </div>
</div>

</div>   
<div class="moreinfo">
    	<h1>Find Out More</h1>
    </div>      
<div class="bottomshortcut">
	<div class="shortcutinline">
    	<a href="https://www.buildteam.com/watch_a_build.html">
            <div class="shortcutwarp">
                <img src="images/archiimage/1-1.jpg" alt="">
                <div class="shortcutdesc"><span>Watch A Build</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/getting-started/book-site-visit.html ">
            <div class="shortcutwarp">
                <img src="images/archiimage/1-2.jpg" alt="">
                <div class="shortcutdesc"><span>Book a Site Visit</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
    	<a href="https://www.buildteam.com/project-gallery/gallery.html">
            <div class="shortcutwarp">
                <img src="images/archiimage/1-3.jpg" alt="">
                <div class="shortcutdesc"><span>Browse Our Gallery</span></div>	
            </div>
        </a>
    </div>
    <div class="shortcutinline">
        <a href="https://www.buildteam.com/party_wall.html">
            <div class="shortcutwarp">
                <img src="images/archiimage/1-4.jpg" alt="">
                <div class="shortcutdesc"><span>Party Wall Service</span></div>	
            </div>
         </a>
    </div>
</div>
<script src="js/slick.min.js"></script>

<script>
$(".slick-slider").click(function(){
	//alert();
	$(".noteinfo3").fadeIn(500);
	});
$(".helpmechoose").click(function(){
	$(".quizwrap").animate({left:"0%"});
	});
$(".closebuttonquiz").click(function(){
	$(".quizwrap").animate({left:"-100%"});
	});	
</script>
<script>
$('.slick-slider').slick({
  slidesToShow: 4,
  slidesToScroll: 4,
  autoplay: false,
  autoplaySpeed: 2000,
  responsive: [
    {
      breakpoint: 768,
      settings: {
        arrows: true,
        centerMode: true,
        centerPadding: '10px',
        slidesToShow: 1
      }
    }
	]
});
</script>
<?php require_once('./inc/footer.inc.php'); ?>