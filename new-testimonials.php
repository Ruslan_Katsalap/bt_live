<?php

require_once('./inc/header.inc.php');
?>

<style>
/* Css for gallery */
@media only screen and (min-width: 1010px) {
	.full {
		width: 1006px;
		position:relative;
	}
}
.prebutton img {
    max-width: 120px;
}
.prebutton {
    position: absolute;
    bottom: 10px;
    right: 10px;
}
.img-block {
    
    position: relative;
}
.col_one_third a {
    position: relative;
    width: 100%;
    display: block;
}
.col_one_third img {
    border: 0;
    /* max-width: 450px !important; */
    width: 100% !important;
	max-width: inherit !important;
	//min-height: 200px;
}
@media (min-width: 800px) {
	.col_one_third {
		margin-right: calc(2.7% - 10px);
		margin-bottom: 16px;
		width:28%;
	}
}
</style>
<style>
/* ---- button ---- */

.button-group  .button {
  display: inline-block;
  padding: 10px 18px;
  margin-bottom: 10px;
  background: #EEE;
  border: none;
  border-radius: 7px;
  background-image: linear-gradient( to bottom, hsla(0, 0%, 0%, 0), hsla(0, 0%, 0%, 0.2) );
  color: #222;
  font-family: sans-serif;
  font-size: 16px;
  text-shadow: 0 1px white;
  cursor: pointer;
}

.button-group  .button:hover {
  background-color: #8CF;
  text-shadow: 0 1px hsla(0, 0%, 100%, 0.5);
  color: #222;
}

.button-group  .button:active,
.button-group  .button.is-checked {
  background-color: #28F;
}

.button-group  .button.is-checked {
  color: white;
  text-shadow: 0 -1px hsla(0, 0%, 0%, 0.8);
}

.button-group  .button:active {
  box-shadow: inset 0 1px 10px hsla(0, 0%, 0%, 0.8);
}

/* ---- button-group ---- */

.button-group:after {
  content: '';
  display: block;
  clear: both;
}
.clear {
	clear: both;
}
.button-group .button {
  float: left;
  border-radius: 0;
  margin-left: 0;
  margin-right: 1px;
}

.button-group .button:first-child { border-radius: 0.5em 0 0 0.5em; }
.button-group .button:last-child { border-radius: 0 0.5em 0.5em 0; }
.grid-gallery {
	position: relative;
}
a.hover-box {
    display: none;
    outline: none;
    position: absolute;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    background-color: rgba(255, 255, 255, 0.77);
    visibility: hidden;
    opacity: 0;
}
.inner-hover-box {
	display: block;
    position: absolute;
    text-align: center;
    top: 50%;
    left: 50%;
    width: 100%;
    transform: translateX(-50%) translateY(-50%);
}
.inner-hover-box i {
    display: inline-block;
    font-style: normal;
}
.inner-hover-box i {
font-size:24px;
font-weight: 500;
}
.element-item-gallery:hover .hover-box {
    visibility: visible;
    opacity: 1;
    display: block;
}
.filter-btn {
	border: none;
    color: #fff;
    padding: 14px 0px;
    background: #0f4778;
    font-size: 16px;
    /* box-shadow: none; */
	    border-radius: 5px;
		display: block;
    width: 100%;
}
.noresult {
	display:none;
	padding:50px 0px;
	text-align:center;
}
.noresult p {
	color:#f58630;
	font-size:22px;
	font-weight: 500;
}
.noresult p a{
	color:#f58630;
	text-decoration:underline;
}
@media only screen and (max-width: 768px){
	.col_one_fourth  {
		min-height: 20px !important;
	}
	.nice-select.wide {
		width: 100% !important;
	}
	.col_one_fourth:nth-child(2n) {
		margin: 0 auto;
	}
}

</style>
<style>
.main p, .main-bottom li {
	font-size: 18px;
	color: #9A9A9A;
	line-height: 20px;
}
.main-bottom li {
	line-height: 26px;
}
.main-bottom ul {
	list-style: none;
}
.main-bottom li::before{
	content:"•";
	color: rgb(15,71,120);
	display:inline-block; width:1em; margin-left:0px;
}
hr{
	
	height:1px;
	display: block;
	border:0px;
	border-top:2px solid rgba(154,154,154,0.5);
	margin: 1em 0;
	padding:0
}
.list{
	position:relative;
	display: inline-block;
	width: 485px;
	vertical-align: top;
	line-height:22px;
}

.list ul {
    margin-top: 0;
    padding-top: 0;
}
.list ul.pleft {
	padding-left: 25%;
}
.Testimonial{
	 margin-bottom: 0;
	padding-bottom: 0;
}
.Testimonialtitle{
	margin-top: 0;
	padding-top: 0;
	
}
.main-bottom {
	padding: 0 !important;
}
.main-bottom .col_two_fifth.col_last {
	display: none;
}
h1.p-heading {
	margin-bottom:18px !important;
	padding-bottom: 0 !important;
}
h2 {
	font-size: 24px !important;
    color: #003c70;
    text-transform: capitalize !important;
    font-weight: bold;
    font-family: calibri !important;
}
h2.p-heading { 
	margin-bottom: 20px !important;
	padding-bottom: 0 !important;
}
.main-bottom h2 {
	font-size: 18px;
	color: #003c70;
	margin-top: 0;
	margin-bottom: 5px !important;
}
.img-block {
	margin: 20px 0;
}
.img-block img {
	width: 100%;
}
.list-space li {
	height: 0;
    overflow: hidden;
   	animation: slide 0.3s ease 0.3s forwards;
}
.list-space li {
    list-style: none;
    font-size: 17px;
    font-weight: normal !important;
    padding: 0px !important;
    margin: 0px !important;
    margin-bottom: 10px !important;
    color: #0f4778 !important;
    cursor: pointer;
    background-image: url(http://www.buildteam.com/images_new_design/faq-arrow-img.jpg);
    background-position: 0px -54px;
    background-repeat: no-repeat;
    padding-left: 30px !important;
	text-align:justify;
	    width: 93%;
	  }

.expand
{
	background-position: 0px 1px !important;
}
	  
.list-space span {
    font-size: 15px important;
    font-weight: normal;
    line-height: 24px;
    margin-bottom: 18px;
    color: #7b7b7b;
    margin-top: -10px;
    width: 97%;
    font-size: 12pt !important;
    font-family: roboto !important;
    text-indent: 0px !important;
    padding-left: 30px;
}

.list-space span p{
text-align:justify;
padding-top:5px;
font-weight: normal;
font-family: calibri;
}

.list-space span p span{
	margin-left:0px !important;
}
@keyframes slide {
  from { height: 0;}
  to {height: 20px;}
}	
@media (min-width:480px) and (max-width:1024px){
	body{
	  margin-left:100px;
	  margin-right:100px;
	  margin-top:120px;
	}

	.list{
		width: 100%;
	}
	.list ul.pleft {
		padding-left: 0;
	}
}

@media (min-width:320px) and (max-width:799px){
	.col_one_third {
		width: calc(100% - 36px);
	}
	
}

@media only screen and (max-width: 480px)
{
	p, li {
		font-size: 14px !important;
	}
	.list{
		width: 100%;
	}
	.list ul.pleft {
		padding-left: 0;
	}
.list-space li
{
margin-left:10px !important;
box-sizing: border-box;
line-height: 20px;
text-align: left;
}
.list-space span {
    margin-left: 50px;
	box-sizing: border-box;
	width: 82%;
	line-height: 20px;
}
.list-space span p {
    text-align: left;
    padding-top: 5px;
}
}
@media (max-width:640px) {
	.prebutton img {
    max-width: 50px;
}
	}
	
.custom-col {
	float: left;
	width: 33.33%;
}
q {
  quotes: "“" "”" "‘" "’";
 // color: #003c70;
  font-size: 14px;
font-family: calibri;
line-height: 22px;
font-weight: 600;
color: #003c70;
}
q:before {
    content: open-quote;
	font-size: 70px;
	float: left;
	width: 100%;
	font-family: arial;
	text-align: center;
	color: #939dad;
	line-height: 0;
	padding-top: 56px;
	padding-bottom: 8px;
}
q:after {
    content: close-quote;
	font-size: 70px;
	float: left;
	width: 100%;
	font-family: arial;
	text-align: center;
	color: #939dad;
	line-height: 0;
	padding-top: 44px;
	padding-bottom: 20px;
}
.box-bg {
	background-color: rgba(244,244,244, .5);
	padding: 18px;
	overflow: hidden;
}
.box-bg .box-content {
	border-top: 2px dotted #939dad;
	border-bottom: 2px dotted #939dad;
	float: left;
	width: 100%;
	color: #003c70;
	text-align: center;
}
.box-bg video {
	max-height: 170px;
	max-width: 400px;
}
.box-bg .client-info .c-info {
	padding: 10px 0;
	text-align: center;
	font-size: 14px;
	font-family: calibri;
	color: #003c70;
	padding-bottom: 30px;
	font-weight: 500;
	font-style: italic;
}
.box-bg .client-info .c-image {
	float: right;
}
.box-bg .client-info .c-image img{
	padding-bottom: 6px;
	//max-height: 30px;
	//min-height: auto;
	max-width: 60px !important;
}
.playpause {
    background-image:url(../housetour/play_arrow.png);
    background-repeat:no-repeat;
    width:100%;
    height:100%;
    position:absolute;
    left:0%;
    right:0%;
    top:0%;
    bottom:0%;
    margin:auto;
    background-size:cover;
    background-position: top;
}	
.video-outter {
	position: relative;
	margin: 18px 0;
}
.filtertab {
    /* text-align: right; */
    max-width: 980px;
    margin-bottom: 10px;
    /* position: absolute; */
    width: 100%;
    /* top: 10px; */
    border-top: 1px solid;
    border-top: 2px solid #c3d1dd;
    border-bottom: 2px solid #c3d1dd;
    margin-bottom: 30px;
    margin-top: 30px;
    display: block;
}
.filtertab select {
	height:30px;
	width:150px;
	}
.inlinefilters {
    display: inline-block;
    margin-right: 30px;
    padding: 15px 0px;
}	
.inlinefilters p {
    font-weight: bold;
    margin-top: 0px;
    padding-top: 0px;
}	
.inlinefilters p a{ 
	font-weight:normal;
	color:#969696;
}
.inlinefilters p a.active{ 
	font-weight:normal;
	color:#f1622a !important;
}
</style>
<link rel="stylesheet" type="text/css" href="/css/nice-select.css" />
<div class="full">
	<h1 class="p-heading">Testimonials</h1>
    <div class="filtertab">
    	<div class="insidefiltertab">
    		<div class="inlinefilters">
            	<p>Design Testimonials | <a href="javascript:void(0)" data-filter=".Design">Filter</a></p>
            </div>
    		<div class="inlinefilters">
            	<p>Build Testimonials | <a href="javascript:void(0)" data-filter=".Build">Filter</a></p>
            </div>
    		<div class="inlinefilters">
            	<p>All Testimonials | <a href="javascript:void(0)" class="active" data-filter=".All">Filter</a></p>
            </div>
            </div>
        	<!--<form action="">
            	<select name="" id="">
                	<Option disabled selected>Category</Option>
                	<Option data-filter=".Design">Design</Option>
                    <Option data-filter=".Build">Build</Option>
                </select>
            </form>-->
        </div>
	<div class="grid-gallery">
    	
		<?php
			$i = 0;
			$rs = getRs("SELECT testimonial_id, quote, name, title, project_url, video_url, thumbnailImg,category FROM testimonials WHERE is_active = 1 AND is_enabled = 1 ORDER BY sort");
			$total = mysqli_num_rows($rs);
			$half = false;
			while ( $row = mysqli_fetch_assoc($rs) ) {
				  $i++;
				  $cls = $i % 3 == 0 ? 'col_last' : '';
				  if($row['video_url'] != '' && $row['video_url'] != null) {
					  $html = '<div class="col_one_third box-bg All '.$cls.' '.$row['category'].'" data-cost="'.$row['category'].'">
						<div class="box-content">
							<div class="video-outter">
								<video class="video" controls>
									<source src="'.$row['video_url'].'" type="video/mp4"> 
								</video>
								<div class="playpause"></div>
							</div>
						</div>
						<div class="clear"></div>
					</div>';
					  
				  } else {
					  $html = '<div class="col_one_third box-bg All '.$cls.' '.$row['category'].'" data-cost="'.$row['category'].'">
						<div class="box-content">
							<q>
							 '.$row['quote'].'
							</q>
							<div class="client-info">
								<div class="c-info">'.$row['name'] . ', ' . $row['title'].'</div>';
							if($row['thumbnailImg'] != '') {
								$html .= '<div class="c-image"><a href="'.$row['project_url'].'" title="Check out our gallery">
								<img src="/phpthumb/phpThumb.php?src='.urlencode('/testimonials/thumbnail/' . $row['thumbnailImg']) . '&amp;w=150&h=50" style="width:100%;max-width:450px" alt="' . htmlentities($row['title']) . '" />
								</a></div>';
							}
								
						$html .= '</div>
						</div>
						<div class="clear"></div>
					</div>'; 
				}
				echo $html;
			}
		?>
	</div>
</div>

<?php

require_once('./inc/footer.inc.php');

?>
<script src="/js/isotope.pkgd.min.js"></script>
<script src="/js/jquery.nice-select.js"></script>
<script>
// external js: isotope.pkgd.js
//$(document).ready(function() {
// init Isotope
var $grid2 = $('.grid-gallery').isotope({
  // options
  itemSelector: '.col_one_third',
  //percentPosition: true
});
var filterFns = {
  // show if number is greater than 50
  numberGreaterThan50: function() {
    var number = $(this).find('.number').text();
    return parseInt( number, 10 ) > 50;
  },
  // show if name ends with -ium
  ium: function() {
    var name = $(this).find('.name').text();
    return name.match( /ium$/ );
  }
};
$('.inlinefilters p a').on( 'click', function() {
	var filterValue = $(this).attr('data-filter');
	$('.inlinefilters p a').removeClass("active");
	$(this).addClass("active");
	//var newfilter = String(filterValue)
	//filterValue = filterFns[ filterValue ] || filterValue;
	//alert(filterValue);
	
  
  	// use filterFn if matches value
  	//filterValue = filterFns[ filterValue ] || filterValue;
  	$grid2.isotope({ filter: filterValue });
});
//});
$(document).ready(function() {
		setTimeout(function() {
			/*var $grid = $('.grid-gallery').isotope({
				itemSelector: '.col_one_third',
				percentPosition: true
			});*/
			 $('.video').parent().click(function () {
				if($(this).children(".video").get(0).paused){
					$(this).children(".video").get(0).play();
					$(this).children(".playpause").fadeOut();
				}else{
				   $(this).children(".video").get(0).pause();
					$(this).children(".playpause").fadeIn();
				}
			});
		}, 1000);
});
</script>