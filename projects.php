<?php

require_once('./inc/header.inc.php');
?>

<div class="full">

<?php

$ret = '';
$c_id = 0;
$project_code = '';

if ( isset($_GET['c']) ) {
	$project_code = $_GET['c'];
}

if ( strlen($project_code) == 0 ) {
	$rs = getRs("SELECT c.project_category_id, c.project_category_code, c.project_category_name, p.project_code, p.project_name, p.imagefile FROM project p INNER JOIN project_category c ON c.project_category_id = p.project_category_id WHERE p.is_active = 1 AND p.is_enabled = 1 ORDER BY c.sort, c.project_category_id, p.sort, p.project_id");
	$ret .= '
    <div id="bc"><a href="/">Home</a> &rsaquo; <a href="/our-experience.html">Our Experience</a> &rsaquo; <b>Past Projects</b></div>
    <h1>Past Projects</h1>';
	while ( $row = mysqli_fetch_assoc($rs) ) {
		if ( $c_id <> $row['project_category_id'] ) {
			if ( $c_id > 0 ) {
				$ret .= '</ul>';
			}
			$ret .= '<h3>' . $row['project_category_name'] . '</h3>';
			$ret .= '<ul>';
			$c_id = $row['project_category_id'];
		}
		$ret .= '
		<li> <a href="/our-experience/projects/' . $row['project_category_code'] . '/' . $row['project_code'] . '.html">' . $row['project_name'] . '</a></li>
		';
	}	
	$ret .= '</ul>';

}

else {

	$rs = getRs("SELECT c.project_category_code, c.project_category_name, i.filename, p.project_name FROM project_category c INNER JOIN (project_image i INNER JOIN project p ON p.project_id = i.project_id) ON c.project_category_id = p.project_category_id WHERE c.is_active = 1 AND c.is_enabled = 1 AND p.is_active = 1 AND p.is_enabled = 1 AND i.is_active = 1 AND i.is_enabled = 1 AND p.project_code = '" . formatSql($project_code) . "' ORDER BY i.sort, i.project_image_id");

	$ret_1 = '';
	$ret_2 = '';
	$i = 0;

	while ($row = mysqli_fetch_assoc($rs) ) {
		if ( $i == 0 ) {
			echo '<div id="bc"><a href="/our-experience.html">Our Experience</a> &rsaquo; &rsaquo; <a href="/our-experience/projects/' . $row['project_category_code'] . '.html">' . $row['project_category_name'] . '</a> &rsaquo; <b>' . $row['project_name'] . '</b></div><h1>' . $row['project_name'] . '</h1>';
		}
		$ret_1 .= 
		'<div class="panel">
			<img src="/projects/' . $row['filename'] . '" alt="" /> 
			<div class="panel-overlay">
				<h2>' . $row['project_name'] . '</h2>
				View full-size photo <a href="/projects/' . $row['filename'] . '" alt="" rel="shadowbox">here</a>.</p>
			</div>
		</div>
		';
		$ret_2 .= '<li><img src="/projects/' . $row['filename'] . '" width="100" alt="" /></li>';
		$i += 1;
	}


echo '
<div id="photos" class="galleryview">
	' . $ret_1 . '
  <ul class="filmstrip">
    ' . $ret_2 . '
  </ul>
</div>
';

}

?>


<?php echo $ret; ?>

 


 

      
      </p>
		</div>
<?php

require_once('./inc/footer.inc.php');

?>