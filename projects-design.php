<?php

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    
    	<div id="bc"><a href="index.php">Home</a> &rsaquo; <a href="our-experience">Our Experience</a> &rsaquo; <b>Past Projects</b></div>
      
    	<h1>Past Projects</h1>
      <p>
Design-Led
<ul>
	<li> Avenue Road, St John’s Wood</li>
  <li> 12 Kensington Place, W8</li>
  <li> Millbank Roof Terrace, SW1</li>
  <li> Golden Lane, EC1 (under construction)</li>
  <li> Westmoreland Place, SW1 (under construction)
</li></ul>
 

Refurbishment
<ul>
  <li> King George Street, Greenwich</li>
  <li> Shoreham by Sea</li>
  <li> Lord North Street, SW1</li>
  <li> Westbourne Grove, W2 </li>
  <li> John Islip Street (Flat 37), SW1

</li></ul> 

 

      
      </p>
		</div>
    <div class="right">
    	<img src="images/ourexperience03.jpg" width="326" height="436" alt="BuildTeam" />
    </div>
<?php

require_once('./inc/footer.inc.php');

?>