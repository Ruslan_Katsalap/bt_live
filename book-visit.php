<?php
header("");
require_once('./inc/header.inc.php');
?>
<!--link href="/css/book-visit.css?v=1" rel="stylesheet"-->
<?php
$flagenew = false;
global $a_settings;
$newflaging = false;
require_once('./inc/settings.php');
require_once('./sendgrid/sendgrid-php.php');


if (!isset($_GET['type']) || !in_array($_GET['type'], array_keys($a_settings['a_visit_types']))) {
    header('Location: /getting-started/book-site-visit-new.html');
    exit();
}
  
$error = "";

@session_start();


if (isset($_SESSION[WISE_DB_NAME]['book_visit']['fname']) && !isset($_POST['nonce'])) {
  
  //echo '!!!<pre>'.print_r($_SESSION[WISE_DB_NAME]['book_visit'], 1).'</pre>';#debug
  
    $_POST['fname'] = $_SESSION[WISE_DB_NAME]['book_visit']['fname'];
    $_POST['email'] = $_SESSION[WISE_DB_NAME]['book_visit']['email'];
    $_POST['phone'] = $_SESSION[WISE_DB_NAME]['book_visit']['phone'];
    $_POST['coupon_code'] = $_SESSION[WISE_DB_NAME]['book_visit']['coupon_code'];
    $_POST['postcode'] = $_SESSION[WISE_DB_NAME]['book_visit']['postcode'];
    $_POST['addr'] = $_SESSION[WISE_DB_NAME]['book_visit']['addr'];
    $_POST['svSubscribe'] = $_SESSION[WISE_DB_NAME]['book_visit']['svSubscribe'];
    $_POST['comments'] = $_SESSION[WISE_DB_NAME]['book_visit']['comments'];
    $_POST['availibility'] = isset($_SESSION[WISE_DB_NAME]['book_visit']['availibility'])?$_SESSION[WISE_DB_NAME]['book_visit']['availibility']:'';
    //$mystring = $_GET['coupon_code'];
    //echo $mystring;
  
    if ($_POST['availibility']) {
        $o_avail = json_decode($_POST['availibility']);
        $a_pref_dates = array();
        $availibility = '';
        if ($o_avail) {
            foreach ($o_avail as $k => $v) {
                $k = explode('/', $k);
                $k = $k[1] . '.' . $k[0] . '.' . $k[2];
                $availibility .= $k.' - '.strtoupper($v)."\n";
        
                $a_pref_dates[$k] = $v;
            }
      
            $availibility = trim($availibility);
      
            //echo '<pre>'.$availibility.'!!!'.print_r($o_avail, 1).'</pre>';#debug
        }
    }
}

if (isset($_POST['nonce']) && $_POST['nonce']==session_id()) {
  // validation, set values:
  
    $k = 'fname';
    $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
    if (!$_POST[$k]) {
        $error .= "Enter Full Name \n";
        
    } else
    {
    } //enter Full name
  
    $k = 'email';
    $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
    if (!$_POST[$k]) {
        $error .= "Enter Email address\n";
        
    } //enter Your email\n
    else {
        if (!preg_match("/^[a-z0-9_\.-]+@([a-z0-9]+([\-]+[a-z0-9]+)*\.)+[a-z]{2,7}$/i", $_POST[$k])) {
            $error .= "Enter correct Email address \n"; //enter correct Email address
            
        }else
        {
        }
    }
  
    $k = 'phone';
    $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
    if (!$_POST[$k]) {
        $error .= "Contact number \n";
        
    } //enter Contact number
    else {
        if (!preg_match("/^[0-9\.\-\s\(\)]{10,}$/i", $_POST[$k])) {
            $error .= "Enter correct Contact number (at least 10 digits) \n"; //enter correct Contact number (at least 10 digits)
            
        }else
        {
        }
    }
  
  
    $k = 'postcode';
    $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
    if (!$_POST[$k]) {
        $error .= "Enter Postcode \n";
        
    } //enter Postcode
    else {
              $_POST[$k] = strtoupper($_POST[$k]);
    
        $postcode = $_POST[$k];

        include BT_PATH.'xml/postcode.inc.php';
        $valid = bypValidatePostcode($postcode, 'session');
        /*
        if ($valid) {
          $_SESSION[WISE_DB_NAME]['postcode'] = $postcode;
        }
        else {
          unset($_SESSION[WISE_DB_NAME]['postcode']);
        }
        */
    
        if (!$valid) {
          
            $error .= "Enter correct UK Postcode"; //enter correct UK Postcode
        } else { // if valid, check territory:
      $rs = getRs("SELECT map_postcodes FROM setting s WHERE s.setting_id = 1");
    
            $map_postcodes = array();
            $out_of_territory = false;
            $row = mysqli_fetch_assoc($rs);
            if (isset($row['map_postcodes'])) {
                $map_postcodes = explode(',', $row['map_postcodes']);
            }
      
            //echo '<pre>'.print_r($map_postcodes, 1).'</pre>';#debug
            //exit;//debug
      
            $postpart = $postcode;
            $postpart = str_replace(' ', '', $postpart);
    
            if ($map_postcodes) {
                if (strlen($postpart)<6 && strlen($postpart)>4) {
                    $postpart = substr($postpart, 0, 2);
                } elseif (strlen($postpart)==6) {
                    $postpart = substr($postpart, 0, 3);
                } else {
                    $postpart = substr($postpart, 0, 4);
                }
      
                //echo print_r($map_postcodes, 1).' '.$postpart.':)';#debug
      
                $cover = false;
                foreach ($map_postcodes as $v) {
                    $v = trim($v);
                    if (strtoupper($postpart) == strtoupper($v)) {
                        $cover = true;
                        break;
                    }
                }
      
                if (!$cover) {
                    $out_of_territory = true;
                }
            }
      
            if ($out_of_territory) {
                //$flagenew = true;
                $error .= "\n";
                $flagenew = true;
            }
        }
    }
  
    $k = 'addr';
    $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
  
    if (!$_POST['addr']) {
        $error .= "Enter Your House / Flat No & Street \n"; //enter Your House / Flat No & Street
    }
  
    $k = 'availibility';
    $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
    if (!$_POST['availibility']) {
        $error .= "select at least one Your Availibility Date\n";
    } else {
        $o_avail = json_decode($_POST['availibility']);
        $a_pref_dates = array();
        $availibility = '';
        if (!$o_avail) {
            $error .= "select at least one Your Availibility Date\n";
        } else {
            foreach ($o_avail as $k => $v) {
                $k = explode('/', $k);
                $k[2] = substr($k[2], 2, 3);
                $k = $k[1] . '.' . $k[0] . '.' . $k[2];
                $availibility .= $k.' - '.strtoupper($v)."\n";
                $a_pref_dates[$k] = $v;
            }
            $availibility = trim($availibility);
            //echo '<pre>'.print_r($o_avail, 1).'</pre>';#debug
            if (!$a_pref_dates) {
                $error .= "select at least one Your Availibility Date\n";
            }
        }
    }
  
    $k = 'comments';
    $_POST[$k] = isset($_POST[$k])?trim(get_magic_quotes_gpc()?stripslashes($_POST[$k]):$_POST[$k]):'';
    $_POST[$k] = substr($_POST[$k], 0, 500); // limit comment length, short comment
     //echo $error;
    if (!$error) {
        $visit_id = 0; // order ID, 0 means add new order

        $to = "hello@buildteam.com"; //leana.gardner@buildteam.com,
        $replyTo = $_POST['email'];
        $from = $to;
        $subject = "Requested Visit";
        //$subject2 = "Requested Visit to BuildTeam";

        $message = "

            <html>

            <head>

            <title>Request Call</title>

            </head>

            <body>
            
            <table style='text-align:left; font-family:calibri;'>
            
            <tr>

            <td style='text-align:left;'>Request visit form is filled by the user with the information below.</td>

            </tr>
            
            <tr>

            <td style='text-align:left;'><strong>Full Name :</strong> ".$_POST['fname']."</td>

            </tr>

            <tr>

            <td><strong>Email :</strong> ".$_POST['email']."</td>

            </tr>

            <tr>

            <td><strong>Phone :</strong> ".$_POST['phone']."</td>

            </tr>

            <tr>

            <td><strong>Postcode :</strong> ".$_POST['postcode']."</td>

            </tr>
            <tr>

            <td><strong>Address :</strong> ".$_POST['addr']."</td>

            </tr>

            </table>

            </body>

            </html>

            ";
            
           /* $message2 = "

            <html>

            <head>

            <title>Request Call</title>

            </head>

            <body>
            
            <table style='text-align:left; font-family:calibri;'>
            
            <tr>

            <td style='text-align:left;'></td>

            </tr>
            
            <tr>

            <td style='text-align:left;'><strong>Full Name :</strong> ".$_POST['fname']."</td>

            </tr>

            <tr>

            <td><strong>Email :</strong> ".$_POST['email']."</td>

            </tr>

            <tr>

            <td><strong>Phone :</strong> ".$_POST['phone']."</td>

            </tr>

            <tr>

            <td><strong>Postcode :</strong> ".$_POST['postcode']."</td>

            </tr>
            <tr>

            <td><strong>Address :</strong> ".$_POST['addr']."</td>

            </tr>

            </table>

            </body>

            </html>

            ";*/
/*            

        // Always set content-type when sending HTML email

        $headers1 = "MIME-Version: 1.0" . "\r\n";

        $headers1 .= "Content-type:text/html;charset=UTF-8" . "\r\n";

        //$headers[] = 'MIME-Version: 1.0';

        $headers1 .= 'Content-type: text/html; charset=iso-8859-1';

        // More headers

        $headers1 .= 'From: <webmaster@buildteam.com>' . "\r\n";

        //$headers1 .= 'CC: aiste.cesonyte@buildteam.com' . "\r\n";


        // More headers

        //$headers .= 'From: <webmaster@buildteam.com>' . "\r\n";

        //$headers .= 'Cc: info@buildteam.com' . "\r\n";



        mail($to, $subject, $message, $headers1);
       // mail($from, $subject2, $message2, $headers1);
*/    
    putenv('SENDGRID_API_KEY=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');
	$email = new \SendGrid\Mail\Mail();
	$email->setFrom($from);
    //$email->setFromName($_POST['fname']);
    $email->setReplyTo($replyTo, $_POST['fname']);
	$email->setSubject($subject);
	$email->addTo($to, "Buildteam support");
	//$email->addTo("shizuma@gmx.net", "Buildteam support");
	/*$email->addContent("text/plain", "and easy to do anywhere, even with PHP");*/
	$email->addContent("text/html", $message);
	$sendgrid = new \SendGrid(getenv('SENDGRID_API_KEY'));
    //$sendgrid = new \SendGrid('');
	try {
	    $response = $sendgrid->send($email);
	    /*print $response->statusCode() . "\n";
	    print_r($response->headers());
	    print $response->body() . "\n";*/
	} catch (Exception $e) {
	    echo 'Caught exception: '. $e->getMessage() ."\n";
	    exit;
	}
    //die($_POST[]);

        if (isset($_SESSION[WISE_DB_NAME]['book_visit']['visit_id'])) {
            $visit_id = $_SESSION[WISE_DB_NAME]['book_visit']['visit_id'];
        }
    
        $_SESSION[WISE_DB_NAME]['book_visit'] = array(
      'fname' => $_POST['fname'],
      'email' => $_POST['email'],
      'phone' => $_POST['phone'],
      'coupon_code' => $_POST['coupon_code'],
      'postcode' => $_POST['postcode'],
      'addr' => $_POST['addr'],
      'svSubscribe' => $_POST['svSubscribe'],
      'comments' => $_POST['comments'],
      'availibility' => $_POST['availibility'],
      'product_code' => $_GET['type']
      //, 'visit_type' => $a_settings['a_visit_types'][ $_GET['type'] ]['name'] ]
    );

        /**************This is CRM integration code*****************/
        require_once($_SERVER["DOCUMENT_ROOT"]."/crm_config.php");
        list($name, $last_name) =  explode(" ", $_POST['fname']);
        $full_name =  $name.' '.$last_name;
        $now = strftime("%Y-%m-%dT%H:%M:%S");

        $comment = "\r\n Availability: ".$_POST['availibility'];
        $comment.= "\r\n Product Code: ".$_GET['type'];
        $comment.= "\r\n Comment: ".$_POST['comments'];

        $data = array(
    "NAME" =>  $name,
    "LAST_NAME" => $last_name,
    "UF_CRM_1490175334" => $now,
    "EMAIL_HOME" => $_POST['email'],
    "PHONE_HOME" =>$_POST['phone'],
    "UF_CRM_1500629522" => 146,
    "ADDRESS" => $_POST['addr']."\r\n ".$_POST['postcode'],
    "SOURCE_DESCRIPTION" => "BT",
    "TITLE" => $full_name,
    "COMMENTS" => $comment
  );
        CrmClient::$SOURCE_ID = 'WEB_FORM';

        file_put_contents($_SERVER['DOCUMENT_ROOT'].'/log.txt', print_r($data, 1));

        if (!CrmClient::sendToCRM($data)) {
            // echo 'error send to crm';
        }
        /**************This is CRM integration code**************** */
        $visit_id = 0;
        if ($visit_id) {
            $sql = "UPDATE visit SET full_name='".mysqli_real_escape_string($dbconn,$_POST['fname'])."', email='".mysqli_real_escape_string($dbconn,$_POST['email'])."', phone='".mysqli_real_escape_string($dbconn,$_POST['phone'])."', coupon_code='".mysqli_real_escape_string($dbconn,$_POST['coupon_code'])."', postcode='".mysqli_real_escape_string($dbconn,$_POST['postcode'])."', addr='".mysqli_real_escape_string($dbconn,$_POST['addr'])."', svSubscribe='".mysqli_real_escape_string($dbconn,$_POST['svSubscribe'])."',comments='".mysqli_real_escape_string($dbconn,$_POST['comments'])."', availibility='".mysqli_real_escape_string($dbconn,$availibility)."', visit_type='".mysqli_real_escape_string($dbconn,$a_settings['a_visit_types'][ $_GET['type'] ]['name'])."', amount=".(int)$a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat'].", date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=1 WHERE visit_id=".(int)$visit_id;
            mysqli_query($dbconn,$sql);
            $newflaging = true;
        } else {
            $sql = "INSERT INTO visit SET full_name='".mysqli_real_escape_string($dbconn,$_POST['fname'])."', email='".mysqli_real_escape_string($dbconn,$_POST['email'])."', phone='".mysqli_real_escape_string($dbconn,$_POST['phone'])."', coupon_code='".mysqli_real_escape_string($dbconn,$_POST['coupon_code'])."', postcode='".mysqli_real_escape_string($dbconn,$_POST['postcode'])."', addr='".mysqli_real_escape_string($dbconn,$_POST['addr'])."', svSubscribe='".mysqli_real_escape_string($dbconn,$_POST['svSubscribe'])."', comments='".mysqli_real_escape_string($dbconn,$_POST['comments'])."', availibility='".mysqli_real_escape_string($dbconn,$availibility)."', visit_type='".mysqli_real_escape_string($dbconn,$a_settings['a_visit_types'][ $_GET['type'] ]['name'])."', amount=".(int)$a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['book_visit']['product_code'] ]['price_vat'].", date_created=UNIX_TIMESTAMP(), date_modified=UNIX_TIMESTAMP(), is_active=1, is_enabled=1";
            mysqli_query($dbconn,$sql);
            $visit_id = mysqli_insert_id($dbconn);
            $newflaging = true;
        }
        //die($sql.' '.mysql_errno().' '.mysql_error());#debug
    
        /*************************************************Add new******************************************************* /
            $sql = "SELECT * FROM visit WHERE visit_id=".(int)$visit_id;
            SQL2Array($sql, $visit);

            if (isset($visit_id)) $visit = $visit[0];
            else {
              header('Location: /getting-started/book-site-visit.html');
              exit();
            }


            //if ('POST'==$_SERVER['REQUEST_METHOD'] && $visit) {

              $Message = '';

              if ($visit['amount']==0) {
                if ($visit['coupon_code']) {
                  $Message = "Zero amount (100% discount coupon)";
                }
                else {
                  $Message = "Zero amount (FREE)";
                }
              }

              placeVisit($visit, $Message); // update log, used coupon, thank you session, send email(s)

        /****************************************************************************************************/
        $_SESSION[WISE_DB_NAME]['book_visit']['visit_id'] = $visit_id;
        if ($_GET['type']=='concept') {
            ?>
<script>
  window.location.href =
    "//www.buildteam.com/getting-started/book_visit_payment.html?vid=<?=$visit_id; ?>";

</script>
<?php
        }

        ob_start();
        //header('Location: /book-confirm.html');
    //exit();
    }
}
ob_start();
require_once('./inc/header.inc.php');
?>


<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>-->
<!--script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script-->



<style type="text/css">
  body {
    /*background:url(/images/contact/new_sv_bg.jpg) no-repeat center top !important;*/
    background-size: cover;
  }

  .top_part {
    background: rgba(255, 255, 255, 1);
  }

  .main {
    background: #fff;
    background: rgba(255, 255, 255, 0.9);
    box-sizing: border-box !important;
    padding-bottom: 50px !important;
    max-width: 1920px;
  }


  body {
    //background-image:url(../images/book-a-site-visit.jpg);
    background-attachment: fixed;
    background-size: cover;
    text-align: left !important;
  }

  footer.round-2 {
    margin-top: 0px;
  }

  .page-title {
    font-size: 30px;
    text-align: left;
    font-weight: 900;
    color: #1a3f6e;
    border-bottom: none;
    padding-bottom: 5px;
    margin-bottom: 0px;
    margin-top:10px;
  }

  p {}

  h3 {
    text-align: left;
    font-weight: bold !important;
    margin-bottom: 0px;
    padding-bottom: 8px;
    font-size:20px;
    color: #183e70;
  }
  .your-dates p {
    font-size:16px;
    font-weight:400;
    color: #183e70;
  }

  input#postcode {
    padding: 7px;
    margin-top: 10px;
    width: 210px;
    border: 2px solid #d5d5d5;
  }

  .book-a-site-table {
    margin-top: 30px;
  }

  .options-content {
    visibility: visible;
    max-width: 100% !important;
    margin-top: 20px;
    margin-bottom: 0px;
  }

  .book-a-site-table th {
    padding: 8px 0px;
    font-size: 21px;
    border-top: 2px solid #d1d1d3;
    //border-bottom: 1px solid #d1d1d3;
    color: #4b759a;
    font-weight: normal;
    text-align: inherit;
  }
  table.ui-datepicker-calendar {
    border-bottom: 1px solid #d1d1d3;
  }
  th {
    text-align: inherit;
  }


  .book-a-site-table td {
    padding: 8px 0px;
    font-size: 19px;
    color: #7d7d7d;
    font-weight: normal;
  }

  .tick {
    background-image: url(../images/tick.png);
    background-position: center;
    background-repeat: no-repeat;
  }

  .tick-star {
    background-image: url(../images/tick-star.png);
    background-position: center;
    background-repeat: no-repeat;
  }

  tr {
    border-top: 1px solid #e0e0e0;
  }

  .postcode {
    display: inline;
    padding-left: 7px;
    text-transform: uppercase;
  }

  .up_down {
    display: none;
  }

  .kaction {
    opacity: 0.5;
  }

  .book-wrap {
    border: none !important;
  }

  .book-wrap input[type=text],
  .book-wrap textarea {
    width: 100%;
    margin-top: 24px;
    border: 1px solid #7d7e84;
    margin-bottom: 10px;
    background: none;
    max-width:510px;
    height:44px;
  }

  .book-wrap .ui-widget.ui-widget-content {
    background: none !important;
  }
  

  label,
  span.red {
    font-size: 18px;
  }

  .ui-datepicker-prev {
    display: none;
  }

  .ui-datepicker .ui-datepicker-title {
    margin: 0 2.3em;
    line-height: 1.8em;
    text-align: left;
    margin-left: 0px;
    font-size: 20px;
    font-weight: bold;
    color: #183e70;
    margin-top: 6px;
    border-bottom: 1px solid #d1d1d3;
    padding-bottom: 5px;
    width: 100%;
    text-transform: uppercase;
  }

  .ui-datepicker-year {
    display: none;
  }

  .ui-datepicker th {
    color: #183e70;
    padding: 5px .6em;
    text-align: left;
    font-weight: bold;
    border: 0;
    font-size: 20px;
    border-bottom: 1px solid #d1d1d3;
    padding-bottom: 8px;
  }


  .ui-datepicker td span,
  .ui-datepicker td a {
    font-size: 20px;
    line-height: 0px;
    padding-bottom: 12px;
    height: 39px;
    padding-top: 17px;
    box-sizing: border-box !important;
    border: 2px solid transparent !important;
  }

  .ui-datepicker .ui-datepicker-next {
    right: 0px;
    top: 10px;
  }

  a.ui-state-default {
    color: #183e70 !important;
  }


  .ui-datepicker .ui-datepicker-calendar .dt_pick1 a {
    /*border: 2px solid #ed7401 !important;*/
    line-height: 0px;
    padding-bottom: 12px;
    height: 40px;
    width:40px;
    padding-top: 17px;
    background: none !important;
    border-radius: 40px;
    box-sizing: border-box !important;
    background-color: #183e70 !important;
    margin-left: 11px;
    color:#fff !important;
  }

  .ui-datepicker .ui-datepicker-calendar .dt_pick2 a {
    /*border: 2px solid #7c7c7c !important;*/
    line-height: 0px;
    padding-bottom: 12px;
    height: 40px;
    width:40px;
    padding-top: 17px;
    background: none !important;
    border-radius: 40px;
    box-sizing: border-box !important;
    background-color: #fff !important;
    margin-left: 11px;
  }

  .ui-datepicker .ui-datepicker-calendar .dt_pick3 a {
    /*border: 2px solid #324875 !important;*/
    line-height: 0px;
    padding-bottom: 12px;
    height: 40px;
    width:40px;
    padding-top: 17px;
    background: none !important;
    border-radius: 40px;
    box-sizing: border-box !important;
    background-color: #d6d6d6 !important;
    margin-left: 11px;
  }

  .ui-state-active,
  .ui-widget-content .ui-state-active,
  .ui-widget-header .ui-state-active,
  a.ui-button:active,
  .ui-button:active,
  .ui-button.ui-state-active:hover {
    background: none;
    border-none;
  }

  .ui-state-default,
  .ui-widget-content .ui-state-default {
    border: 2px solid transparent !important;
  }

  span.ui-icon.ui-icon-circle-triangle-e {
    background: url(../images/date-right-arrow.png) !important;
    font-size: 0px !important;
    height: 30px !important;
    width: 30px !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
  }

  span.date-right-arrow {
    background: url(../images/date-right-arrow.png) !important;
    font-size: 0px !important;
    height: 16px !important;
    width: 30px !important;
    background-position: center !important;
    background-repeat: no-repeat !important;
    display: inline-block;
    margin-top: 5px;
  }

  .choice-table tr,
  .choice-table td {
    vertical-align: top;
    padding: 10px 0px;
    font-weight: normal;
  }

  table.choice-table {
    margin-top: 10px;
    padding-top: 10px;
    display: block;
    height: 244px;
  }

  .ui-datepicker td.ui-state-disabled span {
    background-color: transparent;
  }


  a.book-now {
       color: #ffffff;
    font-size: 16px;
    background: #183e70;
    padding: 8px 0px 8px 0px;
    text-decoration: none;
    height: 40px !important;
    float: left !important;
    width: 212px;
    font-weight:500;
  }

  a.book-now:hover {
    background: #5d7c98;
    text-decoration: none;
  }

  #pref_dates>li {
    min-height: 35.5px;
  }

  table.ui-datepicker-calendar td {
    padding: 7px 0px;
  }

  .book-wrap .ui-widget.ui-widget-content {
    position: relative !important;
    z-index: 1 !important;
    border: none !important;
  }

  .ui-state-highlight,
  .ui-widget-content .ui-state-highlight {
    background: none !important;
    border: none !important;
  }

  .ui-datepicker .ui-datepicker-calendar .ui-state-highlight a {
    /*background: none !important;*/
  }

  #pref_dates>li {
    width: 100%;
    padding-left: 30px;
  }

  #pref_dates li i {
    display: none !important;
  }

  #pref_dates li span {
    border: none !important;
  }

  #pref_dates li span em {
    display: block;
    float: left;
    font-style: normal;
    padding: 3px;
    padding-left: 0;
    width: 90px;
    font-size:16px;
    color: #183e70;
  }

  #pref_dates li span ul {
    border-left: 0px;
  }

  #pref_dates li span ul li.date_am {
    border-top-left-radius: 0px !important;
    border-bottom-left-radius: 0px !important;
  }
  #pref_dates li span ul li.selected {
    background-color:#183e70 !important;
    color:#fff;
    border: 1px solid #183e70;
  }
  #pref_dates li span ul li {
    padding: 2px 7px !important;
    margin-top: 3px;
    margin-left: -1px;
    border: 1px solid #183e70;
  }

  #pref_dates {
    padding-left: 0px;
    position: absolute;
    top: 10px;
  }

  #pref_date_1 {}

  li#pref_date_2 {}

  #pref_dates span.removedates {
    position: absolute;
    right: 0px;
    font-size: 22px !important;
    color: #183e70;
    cursor: pointer;
  }

  #pref_dates #pref_date_1,
  #pref_dates #pref_date_2 {
    border-bottom: 1px solid #dedede;
  }

  #pref_dates .fordynamic:nth-child(1) {
    margin-top: 0px;
    margin-bottom: 0px !important;

    padding-bottom: 10px;
  }

  #pref_dates .fordynamic:nth-child(2) {
    margin-top: 10px;
    margin-bottom: 0px !important;
    padding-bottom: 6px;
    height: 39px;

  }

  #pref_dates .fordynamic:nth-child(3) {
    margin-top: 10px;
    margin-bottom: 0px !important;
  }

  li#pref_date_3 {}

  .choice-table tr,
  .choice-table td {
    border: none !important;
  }



  .book-wrap a.book-now {
    margin-top: -58px;
    margin-left: 0px;
  }


  .book-wrap .ui-widget.ui-widget-content {

    border-bottom: 1px solid #d1d1d3 !important;

  }
  .new_width {
      width: 34%;
    }

  .error p {
    padding-top: 0px !important;
    
  }

  .error {
    margin-top: 0px !important;
  }

  .enquirywrap label p {
    padding-top: 10px;
  }

  #popupform .carousel-control span {
    text-shadow: none !important;
  }

  button.ui-dialog-titlebar-close {
    background: url(/images/close.jpg) no-repeat center !important;
    height: 20px !important;
    top: 16px !important;
    background-size: contain !important;
    border: none;
    width: 20px !important;
    z-index: 2;
  }

  .promptimage {
    position: absolute;
    top: 22%;
    right: -1502px;
    display: none;

  }

  .comefromleft {
    right: -152px !important;
    display: block !important;

    animation: newani 0.3s;
  }

  .closethis {
    position: absolute;
    right: 0px;
    top: 10px;
  }

  .closethis a {
    background: none;
    color: #104674;
    font-size: 36px;
    font-weight: 500;
    line-height: 18px;
  }

  .closethis a:hover {
    text-decoration: none;
  }

  .broform100 input[type=checkbox] {
    width: auto;
    height: auto;
    vertical-align: middle;
    width: 20px;
    height: 20px;
    margin-right: 5px;
    padding: 5px;
  }

  .broform100 {
    margin-top:50px;
  }
  .broform100 span a {
    color:#1a3f6e;
  }

  .broform100 input[type=checkbox] {
    width: auto;
    height: auto;
    vertical-align: middle;
    width: 30px;
    height: 30px;
    margin-right: 30px;
    padding: 5px;
  }
  .contact-page form textarea, .book-wrap textarea {
    color: #2e4875 !important;
  }
  .checkboxbro span {
    font-size: 16px;
    display: inline-block;
    width: 81%;
    vertical-align: top;
    color: #000;
  }
  .our-site-title {
    font-size: 30px !important;
  }
  .mobile-no {
      display:none;
    }

  @-webkit-keyframes newani {
    0% {
      right: -165px;
    }

    100% {
      right: -152px;
    }
  }

  .promptimage img {}

  #loadingif img {
    max-width: 80px;

  }

  #loadingif {
    position: absolute;
    bottom: 25px;
    left: 150px;
    display: none;
  }

  @media only screen and (min-width: 480px) {

    .timecont .pertime,
    .daycont .pertiday {
      width: 46px !important;
    }

    .popformfields:nth-child(3) {
      margin-top: 10px !important;
      margin-bottom: 10px !important;

    }

    .popformfields {
      margin-bottom: 5px !important;
    }

    #tabelwidth {
      width: 65%;
    }
  }

  @media only screen and (max-width: 480px) {

    .main {
      max-width: 480px;
      width: 100%;
    }
    .ui-widget.ui-widget-content {
    display:contents !important;
  }
  #dialog-request {
    display:none !important;
  }
  .ui-draggable .ui-dialog-titlebar {
    display:none !important;
  }
    .full p {
      margin: 0 0 10px;
      font-size: 12px;
      line-height: inherit !important;
    }
    .mobile-no {
      display:none;
    }
    .full h3 {
      font-size: 17px;
    }
    .ui-datepicker .ui-datepicker-title {

    }
    .page-title {
      font-size: 20px;
      text-align: left;
      font-weight: bold;
      color: #1a3f6e;
      border-bottom: none;
      padding-bottom: 5px;
      margin-bottom: 0px;
      margin-top: 0px;
    }
    .book-wrap label {
      font-size: 15px;
      margin-top: 30px;
    }
    .book-wrap input[type="text"], .book-wrap textarea {
      margin-top: 15px;
    }
    .book-wrap .ui-widget.ui-widget-content {
      position: relative !important;
      z-index: 1 !important;
      border: none !important;
      padding-left: 0px !important;
      margin-left: 0px;

    }



    .ui-datepicker-calendar td {
      padding: 0px !important;
    }

    .ui-datepicker-calendar td span,
    .ui-datepicker-calendar td a,
    .ui-datepicker-calendar th span,
    .ui-datepicker-calendar th a {
      font-size: 13px;
      padding: 2px 0px !important;
      line-height: 20px !important;
    }
    .ui-datepicker .ui-datepicker-calendar .dt_pick1 a {
      height: 40px !important;
width: 40px;
padding-top: 8px !important;
    }
    .ui-datepicker .ui-datepicker-calendar .dt_pick2 a {
      height: 40px;
width: 40px;
padding-top: 8px !important;
    }
    .ui-datepicker .ui-datepicker-calendar .dt_pick3 a {
      height: 40px;
width: 40px;
padding-top: 8px !important;
    }
    .ui-widget.ui-widget-content {
      max-width: 100% !important;
    }
    .ui-datepicker .ui-datepicker-title {
      font-size:13px;
    }
    h3 {
      font-size:13px;
    }
    .ui-datepicker th {
      font-size: 10px !important;
    }
    .date-choice {
      font-size:13px;
      line-height: 29px;
    }
    .new_width {
      width: 150px;
    }
    .ui-datepicker .ui-datepicker-prev span, .ui-datepicker .ui-datepicker-next span {
      top: 20%;
    }
    .col-xs-8 {
    width: 70%;
    margin: 0px 10% 30px 14%;
    }
    .col-xs-8 .event-button {
      margin: 30px 5% 0px 0% !important;
    }
    .home-gallery-p {
      width: 100% !important;
    }
    #pref_dates > li {
      min-height: 40.5px;
    }
    #pref_dates li span em {
      font-size:12px;
      margin-top: 3px;
    }
    #pref_dates>li {
      padding-left: 0px !important;
    }

    form#fm_book_visit {
      display: -webkit-box;
      display: -moz-box;
      display: -ms-flexbox;
      display: -webkit-flex;
      display: flex;
      -webkit-box-orient: vertical;
      -moz-box-orient: vertical;
      -webkit-flex-direction: column;
      -ms-flex-direction: column;
      flex-direction: column;
      -webkit-box-align: start;
      -moz-box-align: start;
      -ms-flex-align: start;
      -webkit-align-items: flex-start;
      align-items: flex-start;
    }

    .col-sm-6.your-dates {
      -webkit-box-ordinal-group: 2;
      -moz-box-ordinal-group: 2;
      -ms-flex-order: 2;
      -webkit-order: 2;
      order: 2;
    }

    .col-sm-6.clalendargetup {
      -webkit-box-ordinal-group: 1;
      -moz-box-ordinal-group: 1;
      -ms-flex-order: 1;
      -webkit-order: 1;
      order: 1;
    }

    #tabelwidth {
      width: 60%;
    }

    .bookthanksinner {
      height: 200px !important;
      padding: 20px 10px !important;
      width: 96% !important;
    }

    .bookthanksinner h4 {
      font-size: 20px !important;
    }

  }

  .mmt_normal_text {
    font-weight: normal;
    font-size: 24px;
  }
  .cbp-hrmenu {
    margin: 3.7em 0 0;
  }
  .main {
    max-width: 1920px !important;
  }
  .ui-widget.ui-widget-content {
    z-index: 1002;
  }
  .open-hrs-p {
    width:81%;
  }
  div.error {
    color:#ff0000;
    font-size:20px;
    font-weight:normal;
    margin-left:16px;
  }
  .ui-dialog {
    z-index:9999999 !important;
    background:#fff !important;
  }
</style>



<section class="open-hrs-header">
	<div class="book-header-bg"></div>
	<h1 class="page_house_tours_new">Design Consultations</h1>
	<p class="open-hrs-p">Get started today by booking a Design Consultation over the phone with one of our
Architectural Designers to discuss your project and find out more.</p>
</section>


<div style="width:100%;max-width:1920px;background-color:#E6E6E7;">
<section id="intdesbg">

  <div class="col-sm-12">
    <h2 class="page-title"><?php if ($_GET['type']=='standard') { ?> <?php   } else { ?> <?php echo $a_settings['a_visit_types'][ $_GET['type'] ]['name'];  ?> 
    Visit <?php } ?> <span class="mmt_normal_text"> <?php if ($_GET['type']!='standard') { ?>
        (£<?php echo $a_settings['a_visit_types'][ $_GET['type'] ]['price']; ?>
        + VAT)<?php } ?></span>
    </h2>
  </div>


  <div class="book-wrap">
    <form method="POST" id="fm_book_visit" name="fm_book_visit" onsubmit="$('#loadingif').show();">
    <?php if ($error) {
    echo '<div class="error"><p>'.nl2br(htmlspecialchars($error, ENT_QUOTES, 'UTF-8')).'</p></div>';
} ?>
      <input type="hidden" name="nonce"
        value="<?php echo session_id() ?>" />
      <div class="col-sm-12">

       

      </div>
      <div class="col-xs-12 col-sm-6">
        <label>Full Name<span class="red">*</span>:</label>
        <input type="text" name="fname" value="<?php if (isset($_POST['fname'])) {
    echo htmlspecialchars($_POST['fname'], ENT_QUOTES, 'UTF-8');
} ?>" />
      </div>
      <div class="col-xs-12 col-sm-6">
        <label>Contact Number<span class="red">*</span>:</label>
        <input type="text" name="phone" value="<?php if (isset($_POST['phone'])) {
    echo htmlspecialchars($_POST['phone'], ENT_QUOTES, 'UTF-8');
} ?>" />
        <?php
            $hithis = $a_settings['a_visit_types'][ $_GET['type'] ]['name'];
            //echo $hithis;
            if ($hithis == "Premium") {
                ?>
        <!-- <label>Coupon code</label>
              <input type='text' name='coupon_code' value='<?php if (isset($_POST['coupon_code'])) {
                    echo htmlspecialchars($_POST['coupon_code'], ENT_QUOTES, 'UTF-8');
                } ?>'
        />-->
        <?php
            } ?>




      </div>
      <div class="col-xs-12 col-sm-6">


        <label>House / Flat No. &amp; Street<span class="red">*</span>:</label>
        <input type="text" name="addr" value="<?php if (isset($_POST['addr'])) {
                echo htmlspecialchars($_POST['addr'], ENT_QUOTES, 'UTF-8');
            } ?>" />
      </div>
      <div class="col-xs-12 col-sm-6">
        <label>Postcode<span class="red">*</span>:</label>
        <input id="book-postcode" type="text" name="postcode" value="<?php if (isset($_POST['postcode'])) {
                echo htmlspecialchars($_POST['postcode'], ENT_QUOTES, 'UTF-8');
            } ?>" />

</div>
<div class="col-xs-12 col-sm-6">
        <label>Message:</label>
        <textarea name="comments" style="height:173px !important;"><?php if (isset($_POST['comments'])) {
                echo htmlspecialchars($_POST['comments'], ENT_QUOTES, 'UTF-8');
            } ?></textarea>
      </div>
      <div class="col-xs-12 col-sm-6">
        <label>Email Address<span class="red">*</span>:</label>
        <input type="text" name="email" value="<?php if (isset($_POST['email'])) {
    echo htmlspecialchars($_POST['email'], ENT_QUOTES, 'UTF-8');
} ?>" />
      <div class="broform100 checkboxbro broleft">
          <input type="checkbox" name="svSubscribe" value="Yes"><span>Sign up to our newsletter and receive updates on
            our latest blogs, news items and special offers – see our <a
              href="/privacy-policy.html">privacy policy</a> for more information on how we use
            your personal data.</span>
        </div>
      </div>

      
      
      
      
      
      <div class="col-xs-12 col-sm-6">
        
      </div>

      </section>
      </div>
      <div style="width:100%;max-width:1920px;background-color:#f2f2f2;">
      <section id="intdesbg" style="padding: 0px 0 20px 0;">
      <div class="home-gallery">
  <div class="home-gallery-text mobile-no">
	<div class="home-gallery-h2" style="width: 39%;n">
  		<h2>Design<br>
		  Consultations.</h2>
	</div>
<div class="home-gallery-p">
  <p style="margin-top: 62px;">Get started today by booking a quick consultation over the phone,
with one of our Architectural Designers.</p>
</div>
</div>
      <div class="col-sm-6">
        <div id="avail_cal"></div>
        
      </div>
      <div class="col-sm-6">
        <h3>Your Dates</h3>
        <p>(In order of preference)<br>To set or remove a date, just click on a calendar day</p>
        <input type="hidden" name="availibility" value="<?php if (isset($_POST['availibility'])) {
                echo htmlspecialchars($_POST['availibility'], ENT_QUOTES, 'UTF-8');
            } ?>" />
        <table class="choice-table">
          <tr style="border-bottom: 1px solid #dedede !important;">
            <td class="new_width">
              <div class="date-choice"><div style="width:20px;height:20px;border-radius:20px;background-color:#183e70;float:left;margin-top: 5px;margin-right: 5px;"></div>1st Choice</div>
            </td>
            <td id="tabelwidth" rowspan="3" style="position:relative">
              <ol id="pref_dates">
                <li id="pref_date_1" style="height:40px;"></li>
                <li id="pref_date_2" style="height:40px;"></li>
                <li id="pref_date_3" style="height:36px;"></li>
              </ol>
            </td>
          </tr>
          <tr style="border-bottom: 1px solid #dedede !important;">
            <td>
            <div class="date-choice"><div style="width:20px;height:20px;border-radius:20px;background-color:#fff;float:left;margin-top: 5px;margin-right: 5px;"></div>2nd Choice</div>
          </td>
          </tr>
          <tr style="border-bottom: 1px solid #dedede !important;">
            <td>
            <div class="date-choice"><div style="width:20px;height:20px;border-radius:20px;background-color:#d6d6d6;float:left;margin-top: 5px;margin-right: 5px;"></div>3rd Choice</div>
            </td>
          </tr>
          <!--tr>
            <td colspan="3">
              <font color="#4b759a" size="4" class="request-call">Request a call back</font>
            </td>
          </tr-->
        </table>



        <a href="#" class="book-now">Book now </a>
        <span id="loadingif"><img src="/images/new_sv/loading.gif" alt=""></span>
      </div>

      





    </form>



    </div>


  </div>
</div>

<div class="clear"></div>
<div class="outsidepoup">
  <div class="outsideinner">
    <div class="lightbox_content">
      <h4>Thank you for considering Build Team</h4>
      <p>You are outside of our standard coverage area, please contact us on 0207 495 6561 or e-mail hello@buildteam.com
        to arrange your site visit.</p>
      <a href="javascript:void(0);">Ok</a>
    </div>
  </div>
</div>

<div class="bookthanks">
  <div class="bookthanksinner">
    <div class="lightbox_content">
      <div class="closethis">
        <a href="/getting-started/book-site-visit.html">x</a>
      </div>
      <h4>Thank you for your booking</h4>
      <p>You should receive a confirmation email within 24 hours. While you wait why not check out our gallery.</p>
      <a href="/project-gallery/gallery.html">Ok</a>
    </div>
  </div>
</div>
</section>
        </div>
        <?php
        require_once('explore-our-site.php');
    ?>
<script>
        $(function(){
							
							$(".ui-datepicker-calendar tr").each(function(index, element) {
								$($(this).children('td')).each(function(index, element) {
                                    if (!$(this).hasClass('ui-state-disabled')) {
										var getchildern = "0" + $(this).children('a').text();
										console.log(getchildern);
										if ($.cookie('fromdate') == getchildern) {
											
											$(this).addClass('dt_pick1 ui-state-highlight');
											}
										else if ($.cookie('gettodate') == getchildern)	{
											$(this).addClass('dt_pick2 ui-state-highlight');
											} 
									}
									
                                });
                            });
							
							});
        </script>
<?php
            $hithis = $a_settings['a_visit_types'][ $_GET['type'] ]['name'];
            //echo $hithis;
            if ($hithis == "Premium") {
                ?>
<!-- <script>
 	$("#fm_book_visit").submit(function(){
		
		var getcoupon = $("input[name=coupon_code]").val();
		if (getcoupon !== "") {
		if (getcoupon.substr(0,3) !== "FP_") {
			alert("Enter correct coupon code");
			return false;
			}
			
		else {
			
		}
		}
		});
 </script>       -->
<?php
            } ?>
<!--script src="/js/jquery-ui.js"></script-->
<script src="/js/jquery-ui.multidatespicker.js?v=1"></script>
<script src="/js/book-visit.js?v=1"></script>
<script type="text/javascript">
    jQuery(document).ready(DesignTeam.initBookCal);
	
	
	
</script>
<script>
  $(document).ready(function(e) {
    $(".choice-table").hover(function(e) {
      $(".promptimage").addClass("comefromleft");
    });
  });

</script>

<script>
  $(".selected").click(function() {

  });

</script>
<?php

require_once('./inc/footer.inc.php');
