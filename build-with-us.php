<?php require_once('./inc/header.inc.php'); ?>

<?php require_once('save_how_can_help_form.php'); ?>
<link rel="stylesheet" href="css/quiz.css" type="text/css">
<link rel="stylesheet" href="js/accordian/jquery.accordion.css" type="text/css">
<link rel="stylesheet" href="js/slidertab/jquery.sliderTabs.css" type="text/css">
<style>
	.titlehowwecan h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
}	
.howwedesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;	
    color: #74787f;
    line-height: 24px;
    font-weight: normal;
}
.buildwithbuildsec {
    
    padding: 20px 0px 30px 0px;
}
.innerbuildwith {
    max-width: 980px;
    margin: 0 auto;
}
.leftbuildwith {
    float: left;
	width:50%;
	
}
.rightbuildwith {
    float: right;
	width:50%;
	background: #f7f7f7;
}
.buildsecdetails img {
    width: 100%;
}
.buildsecdetails {
	text-align:center;
	}
.buildsecdetails p {
    font-size: 22px;
    color: #afafaf;
    line-height: 25px;
    padding-top: 0px;
}
.buildsecdetails h2 {
    color: #124b7b;
    font-size: 24px;
    font-weight: bold;
    padding-bottom: 10px;
}
.buildsecdetails a {
    display: inline-block;
    margin-top: 20px;
    font-size: 20px;
    /* font-weight: normal; */
    width: 200px;
    border-radius: 10px;
    color: #f5641e;
    font-style: italic;
}	

.rightbuildwith .buildsecdetails {
    padding:66px 0px;
}
.leftbuildwith .buildsecdetails {

}
.videobottom h2 {
    color: #0e4677;
    font-size: 22px;
    font-weight: bold;
    margin-top: 20px;
}
.videobottom p {
    font-size: 18px;
    line-height: 20px;
    color: #b4b4b4;
    font-weight: normal;
}
.wrapinside {
	text-align:center;
	}
.howcanwetitle h1 {
    font-size: 22px !important;
    padding-bottom: 0px !important;
    margin-bottom: 5px !important;
    letter-spacing: 1px;
}
.howcanwesub p {
    font-weight: bold;
    color: #b4b4b4;
    font-size: 18px;
    padding-top: 10px;
}
.questionslist {
	background:#f7f7f7;
	}
.testinline {
    display: inline-block;
	vertical-align:top;
}
.qustiontext.testinline {
    width: 91%;
	margin-left:10px;
}
.questioarrow.testinline {
    width: 15px;
    margin: 0px 10px;
	display:none;
}
.questionumber {
    width: 30px;
    position: relative;
    top: -5px;
}
.quationanswer p {
    font-size: 16px;
    color: #b4b4b4;
    padding-top: 10px;
}
.questionfinal h2 {
    color: #0e4677;
    font-size: 18px;
	    cursor: pointer;
}
.quesstionsinner {
    max-width: 980px;
    margin: 0 auto;
}
.quationanswer {
    height:auto;
	
}
.questionclass.active .qustiontext .quationanswer {
	max-height:200px;
	
	}
.questionclass.active .innerimagetest {
    transform: rotate(180deg);
	transition:0.15s ease-in-out;
}
.innerimagetest {
	transform: rotate(90deg);
	transition:0.15s ease-in-out;
	}	
.questionumber.testinline p {
    background: #fff;
    height: 25px;
    width: 25px;
    padding: 0px;
    line-height: 25px;
    text-align: center;
    border-radius: 100px;
    border: 1px solid #0e4677;
    color: #0e4677;
    font-size: 16px;
    font-weight: normal;
	z-index:2;
	position:relative;
}
.innerimagetest img {
    width: 20px;
}
.questionclass {
	margin-bottom:40px;
	}
.lastnomargin {
	margin-bottom:20px;
	}	
.questionslist {
    background: #f2f5f8;
    padding: 40px 0px 30px 0px;
}	

#video {
	display:none;
	height:275px !important;
	}



.howcanwedesc p {
    font-size: 16px;
    padding-top: 10px;
    color: #b4b4b4;
	line-height:18px;
}
.howcanwebox h1 {
    font-size: 16px !important;
    font-weight: normal;
    margin-bottom: 0px !important;
    padding-top:0px !important;
    padding-bottom: 0px !important;
	color:#86a2bb;
}
.howcanwebox p {
    padding-top: 0px;
    margin-top: 5px;
    color: #b4b4b4;
	font-size:16px;
}
.howcanwebutton {
    padding: 15px 0px;
}
.howcanwebutton a {
    background: #0a4f7c;
    color: #FFF;
    display: inline-flex;
    padding: 10px 50px;
    border-radius: 10px;
    text-align: center;
    margin: 0 auto;
    font-size: 16px;
}
.howcanwebutton {
    padding: 25px 0px;
}
.howcanwebox {
    padding: 25px 0px;
    border-bottom: 1px dashed #b4b4b4;
    border-left: 1px solid #b4b4b4;
}
.howcanwetop {
	border-left:1px solid #b4b4b4;
	border-bottom:1px solid #b4b4b4;
	padding-bottom:15px;
	min-height: 290px;
	padding-top:15px;
	}
.howcanwebutton {
    padding: 25px 0px;
    border-left: 1px solid #b4b4b4;
}	
.innerhowcanwe {
	margin-top:50px;
	}
.inlinehowcanwe {
	float:left;
	width:33.33%;
	}
.lasthowcan .howcanwetop{
	border-right: 1px solid #b4b4b4;
	}
.lasthowcan .howcanwebox , .lasthowcan .howcanwebutton{
    border-right: 1px solid #b4b4b4;
}
.howcanwebutton a strong {
    margin-left: 5px;
}	
.helpmechoose a {
    background: #f2f5f8;
    padding: 15px 31px;
    font-size: 20px;
    color: #0a4f7c;
    letter-spacing: 1px;
}
.helpmechoose a span {
    font-weight: bold;
}
.helpmechoose {
    position: absolute;
    left: -83px;
    transform: rotate(-90deg);
}
.dottedline {
    position: absolute;
    height: 99%;
    width: 3px;
    background: url(images/build_phase/Dotted-line.png) repeat-y;
    background-size: contain;
    top: 0px;
    left: 12px;
    z-index: 1;
}
.questioninner {
    position: relative;
}
.stepstitle h2 {
	    color: #0e4677;
    font-size: 22px;
    font-weight: bold;
    margin-bottom: 20px;
	}
.up_down {
	display:none;
	}
.col_two_fifth.col_last {
	height:0px;
	display:none;
	}
footer.round-2 {
	margin:0% auto 0;
	}
.helpmechoose {
    position: absolute;
    left: -83px;
    top: 200px;
    transform: rotate(-90deg);
}	
@media (max-width:768px) {
	.leftbuildwith, .rightbuildwith {
		width:100%;
		float:none;
		}
	.rightbuildwith .buildsecdetails {
    padding: 45px 0px;
}
.qustiontext.testinline {
    width: 73%;
}
.questionslist {
    background: #f2f5f8;
    padding: 40px 15px 5px 20px;
}	
		
	}					
</style>
<div class="howwecanhelp">
    	<div class="titlehowwecan">
        	<h2>Build With Build Team</h2>
        </div>
        <div class="howwedesc">
        	 <p>We can help in a variety of ways when it comes to your Build. If you want to Build with our construction arm, here is a breakdown of what to expect:</p>
        	<!--<p>We can help in a variety of ways when it comes to the Build Phase, and we’re here to help you choose the avenue best for you, your budget, and your build. There are loads of questions to ask yourself when it comes to appointing a contractor. You might now be wondering : what questions should I be asking? Well, we’re glad you asked! Complete our quiz to find out.</p>-->
        </div>
    </div>
<div class="buildwithbuildsec">
	<div class="innerbuildwith">
    	<div class="leftbuildwith">
        	<div class="buildsecdetails2">
            	<a href="javascript:void(0)"><img src="images/build_phase/video_layer.jpg" alt=""></a>
                <video id="video" width="320" height="240" controls>
                  <source src="/images/build_phase/Build_Team_Hardcoded.mp4" type="video/mp4">
                	Your browser does not support the video tag.
                </video>

                <!--<div id="video" style="padding:56.25% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/260411006" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>-->
               
            </div>
        </div>
        
        <div class="rightbuildwith">
        	<div class="buildsecdetails">
                <h2>See For Yourself</h2>
                <p>
                   Our Lead Designer, David, talks <br>
				   you through an ongoing build.
				</p>
                <a href="https://www.buildteam.com/build_with_buildteam.html">Click here to watch our 8 part series</a>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="videobottom">
    </div>
</div>
</div>
<div class="questionslist">
	<div class="quesstionsinner">
    	<div class="stepstitle"><h2>Steps</h2></div>
    	<div class="questioninner">
        	
        	<div class="questionclass">
            	<div class="questionumber testinline"><p>1</p></div>
                <div class="questioarrow testinline">
                	<div class="innerimagetest">
                    	<img src="images/build_phase/arrow.png" alt="">
                    </div>
                </div>
                <div class="qustiontext testinline">
                	<div class="questionfinal">
                    	<h2>Site Meeting</h2>
                    </div>
                    <div class="quationanswer">
                    	<p>Upon your instruction of the Build Phase, we will arrange a meeting with you at the property with your Project Manager (PM) and Foreman. This enables us to confirm all of the details, answer any remaining questions and book a start date for the works to commence on site.</p>
                    </div>
                </div>
            </div>
            <div class="questionclass">
            	<div class="questionumber testinline"><p>2</p></div>
                <div class="questioarrow testinline">
                	<div class="innerimagetest">
                    	<img src="images/build_phase/arrow.png" alt="">
                    </div>
                </div>
                <div class="qustiontext testinline">
                	<div class="questionfinal">
                    	<h2>Preparation Activities</h2>
                    </div>
                    <div class="quationanswer">
                    	<p>Before the build starts, we will have to complete a checklist of activities. This varies, but may include assigning an Approved Inspector, arranging the necessary documentation and suspending a parking bay for a skip.</p>
                    </div>
                </div>
            </div>
            <div class="questionclass">
            	<div class="questionumber testinline"><p>3</p></div>
                <div class="questioarrow testinline">
                	<div class="innerimagetest">
                    	<img src="images/build_phase/arrow.png" alt="">
                    </div>
                </div>
                <div class="qustiontext testinline">
                	<div class="questionfinal">
                    	<h2>Start Date</h2>
                    </div>
                    <div class="quationanswer">
                    	<p>The foreman will arrange for protection to be laid, the strip out of any unwanted fixtures and fittings – before starting work to dig the foundations and any other excavation works that may be required. </p> 
                    </div>
                </div>
            </div>
            <div class="questionclass">
            	<div class="questionumber testinline"><p>4</p></div>
                <div class="questioarrow testinline">
                	<div class="innerimagetest">
                    	<img src="images/build_phase/arrow.png" alt="">
                    </div>
                </div>
                <div class="qustiontext testinline">
                	<div class="questionfinal">
                    	<h2>On-site Construction</h2>
                    </div>
                    <div class="quationanswer">
                    	<p>An extension usually takes between 12 to 16 weeks. During this time your PM will give you regular updates about the works and you can be involved as little or as much as you like.</p>
                    </div>
                </div>
            </div>
            <div class="questionclass">
            	<div class="questionumber testinline"><p>5</p></div>
                <div class="questioarrow testinline">
                	<div class="innerimagetest">
                    	<img src="images/build_phase/arrow.png" alt="">
                    </div>
                </div>
                <div class="qustiontext testinline">
                	<div class="questionfinal">
                    	<h2>Electrics & Plumbing</h2>
                    </div>
                    <div class="quationanswer">
                    	<p>The internal finish of the structure is carried out including 1st & 2nd fix electrics and 1st fix plumbing. We also begin plaster boarding and plastering.</p>
                    </div>
                </div>
            </div>
            <div class="questionclass">
            	<div class="questionumber testinline"><p>6</p></div>
                <div class="questioarrow testinline">
                	<div class="innerimagetest">
                    	<img src="images/build_phase/arrow.png" alt="">
                    </div>
                </div>
                <div class="qustiontext testinline">
                	<div class="questionfinal">
                    	<h2>Snagging</h2>
                    </div>
                    <div class="quationanswer">
                    	<p>When the agreed works have been carried out the project has reached practical completion. You will give your Foreman a snagging list and after these are carried out we tidy up the site and remove any waste and tools.</p>
                    </div>
                </div>
            </div>
            <div class="questionclass lastnomargin">
            	<div class="questionumber testinline"><p>7</p></div>
                <div class="questioarrow testinline">
                	<div class="innerimagetest">
                    	<img src="images/build_phase/arrow.png" alt="">
                    </div>
                </div>
                <div class="qustiontext testinline">
                	<div class="questionfinal">
                    	<h2>Completion Pack</h2>
                    </div>
                    <div class="quationanswer">
                    	<p>The project is handed over together with a folder containing our 10 Year Structural Guarantee for the works that have been carried out and other important documentation regarding your project.</p>
                    </div>
                </div>
            </div>
            <div class="dottedline"></div>
        </div>
    </div>    
</div>
<div class="helpmechoose">
	<a href="#"><span>HELP ME</span> CHOOSE</a>
</div>  
	<div class="quizwrap">
	<div class="quizinner">
    	<div class="quizesection">
        	<form id="example-advanced-form" action="#">
                <h3></h3>
                <fieldset>
                	<div class="innerfeild">
                    	<div class="fieldtablecell">
                        	 <legend>Have you got planning permission and detailed drawings?</legend>
                                <p>Please pick one to continue</p>
                                <label>
                                <input type="radio" id="opt1" name="planningpermission" value="Yes">
                                <span>Yes</span>
                                </label>
                                <label>
                                <input type="radio" id="opt2" name="planningpermission" value="No">
                                <span>No</span>
                                </label>
                        </div>
                    </div>
                   
                </fieldset>
                
                 <h3></h3>
                <fieldset>
                	<div class="innerfeild">
                    	<div class="fieldtablecell">
                        	 <legend>Who did you use for your drawings? </legend>
                                
                                <label>
                                <input type="radio" name="fordrawing" value="Design Team (Build Team's sister Company)">
                                <span>Design Team (Build Team's sister Company)</span>
                                </label>
                                <label>
                                <input type="radio" name="fordrawing" value="An Architect">
                                <span>An independant Architect</span>
                                </label>
                                <label>
                                <input type="radio" name="fordrawing" value="Other">
                                <span>Other</span>
                                </label>
                        </div>
                    </div>
                   
                </fieldset>
             
                             
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                    <legend>Have you completed a home extension project before?</legend>
                    <label>
                    <input type="radio" name="experience" value="No, this is my first time">
                    <span>No, this is my first time</span>
                    </label>
                    <label>
                    <input type="radio" name="experience" value="Yes, but I wasn't heavily involved in the process">
                    <span>Yes, but I wasn't heavily involved in the process</span>
                    </label>
                    <label>
                    <input type="radio" name="experience" value="IYes, I was very involved in the process and know what to expect">
                    <span>Yes, I was very involved in the process and know what to expect</span>
                    </label>
                   
                    </div>
                    </div>
                </fieldset>
                
                 <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                    <legend>Do you want adequate insurance during the build?</legend>
                    <label>
                    <input type="radio" name="insurance" value="No">
                    <span>No</span>
                    </label>
                    <label>
                    <input type="radio" id="someof1" name="insurance" value="If it keeps cost down, I don't need it">
                    <span>If it keeps cost down, I don't need it</span>
                    </label>
                    <label>
                    <input type="radio" id="yesinsurance2" name="insurance" value="Yes">
                    <span>Yes</span>
                    </label>
                    
                    </div>
                    </div>
                </fieldset>
             
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>Where do you intend to live during the build?</legend>										  					<label>
                     <input id="1no" type="radio" name="live" value="In the property">
                     <span>In the property</span>
                    </label>
                    
                   <label>
                    <input id="2no" type="radio" name="live" value="I'll move out for the worst of it">
                    <span>I'll move out for the worst of it</span>
                   </label>
                   <label> 
                    <input id="3yes" type="radio" name="live" value="I'll move out for the entire build">
                    <span>I'll move out for the entire build</span>
                   </label>
                   </div>
                   </div> 
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>What level of involvement would you like during the Build?</legend>										  					
                     <label>
                     <input type="radio" name="involvement" id="involvment1" value="None at all, I'm very busy and just want to be updated of progress">
                     <span>None at all, I'm very busy and just want to be updated of progress</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment2" value="Some involvement, I'd like to visit site once a week to see how it's going">
                    <span>Some involvement, I'd like to visit site once a week to see how it's going</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment3" value="Lots of involvement, I have time to manage the project day to day">
                    <span>Lots of involvement, I have time to manage the project day to day</span>
                    </label>
                    <label>
                    <input type="radio" name="involvement" id="involvment4" value="I live abroad, so I won't be around at all">
                    <span>I live abroad, so I won't be around at all</span>
                    </label>
                    </div>
                    </div>
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>What is your budget?</legend>										  					
                     <label>
                     <input type="radio" name="budget" id="budget1" value="Less than £30,000">
                     <span>Less than £30,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget2" value="Between £30,000 and £50,000">
                    <span>Between £30,000 and £50,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget3" value="Between £50,000 and £100,000">
                    <span>Between £50,000 and £100,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget4" value="Between £100,000 and £150,000">
                    <span>Between £100,000 and £150,000</span>
                    </label>
                    <label>
                    <input type="radio" name="budget" id="budget5" value="Over £100,000">
                    <span>Over £150,000</span>
                   </label>
                   </div>
                   </div>
                </fieldset>
                
                <h3></h3>
                <fieldset>
                <div class="innerfeild">
                    	<div class="fieldtablecell">
                     <legend>Our Recommendation</legend>										  					
                     
                     <div class="ourrecomendationdiv selectedreco">
                     	<div class="leftreco">
                        	<p id="reccomendation"></p>
                        </div>
                        <div class="descreco">
                        	<p>Sit back and relax! We will take care of everything. You’ll be assigned one dedicated Project Manager who will keep you updated throughout the process and ensure things are running smoothly. They’ll be onsite most days to ensure things are pushing along nicely. We have public liability insurance and a 10 year guarantee, so you have absolute reassurance both during and after the build is complete.</p>
                        	<!--<div class="detailereco">
                            	<h2>• 10 Year Guarantee</h2>
                                <p>we offer a 10 year guarantee</p>
                            </div>
                            <div class="detailereco">
                            	<h2>• Project Management</h2>
                                <p>we handle all of the hassle</p>
                            </div>
                            <div class="detailereco">
                            	<h2>• Public Liability Insurance</h2>
                                <p>giving you peace of mind</p>
                            </div>-->
                        </div>
                        <div class="rightreco">
                        	<a href="#">FIND OUT <span>MORE</span></a>
                        </div>
                        
                     </div>
                    <div class="otherbundles">
                    	<p>Our other bundles:</p>
                    </div> 
                     <div class="ourrecomendationdiv otherbudles">
                     	<div class="leftreco">
                        	<div class="inlineone"><p>• <span id="otherbudle2">Do it with our help</span> </p>
                            	<div id="otherllink2" class="linkright">
                                	<a href="#">FIND OUT <span>MORE</span></a>
                                 </div>
                            </div>
                        </div>
                     </div>
                     <div class="ourrecomendationdiv otherbudles">
                     	<div class="leftreco">
                        	<div class="leftreco">
                        	<div class="inlineone">
                            <p>• <span id="otherbudle3">Build with us</span></p>
                            	<div id="otherllink3" class="linkright">
                                	<a href="#">FIND OUT <span>MORE</span></a>
                                </div>
                            </div>
                            </div>
                            
                        </div>
                     </div>
                     <!--div class="recoprefer">
                     	<p>Prefer to speak with a member of the team?</p>
                        <a href="#" class="request-call">REQUEST A CALL BACK</a>
                     </div-->
                     </div>
                     </div>
                </fieldset>
                
            </form>
        </div>
        <div class="closebuttonquiz"><i>CLOSE AND RETURN TO THE SITE</i> <span>x</span></div>
        <div class="backtostepone"><img src="/images/build_phase/Back-Button.png" alt=""><i>TAKE ME BACK TO THE BEGINNING</i></div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>
<script src="js/jquery.steps.min.js"></script>
<script>
	var form = $("#example-advanced-form").show();
 
form.steps({
    headerTag: "h3",
    bodyTag: "fieldset",
    transitionEffect: "slideLeft",
	transitionEffectSpeed:30,
    onStepChanging: function (event, currentIndex, newIndex)
    {
        // Allways allow previous action even if the current form is not valid!
        if (currentIndex > newIndex)
        {
            return true;
        }
        // Forbid next action on "Warning" step if the user is to young
        if (newIndex === 3 && Number($("#age-2").val()) < 18)
        {
            return false;
        }
        // Needed in some cases if the user went back (clean up)
        if (currentIndex < newIndex)
        {
            // To remove error styles
            form.find(".body:eq(" + newIndex + ") label.error").remove();
            form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
        }
		
		//else if (currentIndex === 0 && (!$('#opt1').is(':checked'))) return false;
        form.validate().settings.ignore = ":disabled,:hidden";
        return form.valid();
		//return true;
    },
    onStepChanged: function (event, currentIndex, priorIndex)
    {
        // Used to skip the "Warning" step if the user is old enough.
        if (currentIndex === 2 && Number($("#age-2").val()) >= 18)
        {
            form.steps("next");
        }
        // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
        if (currentIndex === 2 && priorIndex === 3)
        {
            form.steps("previous");
        }
		if (currentIndex === 7 ){
			
			$(document).find(".actions").hide();
			$(document).find(".steps").hide();
			$(".backtostepone").fadeIn(300);
			$(".backtostepone").click(function(){
				//form.steps("setStep", 1);
				$(this).fadeOut(300);
				$( "#example-advanced-form" ).steps('reset');
				$(document).find(".actions").show();
				$(document).find(".steps").show();
				});
			}
		if (currentIndex === 1 && $('#opt2').is(":checked")) {
			steping = 1;
			for(var i=0; i<steping; i++) {
				console.log(i);
					form.steps("next");
				}
			} 
				
		if (currentIndex === 1 && $('#opt2').is(":checked")) {
			//form.steps("previous");
			$('#opt2').prop('checked', false);
			//form.steps("previous");
		}
		
		if ($("#yesinsurance2").is(":checked")) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if ($("#involvment1").is(":checked") || $("#involvment4").is(":checked")) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}	
		if ($("#yesinsurance2").prop('checked') == false && $("#involvment2").is(":checked")) {
			$("#reccomendation").text("Do it with our help");
			$(".rightreco a").attr("href", "/do_it_with_our_help.html");
			$("#otherbudle2").text("Do it yourself");
			$("#otherbudle3").text("Build with us");
			$("#otherllink2 a").attr("href", "/do_it_yourself.html");
			$("#otherllink3 a").attr("href", "/build_with_us.html");
			}	
		if ($("#yesinsurance2").prop('checked') == false && $("#involvment3").is(":checked")) {
			$("#reccomendation").text("Do it yourself");
			$(".rightreco a").attr("href", "/do_it_yourself.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Build with us");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/build_with_us.html");
			}
		if (($("#involvment2").prop('checked') == true && $("#budget3").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget3").is(":checked")) || 
			($("#involvment2").prop('checked') == true && $("#budget4").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget4").is(":checked")) || 
			($("#involvment2").prop('checked') == true && $("#budget5").is(":checked")) || 
			($("#involvment3").prop('checked') == true && $("#budget5").is(":checked"))
			) {
			$("#reccomendation").text("Build with us");
			$(".rightreco a").attr("href", "/build_with_us.html");
			$("#otherbudle2").text("Do it with our help");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/do_it_with_our_help.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if (($("#involvment2").prop('checked') == true && $("#budget1").is(":checked") && $("#yesinsurance2").prop('checked') == false)) {
			$("#reccomendation").text("Do it with our help");
			$(".rightreco a").attr("href", "/do_it_with_our_help.html");
			$("#otherbudle2").text("Build with us");
			$("#otherbudle3").text("Do it yourself");
			$("#otherllink2 a").attr("href", "/build_with_us.html");
			$("#otherllink3 a").attr("href", "/do_it_yourself.html");
			}
		if (($("#involvment3").prop('checked') == true && $("#budget2").is(":checked") && $("#yesinsurance2").prop('checked') == false)) {
			$("#reccomendation").text("Do it yourself");
			$(".rightreco a").attr("href", "/do_it_yourself.html");
			$("#otherbudle2").text("Build with us");
			$("#otherbudle3").text("Do it with our help");
			$("#otherllink2 a").attr("href", "/build_with_us.html");
			$("#otherllink3 a").attr("href", "/do_it_with_our_help.html");
			}								
	
    },
    onFinishing: function (event, currentIndex)
    {
        form.validate().settings.ignore = ":disabled";
        return form.valid();
		
    },
    onFinished: function (event, currentIndex)
    {
        alert("Submitted!");
		
    }
}).validate({
    errorPlacement: function errorPlacement(error, element) { element.before(error); },
    rules: {
        planningpermission: {
		   required: true
		}
		,budget: {
			required: true
			},
		involvement: {
			required: true
			},
		live: {
			required: true
		},
		insurance: {
			required: true
		},
		experience: {
			required: true
		},
		fordrawing: {
			required: false
		}	
    },
	messages: {
		planningpermission: {
		  //minlength: jQuery.format("Zip must be {0} digits in length"),
		  //maxlength: jQuery.format("Please use a {0} digit zip code"),
		  required: "Please select an option to continue."
		},
		budget: {
			required: "Please Select option to continue"
			},
		involvement: {
			required: "Please Select option to continue"
			},
		live: {
			required: "Please select an option to continue."
			},
		insurance: {
			required: "Please select an option to continue."
				},	
		experience: {
			required: "Please select an option to continue."
			},	
		fordrawing: {
			required: "Please select an option to continue."
			}	
		  }
});

	//alert(currentIndex + " ok");
	/*$('a[href="#next"]').click(function(){
		
		function checkindex(event, currentIndex, newIndex){
			alert(currentIndex + " ok");
			}
		
		});
		if (currentIndex === 2 && $('#opt2').is(":checked")) {
			
				$('#opt2').prop('checked', false);
				steping2 = 2;
				
				for(var i=0; i<steping2; i++) {
					console.log(i);
					form.steps("previous");
				}
				
				}*/

	
	

 //$("#example-advanced-form-p-1").hide();
 //$("#example-advanced-form").steps("remove", Number(1));
 


</script>
<script>
$(".helpmechoose").click(function(){
	$(".quizwrap").animate({left:"0%"});
	});
$(".closebuttonquiz").click(function(){
	$(".quizwrap").animate({left:"-100%"});
	});	
</script>
<script>
	$(".questionclass").click(function(){
		//$(".questionclass").removeClass("active");
		if ($(this).hasClass("active")) {
			$(this).removeClass("active");
			}
		else {	
		$(this).addClass("active");
		}
		});
	$(".buildsecdetails2 a").click(function(){
		$(this).hide();
		$("#video").show();
		//$("#video").play();
		});
	
	$(document).on('click', '.buildsecdetails2 a', function (e) {
		var video = $("#video").get(0);
		if (video.paused === false) {
			video.pause();
		} else {
			video.play();
		}

    return false;
});	
</script>

<?php require_once('./inc/footer.inc.php'); ?>