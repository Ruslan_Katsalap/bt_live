<?php

require_once ('./inc/util.inc.php');

// ********This is CRM integration code*********


require_once($_SERVER["DOCUMENT_ROOT"]."/crm_config.php");

if(isset($_POST['name']) && isset($_POST['phone'])) {
 $now = strftime("%Y-%m-%dT%H:%M:%S");
 $comment = "Postcode: ".$_POST['postcode_f'];
 $comment.= "\r\nMessage:\r\n".$_POST['message'];

 $data = array(
    "NAME" =>  $_POST['name'],
    "LAST_NAME" => $_POST['surname'],
    "UF_CRM_1490175334" => $now,
    "EMAIL_HOME" => $_POST['email'],
    "PHONE_HOME" =>$_POST['phone'],
    "TITLE" =>$_POST['name'].' '.$_POST['surname'],
    "COMMENTS" =>  $comment
  );
 CrmClient::$SOURCE_ID = 2;

 if(!CrmClient::sendToCRM($data)) {
      echo 'error send to crm';
 }

}


// ********This is CRM integration code*********

/*debug:
$_POST['act'] = 'login';
$_POST['email'] = 'dmitry@bikosite.net';
$_POST['pwd'] = '1234567';*/

if (isset($_POST['act'])) {
  if ('validate_email' == $_POST['act'] && isset($_POST['email'])) {
    $out = array();
    $_POST['email'] = trim($_POST['email']);
    
    $sql = "SELECT id, email FROM `user` WHERE email='".mysql_real_escape_string($_POST['email'])."'";
    $rs = getRs($sql);
    
    if (mysqli_num_rows($rs)) {
      $out['msg'] = 'ERR';
      $out['err'] = $_POST['email'].' is already registered!';
    }
    else {
      $out['msg'] = 'OK';
    }
    echo json_encode($out);
  }
  
  if ('login' == $_POST['act'] && isset($_POST['email']) && isset($_POST['pwd']) ) {
    $out = array();
    
    $_POST['email'] = trim($_POST['email']);
    $_POST['pwd'] = trim($_POST['pwd']);
    
    if ($_POST['email'] && $_POST['pwd']) {
      $sql = "SELECT id, email, firstname, surname, address, phone, postcode FROM `user` WHERE email='".mysql_real_escape_string($_POST['email'])."' AND password='".mysql_real_escape_string($_POST['pwd'])."'";
      $rs = getRs($sql);
      
      if (!mysqli_num_rows($rs)) {
        $out['msg'] = 'ERR';
        $out['err'] = 'Email or password is incorrect!';
      }
      else {
        $row = mysqli_fetch_assoc($rs);
        $out['msg'] = 'OK';
        $out['userid'] = $row['id'];
        $out['firstname'] = $row['firstname'];
        $out['surname'] = $row['surname'];
        $out['address'] = $row['address'];
        $out['phone'] = $row['phone'];
        
        $_SESSION['userid'] = $row['id']; // login
        $_SESSION['user_info'] = array('email'=>$row['email'], 'firstname'=>$row['firstname'], 'surname'=>$row['surname'], 'phone'=>$row['phone'], 'address'=>$row['address'], 'postcode'=>$row['postcode']);
      }
      
      echo json_encode($out);
      
    }
    
  }
  
  if ('forgot' == $_POST['act'] && isset($_POST['email']) && ($_POST['email']=trim($_POST['email']))) {
    $out = array();
    
    $sql = "SELECT id, email, firstname, surname, password FROM `user` WHERE email='".mysql_real_escape_string($_POST['email'])."'";
    $rs = getRs($sql);
    
    if (!mysqli_num_rows($rs)) {
      $out['msg'] = 'ERR';
      $out['err'] = 'This email is not registered in our system!';
    }
    else {
      $row = mysqli_fetch_assoc($rs);
      $out['msg'] = 'OK';
      
      $admin_email = 'info@buildteam.com';
      
      //require_once ('./phpmailer/class.phpmailer.php');
      require_once ('./phpmailer/PHPMailerAutoload.php'); // class.phpmailer.php

      $email_subject = "Build Team: Password Recovery Request";

      $mail = new PHPMailer();
      $mail->IsSMTP();
      $mail->SMTPDebug = 0;
      $mail->SMTPAuth = true;
      $mail->SMTPSecure = 'TLS';
      $mail->Host = "smtp.sendgrid.net";
      $mail->Port = 587;
      //$mail->IsHTML(true);
      $mail->Charset = 'UTF-8';
      $mail->Username = "apikey";
      $mail->Password = "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";
      
      $mail->From = $admin_email;
      $mail->FromName = 'Build Team';
      $mail->AddAddress($row['email'], $row['firstname']);
      $mail->Subject = $email_subject;
      $mail->Body = "Hello {$row['firstname']}

YouÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂ¢ÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂÃÂve received this e-mail because you have forgotten your password to log-on to our site.

Your password is: {$row['password']}

If you did not request your password, then please ignore this message. This password has been sent to your e-mail address only.

Kind Regards

Build Team

www.buildteam.com
hello@buildteam.com
020 7495 6561";

      if (!$mail->Send()) {
        $out['msg'] = 'ERR';
        $out['err'] = 'Can\'t send you email because of system error! Please contact us: <a href="mailto:'.$admin_email.'">'.$admin_email.'</a>';
      }
      
    }
    
    echo json_encode($out);
  }
  
  if ('logout' == $_POST['act']) {
    $out = array();
    
    unset($_SESSION['userid']);
    unset($_SESSION['user_info']);
    
    $out['msg'] = 'OK';
    echo json_encode($out);
  }
  
  if ('update_email' == $_POST['act'] && isset($_POST['email']) && ($_POST['email']=trim($_POST['email'])) && isset($_SESSION['userid']) && ($_SESSION['userid']=(int)$_SESSION['userid'])) {
    
    if (preg_match("/^[a-z0-9_\.-]+@([a-z0-9]+([\-]+[a-z0-9]+)*\.)+[a-z]{2,7}$/i", $_POST['email'])) {
      $out = array();
      
      $sql = "SELECT id FROM `user` WHERE email='".mysql_real_escape_string($_POST['email'])."'";
      $rs = getRs($sql);
      
      $email_found = false;
      $email_unique = true;
      
      if (mysqli_num_rows($rs)) {
        $email_found = true;
        $row = mysqli_fetch_assoc($rs);
        if ($row['id']!=$_SESSION['userid']) $email_unique = false;
      }
      
      if ($email_unique) {
        $out['msg'] = 'OK';
        if (!$email_found) {
          $sql = "UPDATE `user` SET email='".mysql_real_escape_string($_POST['email'])."' WHERE id=".$_SESSION['userid'];
          setRs($sql);
        }
      }
      else {
        $out['msg'] = 'ERR';
        $out['err'] = 'This email is already registered!';
      }
      
      echo json_encode($out);
    }
    
  }
 
  if ('update_pwd' == $_POST['act'] && isset($_POST['pwd']) && ($_POST['pwd']=trim($_POST['pwd'])) && isset($_SESSION['userid']) && ($_SESSION['userid']=(int)$_SESSION['userid'])) {
    $out = array();
    
    $out['msg'] = 'OK';
    $_POST['pwd'] = substr($_POST['pwd'], 0, 100);

    $sql = "UPDATE `user` SET password='".mysql_real_escape_string($_POST['pwd'])."' WHERE id=".$_SESSION['userid'];
    setRs($sql);

    echo json_encode($out);
    
  }
 
}

/*

$name = mysql_real_escape_string($_REQUEST['name']);
$surname = mysql_real_escape_string($_REQUEST['surname']);
$email = mysql_real_escape_string($_REQUEST['email']);
$email = mysql_real_escape_string($_REQUEST['register_email']);
$phone = mysql_real_escape_string($_REQUEST['phone']);
$postcode = mysql_real_escape_string($_REQUEST['postcode_f']);
$address = mysql_real_escape_string($_REQUEST['house']);
$password = mysql_real_escape_string($_REQUEST['register_password']);

$is_user = mysqli_query($dbconn,"SELECT * FROM user WHERE email = '{$email}'");

if (mysqli_num_rows($is_user) > 0) {
	echo "A user with this e-mail address is already registered";
	exit();
}

setRs("INSERT INTO user (firstname, surname, email, phone, postcode, address, password) VALUES ('{$name}', '{$surname}', '{$email}', '{$phone}' ,'{$postcode}', '{$address}', '{$password}')");
echo 'Successful registration!';

*/

?>