<?php
require_once('./inc/header.inc.php');
?>
<style>
	.bannerdiv {
			/*background: url(images/cookies/banner.jpg) center;
			background-size: cover;*/
		}
	.main.nopadding {
		background-color:transparent;
		padding:0px;	
		}
	.mainwrap {
		max-width:980px;
		margin:0 auto;
		}	
	.cookietitle {
			
		}	
	.cookietitle h1 {
			color: #134573;
			padding:0px;
		}
	.textwrap p {
        /* font-weight: normal; */
		line-height: 20px;
		margin-bottom: 30px;
		font-size: 16px;
		text-align: justify;
		color: #000;
		padding-top: 0px;
	}
.textwrap p span {
    color: #0f4577;
    font-weight: bold;
}
.textwrap ul li {
    font-weight: normal;
    line-height: 20px;
    font-size: 20px;
    text-align: justify;
    color: #74787f;
    padding-left: 15px;
}
.textwrap ul {
    list-style: none;
    max-width: 520px;
    position: relative;
}
.textwrap ul li:before {
    content: "•";
    color: #0f4577;
    margin-right: 5px;
    position: absolute;
    left: 0;
}	
@media (max-width:1024px) {
	.mainwrap {
		padding:0px 15px;
		}
	.bannerdiv {
		background:none;
		
		}
	.cookietitle {
    padding: 10px 0px 0px 0px;
}		
.cookietitle h1 {
    color: #134573;
    font-size: 30px !important;
    margin-bottom: 10px !important;
    padding-bottom: 10px !important;
}
.textwrap p, .textwrap ul li {
	font-size:18px !important;
	}
	}
	
</style>
</div>
<div class="bannerdiv">
	<div class="bannerinnner">
    	<div class="mainwrap">
        	<div class="cookietitle">
            	<h1>Cookie Policy</h1>
            </div>
        </div>
    </div>
</div>

<div class="mainwrap">
	<div class="textwrap">
    	<p>Cookies are text files which contain small amounts of information, which your computer or mobile device downloads when you visit a website. When you return to the website – or visit a website that uses the same cookies – they recognise these cookies and therefore your browsing device. Like most websites, we use cookies to do lots of different jobs, like letting you navigate between pages efficiently, remembering your preferences and generally improving your browsing experience. In addition to the cookies we use on this website, we also use cookies and similar technologies in some emails. These helps us to understand whether you have opened an email and how you have interacted with it.</p>
    </div>
    <div class="textwrap">
    	<p>
        	Where available, the “Cookies” link will appear at the bottom of the page, notifying you of our intention to use cookies during your visit. By continuing to browse our website, you are agreeing to our use of cookies. Technical cookies are strictly necessary for the Build Team Website to work properly.
        </p>
    </div>
    <div class="textwrap">
    	<p>In many cases, you can control tracking technologies using your browser. Please ensure that your browser setting reflects whether you wish to be warned about and/or accept tracking technologies (such as cookies) wherever possible. The specific capabilities of your browser and instructions on how to use them can be found in the manual or help file of your browser.</p>
    </div>
    <!--<div class="textwrap">
    	<p>For the same reason, we may obtain information about your general internet usage by using a cookie file which is stored on the hard drive of your computer. Cookies contain information that is transferred to your computer’s hard drive. They help us to improve our site and to deliver a better and more personalised service. They enable us:</p>
    </div>
    <div class="textwrap">
    	<ul>
    		<li>To estimate our audience size and usage pattern.</li>
    		<li>To store information about your preferences, and so allow us to customise our site according to your individual interests.</li>
    		<li>To speed up your searches.</li>
    		<li>To recognise you when you return to our site.</li>
    	</ul>
    </div>
    <div class="textwrap">
    	<p>You may refuse to accept cookies by activating the setting on your browser which allows you to refuse the setting of cookies. However, if you select this setting you may be unable to access certain parts of our site. Unless you have adjusted your browser setting so that it will refuse cookies, our system will issue cookies when you log on to our site.</p>
    </div>-->
</div>

<?php
require_once('./inc/footer.inc.php');
?>