<?php

require_once('./inc/header.inc.php');
?>

<div class="full">
<h1>Social Share</h1><br/>

<?php

$photo = '';

if (isset($_GET['photo'])) {
  $photo = trim($_GET['photo']);
 
  $sql = "SELECT i.project_id, p.project_name, p.project_code, p.project_category_id FROM project_image i LEFT JOIN project p ON (i.project_id=p.project_id) WHERE filename='".mysqli_real_escape_string($dbconn, $photo)."'";
  $res = mysqli_query($dbconn,$sql);
  
  if (!mysqli_num_rows($res)) $photo = '';
  else {
    $row = mysqli_fetch_assoc($res);
  }
}

if ($photo) {
  //echo 'Hi!)<pre>'.print_r($row, 1).'</pre>';#debug
  $project_name = htmlspecialchars($row['project_name'], ENT_QUOTES, 'UTF-8');
  echo '<h2 style="float:left;font-size:1.1em;margin-left:1.5em">'.$project_name.'</h2><div style="float:right;margin-right:2em"><!-- AddThis Button BEGIN -->
      <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
      <a class="addthis_button_facebook"></a>
      <a class="addthis_button_twitter"></a>
      <a class="addthis_button_email"></a>
      <a class="addthis_button_google_plusone_share"></a>
      <a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
      </div>
      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=dmbiko"></script>
      <!-- AddThis Button END --></div><br clear="all" />';
  echo '<div style="text-align:center"><img src="/projects/'.$photo.'" alt="'.$project_name.'" style="max-width:100%" /></div>';
  echo '<!--div style="clear:both;width:100%"><div style="float:left;margin-left:1.5em"><a href="/'.$row['project_code'].'-'.($row['project_category_id']==3?'side-return-extension':'refurbishment').'.html">View other photos of this project</a></div-->';
  echo '<div style="clear:both;width:100%"><div style="float:left;margin-left:1.5em"><a href="'.$_SERVER['HTTP_REFERER'].'">View other photos of this project</a></div>';
  echo '<div style="float:right;margin-right:2em"><!-- AddThis Button BEGIN -->
      <div class="addthis_toolbox addthis_default_style addthis_16x16_style">
      <a class="addthis_button_facebook"></a>
      <a class="addthis_button_twitter"></a>
      <a class="addthis_button_email"></a>
      <a class="addthis_button_google_plusone_share"></a>
      <a class="addthis_button_compact"></a><a class="addthis_counter addthis_bubble_style"></a>
      </div>
      <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=dmbiko"></script>
      <!-- AddThis Button END --></div></div>';

}
else {
  echo 'Oops, no project photo found. You can find your photo in projects below:<ul style="margin-left:4em;margin-top:1em"><li><a href="/project-gallery/projects-side-return-extensions.html">Side Return Extensions</a></li><li><a href="/project-gallery/projects-refurbishment.html">Refurbishment Projects</a></li></ul>';
}

?>

</div>

<?php

require_once('./inc/footer.inc.php');

?>