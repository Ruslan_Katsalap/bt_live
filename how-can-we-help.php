<?php require_once('./inc/header.inc.php'); ?>

<?php require_once('save_how_can_help_form.php'); ?>
<link rel="stylesheet" href="js/accordian/jquery.accordion.css" type="text/css">
<link rel="stylesheet" href="js/slidertab/jquery.sliderTabs.css" type="text/css">
<style>
.nopaddingmain {
    max-width: 980px;
    margin: 0 auto;
    overflow: hidden;
}
.page-title {
	border-bottom:2px solid #d7d7d7;
	}
.howcanhelptopwrap {
	margin-bottom:30px;
	}
	.howcanhelptopwrap p {
		    padding-top: 10px;
			font-size: 18px;
			text-align: justify;
			color: #74787f;
			line-height: 24px;
			font-weight: normal;
		}
	.howcanmiddle {
		background: #adbecd;
	}
	.howcanwemainner {
    max-width: 90%;
    margin: 0 auto;
}
.build-col-3 {
    float: left;
    width: 32%;
    background: #5b7c9a;
    text-align: center;
	position:relative;
}
.build-col-3:nth-child(2) {
    margin: 0% 2%;
}
.howcanwemainner:after {
	content:"";
	display:block;
	clear:both;
	}
.howcan-3-title h1 {
    color: #FFF;
    font-size: 28px !important;
    padding: 0px;
    padding-top: 20px;
}
.howcan-3-icon {
    margin-bottom: 20px;
	margin-top:50px;
}
.howcan-3-icon img {
    height: 80px;
}
.howcan-3-semititle h3 {
    font-size: 20px;
    color: #FFF;
}
.howcan-3-desc p {
    font-size: 18px;
    color: #FFF;
    padding-left: 35px;
    padding-right: 35px;
	line-height:22px;
}
.howcan-3-link img {
    width: 45px;
}
.howcan-3-link {
    margin-top: 30px;
    margin-bottom: 150px;
}	
.tab {
    overflow: hidden;
	margin-top: 50px;
	border-bottom: 2px solid #124779;
	padding-bottom: 5px;
}

/* Style the buttons inside the tab */
.tab .tablinks {
    display: inline-block;
    color: #fff;
    font-size: 14px;
    text-align: center;
    padding: 10px 40px;
    width: auto;
    height: 44px;
    line-height: 44px;
    -webkit-transform: translateY(6px);
    -moz-transform: translateY(6px);
    -ms-transform: translateY(6px);
    -o-transform: translateY(6px);
    transform: translateY(6px);
    background: #f3f4f8;
    color:#0f4777;
    border: none;
	margin-right:10px;
}
.tab .tablinks a {
	font-weight:bold;
	}
.tab .tabbutton1:after, .tab .tabbutton2:after {
    /*content: "";
    position: absolute;
    height: 2px;
    width: 121px;
    background: #969696;
    top: -1px;
    left: 120px;*/
}

/* Change background color of buttons on hover */
.tab button:hover {
    background-color: #ddd;
}
.thankscan {
	z-index:111;
	}
/* Create an active/current tablink class */
@media (min-width:640px) {
.tab .tablinks.selected {
    color: #fff;
    background: #124779;
}
}
.tab .tablinks.selected {
    color: #fff;
    background: #124779;
}
/* Style the tab content */
.tabcontent {
    
    padding: 5px 0px;
    /* border: 1px solid #ccc; */
    border-top: none;
	margin-top:0px;
}
#we_do_it_1 {
	/*padding-left:20px;*/
	}
.main {
	max-width:980px;
	margin:0 auto;
	overflow:hidden;
	}
.build-40 {
    float: left;
    width: 45.8%;
    padding-right: 30px;
}
.build-60 {
    float: left;
    width: 51%;
}
.icon-33-inner {
    text-align: center;
}	
.build-icon-33:nth-child(3) .icon-image-wrap {
    border: none;
}
.howcanbig h1 {
    color: #0f4778;
    font-size: 36px !important;
    margin-bottom: 0px !important;
	line-height:36px;
	margin-top: 0px;
    padding-top: 0px;
	padding-bottom: 0px !important;
}
.howcanbig {
	position:relative;
	}
.howcanbig span {
    position: absolute;
    right: 0px;
    top: 0px;
    font-size: 40px;
    /* background: #5b7c9a; */
    /* font-weight: bold; */
    color: #124779;
}
.howcantab h4 {
    font-size: 22px;
    color: #969696;
    font-weight: 200;
    padding-top: 0px !important;
    margin-top: 0px;
	margin-bottom:0px;
}
.howcandesc p {
    color: #969696;
    font-size: 16px;
	line-height:18px;
    text-align: justify;
    margin-top: 0px;
    padding-top: 10px;
    /* margin-bottom: 30px; */
}
.howcanlist p {
    color: #0f4778;
    font-weight: bold;
	
}
.howcanlist {
    font-size: 16px;
	margin-top:0px;
	padding:10px 0px;
}
.howcanlist ul {
    color: #969696;
	list-style-image: url('/images/howcanwe/images/orange_sign.jpg');
	padding-left: 25px;
    margin-top: 10px;
}
.howcanlist ul li {
    margin-bottom: 2px;
}
.build-icon-33 {
    display: inline-block;
    width: 32%;
	
}
.icon-image-desc p {
	color: #0f4778;
	font-size:16px;
}
.icon-image-wrap {
    border-right: 1px solid #efefef;
    min-height: 70px;
    vertical-align: middle;
	position:relative;
    /* display: table-cell; */
}
.icon-image-wrap img {
    position: absolute;
    left: 0px;
    right: 0px;
    bottom: 0;
    margin: 0 auto;
	    max-height: 60px;
}
.build-request-sec-inner a {
    background: #0f4778;
    color: #FFF !important;
    display: inline-block;
    padding: 7px 10px;
    border-radius: 5px;
}
.build-request-sec-inner h3 {
    padding-top: 0px;
    margin-top: 0px;
    margin-bottom: 5px;
    padding-bottom: 0px;
	color:#0f4778;
	font-weight:bold;
	font-size:18px;
}
.build-request-sec-inner p {
    /* margin-top: 10px; */
    color: #969696;
    padding-top: 5px;
    font-size: 16px;
    margin-bottom: 10px;
}
.build-request-sec {
    background: #efefef;
    padding: 15px;
	margin-top:20px;
}
#do_it_our_1 .build-request-sec {
	margin-top:40px;
	}
.build-more-text h3 {
    color: #0f4778;
    padding-bottom: 0px;
    margin-bottom: 0px;
    font-size: 18px;
}
.build-more-text p {
    /* margin-top: 10px; */
    color: #969696;
    font-size: 16px;
    padding-top: 5px;
}
.build-more-text p a {
	color: #0f4778;
	font-weight:bold;
	text-decoration:none;
	
	}
.build-more-text {
    margin-top: 20px;
    display: block;
}
.build-sitevisit-link {
    margin-top: 25px;
}
.build-sitevisit-link a {
    padding: 10px 20px;
    border: 1px solid #0e4677;
    color: #0e4677;
    font-size: 16px;
    font-weight: bold;
}
.build-form-field input[type="text"], .build-form-field input[type="email"], .build-form-field input[type="tel"] {
    width: 100%;
    height: 39px;
    text-indent: 10px;
    background: #f3f4f8;
    border: none;
    border-radius: 10px;
}
.build-form-field textarea {
    width: 99%;
    height: 40px !important;
    border-radius: 10px;
    background: #f3f4f8;
    border: none;
    text-indent: 10px;
    padding-top: 10px;
}
.build-form-submit input[type="submit"] {
    background: #0f4778;
    border: none;
    height: 40px;
    color: #FFF;
    width: 80px;
    border-radius: 10px;
}
.build-form-submit {
    position: relative;
    top: -18px;
}
.build-form-field {
    margin-bottom: 20px;
}
.build-form-2 .build-form-field:nth-child(2) {
    margin-right: 0px !important;
}
.build-form-2 .build-form-field {
    width: 48%;
    margin-right: 15px;
    float: left;
}
.build-contract-form {
    margin-top: 30px;
}
.build-form-1 {
    position: relative;
}
.build-for-hide {
	}
.tablinks img {
    max-width: 30px;
    max-height: 30px;
	width:auto;
    vertical-align: middle;
    margin-right: 10px;
}	
#do_it_yourself_1 .build-more-text {
    margin-top: 10px;
}
</style>
<style>
	#accordion {
list-style: none;
margin: 30px 0;
padding: 0;
overflow: hidden;
	}
 
#accordion .acrdli {
float: left;
border-left:
display: block;
width: 0px;
height:0px;
padding: 0px 0;
overflow: hidden;
color: #fff;
text-decoration: none;
font-size: 16px;
line-height: 1.5em;
border-left: 1px solid #fff;
background:#fff;
position:relative;
transition:all 0.5s;
}

 .acrdli .tabcontent {
   
}
.acrdli.active .tabcontent{
	opacity:1;
	} 
#accordion .acrdli.selected .titleclose {
	display:none;
	}
 
#accordion .acrdli.active {
width: 976px;
background:#FFF;
}
li.acrdli .titleclose {
    transform: rotate(-90deg);
    display: block;
    width: 250px;
    position: absolute;
    left: -106px;
    bottom: 115px;
    text-align: left;
	color:#FFF;
	display:none;
}
div#do_it_our_1 {
    /*padding-left: 20px;
    padding-right: 10px;*/
}
.build-for-hide {
    /* display: none; */
    width: 980px;
    float: left;
}
.build-hide-click {
    width: 980px;
    float: left;
}
.overflowhiddenforslide {
    width: 1960px;
	transition:all 0.5s;
	position:relative;
	display:block;
}
.tabbanner {
	text-align:center;
	margin:20px 0px 30px 0px;
	}
@media (max-width:640px) {
	.nopaddingmain {
   
    width: 90%;
}
.ui-slider-tabs-list-container {
	margin:0px 0px !important;
	}
.tab .tablinks {
	display:block;
	margin-bottom: 10px;
	background:#fff;
	}	
	.ui-slider-tabs-list-wrapper {
		margin-bottom:40px;
		}
.ui-slider-left-arrow, .ui-slider-right-arrow {
	display:none !important;
	}
	.overflowhiddenforslide {
		left:inherit !important;
		width:100% !important; 
		}
	.build-hide-click {
		display:none;
		}
	.build-for-hide {
		float:none;
		width:100% !important;
		}	
	.build-40 {
		width:98%;
		float:none;
		}
	#accordion {
		height:auto;
		}
	#accordion .acrdli {
		
		}
	
.tablinks img  {
    
    margin-right:20px;
}
	#accordion .acrdli.active {
    width: 99%;
    background: #FFF;
}		
.page-title {
    font-size: 22px;
    border: none;
    text-align: center;
    font-weight: bold;
}
.tab {
	margin-top:5px;
	}
.build-60 {
	width:98%;
	float:none;
	}
.icon-image-wrap img {
	width: auto !important;
    max-height: 50px;
    max-width: 50px;
	}	
.build-icon-33 {
	vertical-align:top;
	}
.icon-image-desc p {
    color: #0f4778;
    font-size: 14px;
    line-height: 16px;
    padding-top: 10px;
}	
.build-form-2 .build-form-field {
	margin-right:10px;
	}
.hidetabformob {
	display:none;
	}
.tabformobile {
	display:inherit;
	}
.tabformobile {
    background: #f3f4f8;
    padding: 20px 0px;
}
.mobilebg {
	background: #f3f4f8;
	}	
.innermobiletab {
    max-width: 90%;
    margin: 0 auto;
}

.tab button {
    background: #FFF;
    width: 80%;
    margin: 0 auto;
    margin-bottom: 10px;
    font-weight: bold;
    font-size: 16px;
    text-align: left;
	padding: 0px 20px;
    height: 50px;
    margin-bottom: 10px;
    display: block;
}
.tabcontent {
	padding-right:10px;
	}
.howcanbig span {
	    position: relative;
    margin-top: 10px;
    display: block;
    padding: 0px;
    font-size: 20px;
	}
.build-form-3 .build-form-field {
    width: 100% !important;
    display: inline-block;
    margin-bottom: 10px;
}
.build-form-3 .build-form-field:nth-child(2) {
    margin:0px 0px !important;
	margin-bottom:10px !important; 
}	
.leftline {
	display:none;
	}			
.righttext {
	width:100% !important;
	}
li.active#getlithree {
    height: 1300px !important;
}	
li.active#getlitwo {
    height: 1070px !important;
}	
li.active#getlione {
    height: 650px !important;
}	
.tab {
	border:none;
	}
.tabbanner {
	margin: 20px 0px 10px 0px !important;
	}
.newbottomtitle {
    text-align: center;
    margin-bottom: 20px;
}
.thanksinner {
    height: 125px !important; 
    width: 75%;
    margin: 0 auto;
}
.mobilebg {
    background: #f3f4f8;
    position: absolute;
    height: 420px;
    top: 0px;
    width: 100%;
}
.build-form-submit {
	top:0px !important; 
	}
.deskimg {
		display:none;
		
		}	
	.mobimg {
		display:block;
		width:100px;
		margin:0 auto;
		}
	.innerbanner .mobimg {
		display:block;
		width:100% !important;
		margin:0 auto;
		}	
	.newbottomtitle {
		text-align:left !important;
		}
		
		.formobileinline.left {
			float: left;
    width: 20%;
			}			
		.formobileinline img {
			width: 39px;
			}		
		#we_do_it_1 .righttext {
    width: 80% !important;
	float: right !important;
}	
#we_do_it_1  .righttext .smallpara p {
    font-size: 12px;
    line-height: 16px;
}
span.mobinline {
    display: inline !important;
    text-align: left;
}
.imgoption span {
	text-align:left;
	}
.ui-slider-tabs-leftPanelArrow, .ui-slider-tabs-rightPanelArrow {
	display:none;
	}
.imgicon img {
	width:18px !important;
	}
.newbottomtitle p {
	font-size:18px !important;
	}									
	}
@media (min-width:640px) {
	.hidetabformob {
	display:inherit;
	}
.tabformobile {
	display:none;
	}	
	.forsameheight {
		padding-top:30px;
		padding-bottom:20px;
	}
	/*li.active#getlithree {
    height: 789px !important;
	}	
	li.active#getlitwo {
		height: 630px !important;
	}	
	li.active#getlione {
		height: 563px !important;
	}	*/
	li.active#getlithree {
    height: 670px !important;
	}	
	li.active#getlitwo {
	height: 670px !important;
	}	
	li.active#getlione {
	height: 670px !important;
	}
	.tabbanner img {
			width: 700px;
			position: relative;
    left: 20px;
		}
	#do_it_our_1 .bottomnavigation {
		margin-top:10px;
		}	
	.tab {
		width:100% !important; 
		}	
	.tabcontent {
		font-size:initial;
		}
	.deskimg {
		display:block;
		margin:0 auto;
		}	
	.mobimg {
		display:none;
		
		}			
	
	}	
.selected .blueicon {
	display:none;
	}	
.whiteicon {
	display:none;
	}	
.selected .whiteicon {
	display:inline !important;
	}
.stickerdiv img {
    
}	
.stickerdiv {
    position: absolute;
    left: -20px;
    bottom: 30px;
}
.newbottomtitle {
	text-align:center;
	}
.newbottomtitle p {
	color:#969696;
	font-size:20px;
	}	
.newbottomtitle p strong{
	color:#0f4778;
	}
.imgoption span {
    display: block;
}
.lefticontitle {
    float: left;
    color: #969696;
}	
.imgicon img {
    width: 30px;
}
.imgoption {
    display: inline-block;
	vertical-align:top;
	line-height: 18px;
}	
.imgicon {
    display: inline-block;
	margin-right:10px;
}
.next-icon img {
	width:40px;
	}
.rightlinkicon {
    float: right;
}
.imgoption span {
	font-weight:normal;
	padding-left:0px;
	padding: 2px;
	}
.next-icon {
    padding-top: 15px;
}	
.optionbottom {
    padding: 10px 0px;
    border-top: 2px solid #124779;
    border-bottom: 2px solid #124779;
    margin-top:7px;
}
.build-form-3 .build-form-field:nth-child(2) {
    margin: 0px 15px;
}
.build-form-3 .build-form-field {
    width: 32%;
    display: inline-block;
    margin-bottom: 10px;
}
.build-form-3 select {
    height: 40px;
    border-radius: 10px;
    width: 100%;
    background: #f3f4f8;
    border: none;
	text-indent: 5px;
}	
.smalltitle p {
	color:#124779;
	font-weight:bold;
	margin-bottom:15px;
	}	
.leftline img {
    width: 78px;
    position: relative;
    top: 0px;
}
.leftline {
    float: left;
    width: 20%;
}
.smallpara p {
    color: #969696;
    padding-top: 0px;
    text-align: justify;
	line-height:16px;
    /* font-size: 14px; */
}
.smallpara span {
    color: #124779;
}
.righttext {
    float: left;
    width: 80%;
} 
.righttext .smallpara p {
	font-size:15px;
	line-height:16px;
	}
.howcantab.newtitle h4 {
    color: #124779 !important;
    font-weight: bold;
    margin: 20px 0px;
    font-size: 22px;
}
.smallpara {
    margin-bottom: 10px;
}
.bottomnavigation {
	padding:20px 0px;
    border-bottom: 2px solid #124779;
	margin-top:0px;
	height:45px;
	}
	
.bottomnavigation img {
    width: 40px;
}
.lefttab {
    float: left;
}
.righttab {
    float: right;
}
.forbannerinlie {
	display:inline-block;
	}	
.forfloat {
	float:right;
	margin-top:20px;
	}
.thanksinner {
	height:100px;
	}	
.thanksinner p {
    font-size: 18px;
    padding-top: 18px;
    color: #969696;
}
.thanksinner a {
	margin-top:15px;
	}
span.request-call{
    color: #124779;
}			
span.request-call:hover{
    text-decoration:underline;
	background: none;
}
.smallalone {
	margin-bottom:15px;
	}			
</style>
</div>
<!--<div class="tabformobile">
	<div class="innermobiletab">
        <h2 class="page-title">The Build Phase</h2>
        <div class="tab">
          <button class="tablinks tabbutton1 active forulheight" onclick="openCity(event, 'do_it_yourself')"><img class="whiteicon" src="/images/howcanwe/images/tab-1.png" alt=""><img class="blueicon" src="/images/howcanwe/images/blue-tab-1.png" alt=""> Do it yourself</button>
          <button class="tablinks tabbutton2 forulheight" onclick="openCity(event, 'do_it_our')"><img src="/images/howcanwe/images/tab-2.png" class="whiteicon" alt=""><img class="blueicon" src="/images/howcanwe/images/blue-tab-2.png" alt=""> Do it with our help</button>
          <button class="tablinks tabbutton3 forulheight" onclick="openCity(event, 'we_do_it')"><img src="/images/howcanwe/images/tab-3.png" class="whiteicon" alt=""><img class="blueicon" src="/images/howcanwe/images/blue-tab-3.png" alt=""> We’ll do it for you</button>
        </div>
    </div>
</div>-->
<div class="mobilebg"></div>
<div class="nopaddingmain">
<div class="overflowhiddenforslide">
	<div class="build-hide-click">
<div class="howcanhelptopwrap">
	<h2 class="page-title">The Build Phase</h2>
    
    <p>All of our clients have different requirements when it comes to the Build Phase; some like to be heavily involved in the process and others like to take a back seat and let us drive it forward. When you’re ready to Build, we can assist in a variety of ways:</p>
</div>
<div class="howcanmiddle">
	<div class="howcanwemainner">
    	<div class="howcanwewrap">
        	<div class="build-col-3">
            	<div class="howcan-3-title"><h1>Do It Yourself</h1></div>
                <div class="stickerdiv"><img src="/images/howcanwe/images/sticker.png" class="stickerimage" alt=""></div>
                <div class="howcan-3-icon"><img src="images/howcanwe/images/we_all.png" alt=""></div>
                <div class="howcan-3-semititle"><h3>Tender the scheme yourself</h3></div>
                <div class="howcan-3-desc"><p>We equip you with</br>the tools necessary to</br>tender your scheme within</br>our ‘Get Ready to Build’</br>Design Bundle.</p></div>
                <div class="howcan-3-link"><img id="do_it_your" src="images/howcanwe/images/link-icon.jpg" alt=""></div>
            </div>
            <div class="build-col-3">
            	<div class="howcan-3-title"><h1>Do It With Our Help</h1></div>
                <div class="howcan-3-icon"><img src="images/howcanwe/images/we_do.png" alt=""></div>
                <div class="howcan-3-semititle"><h3>Contract Management</h3></div>
                <div class="howcan-3-desc"><p>If you want to Project Manage your build but want some professional assistance to keep things on track, we can help.</p></div>
                <div class="howcan-3-link"><img id="do_it_our" src="images/howcanwe/images/link-icon.jpg" alt=""></div>
            </div>
            <div class="build-col-3">
            	<div class="howcan-3-title"><h1>We’ll Do It All</h1></div>
                <div class="howcan-3-icon"><img src="images/howcanwe/images/yourself.png" alt=""></div>
                <div class="howcan-3-semititle"><h3>Build with Build Team</h3></div>
                <div class="howcan-3-desc"><p>Our Build arm can take care of all aspects of the project. You can sit back and relax and let us take care of everything.</p></div>
                <div class="howcan-3-link"><img id="we_do_it" src="images/howcanwe/images/link-icon.jpg" alt=""></div>
            </div>
        </div>
    </div>
</div>
</div>

<div class="build-for-hide" >
		
    	<div id="mySliderTabs">
        <h2 class="page-title">How can we help with your Build?</h2>
        
        <ul class="tab">
            <li class="tablinks"><a class="" href="#doityourself"><img class="whiteicon" src="/images/howcanwe/images/tab-1.png" alt=""><img class="blueicon" src="/images/howcanwe/images/blue-tab-1.png" alt="">Do it yourself</a></li>
            <li class="tablinks"><a class="" href="#doitwithourhelp"><img src="/images/howcanwe/images/tab-2.png" class="whiteicon" alt=""><img class="blueicon" src="/images/howcanwe/images/blue-tab-2.png" alt="">Do it with our help</a></li>
            <li class="tablinks"><a class="" href="#welldoit"><img src="/images/howcanwe/images/tab-3.png" class="whiteicon" alt=""><img class="blueicon" src="/images/howcanwe/images/blue-tab-3.png" alt="">We’ll do it for you</a></li>
         </ul>
    


      <div class="acrdli" id="doityourself">
      	<!--<h1 class="titleclose">Do it yourself</h1>-->
        <div id="do_it_yourself_1" class="tabcontent" style="display:block;">
  	<div class="">
    	<div class="forsameheight">
    	<div class="howcanbig">
        	<h1>Tender the scheme yourself</h1>
        </div>
        <!--<div class="howcantab">
        	<h4>Do it Yourself</h4>
        </div>-->
        <div class="howcandesc">
        	<p>Our Design Phase has been specifically designed to equip you with the tools necessary to tender your scheme amongst contractors. Lots of our clients elect to use our Design Service with a contractor already assigned, or with the intention to gather comparative quotes once they have those detailed drawings. To make things simple, we include a Detailed Schedule of Works within your package which outlines the scope of works you intend to undertake.</p>        
        </div>
        <div class="tabbanner">
        	<img class="deskimg" src="images/howcanwe/images/how_tab1.jpg" alt="">
            <img class="mobimg" src="images/howcanwe/images/mobile-tab1.jpg" alt="">
        </div>
        <div class="newbottomtitle">
        	<p><strong>How do I tender my scheme?</strong> Simply send the following documentation to a variety of contractors and request a quote</p>
        </div>
        </div>
        <div class="optionbottom">
        	<div class="lefticontitle">
            	<div class="imgicon">
                	<img src="/images/howcanwe/images/call-icon.jpg" alt="">
                </div>
                <div class="imgoption">
                	<span class="mobinline" style="letter-spacing: 0.2px;">Need Help Choosing</span>
                    <span class="mobinline" style="letter-spacing: 0.9px;">A Design Package?</span>
                    <span class="request-call ui-button ui-corner-all ui-widget">Request a call back</span>
                </div>
            </div>
           <div class="rightlinkicon">
            	<div id="next1"  class="next-icon forulheight">
                	<!--<img src="/images/howcanwe/images/next-icon.jpg" alt="">-->
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        
    </div>
    
   
    <div class="clearfix"></div>
</div>

      </div>
      
      <div class="acrdli"  id="doitwithourhelp">
      	<!--<h1 class="titleclose">Contract Management</h1>-->
        <div id="do_it_our_1" class="tabcontent">
            <div class="">
            <div class="forsameheight">
                <div class="howcanbig">
                    <h1>Contract Management</h1>
                    <!--<span>Fixed fee of 5k*</span>-->
                </div>
               <!-- <div class="howcantab">
                    <h4>Do it with our help</h4>
                </div>-->
                <div class="howcandesc">
                    <p>If you want to invest your own time in Project Managing your own build to keep the costs down, we can help. In this case we simply offer our professional guidance and assist with the complex stuff, like getting the details of a contract agreed. We also inspect the build at those key stages to ensure the quality is up to scratch.</p>
                </div>
                <div class="howcanlist">
                	<div class="forbannerinlie">
                    <p>What’s included?</p>
                    <ul>
                        <!--<li>Help with finding a contractor</li>-->
                        <li>Assistance with putting the contract in place</li>
                        <li>Regular site visits and progress reports</li>
                        
                    </ul>
                    </div>
                    <div class="forbannerinlie forfloat">
                    	<div class="innerbanner">
                        	<img src="/images/howcanwe/images/tab2-banner.jpg" alt="">
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            
            <div class="smalltitle"><p>Want more information? Book a meeting with our experts:</p></div>
            
            <div class="build-form-inner">
                        <form method="post" action>
                            <div class="build-form-3">
                                <div class="build-form-field">
                                    <input type="text" name="howcanname" required placeholder="Name*">
                                </div>
                                <div class="build-form-field">
                                    <input type="text" name="address" required placeholder="Address*">
                                </div>
                                <div class="build-form-field">
                                    <input type="text" name="howcanpostcode" required placeholder="Postcode*">
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            
                            <div class="build-form-3">
                                <div class="build-form-field">
                                    <input type="email" name="email" required placeholder="Email Address*">
                                </div>
                                <div class="build-form-field">
                                    <input type="tel" name="telephone" required placeholder="Telephone*">
                                </div>
                                <div class="build-form-field">
                                    <select name="status" id="">
                                        <option value="">Status</option>
                                        <option value="I have no designs or permission">- I have no designs or permission</option>
                                        <option value="I have basic designs and planning permission">- I have basic designs and planning permission</option>
                                        <option value="I have detailed drawings and structural calculations">- I have detailed drawings and structural calculations</option>
                                        <option value="Other">- Other</option>
                                    </select>
                                </div>
                            </div>
                            <div class="build-form-3">
                                <div class="build-form-field">
                                    <textarea type="text" name="message" required placeholder="Message*"></textarea>
                                </div>
                                <div class="build-form-field">
                                    <div class="build-form-submit">
                                    <input type="submit" value="Submit">
                                </div>
                                </div>
                            </div>
                           <!-- <div class="build-form-3">
                                <div class="build-form-submit">
                                    <input type="submit" value="Submit">
                                </div>
                            </div>-->
                        </form>
                    </div>
                    </div>
                    <div class="bottomnavigation">
                    	<div class="lefttab" class="forulheight">
                        	<!--<img id="prev2" src="/images/howcanwe/images/prev-icon.jpg" alt="">-->
                        </div>
                        <div class="righttab" class="forulheight ">
                        	<!--<img id="next2" src="/images/howcanwe/images/next-icon.jpg" alt="">-->
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                
                <div class="clearfix"></div>
            </div>
      </div>
      
      <div class="acrdli"  id="welldoit">
      	<!--<h1 class="titleclose">We’ll do it for you</h1>-->
        <div id="we_do_it_1" class="tabcontent">
         <div class="forsameheight">
        <div class="howcanbig">
        	<h1>Build With Build Team</h1>
        </div>
       
        <div class="howcandesc">
        	<p>Once you have those detailed drawings, our construction team would be delighted to offer a quote to undertake the Build for you. Our Build Phase takes care of everything – from start to finish. Your Build Contract is with us, and with that come lots of benefits such as our comprehensive Project Management Service and our 10 Year Guarantee.</p>
        </div>
        <div class="forbannerinlie forfloat">
                    	<div class="innerbanner">
                        	<img class="mobimg" src="/images/howcanwe/images/tab3-banner.jpg" alt="">
                            <img class="deskimg" src="/images/howcanwe/images/tab3-banner-desk.jpg" alt="">
                        </div>
                    </div>
         <div class="clearfix"></div>           
        <div class="howcantab newtitle">
        	<h4>Build Process</h4>
        </div>
  	<div class="build-40">
    	
    	
            <div class="smallpara">
                <p class="smallalone">If you want to Build with our construction arm, here is a breakdown of what to expect:</p>
            </div>
            <div class="leftline">
            	<img class="whiteicon" src="/images/howcanwe/images/left-line-1.jpg" alt="">
            </div>
            <div class="formobileinline left">
            	<div class="motime">
                	<img class="whiteicon" src="/images/howcanwe/images/mobile-timeline.jpg" alt="">
                </div>
            </div>
            <div class="righttext">
            <div class="smallpara">
            	<span>Site Meeting</span>
                <p>Upon your instruction of the Build Phase, we will arrange a meeting with you at the property with your Project Manager (PM). This enables us to confirm all of the details and answer any remaining questions.</p>
            </div>
            <div class="smallpara">
            	<span>Preparation Activities</span>
                <p>Before the build starts, we will have to complete a checklist of activities. This varies, but may include assigning an Approved Inspector, arranging the necessary documentation and suspending a parking bay for a skip.</p>
            </div>
            <div class="smallpara">
            	<span>Start Date</span>
                <p>At the commencement of the works, if you wish to be present, you can meet your PM and the builders at the property as the works commence. We will lay protective layers, strip off, dig foundations and carry out excavation works.</p>
            </div>
        </div>
         
        
    </div>
    <div class="build-60">
    	
    	<div class="build-60-inner">
        	
            
            <div class="right">
            <div class="leftline">
            	<img class="whiteicon" src="/images/howcanwe/images/right-line.jpg" alt="">
            </div>
            <div class="righttext">
            <div class="smallpara">
            	<span>On-site Construction</span>
                <p>An extension usually takes between 12 to 16 weeks. During this time your PM will give you regular updates about the works and you can be as involved as you would like to be.</p>
            </div>
            <div class="smallpara">
            	<span>Electrics & Plumbing</span>
                <p>The internal finish of the structure is carried out including 1st & 2nd fix electrics and 1st fix plumbing. We also begin plaster boarding and plastering.</p>
            </div>
            <div class="smallpara">
            	<span>Snagging</span>
                <p>When the agreed works have been carried out the project has reached practical completion. You will give your PM a list of final touches that need to be done and after these are carried out we tidy up the site and remove the waste and tools.</p>
            </div>
            <div class="smallpara">
            	<span>Completion Pack</span>
                <p>The project is handed over together with a folder containing our 10 year guarantee for the works that we’ve carried out and other important documentation regarding your project.</p>
            </div>
        </div>
             <div class="clearfix"></div>          
        </div>
        </div>
    </div>
    <div class="clearfix"></div>
    </div>
    <div class="clearfix"></div>
    <div class="bottomnavigation">
                    	<div class="lefttab forulheight">
                        	<!--<img id="prev3" src="/images/howcanwe/images/prev-icon.jpg" alt="">-->
                        </div>
                        <div class="righttab">
                        	
                        </div>
                        <div class="clearfix"></div>
                    </div>
    
</div>
      </div>
      </div>
</div>

<div class="clearfix"></div>
</div>
<div class="thankyoupop thankscan">
    	<div class="thanksinner">
        	<h4>Thank you for your request.</h4>
            <p>We will contact you at your preferred time.</p>
			<a onClick="window.location.reload()" href="javascript:void(0);">Ok</a>
            
        </div>
    </div>
<script>

$("#do_it_your").click(function(){
		$(".overflowhiddenforslide").animate({left: "-980px"}, {duration:300, queue:false});;
		$(".build-for-hide").css("display","block");
		/*$(".tablinks").removeClass("selected");
		$(".tabbutton1").addClass("selected");
		
		$(".acrdli").removeClass("selected");
		$("#doityourself").addClass("selected");*/
	});
$("#do_it_our").click(function(){
	$(".overflowhiddenforslide").animate({left: "-980px"}, {duration:300, queue:false});;
	$(".build-for-hide").css("display","block");
	/*$(".tablinks").removeClass("selected");
		$(".tabbutton2").addClass("selected");
		
		$(".acrdli").removeClass("selected");
		$("#doitwithourhelp").addClass("selected");
		$("#doityourself").css("left","-980px");
		$("#doitwithourhelp").css("left","0px");
		$("#doitwithourhelp").css("display","block");*/
	});
$("#we_do_it").click(function(){
	$(".overflowhiddenforslide").animate({left: "-980px"}, {duration:300, queue:false});;
	$(".build-for-hide").css("display","block");
	/*$(".tablinks").removeClass("selected");
		$(".tabbutton3").addClass("selected");
		
		$(".acrdli").removeClass("selected");
		$("#doityourself").css("left","-980px");
		$("#doitwithourhelp").css("left","-980px");
		$("#welldoit").css("left","0px");
		$("#welldoit").css("display","block");
		$("#welldoit").addClass("selected");*/
	});


</script>
<script>
	
$("#next1").click(function(){
	$(".tablinks").removeClass("active");
		$(".tabbutton2").addClass("active");
		//$(".tabcontent").css("display","none");
		//document.getElementById("do_it_our_1").style.display = "block";
		$(".acrdli").removeClass("selected")
		//$("#do_it_yourself_1").css("display","block");
		$("#getlitwo").addClass("selected");
	
	});	
$("#next2").click(function(){
	
	$(".tablinks").removeClass("active");
		$(".tabbutton3").addClass("active");
		//$(".tabcontent").css("display","none");
		//document.getElementById("do_it_our_1").style.display = "block";
		
		$(".acrdli").removeClass("selected");
		$("#welldoit").addClass("selected");
		$("#doityourself").css("left","-980px");
		$("#doitwithourhelp").css("left","-980px");
		$("#welldoit").css("left","0px");
		
		$("#welldoit").css("display","block");
	
	});	
$("#prev2").click(function(){
	$(".tablinks").removeClass("active");
		$(".tabbutton1").addClass("active");
		//$(".tabcontent").css("display","none");
		//document.getElementById("do_it_our_1").style.display = "block";
		$(".acrdli").removeClass("selected")
		//$("#do_it_yourself_1").css("display","block");
		$("#getlione").addClass("selected");
	
	});
$("#prev3").click(function(){
	$(".tablinks").removeClass("active");
		$(".tabbutton2").addClass("active");
		//$(".tabcontent").css("display","none");
		//document.getElementById("do_it_our_1").style.display = "block";
		$(".acrdli").removeClass("selected")
		//$("#do_it_yourself_1").css("display","block");
		$("#getlitwo").addClass("selected");
	
	});
$(".forulheight").click(function(){
	var getheight = 0;
	getheight = $("#accordion .acrdli.active").innerHeight();
	console.log(getheight)
	//$("#accordion").height(getheight);
	});					
function openCity(evt, cityName) {
    
}

</script>
<script>
$(document).ready(function(e) {
    <? if($howcansend == true){?>
$(".thankscan").fadeIn(200);
<? } ?>
});

</script>


<script src="js/slidertab/jquery.sliderTabs.js"></script>
<script src="js/accordian/jquery.accordion.js"></script> 
<script>
var slider = $("div#mySliderTabs").sliderTabs({
  autoplay: false,
  mousewheel: false,
  position: "top"
});
</script>
<script>
$(document).ready(function(){
 
    $(".thankscan a").click(function(){
	
	location.reload();
	});
 
});
</script>
<script>
$(document).ready(function(e) {
    if ($(window).width() < 640) {
		$("#getlione").addClass("active");
		$(".acrdli").css("max-width",$(window).width() - 30);
		$(".tab").css("max-width",$(window).width() - 30);
		}
		
});

</script>
<?php require_once('./inc/footer.inc.php'); ?>