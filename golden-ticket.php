<?php 
require_once('./inc/header.inc.php');
?>

<style>
.main1{
	width: 100%;
	margin: 0 auto;
}
.golden .bx-wrapper {
    margin: 0 auto !important;
    text-align: center;
}
.golden .bx-wrapper .bx-controls-direction a {
    display: none;
}
.term a {
    color: #808385;
}
img.img_slide {
    margin: 0 auto;
}
.booksite{
	font-size: 20px; color: #aba412; padding: 15px;
	padding-left: 0px !important;
}

.img{
	text-align: center; width: 100%; border: 1px lightgray solid;
}
.term{
	position: relative;
    z-index: 666;
    top: -30px;
	text-align: center;
    font-size: 5px;
    font-weight: bold;
}
.havewin{
	background-color: #aba412; padding: 5px; text-align: center; font-size: 32px; color: white; font-weight: bold; margin-top: 20px;
}
.whatwin{
	text-align: center; font-size: 24px;
	padding: 20px;
}

.prize{
	float: left; 
	width: 23%;
	padding: 15px;
	padding-left: 0px !important;
	line-height:1.5;
	font-size: 13px;
	color: gray;
}
.prizeheading{
	padding: 5px;
	font-weight: bold;
	letter-spacing: 1;
	margin-top: -22px;
}


@media (max-width: 760px){
	.main1{
	width: 60%;
	margin: 0 auto;
}

.booksite{
	font-size: 22px; color: #aba412; padding: 15px;
	padding-left: 0px !important;
}

.img{
	text-align: center;
	min-width: 100%; 
	border: 1px lightgray solid;
	margin: 0 auto;
}
.term{
	position: relative;
    z-index: 666;
    top: -30px;
    font-size: 8px;
    font-weight: bold;
}
.havewin{
	background-color: #aba412; padding: 5px; text-align: center; font-size: 15px; color: white; font-weight: bold; margin-top: 17px;
}
.whatwin{
	text-align: center; font-size: 22px;
	padding: 20px;
}

.prize{
	float: left; 
	min-width: 100%;
	padding: 15px;
	padding-left: 0px !important;
	line-height:1.5;
	font-size: 13px;
	color: gray;
}
.prizeheading{
	padding: 5px;
	font-weight: bold;
	letter-spacing: 1;
}
}

@media (min-width: 760px){
	.main1{
	width: 100%;
	margin: 0 auto;
}

.booksite{
	font-size: 28px; color: #aba412; padding: 15px;
	padding-left: 0px !important;
}

.img{
	text-align: center;
	min-width: 100%; 
	border: 1px lightgray solid;
	margin: 0 auto;
}
.term{
	position: relative;
    z-index: 666;
    top: -30px;
    font-size: 9px;
    font-weight: bold;
}
.havewin{
	background-color: #aba412; padding: 5px; text-align: center; font-size: 25px; color: white; font-weight: bold; margin-top: 20px;
}
.whatwin{
	text-align: center; font-size: 22px;
	padding: 20px;
}

.prize{
	float: left; 
	min-width: 100%;
	padding: 15px;
	padding-left: 0px !important;
	line-height:1.5;
	font-size: 13px;
	color: gray;
}
.prizeheading{
	padding: 5px;
	font-weight: bold;
	letter-spacing: 1;
}
}

@media (min-width:1200px){
			.main1{
	width: 100%;
	margin: 0 auto;
}

.booksite{
	font-size: 32px; color: #aba412; padding: 15px;
	padding-left: 0px !important;
}

.img{
	text-align: center;
	min-width: 100%; 
	border: 1px lightgray solid;
	margin: 0 auto;
}

.term{
	 position: relative;
    z-index: 666;
    top: -30px;
    font-size: 9px;
    font-weight: bold;	
}
.whatwin{
	text-align: center; font-size: 22px;
	padding: 20px;
}

.prize{
	float: left; 
	min-width: 23%;
	padding: 15px;
	padding-left: 0px !important;
	line-height:1.5;
	font-size: 13px;
	color: gray;
}
.prizeheading{
	padding: 5px;
	font-weight: bold;
	letter-spacing: 1;
}
}
@media (max-width:480px){
.main1 {
    width: 100%;
margin: 0 auto;}
}
</style>
<div class="main1">
<div class="booksite">Book a Site Visit to Win a Golden Ticket</div>
	<div id="merry_xmas_happy_ny golden" class="golden" style="position:relative;border: 1px lightgray solid;">
   
	
	<ul class="bxslider">
		<li>
			<img src="/images/golden_ticket1.png" alt="" width="55%" height="252" class="img_slide"/>
			
			<div class="SliderLayered">
				<div class="SliderText">
					
				</div>
				
			</div>
		</li>
		<li>
			<img src="/images/golden_ticket_landscape.png" alt=""  width="55%" height="252" class="img_slide"/>
			
			<div class="SliderLayered">
				<div class="SliderText">
					
				</div>
				
			</div>
		</li>
		<li>
			<img src="/images/golden_ticket_interior.png" alt="" width="55%" height="252" class="img_slide"/>
			
			<div class="SliderLayered">
				<div class="SliderText">
					<!--h1 class="extraLarge">10 YEAR GUARANTEE</h1>
					<span class="subtitletext">Specialist 10 year guarantee <br/> and full completion pack with all builds</span-->
				</div>
				
			</div>
		</li>
		<li>
			<img src="/images/golden_ticket3D.png" alt="" width="55%" height="252" class="img_slide"/>
			
			<div class="SliderLayered">
				<div class="SliderText">
					<!--h1 class="extraLarge">10 YEAR GUARANTEE</h1>
					<span class="subtitletext">Specialist 10 year guarantee <br/> and full completion pack with all builds</span-->
				</div>
				
			</div>
		</li>
		
		
	</ul>
	<script type="text/javascript">
		jQuery('.bxslider').bxSlider({
		  mode: 'fade',
		  speed: 500,
            auto: true,
		  captions: true
		});
		
	</script>

  </div>
	
		
			<div class="term"><a href="/terms-and-conditions.html" target="_blank">Terms and Conditions Apply</a></div>

		
		<div class="havewin">
			YOU HAVE A 1 IN 4 CHANCE OF WINNING!
		</div>
		<div style="padding: 15px; font-size: 14px; color: gray; line-height: 2">
			We are very excited to announce that we are running a Golden Ticket Raffle.

To be a part of this fantastic raffle, all you have to do is book in a <a href="http://www.buildteam.com/getting-started/book-site-visit.html">Site Visit</a>. A member of our Architectural Team will meet you at your

property to discuss your extension and run through how we work. After the site visit, we will leave you with a Golden Envelope which

will hold your ticket. If you get a golden ticket, you're a winner! <br><br>

You have a 1 in 4 chance of winning one of these fantastic prizes - one of which includes a FREE DESIGN PHASE worth £3,995!

There is lots more on offer, including lots of Free Landscape Design, Free Interior Design, and Free 3D Concept Designs. Terms And Conditions Apply
		</div>
		
			
		
			<div class="whatwin">
				What can you win?				
			</div>
			
			<div class="prize">
				<div style="text-align: center; color: #f79646; font-size: 13px;">
					<img src="images/housegold.png" style="width: 115px;">
					<div class="prizeheading">Free Design Phase</div>
				</div><br>
				
				Our Design Phase includes every thing you

				need to start Building your Extension. It

				starts with a Measured Survey, where a

				member of our Architectural Team will visit

				you at your property so we can measure up

				and discuss design and layout options with

				you. Following this, we will send over your

				existing and proposed plans – usually with

				a few options so you can see how the

				layouts compare. You're then free to

				amend the design until you're happy to

				submit to planning. We then submit to

				planning and take care of Party Wall

				Matters for you.
				
			</div>
			
			<div class="prize">
				<div style="text-align: center; color: #f79646; font-size: 13px; font-weight: bold;">
					<img src="/images/house1.png" style="width: 115px;">
					<div class="prizeheading">Landscape Design Service</div>
				</div><br>
				
				You shouldn't under estimate the impact your

				out door space will have on your new indoor

				space, particularly if you are opting for lots of

				glass and bifold doors. Many of our clients

				choose to redesign their garden while they

				are designing their kitchen extension, mostly

				because they can ensure both designs will

				complement each other. Our Landscape

				Design Service includes an in-depth

				consultation with an experienced member of

				our Design Team.
				
			</div>
			
			<div class="prize">
				<div style="text-align: center; color: #f79646; font-size: 13px; font-weight: bold;">
					<img src="/images/house121.png" style="width: 105px;margin-bottom: 15px;">
					<div class="prizeheading" style="padding-bottom: 0px !important; padding-top: 0px !important;">3D Concept Service</div>
				</div><br>
				
				It is important to get your Design just right

				and we understand that at times, it can be

				hard to visualise what you want. Our 3D

				Concept Service helps you visualise

				exactly what Design you want, so you can

				see it on paper before finalising your

				Design and submitting to Planning. The

				feedback we have received from this

				service is fantastic, and we find that clients

				have often changed their mind about

				design features once they've seen it on

				paper, helping them to avoid mistakes and

				get it right first time around.
				
			</div>
			
			<div class="prize">
				<div style="text-align: center; color: #f79646; font-size: 13px; font-weight: bold;">
					<img src="/images/house4.png" style="width: 115px;">
					<div class="prizeheading" style="padding-bottom: 2px !important;">Interior Design Service</div>
				</div><br>
				
				Many people associate interior design with

				a bit of paint or some fluffy cushions and

				hastily miss the opportunity to really

				execute the true potential of their home.

				The interior of your home can manipulate all

				kinds of things including light, sound and

				temperature. Our Interior Design Service

				helps you visualise the space and offers

				advice on choosing features which are

				specific to you. It’s important to start

				thinking about the small internal stuff at the

				beginning as it might alter the overall

				Design.
				
			</div><br/><br/><br/><br/><br/><br/>
			<br/><br/><br/><br/>
		
	
	<!--header end-->
	
		<div class="clear"></div>

	</div>
	
	
	
<?php
 require_once('./inc/footer.inc.php');
?>