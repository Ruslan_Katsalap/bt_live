<?php
require_once('./inc/util.inc.php');
require_once('./inc/header.inc.php');
?>
<link href="css/bootstrap.css" rel="stylesheet">
<style>
.maximgsize img {
	max-width:100% !important;
	}
.toptitleimg {
	padding:40px 0px;
	}
.topjanubanner {
	padding-bottom:20px;
	}
.janunopadding {
	padding:0px !important;
	}
.topjanubanner {
	margin-bottom:30px;
	}	
.forcentertable {
	display:table;
	width:100%;
	text-align:center;
	height:135px;
	}
.forheight {
	display:table-cell;
	vertical-align:middle;
	}
.janurighttitle p{
	color:#74787f;
	font-weight:bold;
	margin-left:0px;
	padding: 0px;
    margin-bottom: 0px;
	}
.forheight .januornage, .forheight .janublue {
	font-size:34px;
	}
.januornage {
	color:#f7921e;
	}
.janublue {
	color:#104777;
	}		
.janurighttitle p{
	font-size:18px;
	}
.janurightdesc p {
	font-size:16px;
	color:#104777;
	font-weight: 400;
	line-height:20px;
	font-family:calibri !important;
	}	
.janudotted {
	padding-top:20px;
	padding-bottom:20px;
	border-bottom: dotted;
    border-width: 0 0 5px 0;
    border-image-slice: 33% 33%;
    border-image-repeat: round;
    border-image-source: url(../images/dots.svg);
	}
.januterms p {
	font-size:12px;
	padding:0px;
	letter-spacing: 1px;
    line-height: 15px;
	}	
.janutermstitle {
	margin-top:20px;
	font-weight:bold;
	}
@media (max-width:640px) {
	.toptitleimg {
	padding:20px 0px;
	}
	.topjanubanner {
    margin-bottom: 0px;
}
.janurightdesc p {
	text-align:justify;
	 }
	 .janurighttitle p {
    
    text-align: center;
}
.januterms {
	text-align:center;
	}
	}														
</style>
<div class="">
	<div class="toptitleimg maximgsize">
    	<img src="images/january-sale/janu-title.jpg" alt="">
    </div>
</div>

<div class="">
	<div class="topjanubanner maximgsize">
    	<img src="images/january-sale/january-sale.jpg" alt="">
    </div>
</div>
<div class="janudotted">
	<div class="col-sm-2 janunopadding">
    	<div class="forcentertable">
        	<div class="forheight">
            	<div class="januornage">
                    <span>50%</span>
                </div>
                <div class="janublue">
                    <span>OFF</span>
                </div>
            </div>
        </div>
        
    </div>
    <div class="col-sm-10 janunopadding">
    	<div class="janurighttitle">
        	<p>LANDSCAPE DESIGN SERVICE</p>
        </div>
        <div class="janurightdesc">
        	<p>You shouldn’t underestimate the impact your garden will have on your new extension. Many of our clients choose to redesign their garden while they are redesigning their kitchen extension, mostly because they can ensure both designs will complement each other. Our Landscape Design Service includes an in-depth consultation with an experienced member of our Design Team. We will create various design and layout options for your garden. You are then free to amend the proposed options until you have your final design.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="janudotted">
	<div class="col-sm-2 janunopadding">
    	<div class="forcentertable">
        	<div class="forheight">
            	<div class="januornage">
                    <span>30%</span>
                </div>
                <div class="janublue">
                    <span>OFF</span>
                </div>
            </div>
        </div>
        
    </div>
    <div class="col-sm-10 janunopadding">
    	<div class="janurighttitle">
        	<p>INTERIOR DESIGN SERVICE</p>
        </div>
        <div class="janurightdesc">
         	<p>To ensure you make the most of your final design, we encourage all of our clients to think about the finishing touches at the very beginning, as we feel the interior of your extension is important to help shape your ‘shell’ design. Our Interior Design Service helps you visualise your space by offering guidance on the materials you want to use and how they will interact with features such as Velux windows and lighting.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="janudotted">
	<div class="col-sm-2 janunopadding">
    	<div class="forcentertable">
        	<div class="forheight">
            	<div class="januornage">
                    <span>25%</span>
                </div>
                <div class="janublue">
                    <span>OFF</span>
                </div>
            </div>
        </div>
        
    </div>
    <div class="col-sm-10 janunopadding">
    	<div class="janurighttitle">
        	<p>FAST TRACK SERVICE</p>
        </div>
        <div class="janurightdesc">
        	<p>Our Design Team offers a Fast Track Service, which is great for those of you who want to be on site as soon as planning has been granted. It enables us to start your structural drawings immediately. In the event your case officer requests amendments during the planning process, we make these free of charge and amend the structural calculations and drawings accordingly. We can then book in your Pre Contract Meeting and send over your Detailed Schedule of Works.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="janudotted">
	<div class="col-sm-2 janunopadding">
    	<div class="forcentertable">
        	<div class="forheight">
            	<div class="januornage">
                    <span>25%</span>
                </div>
                <div class="janublue">
                    <span>OFF</span>
                </div>
            </div>
        </div>
        
    </div>
    <div class="col-sm-10 janunopadding">
    	<div class="janurighttitle">
        	<p>PARTY WALL SERVICE</p>
        </div>
        <div class="janurightdesc">
        	<p>If you are keen to start building soon, we would recommend starting the Party Wall process as soon as we have submitted your planning application - this is mainly because it is a time consuming process and we find it beneficial to run it alongside the planning window. We can sort everything out for you and serve the relevant Notices and undertake the Schedule of Conditions on your behalf. Our Party Wall surveyor is a RICS Qualified Surveyor, and has years of experience dealing with all kinds of neighbours and varied agreements.</p>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<div class="januterms">
	<div class="col-sm-2 janunopadding"></div>
    <div class="col-sm-10 janunopadding">
    	<p class="januornage janutermstitle">Terms and Conditions</p>
        <p class="janublue janutermsdetails"><span class="januornage">1</span>) You can avail of multiple offers as detailed within this page, however these offers cannot be used in conjunction with any offer you may have received. <span class="januornage">2</span>) Ancillary services are only available as part of a Design Phase instruction. <span class="januornage">3</span>) The discount for the Party Wall Service applies to one Notice and Schedule of Condition only and can only be used if Design Team is instructed to serve all relevant Notices. <span class="januornage">4</span>) These offers are only valid for 30 days from the date of your site visit.</p>
    </div>
</div>

<?php
require_once('./inc/footer.inc.php');
?>