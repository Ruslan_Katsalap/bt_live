<?php
require_once('./inc/util.inc.php');
require_once('./inc/header.inc.php');
?>
<style>
body {
    font-family: 'Open Sans', Arial, sans-serif;
    font-size: 16px;
	font-weight:400;
}
.main {
	max-width:975px !important;
	}
.commonthree .comoninlineblock {
    display: inline-grid;
    max-width: 30.7%;
    width: 100%;
	padding:0px 10px;
}
.topara h4 {
	font-size:18px;
	color: #0f4679;
	}
.topara p {
	line-height:20px;
	color: #75787f;
	}
.ringwrap img {
    max-width: 120px;
}
.ringwrap {
	text-align:center;
	height:150px;
	}
.ringinfo p {
	color: #0f4679;
	font-weight:500;
	font-size:16px;
	max-width: 93%;
    margin: 0 auto;
	}	
.ringsimage .comoninlineblock {
	text-align:center;
	}
.ringsimage {
	padding: 50px 0px;
    margin: 40px 0px;
    border-top: 2px solid #8ca5bc;
    border-bottom: 2px solid #8ca5bc;	
	}
.formfields label {
    display: block;
    margin-bottom: 5px;
	text-align:left;
	color:#6687a6;
}	
.formfields input {
    width: 100%;
    height: 35px;
	padding:0px 5px;
    border-radius: 8px;
    box-shadow: none;
    border: 1px solid #6687a6;
}
.formfields {
    margin-bottom: 25px;
	
}
.terms p {
    font-size: 12px;
    text-align: justify;
	color: #75787f;
}
.formwrap input[type=file] {
    border: none;
}
.formfields textarea {
	height: 115px;
    width: 102%;
	 border-radius: 8px;
    box-shadow: none;
    border: 1px solid #6687a6;
	}
.formwrap .firstfrom {
	text-align:left;
	}
.formwrap .secondfrom {
	text-align:center;
	}
.formwrap .thirdfrom {
	text-align:right;
	}	
.formwrap input[type=submit] {
	background:#6687a6;
	color:#FFF;
	font-size:16px;
	height:38px;
	}
.ringmob {
	display:none;
	}	
.thankyoupop {
	display:none;
    position: fixed;
    height: 100%;
    width: 100%;
    background: rgba(16, 70, 116, 0.67);
    top: 0px;
    left: 0px;
}	
.thanksinner {
    position: relative;
    top: 35%;
    height: 130px;
    width: 300px;
    background: #FFF;
    left: auto;
    right: auto;
    margin: 0 auto;
    padding: 20px 20px;
}
.thanksinner h4 {
    border-bottom: 2px solid #6687a6;
    padding-bottom: 5px;
	color:rgb(16, 70, 116);
}		
.thanksinner a {
    background: #6687a6;
    display: inline-block;
    padding: 10px 20px;
    color: #FFF;
    margin-top: 10px;
    border-radius: 10px;
}
@media (max-width:640px) {
	.commonthree .comoninlineblock {
		width:100%;
		display:block;
		max-width:100%;
		padding:0px;
		margin-bottom:20px;
		}
	.ringwrap {
		height:120px;
		}	
	.formfields input {
		width:95%;
		}	
	.formfields textarea {
		width:97%;
		}
	.ringmob .ringwrap {
		display:table-cell;
		}
	.ringmob .comoninlineblock{
		display:table;
		width:100%;
		text-align:center;
		}	
	.ringmob .ringwrap p {
		font-weight:bolder;
		color:#75787f;
		}	
	.ringmob .ringwrap p:before {
    content: ">";
    position: relative;
    left: -10px;
    color: #ff5d00;
    /* font-size: 20px; */
}	
	.ringdesk {
		display:none;
		}	
	.ringmob {
		display:block;
		}
	.formwrap input[type=submit] {
		width:250px;
		}
	.formwrap .thirdfrom {
		text-align:left;
		}
	.ringwrap img {
    	max-width: 100px;
		}				
	}				
</style>

<div class="topara">
	<h4>Already have architectural plans?</h4>
    <p>We recognise that some clients have already instructed an architect and therefore may already have architectural plans, planning permission and possibly a full structural drawing pack. Don’t worry if you are only part way through this process; when we receive your drawings, we will audit what you already have and inform you if there are any gaps in your design information. We will also offer a fixed fee to fill in these gaps, which can also include the dispatch of neighbourly party Wall matters.</p>
</div>

<div class="commonthree ringsimage ringdesk">
	<div class="comoninlineblock">
    	<div class="ringwrap"><img src="images/architectural/ring-1.png" alt=""></div>
        <div class="ringinfo">
        	<p>Upload your plans.</p>
        </div>
    </div>
	<div class="comoninlineblock">
    	<div class="ringwrap"><img src="images/architectural/ring-2.png" alt=""></div>
        <div class="ringinfo">
        	<p>We will contact you within 24 hours to confirm receipt of your enquiry and book and initial site visit.</p>
        </div>
    </div>
	<div class="comoninlineblock">
    	<div class="ringwrap"><img src="images/architectural/ring-3.png" alt=""></div>
        <div class="ringinfo">
        	<p>Following the initial site visit, we will prepare a detailed schedule of costs and invite you for a further meeting at our office in Clapham to discuss further.</p>
        </div>
    </div>
</div>

<div class="commonthree ringsimage ringmob">
	<div class="comoninlineblock">
    	<div class="ringwrap" data-desc="Upload your plans."><a href="#cometoform"><img src="images/architectural/ring-1.png" alt=""></a><p>UPLOAD</p></div>
        <div class="ringwrap" data-desc="We will contact you within 24 hours to confirm receipt of your enquiry and book and initial site visit."><img src="images/architectural/ring-2.png" alt=""><p>CONTACT</p></div>
        <div class="ringwrap" data-desc="Following the initial site visit, we will prepare a detailed schedule of costs and invite you for a further meeting at our office in Clapham to discuss further."><img src="images/architectural/ring-3.png" alt=""><p>QUOTE</p></div>
       
    </div>
     <div class="ringinfo">
        	<p>Upload your plans.</p>
        </div>
</div>

<div class="commonthree formwrap">
	<form action="" method="post">
	<div class="comoninlineblock firstfrom">
    	<div class="formfields">
            <label for="">Full name*</label>
            <input type="text" name="fullname">
        </div>
        <div class="formfields">
            <label for="">Address*</label>
            <input type="text" name="address">
        </div>
        <div class="formfields">
            <label for="">Email*</label>
            <input type="email" name="email">
        </div>
        <div class="formfields">
            <label for="">Telephone*</label>
            <input type="tel" name="number">
        </div>
    </div>
	<div class="comoninlineblock secondfrom">
    	<div class="formfields">
            <label for="">Planning URL*</label>
            <input type="url" name="plannnig">
        </div>
        <div class="formfields">
            <label for="">When are you looking to start?</label>
            <input type="text" name="lookingtostart">
        </div>
        <div class="formfields">
            <label for="">Message</label>
            <textarea name="message" id=""></textarea>
        </div>
    </div>
	<div class="comoninlineblock thirdfrom">
    	<div class="formfields" id="cometoform">
            <label for="">File upload</label>
            <input type="file" name="file">
        </div>
        <div class="formfields">
            <label for="">&nbsp;</label>
            <input type="submit" value="Submit">
        </div>
        <div class="formfields">
            <div class="terms">
            	<p>By Submitting this form, you consent to your details being added to the build Team customer database which is subject to our privacy policy. As a member of this database, you will receive news and updates which we feel are relevant to your needs. if, however, you would prefer not to receive these updates, you can unsubscribe from the database at any time.</p>
            </div>
        </div>
    </div>
    </form>
    <div class="thankyoupop">
    	<div class="thanksinner">
        	<h4>Thank you for submitting your form</h4>
            <p>We will contact you within 24 hours to confirm receipt of your enquiry
and book and initial site visit.</p>
			<a href="javascript:void(0);">Continue</a>
        </div>
    </div>
</div>
<script>
$(document).ready(function(e) {
    $(".ringmob .ringwrap").click(function(){
		var getdesc = $(this).attr("data-desc");
		//alert(getdesc);
		$(".ringmob .ringinfo p").html(getdesc);
	});
});
</script>
<script>
$(document).ready(function(e) {
    $('.ringwrap a').click(function() {
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') 
        || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
           if (target.length) {
             $('html,body').animate({
                 scrollTop: target.offset().top - 150
            }, 1000);
            return false;
        }
    }
});
});
</script>
<?php
require_once('./inc/footer.inc.php');
?>