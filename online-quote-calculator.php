<?php require_once('./inc/header_2.inc.php');
function getRealIpAddr()
{
	if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	{
	  $ip=$_SERVER['HTTP_CLIENT_IP'];
	}
	elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	{
	  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	}
	else
	{
	  $ip=$_SERVER['REMOTE_ADDR'];
	}
	return $ip;
}
function getBrowser()
{
	$u_agent = $_SERVER['HTTP_USER_AGENT'];
	$bname = 'Unknown';
	$platform = 'Unknown';
	$version= "";

	//First get the platform?
	if (preg_match('/linux/i', $u_agent)) {
		$platform = 'linux';
	}
	elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
		$platform = 'mac';
	}
	elseif (preg_match('/windows|win32/i', $u_agent)) {
		$platform = 'windows';
	}

	// Next get the name of the useragent yes seperately and for good reason
	if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent))
	{
		$bname = 'Internet Explorer';
		$ub = "MSIE";
	}
	elseif(preg_match('/Firefox/i',$u_agent))
	{
		$bname = 'Mozilla Firefox';
		$ub = "Firefox";
	}
	elseif(preg_match('/Chrome/i',$u_agent))
	{
		$bname = 'Google Chrome';
		$ub = "Chrome";
	}
	elseif(preg_match('/Safari/i',$u_agent))
	{
		$bname = 'Apple Safari';
		$ub = "Safari";
	}
	elseif(preg_match('/Opera/i',$u_agent))
	{
		$bname = 'Opera';
		$ub = "Opera";
	}
	elseif(preg_match('/Netscape/i',$u_agent))
	{
		$bname = 'Netscape';
		$ub = "Netscape";
	}

	// finally get the correct version number
	$known = array('Version', $ub, 'other');
	$pattern = '#(?<browser>' . join('|', $known) .
	')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	if (!preg_match_all($pattern, $u_agent, $matches)) {
		// we have no matching number just continue
	}

	// see how many we have
	$i = count($matches['browser']);
	if ($i != 1) {
		//we will have two since we are not using 'other' argument yet
		//see if version is before or after the name
		if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
			$version= $matches['version'][0];
		}
		else {
			$version= $matches['version'][1];
		}
	}
	else {
		$version= $matches['version'][0];
	}

	// check if we have a number
	if ($version==null || $version=="") {$version="?";}

	return array(
		'userAgent' => $u_agent,
		'name'      => $bname,
		'version'   => $version,
		'platform'  => $platform,
		'pattern'    => $pattern
	);
}
if(isset($_POST['fname'])){
	if($_POST['checkused']!='0'){
		$totalCal = (int)$_POST['totalcost'];
		$_POST['depth'] = '';
		$_POST['width'] = '';
		$totalArea = $_POST['checkused'];
		$usedCal = 'Yes';
	}else{
		$totalCal = (int)$_POST['totalcost'];
		$totalArea = ($_POST['depth']*$_POST['width']);
		$usedCal = 'No';
	}

	//changes 2021.02.16 mb
if (!isset($_POST['proptype']) || !isset($_POST['addPostcode']) || !isset($_POST['territory']) || empty($_POST['proptype']) || empty($_POST['addPostcode']) || empty($_POST['territory'])) {
	// something went wrong
	var_dump($_POST);
	printf("/nSomething went wrong.  Please email this data to support team with a short message describing what you did./n");
	exit;
}

	//changes 06.03.2019
	$max_min = mysqli_query($dbconn,"SELECT min_quote, max_quote FROM setting WHERE setting_id = 1");
	if ($row_max_min = mysqli_fetch_assoc($max_min)) {
		if(!empty($row_max_min['min_quote']) && $totalCal < (int)$row_max_min['min_quote']) {
			$totalCal = (int)$row_max_min['min_quote'];
		}
		/*if(!empty($row_max_min['max_quote']) && $totalCal > (int)$row_max_min['max_quote']) {
			$totalCal = -1 * $totalCal;
		}*/
		$_POST['totalcost'] = $totalCal;
	}
	//changes 06.03.2019
	
	$sql_ip = mysqli_query($dbconn,"select id from byp_Quote WHERE clientIP='".getRealIpAddr()."'");
	$count_ip = 0;
	while($rows = mysqli_fetch_assoc($sql_ip)){
		$count_ip++;
	}
	$count_ip +=1;
	$userAgent = getBrowser();
		mysqli_query($dbconn,"insert into byp_Quote (propertyType,postcode,side,side2,depth,width,roomsize,doors,roof,additionnal,fname,surname,email,telphone,addPostcode,address,comment,lookingStart,currentstatus,totalPrice,password,passwordconfirm,usedCal,unitMmeasures,chimneyBreast,chimneyHeating,quoteType,emailQuote,mailingList,contactMethod,lastModified,clientIP,quotesFromIP,userAgent,territory,dates_created)
	values ('".$_POST['proptype']."', '".$_POST['addPostcode']."','".$_POST['side']."','".$_POST['side2']."','".$_POST['depth']."','".$_POST['width']."','".$totalArea."',
	'".$_POST['doors']."', '".$_POST['doorsglass']."', '".$_POST['additionnal1']."', '".$_POST['fname']."', '".$_POST['surname']."', '".$_POST['email']."',
	'".$_POST['telphone']."', '".$_POST['addPostcode']."', '".$_POST['address']."', '".$_POST['comment']."', '".$_POST['lookingStart']."', '".$_POST['currentstatus']."', '".$totalCal."', '".$_POST['password']."', '".$_POST['passwordconfirm']."','".$usedCal."','METRES','".$_POST['additionnal2']."','".$_POST['additionnal3']."','DESIGN & BUILD SERVICE','No','0','E-mail','".date('F j, Y, g:i A')."','".getRealIpAddr()."','".$count_ip."','".$userAgent['userAgent']."','".$_POST['territory']."','".date('Y-m-d H:i:s')."')");
	$last_id = mysqli_insert_id($dbconn);
	$refCode = 'BYP/QUO/'.$last_id;
	mysqli_query($dbconn,"update byp_Quote set refCode='".$refCode."' WHERE id='".$last_id."'");

/* mb */
    $postpart = $_POST['addPostcode'];
    $postpart = strtoupper(str_replace(' ', '', $postpart));

    $rs2 = getRs("SELECT northing,easting FROM open_postcode_geo WHERE postcode_no_space = '{$postpart}' limit 1");

	$row2 = mysqli_fetch_assoc($rs2);
	if ($row2) {
	  $easting = $row2['easting'];
	  $northing = $row2['northing'];
      $sql1 = "UPDATE `byp_Quote` SET easting='".$easting."',northing='".$northing."' WHERE id='".$last_id."'";
	  //echo "{$sql} <br />" ;
	  mysqli_query($dbconn, $sql1);
	}
/* mb */

	$checkREG = mysqli_fetch_assoc(mysqli_query($dbconn,"select * from newbypRegister where email='".$_POST['email']."'"));
	if($checkREG['email']==''){
		mysqli_query($dbconn,"insert into newbypRegister (fName,sName,email,telPho,postcode,address,password)
		values('".$_POST['fname']."','".$_POST['surname']."','".$_POST['email']."','".$_POST['telphone']."','".$_POST['addPostcode']."','".$_POST['address']."','".$_POST['password']."')");


}else{
		$_SESSION['userID'] = $checkREG['id'];
		$_SESSION['user_info']['fName'] = $checkREG['fname'];
		$_SESSION['user_info']['Eamil'] = $checkREG['email'];
		header('Location:saved-quotes.php');
		//header('Location:saved_quote.php');

	}

// --//


	//header('Location:save_quate_getUserListing_Data.php');
	header('Location: https://www.buildteam.com/newbypquote-email.html?totalcost='.$_POST['totalcost']."&code=".$refCode."&id=".$last_id);
	//header('Location: https://www.buildteam.com/newbyp-quote.html?totalcost='.$_POST['totalcost']."&code=".$refCode."&id=".$last_id);

}
if($_SESSION['user_info']['Eamil']){
	$usersData = mysqli_fetch_assoc(mysqli_query($dbconn,"select * from newbypRegister where email='".$_SESSION['user_info']['Eamil']."'"));
	$_SESSION['userID'] = $usersData['id'];
	$_SESSION['user_info']['fName'] = $usersData['fName'];
	$_SESSION['user_info']['Eamil'] = $usersData['email'];
	$_POST['fName'] = $usersData['fName'];
	$_POST['sName'] = $usersData['sName'];
	$_POST['email'] = $usersData['email'];
	$_POST['telphone'] = $usersData['telPho'];
	$_POST['address'] = $usersData['address'];
	$_POST['password'] = $usersData['password'];

}
?>

<link rel="stylesheet" href="byp-css/normalize.css">
<link rel="stylesheet" href="byp-css/main.css">
<link rel="stylesheet" href="byp-css/jquery.steps.css">
<link rel="stylesheet" href="byp-css/custombyp.css">
<script src="lib/modernizr-2.6.2.min.js"></script>
<script src="lib/jquery.cookie-1.3.1.js"></script>

<script src="build/jquery.steps.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.js"></script>

<style>
.salesadded {
	width: 656px !important;
}
.mainnew {
	max-width:980px;
	margin:0 auto;
	}
#contactdrop .continueto p {
	padding-top: 5px;
	margin-top: 0px;
}
.numbercontact h4 {
	margin-bottom:0px !important;
	}
#contactdrop .dayinline p {
	font-size: 16px;
	line-height: 16px;
	padding: 0px !important;
	margin-top: 0px;
}
#contactdrop .addresscontact p {
	margin-bottom: 0px !important;
	padding-top: 3px;
	margin-top: 0px;
}
</style>


 <div class="content">
			  <script>
				$(function ()
				{
					var form = $("#wizard");
					form.validate({
						errorPlacement: function errorPlacement(error, element) { element.before(error); },
						rules: {
							side2: {
							   required: true
							}
							,doors: {
								required: true
								},
							doorsglass: {
								required: true
								}
								//,
							//passwordconfirm: {
							//	equalTo: "#confirmit"
							//}
						},
						  messages: {
							side2: {
							  //minlength: jQuery.format("Zip must be {0} digits in length"),
							  //maxlength: jQuery.format("Please use a {0} digit zip code"),
							  required: "Please select an option to continue."
							}
							,doors: {
								required: "Please Select option to continue"
								},
							doorsglass: {
								required: "Please Select option to continue"
								}
								,
							fname: {
								required: "Please fill in all form fields to continue"
								},
							surname: {
								required: "Please fill in all form fields to continue"
								},
							email: {
								required: "Please fill in all form fields to continue"
								},
							telphone: {
								required: "Please fill in all form fields to continue"
								}
						  }
					});
					form.children("div").steps({
						headerTag: "h3",
						bodyTag: "section",
						transitionEffect: "slideLeft",
						autoFocus: false,
						onStepChanging: function (event, currentIndex, newIndex)
						{
							form.validate().settings.ignore = ":disabled,:hidden";
							if (newIndex < currentIndex) {
									return true;
								}
							return form.valid();

						},
						onStepChanged: function (event, currentIndex, priorIndex)
							{
								// Used to skip the "Warning" step if the user is old enough.
								if (currentIndex === 2)
								{
									$(".etrainfo").fadeIn(500);
								}
								if (currentIndex < 1)
								{
									$('.actions > ul > li:first-child').attr('style', 'display:none');
								}
								else {
									$('.actions > ul > li:first-child').attr('style', '');
									}
								if (currentIndex === 3 || currentIndex < 2)
								{
									$(".etrainfo").fadeOut(500);
								}
								if (currentIndex === 5 )
								{
									$(".restarcalc").fadeIn(500);
									$(".whyweask").fadeIn(300);
								}
								if (currentIndex < 5 )
								{
									$(".restarcalc").fadeOut(500);
									$(".whyweask").fadeOut(300);
								}
								// Used to skip the "Warning" step if the user is old enough and wants to the previous step.

							},
						onFinishing: function (event, currentIndex)
						{
							form.validate().settings.ignore = ":disabled";
							return form.valid();
						},
						onFinished: function (event, currentIndex)
						{
							//alert("Submitted!");
						}
					});
				});
					function checkLogin(email){
					 if(isValidEmailAddress(email)){
						 $.ajax({url: "getvalidateUser.php?email="+email+"&type=check", success: function(result){
								//alert(result);
								if(result == 'Yes'){
									$("#emailLog").val(email);
								}
							}});
						}
					}
					function isValidEmailAddress(emailAddress) {
						var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
						return pattern.test(emailAddress);
					};
					function getUserData(){

						//var byp_email1 = $("#byp_email1").val();
						var email = $("#emailLog").val();
						if(email == ''){
							alert("Please enter Email");
							return false;
						}
						var pass = $("#passwordLog").val();
						if(pass == ''){
							alert("Please enter Password");
							return false;
						}

					 $.ajax({url: "getvalidateUser.php?email="+email+"&password="+pass, success: function(results){
						// alert(results);
							if(results != 'No'){
								var objData = jQuery.parseJSON( results);
								//console.log(results);
								$("#fnameR").val(objData.fName);
								$("#surnameR").val(objData.sName);
								$("#telphoneR").val(objData.telPho);
								$("#addressR").val(objData.address);
								$("#confirmit").val(objData.password);
								$("#passwordconfirm").val(objData.password);
								$("#loginpopup").css("display","none");
								$("#confirmit").attr("disabled","disabled");
								$("#passwordconfirm").attr("disabled","disabled");
							}else{
								alert("Please enter proper detail");
							}
						}});
					}

					function checkLoginData(){
					 var email = $("#emailLog1").val();
						if(email == ''){
							alert("Please enter Email");
							return false;
						}
						var pass = $("#passwordLog1").val();
						if(pass == ''){
							alert("Please enter Password");
							return false;
						}
						if(isValidEmailAddress(email)){
						 $.ajax({url: "getvalidateUser.php?email="+email+"&password="+pass+"&type=login", success: function(result){
								if(result!='No'){
									window.location = "saved-quotes.php";
								}else{
									alert("Please enter valid Data");
								}
							}});
						}else{
							alert("Please enter valid email");
						}
					}
					function checkLoginForgot(email){
						var email = $("#forgetpass").val();
						if(email == ''){
							alert("Please enter Email");
							return false;
						}
						if(isValidEmailAddress(email)){
						 $.ajax({url: "forgetPassword.php?email="+email, success: function(result){
								if(result!='No'){
									alert("Please check your email address, we have sent you a reminder");

								}else{
									alert("Please enter valid Data");
								}
							}});
						}else{
							alert("Please enter valid email");
						}
					}
					// for bi-folds
					$(document).ready(function() {
						$( "#bi-folds" ).click(function() {
							$.ajax({url: "https://www.buildteam.com/byp-get-extension-price.php?doors="+$( "#bi-folds" ).val(),
								success: function(result){
									$("#bi-folds").attr("data-cost", result);
								}
							});
						});
						// for Patio Doors
						$( "#patio-door" ).click(function() {
							$.ajax({url: "https://www.buildteam.com/byp-get-extension-price.php?doors="+$( "#patio-door" ).val(),
								success: function(result){
									$("#patio-door").attr("data-cost", result);
								}
							});
						});
						// for all-glass
						$( "#all-glass" ).click(function() {
							$.ajax({url: "https://www.buildteam.com/byp-get-extension-price.php?doorsglass="+$( "#all-glass" ).val(),
								success: function(result){
									$("#all-glass").attr("data-cost", result);
								}
							});
						});
						// for velux-window
						$( "#velux-window" ).click(function() {
							$.ajax({url: "https://www.buildteam.com/byp-get-extension-price.php?doorsglass="+$( "#velux-window" ).val(),
								success: function(result){
									$("#velux-window").attr("data-cost", result);
								}
							});
						});
						// for addunder
						$( "#addunder" ).click(function() {
							$.ajax({url: "https://www.buildteam.com/byp-get-extension-price.php?additionnal1="+$( "#addunder" ).val(),
								success: function(result){
									$("#addunder").attr("data-cost", result);
								}
							});
						});
						// for removechimney
						$( "#removechimney" ).click(function() {
							$.ajax({url: "https://www.buildteam.com/byp-get-extension-price.php?additionnal2="+$( "#removechimney" ).val(),
								success: function(result){
									$("#removechimney").attr("data-cost", result);
								}
							});
						});
						// for removechimney
						$( "#add-wc" ).click(function() {
							$.ajax({url: "https://www.buildteam.com/byp-get-extension-price.php?additionnal3="+$( "#add-wc" ).val(),
								success: function(result){
									$("#add-wc").attr("data-cost", result);
								}
							});
						});
					});
					// for price
					function addpriceTo(){
						$.ajax({url: "https://www.buildteam.com/byp-get-price.php?depth="+$( "#depth1" ).val()+"&width="+$( "#width1" ).val(),
							success: function(result){
								$("#totalcost").attr("data-cost", parseInt(result));
								//alert(result);
							}
						});
					}
					$( document ).ready(function() {
						$.ajax({url: "https://www.buildteam.com/byp-get-price.php?depth="+$( "#depth1" ).val()+"&width="+$( "#width1" ).val(),
							success: function(result){
								$("#totalcost").attr("data-cost", parseInt(result));
								$("#totalcost").val(parseInt(result));
								//alert(result);
							}
						});
					});

			</script>
			</div>
			</div>
			<div class="byp-start step_one">
				<div class="startinner">
				<div class="byp-start-title">
					<h1 class="byptitle">Online Quote Calculator</h1>
				</div>
				</div>
				<div class="byp-searchpostcode">
					<div class="startinner">
					<div class="inlinestart">
					<div class="bypposcode"><label>Please enter your postcode <input type="text" name="postcode" id="byp_postcode"> </label></div>
					<div id="byp_img_loading" style="display:none;"><img src="img/loading.gif"></div>

					</div>
					<div class="inlinestart">
					<div class="forleftpadding">

						<div class="byptype">
						<label><span class="forinline">What type of property do you own?</span>
							<div class="searchcheck forsearchblock">
								<input name="propertyType" id="house" value="house" type="radio"><span><label for="house">House</label></span>
							</div>
							<div class="searchcheck forsearchblock">
								<input name="propertyType" id="flat" value="flat" type="radio"><span><label for="flat">Flat</label></span>
							</div>
						</label>
						</div>
						<div class="startbtn">
							<script>
								document.write("<a href=\"javaScript:void(0);\" onclick=\"bypStep();\">Start</a>");
							</script>
							<noscript>
								<p style="color: red;"><b><i>Please enable JavaScript to continue</i></b><p>
							</noscript>
						</div>
					</div>
					</div>
					</div>
				</div>

				<div class="bypslideandquote">
				<div class="startinner">
					<div class="newwrapin">
						<div class="startslierleft slideinline">
							<img src="img/slider.jpg">
						</div>
						<div class="startinnnertwo">
							<div class="bypquoteinfo">
								<p>Build Your Price</p>
								<ul>
									<li><span>Quote in under 60 seconds</span></li>
									<li><span>Detailed quote by email</span></li>
									<li><span>Computer generated image of your extension</span></li>
									<li><span>FREE Follow-up Design Consultation by phone or video call</span></li>
									<li style="display:none;"></li>
								</ul>
							</div>
						</div>
					</div>

					<div class="startquotesright slideinline">
					  <h5>LATEST ONLINE QUOTES</h5>
						<ul>
						<marquee>
						<? $sqlQuets = mysqli_query($dbconn,"select postcode,roomsize,totalPrice from byp_Quote order by id DESC limit 8");
							while($rowQ = mysqli_fetch_assoc($sqlQuets)){
						?>
							<li><?=$rowQ['postcode'].' '.$rowQ['roomsize'].' '.$rowQ['totalPrice']?></li>
						<? }?>


						</marquee>
						</ul>
					</div>
					</div>
				</div>

			</div>
		</div>
		<div class="forextraset">
		<div class="mainnew">
		<div class="content"></div>
			<div class="step_two">
		   <div class="pagetitle">
				<h1 class="commonpagetitlenobor">Build Your Price</h1>
		   </div>
			 <div class="restarcalc">

				<a href="/online_quote_calculator.html">
							<span>RESTART CALCULATOR</span>
							<img src="img/illus/restart_img.jpg" alt="">
							</a>
					   </div>

			   <div class="wrapper">
			   <form id="wizard" method="post">
				   <div>
						<h3>What best represents your existing property?</h3>

						<section>
						 <div class="bypfirststep">
							 <div class="titleinfowrap">
								 <p>What best represents your existing property?</p>
								<span>Step 1/6</span>
							</div>
						<div class="forcenterform">
							<div class="checkboximage">
								<label for="existingside">
									 <input id="existingside" checked data-nametype="existing-side-infill"  type="radio" name="side" value="Existing Side Infill" onChange="checkthetype(this);">
									   <div class="iconimage">
											 <span>Existing Side Infill</span>
											<div class="imagein">
												<img src="img/illus/ESI_existing-side infill.jpg">
											</div>

										</div>
								  </label>

							</div>
							 <div class="checkboximage">
								<label for="flattorear">
									<input id="flattorear"  type="radio" data-nametype="flat-to-the-rear" name="side" value="Flat to the rear" onChange="checkthetype(this);">
									<div class="iconimage">
											<span>Flat to the Rear</span>
											<div class="imagein">
												<img src="img/illus/FR_flat-to-the-rear.jpg">
											</div>

										</div>
								  </label>
							</div>
							</div>
				   </div>
						</section>

						<h3>What kind of extension would you like?</h3>
						<section>
							 <div class="bypsecstep">
								<div class="titleinfowrap">
									<p>What kind of extension would you like?</p>
									<span>Step 2/6</span>
								</div>
								<div class="forcenterform">
								<div class="checkfordata existing-side-infill">
								<div class="checkboximage">
									<label for="sideinfill">
										  <input type="radio" class="required" id="sideinfill" name="side2" value="Side Infill">
										   <div class="iconimage">
												<span>Side Infill</span>
												<div class="imagein">
													<img src="img/illus/ESI_side-infill.jpg">
												</div>

											</div>
									  </label>



								</div>
								 <div class="checkboximage">
									<label for="rearext">
										<input type="radio" id="rearext" class="required"  name="side2" value="Rear Extension">
										<div class="iconimage">
												<span>Rear Extension</span>
												<div class="imagein">
													<img src="img/illus/ESI_rear-extension.jpg">
												</div>

											</div>
									  </label>
								</div>
									 <div class="checkboximage">
									<label for="wraparound">
									   <input type="radio" id="wraparound" class="required"  name="side2" value="Wraparound">
										<div class="iconimage">
												<span>Wraparound</span>
												<div class="imagein">
													<img src="img/illus/ESI_wraparound.jpg">
												</div>

											</div>
									  </label>
								</div>
								</div>
								<div class="checkfordata flat-to-the-rear">
									<div class="checkboximage">
										<label for="sideinfill-2">
											  <input type="radio" id="sideinfill-2" class="required" name="side2" value="Side Infill">
											   <div class="iconimage">
													<span>Side Infill</span>
													<div class="imagein">
														<img src="img/illus/FR_side-infill.jpg">
													</div>

												</div>
										  </label>



									</div>
									 <div class="checkboximage">
										<label for="rearext-2">
											<input type="radio" id="rearext-2" class="required"  name="side2" value="Rear Extension">
											<div class="iconimage">
													<span>Rear Extension</span>
													<div class="imagein">
														<img src="img/illus/FR_rear-extension.jpg">
													</div>

												</div>
										  </label>
									</div>

								</div>
								</div>
							</div>
						</section>

						<h3>Please give us the measurements of the finished space you wish to create:</h3>
						<section>
							<div class="bypthirdstep">
								<div class="titleinfowrap">
									<p>Please give us the measurements of the finished space you wish to create:</p>
									<span>Step 3/6</span>
								</div>
								<div class="widthproperty">
								<div class="forcenterform">
									<div class="leftprop">
										<div class="outerimage2">
										<div class="iconimage">
											<div class="imagein">
												<img class="setimagethird" src="img/existing_side.jpg">
											</div>
											<!--<span>Side Infill</span>-->

										</div>
											</div>
									</div>
									<div class="rightprop">
										<div class="rightpropinput">
											<div class="depthbock withinline">
												 <label>Depth <select id="depth1" name="depth"  onchange="addpriceTo();">
													<option value="3">3</option>
													<option value="3.5">3.5</option>
													<option value="4">4</option>
													<option value="4.5">4.5</option>
													<option value="5" selected>5</option>
													<option value="5.5">5.5</option>
													<option value="6">6</option>
													<option value="6.5">6.5</option>
													<option value="7">7</option>
													<option value="7.5">7.5</option>
													<option value="8">8</option>
													<option value="8.5">8.5</option>
													<option value="9">9</option>
													<option value="9.5">9.5</option>
													<option value="10">10</option>
												 </select></label>
											</div>
											 <div class="widthbock withinline">
												 <label>Width<select name="width" id="width1" onchange="addpriceTo();">
													<option value="3">3</option>
													<option value="3.5">3.5</option>
													<option value="4">4</option>
													<option value="4.5">4.5</option>
													<option value="5" selected>5</option>
													<option value="5.5">5.5</option>
													<option value="6">6</option>
													<option value="6.5">6.5</option>
													<option value="7">7</option>
													<option value="7.5">7.5</option>
													<option value="8">8</option>
													<option value="8.5">8.5</option>
													<option value="9">9</option>
													<option value="9.5">9.5</option>
													<option value="10">10</option>
												 </select></label>
											</div>

											<div class="infobock withinline">
												 <a href="javascript:void(0);" onClick="getestimator();">?</a>
												 <div class="sizetimator">
												   <div class="sizetitle">
														<h2>Room Size Estimator</h2>
														<p>How many bedrooms do you have?</p>
														<span class="closeetimator">
															x
														</span>
													</div>
													<div class="sizeoptions">
														<select>
															<option>2</option>
															<option>3</option>
															<option>4</option>
															<option>5</option>
															<option>6</option>
														</select>
														<div class="terracedopt">
															<label><input type="radio" checked value="terraced" name="typehouse">Terraced</label>
															<label><input type="radio" value="detached" name="typehouse">Detached</label>
														</div>
														<div class="basedon">
															<p>Based on this information we estimate your extension should be:</p>
														</div>
														<div class="setimatedsqm">
															<h5><span id="sqmvalue">25</span>&nbsp;SQM</h5>
															<input id="checkused" name="checkused" hidden="hidden" class="sqmvalue" value="0" type="text">
														</div>
													</div>
												</div>
												<div class="overlapsize"></div>
											</div>

										</div>
										<div class="rightpropanimator">
											<div class="floopplanimg">
												<div class="floorplanfuc floorplan_1"><img src="img/floor_plan_1.jpg">
												<div class="floorplaninfo">
													<p class="widthwrap3">Width <span class="widthinfo">5</span>m</p>
													<p class="depthwrap3">Depth <span class="depthinfo">5</span>m</p>
													<p class="totalsqm"><span class="totalsize">25</span> SQM</p>
												 </div>
												</div>
												 <div class="floorplanfuc floorplan_2"><img src="img/floor_plan_2.jpg">
												 <div class="floorplaninfo">
													<p class="widthwrap3">Width <span class="widthinfo">5</span>m</p>
													<p class="depthwrap3">Depth <span class="depthinfo">5</span>m</p>
													<p class="totalsqm"><span class="totalsize">25</span> SQM</p>
												 </div>
												 </div>
												<div class="floorplanfuc floorplan_3"><img src="img/floor_plan_3.jpg">
												<div class="floorplaninfo">
													<p class="widthwrap3">Width <span class="widthinfo">5</span>m</p>
													<p class="depthwrap3">Depth <span class="depthinfo">5</span>m</p>
													<p class="totalsqm"><span class="totalsize">25</span> SQM</p>
												 </div>

												</div>
												<div class="floorplanfuc floorplan_4"><img src="img/floor_plan_4.jpg">
												<div class="floorplaninfo">
													<p class="widthwrap3">Width <span class="widthinfo">5</span>m</p>
													<p class="depthwrap3">Depth <span class="depthinfo">5</span>m</p>
													<p class="totalsqm"><span class="totalsize">25</span> SQM</p>
												 </div>
												</div>
												 <div class="floorplanfuc floorplan_5"><img src="img/floor_plan_5.jpg">
												 <div class="floorplaninfo">
													<p class="widthwrap3">Width <span class="widthinfo">5</span>m</p>
													<p class="depthwrap3">Depth <span class="depthinfo">5</span>m</p>
													<p class="totalsqm"><span class="totalsize">25</span> SQM</p>
												 </div>
												 </div>
																						  </div>

										</div>
									</div>
								</div>

								</div>
							</div>

						</section>

						<h3>What kind of doors and windows would you like?</h3>
						<section>
							<div class="bypfourstep">
							<div class="titleinfowrap">
								<p>What kind of doors and windows would you like?</p>
								<span>Step 4/6</span>
							</div>
								<div class="forcenterform">
							<div class="fourthsteptoptwo">
								<img id="getimageurl" src="">
								<div class="imagewithborderright">
									<div class="checkformain ESI_wraparound">
										<img data-door="bi-folds" data-window="velux-window" class="bi-folds velux-window" src="img/illus/ESI_wrap_bifold-velux.jpg">
										<img data-window="velux-window" data-door="patio-door" class="bi-folds velux-window" src="img/illus/ESI_velux-and-patio_2.jpg">
										<img data-door="patio-door" data-window="all-glass"  class="all-glass patio-door" src="img/illus/ESI_wraparound_all-glass-patio.jpg">
										<img data-door="bi-folds" data-window="all-glass"  class="all-glass patio-door" src="img/illus/ESI_glass-and-bifold.jpg">
										<img data-only-window="all-glass"  class="all-glass patio-door" src="img/illus/ESI_Glass-roof_3.jpg">
										<img data-only-window="bi-folds"  class="all-glass patio-door" src="img/illus/ESI_Bifold.jpg">
										<img data-only-window="patio-door"  class="all-glass patio-door" src="img/illus/ESI_patio_4.jpg">
										<img data-only-window="velux-window"  class="all-glass patio-door" src="img/illus/ESI_Velux_4.jpg">
									</div>


									<div class="checkformain ESI_side-infill">
										<img data-door="patio-door" data-window="all-glass" class="all-glass patio-door"  src="img/illus/ESI_side-infill_all-glass-patio.jpg">
											<img data-door="bi-folds" data-window="velux-window" class="bi-folds velux-window" src="img/illus/ESI_side-infill_bifold-velux.jpg">

											 <img data-door="bi-folds" data-window="all-glass"  class="all-glass patio-door" src="img/illus/ESI_ESI_bi-and-glass.jpg">
											<img data-window="velux-window" data-door="patio-door" class="bi-folds velux-window" src="img/illus/ESI_patio-and-velux.jpg">

											<img data-only-window="all-glass"  class="all-glass patio-door" src="img/illus/ESI_Glass-roof.jpg">
										<img data-only-window="bi-folds"  class="all-glass patio-door" src="img/illus/ESI_bifolds.jpg">
										<img data-only-window="patio-door"  class="all-glass patio-door" src="img/illus/ESI_Patio.jpg">
										<img data-only-window="velux-window"  class="all-glass patio-door" src="img/illus/ESI_Velux.jpg">
									</div>


									<div class="checkformain ESI_rear-extension">
										<img data-door="patio-door" data-window="all-glass" class="all-glass patio-door" src="img/illus/ESI_rear-ext_all-glass-patio.jpg">
											<img data-door="bi-folds" data-window="velux-window" class="bi-folds velux-window" src="img/illus/ESI_rear-ext_bifold-velux.jpg">


											<img data-door="bi-folds" data-window="all-glass"  class="all-glass patio-door" src="img/illus/ESI_ESI_bi-fold-and-glass-roof.jpg">
											<img data-window="velux-window" data-door="patio-door" class="bi-folds velux-window" src="img/illus/ESI_Velux-and-Patio.jpg">
											<img data-only-window="all-glass"  class="all-glass patio-door" src="img/illus/ESI_Glass-roof_1.jpg">
										<img data-only-window="bi-folds"  class="all-glass patio-door" src="img/illus/ESI_bifolds.jpg">
										<img data-only-window="patio-door"  class="all-glass patio-door" src="img/illus/ESI_Patio_1.jpg">
										<img data-only-window="velux-window"  class="all-glass patio-door" src="img/illus/ESI_Velux_1.jpg">
									</div>



									<div class="checkformain FR_side-infill">
										<img data-door="patio-door" data-window="all-glass" class="all-glass patio-door" src="img/illus/FR_side-infill_all-glass-patio.jpg">
											<img data-door="bi-folds" data-window="velux-window" class="bi-folds velux-window" src="img/illus/FR_side-infill_bifold-velux.jpg">


											 <img data-door="bi-folds" data-window="all-glass"  class="all-glass patio-door" src="img/illus/FR_Glass-and-Bi.jpg">
											<img data-window="velux-window" data-door="patio-door" class="bi-folds velux-window" src="img/illus/FR_Velux-and-Patio_1.jpg">
											<img data-only-window="all-glass"  class="all-glass patio-door" src="img/illus/FR_Glass-Roof_2.jpg">
										<img data-only-window="bi-folds"  class="all-glass patio-door" src="img/illus/FR_Bi---Fold_1.jpg">
										<img data-only-window="patio-door"  class="all-glass patio-door" src="img/illus/FR_Patio_3.jpg">
										<img data-only-window="velux-window"  class="all-glass patio-door" src="img/illus/FR_Velux_3.jpg">
									</div>
									<div class="checkformain FR_rear-extension">
										<img data-door="patio-door" data-window="all-glass" class="all-glass patio-door" src="img/illus/FR_rear-ext_all-glass-patio.jpg">
											<img data-door="bi-folds" data-window="velux-window" class="bi-folds velux-window" src="img/illus/FR_rear-ext_bifold-velux.jpg">


											 <img data-door="bi-folds" data-window="all-glass"  class="all-glass patio-door" src="img/illus/FR_Bi-and-Glass-roof.jpg">
											<img data-window="velux-window" data-door="patio-door" class="bi-folds velux-window" src="img/illus/FR_Patio-and-Velux_1.jpg">
											<img data-only-window="all-glass"  class="all-glass patio-door" src="img/illus/FR_Glass.jpg">
										<img data-only-window="bi-folds"  class="all-glass patio-door" src="img/illus/FR_Bi---Fold.jpg">
										<img data-only-window="patio-door"  class="all-glass patio-door" src="img/illus/FR_Patio_2.jpg">
										<img data-only-window="velux-window"  class="all-glass patio-door" src="img/illus/EFR_Velux_2.jpg">
									</div>

								   <!-- <img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_side-infill" src="img/flat_rear.jpg">
									<img data-maincate="ESI_rear-extension" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">
									<img data-maincate="ESI_wraparound" src="img/flat_rear.jpg">-->
								</div>
							</div>
							<div class="fourthsteptoptwo">


								<div class="foursteptwo">
									<div class="checkboximage">
										<label for="bi-folds">
											 <input type="radio" id="bi-folds" data-cost="0" name="doors" value="Bi-folds">
											   <div class="iconimage">
												  <span>Bi-folds</span>
												</div>
										  </label>
									</div>
									<div class="ordiv">
										<span>OR</span>
									</div>
									<div class="checkboximage">
										<label for="patio-door">
											<input type="radio" id="patio-door" data-cost="-1500" name="doors" value="Patio Doors">
											   <div class="iconimage">
												 <span>Patio<br>Doors</span>
												</div>
										  </label>
									</div>
								</div>
								<div class="foursteptwo">
									<div class="checkboximage">
										<label for="all-glass">
											 <input type="radio" id="all-glass" name="doorsglass" data-cost="3000" value="All-glass">
											   <div class="iconimage">
												   <span>All-glass</span>
												</div>
										  </label>
									</div>
									 <div class="ordiv">
										<span>OR</span>
									</div>
									<div class="checkboximage">
										<label for="velux-window">
											 <input type="radio" id="velux-window" data-cost="0" name="doorsglass" value="Velux Window">
											   <div class="iconimage">
												  <span>Velux<br>Window</span>
												</div>
										  </label>
									</div>
								</div>

								</div>
							</div>

							</div>
						</section>

					   <h3>What additional items would you like to include?</h3>
						<section>
						   <div class="titleinfowrap">
								<p>What additional items would you like to include?</p>
								<span>Step 5/6</span>
							</div>
							<div class="bypfifthstep">
							 <div class="widthproperty">
								<div class="forcenterform">
									<div class="leftprop">
										<div class="outerimage2">
										<div class="iconimage">
											<div class="imagein">
												<img class="setimagethird" src="img/existing_side.jpg">
											</div>
										   <!-- <span>Side Infill</span> -->

										</div>
											</div>
									</div>
									<div class="rightprop">
										<div class="rightpropinput">
											<div class="checkboximage">
												<label for="addunder">
													 <input id="addunder" class="add_heating" data-additional="add_underfloor" type="checkbox" name="additionnal1" value="Heating" data-cost="1500" onclick="">
													   <div class="iconimage">
														   <span>Add<br>Underfloor<br>Heating</span>
														</div>
												  </label>
											</div>
											<div class="checkboximage">
												<label for="removechimney">
													  <input id="removechimney" class="remove_chimney" data-additional="remove_chimney" type="checkbox" name="additionnal2" data-cost="2000" value="Remove a Chimney" onclick="">
													   <div class="iconimage">
															<span>Remove a<br>Chimney</span>
														</div>
												  </label>
											</div>
											<div class="checkboximage">
												<label for="add-wc">
													  <input id="add-wc" data-cost="2000" class="add_wc" type="checkbox" data-additional="add_wc" name="additionnal3" value="Add a WC" onclick="">
													   <div class="iconimage">
														  <span>Add a WC</span>
														</div>
												  </label>
											</div>




										</div>
										<div class="rightpropanimator">
											<div class="floorimage">
												<img class="no_option" src="img/floor_no_option.jpg" alt="">
												</div>
												 <div class="floorimage2">
												<img data-add="add_wc,remove_chimney" class="four-4" src="img/floor_add_wc_remove_chimney.jpg" alt="">
												<img class="one-1" src="img/floor_heating.jpg" alt="">
												<img class="five-5" src="img/floor_heating_WC.jpg" alt="">
												<img class="two-2" src="img/floor_remove_chimney.jpg" alt="">
												<img class="six-6" src="img/floor_remove_chimney_heating.jpg" alt="">
												<img class="three-3" src="img/floor_WC.jpg" alt="">
												<img class="all" src="img/floor_WC_chimney_heating.jpg" alt="">
											</div>
										</div>
									</div>
								</div>

								</div>
							</div>

						</section>

					   <h3>Please fill out a few details to proceed</h3>

						<section>
							<div class="titleinfowrap">
								<p>Please fill out a few details to proceed</p>
								<span>Step 6/6</span>
							</div>
							<div class="bypsixthstep">
							 <div class="forcenterform">
							<div class="formthreefeild">
								<div class="bypformfield fortectindent">
									<input type="text" name="fname" value="<?=$_POST['fName']?>" id="fnameR" class="required" placeholder="First Name*">
								</div>
								<div class="bypformfield fortectindent">
									<input type="text" name="surname" value="<?=$_POST['sName']?>" id="surnameR" class="required" placeholder="Surname*">
								</div>
								<div class="bypformfield fortectindent">
									<input id="byp_email1" type="email" name="email"  value="<?=$_POST['email']?>" onChange="checkLogin(this.value);" onKeyup="checkLogin(this.value);" class="required" placeholder="Email Address*">
								</div>
							</div>
							<div class="formtwofeild">
								<div class="bypformfield fortectindent">
									<input type="text" class="required"  name="telphone" value="<?=$_POST['telphone']?>" id="telphoneR" placeholder="Telephone*" maxlength=11>
								</div>
								<div class="bypformfield fortectindent">
									<input type="text" class="required postcodeval" id="postcodeval" placeholder="Postcode*">
								</div>
							</div>
							<div class="formtwofeild">
								<div class="bypformfield fortectindent">
									<input type="text" name="address" value="<?=$_POST['address']?>" id="addressR" placeholder="Address">
								</div>
								<div class="bypformfield fortectindent">
									<input type="textarea" name="comment" placeholder="Comments">
								</div>

							</div>
							<div class="formtwofeild">
								<div class="bypformfield fortectindent">
									<select name="lookingStart" placeholder="">
										<option value="" selected disabled>When are you looking to start?</option>
										<option value="Immediately">Immediately</option>
										<option value="1-3 months">1-3 Months</option>
										<option value="3-6 Months">3-6 Months</option>
										<option value="1 Year">1 Year</option>
									</select>
								</div>
								<div class="bypformfield fortectindent">

									<select name="currentstatus" class="required">
										<option value="" selected disabled>Please describe your current status*</option>
										<option value="Pre-Purchase">Pre-Purchase</option>
										<option value="Just Moved In">Just Moved In</option>
										<option value="Resident For Less Than 1 Year">Resident For Less Than 1 Year</option>
										<option value="Resident For More Than 1 Year">Resident For More Than 1 Year</option>
									</select>
									<input type="text" hidden="hidden" class="postcodeval" name="addPostcode" placeholder="Postcode*">
									<input type="text" hidden="hidden" id="proptype" name="proptype">
									<input type="text" hidden="hidden" id="territory" name="territory">
									<input type="text" hidden="hidden" id="totalcost" name="totalcost">
								</div>

							</div>
							<div class="formthreefeild">
								<div class="bypformfield">
									<input class="disabled" id="submitquote" type="submit" value="Show my quote" disabled="disabled">
								</div>
							</div>
							<div class="viewexample">
								<a href="/images/byp/example_quote.pdf" target="_blank">View Example quote</a>
							</div>
							<div class="formonefield">
								<p>By clicking on the Show my quote button you confirm that you agree to our <a href="#" id="termscondition">terms and conditions</a> statement and you have read and understand the <a id="privacyid" href="#">privacy policy</a></p>
							</div>
							<script>
							$('#telphoneR').on('input', function() {
								var input=$(this);
								var re = /^0[0-9]{10}$/;
								var is_valid=re.test(input.val());
								if(is_valid){
									input.removeClass("error");
									$('#submitquote').removeAttr('disabled').removeClass('disabled');
								}
								else{
									input.addClass("error");
									$('#submitquote').addClass('disabled').prop("disabled", true);
								}
							});
							/*$('#submitquote').mouseenter(function() {
								var a = $(this);
								if ($('.postcodeval').val().length > 1 && $('#telphoneR').val().length == 11) $(this).removeAttr('disabled');
							});*/
							
							</script>
							</div>
							</div>
						</section>

					</div>
				</form>
				</div>

				</div>
				<div class="whyweask">
					<div class="askinner">
						<p>Why we ask for this information?</p>
					</div>
				</div>
				<div id="whyweask" class="commonnewpopup">
					<div class="newpoupinner">
						<a class="closeiitpopup" href="javascript:void(0);">x</a>
						<h1>Why we ask for this information?</h1>
						<div class="popupcontentmiddle">
									<p>You will get a personalised PDF of your quotation sent directly to your email.</p>
									<p>Your personal information will only be used as set out in Build Team's privacy policy.</p>
									<p>Our team may use your information to contact you about your Side Return needs. This service can help you at both the Design and Build phase of the project, including costs, design options and planning considerations.</p>
									<p>We may also contact you via post, phone or email. You can unsubscribe at any time by clicking on the unsubscribe link or by using the contact details in our privacy policy.</p>
								</div>
					</div>
				</div>
				<div id="posswordinfo" class="commonnewpopup">
								<div class="newpoupinner">
									<a class="closeiitpopup" href="javascript:void(0);">x</a>
									<h1>Password - Why we ask for this information?</h1>
									<div class="popupcontentmiddle">
										<p>We keep a record of all the quotes you formulate. You can view these by logging in to our 'Retrieve a saved quote' portal. A link to this can be found on: <a href="#">https://www.buildteam.com/build-your-price-start.html</a> All you are required to enter is your username (email address) and password.</p>
									</div>

									</div>
							</div>
				<div id="termpopup" class="commonnewpopup">
								<div class="newpoupinner">

								<a class="closeiitpopup" href="javascript:void(0);">x</a>
								<h1>Terms and conditions</h1>
								<div class="popupcontentmiddle">
									<p>Prices are subject to the addition of VAT at the prevailing rate.</p>
									<p>The estimate is provided on the basis of the information given.  All figures are presented on a best endeavours basis and subject to a confirmatory quotation from Build Team.  The estimate should be regarded as an approximation of cost and not relied upon for property acquisition or budgetary decision making.</p>
								</div>

								</div>
							</div>
							<div id="policypopup" class="commonnewpopup">
							<div class="newpoupinner">
								<a class="closeiitpopup" href="javascript:void(0);">x</a>
								<h1>Privacy policy</h1>
								<div class="popupcontentmiddle">
									<p>Build Team is committed to safeguarding the privacy of our users while providing the highest possible quality of service. We will only use the information that we collect about you lawfully (in accordance with the Data Protection Act 1998).</p>
									<p>We will not pass your information to any third parties.</p>
									<a class="moreurl" href="./privacy-policy.html" target="_blank">More info</a>
								</div>

								</div>
							</div>
							<div id="learnmorepopup" class="commonnewpopup">
							<div class="newpoupinner">
								<a class="closeiitpopup" href="javascript:void(0);">x</a>

								<div class="popupcontentmiddle">
									<div class="bottomextra">
										<div class="gifimage">
											<img src="img/full_size_3.gif" alt="">
										</div>
										<h1>Finished Room size</h1>
										<p>All of our extensions are based on the total finished room size, which does include the existing room and the newly extended area. The reason we calculate on this SQM rate is because it's important to properly connect new with the old, so we completely refurbish the room to ensure the connection is seamless.</p>
									</div>
								</div>

								</div>
							</div>
				<div class="etrainfo">
								<div class="innerextra">
									<div class="topextra">
									<p>We are looking for the finished room size which includes the existing & extended area. <span class="moreinfo">Learn more</span></p>
									<!--<span class="closeextra">x</span>-->
									</div>

								</div>
							</div>

			   </div>

			   </div>
				<div id="loginpopup" class="commonnewpopup">
							<div class="newpoupinner">
								<a class="closeiitpopup" href="javascript:void(0);">x</a>
								<h1>Login</h1>
								<form action="">
								<div class="popupcontentmiddle">
								   <input type="email" id="emailLog" placeholder="Email">
								   <input type="password" id="passwordLog" placeholder="Password">
								   <input type="button" value="Submit" onclick="getUserData();">
								</div>

								</form>
								<div class="forgotpass">
									<a href="javascript:void(0);">Forget Password?</a>
								</div>
								</div>
							</div>
				<div id="loginpopup1" class="commonnewpopup">
					<div class="newpoupinner">
						<a class="closeiitpopup" href="javascript:void(0);">x</a>
						<h1>Login</h1>
						<form action="">
						<div class="popupcontentmiddle">
						   <input type="email" id="emailLog1" placeholder="Email">
						   <input type="password" id="passwordLog1" placeholder="Password">
						   <input type="button" value="Submit" onclick="checkLoginData();">
						</div>

						</form>
						<div class="forgotpass">
							<a href="javascript:void(0);">Forget Password?</a>
						</div>
					</div>
				</div>
				<div id="forgetpopup" class="commonnewpopup">
					<div class="newpoupinner" id="loginpopup1">
						<a class="closeiitpopup" href="javascript:void(0);">x</a>
						<h1>Forget Password?</h1>
						<form action="">
						<div class="popupcontentmiddle">
						   <input type="email" id="forgetpass" placeholder="Email">
						   <input type="button" value="Submit" onclick="checkLoginForgot();">
						</div>

						</form>
						<div class="knowpass">
							<a href="javascript:void(0);">Login</a>
						</div>
					</div>
				</div>

		</div>
<script src="byp-css/jsbyp.js?2"></script>
<?php require_once('./inc/footer_2.inc.php');

/** CRM **/

require_once($_SERVER["DOCUMENT_ROOT"]."/crm_config.php");

$full_name = $_POST['fname'].' '.$_POST['surname'];

list($name,  $last_name) = explode(' ',$full_name);

$now = date("d.m.Y H:i:s");

$param1 = "depth ".$_POST['depth'];
$param1.= "\r\n width ".$_POST['width'];

$doors = $_POST['doors'];
$doors.= "\r\n ".$_POST['doorsglass'];

$addit = '';

foreach(range(1,3) as $val) {

   $addit.= $_POST['additionnal'.$val].", ";

}

$data = array(
	"NAME" =>  $name,
	"LAST_NAME" => $last_name,
	"UF_CRM_1490175334" => $now,
	"EMAIL_HOME" => $_POST['email'],
	"PHONE_HOME" =>$_POST['telphone'],
	"SOURCE_DESCRIPTION" => "BT",
	"TITLE" => $full_name,
	"UF_CRM_1506432178" => $_POST['comment'],
   "UF_CRM_1506432431" => $_POST['addPostcode'],
	"ADDRESS" => $_POST['address'] ,
	"UF_CRM_1516184576" => $_POST['proptype'],
	"UF_CRM_1516184591" => $_POST['side'] ,
	"UF_CRM_1516184604" => $_POST['side2'] ,
	"UF_CRM_1516184650" => $param1,
	"UF_CRM_1516184674" => $doors,
	"UF_CRM_1516184686" => $addit,
	"UF_CRM_1516184707" => $_POST['lookingStart'],
	"UF_CRM_1516184716" => $_POST['currentstatus'],
 );

CrmClient::$SOURCE_ID = 2;

if(!CrmClient::sendToCRM($data) ) {
 //   echo 'error';
}

/** CRM **/

?>
