<?php

require_once('./inc/header.inc.php');

?>
  	<div class="full">
    	<?php
			
			echo $bc_trail;
			
			?>
    	<h1>Testimonials</h1>
 
      
      <div class="col_half">
        <img src="/images/testimonials_res.jpg" style="width: 100%" />
      </div>
      <div class="tv_video col_half col_last">
        <iframe src="//player.vimeo.com/video/99189301?title=0&amp;byline=0&amp;portrait=0" width="447" height="251" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
      </div>
      
      <div class="col_full">
        <p>Building refurbishment in London can be a hassle if done by the wrong London builders or interior designers. To make sure you are hiring a London contractor with confidence, we are providing you with customer testimonials for various projects we have attended to. We have experience with London property management as well as improving house designs to maximise space through the design and build of house extensions. The solutions we provide are for anybody interested in loft conversions, basement conversions, side return kitchen extensions, and home extensions in general. Please read about our clients’ experience with our one-stop-shop solution and what they think of our kitchen design ideas and loft conversion ideas.</p>
      </div>

		<div class="col_half">
		<?php
    $i = 0;
    $rs = getRs("SELECT testimonial_id, quote, name, title FROM testimonial WHERE is_active = 1 AND is_enabled = 1 ORDER BY sort");
    $total = mysqli_num_rows($rs);
    $half = false;
    while ( $row = mysqli_fetch_assoc($rs) ) {
      $i++;
      
      echo '<p><div class="block-quote">"' . $row['quote'] . '"<br /><b style="padding-top: 5px; font-size: 12px; display: inline-block;">' . $row['name'] . ', ' . $row['title'] . '</b></div></p>';
      
      if ($i >= $total/2 && !$half) {
        echo '</div><div class="col_half col_last">';
        $half = true;
      }
      
     
    }
    
    ?>
    </div>
    
    <div class="col_full">
	<div class="darkblue-box" style="margin-right: 10px;">
		find out<br>
		<span class="orange">more</span></div>
	<div class="lightblue-box" style="margin-right: 10px;">
		<a href="/what-we-do/guarantees-and-insurance-page.html">10 year<br>
		<span class="orange">guarantee</span></a></div>		
		<div class="lightblue-box" style="margin-right: 10px;"><a href="/about-us/guarantees-and-insurance-page.html">
		completion<br><span class="orange">pack</span></a></div>
		
		
	<div class="lightblue-box" style="margin-right: 10px;"><a href="/build-your-price-start.html">
		build your<br>
		<span class="orange">price</span></a></div>
			<div class="lightblue-box"><a href="/what-we-do/process.html">
		the <span class="orange">8 step</span><br>
		process</a></div>
	</div>

		</div>
<?php

require_once('./inc/footer.inc.php');

?>