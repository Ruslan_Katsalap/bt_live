<?php

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    	<?php
			
			echo $bc_trail;
			
			?>
    	<h1>Quality</h1>
      <p><i>"The closer you get, the better it should become"</i></p>
      <p>Quality is very important to us a Build Team and our aim is to provide the best quality craftsmanship to all of our clients no matter how big or small the project may be.</p>
      <p>Quality lies at the heart of the ethos of Build Team and we define quality as:</p>
      <ul>
      	<li>Design interpretation on site</li>
        <li>The form, function and application of materials and finishes on site</li>
        <li>The adherence to project timeframe and budget</li>
      </ul>
      <p>On our larger projects we appoint a quality assurance board, whose job it is to make sure all aspects of quality are met from design interpretation through to the finish on site, including adherence to the project programme.</p>
      <p>Our Customers whether private or commercial will receive:</p>
      <ul>
      	<li>Value for money</li>
        <li>Professional and courteous service</li>
        <li>Honest and effective advice</li>
        <li>Long term and continued support</li>
      </ul>
      <p>At Build Team we constantly review existing products and prices to guarantee we can offer clients the best value for money. And all our team live by the motto: "The closer you get, the better it should become".</p>
      <p>Finally, where there are quality issues reported, we undertake a full and thorough investigation and ensure the correct remedial action is taken.</p>
		</div>
    <div class="right">
    	<img src="/images/quality01.jpg" width="326" height="436" alt="BuildTeam" title="BuildTeam" />
    </div>
<?php

require_once('./inc/footer.inc.php');

?>