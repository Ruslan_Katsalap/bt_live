<?php

global $a_settings;

require_once('./inc/settings.php');

@session_start();

if (!isset($_SESSION[WISE_DB_NAME]['thank_you']['product_code'])) {
  header('Location: /getting-started/book-site-visit.html');
  exit();
}

$e_visit_id = str_pad($_SESSION[WISE_DB_NAME]['thank_you']['visit_id'], 5, '0', STR_PAD_LEFT);

$e_visit_id = htmlentities($e_visit_id, ENT_QUOTES, 'UTF-8');

$e_name = htmlentities($_SESSION[WISE_DB_NAME]['thank_you']['full_name'], ENT_QUOTES, 'UTF-8');

$e_postcode = htmlentities($_SESSION[WISE_DB_NAME]['thank_you']['postcode'], ENT_QUOTES, 'UTF-8');

$e_availibility = nl2br(htmlentities($_SESSION[WISE_DB_NAME]['thank_you']['availibility'], ENT_QUOTES, 'UTF-8'));

$price = $_SESSION[WISE_DB_NAME]['thank_you']['price'];

$e_visit_name = $a_settings['a_visit_types'][ $_SESSION[WISE_DB_NAME]['thank_you']['product_code'] ]['name'];

$e_visit_name = htmlentities($e_visit_name, ENT_QUOTES, 'UTF-8');

require_once('./inc/header.inc.php');
?>

  <div class="full thank_you-page">
		
    <div class="col_two_third book-visit">
    
      <div id="bc"><a href="/">Home</a> &rsaquo; <a href="/getting-started/book-site-visit.html">Book a Site Visit</a> &rsaquo; <b><?php echo $a_settings['a_visit_types'][  $_SESSION[WISE_DB_NAME]['thank_you']['product_code']  ]['name'] ?> Visit Booked</b>
      </div>
      
      <h1>Confirmation</h1>
      
      <div class="book-wrap">
        <div class="col-sm-12 confirm_box">
          <p>Dear <?php $fname = explode(' ', $e_name); echo $fname[0]; ?></p>
          <p></p>
          <p>
            Thank you for booking your <?php echo $e_visit_name ?> visit. A member of our team will be in contact within 1 working day to confirm your site visit based on your preferences listed below<?php 
          if ($price) {
            echo ', and take payment for the visit'; // &pound;'.$price.' (&pound;'.round($price/1.2).' + VAT)
          }
          ?>.</p>
          <p></p>
          <p>Kind regards,</p>
          <p></p>
          <p>Build Team</p>

          <h2>Reference Number : <?php echo $e_visit_id ?></h2>
        </div>
        <div class="book-summary">
          <div class="col-sm-6">
            <p><em>Name:</em> <span><?php echo $e_name ?></span></p>
            <p><em>Postcode:</em> <span><?php echo $e_postcode ?></span></p>
            <p><em>Dates selected:</em> <span><?php echo $e_availibility ?></span></p>
          </div>
        </div>

      </div>
      
    </div>

    <div class="col_one_third col_last">
      <div class="v_progress">
          <h3>how it works</h3>
          <div class="step">
            <h4>choose</h4>
            <p>you select dates that are convenient for you</p>
          </div>
          <div class="step">
            <h4>confirm</h4>
            <p>confirmation of your site visit request</p>
          </div>
          <h4>site visit</h4>
          <p>a member of our team will contact you to schedule the visit <?php if ($_SESSION[WISE_DB_NAME]['thank_you']['product_code'] != 'standard') : ?>and arrange payment<?php endif; ?></p>
      </div>
    </div>
    
  </div>
<?php

require_once('./inc/footer.inc.php');
  
?>
      
      