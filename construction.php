<?php

require_once('./inc/header.inc.php');

?>
  	<div class="left">
    	
			<?php echo $bc_trail; ?>
      
      
    	<h1>Construction</h1>
      <p>When it comes to construction, Build Team's managerial and communication skills with clients, consultants and architect's have proven to be one of our greatest assets. Listening and understanding has been a core belief that trademarks us and helps the ideas, the changes and problems ease the project along its timeline in order to keep us all within time and budget.</p>
		</div>
    <div class="right">
    	<img src="/images/ourexperience03.jpg" width="326" height="436" alt="BuildTeam" />
    </div>
<?php

require_once('./inc/footer.inc.php');

?>