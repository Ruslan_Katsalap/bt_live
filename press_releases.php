<?php
require_once('./inc/util.inc.php');
// Pagination custom
include './inc/my_pagina_class.php';

error_reporting(E_ALL ^ E_NOTICE);
error_reporting(E_ERROR | E_PARSE);
error_reporting(0);

class Pagina extends MyPagina {
  function __Pagina() {
    // to prevent DB connection new creation
  }
}

define('QS_VAR', 'p'); // the number of records on each page
define('NUM_ROWS', 20); // the number of records on each page
define('STR_FWD', '&raquo;'); // the string is used for a link (step forward)
define('STR_BWD', '&laquo;'); // the string is used for a link (step backward)
define('NUM_LINKS', 10); // the number of links inside the navigation (the default value)
$nav = new Pagina;
$navv = new Pagina;

$show_detail = false;

$url_code = '';

$title = '';

$imagefile = '';

if ( isset($_GET['c']) ) {

	$c = safe($_GET['c']);

	$rs = getRs("SELECT id, url_code, title, imagefile, pdffile, date_release, time_release, body FROM press_release WHERE is_enabled='1' AND url_code = '" . formatSql($c) . "'");
	


	while ( $row = mysqli_fetch_assoc($rs) ) {

		$show_detail = true;

		$url_code = $row['url_code'];

		$title = $row['title'];

		$imagefile = $row['imagefile'];
    
    	$pdffile = $row['pdffile'];

		$body = $row['body'];

		$meta_title = $row['title'];
		
		$dr = $row['date_release'];
		$tr = $row['time_release'];

	}
  
  unset($row);
}



require_once('./inc/header.inc.php');



if ( $show_detail == false ) {



?>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
  	<div class="full press_release">

    	

			<?php //echo $bc_trail; ?>

<style>
.download
{
    float: left;
    margin-top: 2px;
}

a
{
	display:block;
	position:relative;
}
.press_release .col_half
{
	margin-right:0px !important;
	width:50% !important;
}


.press_release .col_half h1 {
    margin-bottom: 5px;
    padding-bottom: 0px;
    margin-top: 0px;
    padding-top: 0px;
}

.press_release .col_half h1 a {
    color: #50799c !important;
    text-transform: none !important;
}

.press_release .col_half .date
{
    font-size: 15px;
    color: #96999e;
    font-weight: bold;
    margin-bottom: 10px;
}

.press_release .col_half h2
{
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    border-bottom: 2px solid #d9d9d9 !important;
	    color: #003c70 !important;
}

.press_release .col_half .brief {
    color: #9ea2a6 !important;
    font-size: 15px !important;
    font-weight: normal !important;
    display: inline !important;
    text-align: justify !important;
    width: 100% !important;
    line-height: 20px !important;
    float: left;
    margin-bottom: 10px;
}
.press_release .col_half .brief a {
	 color: #9ea2a6 !important;
	}
.press_release .col_half a
{
    color: #ff8844 !important;
}
.press_release .download a
{
	font-size:16px;
}
.readmore a {
	font-size:16px;
	display: block;
    top: 7px;
	}
ul.press_releases {
    list-style: none;
    margin: 00px 0px;
    padding: 0;
    float: left;
    display: block;
	    margin-top: -10px;
}

.full.press_release {
    width: 100%;
    float: left;
    display: block;
	overflow:hidden;
}
.full.press_release .col_half img
{
    width: 100%;
    float: left;
}

@media (min-width:480px) {
	ul.press_releases {
	 width: 1010px;
     left: -10px;
     position: relative;
		}
	}

@media only screen and (max-width:480px){
.press_release .col_half,
.col_one_fourth	{
    width: 90% !important;
    float: left;
    left: 5%;
	height:auto !important;
    padding-left: 0px !important;
	margin-bottom: 10px !important;
}
.col_one_fourth	{
	margin-bottom: 0px !important;
}

.press_releases
{
 margin-top: 0px !important;
}

.press_release h2 {
    width: 90% !important;
    margin-left: 5% !important;
}

.rule {
    width: 90% !important;
    margin: 15px auto;
	float:left !important;
	margin-left: 5% !important;
}



}
</style> 	

     <h2>Press Coverage</h2>
	 
	 
	 <?php 
	 
	 $navv = new Pagina;

	$show_detail = false;
	
	$url_code = '';
	
	$title = '';
	
	$imagefile = '';

		$sqll = "SELECT id, url_code, title, imagefile, pdffile, body, date_release, time_release FROM press_release WHERE is_enabled='1' ORDER BY date_release DESC, time_release DESC LIMIT 1";
        
        /////////////////////////////////////////////////////
        $navv->sql = $sqll; // the (basic) sql statement (use the SQL whatever you like)
       $rss = mysqli_query($dbconn,$sqll); // result set
       //if(mysqli_num_rows($rss)>0){
	   while ( $roww = mysqli_fetch_assoc($rss) ) {
			echo '<div class="col_half" style="text-align:center">';
          
          		if ($roww['imagefile']) {
          
				if (strpos($roww['imagefile'], 'videothumb')) {
				  echo '<a href="https://vimeo.com/112830340" target="_blank">';
				}
				else { 
				  echo '<a href="/press_releases/' . $roww['url_code'] . '.html">';
				}  
            $imgurl1 = $roww['imagefile'];
           echo '<img src="/media/'.$imgurl1.'" alt="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '" title="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '"  /></a>';
                    
           
          }
          
          echo '</div><div class="col_half" style="margin-right: 0px !important;padding: 0px 0px 0px 20px;box-sizing: border-box !important;"><h1><a href="/press_releases/' . $roww['url_code'] . '.html" title="' . htmlspecialchars($roww['title'], ENT_QUOTES, 'UTF-8') . '">' . htmlspecialchars($roww['title'], ENT_QUOTES, 'UTF-8') . '</a></h1><div class="date">';

					if ( $roww['date_release'] > 0 ) {
            
            $roww['time_release'] = trim($roww['time_release']);
            
            $date_release = $roww['date_release'];
            
            $date_release = date('Y-m-d', $date_release);
            
            if ($roww['time_release']) $date_release .= ' '.$roww['time_release'];
            
            $date_release = strtotime($date_release);
            
					  $date_release = gmdate('d M Y', $date_release);
            
            //if ($date_release) $date_release .= ' GMT';
            
            echo $date_release;
					}
          
          $brief = strip_tags($roww['body']);
          $brief = substr($brief, 0, 500);
          $brief = explode(' ', $brief);
          array_pop($brief);
          $brief = trim(implode(' ', $brief), ',-!?');
		  echo '</div><div class="brief"><a href="/press_releases/' . $roww['url_code'] . '.html" title="' . htmlspecialchars($roww['title'], ENT_QUOTES, 'UTF-8') . '">'.$brief.'...</a></div>'.($roww['pdffile']?'<br></br><div class="readmore"><a href="/press_releases/' . $roww['url_code'] . '.html" title="' . htmlspecialchars($roww['title'], ENT_QUOTES, 'UTF-8') . '">Read more</a></div><br></br><div class="download"><a href="/pdf/'.$roww['pdffile'].'" target="_blank">Download PDF</a></div>':'').'</div>';

        }
	  // }
	 ?>
	 
	 

	 

      <ul class="press_releases">
<div class="rule"></div>
				<?php

        $sql = "SELECT id, url_code, title, imagefile, pdffile, body, date_release, time_release FROM press_release where id != (SELECT MAX(id) from press_release) AND is_enabled='1' ORDER BY date_release DESC, time_release DESC ";
        
        /////////////////////////////////////////////////////
        //$nav->sql = $sql; // the (basic) sql statement (use the SQL whatever you like)
        //$rs = $nav->get_page_result(); // result set
        //$num_rows = $nav->get_page_num_rows(); // number of records in result set 
        //$nav_links = $nav->navigation(' | ', 'page_style'); // the navigation links (define a CSS class selector for the current link)
        //$nav_info = $nav->page_info('to'); // information about the number of records on page ("to" is the text between the number)
        //$simple_nav_links = $nav->back_forward_link(); // the navigation with only the back and forward links
        //$total_recs = $nav->get_total_rows(); // the total number of records
        
        // if ($this->is_tpl_exists) $this->nav->free_page_result();

        ///////////////////////////////////////////////////////////////////////
     
		
		
		$nav->sql = $sql; // the (basic) sql statement (use the SQL whatever you like)
       $rs = mysqli_query($dbconn,$sql); // result set
		
        
        //$i = 0;
        
		//if (isset($_GET[QS_VAR])) $i = $_GET[QS_VAR] * NUM_ROWS;
       
	    while ( $row = mysqli_fetch_assoc($rs) ) {
			
          //$i++;
          
          
          echo '<div class="col_one_fourth"><div class="press-thumbnail">';
          if ($row['imagefile']) {
          
            if (strpos($row['imagefile'], 'videothumb')) {
              echo '<a href="https://vimeo.com/112830340" target="_blank">';
            }
            else { 
              echo '<a href="/press_releases/' . $row['url_code'] . '.html">';
            }  
            
           $imgurl = $row['imagefile'];
           echo '<img src="/media/'.$imgurl.'" alt="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '" title="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '" width="315"  /></a>';
          }
          
          echo '<div class="press-title"><a href="/press_releases/' . $row['url_code'] . '.html" title="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '">' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '<div class="date">';

			if ( $row['date_release'] > 0 ) {
            
            $row['time_release'] = trim($row['time_release']);
            
            $date_release = $row['date_release'];
            
            $date_release = date('Y-m-d', $date_release);
            
            if ($row['time_release']) $date_release .= ' '.$row['time_release'];
            
            $date_release = strtotime($date_release);
            
			$date_release = gmdate('d M Y', $date_release);
            
            //if ($date_release) $date_release .= ' GMT';
            
            echo $date_release.'  <img src="../images/right-arrow.jpg" style="height:10px;width:auto;margin-top:10px;">';
					}
          
          $brief = strip_tags($row['body']);
          $brief = substr($brief, 0, 250);
          $brief = explode(' ', $brief);
          array_pop($brief);
          $brief = trim(implode(' ', $brief), ',-!?');
		  echo '</div><div class="brief">'.$brief.'...</div>'.($row['pdffile']?'<div class="download"></div>':'').'</a></div></div></div>';

        }

        

        ?>
  <!--<span class="message"></span>-->
  <span class='moreless'></span>
			</ul>
      
      <div style="margin-top:1em;font-size:1.1em;text-align:center;width:100%;float:left;clear:both;" class="link-navs">
      <?php echo $nav_links ?>
      </div>
      
    </div>

	
	
	
	

	
	

	
	
	<style>
	
	
	.moreless {
    height: 50px;
    border-bottom: 3px solid #e5e5e5;
    width: 100%;
    float: left;
    clear: both;
    margin-bottom: -15px;
}
.mbutton {
    height: 30px;
    border: 3px solid #eaeaea;
    width: 100px;
    margin: auto;
    display: table;
    position: relative;
    bottom: -20px;
    border-bottom: 0px;
    background: #fff;
    text-align: center;
    color: rgba(0, 60, 112, 0.62);
    font-size: 18px;
    font-weight: bold;
    letter-spacing: 1px;
    line-height: 30px;
	cursor:pointer;
}
	
	
	.link-navs a
	{
		display:inline !important;
	}
	
	.col_one_fourth {
    width: 31%;
    margin-right: 1% !important;
    margin-left: 1% !important;
    margin-bottom: 0px;
    height: auto;
	//display:none;
}
	
	.press-thumbnail
	{
		//height:150px;
		width:100%;
		
		position:relative;
	}
	
	.press-title {
    position: absolute;
    bottom: 0px;
    padding: 10px 0px;
    text-align: center;
    background: rgba(255,255,255,0.8);
    width: 100%;
    font-size: 16px;
    line-height: 18px;
    font-weight: normal;
}
	
	.press_releases .col_one_fourth img
	{
		width:100%;
	}
	
	.press_releases .col_one_fourth .date
	{
		height:0px;
		overflow:hidden;
		color:#525252;
	}
	
	.press_releases .col_one_fourth .brief,
	.press_releases .col_one_fourth .download
	{
		display:none;
	}
	
	
		.press_release h2
{
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    border-bottom: 2px solid #d9d9d9 !important;
	    color: #003c70 !important;
}
.rule
{
	height:1px;background: #e8e8e8;width:100%;float:left;margin:15px 0px
}



	</style>
	
	
<?php

}



else {



define('hidePageMedia', true);




?>
<style>

.single-press .col_half
{
	margin-right:0px !important;
	width:50% !important;
}

.single-press .col_half img
{
	width:100%;
}

.single-press h1 {
    color: #50799c !important;
    text-transform: inherit !important;
    padding-top: 0px;
    padding-bottom: 5px;
}
.single-press h2
{
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    border-bottom: 2px solid #d9d9d9 !important;
	    color: #003c70 !important;
}

.single-press .col_half
{ 
color: #9ea2a6 !important;
    font-size: 15px !important;
    font-weight: normal !important;
    text-align: justify !important;
	line-height: 20px !important;
	font-family:calibri !important;
	
}
.single-press p,
.single-press p span{
    color: #9ea2a6 !important;
    font-size: 15px !important;
    font-weight: normal !important;
    text-align: justify !important;
    width: 100% !important;
	line-height: 20px !important;
	font-family:calibri !important;
}

.single-press a
{
    color: #ff8844;
}

.single-press .col_half .date
{
    font-size: 15px;
    color: #96999e;
    font-weight: bold;
    margin-bottom: 10px;
}
.horizon-swiper .horizon-outer {
	}
.horizon-swiper a {
	color:#003c70 !important;
	}
@media (min-width:480px) {
	.blogwraps {
		max-width:98% !important;
	}
	.blogwraps img {
		max-width:100% !important;
		}
	
		
	}		
@media only screen and (max-width:480px){
.single-press .col_half{
    width: 90% !important;
    float: left;
    left: 5%;
	height:auto !important;
    padding-left: 0px !important;
	margin-bottom: 10px !important;
}

.single-press h2 {
    width: 90%;
    margin-left: 5%;
}

.single-press{
    float: left;
    margin-bottom: 35px;
}
.horizon-swiper .titleofblog {
	padding:7px 0px !important;
	}
.latest-posts {
	margin-top:10px !important;
	max-width: 90%;
    margin: 0 auto;
	}
.horizon-swiper .blogwraps {
	max-width:97% !important;
	}	
	

.newblogtitle {
	max-width:97% !important;
	}		
}
</style>

<div class="full single-press">

<h2>Press Coverage</h2>

	<div class="col_half" style="text-align:center">

    	<?php

			if ( strlen($imagefile) > 0 && 'why-a-side-return-can-win-the-race-for-space' != $url_code ) {
        
        if (strpos($imagefile, 'videothumb')) {
          echo '<a href="https://vimeo.com/112830340" target="_blank">';
        }
        
				echo '<img src="/media/' . $imagefile . '" alt="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" title="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'"/>'; // Office Refurbishment London
        
        if (strpos($imagefile, 'videothumb')) echo '</a>';
      
			}

			?>

    </div>
	

  	<div class="col_half" style="margin-right: 0px !important;padding: 0px 0px 0px 20px;box-sizing: border-box !important;">

			<div id="bc" style="display:none;"><a href="/">Home</a> &rsaquo; <a href="/news/press_releases.html">Press Reviews</a> &rsaquo; <b><?php echo $title; ?></b></div>

    	<?php if ('why-a-side-return-can-win-the-race-for-space' != $url_code) { ?><h1><?php echo $title; ?></h1><?php } else { ?>
      <img src="/media/the_times_460.gif" style="max-width:300px;width:90%;margin-top:1em;margin-left:16px" alt="Times" />
      <?php } ?>

	  <div class="date">
	  <?php 
	  
	  if ( $dr > 0 ) {
            
            $tr = trim($tr);
            
            $date_release = $dr;
            
            $date_release = date('Y-m-d', $date_release);
            
            if ($tr) $date_release .= ' '.$tr;
            
            $date_release = strtotime($date_release);
            
					  $date_release = gmdate('d M Y', $date_release);
            
            //if ($date_release) $date_release .= ' GMT';
            
            echo $date_release;
					}
	  
	  ?></div>
	  
			<?php 
      if (is_int(strpos($body, '<p>'))) echo $body;
      else echo '<p>'.nl2br($body).'</p>';
      
      if ($pdffile) {
        echo '<br><br><a href="/pdf/'.$pdffile.'" target="_blank">Download the PDF</a>';
      }
      
      ?>

    </div>

    <div class="clearfix"></div>
<div class="latest-posts">
    <h1 class="newblogtitle">Latest Press Coverage</h1>
	
	
	<ul class="horizon-swiper">
		<?
			//$sql = "SELECT c.news_category_code, c.news_category_name, n.news_id, n.news_code, n.title, n.imagefile,n.smallImage, n.description, n.date_release FROM news n INNER JOIN news_category c ON c.news_category_id = n.news_category_id WHERE c.is_enabled = 1 AND c.is_active = 1 AND n.is_enabled = 1 AND n.is_active = 1 ORDER BY n.date_release DESC limit 8";
        	$sql = "SELECT id, url_code, title, imagefile, pdffile, body, date_release, time_release FROM press_release where id != (SELECT MAX(id) from press_release) AND is_enabled='1' ORDER BY date_release DESC, time_release DESC  limit 6";
			$rs = mysqli_query($dbconn,$sql);
			while ( $row = mysqli_fetch_assoc($rs)) {
			  
			  $i++;
			  
			  echo '<li class="horizon-item"><div class="blogwraps"><span class="datebadge">'. date('d/m', $row['date_release']).'</span><img src="/media/' . $row['imagefile'] . '" alt="'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'" title="'.htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8').'" width="480" />
		  <a class="titleofblog" href="/press_releases/' . $row['url_code'] . '.html" title="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '" style="font-size: 16px;">' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '</a><br /><span style="font-size:10px;">';
			//'<a href="/press_releases/' . $row['url_code'] . '.html" title="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '">' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8');		
			// echo '<img src="/phpthumb/phpThumb.php?src='.urlencode('/media/' . $row['imagefile']) . '&amp;w=326&amp;h=480" alt="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '" title="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '" /></a>';			

						echo '</span></div></li>';

			}

		?>
	</ul>
   
	</div>	

</div>

<script type="text/javascript" src="https://www.buildteam.com/js/horizon-swiper.js"></script>
<script type="text/javascript">
( function ( $ ) {
  'use strict';
			$('.horizon-swiper').horizonSwiper( {
    	showItems:3
  	});
  })( jQuery );
		</script>
<?php

}

require_once('./inc/footer.inc.php');


?>

<script>
jQuery(document).ready(function(){
	
	if(jQuery(window).width() < 500){
	jQuery(".press_releases .col_one_fourth").after("<div class='rule'></div>");
	}
	else
	{
	jQuery(".press_releases .col_one_fourth:nth-child(3n+1)").after("<div class='rule'></div>");
	}
	
	jQuery(".press_releases .col_one_fourth .press-thumbnail").mouseenter(function(){
		
		$(this).find('.date').animate({height: '30px'},100);
		
	});
	
	jQuery(".press_releases .col_one_fourth").mouseleave(function(){
		
		$(this).find('.date').animate({height: '0px'},100);
		
	});
	

/*
	$(".press_releases .col_one_fourth:nth-child(8)").after("<div class='more-less'><div class='mbutton expand'>More +</div></div>");
	
	
	

$(".press_releases .col_one_fourth").hide();

size_li = $(".press_releases .col_one_fourth").size();
    x=6;
    $('.press_releases .col_one_fourth:lt('+x+')').show();
    $('.expand').click(function () {
        x= (x+100 <= size_li) ? x+1000 : size_li;
        $('.press_releases .col_one_fourth:lt('+x+')').show();
		$(this).addClass("less");
		$(this).removeClass("expand");
		$(this).html("Less -");
    });
    $('.less').click(function () {
		alert("test");
		
        x=(x-1000<0) ? 6 : x-1000;
        $('.press_releases .col_one_fourth').not(':lt('+x+')').hide();
		
		$(this).addClass("expand");
		$(this).removeClass("less");
		$(this).html("More +");
    });
	*/
	
	
$ShowHideMore = $('.press_releases');

$ShowHideMore.each(function() {
    var $times = $(this).children('.col_one_fourth');
    if ($times.length > 10) {
        $ShowHideMore.children(':nth-of-type(n+17)').addClass('moreShown').hide();
        $(this).find('.moreless').addClass('more-times').html('<span class="mbutton">More +</span>');
    }
});

$(document).on('click', '.press_releases > span', function() {
  var that = $(this);
  var thisParent = that.closest('.press_releases');
  if (that.hasClass('more-times')) {
    thisParent.find('.moreShown').show();
    that.toggleClass('more-times', 'less-times').html('<span class="mbutton">Less -</span>');
  } else {
    thisParent.find('.moreShown').hide();
    that.toggleClass('more-times', 'less-times').html('<span class="mbutton">More +</span>');
  }  
}); 

	
});
</script>