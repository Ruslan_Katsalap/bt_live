<?php

require_once('./inc/util.inc.php');

// Pagination custom
include './inc/my_pagina_class.php';

class Pagina extends MyPagina {
  function Pagina() {
    // to prevent DB connection new creation
  }
}

define('QS_VAR', 'p'); // the number of records on each page
define('NUM_ROWS', 20); // the number of records on each page
define('STR_FWD', '&raquo;'); // the string is used for a link (step forward)
define('STR_BWD', '&laquo;'); // the string is used for a link (step backward)
define('NUM_LINKS', 10); // the number of links inside the navigation (the default value)
$nav = new Pagina;

$show_detail = false;

$url_code = '';

$title = '';

$imagefile = '';

if ( isset($_GET['c']) ) {

	$c = safe($_GET['c']);

	$rs = getRs("SELECT id, url_code, title, imagefile, pdffile, date_release, time_release, body FROM press_release WHERE url_code = '" . formatSql($c) . "'");

	while ( $row = mysqli_fetch_assoc($rs) ) {

		$show_detail = true;

		$url_code = $row['url_code'];

		$title = $row['title'];

		$imagefile = $row['imagefile'];
    
    $pdffile = $row['pdffile'];

		$body = $row['body'];

		$meta_title = $row['title'];

	}
  
  unset($row);
}



require_once('./inc/header.inc.php');



if ( $show_detail == false ) {



?>

  	<div class="full">

    	

			<?php echo $bc_trail; ?>

    	

      <h1>Press Coverage</h1>

      <ul class="press_releases">

				<?php

        $sql = "SELECT id, url_code, title, imagefile, pdffile, body, date_release, time_release FROM press_release ORDER BY date_release DESC, time_release DESC";
        
        /////////////////////////////////////////////////////
        $nav->sql = $sql; // the (basic) sql statement (use the SQL whatever you like)
        $rs = $nav->get_page_result(); // result set
        $num_rows = $nav->get_page_num_rows(); // number of records in result set 
        $nav_links = $nav->navigation(' | ', 'page_style'); // the navigation links (define a CSS class selector for the current link)
        $nav_info = $nav->page_info('to'); // information about the number of records on page ("to" is the text between the number)
        $simple_nav_links = $nav->back_forward_link(); // the navigation with only the back and forward links
        $total_recs = $nav->get_total_rows(); // the total number of records
        
        // if ($this->is_tpl_exists) $this->nav->free_page_result();

        ///////////////////////////////////////////////////////////////////////
        
        
        $i = 0;
        if (isset($_GET[QS_VAR])) $i = $_GET[QS_VAR] * NUM_ROWS;
        while ( $row = mysqli_fetch_assoc($rs) ) {
          $i++;
          
          
          echo '<li><div class="col_one_fifth thumb">';
          
          if ($row['imagefile']) {
          
            if (strpos($row['imagefile'], 'videothumb')) {
              echo '<a href="https://vimeo.com/112830340" target="_blank">';
            }
            else { 
              echo '<a href="/press_releases/' . $row['url_code'] . '.html">';
            }  
            
            echo '<img src="/phpthumb/phpThumb.php?src='.urlencode('/media/' . $row['imagefile']) . '&amp;w=326&amp;h=480" alt="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '" title="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '" /></a>';
          }
          
          echo '</div><div class="col_three_fifth col_last release"><a href="/press_releases_new/' . $row['url_code'] . '.html" title="' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '">' . htmlspecialchars($row['title'], ENT_QUOTES, 'UTF-8') . '</a><div class="date">';

					if ( $row['date_release'] > 0 ) {
            
            $row['time_release'] = trim($row['time_release']);
            
            $date_release = $row['date_release'];
            
            $date_release = date('Y-m-d', $date_release);
            
            if ($row['time_release']) $date_release .= ' '.$row['time_release'];
            
            $date_release = strtotime($date_release);
            
					  $date_release = gmdate('d M Y H:i', $date_release);
            
            if ($date_release) $date_release .= ' GMT';
            
            echo $date_release;
					}
          
          $brief = strip_tags($row['body']);
          $brief = substr($brief, 0, 250);
          $brief = explode(' ', $brief);
          array_pop($brief);
          $brief = trim(implode(' ', $brief), ',-!?');

					echo '</div><div class="brief">'.$brief.'...</div>'.($row['pdffile']?'<div class="download"><a href="/pdf/'.$row['pdffile'].'" target="_blank">Download PDF</a></div>':'').'</div></li>';

        }

        

        ?>

			</ul>
      
      <div style="margin-top:1em;font-size:1.1em;text-align:center">
      <?php echo $nav_links ?>
      </div>
      
    </div>

<?php

}



else {



define('hidePageMedia', true);



?>

<div class="full">

  	<div class="col_half">

			<div id="bc"><a href="/">Home</a> &rsaquo; <a href="/news/press_releases.html">Press Reviews</a> &rsaquo; <b><?php echo $title; ?></b></div>

    	<?php if ('why-a-side-return-can-win-the-race-for-space' != $url_code) { ?><h1><?php echo $title; ?></h1><?php } else { ?>
      <img src="/media/the_times_460.gif" style="max-width:352px;width:100%;margin-top:1em;margin-left:16px" alt="Times" />
      <?php } ?>

			<?php 
      if (is_int(strpos($body, '<p>'))) echo $body;
      else echo '<p>'.nl2br($body).'</p>';
      
      if ($pdffile) {
        echo '<p><a href="/pdf/'.$pdffile.'" target="_blank">Download the PDF</a></p>';
      }
      
      ?>

    </div>

    <div class="col_half" style="text-align:center">

    	<?php

			if ( strlen($imagefile) > 0 && 'why-a-side-return-can-win-the-race-for-space' != $url_code ) {
        
        if (strpos($imagefile, 'videothumb')) {
          echo '<a href="https://vimeo.com/112830340" target="_blank">';
        }
        
				echo '<img src="/phpthumb/phpThumb.php?src='.urlencode('/media/' . $imagefile) . '&amp;w=326&amp;h=480" alt="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" title="'.htmlspecialchars($title, ENT_QUOTES, 'UTF-8').'" style="width:100%;max-width:326px" />'; // Office Refurbishment London
        
        if (strpos($imagefile, 'videothumb')) echo '</a>';
      
			}

			?>

    </div>


</div>


<?php

}

require_once('./inc/footer.inc.php');



?>