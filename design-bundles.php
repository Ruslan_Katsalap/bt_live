<?php
$flagenew = false;
global $a_settings;
$newflaging = false;
require_once('./inc/settings.php');
/**************This is CRM integration code*****************/
@session_start();
$success = (isset($_SESSION['quote_success']) &&  $_SESSION['quote_success'] ) ? true : false;
unset($_SESSION['quote_success']);
$fetch = mysqli_query($dbconn,"SELECT * FROM bundle_project_formula group by product"); 

$arr = array();
while ($row = mysqli_fetch_assoc($fetch)) {
	$rec = mysqli_query($dbconn,"SELECT * FROM bundle_project_formula where product='".$row['product']."'"); 
	while ($row1 = mysqli_fetch_assoc($rec)) {
		$arr[$row['short_name']][$row1['short_sub_nm']]['price'] = $row1['price'];
		$arr[$row['short_name']][$row1['short_sub_nm']]['vat'] = $row1['vat'];
	}
}


//print_r($arr);
//die();
$formula_json = json_encode($arr);

if($_SERVER['REQUEST_METHOD'] == 'POST') { 
	require_once($_SERVER["DOCUMENT_ROOT"]."/crm_config.php");
	$now = strftime("%Y-%m-%dT%H:%M:%S");
	/**************This is CRM integration code**************** */
	
	//id,u_name,u_email,u_address,u_telephone, message, project, bundle, price, date_submit
  $sql = "INSERT INTO project_quote_records SET u_name='".q($_POST['name'])."', u_email='".q($_POST['email'])."', u_address='".q($_POST['address'])."', u_telephone='".q($_POST['telephone'])."', u_postcode='".q($_POST['postcode'])."', message='".q($_POST['message'])."',project='".q($_POST['selectedProject'])."',bundle='".q($_POST['selectedBundle'])."',price='".q($_POST['updatedPrice'])."', date_submit=UNIX_TIMESTAMP()";
  if(query($sql)) {
	  $success = true;
  }
    if($success) {
		$to = 'hello@buildteam.com';
		$cto = $_REQUEST['email'];
		$subject = 'Design Bundle';
		$message = '
<div style="background:#fff;width:650%; margin:0 auto; padding:10px;">
				'.$_REQUEST['name'].' submitted an enquiry. The details are as follows:<br/></br>
				Name : '.$_REQUEST['name'].' <br/>
				Address : '.$_REQUEST['address'].' <br/>
				Postcode : '.$_REQUEST['postcode'].' <br/>
				Email Address : '.$_REQUEST['email'].' <br/>
				Contact Number : '.$_REQUEST['telephone'].' <br/>
				Message : '.$_REQUEST['message'].' <br/><br/>
				Project : '.$_REQUEST['selectedProject'].' <br/><br/>
				Bundle : '.$_REQUEST['selectedBundle'].' <br/><br/>
				Quoted Price : '.$_REQUEST['updatedPrice'].' <br/><br/>
			</div>';
		$message1 = '
<div style="background:#fff;width:650%; margin:0 auto; padding:10px;">
				Dear '.$_REQUEST['name'].',
	<br/>
	<br/>
				Thank you for your enquiry, your form was submitted successfully. A member of the team will contact you in 1-2 working days.
	<br/>
	<br/>
				
				Kind regards,
	<br/>Liam Davies<br/>
		Architectural Assistant<br/>
		Build Team
	<br/>
</div>';
		
		// Always set content-type when sending HTML email
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= 'From: Build Team 
<info@buildteam.com>' . "\r\n";
		
		mail($to,$subject,$message,$headers);
		mail($cto,$subject,$message1,$headers);
	}
	
	$_SESSION['quote_success'] = true;
	header('Location: /the_design_bundles.html');
}
ob_start();
require_once('./inc/header.inc.php');
?>
	<!--link href="https://www.buildteam.com/css/book-visit.css?v=1" rel="stylesheet"-->
		<!--<script src="//code.jquery.com/jquery-1.10.2.js"></script>-->
		<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
		<style type="text/css">
/*new css*/
body {
	/*background:url(http://www.buildteam.com/images/contact/new_sv_bg.jpg) no-repeat center top !important;*/
	background-size:cover;
	}
.top_part {
    background:rgba(255,255,255,1);
}
	
	.main {
		background: #fff;
		background: rgba(255,255,255,0.9);
		padding-left: 35px !important;
		padding-right: 35px !important;
		box-sizing: border-box !important;
		/* padding-bottom: 121px !important; */
		max-width: 1080px;
		/* padding: 135px 0 0 0; */
		/* background: red; */
		/* max-height: 100px !important; */
		padding-bottom: 0;
		margin-bottom: 0;
		padding-top: 110px;
	}
	
	
	body
	{
		//background-image:url(../images/book-a-site-visit.jpg);
		background-attachment:fixed;
		background-size:cover;
		text-align: left !important;
	}
	
	footer.round-2 {
    margin-top: 0px;
	}
	
	.page-title
	{
    font-size: 30px;
    text-align: left;
    font-weight: bold;
    color: #0f4778;
    border-bottom: 0;
    padding-bottom: 15px;
	margin-bottom: 0px;
	}

	p.description_gdpr {
		padding-top: 0px;
		font-size: 16px;
		text-align: justify;
		color: #74787f;
		line-height: 24px;
		font-weight: normal;
	}
	h2.form-title {
		font-size: 21px;
		padding-bottom: 0;
		margin-bottom: 12px;
		color: #003c70;
		font-family: calibri;
		font-weight: 600;
	}
	h3 {
    text-align: left;
    font-weight: 400 !important;
    margin-bottom: 0px;
    border-bottom: 1px solid #d7d7d7;
    padding-bottom: 8px;
}
	
	
	input#postcode {
    padding: 7px;
    margin-top: 10px;
    width: 210px;
    border: 2px solid #d5d5d5;
}

.book-a-site-table {
    margin-top: 30px;
}
.options-content
{
    visibility: visible;
    max-width: 100% !important;
    margin-top: 20px;
    margin-bottom: 0px;
}

.book-a-site-table th {
    padding: 8px 0px;
    font-size: 21px;
    border-top: 2px solid #d7d7d7;
    //border-bottom: 1px solid #d7d7d7;
    color: #4b759a;
    font-weight: normal;
	text-align:inherit;
}
th
{
		text-align:inherit;
}


.book-a-site-table td {
    padding: 8px 0px;
    font-size: 19px;
	color:#7d7d7d;
	font-weight:normal;
}

.tick
{
	background-image:url(../images/tick.png);
		background-position:center;
		background-repeat:no-repeat;
}

.tick-star
{
	background-image:url(../images/tick-star.png);
		background-position:center;
		background-repeat:no-repeat;
}

tr
{
	border-top:1px solid #e0e0e0;
}

.postcode{
    display: inline;
    padding-left: 7px;
    text-transform: uppercase;
}

.up_down
{
	display:none;
}
.kaction
{
	opacity:0.5;
}
.book-wrap
{
border:none !important;	
padding-top: 15px;
}

.book-wrap input[type=text],
.book-wrap textarea
{
	width:100%;
	margin-top:5px;
	border:1px solid #8b8f94;
	margin-bottom:10px;
	background:none;
	max-height:32px;
}
.book-wrap .ui-widget.ui-widget-content {
	background:none !important;
	}

label,
span.red
{
	font-size: 18px;	
}
input.book-now, a.book-now  {
  -webkit-border-radius: 6;
  -moz-border-radius: 6;
  border-radius: 6px;
  font-family: Arial;
  color: #ffffff;
  font-size: 13px;
  background:#0f4778;
  padding: 10px 30px 10px 30px;
  text-decoration: none;
  height:auto !important;
  float: right !important;
  max-width: 140px;
  width: 100%;
  padding: 14px;
  border: 0;
  -webkit-appearance: unset;
}

input.book-now:hover, a.book-now:hover {
  background: #5d7c98;
  text-decoration: none;
}
a.book-now {
	float: left !important;
}
.greyBg {
	background: #f4f6f8;
	margin-top: 20px;
}
.greyBg .main {
	padding-top: 20px;
	background: transparent;
}
.clear {
	clear: both;
}
.m-0 {
	margin: 0;
}
.pt-0 {
	padding-top: 0;
}
.greyBg .main .form-control { 
    box-shadow: none;
    font-size: 16px;
    padding: 24px 10px;
	border: 0;
}
form {
	margin-top: 24px;
}
p.data_info {
	font-size: 16px;
    font-weight: 500;
}
.bookthanksinner h4 {
	font-size: 28px;
	padding-bottom: 10px;
}
@media only screen and (max-width: 768px) {
	.main {
		padding: 94px 0 0 0 !important;
		width: 100%;
	}
	.greyBg {
		margin-top: 30px !important;
	}
	.greyBg .main {
		padding-top: 20px !important;
		padding-bottom: 20px !important;
	}
	.main.pt-0 {
		padding-top: 0 !important;
	}
}
.greyBg .main .form-control::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: #74787f !important;
    opacity: 1; /* Firefox */
}

.greyBg .main .form-control:-ms-input-placeholder { /* Internet Explorer 10-11 */
    color: #74787f !important;
}

.greyBg .main .form-control::-ms-input-placeholder { /* Microsoft Edge */
    color: #74787f !important;
}

.storydesc p {
    padding-top: 0px;
    font-size: 20px;
    text-align: justify;
    color: #74787f;
    line-height: 24px;
    font-weight: normal;
}
.our-story h2 {
    font-size: 30px !important;
    padding-bottom: 10px !important;
    margin-bottom: 20px !important;
    color: #003c70 !important;
    font-family: calibri !important;
	font-weight: 700;
}

h2.drop-down {
	font-size: 24px !important;
    color: #003c70;
    text-transform: capitalize !important;
    font-weight: bold;
    font-family: calibri !important;
	margin-top: 0;
    margin-bottom: 0px;
	position: relative;
}
hr {
    height: 1px;
    display: block;
    border: 0px;
    border-top: 2px solid rgba(154,154,154,0.5);
    margin: 1em 0;
    padding: 0;
}
.drop-down-box {
    border: 1px solid #bdc5cb;
    color: #003c70;
    padding: 14px 10px;
    font-size: 19px;
    font-weight: 600;
    margin: 20px 0;
    box-shadow: 6px 6px 0 #bdc5cb20;
    width: 17.6%;
    float: left;
    min-height: 192px;
	text-align: center;
	line-height: 20px;
}
.drop-down-box.mr {
	margin: 20px 3% 0;
}
.drop-down-box.btn-project.selected, .drop-down-box.btn-bundle.selected,.drop-down-box.contact-book.selected  {
    background:  #003c70;
    color:  #fff;
    border-radius: 0;
}
.invert-image {
	display: none;
}
.drop-down-box.selected .invert-image {
	display: block;
	margin: 0 auto;
	margin-bottom: 12px;
}
.drop-down-box.selected .o-image {
	display: none;
	margin: 12px auto;
}
.drop-down-box.btn-project.selected .invert-image {
	margin-top: 0;
	margin-bottom: 12px;
}
span#selectedProject , span#selectedBundle{
    color:  #74787f;
    font-weight: 500;
    font-size: 18px;
    line-height: 25px;
}
.contact-book span {
    display:  block;
}
.l-conv {
	max-width:  100px;
    margin:  0 auto;
}
span.deleteProject {
	border: 1px solid;
    border-radius: 100%;
    width: 24px;
    height: 24px;
    float: right;
    position: relative;
    cursor: pointer;
    margin-left: 5px;
}
span.deleteProject:after {
    content:  "x";
    position:  absolute;
    top: -4px;
    font-size: 20px;
    left: 50%;
    transform: translateX(-50%);
}
.bundle .drop-down-box .desc{
	font-size: 16px;
	color: #74787f;
	padding-top:10px;
	font-weight: normal !important;
}
.drop-down-box.price-box:after {
    content:  "";
    position:  absolute;
    right:  -60px;
    top: 18px;
}


.drop-down-box.price-box {
    max-width:  200px;
    /* border-width:  2px; */
    border-color:  #003c70;
    border-radius:  5px;
    padding: 20px;
    min-height: 60px;
	position: relative;
}
.drop-down-box .left1 {
	float: left;
	padding-top: 5px;
    width: 70px;
} 
.drop-down-box .left2{
	float: left;
    padding-left: 15px;
    padding-top: 0px;
}
h2.drop-down.arrow-slide {
	cursor: pointer;
}
h2.drop-down.arrow-slide:before {
    background-image: url(http://www.buildteam.com/images_new_design/faq-arrow-img.jpg);
    background-position: 0px -54px;
    content:  "";
    position:  absolute;
    left: -25px;
    top: 5px;
    width:  40px;
    height:  40px;
    cursor:  pointer;
    background-repeat:  no-repeat;
}
h2.drop-down.arrow-slide.expand:before {
	background-position: 0px 1px !important;
}
a.book-now.disabled {
	background: #5d7c98;
	cursor: default;
	opacity: 0.81;
}
.lightbox_content input[type=text] {
	width: 100%;
}
.outsideinner {
	height: auto;
}
.outsideinner {
	top: 140px;
	max-width: 580px;
	background: #f6f3f5;
}
.outsideinner h4 {
   border-bottom: none;
   padding-bottom: 0;
   margin-bottom: 0;
	
}
.outsideinner form { 
	margin-top: 10px;
}
.outsideinner p {
	margin-bottom: 0;	
}
.outsideinner.formfields {
    margin-bottom: 12px;
}

.outsideinner .lightbox_content input[type=text], .outsideinner .lightbox_content input[type=password] {
    border: none !important;
    border-radius:  0;
    padding: 20px 10px;
}
.contactus form input[type='submit'] {
	max-width: 110px;
    margin-top: 14px;
	margin-bottom: 0;
}
.house-icon {
	margin: 0 auto;
	//display: block;
}
.drop-down-box.btn-bundle {
    width: 25%;
    border-left: none;
    padding: 6px 0 0 0;
    box-shadow:  none;
	min-height: 490px;
}

.drop-down-box.btn-bundle:first-child {
    /* background: aliceblue; */
    border-left: 1px solid #bdc5cb;
}

.drop-down-box.btn-bundle .inner-box-bundle{
	    padding: 0 14px;
}

img.o-image,img.invert-image {
    margin-bottom: 12px;
}
.drop-down-box.btn-bundle ul {
	border-top: 1px dashed #bdc5cb;margin-top:  16px;
	list-style: none;
	padding-top: 14px;
}
.drop-down-box.btn-bundle ul li{
	text-align: left;
	font-size: 18px;
	font-weight: 500;
	color: #74787f;
	position: relative;
	margin-left: 26px;
	line-height: 20px;
}
.drop-down-box.btn-bundle ul li:before {
	content: ".";
	font-size: 40px;
	line-height: 0;
	left: -14px;
	top: 2px;
    position: absolute;
	color: #003c70;
}
.heading-row {
	padding-top: 10px;
    padding-bottom: 6px;
}
.drop-down-box.btn-bundle.selected ul li, .drop-down-box.btn-bundle.selected p, .drop-down-box.btn-bundle.selected .desc, .drop-down-box.btn-bundle.selected ul li:before {
    color: #fff;
}
.drop-down-box.btn-project span {
    display:  block;
}
p.confirmation {
    display:  block;
    float:  left;
    width:  100%;
    margin-top: -12px;
}
a.privacy-policy {
    background:  none;
    padding:  0;
    color: #2e386c;
    font-size: 14px;
}
a.pop-up-close {
    float: right;
    background: none;
    color: #6687a6;
    padding-right: 0;
    font-weight: bold;
    position: absolute;
    right: 24px;
    top: 6px;
}
.inner-box-bundle .o-image, .inner-box-bundle .invert-image {
    margin-top: 10px;
}
a.pop-up-close:hover {
   color: #2e386c;
}
.number1 {
	margin-top:-7px !important;
	}
@media only screen and (max-width: 1020px) {
    .drop-down-box {
		width: 100% !important;
		min-height: auto !important;
		margin: 10px 0 !important;
		border: 1px solid #bdc5cb !important;
		float: none;
	}

	.drop-down-box.btn-bundle .inner-box-bundle {
		    padding: 10px 14px 6px;
	}
	.drop-down-box.btn-bundle.selected .invert-image {
		margin-top: 0;
	}
	h2.drop-down.arrow-slide {
		margin-left:  30px;
	}
	h2.drop-down.arrow-slide:before {
		height:30px;
	}
	.outsidepoup {
		position: absolute;
	}
	.outsideinner {
		top: 140px;
	}
	span#selectedProject, span#selectedBundle {
		display: none;
	}
}
</style>
		<div class="full">
			<div class="col-sm-12">
				<span class="our-story">
					<h2 class="">Design Bundles
					</h2>
				</span>
				<div class="storydesc">
				<p class="">Whether you need some planning feasibility advice or a full Building Control drawing set, we offer a variety of Design Packages which can be adapted to suit you. You can commit to everything from the very beginning, or build your package as you go. Simply select your project and your preferred bundle from the options below and we’ll confirm our fixed Design Fee.</p>
				</div>
			</div>
			<div class="col-sm-12">
				<hr>
				<div>
					<div>
						<div class="row heading-row">
							<div class="col-md-4 col-sm-4">
								<h2 class="drop-down arrow-slide expand" data-id="project">Select Project: </h2>
							</div>
							<div class="col-md-8 col-sm-8 text-right">
								<span id="selectedProject"></span>
							</div>
						</div>
					</div>
					<div class="inner-box" data-id="project">
						<div class="drop-down-box btn-project" data-id="gfe"><img src="/images/get-quote/house-icon.png" class="house-icon o-image"><img src="/images/get-quote/house-icon-white.png" class="house-icon invert-image"><span>Ground Floor Extension</span></div>
						<div class="drop-down-box btn-project mr" data-id="loft">
							<img src="/images/get-quote/house-icon-1.png" class="house-icon o-image">
							<img src="/images/get-quote/house-icon-white-1.png" class="house-icon invert-image">
						<span>Ground Floor &amp; Loft Extension</span></div>
						<div class="drop-down-box btn-project" data-id="gfe-loft"><img src="/images/get-quote/house-icon-2.png" class="house-icon o-image"><img src="/images/get-quote/house-icon-white-2.png" class="house-icon invert-image"><span>Loft<br>Conversion</span></div>
						<div class="drop-down-box btn-project mr" data-id="gf-ff"><img src="/images/get-quote/house-icon-3.png" class="house-icon o-image"><img src="/images/get-quote/house-icon-white-3.png" class="house-icon invert-image"><span>Ground Floor <br>&amp; First Floor Extension</span></div>
						<div class="drop-down-box contact-book"><img src="/images/get-quote/house-icon-4.png" class="house-icon o-image"><img src="/images/get-quote/house-icon-white-4.png" class="house-icon invert-image"><span>Something Else? Contact us for a quote</span></div>
					</div>
				</div>
			</div>
			<div class="col-sm-12 bundle">
				<hr>
				<div>
					<div class="row heading-row">
						<div class="col-md-4 col-sm-4">
							<h2 class="drop-down arrow-slide" data-id="bundle">Select Bundle: </h2>
						</div>
						<div class="col-md-8 col-sm-8 text-right">
							<span id="selectedBundle"></span>
						</div>
					</div>
				</div>
				<div class="inner-box" data-id="bundle" style="display: none;">
					<div class="drop-down-box btn-bundle" data-id="preplaning">
						<div class="inner-box-bundle">
							<div class="">
								<img src="/images/get-quote/new_icons.png" class="o-image">
								<img src="/images/get-quote/invert.png" class="invert-image">
							</div>
							<div class="">
								<div class="">Pre-Planning Advice</div>
								<div class="desc">Explore feasibility options via your local council
</div>
							</div>
						</div>
						<div class="clear"></div>
						<ul>
							<li>Expert advice on planning </li>
							<li>Drawings and design development </li>
							<li>Preparation in advance of
attending the Pre-planning
Application meeting</li>
						</ul>
					</div>
					<div class="drop-down-box btn-bundle"  data-id="designplan">
						<div class="inner-box-bundle">
						<div class="">
							<img src="/images/get-quote/new_icons2.png" class="o-image">
							<img src="/images/get-quote/invert2.png" class="invert-image">
						</div>
						<div class="">
							<div class="">Design &amp; Planning </div>
							<div class="desc">Finalise your design and obtain planning consent
</div>
						</div>
						</div>
						<div class="clear"></div>
						<ul>
							<li>Full measured survey </li>
							<li>Unlimited drawing amendments </li>
							<li>Planning advice and submission of application</li>
						</ul>
					</div>
					<div class="drop-down-box btn-bundle" data-id="ggtb">
						<div class="inner-box-bundle">
						<div class="">
							<img src="/images/get-quote/new_icons3.png" class="o-image">
							<img src="/images/get-quote/invert3.png" class="invert-image">
						</div>
						<div class="">
							<div class="">Ready to Build</div>
							<div class="desc">Get ready to build with planning
submission, building regs pack and
full structural engineering.
</div>
						</div>
						</div>
						<div class="clear"></div>
						<ul>
							<li>Full measured survey </li>
							<li>Unlimited drawing amendments </li>
							<li>Planning advice and submission of application </li>
							<li>Full Building Reg. Pack</li>
							<li>Structural calculations</li>
                            <li>Detailed schedule of works</li>
						</ul>
					</div>
					<div class="drop-down-box btn-bundle" data-id="fftb">
						<div class="inner-box-bundle">
						<div class="">
							<img src="/images/get-quote/new_icons4.png" class="o-image">
							<img src="/images/get-quote/invert4.png" class="invert-image">
						</div>
						<div class="">
							<div class="">Fast Track To Build </div>
							<div class="desc">Fast Track your Tender Pack and begin collecting fixed price quotations.
</div>
						</div>
						</div>
						<div class="clear"></div>
						<ul>
							<li>Full measured survey</li>
							<li>Unlimited drawing amendments </li>
							<li>Planning advice and submission of application </li>
							<li>Fast Track full Building Reg. Pack (save 8 weeks) </li>
							<li>Fast Track Structural calculations (save 8 weeks) </li>
							<li>Fast Track Detailed schedule of works (save 8 weeks) </li>
<!--							<li>Party Wall Notice to one adjoining owner</li> -->
						</ul>
					</div>
				</div>
			</div>
			<div class="col-sm-12 bundle">
				<hr>
				<div>
					<h2 class="drop-down">Your Design Bundle</h2>
					<div class="drop-down-box price-box"></div>
					<div class="clear"></div>
					<a href="javascript:void(0)" class="book-now disabled" id="book-now-quote">Enquire Now</a>
<!--					<p class="confirmation number1">* Subject to confirmation and issuance of Design & Build Quote</p>
                    <p class="confirmation">* Excluding VAT at prevailing rate</p> -->
                    <p class="confirmation">Prices quoted exclude VAT at prevailing rate. Quoted fixed fees are based on standard size projects. Larger schemes or bespoke projects may be quoted for differently.</p>
				</div>
				
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="main pt-0">
		<!--<div class="full"><div class="col-md-12"><p class="data_info">All of the data we hold is used for the greater good. We only refer to your data to assist with things like
		enquiries. </p></div></div>-->
		
		
		<div class="clear"></div>
		<div class="outsidepoup">
			
			<div class="outsideinner">
				<div class="lightbox_content contactus">
					<a href="javascript:void(0)" class="pop-up-close">X</a>
					<h4>Enquire Now</h4>
					<p>Please enter a few details</p>
					<form method="post">
						<div class="">
							<input type="hidden" name="selectedProject" />
							<input type="hidden" name="selectedBundle" />
							<input type="hidden" name="updatedPrice" />
							<div class="formfields">
								<input type="text" name="name" value="" placeholder="Name*">
							</div>
							<div class="formfields">
								<input type="text" name="address" value="" placeholder="Address*">
							</div>
							<div class="formfields">
								<input type="text" name="postcode" value="" placeholder="Postcode*">
							</div>
							<div class="formfields">
								<input type="text" name="email" value="" placeholder="Email*">
							</div>
							<div class="formfields">
								<input type="text" name="telephone" value="" placeholder="Telephone*">
							</div>
							<div class="formfields">
								<input type="text" name="message" value="" placeholder="Message*">
							</div>
							<div class="">
								<div class="broform100 checkboxbro broleft">
									<div class="col-md-1 col-sm-1" style="padding-left: 0; padding-right: 0; max-width: 14px;">
										<input type="checkbox" name="contactSubscribe" value="Yes"><span>
									</div>
									<div class="col-md-11 col-sm-11" style="width: 96%; padding-right: 0;"><span>Sign up to our newsletter and receive updates on our latest blogs, news items and special offers – see our <a href="https://www.buildteam.com/privacy-policy.html" class="privacy-policy">Privacy Policy</a> for more information on how we use your personal data</span>
									</div>
								</div>
							</div>
							<div class="clear"></div>
							<div class="formfields" style="margin-bottom: 0;">
								<input type="submit" name="submit" value="Submit">
							</div>
						</div>
					</form>
					<!--<a href="javascript:void(0);">Ok</a>-->
				</div>
			</div>
		</div>
		<?php
				$style = $success ? 'display:block' : 'display: none';
			?>
		<div class="bookthanks" style="
			<?=$style;?>">
			<div class="bookthanksinner">
				<div class="lightbox_content">
					<h4>Thank you!</h4>
					<p>Thank you for your request, your form was submitted successfully. We will contact you within 1-2 working days.</p>
					<a href="https://www.buildteam.com" class="book-now">Okay</a>
					<div class="clear"></div>
				</div>
			</div>
		</div>
		<script>
$(document).ready(function(e) {
    $(".choice-table").hover(function(e) {
        $(".promptimage").addClass("comefromleft");
    });
});
</script>
		<script>
 
$(".selected").click(function(){
	
	});
</script>

<script>
var content = <?php echo $formula_json; ?>;
content.project = {
		gfe: 'Ground Floor Extension',
		loft: 'Ground Floor &amp; Loft Extension',
		"gfe-loft": 'Loft Conversion',
		"gf-ff": 'Ground Floor &amp; First Floor Extension'
	};
content.bundle = {
		preplaning: 'Pre Planning Advice',
		designplan: 'Design & Planning',
		ggtb: 'Building Reg. Pack',
		fftb: 'Fast Track To Build'
}

const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var updatePrice = function() {
	$('.price-box').html('');
	if($('#selectedProject').html() != '' && $('#selectedBundle').html() != '') {
		var bundle = $('#selectedBundle .deleteProject').attr('data-id');
		var project = $('#selectedProject .deleteProject').attr('data-id');
		var price = content[bundle][project];
		var vat = price.vat;
		var total = parseFloat(price.price) + parseFloat(price.price * vat / 100);
		$('.price-box').html("&pound;" + numberWithCommas(total));
		$('#book-now-quote').removeClass('disabled');
		$('[name="selectedProject"]').val(content.project[project]);
		$('[name="selectedBundle"]').val(content.bundle[bundle]);
		$('[name="updatedPrice"]').val(price.price);
	} else {
		$('#book-now-quote').addClass('disabled');
	}
	
}

$(document).ready(function() {
	console.log(<?php echo $formula_json; ?>);
	//content.dat= JSON.parse(JSON.stringify(<?php $formula_json; ?>));
	$('div.btn-project').click(function() {
		$('div.btn-project').removeClass('selected');
		$(this).addClass('selected');
		var projectName = content.project[$(this).attr('data-id')] + '<span class="deleteProject" data-id="'+$(this).attr('data-id')+'"></span>';
		var html = projectName;
		$('#selectedProject').html(html);
		updatePrice();
		$('h2[data-id="bundle"]').addClass('expand');
		$('.inner-box[data-id="bundle"]').slideDown();
	})
	$(document).on("click", '.deleteProject', function() {
		$('#'+$(this).parent().attr('id')).html('');
		$('[data-id="'+$(this).attr('data-id')+'"]').removeClass('selected');
		updatePrice();
	});
	
	$('div.btn-bundle').click(function() {
		$('div.btn-bundle').removeClass('selected');
		$(this).addClass('selected');
		var projectName = content.bundle[$(this).attr('data-id')] + '<span class="deleteProject" data-id="'+$(this).attr('data-id')+'"></span>';
		var html = projectName;
		$('#selectedBundle').html(html);
		updatePrice();
	});
	
	$('h2.drop-down.arrow-slide').click(function() {
		var dataId = $(this).attr('data-id');
		if($(this).hasClass('expand')) {
			$(this).removeClass('expand');
		} else {
			$(this).addClass('expand');
		}
		$('.inner-box[data-id="'+dataId+'"]').slideToggle();
	});
	
	$('#book-now-quote').click(function(e) {
		e.preventDefault();
		if(!($(this).hasClass('disabled'))) {
			$("html, body").animate({ scrollTop: 0 }, "slow");
			$('.outsidepoup').height($( document ).height()).show();
		}
		
	})
	
	$(".contact-book").on("click", function(){
		$(this).addClass('selected');
		$("#book-now-quote").removeClass('disabled').click();
	});
	
	$('.pop-up-close').on("click", function() {
		$('.drop-down-box.contact-book').removeClass('selected');
		$('.outsidepoup').hide();
	});
})

</script>
		<?php

require_once('./inc/footer.inc.php');
  
?>