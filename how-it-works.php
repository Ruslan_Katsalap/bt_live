<?php


if (!isset($_POST['api'])) {
  require_once('./inc/header.inc.php');
}
else {
  require_once('./inc/util.inc.php');
}

//require_once ('./phpmailer/class.phpmailer.php');
require_once ('./phpmailer/PHPMailerAutoload.php'); // class.phpmailer.php

$err = '';
$status = 0;

if ('POST'==$_SERVER['REQUEST_METHOD']) $err = "Message was not send successfully. Please enter all fields.";

if (isset($_POST['name']) && isset($_POST['surname']) && isset($_POST['address1']) && isset($_POST['address2']) && isset($_POST['email']) && isset($_POST['mobile']) && isset($_POST['comments']) && isset($_POST['postcode'])) {
  // die('inside isset if');
  
  require ('xml/postcode.inc.php');
  $valid = bypValidatePostcode($_POST['postcode']);
  if (!$valid) {
    $err .= " Please enter correct UK postcode.";
    // print_r($err); 
  }

  if ($valid && trim($_POST['name']) && trim($_POST['surname']) && trim($_POST['address1']) && trim($_POST['address2']) && trim($_POST['email']) && trim($_POST['mobile']) && trim($_POST['comments']) && 'Your Name'!=$_POST['name'] && 'Your E-mail address'!=$_POST['email']) {

    $mail = new PHPMailer();
    
    $mail->IsSMTP();
    $mail->SMTPDebug = 0;
    $mail->SMTPAuth = true;
    $mail->SMTPSecure = 'TLS';
    $mail->Host = "smtp.sendgrid.net";
    $mail->Port = 587;
    //$mail->IsHTML(true);
    $mail->Charset = 'UTF-8';
    $mail->Username = "apikey";
    $mail->Password = "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";
    
    //$mail->IsHTML(true);
    $file_tmp_name    = $_FILES['data']['tmp_name'];
    $file_name        = $_FILES['data']['name'];
    $file_size        = $_FILES['data']['size'];
    $file_type        = $_FILES['data']['type'];
    $file_error       = $_FILES['data']['error'];

    $mail->From     = $_POST['email']; //'do-not-reply@buildteam.com';

    $mail->FromName = $_POST['name'] . ' ' . $_POST['surname'];

    $admin_email = 'info@buildteam.com'; 
    
    $mail->AddAddress( $admin_email );
    for($i=0; $i < count($_FILES['data']['name']); $i++){
      $mail->addAttachment($file_tmp_name[$i], $file_name[$i]);
    }
    // $mail->addAttachment($file_tmp_name, $file_name);

    $ret = 'Name : ' . $_POST['name'] . ' ' . $_POST['surname'] . '

    E-mail : ' . $_POST['email'] . '

    Mobile : ' . $_POST['mobile'] . '

    Message : ' . $_POST['comments'] . "\n\nPostcode: " . $_POST['postcode'] . '

    Date / Time Posted : ' . date($PHP_LONG_DATE_FORMAT, time()) . '

    Client IP Address : ' . $IP;

    //put html wrappers in place

    //$ret = insertHtmlWrapper($ret);

    $mail->Subject  = 'Uploaded Image';
    $mail->Body     = $ret;

    if ( $mail->Send() ) {

      $err = "Message sent successfully.";
      $status = 1;
  ?>
  <script type="text/javascript">
    alert("Thanks for submitting your drawings, a member of our Team will be in touch within 48 hours");
  </script>
<?php
      // print_r($err);
    }
    else {
      $err = "Message was not send successfully. Some error occurs.";
      // print_r($err);
    }
  }
}
?>

<style type="text/css">

  .top_part {
    position: relative;
  }
  
  .hide{
    display: none;
  }

  /* ================================================= */
  /*             Styles for Header Section             */
  /* ================================================= */
.form-btn[disabled]{
    box-shadow: none;
    cursor: not-allowed;
    opacity: 0.65;
    pointer-events: none;

}
.main {
	padding:0px !important;
	}

  .col_one_fourth{
    overflow: hidden;
  }

  .process-wrapper .col_one_fourth{
    margin-right:3%;
  }
  .process-wrapper{
    margin-top: 50px;
  }
  .main-heading{
    font-size: 25px;
    color: #231f1e;
    margin-top: 50px;
  }
  .head-bar{
    border: 2.5px solid #353a58;
    width: 4%;
    margin: 35px auto 0;
  }
  .text-center{
    text-align: center;
  }
  .head-text{
    max-width: 410px;
    margin: 20px auto 60px;
    font-size: 17px;
    color: #767676;
  }
  #pincodeForm .input-group {
    clear: both;
    margin: 0 auto;
    max-width: 58%;
  }
   
    .grey-design-img,
     .site-visit-content,
    .grey-site-visit-img, .grey-build-img,
     .site-visit-content-m25, .design-content,
     .design-content-m25, .build-content, .build-upload-content{
      display: none;
    }
    
  .grey-design-img {
    left: 43px;
    max-width: 100px;
    position: absolute;
    top: 34px;
    }
  #pincodeForm #postcode{
    z-index: 1;
  }
  .postcode-label {
    background-clip: padding-box;
    background-color: #f6f6f6 !important;
    border: 1px solid #dbdbdb !important;
    border-radius: 2px 0 0 2px !important;
    color: #949494 !important;
    float: left !important;
    font-size: 15px !important;
    line-height: 49px !important;
    max-width: 200px !important;
    padding: 0 12px !important;
  }
  .postcode-search-box {
    background-color: #fbfbfb !important;
    border: 1px solid #dbdbdb !important;
    float: left !important;
    font-size: 25px !important;
    height: 49px !important;
    padding: 0 12px !important;
    max-width: 220px;
  }
  .form-btn-wrapper{
    padding: 0 !important;
  }
  .form-btn {
    background-color: #3ab54a !important;
    border: 1px solid #dbdbdb !important;
    border-left: none !important;
    color: #fff !important;
    font-size: 14px !important;
    min-height: 51px !important;
    cursor: pointer !important;
    padding: 12px 25px !important;
    position: relative !important;
  }
#myBtn.form-btn{
    font-size: 11px !important;
    min-height: 30px !important;
    padding: 5px 7px 5px !important;
  }
  .form-btn .btn-file{
    height: 100%;
    left: 0;
    opacity: 0;
    position: absolute;
    top: 0;
    width: 100%;
}
  .form-btn:hover, .form-btn:focus, .form-btn:active{
    background-color: #38b449;
    outline: none !important;
    border-color: #dbdbdb;
  }
  .home-img{
    display: block;
    margin: 20px auto;
  }
  /*.home-img-wrapper:before{
      position: absolute;
      content: '';
      top: 200px;
      left: -10px;
      height: 170px;
      width: 50px;
      background: url('../images/arrow.png') no-repeat;
  }
  */.upload-btn {
    font-size: 14px;
    position: relative;
    bottom: 95px;
    left: 0px;
    border-radius: 0;
    display: block;
    margin: 0 auto;
    width: 50%;
    height: 60px;
  }
  .upload-btn, .upload-btn:hover, .upload-btn:focus, .upload-btn:active{
    background-color: #343959;
    border-color: #343959;
  }
  .upload-btn-text{
    font-size: 14px;
    margin: 3px 0 0;
  }
  .text-wrapper{
    height: 70px;
  }
  .options-content {
    color: #767676;
    display: block;
    font-size: 14px;
    margin: 20px auto 0;
    max-width: 90%;
  }
  .premium-site-content{
    max-width: 70%;
   }

  /* ================================================= */
  /*                  Styles for Options               */
  /* ================================================= */

  .badge-ribbon {
    background: #fff;
    position: relative;
    height: 170px;
    width: 170px;
    margin: 0 auto;
    -moz-border-radius:    125px;
    -webkit-border-radius: 125px;
    border-radius:         125px;
  }
  .badge-ribbon:before{
    position: absolute;
    content: '';
    top: 100px;
    left: -10px;
    border-left: 40px solid transparent;
    border-right: 40px solid transparent;
    -webkit-transform: rotate(-140deg);
    -moz-transform:    rotate(-140deg);
    -ms-transform:     rotate(-140deg);
    -o-transform:      rotate(-140deg);
    z-index: -1;
  }
  .badge-ribbon:after {
    position: absolute;
    content: '';
    top: 100px;
    left: auto;
    right: -10px;
    border-left: 40px solid transparent;
    border-right: 40px solid transparent;
    -webkit-transform: rotate(140deg);
    -moz-transform:    rotate(140deg);
    -ms-transform:     rotate(140deg);
    -o-transform:      rotate(140deg);
    z-index: -1;
  }
  .badge-ribbon-quote{
    border: 6px solid #837978;
  }
  .badge-ribbon-quote:before, .badge-ribbon-quote:after{
    border-bottom: 70px solid #837978;
  }
  .badge-ribbon-site-visit, .badge-ribbon-build{
    border: 6px solid #2E3350;
  }
  .badge-ribbon-site-visit:before, .badge-ribbon-site-visit:after, .badge-ribbon-build:before, .badge-ribbon-build:after{
    border-bottom: 70px solid #2E3350;  
  }
  .badge-ribbon-design{
    border: 6px solid #E55520;
  }
  .badge-ribbon-design:before, .badge-ribbon-design:after{
    border-bottom: 70px solid #E55520;  
  }
  .quote-img{
    width: 75%;
    height: 50%;
    top: 45px;
    position: relative;
    margin: 0px auto;
    display: block;
  }
  .site-visit-img, .grey-site-visit-img{
    width: 75%;
    height: 50%;
    top: 50px;
    position: relative;
    margin: 0 auto;
    display: block;
  }
  .design-img{
    display: block;
    height: 50%;
    left: 8px;
    margin: 0 auto;
    position: relative;
    top: 40px;
    width: auto;
  }
  .build-img, .grey-build-img{
    display: block;
    height: 50%;
    margin: 0 auto;
    position: relative;
    top: 40px;
  }
  .chevron{
    font-size: 20px;
    color: #fff;
    text-align: center;
    position: relative;
    height: 80px;
    display: block;
    margin: -15px auto 0; 
    padding: 10px;
  }
  .chevron-quote, .chevron-quote:before, .chevron-quote:after{
    background: #756B6A;
    border: 0 solid #756B6A;
  }
  .chevron-site-visit, .chevron-site-visit:before, .chevron-site-visit:after, .chevron-build, .chevron-build:before, .chevron-build:after{
    background: #2E3350;
    border: 0 solid #2E3350;
  }
  .chevron-design, .chevron-design:before, .chevron-design:after{
    background: #E55520;
    border: 0 solid #E55520;
  }
  .chevron > p{
    z-index: 1;
    position: relative;
    margin-top: 10px;
    font-size: 22px !important;
  }
  .chevron:before {
    position: absolute;
    content: '';
    top: -22px;
    left: 0;
    width: 52%;
    height: 100%;
    -webkit-transform: skew(0deg, 15deg);
    -moz-transform: skew(0deg, 15deg);
    -ms-transform: skew(0deg, 15deg);
    -o-transform: skew(0deg, 15deg);
    transform: skew(0deg, 15deg);
    border-right: none;
  }
  .chevron:after {
    position: absolute;
    content: '';
    top: -22px;
    right: 0;
    height: 100%;
    width: 51%;
    -webkit-transform: skew(0deg, -15deg);
    -moz-transform: skew(0deg, -15deg);
    -ms-transform: skew(0deg, -15deg);
    -o-transform: skew(0deg, -15deg);
    transform: skew(0deg, -15deg);
    border-left: none;
  } 
  .flag{
    text-align: center;
    position: relative;
    /*height: 135px;*/
    padding: 45px 9px 55px;
    overflow: hidden;
  }
  .flag:before{
    position: absolute;
    content: "";
    left: 0;
    top: 0;
    width: 0;
    height: 0;
    border-top: 2px solid #8A817F;
    border-left: 130px solid transparent;
    border-right: 130px solid transparent;
  }
  .flag-quote-wrapper{
    background: #837978;
    font-size: 21px;
    color: #fff;
    padding: 10px 18px;
    text-align: center;
    position: relative;
    height: 115px;
    overflow: hidden;
    display: block;
  }
  .flag-quote-wrapper:hover{
    color: #fff;
    text-decoration: none;
   }
  .flag-quote-wrapper:before{
    position: absolute;
    content: "";
    left: 0;
    top: 0;
    width: 0;
    height: 0;
    border-top: 2px solid #8A817F;
    border-left: 130px solid transparent;
    border-right: 130px solid transparent;
  }
  .flag-site-visit, .flag-build{
    background: #36395C;
    font-size: 22px;
    color: #fff;
  }
  .flag-design{
    background: #F4641E;
    font-size: 22px;
    color: #fff;
  }
  .flag:hover{
    cursor: pointer;
  }
  .flag-quote-wrapper > .quote-text{
    line-height: 20px;
    padding-top: 40px !important;
  }
  .flag > .learn-more{
    line-height: 40px;
    width: 140px;
    padding-top: 0;
    margin: 0 auto;
    color: #fff;
  }
  .flag > .learn-more:hover, .flag > .learn-more:focus{
    text-decoration: none;
  }
  .ribbon-red{
    background-color: #e55520;
    bottom: 28px;
    color: #fff;
    font-size: 10px;
    left: -38px;
    padding: 4px 5px 4px 2px;
    position: absolute;
    text-align: center;
    width: 110px;
    z-index: 1;
    -webkit-transform: rotate(45deg);
    -moz-transform:    rotate(45deg);
    -ms-transform:     rotate(45deg);
    -o-transform:      rotate(45deg);
  }
  .white-triangle{
    position: absolute;
    left: 0;
    bottom: -20px;
    display: block;
    width: 100%;
    height: 50px;
    z-index: 1;
  }
  .white-triangle:before{
    position: absolute;
    content: '';
    top: 0;
    left: 0;
    width: 0;
    height: 0;
    border-bottom: 30px solid #fff;
    border-right: 130px solid transparent;
  }
  .white-triangle:after{
    position: absolute;
    content: '';
    top: 0;
    right: 0;
    width: 0;
    height: 0;
    border-bottom: 30px solid #fff;
    border-left: 130px solid transparent;
  }
  .site-visit-content, .design-content, .build-content{
    font-size: 14px !important;
    line-height: 22px;
  }
 
  .build-upload-content{
    font-size: 20px;
    margin-bottom: 5px;
  }
  .build-phase-section .btn, .build-phase-section .btn:hover,
  .build-phase-section .btn:focus{
    background-color: #3ab54a;
    border: 1px solid #3ab54a;
    border-left: none;
    color: #fff;
    font-size: 14px;
    min-height: 50px;
    padding: 6px 14px;
    font-weight: bold;
    border-radius: 4px;
  }

  /* Styles for M25 as pincode */
  .grey-chevron-site-visit, .grey-chevron-site-visit:before, .grey-chevron-site-visit:after, .grey-chevron-design, .grey-chevron-design:before, .grey-chevron-design:after, .grey-chevron-build, .grey-chevron-build:before, .grey-chevron-build:after{
    background: #C2C5CC;
    border: 3px solid #C2C5CC;
  }
  .grey-badge-ribbon-site-visit, .grey-badge-ribbon-build{
    border: 6px solid #C2C5CC;
  }
  .grey-badge-ribbon-site-visit:before, .grey-badge-ribbon-site-visit:after, .grey-badge-ribbon-build:before, .grey-badge-ribbon-build:after{
    border-bottom: 70px solid #C2C5CC;  
  }

  .newsletter-wrapper{
    margin-top: 80px;
    background-color: #353A58;
    padding: 30px;
    margin-bottom: 30px;
  }
  .newsletter-text{
    font-size: 33px;
    color: #fff;
  }
  .newsletter-wrapper input{
    font-size: 20px;
    color: #fff;
  }
  .options-wrapper p {
    font-size: 16px;
  }
  /* ================================================= */
  /*                  Styles for Options               */
  /* ================================================= */

  /* ================================================= */
/*      Styles for Upload Modal Box Starts Here      */
/* ================================================= */

.upload-label{
    color: #757575;
    font-weight: bold;
}
.upload-submit-btn, .upload-submit-btn:hover, 
.upload-submit-btn:focus, .upload-submit-btn:active{
    background-color: #f7941d !important;
    border-color: #d27302 !important;
    border-radius: 10px !important;
    font-weight: bold !important;
    width: 160px !important;
    text-transform: uppercase !important;
}
#uploadImageModal input{
    border-radius: 8px;
    box-shadow: none;
    height: 36px;
}
#uploadImageModal  textarea{
    resize: none;
}
.fileUpload, .fileUpload:hover,
.fileUpload:focus, .fileUpload:active{
    position: relative;
    overflow: hidden;
    margin: 10px 0 !important;
    border-radius: 10px !important;
    border-color: #adadad !important;
    width: 100px;
    color: #757575 !important;
    background: #efeeee !important;
    background: -moz-linear-gradient(top, #efeeee 0%, #fcfcfc 100%);
    background: -webkit-linear-gradient(top, #efeeee 0%,#fcfcfc 100%);
    background: linear-gradient(to bottom, #efeeee 0%,#fcfcfc 100%);
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#efeeee', endColorstr='#fcfcfc',GradientType=0 );
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
#uploadFile{
    background-color: #fff;
    border: 0;
    color: #757575;
}
#pincodeForm .input-group-addon, #pincodeForm .input-group-btn{
  width: auto;
}

#uploadImageForm * {
    box-sizing: border-box;
}

#uploadImageForm *::before, #uploadImageForm *::after {
    box-sizing: border-box;
}

/* Styles for new modal box */

/* The Modal (background) */
.modal-upload-image {
    display: none; /* Hidden by default */
    position: fixed; /* Stay in place */
    z-index: 1; /* Sit on top */
    padding-top: 70px; /* Location of the box */
    left: 0;
    top: 0;
    width: 100%; /* Full width */
    height: 100%; /* Full height */
    overflow: auto; /* Enable scroll if needed */
    background-color: rgb(0,0,0); /* Fallback color */
    background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content-upload-image {
    position: relative;
    background-color: #fefefe;
    margin: 0 auto 60px;
    padding: 0;
    border: 1px solid #888;
    width: 70%;
    box-shadow: 0 4px 8px 0 rgba(0,0,0,0.2),0 6px 20px 0 rgba(0,0,0,0.19);
    -webkit-animation-name: animatetop;
    -webkit-animation-duration: 0.4s;
    animation-name: animatetop;
    animation-duration: 0.4s;
    border-radius: 15px;
}

/* Add Animation */
@-webkit-keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

@keyframes animatetop {
    from {top:-300px; opacity:0}
    to {top:0; opacity:1}
}

/* The Close Button */
.close-btn {
    bottom: -20px;
    color: white;
    float: right;
    font-size: 28px;
    font-weight: bold;
    opacity: 1;
    position: absolute;
    right: -113px;
}

.close-btn:hover,
.close-btn:focus {
    color: #000;
    text-decoration: none;
    cursor: pointer;
}

.modal-header-upload-image {
    padding: 20px 0;
    border-bottom: 1px solid #e5e5e5;
    position: relative;
}

.modal-body-upload-image, .modal-footer-upload-image{
    padding: 20px 0;
}

.uploaded-file-name{
    display: inline !important;
}
.remove-btn, .remove-btn:hover,
.remove-btn:focus, .remove-btn:active{
    background-color: #fff !important;
    border: 0 !important;
    color: blue !important;
}
#add-file-field{
    margin-top: 15px;
    border: 1px solid #808385;
    border-radius: 6px !important;
    color: #000;
    padding: 0 14px;
}

/* ================================================= */
/*       Styles for Upload Modal Box Ends Here       */
/* ================================================= */

/* Click here links style*/
.click-here, .click-here:hover,
.click-here:focus, .click-here:active{
  color: #fff;
  text-decoration: underline;
}

.build-upload-content{
  padding-top: 0 !important;
}

  /* Media Queries */
  @media (min-width: 1200px){
    /* Styles for Upload Modal Box */
    .modal-content-wrapper{
        width: 800px;
        padding: 10px 30px;
    }
    .modal-upload-image{
        padding-top: 20px;
    }
    .modal-content-upload-image{
        width: 900px;
    }
    .close-btn{
        right: -70px;
    }
    .padL0{
        padding-left: 0;
    }
    .padL10{
        padding-left: 10px;
    }
    .padR10{
        padding-right: 10px;
    }
  }
  @media (min-width: 992px) and (max-width: 1199px){
    .newsletter-text{
      font-size: 30px;
      padding-top: 5px;
    }
    #newsletterForm .form-btn{
      font-size: 15px;
      padding: 12px;
    }

    /* Styles for Upload Modal Box */
    .modal-content-wrapper{
        width: 800px;
        padding: 10px 30px;
    }
    .padL0{
        padding-left: 0;
    }
    .padL10{
        padding-left: 10px;
    }
    .padR10{
        padding-right: 10px;
    }
    .modal-upload-image{
        padding-top: 20px;
    }
    .modal-content-upload-image{
        width: 900px;
    }
    .close-btn{
        right: -70px;
    }
  }
  @media (min-width: 768px) and (max-width: 930px){
    #pincodeForm .input-group {
      max-width: 600px;
    }
  }
  @media (min-width: 768px) and (max-width: 991px){
    .badge-ribbon::before{
      border-left: 20px solid transparent;
      top: 125px;
    }
    .badge-ribbon::after{
      border-right: 20px solid transparent;
      top: 125px;
    }
    .chevron::before, .chevron::after{
      width: 61%;
    }
    .chevron{
      margin: -10px auto 0;
    }
    .flag-quote-wrapper:before, .flag::before{
      border-left: 180px solid transparent;
      border-right: 180px solid transparent;
    }
    .white-triangle::before{
      border-right: 175px solid transparent;
    }
    .white-triangle::after{
      border-left: 175px solid transparent;
    }
    .newsletter-text{
      font-size: 23px;
      padding-top: 10px;
    }
    #newsletterForm .form-btn{
      font-size: 13px;
      padding: 13px 6px;
    }
    .newsletter-wrapper input{
      font-size: 17px;
    }

    /* Styles for Upload Modal Box */
    .close-btn {
        bottom: 15px;
        right: -10px;
    }
    .close-btn  img{
        width: 40px;
        height: 40px;
    }
    .padL0{
        padding-left: 0;
    }
    .padL10{
        padding-left: 10px;
    }
    .padR10{
        padding-right: 10px;
    }
    .modal-upload-image{
        padding-top: 20px;
    }
    .modal-content-upload-image{
        width: 95%;
    }
    .modal-content-wrapper{
        padding: 10px;
    }
  }
  @media (min-width: 481px) and (max-width: 767px){
    .postalcode-form-wrapper{
      float: none;
      margin: 0 auto;
      max-width: 440px;
    }
    .postcode-label{
      font-size: 16px;
    }
    .process-wrapper > div:nth-child(2), .process-wrapper > div:nth-child(3), .process-wrapper > div:last-child{
      margin-top: 40px;
    }
    .options-wrapper{
      float: none;
      margin: 0 auto;
      max-width: 440px;
    }
    .badge-ribbon{
      width: 180px;
      height: 180px;
    }
    .badge-ribbon::before{
      border-right: 20px solid transparent;
    }
    .badge-ribbon::after{
      border-left: 20px solid transparent;
    }
    .chevron::before, .chevron::after{
      width: 70%;
    }
    .chevron{
      width: 100%;
      margin: -18px auto 0;
    }
    .flag-quote-wrapper, .flag{
      width: 100%;
      margin: 0 auto;
    }
    .flag-quote-wrapper:before, .flag::before{
      border-left: 200px solid transparent;
      border-right: 200px solid transparent;
    }
    .ribbon-red{
      bottom: 50px;
      width: 160px;
    }
    .white-triangle::before{
      border-bottom: 50px solid #fff;
      border-right: 195px solid transparent;
    }
    .white-triangle::after{
      border-bottom: 50px solid #fff;
      border-left: 195px solid transparent;
    }
    .white-triangle{
      height: 70px;
    }


    .postcode-label {
      font-size: 14px;
    }
    .postcode-search-box{
      max-width: 150px;
    }
    #pincodeForm .input-group {
      max-width: 450px;
    }

    /* Styles for Upload Modal Box */
    .xxs-m15{
        margin-bottom: 15px;
    }
    .close-btn{
        bottom: 20px;
        right: -10px;
    }
    .close-btn  img{
        width: 40px;
        height: 40px;
    }

    /* Styles for multiple files upload fields */
    .modal-upload-image{
        padding-top: 20px;
    }
    .modal-content-upload-image{
        width: 95%;
    }
    .modal-content-wrapper{
        padding: 10px;
    }
  }
  @media (min-width: 771px) and (max-width: 885px){
    .process-wrapper .col_one_fourth{
      margin-right: 0;
      min-width: 45%;
      margin-left: 20px;
    }
  }
  @media (min-width: 480px) and (max-width: 770px){
    .process-wrapper .col_one_fourth{
      float: left;
      margin-left: 20px;
      margin-right: 0;
      margin-top: 0 !important;
      min-width: 45%;
      width: 45%;
    }
    .flag {
      margin: 0 auto;
      /*width: 93%;*/
    }
    .flag-quote-wrapper{
      margin: 0 auto;
      /*width: 86.5%;*/
    }
  }
  @media (min-width: 320px) and (max-width: 480px){
    .hidden-xxs{
      display: none; 
    }
    .main-heading{
      font-size: 25px;
    }
    .head-text{
      width: 270px;
      font-size: 17px;
    }
    .postalcode-form-wrapper{
      float: none;
      margin: 0 auto;
      max-width: 280px;
    }
    #pincodeForm .input-group-addon{
      white-space: normal;
    }
    .postcode-search-box{
      height: 49px;
    }
    #pincodeForm .form-btn{
      font-size: 14px;
      padding: 15px 10px;
    }
    .postcode-label{
      font-size: 13px;
      padding: 0 5px !important;
    }
    .form-btn{
      padding: 12px 20px !important;
    }
    .process-wrapper > div:nth-child(2), .process-wrapper > div:nth-child(3), .process-wrapper > div:last-child{
      margin-top: 40px;
    }
    .options-wrapper{
      float: none;
      margin: 0 auto;
      max-width: 280px;
    }
    .badge-ribbon{
      height: 150px;
      width: 150px;
      border-radius: 80px;
    }
    .quote-img, .site-visit-img, .grey-site-visit-img, .design-img, .build-img, .grey-build-img{
      height: 45%;
      width: 70%;
      top: 40px;
    }
   
    .badge-ribbon::before{
      border-right: 25px solid transparent;
    }
    .badge-ribbon::after{
      border-left: 25px solid transparent;
    }
    .chevron{
      margin: -5px auto 0;
    }
    .white-triangle::before{
      border-right: 140px solid transparent;
    }
    .white-triangle::after{
      border-left: 140px solid transparent;
    }
    .newsletter-wrapper{
      margin-top: 40px;
    }
    .newsletter-text{
      font-size: 19px;
    }
    #newsletterForm .form-btn{
      font-size: 12px;
      padding: 16px 6px;
    }
    .newsletter-wrapper input{
      font-size: 17px;
    }

    .postcode-label {
      font-size: 14px;
    }
    .postcode-search-box{
      max-width: 100px;
    }
    #pincodeForm .input-group {
      max-width: 318px;
    }

    /* Styles for Upload Modal Box */
    .upload-label{
        font-size: 20px;
    }
    .close-btn{
        bottom: 15px;
        right: -10px;
    }
    .close-btn  img{
        width: 30px;
        height: 30px;
    }
    .xxs-m15{
        margin-bottom: 15px;
    }
    #uploadFile{
        width: 140px;
    }
    .modal-upload-image{
        padding-top: 20px;
    }
    .modal-content-upload-image{
        width: 95%;
    }
    .modal-content-wrapper{
        padding: 10px;
    }

    /* Styles for multiple files upload fields */
    .added-field{
        margin-top: 15px;
        
    }
    .uploaded-file-name {
        width: 190px !important;
    }
    .remove-btn{
        padding: 0 !important;
        margin-left: 5px !important;
    }
  }

</style>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<script type="text/javascript">

$(document).ready(function () {

    $('#uploadBtn').on('change', function(){
      $('#uploadFile').val($(this).val());
    });
    $(window).innerWidth();   // returns width of browser viewport
    $(document).width();
    var twoChars, threeChars, fourChars, london2;
    /* Hiding the content showed on entering invalid pincode during DOM load*/
    $('.grey-site-visit-img, .grey-design-img, .grey-build-img').hide();
    $('.options-content').css('visibility', 'hidden');
    /* Setting the flag, used in expanding or closing of options, as false */
    var flagSite = flagDesign = flagBuild = false;

    /* Hiding the option descriptions during DOM load */
    $('.site-visit-content, .design-content, .build-phase-section, #myBtn').hide();

    /* All Pincodes */
    var all = ["WD6", "EN5", "EN4", "EN2", "EN1", "EN3", "E4", "N9", "N21", "N14", "N13", "N18", "E17", "E18", "E10", "E11", "E15", "E7", "E12", "E13", "E6",
        "E16", "SE28", "SE18", "SE2", "SE9", "DA16", "DA15", "BR7", "BR1", "BR2", "BR4", "BR3", "CR0", "SE25", "CR7", "CR4", "SM6", "SM5", "SM1",
        "SM4", "SM3", "KT4", "KT4", "KT5", "KT1", "KT2", "TW11", "TW12", "TW2", "TW10", "TW1", "TW9", "TW8", "TW7", "TW3", "TW4", "TW5", "UB2",
        "UB1", "UB5", "UB6", "HA0", "HA9", "HA2", "HA1", "HA3", "NW9", "NW4", "N3", "NW7", "N12", "N11", "N20", "HA8", "HA7"];

    var inM25 = ['SW20', 'SW19', 'SW17', 'SW16', 'SE19', 'SE20', 'SE26', 'SE6', 'SE12', 'SE3', 'SE7', 'SE13', 'SE23', 'SE27', 'SE21', 'SE22', 
        'SE4', 'SE14','SE8', 'SE10', 'E14', 'E3', 'E9', 'SE16', 'SE15', 'SE24', 'SW2', 'SW12', 'SW4', 'SW9', 'SE5', 'SW18', 'SW15', 'SW14', 'SW13', 
        'SW6', 'SW11', 'SW8','SW1', 'SE11', 'SE17', 'SE1', 'EC3', 'EC2', 'E1', 'E2', 'E8', 'E5', 'N16', 'N15', 'N17', 'N22', 'N8', 'N4', 'N5', 'N1', 'EC1', 
        'EC4', 'WC2', 'WC1', 'NW1', 'W1', 'W2','SW7', 'SW3', 'W14', 'W6', 'W4', 'W3', 'W5', 'W13', 'W7', 'NW10', 'NW2', 'NW11', 'N2', 'N10', 'N6', 
        'N19', 'N7', 'NW5', 'NW3', 'NW6', 'NW8', 'W9', 'W10','W12', 'W11', 'W8', 'SW10', 'SW5'];

    var outLondon1 = ["AB", "AL", "BA", "BB", "BD", "BH", "BL", "BN", "BS", "CA", "CF", "CH", "CM", "CO", "CT", "CV", "CW", "DE", "DG",
        "DH", "DL","DN", "DT", "DY", "EH", "EX", "FK", "FY", "GL", "GU", "HD", "HG", "HP", "HR", "HU", "HX", "IV", "KA", "KW", "KY",
        "LA", "LD", "LE", "LL","LN", "LS", "LU", "ME", "MK", "ML", "NE", "NG", "NN", "NP", "NR", "OL", "OX", "PA", "PE", "PH", "PL", "PO",
        "PR", "RG", "RH", "SA", "SG","SK", "SL", "SN", "SO", "SP", "SR", "SS", "ST", "SY", "TA", "TD", "TF", "TN", "TQ", "TR", "TS", 
        "WA", "WF", "WN", "WR", "WS", "WV", "YO"];
    
        var outLondon2 = ["G1","G2","G3","G4","G5","G9","G11","G12","G13","G14","G15","G20","G21","G22","G23",
        "G31","G32","G33","G34","G40","G41","G42","G43","G44","G45","G46","G51","G52","G53","G58","G60","G61","G62","G63","G64",
        "G65","G66","G67","G68","G69","G70","G71","G72","G73","G74","G75","G76","G77","G78","G79","G81","G82","G83","G84","S1","S2",
        "S3","S4","S5","S6","S7","S8","S9","S10","S11","S12","S13","S14","S17","S18","Stw2 6hg19","S20","S21","S25","S26","S30","S31","S32","S33",
        "S35","S36","S40","S41","S42","S43","S44","S45","S49","S60","S61","S62","S63","S64","S65","S66","S70","S71","S72","S73","S74",
        "S75","S80","S81","M1","M2","M3","M3","M4","M5","M6","M7","M8","M9","M11","M12","M13","M14","M15","M16","M17","M18","M19","M20",
        "M21","M22","M23","M24","M25","M26","M27","M28","M29","M30","M31","M32","M33","M34","M35","M38","M40","M41","M43","M44","M45",
        "M46","M50","L1","L2","L3","L4","L5","L6","L7","L8","L9","L10","L11","L12","L13","L14","L15","L16","L17","L18","L19","L20","L20","L21","L22",
        "L23","L24","L25","L26","L27","L28","L29","L30","L31","L32","L33","L34","L35","L36","L37","L38","L39","L40","B1","B2","B3","B4","B5",
        "B6","B7","B8","B9","B10","B11","B12","B13","B14","B15","B16","B17","B18","B19","B20","B21","B23","B24","B25","B26","B27","B28","B29",
        "B30","B31","B32","B33","B34","B35","B36","B37","B38","B40","B42","B43","B44","B45","B46","B47","B48","B49","B50","B60","B61",
        "B62","B63","B64","B65","B66","B67","B68","B69","B70","B71","B72","B73","B74","B75","B76","B77","B78","B79","B80","B90","B91",
        "B92","B93","B94","B95","B96","B97","B98"];

    /* Disabling the submit button if length is less than 5 */
    $('#postcode').on('keyup', function (e) {
        // var regex = /^[!@#\$%\^\&*\)\(+=._-]+$/g;
        var fieldValue = $(this).val();
        // if(regex.test(fieldValue)){
        //     $('#search').attr('disabled', 'disabled');
        // }else{
        //     $('#search').removeAttr('disabled');
        // }
       if (fieldValue.length >= 6) {
           $('#search').removeAttr('disabled');
       } else if (fieldValue.length < 6) {
           $('#search').attr('disabled', 'disabled');
       }
       if(e.keyCode == 13){
        clickMe();
       }
    });

    $(document).on('click', '.flag', function (e) {
        /* Storing the value of the class of the current div clicked */
        var classVal = $(this).attr('class');
        /* Performing functions based on the class of the div clicked */
        if ((classVal.indexOf('flag-site-visit') >= 0) && (classVal.indexOf('disable-all') == -1)) {
            if (flagSite == false) {
                flagSite = true;
                $(this).parent('div').find('.learn-more').hide();
                if (window.innerWidth >= 991 && window.innerWidth <= 1199) {
                    $(this).animate({
                        height: 'auto'
                    }, 1000).css({
                        'padding-bottom': '30px',
                        'padding-top': '10px',
                        'height': 'auto'
                    });
                } else {
                    $(this).animate({
                        height: 'auto'
                    }, 1000).css({
                        'padding-bottom': '30px',
                        'padding-top': '10px',
                        'height': 'auto'
                    });
                    /*$('.site-visit-content').show(4000);*/
                    $('.site-visit-content').slideDown('slow');
                }
            } else {
                flagSite = false;
                $(this).animate({
                    height: '40px'
                }, 500).css({'padding': '45px 9px 55px', 'height': 'auto'});
                /*$('.site-visit-content').hide(500);*/
    $('.site-visit-content').slideUp(500, function(){
      $(this).parent('div').find('.learn-more').fadeIn(200);
    });
                /*$(this).parent('div').find('.learn-more').fadeIn(4000);*/
            }
        } else if (classVal.indexOf('flag-design') >= 0 && (classVal.indexOf('disable-all') == -1)) {
            if (flagDesign == false) {
                flagDesign = true;
                $(this).parent('div').find('.learn-more').hide();
                if (window.innerWidth >= 991 && window.innerWidth <= 1199) {
                    $(this).animate({
                        height: 'auto'
                    }, 1000).css({
                        'padding-bottom': '30px',
                        'padding-top': '10px',
                        'height': 'auto'
                    });
                } else {
                    $(this).animate({
                        height: 'auto'
                    }, 1000).css({
                        'padding-bottom': '30px',
                        'padding-top': '10px',
                        'height': 'auto'
                    });
                   /* $('.design-content').show(1000);*/
                    $('.design-content').slideDown(1000);
                }
            } else {
                flagDesign = false;
                $(this).animate({
                    height: '40px'
                }, 500).css({
                    'padding': '45px 9px 55px',
                    'height': 'auto'
                });
                /*$('.design-content').hide(500);*/
                $('.design-content').slideUp(500, function(){
       $(this).parent('div').find('.learn-more').fadeIn(200);
    });
                /*$(this).parent('div').find('.learn-more').fadeIn(500);*/
            }
        } else if (classVal.indexOf('flag-build') >= 0 && (classVal.indexOf('disable-all') == -1)) {
            if (flagBuild == false) {
                flagBuild = true;
                $(this).parent('div').find('.learn-more').hide();
                if (window.innerWidth >= 991 && window.innerWidth <= 1199) {
                    $(this).animate({
                        height: 'auto'
                    }, 1000).css({
                        'padding-bottom': '30px',
                        'padding-top': '10px',
                        'height': 'auto'
                    });
                } else {
                    $(this).animate({
                        height: 'auto'
                    }, 1000).css({
                        'padding-bottom': '30px',
                        'padding-top': '10px',
                        'height': 'auto'
                    });
                    // $('.build-phase-section,.build-content, .build-upload-content').show(1000);
                    $('.build-phase-section, .build-upload-content, #myBtn').slideDown('slow');
                }
            } else {
                flagBuild = false;
                $('#myBtn').hide();
                $(this).animate({
                    height: '40px'
                }, 500).css({
                    'padding': '45px 9px 55px',
                    'height': 'auto'
                });
                // $('.build-phase-section').hide(500);
                $('.build-phase-section, .build-upload-content, #myBtn').slideUp(500, function(){
                  $(this).parent('div').find('.learn-more').fadeIn(2000);  
                });
                // $(this).parent('div').find('.learn-more').fadeIn(500);
            }
        }
        e.stopPropagation();
    });
    function clickMe(){
        flagSite = flagDesign = flagBuild = false;
        $('.site-visit-content, .design-content, .build-phase-section, #myBtn').hide();
        $('.learn-more').show();
        $('.flag').animate({
            height: 'auto'
        }).css({
            'padding': '45px 9px 55px',
            'height': 'auto'
        });
        var pincode = $('#postcode').val().toUpperCase();
        switch (pincode.length) {
            case 6:
                twoChars = pincode.substr(0, 2);
                if ($.inArray(twoChars, all) >= 0) {
                    checkForAll();
                } else if ($.inArray(twoChars, inM25) >= 0) {
                    sameText();
                } else if($.inArray(twoChars, outLondon2) >= 0){
                    outsideLondon();
                } else if (($.inArray(twoChars, inM25) == -1) && ($.inArray(twoChars, all) == -1) && ($.inArray(twoChars, outLondon2) == -1)) {
                    outsideEngland();
                }
                break;
            case 7:
                threeChars = pincode.substr(0, 3);
                london2 = pincode.substr(0,2);
                if ($.inArray(threeChars, all) >= 0) {
                    checkForAll();
                } else if ($.inArray(threeChars, inM25) >= 0) {
                    sameText();
                } else if ($.inArray(london2, outLondon1) >= 0) {
                    outsideLondon();
                }  else if ($.inArray(threeChars, outLondon2) >= 0) {
                   outsideLondon();
                } else if (($.inArray(threeChars, inM25) == -1) && ($.inArray(threeChars, all) == -1) && ($.inArray(threeChars, outLondon1) == -1) && ($.inArray(threeChars, outLondon2) == -1)) {
                    outsideEngland();
                } 
                break;
            case 8:
                fourChars = pincode.substr(0, 4);
                london2 = pincode.substr(0,2);
                if ($.inArray(fourChars, all) >= 0) {
                    checkForAll();
                } else if ($.inArray(fourChars, inM25) >= 0) {
                    sameText();
                } else if ($.inArray(london2, outLondon1) >= 0) {
                    outsideLondon();
                }  else if ($.inArray(fourChars, outLondon2) >= 0) {
                   outsideLondon();
                } else if (($.inArray(fourChars, inM25) == -1) && ($.inArray(fourChars, all) == -1) && ($.inArray(fourChars, outLondon1) == -1) && ($.inArray(fourChars, outLondon2) == -1)) {
                    outsideEngland();
                } 
                break;
        }
    }
    $('#search').click(function () {
        clickMe();
    });

    function outsideLondon() {
        $('.flag').removeClass('disable-all');
        $('.site-visit-img, .build-img, .grey-design-img').hide();
        $('.grey-site-visit-img, .design-img, .grey-build-img').show();
        $('.options-content').html('Due to your location, we wouldn\'t be able to offer your a Site Visit or our Build Service, however we do offer a Nationwide Design Service, details of which can be found below.').css({'visibility':'visible', 'max-width':'90%'});
        $('.design-content').html('As you are outside of our coverage area, we would like to offer you our Nationwide Design Service. We would take a small deposit and a member of our Architectural Design Team will meet you at your property to explain our Design Process, measure up, and talk through design options with you. Following this meeting, you can decide whether you want to proceed with our Design Phase. To learn more about our Nationwide Design Service, please <a href="http://www.buildteam.com/getting-started/nationwide.html" class="click-here">click here.</a>');
        $('.badge-ribbon-site-visit, .badge-ribbon-build').css('border-color', '#C2C5CC');
        $('.badge-ribbon-design').css('border-color', '#E55520');
        $('.badge-ribbon-site-visit:before, .badge-ribbon-site-visit:after, .badge-ribbon-build:before, .badge-ribbon-build:after').css('border-bottom', '#C2C5CC');
        $('.badge-ribbon-design:before, .badge-ribbon-design:after').css('border-bottom', '#E55520');
        $('.chevron-site-visit').addClass('grey-chevron-site-visit');
        $('.chevron-build').addClass('grey-chevron-build');
        $('.chevron-design').removeClass('grey-chevron-design');
        $('.badge-ribbon-site-visit').addClass('grey-badge-ribbon-site-visit');
        $('.badge-ribbon-build').addClass('grey-badge-ribbon-build');
        $('.badge-ribbon-design').removeClass('grey-badge-ribbon-design');
        $('.flag-site-visit, .flag-build').css('background', '#C2C5CC');
        $('.flag-design').css('background', '#F4641E');
        $('.flag-site-visit, .flag-build').addClass('disable-all');
        /* Disabling the click on Learn More of Site Visit and Build Phase */
        $('.flag-site-visit, .flag-build').off('click');
    }

    function outsideEngland() {
        $('.site-visit-img, .design-img, .build-img').hide();
        $('.grey-site-visit-img, .grey-design-img, .grey-build-img').show();
        $('.options-content').html('As you are outside of our coverage area, we wouldn\'t be able to assist at this stage. '+
          ' <a href="http://www.buildteam.com/contact.html">Contact Us</a> if anything is unclear or if you have any questions. ').css({'visibility':'visible', 'max-width':'90%'});
        $('.badge-ribbon-site-visit, .badge-ribbon-design, .badge-ribbon-build').css('border-color', '#C2C5CC');
        $('.badge-ribbon-site-visit:before, .badge-ribbon-site-visit:after, .badge-ribbon-design:before, .badge-ribbon-design:after, .badge-ribbon-build:before, .badge-ribbon-build:after').css('border-bottom', '#C2C5CC');
        $('.chevron-site-visit').addClass('grey-chevron-site-visit');
        $('.chevron-design').addClass('grey-chevron-design');
        $('.chevron-build').addClass('grey-chevron-build');
        $('.badge-ribbon-site-visit').addClass('grey-badge-ribbon-site-visit');
        $('.badge-ribbon-design').addClass('grey-badge-ribbon-design');
        $('.badge-ribbon-build').addClass('grey-badge-ribbon-build');
        $('.flag').addClass('disable-all');
        $('.flag').css('background', '#C2C5CC');
        /* Disabling the click on Learn More of Site Visit and Build Phase */
        $('.flag').unbind('click');
    }

    function sameText() {
        $('.flag').removeClass('disable-all');
        $('.grey-site-visit-img, .grey-design-img, .grey-build-img').hide();
        $('.site-visit-content').html(' Meet a member of our Design Team at your property to discuss feasibility, design options and the process. Following the site visit, we\'ll be able to send over a more in-depth quote. To book a site visit, please <a href="http://www.buildteam.com/getting-started/book-site-visit.html" class="click-here">click here</a>');
        $('.design-content').html('Our Design Phase includes everything you need to get you on site and start your build. We create your design, get you through planning and create your Structural Pack. We can even help with Party Wall, Landscape Design and Interior Design if you want us to. To learn more about our Design Service, please <a href="http://www.buildteam.com/what-we-do/architectural-design-phase-page.html" class="click-here">click here.</a>');
        $('.site-visit-img, .design-img, .build-img').show();
        $('.options-content').text('We can offer you all of our services which are detailed below.').css({'visibility':'visible', 'max-width':'90%'});
        $('.badge-ribbon-site-visit, .badge-ribbon-build').css('border-color', '#2E3350');
        $('.badge-ribbon-site-visit:before, .badge-ribbon-site-visit:after, .badge-ribbon-build:before, .badge-ribbon-build:after').css('border-bottom', '#C2C5CC');
        $('.badge-ribbon-design, .badge-ribbon-design:before, .badge-ribbon-design:after').css('border-color', '#E55520');
        $('.chevron-site-visit').removeClass('grey-chevron-site-visit');
        $('.chevron-design').removeClass('grey-chevron-design');
        $('.chevron-build').removeClass('grey-chevron-build');
        $('.badge-ribbon-site-visit').removeClass('grey-badge-ribbon-site-visit');
        $('.badge-ribbon-design').removeClass('grey-badge-ribbon-design');
        $('.badge-ribbon-build').removeClass('grey-badge-ribbon-build');
        $('.flag-site-visit, .flag-build').css('background', '#36395C');
        $('.flag-design').css('background', '#F4641E');
    }

    function checkForAll() {
        flagSite = flagDesign = flagBuild = false;
        $('.flag').removeClass('disable-all');
        $('.grey-site-visit-img, .grey-design-img, .grey-build-img').hide();
        $('.site-visit-content').html('As you are outside of our coverage area, we would be able to offer you Premium Site Visit. This site visit ' +
                'is &pound;125 + VAT and a member of our Architectural Design Team would meet you at your property to discuss design ' +
                'and layout options, planning permission, party wall matters, and explain how we work. This fee is refundable against ' +
                'the Design Instruction. To book a site visit, please <a href=" http://www.buildteam.com/book-visit.html?type=premium" class="click-here">click here</a>');
        $('.design-content').html('Our Design Phase includes everything you need to get you on site and start your build. We create your design, get you through planning and create your Structural Pack. We can even help with Party Wall, Landscape Design and Interior Design if you want us to. To learn more about our Design Service, please <a href="http://www.buildteam.com/what-we-do/architectural-design-phase-page.html" class="click-here">click here.</a>');
        $('.site-visit-img, .design-img, .build-img').show();
        $('.options-content').text('We can offer you all of our services, however we would only be able to offer you a Premium Site Visit, '+
                        'details of which can be found below.').css({'visibility':'visible', 'max-width':'70%'});
        $('.badge-ribbon-site-visit, .badge-ribbon-build').css('border-color', '#2E3350');
        $('.badge-ribbon-site-visit:before, .badge-ribbon-site-visit:after, .badge-ribbon-build:before, .badge-ribbon-build:after').css('border-bottom', '#C2C5CC');
        $('.badge-ribbon-design, .badge-ribbon-design:before, .badge-ribbon-design:after').css('border-color', '#E55520');
        $('.chevron-site-visit').removeClass('grey-chevron-site-visit');
        $('.chevron-design').removeClass('grey-chevron-design');
        $('.chevron-build').removeClass('grey-chevron-build');
        $('.badge-ribbon-site-visit').removeClass('grey-badge-ribbon-site-visit');
        $('.badge-ribbon-design').removeClass('grey-badge-ribbon-design');
        $('.badge-ribbon-build').removeClass('grey-badge-ribbon-build');
        $('.flag-site-visit, .flag-build').css('background', '#36395C');
        $('.flag-design').css('background', '#F4641E');
    }
});
</script>


<div class="main">
<h3 class="text-center main-heading">How can we help you?</h3>
<hr class="head-bar">
<p class="text-center head-text">Tell us your postcode so we can tell you more about what we can offer you</p>

<form name="pincodeForm" id="pincodeForm" class="" onsubmit="return false;">
  <div class="form-group">
    <div class="input-group">
      <div class="input-group-addon postcode-label"><span class="hidden-xxs">Enter Your</span> Postcode</div>
      <input type="text" id="postcode" class="form-control postcode-search-box" minlength="6" maxlength="8">
      <div class="input-group-addon form-btn-wrapper">
        <button type="button" id="search" class="btn btn-primary form-btn" disabled="true" >ENTER</button>
      </div>
    </div>
  </div>
</form>


<div class="row">
  <div class="col-xs-12">
    <div class="text-wrapper">
      <p class="options-content all-service-content text-center">
          We can offer you all of our services which are detailed below.
      </p>
    </div>
  </div>
</div>
<div class="row process-wrapper">
  <div class="col_one_fourth">
    <div class="options-wrapper">
      <div class="badge-ribbon badge-ribbon-quote">
        <img class="quote-img" src="../images/quote.png">
      </div>
      <div class="chevron chevron-quote">
        <p>
          I Want a Quote
        </p>
      </div>
      <a href="http://www.buildteam.com/build-your-price-start.html" class="flag-quote-wrapper">
        <p class="quote-text">
          You can do this online - its free!
        </p>
        <div class="ribbon-red">FREE</div>
        <div class="white-triangle"></div>
      </a>
    </div>
  </div>
  <div class="col_one_fourth">
    <div class="options-wrapper">
      <div class="badge-ribbon badge-ribbon-site-visit">
        <img class="site-visit-img" src="../images/site_visit.png">
        <img class="grey-site-visit-img" src="../images/grey_site_visit.png">
      </div>
      <div class="chevron chevron-site-visit">
        <p>
          Book a Site Visit
        </p>
      </div>
      <div class="flag flag-site-visit">
        <p class="learn-more">Learn More</p>
        <p class="site-visit-content">
          Meet a member of our Design Team at your property to discuss feasibility, design options and the process. Following the site visit, we'll be able to send over a more in-depth quote.To book a site visit, please 
          <a href="http://www.buildteam.com/getting-started/book-site-visit.html" class="click-here">click here</a>.
        </p>
        <div class="white-triangle"></div>
      </div>
    </div>
  </div>
  <div class="col_one_fourth">
    <div class="options-wrapper">
      <div class="badge-ribbon badge-ribbon-design">
        <img class="design-img" src="../images/design.png">
        <img class="grey-design-img" src="../images/grey_design.png">
      </div>
      <div class="chevron chevron-design">
        <p>
          Design Phase
        </p>
      </div>
      <div class="flag flag-design">
        <p class="learn-more">Learn More</p>
        <p class="design-content">
          Our Design Phase includes everything you need to get you on site and start your build. We create your design, 
          get you through planning and create your Structural Pack. We can even help with Party Wall, Landscape Design and 
          Interior Design if you want us to. To learn more about our Design Service, please <a href="http://www.buildteam.com/what-we-do/architectural-design-phase-page.html" class="click-here">click here.</a>
        </p>
        <div class="white-triangle"></div>
      </div>
    </div>
  </div>
  <div class="col_one_fourth">
    <div class="options-wrapper">
      <div class="badge-ribbon badge-ribbon-build">
        <img class="build-img" src="../images/build.png">
        <img class="grey-build-img" src="../images/grey_build.png">
      </div>
      <div class="chevron chevron-build">
        <p>
          Build Phase
        </p>
      </div>
      <div class="flag flag-build">
        <p class="learn-more">Learn More</p>
        <!-- <div class="build-phase-section"> -->
        <p class="build-phase-section build-content">
          We can help in a variety of ways for your Build. <a href="http://www.buildteam.com/what-we-do/build-phase.html" class="click-here">Click here</a> to learn more about the services we can offer you.
        
          <span class="build-upload-content build-content">
            If you have drawings already, get a Build Only Quote.
          </span>
        <button type="button" id="myBtn" class="btn btn-primary form-btn">UPLOAD HERE</button>
        </p>
        <!-- </div> -->
        <div class="white-triangle"></div>
      </div>
    </div>
  </div>
</div>

<!-- The Modal -->
<div id="myModal" class="modal-upload-image">

  <!-- Modal content -->
  <div class="modal-content-upload-image" id="uploadImageModal">
      <div class="modal-content-wrapper">
          <div class="modal-header-upload-image">
              <span class="close-btn"><img src="../images/close-btn.png"></span>
              <h3 class="modal-title upload-label">Upload your Drawings</h3>
          </div>
          <div class="modal-body-upload-image">
              <form class="form-horizontal" id="uploadImageForm" action="" method="post" enctype="multipart/form-data">
                  <div class="form-group">
                      <div class="col-xs-12 col-sm-2 xxs-m15">
                          <select class="form-control" name="salutation" id="salutation">
                              <option value="mr">Mr</option>
                              <option value="miss">Miss</option>
                              <option value="ms">Ms</option>
                              <option value="mrs">Mrs</option>
                          </select>
                      </div>
                      <div class="col-xs-12 col-sm-5 xxs-m15 padL0">
                          <input type="text" class="form-control" id="name" name="name" placeholder="Name">
                      </div>
                      <div class="col-xs-12 col-sm-5 padL0">
                          <input type="text" class="form-control" id="surname" name="surname" placeholder="Surname">
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <input type="text" class="form-control" id="address1" name="address1" placeholder="Address line 1">
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <input type="text" class="form-control" id="address2" name="address2" placeholder="Address line 2">
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <input type="text" class="form-control" id="postcode" name="postcode" placeholder="Postcode">
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12 col-sm-6 xxs-m15 padR10">
                          <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Mobile">
                      </div>
                      <div class="col-xs-12 col-sm-6 padL10">
                          <input type="email" class="form-control" id="email" name="email" placeholder="Email">
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <!-- <div class="fileUpload btn btn-primary">
                              <span>Upload</span>
                              <input id="uploadBtn" name="uploadBtn" type="file" class="upload" />
                          </div>
                          <input id="uploadFile" placeholder="No File Choosen" disabled="disabled" /> -->
                          <div id="text">
                              <div class="first-file">
                                  <input name="data[]" type="file" />
                              </div>
                              <!-- This is where the new file field will appear -->
                          </div>
                          <input type="button" id="add-file-field" name="add" value="Add More" />
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="col-xs-12">
                          <textarea name="comments" id="comments" class="form-control" rows="5" placeholder="Comments"></textarea>  
                      </div>
                  </div>
              </div>
              <div class="modal-footer-upload-image">
                  <button type="submit" class="btn btn-primary upload-submit-btn">Submit</button>
              </div>
          </form>
      </div>
  </div>
</div>
</div>
<script>
    // Get the modal
    var modal = document.getElementById('myModal');

    // Get the button that opens the modal
    var btn = document.getElementById("myBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close-btn")[0];

    // When the user clicks the button, open the modal
    btn.onclick = function() {
        modal.style.display = "block";
    }

    // When the user clicks on <span> (x), close the modal
    span.onclick = function() {
        modal.style.display = "none";
    }

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
<script>
  $(document).ready(function(){
    $("#add-file-field").click(function(){
      $("#text").append("<div class='added-field'><input name='data[]' type='file' class='uploaded-file-name' /><input type='button' class='btn btn-default remove-btn' value='Remove' /></div>");
    });
    // The live function binds elements which are added to the DOM at later time
    // So the newly added field can be removed too
    $(document).on('click', '.remove-btn',function() {
        $(this).parent().remove();
    });
  });
</script>
<?php
  require_once('./inc/footer.inc.php');
?>