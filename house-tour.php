<?php   
	require_once('./inc/header.inc.php');
 ?>
 <style>
 .full{ padding:10px;}
 .content_text p{ font-size:14px; text-align:justify; }
  .content_text p a{color:#f5641e;}
   .imagelist{ margin-top:30px; margin-bottom:30px; border-top:1px solid #333; padding-top:30px;}
  .imagelist ul li{ list-style:none; float:left; width:166px; padding-left:19px;}
  .imagelist ul li:first-child{ padding-left:0px;}
  .content_text{ margin:15px 0px;}
  .content_text p span.blue{ color:#233b76;}
 </style>
	
<div class="full">
<h1 style="padding-bottom: 20px;">House Tours</h1>
<div class="player">
 <video width="100%" controls poster="housetour/video_poster.png" preload="none" height="100%">
  <source src="housetour/BuildTeam_House_Tour No_Strap.mp4" type="video/mp4">
  <source src="housetour/BuildTeam_House_Tour No_Strap.ogg" type="video/ogg">
</video> 
</div>

<div class="content_text">
<p> <span class="blue">Seeking inspiration, further guidance or keen to see our superior quality of workmanship in person? </span> <br/>
Introducing: Open House. A free, no-obligation opportunity for our prospective Design and Build clients to tour one of our fantastic, completed Builds. Both members of our Design and Build Teams will be on hand during the day to assist with any queries. We offer hourly slots but feel free to stay as long as you need - grab a drink, relax and chat to our team. <a href="#">Check out our schedule below and submit the form to register your interest.</a> We're always adding new dates in so if you want to come along, but can't see a Tour Date that's convenient for you, please do register your interest and we'll send you a note as soon as a day comes up that fits your requirement.</p>
</div>
<div class="imagelist">
<ul>
<li><a href="/housetour/HouseTour_1.jpg" rel="shadowbox[project_camberwell-se5-loft]"><img src="housetour/HouseTour_1s.jpg"></a></li>
<li><a href="/housetour/HouseTour_2.jpg" rel="shadowbox[project_camberwell-se5-loft]"><img src="housetour/HouseTour_2s.jpg"></a></li>
<li><a href="/housetour/HouseTour_3.jpg" rel="shadowbox[project_camberwell-se5-loft]"><img src="housetour/HouseTour_3s.jpg"></a></li>
<li><a href="/housetour/HouseTour_4.jpg" rel="shadowbox[project_camberwell-se5-loft]"><img src="housetour/HouseTour_4s.jpg"></a></li>
<li><a href="/housetour/HouseTour_5.jpg" rel="shadowbox[project_camberwell-se5-loft]"><img src="housetour/HouseTour_5s.jpg"></a></li>
</ul>
</div>



</div>

<?php require_once('./inc/footer.inc.php'); ?>