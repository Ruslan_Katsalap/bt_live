<?php
header('HTTP/1.1 301 Moved Permanently');
require_once('./inc/header.inc.php');

$rs = getRs("SELECT * FROM page WHERE page_id=81 AND is_enabled=1 limit 1");
if ($row = mysqli_fetch_assoc($rs)) {
    $page_content = $row['page_content'];
    echo $page_content;
}
/*
$rs1 = getRs("SELECT n.news_id, n.news_code, n.title, n.imagefile, n.description 
                FROM news n 
                JOIN news_category c ON c.news_category_id = n.news_category_id 
                WHERE c.is_enabled = 0 
                AND c.is_active = 1 
                AND n.is_enabled = 1 
                AND n.is_active = 1
                AND c.news_category_id = 10");
?>
<div class="our-team-main-content">
    <?php
    $i = 0;
    while ($row1 = mysqli_fetch_assoc($rs1)) {
        $show_detail = true;
        $news_code = $row1['news_code'];
        $title = $row1['title'];
        $imagefile = $row1['imagefile'];
        $description = $row1['description'];
        $meta_title = $row1['title'];
        $cls = "";
        if ($i % 2 == 0) {
            $cls = "ourteam-sec-lightgray";
           
        } else {
            $cls = "ourteam-sec-gray";
     		
        }
        ?>

        <div class="<?php echo $cls; ?>">
          <!--  <img src="../media/<?php //echo $imagefile;?>" />  -->
            <h3><?php echo $title;?></h3>
            <?php
                //explode("<p>", $description);
					echo $description;
			?>
        </div>
        <?php
        $i++;
    }
    ?>
</div>
 */?>

<?
$rs1 = getRs("SELECT n.id, n.title, n.pdffile, n.description 
                FROM join_our_team n 
               WHERE n.is_active=1");
?>
<div class="our-team-main-content">
    <?php
    $i = 0;
    while ($row1 = mysqli_fetch_assoc($rs1)) {
        $show_detail = true;
        $title = $row1['title'];
        $imagefile = $row1['pdffile'];
        $description = $row1['description'];
        $meta_title = $row1['title'];
        $cls = "";
        if ($i % 2 == 0) {
            $cls = "ourteam-sec-lightgray";
           
        } else {
            $cls = "ourteam-sec-gray";
     		
        }
        ?>

        <div class="<?php echo $cls; ?>">
          <!--  <img src="../media/<?php //echo $imagefile;?>" />  -->
            <h3><?php echo $title;?></h3>
            <p class="page-text"><?php
                //explode("<p>", $description);
					echo $description;
			?></p>
			<p>
				<a class="out-team-learn-more-btn" href="http://www.buildteam.com/pdf/<?=$imagefile?>" target="_blank">LEARN MORE</a>
				</p>
        </div>
        <?php
        $i++;
    }
    ?>
</div>
<?php
require_once('./inc/footer.inc.php');
?>
<script>
    jQuery(document).ready(function () {
        jQuery("#content").find("div").first().removeClass("col_two_fifth");
        jQuery("#content")..addClass("media-master");
        jQuery("#content").find("div").first().addClass("full-media-banner");
        jQuery("#content").find("div").first().addClass("full");
    });
</script>
